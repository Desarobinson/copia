--
-- PostgreSQL database dump
--

-- Dumped from database version 10.14 (Ubuntu 10.14-0ubuntu0.18.04.1)
-- Dumped by pg_dump version 10.14 (Ubuntu 10.14-0ubuntu0.18.04.1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: activar_reportes; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.activar_reportes (
    id_activar_rpt integer NOT NULL,
    rpt_cobranza boolean NOT NULL
);


ALTER TABLE public.activar_reportes OWNER TO postgres;

--
-- Name: activar_reportes_id_activar_rpt_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.activar_reportes_id_activar_rpt_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.activar_reportes_id_activar_rpt_seq OWNER TO postgres;

--
-- Name: activar_reportes_id_activar_rpt_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.activar_reportes_id_activar_rpt_seq OWNED BY public.activar_reportes.id_activar_rpt;


--
-- Name: agencia; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.agencia (
    id_agencia integer NOT NULL,
    id_empresa integer NOT NULL,
    nombre_agencia character varying(50) NOT NULL,
    direccion_agencia character varying(50) NOT NULL,
    telefono_agencia character varying(50),
    codigo character(3),
    mensaje character varying(250)
);


ALTER TABLE public.agencia OWNER TO postgres;

--
-- Name: agencia_id_agencia_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.agencia_id_agencia_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.agencia_id_agencia_seq OWNER TO postgres;

--
-- Name: agencia_id_agencia_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.agencia_id_agencia_seq OWNED BY public.agencia.id_agencia;


--
-- Name: agencia_id_empresa_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.agencia_id_empresa_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.agencia_id_empresa_seq OWNER TO postgres;

--
-- Name: agencia_id_empresa_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.agencia_id_empresa_seq OWNED BY public.agencia.id_empresa;


--
-- Name: apertura_cierre; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.apertura_cierre (
    id_caja_apertura_cierre bigint NOT NULL,
    equipo_registrado character varying(50) NOT NULL,
    id_usuario_apertura integer NOT NULL,
    fecha_hora_aperturado timestamp without time zone NOT NULL,
    monto_aperturado double precision NOT NULL,
    obs_aperturado text,
    id_usuario_cierre integer,
    fecha_hora_cierre timestamp without time zone,
    monto_cierre double precision,
    obs_cierre text,
    total_billetes double precision,
    total_monedas double precision,
    total_general double precision,
    total_otros_ingresos double precision,
    total_ingreso_ahorro double precision,
    total_cobrado double precision,
    total_ingresos double precision,
    total_sub_egresos double precision,
    total_retiros double precision,
    total_desembolsado double precision,
    total_egresos double precision,
    total_caja double precision,
    sobra_falta_caja double precision,
    cerrado_principal boolean DEFAULT false NOT NULL,
    id_agencia integer DEFAULT 1 NOT NULL
);


ALTER TABLE public.apertura_cierre OWNER TO postgres;

--
-- Name: apertura_cierre_id_caja_apertura_cierre_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.apertura_cierre_id_caja_apertura_cierre_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.apertura_cierre_id_caja_apertura_cierre_seq OWNER TO postgres;

--
-- Name: apertura_cierre_id_caja_apertura_cierre_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.apertura_cierre_id_caja_apertura_cierre_seq OWNED BY public.apertura_cierre.id_caja_apertura_cierre;


--
-- Name: aportaciones; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.aportaciones (
    id_aportaciones bigint NOT NULL,
    id_caja_apertura_cierre bigint NOT NULL,
    tipo_operacion character varying(15) NOT NULL,
    dni character(8) NOT NULL,
    fecha_hora timestamp without time zone NOT NULL,
    total_pagar real,
    agencia character varying(30),
    puntos bigint,
    id_agencia bigint
);


ALTER TABLE public.aportaciones OWNER TO postgres;

--
-- Name: aportaciones_id_aportaciones_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.aportaciones_id_aportaciones_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.aportaciones_id_aportaciones_seq OWNER TO postgres;

--
-- Name: aportaciones_id_aportaciones_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.aportaciones_id_aportaciones_seq OWNED BY public.aportaciones.id_aportaciones;


--
-- Name: banco; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.banco (
    id_banco bigint NOT NULL,
    entidad character varying(100),
    cuenta_nro character varying(50) NOT NULL,
    fecha_hora_operacion timestamp without time zone NOT NULL,
    usuario character varying(50) NOT NULL,
    obs text,
    monto_actual double precision
);


ALTER TABLE public.banco OWNER TO postgres;

--
-- Name: banco_id_banco_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.banco_id_banco_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.banco_id_banco_seq OWNER TO postgres;

--
-- Name: banco_id_banco_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.banco_id_banco_seq OWNED BY public.banco.id_banco;


--
-- Name: billeteo; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.billeteo (
    id_billeteo bigint NOT NULL,
    id_caja_apertura_cierre bigint NOT NULL,
    denominacion character varying(150) NOT NULL,
    cantidad integer NOT NULL,
    monto character(15) NOT NULL
);


ALTER TABLE public.billeteo OWNER TO postgres;

--
-- Name: billeteo_id_billeteo_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.billeteo_id_billeteo_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.billeteo_id_billeteo_seq OWNER TO postgres;

--
-- Name: billeteo_id_billeteo_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.billeteo_id_billeteo_seq OWNED BY public.billeteo.id_billeteo;


--
-- Name: boleta; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.boleta (
    id_boleta bigint NOT NULL,
    serie integer NOT NULL,
    inicio_boleta integer NOT NULL,
    id_agencia integer
);


ALTER TABLE public.boleta OWNER TO postgres;

--
-- Name: boleta_id_boleta_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.boleta_id_boleta_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.boleta_id_boleta_seq OWNER TO postgres;

--
-- Name: boleta_id_boleta_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.boleta_id_boleta_seq OWNED BY public.boleta.id_boleta;


--
-- Name: boveda; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.boveda (
    id_boveda bigint NOT NULL,
    monto_actual_boveda double precision,
    equipo_registrado character varying(50) NOT NULL,
    fecha_hora_operacion timestamp without time zone NOT NULL,
    tipo character varying(50) NOT NULL,
    monto_movimiento double precision,
    usuario character varying(50) NOT NULL,
    motivo character varying(50) NOT NULL,
    obs text,
    usuario_habilita character varying(100),
    cod_habiltacion bigint,
    confirmar boolean DEFAULT true NOT NULL,
    agencia character varying(50) NOT NULL,
    id_agencia bigint
);


ALTER TABLE public.boveda OWNER TO postgres;

--
-- Name: boveda_id_boveda_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.boveda_id_boveda_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.boveda_id_boveda_seq OWNER TO postgres;

--
-- Name: boveda_id_boveda_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.boveda_id_boveda_seq OWNED BY public.boveda.id_boveda;


--
-- Name: campana; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.campana (
    id_campana integer NOT NULL,
    name_campana character varying(50) NOT NULL,
    monto_campana real NOT NULL,
    inicio_capana timestamp without time zone NOT NULL,
    fin_capana timestamp without time zone NOT NULL,
    id_personal integer NOT NULL,
    obs text,
    id_agencia integer
);


ALTER TABLE public.campana OWNER TO postgres;

--
-- Name: campana_id_campana_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.campana_id_campana_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.campana_id_campana_seq OWNER TO postgres;

--
-- Name: campana_id_campana_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.campana_id_campana_seq OWNED BY public.campana.id_campana;


--
-- Name: central_riesgo; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.central_riesgo (
    id_central_riesgo bigint NOT NULL,
    dni character(8) NOT NULL,
    entidad character varying(80) NOT NULL,
    monto real,
    calificacion character varying(50),
    fecha date,
    id_usuario integer,
    id_agencia integer
);


ALTER TABLE public.central_riesgo OWNER TO postgres;

--
-- Name: central_riesgo_id_central_riesgo_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.central_riesgo_id_central_riesgo_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.central_riesgo_id_central_riesgo_seq OWNER TO postgres;

--
-- Name: central_riesgo_id_central_riesgo_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.central_riesgo_id_central_riesgo_seq OWNED BY public.central_riesgo.id_central_riesgo;


--
-- Name: cierre_caja_principal; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.cierre_caja_principal (
    id_cierre_principal bigint NOT NULL,
    id_usuario integer NOT NULL,
    fecha_cierre date,
    fecha_hora timestamp without time zone NOT NULL,
    obs text,
    monto_cierre real,
    id_agencia integer
);


ALTER TABLE public.cierre_caja_principal OWNER TO postgres;

--
-- Name: cierre_caja_principal_id_cierre_principal_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.cierre_caja_principal_id_cierre_principal_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cierre_caja_principal_id_cierre_principal_seq OWNER TO postgres;

--
-- Name: cierre_caja_principal_id_cierre_principal_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.cierre_caja_principal_id_cierre_principal_seq OWNED BY public.cierre_caja_principal.id_cierre_principal;


--
-- Name: cierre_planilla; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.cierre_planilla (
    id_cierre_planilla integer NOT NULL,
    monto_cierre real NOT NULL,
    fecha_hora_planilla timestamp without time zone,
    login character varying(250),
    agencia character varying(250),
    id_agencia integer
);


ALTER TABLE public.cierre_planilla OWNER TO postgres;

--
-- Name: cierre_planilla_id_cierre_planilla_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.cierre_planilla_id_cierre_planilla_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cierre_planilla_id_cierre_planilla_seq OWNER TO postgres;

--
-- Name: cierre_planilla_id_cierre_planilla_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.cierre_planilla_id_cierre_planilla_seq OWNED BY public.cierre_planilla.id_cierre_planilla;


--
-- Name: ciiu; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.ciiu (
    id_ciiu character varying NOT NULL,
    cod_ciiu character varying(4) NOT NULL,
    descripcion text
);


ALTER TABLE public.ciiu OWNER TO postgres;

--
-- Name: clave_cliente; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.clave_cliente (
    id_clave_cliente bigint NOT NULL,
    dni_cliente character(8) NOT NULL,
    clave character varying(50) NOT NULL
);


ALTER TABLE public.clave_cliente OWNER TO postgres;

--
-- Name: clave_cliente_id_clave_cliente_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.clave_cliente_id_clave_cliente_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.clave_cliente_id_clave_cliente_seq OWNER TO postgres;

--
-- Name: clave_cliente_id_clave_cliente_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.clave_cliente_id_clave_cliente_seq OWNED BY public.clave_cliente.id_clave_cliente;


--
-- Name: cliente; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.cliente (
    dni character(8) NOT NULL,
    paterno character varying(50) NOT NULL,
    materno character varying(50),
    nombres character varying(50) NOT NULL,
    sexo character(10) NOT NULL,
    estado_civil character varying(50) NOT NULL,
    fecha_nacimiento date,
    telefonos character varying(100),
    e_mail character varying(100),
    distrito character varying(50) NOT NULL,
    provincia character varying(50) NOT NULL,
    departamento character varying(50) NOT NULL,
    direccion character varying(120) NOT NULL,
    referencia character varying(120),
    obs text,
    fecha_hora_creacion timestamp without time zone NOT NULL,
    nro_oficina character(50),
    nro_expediente bigint,
    fecha_hora_expediente timestamp without time zone,
    id_analista integer,
    nombre_promotor character varying(100),
    id_plataforma integer,
    historial text,
    nro_ahorro bigint,
    calificacion character(20),
    pass_ahorro character(20),
    nro_expediente_dos bigint,
    estado_cliente character(1),
    usuario character(50),
    castigar_cliente character(5),
    motivo_castigo character(250),
    user_castigo character(50),
    fecha_hora_castigo timestamp without time zone,
    puntos smallint,
    codsocio bigint,
    aportado real,
    tiposocio character varying(50),
    id_registro_user integer,
    id_user_exp integer,
    id_agencia integer,
    fecha_hora_exp_ahorro time with time zone
);


ALTER TABLE public.cliente OWNER TO postgres;

--
-- Name: cliente_aval; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.cliente_aval (
    id_cliente_aval bigint NOT NULL,
    dni character(8) NOT NULL,
    aval character(8),
    parentesco character varying(20)
);


ALTER TABLE public.cliente_aval OWNER TO postgres;

--
-- Name: cliente_aval_id_cliente_aval_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.cliente_aval_id_cliente_aval_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cliente_aval_id_cliente_aval_seq OWNER TO postgres;

--
-- Name: cliente_aval_id_cliente_aval_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.cliente_aval_id_cliente_aval_seq OWNED BY public.cliente_aval.id_cliente_aval;


--
-- Name: cliente_beneficiario; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.cliente_beneficiario (
    id_cliente_beneficiario bigint NOT NULL,
    dni character(8) NOT NULL,
    beneficiario character(8),
    parantesco character varying(20),
    obs text,
    nombre_beni character varying(300)
);


ALTER TABLE public.cliente_beneficiario OWNER TO postgres;

--
-- Name: cliente_beneficiario_id_cliente_beneficiario_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.cliente_beneficiario_id_cliente_beneficiario_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cliente_beneficiario_id_cliente_beneficiario_seq OWNER TO postgres;

--
-- Name: cliente_beneficiario_id_cliente_beneficiario_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.cliente_beneficiario_id_cliente_beneficiario_seq OWNED BY public.cliente_beneficiario.id_cliente_beneficiario;


--
-- Name: compromiso; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.compromiso (
    id_compromiso bigint NOT NULL,
    id_solicitud bigint NOT NULL,
    tipo_compromiso character varying(15) NOT NULL,
    usuario character varying(20) NOT NULL,
    fecha_hora timestamp without time zone NOT NULL,
    fecha_compromiso timestamp without time zone NOT NULL,
    deuda_vencida real,
    id_notificaciones bigint,
    obs text
);


ALTER TABLE public.compromiso OWNER TO postgres;

--
-- Name: compromiso_id_compromiso_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.compromiso_id_compromiso_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.compromiso_id_compromiso_seq OWNER TO postgres;

--
-- Name: compromiso_id_compromiso_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.compromiso_id_compromiso_seq OWNED BY public.compromiso.id_compromiso;


--
-- Name: conyuge; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.conyuge (
    id_conyuge bigint NOT NULL,
    dni character(8) NOT NULL,
    conyuge character(8),
    estado integer DEFAULT 1 NOT NULL,
    obs text
);


ALTER TABLE public.conyuge OWNER TO postgres;

--
-- Name: conyuge_id_conyuge_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.conyuge_id_conyuge_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.conyuge_id_conyuge_seq OWNER TO postgres;

--
-- Name: conyuge_id_conyuge_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.conyuge_id_conyuge_seq OWNED BY public.conyuge.id_conyuge;


--
-- Name: credito_refinanciado; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.credito_refinanciado (
    id_refinanciado bigint NOT NULL,
    id_solicitud integer NOT NULL,
    nro_de_creditos character varying(100),
    detalle_dreditos text,
    fecha_hora_refinanciado timestamp without time zone,
    obs text
);


ALTER TABLE public.credito_refinanciado OWNER TO postgres;

--
-- Name: credito_refinanciado_id_refinanciado_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.credito_refinanciado_id_refinanciado_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.credito_refinanciado_id_refinanciado_seq OWNER TO postgres;

--
-- Name: credito_refinanciado_id_refinanciado_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.credito_refinanciado_id_refinanciado_seq OWNED BY public.credito_refinanciado.id_refinanciado;


--
-- Name: cronograma_ahorro; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.cronograma_ahorro (
    id_cronograma_ahorro bigint NOT NULL,
    id_libreta_ahorro bigint NOT NULL,
    pagado boolean DEFAULT false NOT NULL,
    monto_pactado double precision NOT NULL,
    monto_descontado boolean DEFAULT false NOT NULL,
    nro_cuota integer NOT NULL,
    fecha_pactada date NOT NULL,
    fecha_hora_pagado timestamp without time zone,
    monto_movimiento double precision,
    fecha_hora_amortizacion timestamp without time zone,
    amortizado double precision
);


ALTER TABLE public.cronograma_ahorro OWNER TO postgres;

--
-- Name: cronograma_ahorro_id_cronograma_ahorro_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.cronograma_ahorro_id_cronograma_ahorro_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cronograma_ahorro_id_cronograma_ahorro_seq OWNER TO postgres;

--
-- Name: cronograma_ahorro_id_cronograma_ahorro_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.cronograma_ahorro_id_cronograma_ahorro_seq OWNED BY public.cronograma_ahorro.id_cronograma_ahorro;


--
-- Name: cronograma_pagos; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.cronograma_pagos (
    id_cronograma_pago bigint NOT NULL,
    id_solicitud bigint NOT NULL,
    pagado boolean DEFAULT false NOT NULL,
    capital_x_cuota double precision NOT NULL,
    interes_x_cuota double precision NOT NULL,
    redondeo_x_cuota double precision NOT NULL,
    penalidad_x_cuota double precision NOT NULL,
    cuota_pagar double precision NOT NULL,
    ii double precision NOT NULL,
    ga double precision NOT NULL,
    cuota_descontada boolean DEFAULT false NOT NULL,
    nro_cuota integer NOT NULL,
    fecha_vencimiento date NOT NULL,
    fecha_hora_pagado timestamp without time zone,
    monto_capital double precision,
    monto_interes double precision,
    monto_redondeo double precision,
    mora double precision,
    otros_pagos double precision,
    notificacion double precision,
    fecha_hora_amortizacion timestamp without time zone,
    amortizado double precision,
    pago_con double precision,
    id_operacion bigint
);


ALTER TABLE public.cronograma_pagos OWNER TO postgres;

--
-- Name: cronograma_pagos_id_cronograma_pago_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.cronograma_pagos_id_cronograma_pago_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cronograma_pagos_id_cronograma_pago_seq OWNER TO postgres;

--
-- Name: cronograma_pagos_id_cronograma_pago_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.cronograma_pagos_id_cronograma_pago_seq OWNED BY public.cronograma_pagos.id_cronograma_pago;


--
-- Name: departamento; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.departamento (
    id_departamento integer NOT NULL,
    departamento character varying(250) NOT NULL
);


ALTER TABLE public.departamento OWNER TO postgres;

--
-- Name: departamento_id_departamento_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.departamento_id_departamento_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.departamento_id_departamento_seq OWNER TO postgres;

--
-- Name: departamento_id_departamento_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.departamento_id_departamento_seq OWNED BY public.departamento.id_departamento;


--
-- Name: detalle_negocio; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.detalle_negocio (
    id_detalle_negocio bigint NOT NULL,
    id_negocio bigint NOT NULL,
    tipo_negocio character varying(50),
    cantidad real,
    descripcion character varying(50),
    frecuencia_mes real,
    precio_compra real,
    precio_venta real,
    total_compra real,
    total_venta real,
    usuario character varying(50),
    agencia character varying(50),
    fecha_hora timestamp without time zone,
    estado character(1)
);


ALTER TABLE public.detalle_negocio OWNER TO postgres;

--
-- Name: detalle_negocio_id_detalle_negocio_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.detalle_negocio_id_detalle_negocio_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.detalle_negocio_id_detalle_negocio_seq OWNER TO postgres;

--
-- Name: detalle_negocio_id_detalle_negocio_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.detalle_negocio_id_detalle_negocio_seq OWNED BY public.detalle_negocio.id_detalle_negocio;


--
-- Name: detalle_negocio_id_negocio_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.detalle_negocio_id_negocio_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.detalle_negocio_id_negocio_seq OWNER TO postgres;

--
-- Name: detalle_negocio_id_negocio_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.detalle_negocio_id_negocio_seq OWNED BY public.detalle_negocio.id_negocio;


--
-- Name: dias_festivos; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.dias_festivos (
    id_dia_festivo integer NOT NULL,
    dia integer NOT NULL,
    mes integer NOT NULL,
    anio integer NOT NULL,
    titulo character varying(50),
    id_agencia integer NOT NULL
);


ALTER TABLE public.dias_festivos OWNER TO postgres;

--
-- Name: dias_festivos_id_agencia_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.dias_festivos_id_agencia_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.dias_festivos_id_agencia_seq OWNER TO postgres;

--
-- Name: dias_festivos_id_agencia_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.dias_festivos_id_agencia_seq OWNED BY public.dias_festivos.id_agencia;


--
-- Name: dias_festivos_id_dia_festivo_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.dias_festivos_id_dia_festivo_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.dias_festivos_id_dia_festivo_seq OWNER TO postgres;

--
-- Name: dias_festivos_id_dia_festivo_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.dias_festivos_id_dia_festivo_seq OWNED BY public.dias_festivos.id_dia_festivo;


--
-- Name: distrito; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.distrito (
    id_distrito integer NOT NULL,
    id_provincia integer NOT NULL,
    distrito character varying(50) NOT NULL
);


ALTER TABLE public.distrito OWNER TO postgres;

--
-- Name: distrito_id_distrito_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.distrito_id_distrito_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.distrito_id_distrito_seq OWNER TO postgres;

--
-- Name: distrito_id_distrito_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.distrito_id_distrito_seq OWNED BY public.distrito.id_distrito;


--
-- Name: distrito_id_provincia_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.distrito_id_provincia_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.distrito_id_provincia_seq OWNER TO postgres;

--
-- Name: distrito_id_provincia_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.distrito_id_provincia_seq OWNED BY public.distrito.id_provincia;


--
-- Name: empresa; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.empresa (
    id_empresa integer NOT NULL,
    nombre_empresa character varying(250) NOT NULL,
    codigo_empresa bigint,
    detalles_empresa character varying(250),
    nombre_corto character varying(250)
);


ALTER TABLE public.empresa OWNER TO postgres;

--
-- Name: empresa_id_empresa_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.empresa_id_empresa_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.empresa_id_empresa_seq OWNER TO postgres;

--
-- Name: empresa_id_empresa_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.empresa_id_empresa_seq OWNED BY public.empresa.id_empresa;


--
-- Name: equipos; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.equipos (
    id_aquipos integer NOT NULL,
    id_agencia integer NOT NULL,
    nombre_equipo character varying(50) NOT NULL,
    area character varying(50)
);


ALTER TABLE public.equipos OWNER TO postgres;

--
-- Name: equipos_id_agencia_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.equipos_id_agencia_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.equipos_id_agencia_seq OWNER TO postgres;

--
-- Name: equipos_id_agencia_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.equipos_id_agencia_seq OWNED BY public.equipos.id_agencia;


--
-- Name: equipos_id_aquipos_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.equipos_id_aquipos_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.equipos_id_aquipos_seq OWNER TO postgres;

--
-- Name: equipos_id_aquipos_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.equipos_id_aquipos_seq OWNED BY public.equipos.id_aquipos;


--
-- Name: ficha_domiciliaria; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.ficha_domiciliaria (
    id_ficha_domiciliaria bigint NOT NULL,
    id_analista bigint,
    id_solicitud bigint,
    agencia character varying(50),
    dni_cliente character(8) NOT NULL,
    zonificacion character varying(50),
    t_inmueble character varying(50),
    t_edificacion character varying(50),
    t_zona character varying(50),
    t_construccion character varying(50),
    est_inmueble character varying(50),
    zona character varying(50),
    serv_basicos character varying(50),
    grado_peligro character varying(50),
    num_depend character varying(50),
    name_conyuge character varying(150),
    nivel_instruccion character varying(50),
    acupacion character varying(50),
    direccion_correcta character varying(150),
    jardin character varying(50),
    cochera character varying(50),
    pisos_nro character varying(50),
    acabados character varying(50),
    conservacion character varying(50),
    seguridad character varying(50),
    ubicacion character varying(50),
    vigilancia character varying(50),
    pista character varying(50),
    vereda character varying(50),
    atendio character varying(50),
    residencia character varying(50),
    condicion character varying(50),
    t_residencia character varying(50),
    estado_civil character varying(50),
    habita character varying(50),
    medio_comunicacion character varying(50),
    name_informante character varying(150),
    documento_dni character varying(50),
    parentesco character varying(50),
    telefono_infor character varying(50),
    cerco character varying(150),
    fachada character varying(150),
    paredes character varying(150),
    techo character varying(150),
    piso character varying(150),
    puertas character varying(150),
    ventana character varying(150),
    suministro character varying(50),
    coment_primera_v character varying(250),
    coment_segunda_v character varying(250),
    nombre_vecino character varying(150),
    obs_vecino character varying(150),
    estado_formulario character varying(50),
    fecha_hora_ficha character varying(50),
    fecha_hora_modi character varying(50),
    id_agencia integer,
    obs text
);


ALTER TABLE public.ficha_domiciliaria OWNER TO postgres;

--
-- Name: ficha_domiciliaria_id_ficha_domiciliaria_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.ficha_domiciliaria_id_ficha_domiciliaria_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ficha_domiciliaria_id_ficha_domiciliaria_seq OWNER TO postgres;

--
-- Name: ficha_domiciliaria_id_ficha_domiciliaria_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.ficha_domiciliaria_id_ficha_domiciliaria_seq OWNED BY public.ficha_domiciliaria.id_ficha_domiciliaria;


--
-- Name: ficha_economica; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.ficha_economica (
    id_ficha bigint NOT NULL,
    dni_cliente character(8) NOT NULL,
    ingreso_uno double precision,
    ingreso_dos double precision,
    ingreso_tres double precision,
    carga_familar double precision,
    gastos_alimentacion double precision,
    gastos_estudio double precision,
    servicios_familiar double precision,
    otros_familiar double precision,
    capital_inicial double precision,
    ventas_negocio double precision,
    otro_venta_negocio double precision,
    personal_negocio double precision,
    alquiler_negocio double precision,
    servicios_negocio double precision,
    otros_negocio_salida double precision,
    resultado_economico double precision,
    fecha_hora_ficha timestamp without time zone,
    monto_aprobado double precision,
    fecha_hora_modificacion timestamp without time zone,
    obs text,
    estado_ficha boolean DEFAULT false NOT NULL,
    monto_inicial double precision,
    cambio_monto double precision,
    incremento double precision,
    fecha_limite date,
    sw character(1),
    agencia character varying(250),
    login character varying(15),
    obs_mod text
);


ALTER TABLE public.ficha_economica OWNER TO postgres;

--
-- Name: ficha_economica_id_ficha_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.ficha_economica_id_ficha_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ficha_economica_id_ficha_seq OWNER TO postgres;

--
-- Name: ficha_economica_id_ficha_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.ficha_economica_id_ficha_seq OWNED BY public.ficha_economica.id_ficha;


--
-- Name: fotos; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.fotos (
    codigo integer NOT NULL,
    dni character(8),
    tipo character varying(50),
    descripcion character varying(100),
    imagen bytea,
    fecha date,
    agencia character varying(250)
);


ALTER TABLE public.fotos OWNER TO postgres;

--
-- Name: gastos_operativos; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.gastos_operativos (
    id_gastos_operativos bigint NOT NULL,
    id_negocio bigint NOT NULL,
    descripcion character varying(50),
    monto real,
    usuario character varying(50),
    agencia character varying(50),
    fecha_hora timestamp without time zone,
    estado character(1)
);


ALTER TABLE public.gastos_operativos OWNER TO postgres;

--
-- Name: gastos_operativos_id_gastos_operativos_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.gastos_operativos_id_gastos_operativos_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.gastos_operativos_id_gastos_operativos_seq OWNER TO postgres;

--
-- Name: gastos_operativos_id_gastos_operativos_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.gastos_operativos_id_gastos_operativos_seq OWNED BY public.gastos_operativos.id_gastos_operativos;


--
-- Name: gastos_operativos_id_negocio_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.gastos_operativos_id_negocio_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.gastos_operativos_id_negocio_seq OWNER TO postgres;

--
-- Name: gastos_operativos_id_negocio_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.gastos_operativos_id_negocio_seq OWNED BY public.gastos_operativos.id_negocio;


--
-- Name: grantia_real; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.grantia_real (
    id_grantia_real bigint NOT NULL,
    dni character(8) NOT NULL,
    bien_real character varying(50) NOT NULL,
    caracteristicas character varying(820),
    comprobante character varying(50) NOT NULL,
    serie character varying(50) NOT NULL,
    estado character varying(50) NOT NULL,
    valor_compra real,
    valor_residual real,
    antiguedad real,
    expectativa real,
    id_usuario integer,
    fecha_hora timestamp without time zone NOT NULL,
    agencia character(30),
    valor_real real,
    credito real,
    valor_vendido real,
    almacen real,
    id_usuario_venta integer,
    fecha_venta character varying(50)
);


ALTER TABLE public.grantia_real OWNER TO postgres;

--
-- Name: historial_manipulacion; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.historial_manipulacion (
    id_historial_manipulacion bigint NOT NULL,
    id_usuario integer NOT NULL,
    operacion character varying(100) NOT NULL,
    time_ingreso timestamp without time zone,
    maquina character varying(100),
    agencia character varying(100)
);


ALTER TABLE public.historial_manipulacion OWNER TO postgres;

--
-- Name: historial_manipulacion_id_historial_manipulacion_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.historial_manipulacion_id_historial_manipulacion_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.historial_manipulacion_id_historial_manipulacion_seq OWNER TO postgres;

--
-- Name: historial_manipulacion_id_historial_manipulacion_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.historial_manipulacion_id_historial_manipulacion_seq OWNED BY public.historial_manipulacion.id_historial_manipulacion;


--
-- Name: historial_manipulacion_id_usuario_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.historial_manipulacion_id_usuario_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.historial_manipulacion_id_usuario_seq OWNER TO postgres;

--
-- Name: historial_manipulacion_id_usuario_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.historial_manipulacion_id_usuario_seq OWNED BY public.historial_manipulacion.id_usuario;


--
-- Name: historial_visitas; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.historial_visitas (
    id_historial_visitas bigint NOT NULL,
    dni character(8) NOT NULL,
    tipo_cliente character varying(250),
    nombre_completo character varying(250),
    fecha_hora_visita timestamp without time zone,
    operacion_realizar character varying(250) NOT NULL,
    id_usuario integer NOT NULL,
    credito character(20),
    ahorro character(20),
    detalle_operacion text,
    sacar_cita boolean DEFAULT false NOT NULL,
    cita_analista integer,
    resultado_cita text,
    fecha_cita_realizada timestamp with time zone,
    fecha_acitar timestamp with time zone,
    blokear boolean,
    agencia character varying(250),
    id_agencia integer
);


ALTER TABLE public.historial_visitas OWNER TO postgres;

--
-- Name: historial_visitas_id_historial_visitas_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.historial_visitas_id_historial_visitas_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.historial_visitas_id_historial_visitas_seq OWNER TO postgres;

--
-- Name: historial_visitas_id_historial_visitas_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.historial_visitas_id_historial_visitas_seq OWNED BY public.historial_visitas.id_historial_visitas;


--
-- Name: ingreso_egreso; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.ingreso_egreso (
    id_ingreso_egreso bigint NOT NULL,
    id_caja_apertura_cierre bigint,
    tipo smallint,
    monto double precision,
    solicitante character varying(200),
    concepto character varying(200),
    confirmar boolean DEFAULT true NOT NULL,
    sustento character varying(200),
    fecha_hora_operacion timestamp without time zone,
    agencia character varying(250),
    id_agencia integer,
    tipo_operacion integer DEFAULT 0 NOT NULL,
    numero_documento character varying(250)
);


ALTER TABLE public.ingreso_egreso OWNER TO postgres;

--
-- Name: ingreso_egreso_id_ingreso_egreso_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.ingreso_egreso_id_ingreso_egreso_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ingreso_egreso_id_ingreso_egreso_seq OWNER TO postgres;

--
-- Name: ingreso_egreso_id_ingreso_egreso_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.ingreso_egreso_id_ingreso_egreso_seq OWNED BY public.ingreso_egreso.id_ingreso_egreso;


--
-- Name: insumo_negocio; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.insumo_negocio (
    id_insumo_negocio bigint NOT NULL,
    id_detalle_negocio bigint NOT NULL,
    cantidad real,
    descripcion character varying(50),
    precio real,
    total real,
    usuario character varying(50),
    agencia character varying(50),
    fecha_hora timestamp without time zone,
    estado character(1)
);


ALTER TABLE public.insumo_negocio OWNER TO postgres;

--
-- Name: insumo_negocio_id_detalle_negocio_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.insumo_negocio_id_detalle_negocio_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.insumo_negocio_id_detalle_negocio_seq OWNER TO postgres;

--
-- Name: insumo_negocio_id_detalle_negocio_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.insumo_negocio_id_detalle_negocio_seq OWNED BY public.insumo_negocio.id_detalle_negocio;


--
-- Name: insumo_negocio_id_insumo_negocio_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.insumo_negocio_id_insumo_negocio_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.insumo_negocio_id_insumo_negocio_seq OWNER TO postgres;

--
-- Name: insumo_negocio_id_insumo_negocio_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.insumo_negocio_id_insumo_negocio_seq OWNED BY public.insumo_negocio.id_insumo_negocio;


--
-- Name: inventario_bienes; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.inventario_bienes (
    id_inventario_bienes bigint NOT NULL,
    dni character(8) NOT NULL,
    bien character varying(250) NOT NULL,
    caracteristica character varying(350) NOT NULL,
    valorizado double precision,
    usuario character varying(70) NOT NULL,
    fecha_hora_ingreso text,
    fecha_hora_manipulacion text
);


ALTER TABLE public.inventario_bienes OWNER TO postgres;

--
-- Name: inventario_bienes_id_inventario_bienes_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.inventario_bienes_id_inventario_bienes_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.inventario_bienes_id_inventario_bienes_seq OWNER TO postgres;

--
-- Name: inventario_bienes_id_inventario_bienes_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.inventario_bienes_id_inventario_bienes_seq OWNED BY public.inventario_bienes.id_inventario_bienes;


--
-- Name: libreta_ahorro; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.libreta_ahorro (
    id_libreta_ahorro bigint NOT NULL,
    dni character(8) NOT NULL,
    nombre_oficina character varying(50) NOT NULL,
    id_cajera integer,
    id_administrador real,
    id_benificiario bigint,
    id_tipo_ahorro bigint,
    fecha_apertura date NOT NULL,
    monto_apertura double precision NOT NULL,
    monto_actual double precision NOT NULL,
    interes_actual double precision,
    tiempo_plazo real,
    monto_meta double precision,
    bloquear boolean NOT NULL,
    obs text,
    fecha_a_retirar date,
    monto_financiado double precision,
    fecha_retiro date,
    id_cajera_retiro integer,
    tea double precision,
    interes_generado double precision,
    monto_retirado double precision,
    ultimo_movimiento date,
    monto_pactado double precision,
    dni_benificiario character(8),
    tipo_campana character(3),
    fin_campana date,
    producto text,
    usuario_ope character varying(50),
    id_analista integer,
    usuario_analista character varying(50),
    estado integer,
    id_agencia integer,
    periodo character varying(250),
    p_retiro integer
);


ALTER TABLE public.libreta_ahorro OWNER TO postgres;

--
-- Name: libreta_ahorro_id_libreta_ahorro_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.libreta_ahorro_id_libreta_ahorro_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.libreta_ahorro_id_libreta_ahorro_seq OWNER TO postgres;

--
-- Name: libreta_ahorro_id_libreta_ahorro_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.libreta_ahorro_id_libreta_ahorro_seq OWNED BY public.libreta_ahorro.id_libreta_ahorro;


--
-- Name: metas_personal; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.metas_personal (
    id_metas_personal bigint NOT NULL,
    fecha_hora timestamp without time zone,
    usuario character(50),
    nro_creditos double precision,
    nro_clientes double precision,
    monto_otrogado double precision,
    interes_otrogado double precision,
    redondeo_otrogado double precision,
    monto_saldo double precision,
    interes_saldo double precision,
    redondeo_saldo double precision,
    monto_cobrado double precision,
    interes_cobrado double precision,
    redondeo_cobrado double precision,
    monto_vencido double precision,
    porcentaje double precision,
    mora double precision,
    noti double precision,
    otros double precision,
    nro_creditos_fin double precision,
    nro_clientes_fin double precision,
    monto_otrogado_fin double precision,
    interes_otrogado_fin double precision,
    redondeo_otrogado_fin double precision,
    monto_saldo_fin double precision,
    interes_saldo_fin double precision,
    redondeo_saldo_fin double precision,
    monto_cobrado_fin double precision,
    interes_cobrado_fin double precision,
    redondeo_cobrado_fin double precision,
    monto_vencido_fin double precision,
    porcentaje_fin double precision,
    mora_cobrado double precision,
    noti_cobrado double precision,
    otros_cobrado double precision,
    colacar_meta double precision,
    saldo_meta double precision,
    monto_vencido_meta double precision,
    porcentaje_meta double precision,
    creditos_meta double precision,
    clientes_meta double precision,
    cumplimiento double precision,
    fecha_hora_cierre timestamp without time zone,
    id_usuario bigint,
    id_agencia integer
);


ALTER TABLE public.metas_personal OWNER TO postgres;

--
-- Name: metas_personal_id_metas_personal_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.metas_personal_id_metas_personal_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.metas_personal_id_metas_personal_seq OWNER TO postgres;

--
-- Name: metas_personal_id_metas_personal_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.metas_personal_id_metas_personal_seq OWNED BY public.metas_personal.id_metas_personal;


--
-- Name: movimiento; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.movimiento (
    id_movimiento bigint NOT NULL,
    id_libreta_ahorro bigint NOT NULL,
    id_cajera bigint NOT NULL,
    fecha_hora_movimiento timestamp without time zone,
    tipo_operacion character(10) NOT NULL,
    monto_movimiento double precision NOT NULL,
    interes_movimiento double precision,
    redondeo double precision,
    obs text,
    id_agencia bigint,
    agencia character(30),
    puntos bigint,
    id_caja_apertura_cierre bigint,
    cajera character(20),
    estado integer DEFAULT 1 NOT NULL,
    obs_cancelacion text,
    concepto character varying(100)
);


ALTER TABLE public.movimiento OWNER TO postgres;

--
-- Name: movimiento_id_movimiento_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.movimiento_id_movimiento_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.movimiento_id_movimiento_seq OWNER TO postgres;

--
-- Name: movimiento_id_movimiento_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.movimiento_id_movimiento_seq OWNED BY public.movimiento.id_movimiento;


--
-- Name: movimiento_usuario; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.movimiento_usuario (
    id_movimiento_usuario bigint NOT NULL,
    id_usuario bigint NOT NULL,
    id_cajera bigint NOT NULL,
    fecha_hora_movimiento timestamp without time zone,
    operacion character(20) NOT NULL,
    monto_operacion double precision NOT NULL,
    sustento text,
    agencia character varying(250)
);


ALTER TABLE public.movimiento_usuario OWNER TO postgres;

--
-- Name: movimiento_usuario_id_movimiento_usuario_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.movimiento_usuario_id_movimiento_usuario_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.movimiento_usuario_id_movimiento_usuario_seq OWNER TO postgres;

--
-- Name: movimiento_usuario_id_movimiento_usuario_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.movimiento_usuario_id_movimiento_usuario_seq OWNED BY public.movimiento_usuario.id_movimiento_usuario;


--
-- Name: negocio; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.negocio (
    id_negocio bigint NOT NULL,
    dni character(8) NOT NULL,
    nombre_actividad character varying(50) NOT NULL,
    ciiu character varying(120),
    distrito character varying(50) NOT NULL,
    provincia character varying(50) NOT NULL,
    departamento character varying(50) NOT NULL,
    direccion character varying(100) NOT NULL,
    referencia text NOT NULL,
    valorizacion real,
    obs text,
    estado_negocio character(1),
    id_agencia integer,
    obs_modificacion text
);


ALTER TABLE public.negocio OWNER TO postgres;

--
-- Name: negocio_id_negocio_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.negocio_id_negocio_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.negocio_id_negocio_seq OWNER TO postgres;

--
-- Name: negocio_id_negocio_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.negocio_id_negocio_seq OWNED BY public.negocio.id_negocio;


--
-- Name: nivel_usuario; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.nivel_usuario (
    id_nivel_usuario integer NOT NULL,
    funcion character varying(50),
    funciones text
);


ALTER TABLE public.nivel_usuario OWNER TO postgres;

--
-- Name: nivel_usuario_id_nivel_usuario_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.nivel_usuario_id_nivel_usuario_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.nivel_usuario_id_nivel_usuario_seq OWNER TO postgres;

--
-- Name: nivel_usuario_id_nivel_usuario_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.nivel_usuario_id_nivel_usuario_seq OWNED BY public.nivel_usuario.id_nivel_usuario;


--
-- Name: notificaciones; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.notificaciones (
    id_notificaciones bigint NOT NULL,
    id_solicitud bigint NOT NULL,
    monto_vencido double precision NOT NULL,
    dias_retraso bigint NOT NULL,
    id_analista bigint NOT NULL,
    id_tipo_notifi bigint NOT NULL,
    fecha_gestion date NOT NULL,
    costo double precision NOT NULL,
    accion character varying(120),
    fecha_accion timestamp without time zone,
    respuesta character varying(520),
    observacion character varying(520),
    usuario bigint,
    telefono character varying(520),
    tipo_direccion character varying(120),
    direcccion character varying(520),
    dni character(8),
    id_agencia integer
);


ALTER TABLE public.notificaciones OWNER TO postgres;

--
-- Name: notificaciones_id_notificaciones_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.notificaciones_id_notificaciones_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.notificaciones_id_notificaciones_seq OWNER TO postgres;

--
-- Name: notificaciones_id_notificaciones_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.notificaciones_id_notificaciones_seq OWNED BY public.notificaciones.id_notificaciones;


--
-- Name: operacion; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.operacion (
    id_operacion bigint NOT NULL,
    id_operacion_pago bigint NOT NULL,
    id_solicitud bigint NOT NULL,
    id_cajera integer,
    id_impresion bigint NOT NULL,
    fecha_hora timestamp without time zone NOT NULL,
    concepto character varying(80) NOT NULL,
    monto real NOT NULL,
    de_cuota integer,
    a_cuota integer,
    id_analista integer,
    id_agencia integer,
    capital real,
    interes real,
    redondeo real
);


ALTER TABLE public.operacion OWNER TO postgres;

--
-- Name: operacion_banco; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.operacion_banco (
    id_operacion_banco bigint NOT NULL,
    id_banco bigint NOT NULL,
    operacion character varying(80) NOT NULL,
    monto real NOT NULL,
    fecha_hora timestamp without time zone NOT NULL,
    usuario character varying(80) NOT NULL,
    agencia character varying(80) NOT NULL,
    cod_apertura integer,
    obs character varying(500) NOT NULL
);


ALTER TABLE public.operacion_banco OWNER TO postgres;

--
-- Name: operacion_banco_id_operacion_banco_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.operacion_banco_id_operacion_banco_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.operacion_banco_id_operacion_banco_seq OWNER TO postgres;

--
-- Name: operacion_banco_id_operacion_banco_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.operacion_banco_id_operacion_banco_seq OWNED BY public.operacion_banco.id_operacion_banco;


--
-- Name: operacion_id_operacion_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.operacion_id_operacion_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.operacion_id_operacion_seq OWNER TO postgres;

--
-- Name: operacion_id_operacion_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.operacion_id_operacion_seq OWNED BY public.operacion.id_operacion;


--
-- Name: operacion_pago; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.operacion_pago (
    id_operacion_pago bigint NOT NULL,
    id_caja_apertura_cierre bigint NOT NULL,
    fecha_hora timestamp without time zone NOT NULL,
    total_pagar real,
    pago_con real,
    vuelto real,
    agencia character(30),
    puntos bigint,
    id_agencia bigint,
    obs_cancelacion text
);


ALTER TABLE public.operacion_pago OWNER TO postgres;

--
-- Name: operacion_pago_id_operacion_pago_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.operacion_pago_id_operacion_pago_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.operacion_pago_id_operacion_pago_seq OWNER TO postgres;

--
-- Name: operacion_pago_id_operacion_pago_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.operacion_pago_id_operacion_pago_seq OWNED BY public.operacion_pago.id_operacion_pago;


--
-- Name: pariente; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.pariente (
    id_pariente integer NOT NULL,
    dniuno character(8),
    nombreuno character varying(50),
    parentescouno character varying(20),
    dnidos character(8),
    nombredos character varying(50),
    parentescodos character varying(20),
    estado character varying(20)
);


ALTER TABLE public.pariente OWNER TO postgres;

--
-- Name: pariente_id_pariente_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.pariente_id_pariente_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.pariente_id_pariente_seq OWNER TO postgres;

--
-- Name: pariente_id_pariente_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.pariente_id_pariente_seq OWNED BY public.pariente.id_pariente;


--
-- Name: planilla; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.planilla (
    id_planilla bigint NOT NULL,
    cod_generacion integer NOT NULL,
    cod_aprobacion integer,
    cod_planilla integer,
    fecha_hora timestamp without time zone NOT NULL,
    personal character varying(50) NOT NULL,
    cargo character varying(50) NOT NULL,
    dni character(8),
    sueldo real NOT NULL,
    incentivos double precision,
    bonificacion double precision,
    faltantes double precision,
    movil double precision,
    tardanza double precision,
    adelantos double precision,
    uniforme double precision,
    permisos double precision,
    sanciones double precision,
    otros double precision,
    aporte_afp double precision,
    aporte_onp double precision,
    desc_total double precision,
    neto_pagar double precision,
    estado boolean,
    oficina character varying(50) NOT NULL,
    fecha_hora_ejecutado time with time zone
);


ALTER TABLE public.planilla OWNER TO postgres;

--
-- Name: planilla_id_planilla_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.planilla_id_planilla_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.planilla_id_planilla_seq OWNER TO postgres;

--
-- Name: planilla_id_planilla_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.planilla_id_planilla_seq OWNED BY public.planilla.id_planilla;


--
-- Name: provincia; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.provincia (
    id_provincia integer NOT NULL,
    id_departamento integer NOT NULL,
    provincia character varying(50) NOT NULL
);


ALTER TABLE public.provincia OWNER TO postgres;

--
-- Name: provincia_id_departamento_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.provincia_id_departamento_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.provincia_id_departamento_seq OWNER TO postgres;

--
-- Name: provincia_id_departamento_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.provincia_id_departamento_seq OWNED BY public.provincia.id_departamento;


--
-- Name: provincia_id_provincia_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.provincia_id_provincia_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.provincia_id_provincia_seq OWNER TO postgres;

--
-- Name: provincia_id_provincia_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.provincia_id_provincia_seq OWNED BY public.provincia.id_provincia;


--
-- Name: rpt_cobranza_diaria; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.rpt_cobranza_diaria (
    id_rpt_cobranza_diaria bigint NOT NULL,
    fecha_hora timestamp without time zone,
    id_apertura integer NOT NULL,
    nombre_user character varying(120) NOT NULL,
    monto_inicio_total double precision,
    monto_inicio_hoy double precision,
    monto_fin_total double precision,
    interes_fin_hoy double precision,
    creditos_total double precision,
    creditos_hoy double precision,
    id_agencia integer
);


ALTER TABLE public.rpt_cobranza_diaria OWNER TO postgres;

--
-- Name: rpt_creditos_colocados_historial; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.rpt_creditos_colocados_historial (
    id_rpt_creditos_colocados_historial bigint NOT NULL,
    fecha_hora timestamp without time zone,
    id_analista integer NOT NULL,
    nombre_analista character varying(120) NOT NULL,
    nro_creditos double precision,
    nro_clientes double precision,
    monto_otrogado double precision,
    interes_otrogado double precision,
    redondeo_otrogado double precision,
    monto_saldo double precision,
    interes_saldo double precision,
    redondeo_saldo double precision,
    monto_cobrado double precision,
    interes_cobrado double precision,
    redondeo_cobrado double precision,
    diario_1_7 double precision,
    diario_8_30 double precision,
    diario_mas_30 double precision,
    total_diario double precision,
    semanal_1_7 double precision,
    semanal_8_30 double precision,
    semanal_mas_30 double precision,
    total_semanal double precision,
    total_general double precision,
    id_agencia integer
);


ALTER TABLE public.rpt_creditos_colocados_historial OWNER TO postgres;

--
-- Name: rpt_movimientos_historial; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.rpt_movimientos_historial (
    id_rpt_movimientos_historial bigint NOT NULL,
    fecha_hora timestamp without time zone,
    nro_creditos double precision,
    nro_clientes double precision,
    monto_otrogado double precision,
    interes_otrogado double precision,
    redondeo_otrogado double precision,
    monto_saldo double precision,
    interes_saldo double precision,
    redondeo_saldo double precision,
    monto_cobrado double precision,
    interes_cobrado double precision,
    redondeo_cobrado double precision,
    monto_vencido double precision,
    porcentaje double precision,
    mora_generadas double precision,
    mora_pagada double precision,
    mora_saldo double precision,
    noti_generadas double precision,
    noti_pagada double precision,
    noti_saldo double precision,
    otros_generadas double precision,
    otros_pagada double precision,
    otros_saldo double precision,
    id_agencia integer
);


ALTER TABLE public.rpt_movimientos_historial OWNER TO postgres;

--
-- Name: sectores; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sectores (
    id_sector integer NOT NULL,
    sector character varying(50) NOT NULL,
    nombre character varying(50) NOT NULL,
    id_analista integer NOT NULL,
    obs text
);


ALTER TABLE public.sectores OWNER TO postgres;

--
-- Name: sectores_id_sector_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.sectores_id_sector_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sectores_id_sector_seq OWNER TO postgres;

--
-- Name: sectores_id_sector_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.sectores_id_sector_seq OWNED BY public.sectores.id_sector;


--
-- Name: solicitud_credito; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.solicitud_credito (
    id_solicitud bigint NOT NULL,
    nombre_analista character varying(50) NOT NULL,
    id_analista integer NOT NULL,
    id_administrador integer,
    fecha_hora_solicitud timestamp without time zone NOT NULL,
    tipo_credito character varying(70) NOT NULL,
    producto_credito character varying(70) NOT NULL,
    dni character(8) NOT NULL,
    id_conyuge bigint,
    tipo_garantia_cliente character varying(70) NOT NULL,
    descripcion_garantia_cliente character varying(120),
    valorizacion_garantia_cliente real,
    id_cliente_aval bigint,
    tipo_garantia_aval character varying(70) NOT NULL,
    descripcion_garantia_aval character varying(120),
    valorizacion_garantia_aval real,
    id_negocio bigint,
    interes real,
    monto_interes double precision,
    periodo smallint NOT NULL,
    meses smallint NOT NULL,
    penalidad double precision,
    fecha_inicio_pago date NOT NULL,
    monto_prestado double precision NOT NULL,
    nro_cuotas integer NOT NULL,
    redondeo_total double precision,
    fecha_finalizacion date NOT NULL,
    obs text,
    obs_internas text,
    estado smallint NOT NULL,
    nro_credito character(16),
    con_boleta boolean,
    nro_boleta character(15),
    fecha_hora_desembolso timestamp without time zone,
    id_cajera integer,
    estado_credito smallint,
    capital_x_cuota double precision,
    interes_x_cuota double precision,
    redondeo_x_cuota double precision,
    cuota_a_pagar double precision,
    capital_x_pagar double precision,
    interes_x_pagar double precision,
    redondeo_x_pagar double precision,
    penalidad_x_pagar double precision,
    cuotas_x_pagar integer,
    monto_x_pagar double precision,
    cuotas_vencidas integer,
    monto_vencido double precision,
    moras double precision,
    moras_pagadas double precision,
    notificaciones double precision,
    notificaciones_pagadas double precision,
    otros_pagos double precision,
    otros_pagos_pagadas double precision,
    ult_dia_pago timestamp without time zone,
    id_admin_condonacion bigint,
    fecha_hora_condonacion timestamp without time zone,
    monto_condonacion double precision,
    detalle_condonacion text,
    descuento_precancelacion double precision,
    id_ficha integer,
    dias smallint,
    noti_acomulada character varying(2),
    oficina character(50),
    calificacion smallint,
    area character(2),
    fecha_cierre date,
    contrato text,
    agencia character varying(30),
    penalidades_pagadas double precision,
    hora_pagar time with time zone,
    pago_en character(10),
    obs_negacion text,
    id_caja_apertura_cierre bigint,
    serie integer
);


ALTER TABLE public.solicitud_credito OWNER TO postgres;

--
-- Name: solicitud_credito_id_solicitud_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.solicitud_credito_id_solicitud_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.solicitud_credito_id_solicitud_seq OWNER TO postgres;

--
-- Name: solicitud_credito_id_solicitud_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.solicitud_credito_id_solicitud_seq OWNED BY public.solicitud_credito.id_solicitud;


--
-- Name: sueldo_movimiento; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sueldo_movimiento (
    id_sueldo_movimiento bigint NOT NULL,
    id_personal integer NOT NULL,
    detalle_sueldo character varying(50) NOT NULL,
    monto real NOT NULL,
    fecha_hora timestamp without time zone NOT NULL,
    estado boolean,
    oficina character varying(50) NOT NULL,
    cajera character varying(50) NOT NULL,
    dni character(8),
    obs text,
    fecha_hora_ejecutado time with time zone,
    agencia character(30),
    cod_cierre integer,
    detalle_devolucion character varying(250),
    fecha_hora_devuelto timestamp without time zone,
    user_devolucion character varying(50),
    monto_devuelto real DEFAULT 0 NOT NULL,
    obs_cancelacion text,
    id_agencia integer
);


ALTER TABLE public.sueldo_movimiento OWNER TO postgres;

--
-- Name: sueldo_movimiento_id_sueldo_movimiento_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.sueldo_movimiento_id_sueldo_movimiento_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sueldo_movimiento_id_sueldo_movimiento_seq OWNER TO postgres;

--
-- Name: sueldo_movimiento_id_sueldo_movimiento_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.sueldo_movimiento_id_sueldo_movimiento_seq OWNED BY public.sueldo_movimiento.id_sueldo_movimiento;


--
-- Name: tipo_ahorro; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tipo_ahorro (
    id_tipo_ahorro bigint NOT NULL,
    nombre_ahorro character varying(50) NOT NULL,
    obs text
);


ALTER TABLE public.tipo_ahorro OWNER TO postgres;

--
-- Name: tipo_ahorro_id_tipo_ahorro_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tipo_ahorro_id_tipo_ahorro_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tipo_ahorro_id_tipo_ahorro_seq OWNER TO postgres;

--
-- Name: tipo_ahorro_id_tipo_ahorro_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tipo_ahorro_id_tipo_ahorro_seq OWNED BY public.tipo_ahorro.id_tipo_ahorro;


--
-- Name: tipo_notificacion; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tipo_notificacion (
    id_tipo_notifi bigint NOT NULL,
    nombre_notificacion character varying(50) NOT NULL,
    obs text,
    valor real
);


ALTER TABLE public.tipo_notificacion OWNER TO postgres;

--
-- Name: tipo_notificacion_id_tipo_notifi_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tipo_notificacion_id_tipo_notifi_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tipo_notificacion_id_tipo_notifi_seq OWNER TO postgres;

--
-- Name: tipo_notificacion_id_tipo_notifi_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tipo_notificacion_id_tipo_notifi_seq OWNED BY public.tipo_notificacion.id_tipo_notifi;


--
-- Name: usuario; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.usuario (
    id_usuario integer NOT NULL,
    apellidos character varying(50) NOT NULL,
    nombres character varying(50) NOT NULL,
    tipo_usuario integer NOT NULL,
    direccion character varying(100),
    telefono character varying(100),
    email character varying(150),
    login character varying(50),
    pass character varying(50),
    obs text,
    activado boolean DEFAULT true NOT NULL,
    salario double precision,
    salario_actual double precision,
    agencia bigint,
    dni character(8),
    ingreso_temprano time with time zone,
    ingreso_tarde time with time zone,
    ingreso_sabados timestamp without time zone,
    estado_sueldo boolean DEFAULT false NOT NULL,
    fecha_ingreso date,
    movil real,
    uniforme real,
    aporte_afp real,
    aporte_onp real,
    id_agencia integer DEFAULT 1,
    creacion timestamp without time zone,
    cargo character varying(100)
);


ALTER TABLE public.usuario OWNER TO postgres;

--
-- Name: usuario_id_usuario_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.usuario_id_usuario_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.usuario_id_usuario_seq OWNER TO postgres;

--
-- Name: usuario_id_usuario_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.usuario_id_usuario_seq OWNED BY public.usuario.id_usuario;


--
-- Name: activar_reportes id_activar_rpt; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.activar_reportes ALTER COLUMN id_activar_rpt SET DEFAULT nextval('public.activar_reportes_id_activar_rpt_seq'::regclass);


--
-- Name: agencia id_agencia; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.agencia ALTER COLUMN id_agencia SET DEFAULT nextval('public.agencia_id_agencia_seq'::regclass);


--
-- Name: agencia id_empresa; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.agencia ALTER COLUMN id_empresa SET DEFAULT nextval('public.agencia_id_empresa_seq'::regclass);


--
-- Name: apertura_cierre id_caja_apertura_cierre; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.apertura_cierre ALTER COLUMN id_caja_apertura_cierre SET DEFAULT nextval('public.apertura_cierre_id_caja_apertura_cierre_seq'::regclass);


--
-- Name: aportaciones id_aportaciones; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.aportaciones ALTER COLUMN id_aportaciones SET DEFAULT nextval('public.aportaciones_id_aportaciones_seq'::regclass);


--
-- Name: banco id_banco; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.banco ALTER COLUMN id_banco SET DEFAULT nextval('public.banco_id_banco_seq'::regclass);


--
-- Name: billeteo id_billeteo; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.billeteo ALTER COLUMN id_billeteo SET DEFAULT nextval('public.billeteo_id_billeteo_seq'::regclass);


--
-- Name: boleta id_boleta; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.boleta ALTER COLUMN id_boleta SET DEFAULT nextval('public.boleta_id_boleta_seq'::regclass);


--
-- Name: boveda id_boveda; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.boveda ALTER COLUMN id_boveda SET DEFAULT nextval('public.boveda_id_boveda_seq'::regclass);


--
-- Name: campana id_campana; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.campana ALTER COLUMN id_campana SET DEFAULT nextval('public.campana_id_campana_seq'::regclass);


--
-- Name: central_riesgo id_central_riesgo; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.central_riesgo ALTER COLUMN id_central_riesgo SET DEFAULT nextval('public.central_riesgo_id_central_riesgo_seq'::regclass);


--
-- Name: cierre_caja_principal id_cierre_principal; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cierre_caja_principal ALTER COLUMN id_cierre_principal SET DEFAULT nextval('public.cierre_caja_principal_id_cierre_principal_seq'::regclass);


--
-- Name: cierre_planilla id_cierre_planilla; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cierre_planilla ALTER COLUMN id_cierre_planilla SET DEFAULT nextval('public.cierre_planilla_id_cierre_planilla_seq'::regclass);


--
-- Name: clave_cliente id_clave_cliente; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.clave_cliente ALTER COLUMN id_clave_cliente SET DEFAULT nextval('public.clave_cliente_id_clave_cliente_seq'::regclass);


--
-- Name: cliente_aval id_cliente_aval; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cliente_aval ALTER COLUMN id_cliente_aval SET DEFAULT nextval('public.cliente_aval_id_cliente_aval_seq'::regclass);


--
-- Name: cliente_beneficiario id_cliente_beneficiario; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cliente_beneficiario ALTER COLUMN id_cliente_beneficiario SET DEFAULT nextval('public.cliente_beneficiario_id_cliente_beneficiario_seq'::regclass);


--
-- Name: compromiso id_compromiso; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.compromiso ALTER COLUMN id_compromiso SET DEFAULT nextval('public.compromiso_id_compromiso_seq'::regclass);


--
-- Name: conyuge id_conyuge; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.conyuge ALTER COLUMN id_conyuge SET DEFAULT nextval('public.conyuge_id_conyuge_seq'::regclass);


--
-- Name: credito_refinanciado id_refinanciado; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.credito_refinanciado ALTER COLUMN id_refinanciado SET DEFAULT nextval('public.credito_refinanciado_id_refinanciado_seq'::regclass);


--
-- Name: cronograma_ahorro id_cronograma_ahorro; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cronograma_ahorro ALTER COLUMN id_cronograma_ahorro SET DEFAULT nextval('public.cronograma_ahorro_id_cronograma_ahorro_seq'::regclass);


--
-- Name: cronograma_pagos id_cronograma_pago; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cronograma_pagos ALTER COLUMN id_cronograma_pago SET DEFAULT nextval('public.cronograma_pagos_id_cronograma_pago_seq'::regclass);


--
-- Name: departamento id_departamento; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.departamento ALTER COLUMN id_departamento SET DEFAULT nextval('public.departamento_id_departamento_seq'::regclass);


--
-- Name: detalle_negocio id_detalle_negocio; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.detalle_negocio ALTER COLUMN id_detalle_negocio SET DEFAULT nextval('public.detalle_negocio_id_detalle_negocio_seq'::regclass);


--
-- Name: detalle_negocio id_negocio; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.detalle_negocio ALTER COLUMN id_negocio SET DEFAULT nextval('public.detalle_negocio_id_negocio_seq'::regclass);


--
-- Name: dias_festivos id_dia_festivo; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.dias_festivos ALTER COLUMN id_dia_festivo SET DEFAULT nextval('public.dias_festivos_id_dia_festivo_seq'::regclass);


--
-- Name: dias_festivos id_agencia; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.dias_festivos ALTER COLUMN id_agencia SET DEFAULT nextval('public.dias_festivos_id_agencia_seq'::regclass);


--
-- Name: distrito id_distrito; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.distrito ALTER COLUMN id_distrito SET DEFAULT nextval('public.distrito_id_distrito_seq'::regclass);


--
-- Name: distrito id_provincia; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.distrito ALTER COLUMN id_provincia SET DEFAULT nextval('public.distrito_id_provincia_seq'::regclass);


--
-- Name: empresa id_empresa; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.empresa ALTER COLUMN id_empresa SET DEFAULT nextval('public.empresa_id_empresa_seq'::regclass);


--
-- Name: equipos id_aquipos; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.equipos ALTER COLUMN id_aquipos SET DEFAULT nextval('public.equipos_id_aquipos_seq'::regclass);


--
-- Name: equipos id_agencia; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.equipos ALTER COLUMN id_agencia SET DEFAULT nextval('public.equipos_id_agencia_seq'::regclass);


--
-- Name: ficha_domiciliaria id_ficha_domiciliaria; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ficha_domiciliaria ALTER COLUMN id_ficha_domiciliaria SET DEFAULT nextval('public.ficha_domiciliaria_id_ficha_domiciliaria_seq'::regclass);


--
-- Name: ficha_economica id_ficha; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ficha_economica ALTER COLUMN id_ficha SET DEFAULT nextval('public.ficha_economica_id_ficha_seq'::regclass);


--
-- Name: gastos_operativos id_gastos_operativos; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.gastos_operativos ALTER COLUMN id_gastos_operativos SET DEFAULT nextval('public.gastos_operativos_id_gastos_operativos_seq'::regclass);


--
-- Name: gastos_operativos id_negocio; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.gastos_operativos ALTER COLUMN id_negocio SET DEFAULT nextval('public.gastos_operativos_id_negocio_seq'::regclass);


--
-- Name: historial_manipulacion id_historial_manipulacion; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.historial_manipulacion ALTER COLUMN id_historial_manipulacion SET DEFAULT nextval('public.historial_manipulacion_id_historial_manipulacion_seq'::regclass);


--
-- Name: historial_manipulacion id_usuario; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.historial_manipulacion ALTER COLUMN id_usuario SET DEFAULT nextval('public.historial_manipulacion_id_usuario_seq'::regclass);


--
-- Name: historial_visitas id_historial_visitas; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.historial_visitas ALTER COLUMN id_historial_visitas SET DEFAULT nextval('public.historial_visitas_id_historial_visitas_seq'::regclass);


--
-- Name: ingreso_egreso id_ingreso_egreso; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ingreso_egreso ALTER COLUMN id_ingreso_egreso SET DEFAULT nextval('public.ingreso_egreso_id_ingreso_egreso_seq'::regclass);


--
-- Name: insumo_negocio id_insumo_negocio; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.insumo_negocio ALTER COLUMN id_insumo_negocio SET DEFAULT nextval('public.insumo_negocio_id_insumo_negocio_seq'::regclass);


--
-- Name: insumo_negocio id_detalle_negocio; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.insumo_negocio ALTER COLUMN id_detalle_negocio SET DEFAULT nextval('public.insumo_negocio_id_detalle_negocio_seq'::regclass);


--
-- Name: inventario_bienes id_inventario_bienes; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.inventario_bienes ALTER COLUMN id_inventario_bienes SET DEFAULT nextval('public.inventario_bienes_id_inventario_bienes_seq'::regclass);


--
-- Name: libreta_ahorro id_libreta_ahorro; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.libreta_ahorro ALTER COLUMN id_libreta_ahorro SET DEFAULT nextval('public.libreta_ahorro_id_libreta_ahorro_seq'::regclass);


--
-- Name: metas_personal id_metas_personal; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.metas_personal ALTER COLUMN id_metas_personal SET DEFAULT nextval('public.metas_personal_id_metas_personal_seq'::regclass);


--
-- Name: movimiento id_movimiento; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.movimiento ALTER COLUMN id_movimiento SET DEFAULT nextval('public.movimiento_id_movimiento_seq'::regclass);


--
-- Name: movimiento_usuario id_movimiento_usuario; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.movimiento_usuario ALTER COLUMN id_movimiento_usuario SET DEFAULT nextval('public.movimiento_usuario_id_movimiento_usuario_seq'::regclass);


--
-- Name: negocio id_negocio; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.negocio ALTER COLUMN id_negocio SET DEFAULT nextval('public.negocio_id_negocio_seq'::regclass);


--
-- Name: nivel_usuario id_nivel_usuario; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.nivel_usuario ALTER COLUMN id_nivel_usuario SET DEFAULT nextval('public.nivel_usuario_id_nivel_usuario_seq'::regclass);


--
-- Name: notificaciones id_notificaciones; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.notificaciones ALTER COLUMN id_notificaciones SET DEFAULT nextval('public.notificaciones_id_notificaciones_seq'::regclass);


--
-- Name: operacion id_operacion; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.operacion ALTER COLUMN id_operacion SET DEFAULT nextval('public.operacion_id_operacion_seq'::regclass);


--
-- Name: operacion_banco id_operacion_banco; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.operacion_banco ALTER COLUMN id_operacion_banco SET DEFAULT nextval('public.operacion_banco_id_operacion_banco_seq'::regclass);


--
-- Name: operacion_pago id_operacion_pago; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.operacion_pago ALTER COLUMN id_operacion_pago SET DEFAULT nextval('public.operacion_pago_id_operacion_pago_seq'::regclass);


--
-- Name: pariente id_pariente; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.pariente ALTER COLUMN id_pariente SET DEFAULT nextval('public.pariente_id_pariente_seq'::regclass);


--
-- Name: planilla id_planilla; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.planilla ALTER COLUMN id_planilla SET DEFAULT nextval('public.planilla_id_planilla_seq'::regclass);


--
-- Name: provincia id_provincia; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.provincia ALTER COLUMN id_provincia SET DEFAULT nextval('public.provincia_id_provincia_seq'::regclass);


--
-- Name: provincia id_departamento; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.provincia ALTER COLUMN id_departamento SET DEFAULT nextval('public.provincia_id_departamento_seq'::regclass);


--
-- Name: sectores id_sector; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sectores ALTER COLUMN id_sector SET DEFAULT nextval('public.sectores_id_sector_seq'::regclass);


--
-- Name: solicitud_credito id_solicitud; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.solicitud_credito ALTER COLUMN id_solicitud SET DEFAULT nextval('public.solicitud_credito_id_solicitud_seq'::regclass);


--
-- Name: sueldo_movimiento id_sueldo_movimiento; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sueldo_movimiento ALTER COLUMN id_sueldo_movimiento SET DEFAULT nextval('public.sueldo_movimiento_id_sueldo_movimiento_seq'::regclass);


--
-- Name: tipo_ahorro id_tipo_ahorro; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tipo_ahorro ALTER COLUMN id_tipo_ahorro SET DEFAULT nextval('public.tipo_ahorro_id_tipo_ahorro_seq'::regclass);


--
-- Name: tipo_notificacion id_tipo_notifi; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tipo_notificacion ALTER COLUMN id_tipo_notifi SET DEFAULT nextval('public.tipo_notificacion_id_tipo_notifi_seq'::regclass);


--
-- Name: usuario id_usuario; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.usuario ALTER COLUMN id_usuario SET DEFAULT nextval('public.usuario_id_usuario_seq'::regclass);


--
-- Data for Name: activar_reportes; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.activar_reportes (id_activar_rpt, rpt_cobranza) FROM stdin;
\.


--
-- Data for Name: agencia; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.agencia (id_agencia, id_empresa, nombre_agencia, direccion_agencia, telefono_agencia, codigo, mensaje) FROM stdin;
1	1	HUANCAYO	Jr. Arequipa 705 - Chilca	(064) 000000	1  	**GRACIAS POR SU PREFERENCIA**
2	1	TINGOMARIA	Av. La Raymundi S/N	000000	2  	**GRACIAS POR SU PREFERENCIA**
3	1	PUCALLPA	Jr. Tarapaca S/N	00000000	3  	**GRACIAS POR SU PREFERENCIA**
\.


--
-- Data for Name: apertura_cierre; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.apertura_cierre (id_caja_apertura_cierre, equipo_registrado, id_usuario_apertura, fecha_hora_aperturado, monto_aperturado, obs_aperturado, id_usuario_cierre, fecha_hora_cierre, monto_cierre, obs_cierre, total_billetes, total_monedas, total_general, total_otros_ingresos, total_ingreso_ahorro, total_cobrado, total_ingresos, total_sub_egresos, total_retiros, total_desembolsado, total_egresos, total_caja, sobra_falta_caja, cerrado_principal, id_agencia) FROM stdin;
3847	DESKTOP-6HII7R6	4	2020-06-29 03:57:04	0		4	2020-06-29 04:26:47	0	 	0	0	0	0	0	0	0	0	0	0	0	0	0	f	1
3848	DESKTOP-6HII7R6	4	2020-06-30 10:06:47	0		4	2020-06-30 04:15:47	0	 	0	0	0	0	6242.32999999999993	24.6000000000000014	6266.93000000000029	0	0	6384.19999999999982	6384.19999999999982	-117.269999999999996	117.269999999999996	f	1
3872	DESKTOP-6HII7R6	4	2020-07-16 09:19:20	359		4	2020-07-16 02:56:22	7.09999999999999964	 	0	7.09999999999999964	7.09999999999999964	13653	210	691	14913	5430	0	8199.60000000000036	13629.6000000000004	1283.40000000000009	-1276.29999999999995	f	1
3850	DESKTOP-6HII7R6	4	2020-07-01 01:55:17	6384.19999999999982		4	2020-07-01 06:33:08	108.799999999999997	 	100	8.80000000000000071	108.799999999999997	800	0	0	7184.19999999999982	6060	0	0	6060	1124.20000000000005	-1015.39999999999998	f	1
3851	ADMINIPUCALLPA	9	2020-07-01 07:38:14	0		\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	3
3852	DESKTOP-6HII7R6	4	2020-07-02 08:48:59	108.299999999999997		4	2020-07-02 02:15:02	126.900000000000006	 	110	16.8999999999999986	126.900000000000006	150	24535	8.19999999999999929	24801.5	60	2000	4311.89999999999964	6371.89999999999964	18429.5999999999985	-18302.7000000000007	f	1
3853	DESKTOP-6HII7R6	4	2020-07-03 09:25:42	126.900000000000006		4	2020-07-03 02:55:13	590.899999999999977	 	580	10.9000000000000004	590.899999999999977	0	4220	2464	6810.89999999999964	2014	82	3780.59999999999991	5876.60000000000036	934.299999999999955	-343.399999999999977	f	1
3854	DESKTOP-6HII7R6	4	2020-07-04 09:40:23	590.899999999999977		4	2020-07-04 02:05:15	590.899999999999977	 	470	120.900000000000006	590.899999999999977	0	925	0	1515.90000000000009	140	0	543.399999999999977	683.399999999999977	832.5	-241.599999999999994	f	1
3855	DESKTOP-6HII7R6	4	2020-07-06 09:18:36	590.899999999999977		4	2020-07-06 04:15:23	590.899999999999977	 	470	120.900000000000006	590.899999999999977	5000	0	16.3999999999999986	5607.30000000000018	0	0	1500	1500	4107.30000000000018	-3516.40000000000009	f	1
3857	ADMINIPUCALLPA	9	2020-07-07 10:03:43	2760		\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	3
3856	DESKTOP-6HII7R6	4	2020-07-07 09:21:14	3516.40000000000009		4	2020-07-07 02:37:58	3716.40000000000009	 	3530	186.400000000000006	3716.40000000000009	0	800	0	4316.39999999999964	30	0	0	30	4286.39999999999964	-570	f	1
3858	DESKTOP-6HII7R6	4	2020-07-08 09:15:55	4286.39999999999964		4	2020-07-08 02:07:29	3756.40000000000009	 	3700	56.3999999999999986	3756.40000000000009	0	300	0	4586.39999999999964	654.600000000000023	100	0	754.600000000000023	3831.80000000000018	-75.4000000000000057	f	1
3859	ADMINIPUCALLPA	9	2020-07-08 10:01:55	128.400000000000006		9	2020-07-08 11:56:11	1563.40000000000009	 	1540	23.3999999999999986	1563.40000000000009	0	0	242.199999999999989	370.600000000000023	0	0	1312.79999999999995	1312.79999999999995	-942.200000000000045	2505.59999999999991	f	3
3861	ADMINIPUCALLPA	9	2020-07-09 12:36:26	1563.40000000000009		\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	3
3860	DESKTOP-6HII7R6	4	2020-07-09 12:04:32	3831.80000000000018		4	2020-07-09 02:26:48	3909.80000000000018	 	3900	9.80000000000000071	3909.80000000000018	0	0	324	4155.80000000000018	246	0	300	546	3609.80000000000018	300	f	1
3862	DESKTOP-6HII7R6	4	2020-07-10 09:38:44	709.799999999999955		4	2020-07-10 03:07:57	1031.79999999999995	 	970	61.7999999999999972	1031.79999999999995	1243	645	223.949999999999989	2821.75	4150	645	12310.8999999999996	17105.9000000000015	-14284.1499999999996	15315.9500000000007	f	1
3863	ADMINIPUCALLPA	9	2020-07-10 11:46:59	1930.59999999999991		9	2020-07-10 10:30:01	610.600000000000023	 	600	10.5999999999999996	610.600000000000023	0	0	51.7000000000000028	1982.29999999999995	120	0	1200	1320	662.299999999999955	-51.7000000000000028	f	3
3864	DESKTOP-6HII7R6	4	2020-07-11 09:19:25	15315.9500000000007		4	2020-07-11 01:14:23	486.899999999999977	 	450	36.8999999999999986	486.899999999999977	0	0	286.850000000000023	15602.7999999999993	15115	0	371.300000000000011	15486.2999999999993	116.5	370.399999999999977	f	1
3865	ADMINIPUCALLPA	9	2020-07-11 06:27:05	610.600000000000023		9	2020-07-11 07:47:48	714.200000000000045	 	630	84.2000000000000028	714.200000000000045	0	0	51.7999999999999972	662.399999999999977	0	0	0	0	662.399999999999977	51.7999999999999972	f	3
3866	DESKTOP-6HII7R6	4	2020-07-13 09:10:35	857.299999999999955		4	2020-07-13 02:57:27	201.5	 	200	1.5	201.5	552.299999999999955	6754	1234.20000000000005	9397.79999999999927	7570	500	1300	9370	27.8000000000000007	173.699999999999989	f	1
3867	ADMINIPUCALLPA	9	2020-07-13 11:40:27	714.200000000000045		9	2020-07-13 04:38:55	368.800000000000011	 	330	38.7999999999999972	368.800000000000011	0	0	102.799999999999997	817	0	0	500	500	317	51.7999999999999972	f	3
3868	DESKTOP-6HII7R6	4	2020-07-14 09:56:38	375.199999999999989		4	2020-07-14 01:47:26	4329.60000000000036	 	3950	379.600000000000023	4329.60000000000036	4329.60000000000036	580	0	5284.80000000000018	0	0	4025.40000000000009	4025.40000000000009	1259.40000000000009	3070.19999999999982	f	1
3869	ADMINIPUCALLPA	9	2020-07-14 03:54:13	317		9	2020-07-14 04:15:15	419.800000000000011	 	360	59.7999999999999972	419.800000000000011	0	0	102.799999999999997	419.800000000000011	0	0	0	0	419.800000000000011	0	f	3
3870	DESKTOP-6HII7R6	4	2020-07-15 09:05:43	7399.80000000000018		4	2020-07-15 02:56:12	500.199999999999989	 	490	10.1999999999999993	500.199999999999989	0	0	41.3999999999999986	7441.19999999999982	7000	0	300	7300	141.199999999999989	359	f	1
3871	ADMINIPUCALLPA	9	2020-07-15 03:54:32	419.800000000000011		9	2020-07-15 04:03:01	543.399999999999977	 	500	43.3999999999999986	543.399999999999977	0	0	123.599999999999994	543.399999999999977	0	0	0	0	543.399999999999977	0	f	3
3873	ADMINIPUCALLPA	9	2020-07-16 07:26:48	543.399999999999977		9	2020-07-16 07:45:50	616	 	540	76	616	0	0	72.5999999999999943	616	0	0	0	0	616	0	f	3
3875	ADMINIPUCALLPA	9	2020-07-17 06:38:42	616		9	2020-07-17 07:18:06	728.200000000000045	 	630	98.2000000000000028	728.200000000000045	0	0	112.200000000000003	728.200000000000045	0	0	0	0	728.200000000000045	0	f	3
3874	DESKTOP-6HII7R6	4	2020-07-17 09:12:49	1276.29999999999995		4	2020-07-17 08:49:27	844.799999999999955	 	830	14.8000000000000007	844.799999999999955	0	0	481.25	1757.54999999999995	350	0	0	350	1407.54999999999995	-562.75	f	1
3876	DESKTOP-6HII7R6	4	2020-07-18 10:47:37	562.75		4	2020-07-18 12:23:31	575.25	 	530	45.25	575.25	0	0	12.5	575.25	0	0	300	300	275.25	300	f	1
3877	DESKTOP-6HII7R6	4	2020-07-20 09:18:56	300		4	2020-07-20 03:06:33	0	 	0	0	0	2552.80000000000018	0	161.5	3014.30000000000018	0	500	1153.90000000000009	1653.90000000000009	1360.40000000000009	-1360.40000000000009	f	1
3878	ADMINIPUCALLPA	9	2020-07-20 09:50:32	728.200000000000045		9	2020-07-20 10:18:35	1089.59999999999991	 	1020	69.5999999999999943	1089.59999999999991	0	0	361.399999999999977	1089.59999999999991	0	0	0	0	1089.59999999999991	0	f	3
3879	DESKTOP-6HII7R6	4	2020-07-22 09:24:05	0		4	2020-07-22 02:24:48	555.200000000000045	 	310	245.199999999999989	555.200000000000045	994.200000000000045	0	1904.59999999999991	2898.80000000000018	500	0	850	1350	1548.79999999999995	-993.600000000000023	f	1
3880	ADMINIPUCALLPA	9	2020-07-22 12:49:08	1089.59999999999991		9	2020-07-22 08:05:50	504.800000000000011	 	410	94.7999999999999972	504.800000000000011	0	0	245.199999999999989	1334.79999999999995	30	0	800	830	504.800000000000011	0	f	3
3881	DESKTOP-6HII7R6	4	2020-07-23 09:19:01	555.200000000000045		4	2020-07-23 01:54:40	1322	 	1120	202	1322	0	980	33.1000000000000014	1568.29999999999995	0	0	0	0	1568.29999999999995	-246.300000000000011	f	1
3882	ADMINIPUCALLPA	9	2020-07-23 06:13:46	504.800000000000011		9	2020-07-23 06:30:53	668	 	560	108	668	0	0	163.199999999999989	668	0	0	0	0	668	0	f	3
3883	DESKTOP-6HII7R6	4	2020-07-24 10:01:46	341.5		4	2020-07-24 02:27:48	54.5	 	50	4.5	54.5	500	15	153.599999999999994	1010.10000000000002	940	0	0	940	70.0999999999999943	-15.5999999999999996	f	1
3884	DESKTOP-6HII7R6	4	2020-07-25 12:01:21	0		4	2020-07-25 01:20:07	110.650000000000006	 	100	10.6500000000000004	110.650000000000006	0	10	100.650000000000006	110.650000000000006	0	0	0	0	110.650000000000006	0	f	1
3885	ADMINIPUCALLPA	9	2020-07-25 05:20:40	668		9	2020-07-25 05:34:13	994.399999999999977	 	860	134.400000000000006	994.399999999999977	0	0	326.399999999999977	994.399999999999977	0	0	0	0	994.399999999999977	0	f	3
3886	DESKTOP-6HII7R6	4	2020-07-27 11:33:03	0		4	2020-07-27 10:59:30	0	 	0	0	0	10000	0	0	10000	10000	0	0	10000	0	0	f	1
3887	ADMINIPUCALLPA	9	2020-07-29 04:13:54	994.399999999999977		9	2020-07-29 04:32:15	1730.79999999999995	 	1540	190.800000000000011	1730.79999999999995	0	0	736.399999999999977	1730.79999999999995	0	0	0	0	1730.79999999999995	0	f	3
3888	ADMINIPUCALLPA	9	2020-07-30 04:25:04	1730.79999999999995		9	2020-07-30 04:44:56	1825	 	1630	195	1825	0	0	94.2000000000000028	1825	0	0	0	0	1825	0	f	3
3889	ADMINIPUCALLPA	9	2020-07-31 02:14:50	1825		9	2020-07-31 07:36:24	619.200000000000045	 	580	39.2000000000000028	619.200000000000045	0	0	94.2000000000000028	1919.20000000000005	0	0	1300	1300	619.200000000000045	0	f	3
3890	ADMINIPUCALLPA	9	2020-08-01 06:26:10	619.200000000000045		9	2020-08-01 07:01:38	999.100000000000023	 	930	69.0999999999999943	999.100000000000023	0	0	379.899999999999977	999.100000000000023	0	0	0	0	999.100000000000023	0	f	3
3891	ADMINIPUCALLPA	9	2020-08-03 09:38:25	999.100000000000023		9	2020-08-03 10:02:25	1114.40000000000009	 	1020	94.4000000000000057	1114.40000000000009	0	0	115.299999999999997	1114.40000000000009	0	0	0	0	1114.40000000000009	0	f	3
3892	ADMINIPUCALLPA	9	2020-08-04 08:41:53	1114.40000000000009		9	2020-08-04 08:58:59	1332.90000000000009	 	1210	122.900000000000006	1332.90000000000009	0	0	318.5	1432.90000000000009	0	0	0	0	1432.90000000000009	-100	f	3
3893	ADMINIPUCALLPA	9	2020-08-05 09:13:11	1432.90000000000009		9	2020-08-05 09:43:20	962.200000000000045	 	790	172.199999999999989	962.200000000000045	0	0	179.300000000000011	1612.20000000000005	150	0	500	650	962.200000000000045	0	f	3
3894	ADMINIPUCALLPA	9	2020-08-06 06:10:12	962.200000000000045		9	2020-08-06 06:23:29	1191	 	1020	171	1191	0	0	228.800000000000011	1191	0	0	0	0	1191	0	f	3
3895	ADMINIPUCALLPA	9	2020-08-07 09:23:10	1191		9	2020-08-07 05:29:48	822	 	790	32	822	0	0	231	1422	0	0	600	600	822	0	f	3
3896	ADMINIPUCALLPA	9	2020-08-08 12:09:20	822		\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	3
3897	DESKTOP-6HII7R6	4	2020-08-10 08:55:32	0		4	2020-08-10 01:54:06	435	 	400	35	435	0	0	535	535	0	100	0	100	435	0	f	1
3898	ADMINIPUCALLPA	9	2020-08-10 07:03:00	822		9	2020-08-10 07:21:50	182.800000000000011	 	150	32.7999999999999972	182.800000000000011	0	0	162.800000000000011	984.799999999999955	0	0	0	0	984.799999999999955	-802	f	3
3899	DESKTOP-6HII7R6	4	2020-08-11 09:18:13	0		4	2020-08-11 01:39:53	346.199999999999989	 	300	46.2000000000000028	346.199999999999989	100	0	246.199999999999989	346.199999999999989	0	0	0	0	346.199999999999989	0	f	1
3900	ADMINIPUCALLPA	9	2020-08-11 05:49:22	182.800000000000011		9	2020-08-11 06:06:30	571.200000000000045	 	460	111.200000000000003	571.200000000000045	0	0	386.399999999999977	569.200000000000045	0	0	0	0	569.200000000000045	2	f	3
3901	DESKTOP-6HII7R6	4	2020-08-12 09:27:04	0		4	2020-08-12 01:54:32	456.399999999999977	 	390	66.4000000000000057	456.399999999999977	460	0	16.3999999999999986	476.399999999999977	20	0	0	20	456.399999999999977	0	f	1
3902	DESKTOP-6HII7R6	4	2020-08-14 09:01:58	0		4	2020-08-14 01:24:36	0	 	0	0	0	20	0	0	20	20	0	0	20	0	0	f	1
3903	ADMINIPUCALLPA	9	2020-08-14 06:00:54	571.200000000000045		9	2020-08-14 06:18:55	855.399999999999977	 	720	135.400000000000006	855.399999999999977	0	0	318.399999999999977	889.600000000000023	0	0	0	0	889.600000000000023	-34.2000000000000028	f	3
3904	DESKTOP-6HII7R6	4	2020-08-18 09:21:12	0		4	2020-08-18 01:58:58	26.3999999999999986	 	10	16.3999999999999986	26.3999999999999986	0	10	16.3999999999999986	26.3999999999999986	0	0	0	0	26.3999999999999986	0	f	1
3905	ADMINIPUCALLPA	9	2020-08-18 05:56:29	855.399999999999977		9	2020-08-18 06:05:04	1580.79999999999995	 	1340	240.800000000000011	1580.79999999999995	0	0	691.200000000000045	1546.59999999999991	0	0	0	0	1546.59999999999991	34.2000000000000028	f	3
3906	DESKTOP-6HII7R6	4	2020-08-20 09:30:11	0		4	2020-08-20 01:58:11	0	 	0	0	0	0	0	0	0	0	0	0	0	0	0	f	1
3907	DESKTOP-6HII7R6	4	2020-08-22 09:37:38	0		4	2020-08-22 01:10:29	0	 	0	0	0	0	0	0	0	0	0	0	0	0	0	f	1
3908	ADMINIPUCALLPA	9	2020-08-23 10:29:59	1580.79999999999995		9	2020-08-23 10:53:46	1180.90000000000009	 	1150	30.8999999999999986	1180.90000000000009	0	0	430.899999999999977	2011.70000000000005	30	0	0	30	1981.70000000000005	-800.799999999999955	f	3
3909	DESKTOP-6HII7R6	4	2020-08-24 09:31:08	0		4	2020-08-24 02:00:26	390.199999999999989	 	300	90.2000000000000028	390.199999999999989	570	0	8.19999999999999929	578.200000000000045	188	0	0	188	390.199999999999989	0	f	1
\.


--
-- Data for Name: aportaciones; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.aportaciones (id_aportaciones, id_caja_apertura_cierre, tipo_operacion, dni, fecha_hora, total_pagar, agencia, puntos, id_agencia) FROM stdin;
\.


--
-- Data for Name: banco; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.banco (id_banco, entidad, cuenta_nro, fecha_hora_operacion, usuario, obs, monto_actual) FROM stdin;
\.


--
-- Data for Name: billeteo; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.billeteo (id_billeteo, id_caja_apertura_cierre, denominacion, cantidad, monto) FROM stdin;
\.


--
-- Data for Name: boleta; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.boleta (id_boleta, serie, inicio_boleta, id_agencia) FROM stdin;
1	1	333	\N
\.


--
-- Data for Name: boveda; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.boveda (id_boveda, monto_actual_boveda, equipo_registrado, fecha_hora_operacion, tipo, monto_movimiento, usuario, motivo, obs, usuario_habilita, cod_habiltacion, confirmar, agencia, id_agencia) FROM stdin;
1	7000	DJ-XAVEX	2020-06-30 15:53:53	DEPOSITO	7000	FMARTINEZ17	APORTE A BOVEDA	para desembolso	\N	\N	t	HUANCAYO	1
3	9000	DESKTOP-2COHNSG	2020-07-02 09:00:09	DEPOSITO	2000	FMARTINEZ17	APORTE A BOVEDA	aporte de consejo administrativo	\N	\N	t	HUANCAYO	\N
4	2000	DESKTOP-6HII7R6	2020-07-03 14:47:56	DEPOSITO	2000	BIM	INGRESO DE CAJA	\N	\N	\N	t	HUANCAYO	1
5	3000	DESKTOP-2COHNSG	2020-07-06 13:09:33	DEPOSITO	1000	FMARTINEZ17	APORTE A BOVEDA	habilitado para desembolso	\N	\N	t	HUANCAYO	\N
6	6000	DESKTOP-2COHNSG	2020-07-08 12:56:13	DEPOSITO	3000	FMARTINEZ17	APORTE A BOVEDA		\N	\N	t	HUANCAYO	\N
7	4150	DESKTOP-6HII7R6	2020-07-10 14:57:46	DEPOSITO	4150	BIM	INGRESO DE CAJA	\N	\N	\N	t	HUANCAYO	1
8	19150	DESKTOP-6HII7R6	2020-07-11 13:11:08	DEPOSITO	15000	BIM	INGRESO DE CAJA	\N	\N	\N	t	HUANCAYO	1
9	19650	DESKTOP-6HII7R6	2020-07-13 14:18:55	DEPOSITO	500	BIM	INGRESO DE CAJA	\N	\N	\N	t	HUANCAYO	1
10	26650	DESKTOP-6HII7R6	2020-07-13 14:55:56	DEPOSITO	7000	BIM	INGRESO DE CAJA	\N	\N	\N	t	HUANCAYO	1
11	33650	DESKTOP-6HII7R6	2020-07-15 14:54:43	DEPOSITO	7000	BIM	INGRESO DE CAJA	\N	\N	\N	t	HUANCAYO	1
12	38450	DESKTOP-6HII7R6	2020-07-16 14:55:33	DEPOSITO	4800	BIM	INGRESO DE CAJA	\N	\N	\N	t	HUANCAYO	1
13	38950	DESKTOP-6HII7R6	2020-07-22 14:24:31	DEPOSITO	500	BIM	INGRESO DE CAJA	\N	\N	\N	t	HUANCAYO	1
14	39850	DESKTOP-6HII7R6	2020-07-24 14:26:50	DEPOSITO	900	BIM	INGRESO DE CAJA	\N	\N	\N	t	HUANCAYO	1
15	49850	DESKTOP-6HII7R6	2020-07-27 09:14:28	DEPOSITO	10000	BIM	INGRESO DE CAJA	\N	\N	\N	t	HUANCAYO	1
\.


--
-- Data for Name: campana; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.campana (id_campana, name_campana, monto_campana, inicio_capana, fin_capana, id_personal, obs, id_agencia) FROM stdin;
1	CANASTA 2019	370	2019-07-15 00:00:00	2019-12-25 00:00:00	1	N	\N
\.


--
-- Data for Name: central_riesgo; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.central_riesgo (id_central_riesgo, dni, entidad, monto, calificacion, fecha, id_usuario, id_agencia) FROM stdin;
\.


--
-- Data for Name: cierre_caja_principal; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.cierre_caja_principal (id_cierre_principal, id_usuario, fecha_cierre, fecha_hora, obs, monto_cierre, id_agencia) FROM stdin;
\.


--
-- Data for Name: cierre_planilla; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.cierre_planilla (id_cierre_planilla, monto_cierre, fecha_hora_planilla, login, agencia, id_agencia) FROM stdin;
\.


--
-- Data for Name: ciiu; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.ciiu (id_ciiu, cod_ciiu, descripcion) FROM stdin;
\.


--
-- Data for Name: clave_cliente; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.clave_cliente (id_clave_cliente, dni_cliente, clave) FROM stdin;
\.


--
-- Data for Name: cliente; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.cliente (dni, paterno, materno, nombres, sexo, estado_civil, fecha_nacimiento, telefonos, e_mail, distrito, provincia, departamento, direccion, referencia, obs, fecha_hora_creacion, nro_oficina, nro_expediente, fecha_hora_expediente, id_analista, nombre_promotor, id_plataforma, historial, nro_ahorro, calificacion, pass_ahorro, nro_expediente_dos, estado_cliente, usuario, castigar_cliente, motivo_castigo, user_castigo, fecha_hora_castigo, puntos, codsocio, aportado, tiposocio, id_registro_user, id_user_exp, id_agencia, fecha_hora_exp_ahorro) FROM stdin;
01076980	ESTEBAN	SOLANO	RUTH MARIBEL	MASCULINO 	SOLTERO(A)	2020-01-01	999999999	@	Chilca	Huancayo	Junin	JIRÓN SN  NRO 0	referencia	obser	2020-06-28 11:54:41	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	1	\N	\N	\N
04004879	MATEO	CARHUAMACA VDA DE SIMON	EVA	MASCULINO 	SOLTERO(A)	2020-01-01	999999999	@	Chilca	Huancayo	Junin	CHILCA - PARQUE SANTOS CHOCANO  NRO 133	referencia	obser	2020-06-28 11:54:41	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	1	\N	\N	\N
06659103	DEL	AGUILA	JOVITA	MASCULINO 	SOLTERO(A)	2020-01-01	999999999	@	Chilca	Huancayo	Junin	AVENIDA AGRICULTURA  NRO 100	referencia	obser	2020-06-28 11:54:41	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	1	\N	\N	\N
07285584	VEGA	MENDEZ	MARINA YSABEL	MASCULINO 	SOLTERO(A)	2020-01-01	999999999	@	Tingo María	Tingo María	Huánuco	PASAJE CAJAMARCA  INT V	referencia	obser	2020-06-28 11:54:41	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	1	\N	\N	\N
10059587	RAMIREZ	CONCEPCION	BERTHA TERESA	MASCULINO 	SOLTERO(A)	2020-01-01	999999999	@	Chilca	Huancayo	Junin	ASENTAMIENTO HUMANO: LA PERLA - NRO 100	referencia	obser	2020-06-28 11:54:41	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	1	\N	\N	\N
10526180	FARIAS	SALINAS	TANIA	MASCULINO 	SOLTERO(A)	2020-01-01	999999999	@	Tingo María	Tingo María	Huánuco	AVENIDA RAYMONDI   NRO 592	referencia	obser	2020-06-28 11:54:41	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	1	\N	\N	\N
15373731	CARDENAS	GRANDEZ	CAROLINA	MASCULINO 	SOLTERO(A)	2020-01-01	999999999	@	Tingo María	Tingo María	Huánuco	NARANJILLO - JIRÓN LOS COLONOS  NRO 0	referencia	obser	2020-06-28 11:54:41	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	1	\N	\N	\N
20030559	CHANCA	HUAROC	ADELAYDA TEODORA	MASCULINO 	SOLTERO(A)	2020-01-01	999999999	@	Chilca	Huancayo	Junin	PASAJE LOS SAUCES  MZ P	referencia	obser	2020-06-28 11:54:41	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	1	\N	\N	\N
20663928	GUERE	LIMAYMANTA	MARIA ELENA	MASCULINO 	SOLTERO(A)	2020-01-01	999999999	@	Chilca	Huancayo	Junin	ASENTAMIENTO HUMANO: JUSTICIA PAZ Y VIDA - JIRÓN MARI AOLAYA	referencia	obser	2020-06-28 11:54:41	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	1	\N	\N	\N
22497842	NIEVES	ORDOÑEZ	YONI	MASCULINO 	SOLTERO(A)	2020-01-01	999999999	@	Chilca	Huancayo	Junin	JIRÓN SIMON BOLIVAR  NRO 292	referencia	obser	2020-06-28 11:54:41	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	1	\N	\N	\N
22521330	FAUSTINO	CUADRADO	DALIA YOBANA	MASCULINO 	SOLTERO(A)	2020-01-01	999999999	@	Chilca	Huancayo	Junin	JIRÓN CAJAMARCA  MZ B	referencia	obser	2020-06-28 11:54:41	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	1	\N	\N	\N
22977116	BRAVO	ILDEFONCIO	SANTA	MASCULINO 	SOLTERO(A)	2020-01-01	999999999	@	Tingo María	Tingo María	Huánuco	ASENTAMIENTO HUMANO: 1 DE JULIO -	referencia	obser	2020-06-28 11:54:41	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	1	\N	\N	\N
23001533	SALAS	RENGIFO	MARISOL	MASCULINO 	SOLTERO(A)	2020-01-01	999999999	@	Chilca	Huancayo	Junin	CPME MAPRESA - CARRETERA MZ                         MZ 10	referencia	obser	2020-06-28 11:54:41	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	1	\N	\N	\N
19999596	NARBAIZA	VILELA	ELIBERTA	MASCULINO 	SOLTERO(A)	2020-01-01	999999999	@	Chilca	Huancayo	Junin	CALLE CHACLAS  NRO 620	referencia	obser	2020-06-28 11:54:41	HUANCAYO                                          	10	2020-06-29 12:55:03	5	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	1	2	1	\N
20059776	CONTRERAS	JARA	LUZ ANGELICA	MASCULINO 	SOLTERO(A)	2020-01-01	999999999	@	Chilca	Huancayo	Junin	URBANIZACIÓN: CHILCA - JIRÓN RICARDO PALMA  NRO 776	referencia	obser	2020-06-28 11:54:41	HUANCAYO                                          	17	2020-06-29 14:45:05	3	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	1	3	1	\N
19920401	GOMEZ	VENTURO	JUANITA	MASCULINO 	SOLTERO(A)	2020-01-01	999999999	@	Chilca	Huancayo	Junin	AVENIDA CHIPCHICANCHA	referencia	obser	2020-06-28 11:54:41	HUANCAYO                                          	27	2020-06-29 14:56:16	3	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	1	3	1	\N
20079039	LIZANA	IÑIGO	SONIA LUZ	MASCULINO 	SOLTERO(A)	2020-01-01	999999999	@	Chilca	Huancayo	Junin	PASAJE SAN SILVESTRE	referencia	obser	2020-06-28 11:54:41	HUANCAYO                                          	32	2020-06-29 15:00:01	3	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	1	3	1	\N
19946742	MARMOLEJO	ORELLANA	BETTY ELIZABETH	MASCULINO 	SOLTERO(A)	2020-01-01	999999999	@	Chilca	Huancayo	Junin	URBANIZACIÓN: ANEXO HUAMANMARCA - HUAMANMARCA    NRO 0	referencia	obser	2020-06-28 11:54:41	HUANCAYO                                          	56	2020-06-29 15:30:58	3	0	\N	\N	43	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	1	3	1	10:15:43-05
19944388	PORTA	ADAUTO	GLORIA BERNARDINA	MASCULINO 	SOLTERO(A)	2020-01-01	999999999	@	Chilca	Huancayo	Junin	CALLE PORVENIR	referencia	obser	2020-06-28 11:54:41	HUANCAYO                                          	45	2020-06-29 15:26:38	3	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	1	3	1	\N
19870079	COLLACHAGUA	PEREZ	TOMAS DAVID	MASCULINO 	SOLTERO(A)	2020-01-01	999999999	@	Chilca	Huancayo	Junin	UNIDAD VECINAL: HUANCAN - AVENIDA PANAMERICANA SUR	referencia	obser	2020-06-28 11:54:41	HUANCAYO                                          	85	2020-07-04 12:27:05	5	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	1	5	1	\N
20999895	ORE	BECERRA	NORMA GLADYS	MASCULINO 	SOLTERO(A)	2020-01-01	999999999	@	Chilca	Huancayo	Junin	PASAJE VILCAHUAMAN  NRO 134	referencia	obser	2020-06-28 11:54:41	HUANCAYO                                          	50	2020-06-29 15:28:36	3	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	1	3	1	\N
19940220	CARHUAMACA	PEREZ	TORIBIA TRINIDAD	MASCULINO 	SOLTERO(A)	2020-01-01	999999999	@	Chilca	Huancayo	Junin	JIRÓN JOSE OLAYA	referencia	obser	2020-06-28 11:54:41	HUANCAYO                                          	63	2020-06-30 13:21:07	5	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	1	5	1	\N
16134843	SOTO	AYLAS	ISABEL	MASCULINO 	SOLTERO(A)	2020-01-01	999999999	@	Chilca	Huancayo	Junin	CALLE JUNIN	referencia	obser	2020-06-28 11:54:41	HUANCAYO                                          	64	2020-06-30 13:53:44	5	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	1	5	1	\N
21276617	GUZMAN	ESTRADA	EDILBERTO	MASCULINO 	SOLTERO(A)	2020-01-01	999999999	@	Chilca	Huancayo	Junin	AVENIDA FERROCARRIL  NRO 5157	referencia	obser	2020-06-28 11:54:41	HUANCAYO                                          	35	2020-06-29 15:06:37	3	0	\N	\N	42	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	1	3	1	09:16:47-05
19944260	MOLINA	QUISPE	VERONICA TRINIDAD	MASCULINO 	SOLTERO(A)	2020-01-01	999999999	@	Chilca	Huancayo	Junin	HUANCAN - JIRÓN ALEXANDER FLEMING  NRO 0	referencia	obser	2020-06-28 11:54:41	HUANCAYO                                          	46	2020-06-29 15:26:59	3	0	\N	\N	18	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	1	3	1	10:12:43-05
21142620	CARRION	PIZANGO	ROBHIL ARISOL	MASCULINO 	SOLTERO(A)	1969-05-25	922466787		Irzola	Pucallpa	Ucayali	Caserío: Pueblo Libre - Jirón Huascar	Frente Ala Loza Deportiva De Moronal		2020-06-28 11:54:41	PUCALLPA                                          	10	2020-07-01 18:52:13	11	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	1	11	3	\N
00053944	PINEDO	LINARES	CLARA DEL PILAR	FEMENINO  	CONVIVIENTE	1964-02-28	958433587		Yarinacohca	Pucallpa	Ucayali	Jirón 2 De Mayo  Mz 94 Mz 94 Lt . 6	A 3 Cuadras De Maestranza		2020-06-28 11:54:41	PUCALLPA                                          	11	2020-07-01 18:53:30	11	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	1	11	3	\N
00074623	MONTELUIZA	HUAYTA	CARMEN	FEMENINO  	CONVIVIENTE	1960-07-16	-		Yarinacohca	Pucallpa	Ucayali	Jirón 2 De Mayo Con Ruperto Perez  Nro 150	Frente A La Casa De Empeños		2020-06-28 11:54:41	PUCALLPA                                          	7	2020-07-01 18:51:13	11	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	1	11	3	\N
00125733	RENGIFO	CABRERA	SANDRA	FEMENINO  	CASADO(A)	1977-06-01	938260661		Calleria	Pucallpa	Ucayali	Asentamiento Humano: Micaela Bastidas - Pasaje Union  Mz 2 Lt.3	A Espaldas Del Corah		2020-06-28 11:54:41	PUCALLPA                                          	24	2020-07-02 10:22:09	11	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	1	11	3	\N
23001796	MEZA	SANCHEZ	SONIA ROSA	MASCULINO 	SOLTERO(A)	2020-01-01	999999999	@	Tingo María	Tingo María	Huánuco	LOTIZACION RISUEÑO - MZ M	referencia	obser	2020-06-28 11:54:41	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	1	\N	\N	\N
23002267	SARMIENTO	CESPEDES	GREGORIO	MASCULINO 	SOLTERO(A)	2020-01-01	999999999	@	Chilca	Huancayo	Junin	AVENIDA SAN MARTIN  NRO 119	referencia	obser	2020-06-28 11:54:41	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	1	\N	\N	\N
23003089	DELGADO	YABAR	ROSA MARIA	MASCULINO 	SOLTERO(A)	2020-01-01	999999999	@	Chilca	Huancayo	Junin	ASENTAMIENTO HUMANO: 9 DE OCTUBRE - AVENIDA MIRAFLORES  MZ A	referencia	obser	2020-06-28 11:54:41	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	1	\N	\N	\N
23007896	VASQUEZ	REYES	EVA PRUDENCIA	MASCULINO 	SOLTERO(A)	2020-01-01	999999999	@	Chilca	Huancayo	Junin	PUEBLO JOVEN: 06 DE AGOSTO -	referencia	obser	2020-06-28 11:54:41	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	1	\N	\N	\N
23008859	LAFFOSE	CASTRO	NEREA	MASCULINO 	SOLTERO(A)	2020-01-01	999999999	@	Chilca	Huancayo	Junin	BRISAS DEL HUALLAGA	referencia	obser	2020-06-28 11:54:41	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	1	\N	\N	\N
23011844	EUGENIO	SANDOVAL	IRMA	MASCULINO 	SOLTERO(A)	2020-01-01	999999999	@	Chilca	Huancayo	Junin	ASENTAMIENTO HUMANO: 9 DE OCTUBRE - CALLE LAS VEGAS	referencia	obser	2020-06-28 11:54:41	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	1	\N	\N	\N
23012981	ANGELES	JIMNEZ	SARA ESTHER	MASCULINO 	SOLTERO(A)	2020-01-01	999999999	@	Tingo María	Tingo María	Huánuco	ROSALES  MZ E	referencia	obser	2020-06-28 11:54:41	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	1	\N	\N	\N
23014330	BEZARES	ESPINOZA	CLODOBALDO PEPE	MASCULINO 	SOLTERO(A)	2020-01-01	999999999	@	Chilca	Huancayo	Junin	JIRÓN 7 DE MAYO   MZ 5	referencia	obser	2020-06-28 11:54:41	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	1	\N	\N	\N
23016894	RAMOS	BONIFACIO	MARGOT MERY	MASCULINO 	SOLTERO(A)	2020-01-01	999999999	@	Tingo María	Tingo María	Huánuco	PUEBLO JOVEN: BRISAS DEL HUALLAGA - MZ O	referencia	obser	2020-06-28 11:54:41	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	1	\N	\N	\N
23080269	MONTALVO	JIRON	SIMONA	MASCULINO 	SOLTERO(A)	2020-01-01	999999999	@	Chilca	Huancayo	Junin	JIRÓN MANTARO	referencia	obser	2020-06-28 11:54:41	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	1	\N	\N	\N
23167033	EUGENIO	QUITO	ALICIA	MASCULINO 	SOLTERO(A)	2020-01-01	999999999	@	Tingo María	Tingo María	Huánuco	NARANJILLO - ENTRADA A CORA CORA	referencia	obser	2020-06-28 11:54:41	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	1	\N	\N	\N
25701623	JESUS	PORRAS	SONIA MARIA	MASCULINO 	SOLTERO(A)	2020-01-01	999999999	@	Chilca	Huancayo	Junin	AVENIDA FERROCARRIL	referencia	obser	2020-06-28 11:54:41	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	1	\N	\N	\N
33819551	GARCIA	INUMA	SEGUNDO	MASCULINO 	SOLTERO(A)	2020-01-01	999999999	@	Tingo María	Tingo María	Huánuco	AVENIDA RAYMONDI	referencia	obser	2020-06-28 11:54:41	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	1	\N	\N	\N
41156322	QUISPE	TACUNAN	MARISIELA FELICITA	MASCULINO 	SOLTERO(A)	2020-01-01	999999999	@	Chilca	Huancayo	Junin	CHUPACA - JIRÓN LA UNION  NRO 320	referencia	obser	2020-06-28 11:54:41	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	1	\N	\N	\N
41367618	GUEVARA	HIDALGO	KATHIUSKA	MASCULINO 	SOLTERO(A)	2020-01-01	999999999	@	Tingo María	Tingo María	Huánuco	JIRÓN MONZON  NRO 785	referencia	obser	2020-06-28 11:54:41	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	1	\N	\N	\N
41425554	MALLQUI	ARCOS	GABY	MASCULINO 	SOLTERO(A)	2020-01-01	999999999	@	Tingo María	Tingo María	Huánuco	UNIDAD VECINAL: TUPAC AMARU  - JIRÓN SIMON BOLIVAR  NRO 176	referencia	obser	2020-06-28 11:54:41	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	1	\N	\N	\N
41529651	SEDANO	MUÑOZ	HUBERTH ELDIS	MASCULINO 	SOLTERO(A)	2020-01-01	999999999	@	Chilca	Huancayo	Junin	AVENIDA LOS PROCERES  NRO 394	referencia	obser	2020-06-28 11:54:41	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	1	\N	\N	\N
41668516	DE	LA CRUZ MATOS	HECTOR OVIDIO	MASCULINO 	SOLTERO(A)	2020-01-01	999999999	@	Chilca	Huancayo	Junin	PASAJE PRIMAVERA   NRO 325	referencia	obser	2020-06-28 11:54:41	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	1	\N	\N	\N
41683196	QUISPE	CONDOR	ALICIA	MASCULINO 	SOLTERO(A)	2020-01-01	999999999	@	Chilca	Huancayo	Junin	JIRÓN ORIHUELA  NRO 109	referencia	obser	2020-06-28 11:54:41	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	1	\N	\N	\N
41693322	LAGOS	VALERIO	GLENY LUZ	MASCULINO 	SOLTERO(A)	2020-01-01	999999999	@	Tingo María	Tingo María	Huánuco	AVENIDA LOS CLAVELES  MZ P	referencia	obser	2020-06-28 11:54:41	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	1	\N	\N	\N
42370216	LOPEZ	GUTARRA	TANY MERALY	MASCULINO 	SOLTERO(A)	2020-01-01	999999999	@	Chilca	Huancayo	Junin	URBANIZACIÓN: CHILCA - CALLE REAL  NRO 386	referencia	obser	2020-06-28 11:54:41	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	1	\N	\N	\N
42411266	MATTA	QUISPE	OLGA YOVANA	MASCULINO 	SOLTERO(A)	2020-01-01	999999999	@	Tingo María	Tingo María	Huánuco	CASTILLO GRANDE  - AVENIDA IQUITOS   NRO 104	referencia	obser	2020-06-28 11:54:41	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	1	\N	\N	\N
42475166	PIZANGO	MORALES	PAUL	MASCULINO 	SOLTERO(A)	2020-01-01	999999999	@	Chilca	Huancayo	Junin	ASENTAMIENTO HUMANO: LA LIBERTAD -	referencia	obser	2020-06-28 11:54:41	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	1	\N	\N	\N
42520860	BERAUN	ESPINOZA	SADITH	MASCULINO 	SOLTERO(A)	2020-01-01	999999999	@	Tingo María	Tingo María	Huánuco	CASTILLO GRANDE  -	referencia	obser	2020-06-28 11:54:41	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	1	\N	\N	\N
42636067	JIMENEZ	ROSALES	SEGUNDINA	MASCULINO 	SOLTERO(A)	2020-01-01	999999999	@	Tingo María	Tingo María	Huánuco	JIRÓN LORETO  NRO 861	referencia	obser	2020-06-28 11:54:41	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	1	\N	\N	\N
42967504	GRANADOS	OKAHARA	GERALD EDWARD	MASCULINO 	SOLTERO(A)	2020-01-01	999999999	@	Chilca	Huancayo	Junin	URBANIZACIÓN: HUANCAYO - JIRÓN JOSE GALVES   NRO 0	referencia	obser	2020-06-28 11:54:41	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	1	\N	\N	\N
41274111	ROJAS	VILCAHUAMAN	ORLANDINI	MASCULINO 	SOLTERO(A)	2020-01-01	999999999	@	Chilca	Huancayo	Junin	AVENIDA HEROES DE LA BREÑA  NRO 266	referencia	obser	2020-06-28 11:54:41	HUANCAYO                                          	70	2020-07-01 10:55:03	5	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	1	5	1	\N
41773365	VASQUEZ	QUERRE	LIZ CATHERINE	MASCULINO 	SOLTERO(A)	2020-01-01	999999999	@	Chilca	Huancayo	Junin	JIRÓN ALFONSO UGARTE  NRO 212	referencia	obser	2020-06-28 11:54:41	HUANCAYO                                          	15	2020-06-29 14:39:17	3	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	1	3	1	\N
42831861	GUTIERREZ	MENDOZA	MERCEDES	MASCULINO 	SOLTERO(A)	2020-01-01	999999999	@	Chilca	Huancayo	Junin	PASAJE SANTA ROSA	referencia	obser	2020-06-28 11:54:41	HUANCAYO                                          	20	2020-06-29 14:49:19	3	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	1	3	1	\N
42579842	CHIPANA	ROJAS	ALICIA	MASCULINO 	SOLTERO(A)	2020-01-01	999999999	@	Chilca	Huancayo	Junin	CALLE CEMENTERIO	referencia	obser	2020-06-28 11:54:41	HUANCAYO                                          	29	2020-06-29 14:57:43	3	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	1	3	1	\N
42781214	AVILA	ROJAS	KARINA	MASCULINO 	SOLTERO(A)	2020-01-01	999999999	@	Chilca	Huancayo	Junin	JIRÓN ISPACH	referencia	obser	2020-06-28 11:54:41	HUANCAYO                                          	33	2020-06-29 15:00:42	3	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	1	3	1	\N
40490052	APAZA	CARRION DE NOLAZCO	LICCETH PAOLA	MASCULINO 	SOLTERO(A)	2020-01-01	999999999	@	Chilca	Huancayo	Junin	PASAJE MARISCAL CACERES  NRO 205	referencia	obser	2020-06-28 11:54:41	HUANCAYO                                          	38	2020-06-29 15:07:55	3	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	1	3	1	\N
28281045	HUAMAN	RAMOS	ZENAIDA	MASCULINO 	SOLTERO(A)	2020-01-01	999999999	@	Chilca	Huancayo	Junin	URBANIZACIÓN: PILCOMAYO - JIRÓN CORDOVA  NRO 0	referencia	obser	2020-06-28 11:54:41	HUANCAYO                                          	42	2020-06-29 15:24:34	3	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	1	3	1	\N
42388030	BONIFACIO	SINCHE	EDSON GULLERMO	MASCULINO 	SOLTERO(A)	2020-01-01	999999999	@	Chilca	Huancayo	Junin	AVENIDA PANAMERICANA	referencia	obser	2020-06-28 11:54:41	HUANCAYO                                          	48	2020-06-29 15:27:45	3	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	1	3	1	\N
43185765	ARROYO	ECHABAUDIS	LUCIANA GABRIELA	MASCULINO 	SOLTERO(A)	2020-01-01	999999999	@	Chilca	Huancayo	Junin	CALLE AMAZONAS  NRO 783	referencia	obser	2020-06-28 11:54:41	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	1	\N	\N	\N
44047370	CASAS	ANDRADE	JOEY JOSE LUIS	MASCULINO 	SOLTERO(A)	2020-01-01	999999999	@	Chilca	Huancayo	Junin	JIRÓN PUNO  NRO 391	referencia	obser	2020-06-28 11:54:41	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	1	\N	\N	\N
44051334	DAVALOS	ESPINOZA	CARMEN GENOVEVA	MASCULINO 	SOLTERO(A)	2020-01-01	999999999	@	Chilca	Huancayo	Junin	ASENTAMIENTO HUMANO: BRISAS DEL HUALLAGA -	referencia	obser	2020-06-28 11:54:41	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	1	\N	\N	\N
44107580	PONCE	SABOYA	JUAN CARLOS	MASCULINO 	SOLTERO(A)	2020-01-01	999999999	@	Tingo María	Tingo María	Huánuco	NUEVA ESPERANZA - CARRETERA LA MARGINAL	referencia	obser	2020-06-28 11:54:41	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	1	\N	\N	\N
44320820	MEDINA	CONTRERAS	HILDA BLANCA	MASCULINO 	SOLTERO(A)	2020-01-01	999999999	@	Chilca	Huancayo	Junin	AVENIDA MARISCAL CASTILLA  NRO 1378	referencia	obser	2020-06-28 11:54:41	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	1	\N	\N	\N
44557853	FLORES	VARA	ALEXANDER MARLON	MASCULINO 	SOLTERO(A)	2020-01-01	999999999	@	Tingo María	Tingo María	Huánuco	CALLE QUEBRADA DEL AGUILA  MZ B	referencia	obser	2020-06-28 11:54:41	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	1	\N	\N	\N
44696215	QUISPE	TAIPE	VICTOR ANTONIO	MASCULINO 	SOLTERO(A)	2020-01-01	999999999	@	Chilca	Huancayo	Junin	URBANIZACIÓN: AUQUIMARCA - JIRÓN LAS COLMEDAS   NRO 278	referencia	obser	2020-06-28 11:54:41	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	1	\N	\N	\N
44726276	FABIAN	EUGENIO	KARINA YOVANA	MASCULINO 	SOLTERO(A)	2020-01-01	999999999	@	Chilca	Huancayo	Junin	PUEBLO JOVEN: 9 DE OCTUBRE - MZ C	referencia	obser	2020-06-28 11:54:41	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	1	\N	\N	\N
44816947	RAMIREZ	TERRONES	DANAE FIORELLA	MASCULINO 	SOLTERO(A)	2020-01-01	999999999	@	Tingo María	Tingo María	Huánuco	CASTILLO GRANDE HAYA DE LA TOR -	referencia	obser	2020-06-28 11:54:41	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	1	\N	\N	\N
44846336	CHUQUIMIA	VALDEZ	MARIA DEL CARMEN	MASCULINO 	SOLTERO(A)	2020-01-01	999999999	@	Chilca	Huancayo	Junin	JIRÓN SIN DIRECCION	referencia	obser	2020-06-28 11:54:41	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	1	\N	\N	\N
45145335	SOLDEVILLA	SOLORZANO	MAYRA GLADYS	MASCULINO 	SOLTERO(A)	2020-01-01	999999999	@	Chilca	Huancayo	Junin	AVENIDA AMAZONAS  NRO 7	referencia	obser	2020-06-28 11:54:41	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	1	\N	\N	\N
45354124	DEL AGUILA	ANGULO	FLOR DE MARIA	MASCULINO 	SOLTERO(A)	2020-01-01	999999999	@	Chilca	Huancayo	Junin	JIRÓN LAMAS   NRO 265	referencia	obser	2020-06-28 11:54:41	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	1	\N	\N	\N
45545084	DAMAS	CARHUAMACA	MAURO	MASCULINO 	SOLTERO(A)	2020-01-01	999999999	@	Chilca	Huancayo	Junin	UNIDAD VECINAL: LA PUNTA - CALLE REAL  NRO 450	referencia	obser	2020-06-28 11:54:41	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	1	\N	\N	\N
46000236	LEON	SARAVIA	EVA	MASCULINO 	SOLTERO(A)	2020-01-01	999999999	@	Tingo María	Tingo María	Huánuco	ASENTAMIENTO HUMANO: LOS ROSALES - DPTO I	referencia	obser	2020-06-28 11:54:41	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	1	\N	\N	\N
46262298	CONDORI	CUADROS	CARMEN ISABEL	MASCULINO 	SOLTERO(A)	2020-01-01	999999999	@	Chilca	Huancayo	Junin	CARRETERA GARCIA CALDERON  NRO 272	referencia	obser	2020-06-28 11:54:41	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	1	\N	\N	\N
46286008	PUCHOC	PAUCAR	ELIAZAR	MASCULINO 	SOLTERO(A)	2020-01-01	999999999	@	Chilca	Huancayo	Junin	PASAJE ANTONIO DE ZELA  NRO 205	referencia	obser	2020-06-28 11:54:41	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	1	\N	\N	\N
44729185	MULLISACA	ENCISO	ELIZABETH PAMELA	MASCULINO 	SOLTERO(A)	2020-01-01	999999999	@	Chilca	Huancayo	Junin	PASAJE AUGUSTO PEÑALOZA  NRO 173	referencia	obser	2020-06-28 11:54:41	HUANCAYO                                          	87	2020-07-06 12:19:44	5	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	1	5	1	\N
45890278	ROMERO	SALAZAR	JHONATAN GUSTAVO	MASCULINO 	SOLTERO(A)	2020-01-01	999999999	@	Chilca	Huancayo	Junin	PASAJE TUPAC AMARU  NRO 169	referencia	obser	2020-06-28 11:54:41	HUANCAYO                                          	16	2020-06-29 14:40:39	3	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	1	3	1	\N
43728604	LEON	QUISPE	HECTOR	MASCULINO 	SOLTERO(A)	2020-01-01	999999999	@	Chilca	Huancayo	Junin	OCOPILLA - PASAJE SULLUCHUCO  NRO 130	referencia	obser	2020-06-28 11:54:41	HUANCAYO                                          	22	2020-06-29 14:50:28	3	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	1	3	1	\N
44931763	PEREZ	PADILLA	LOIDA EUNICE	MASCULINO 	SOLTERO(A)	2020-01-01	999999999	@	Chilca	Huancayo	Junin	JIRÓN 28 DE JULIO  NRO 790	referencia	obser	2020-06-28 11:54:41	HUANCAYO                                          	26	2020-06-29 14:55:16	3	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	1	3	1	\N
43910566	MONTERO	SANCHEZ	YULY	MASCULINO 	SOLTERO(A)	2020-01-01	999999999	@	Chilca	Huancayo	Junin	PUEBLO JOVEN: PORVENIR - JIRÓN JUNIN	referencia	obser	2020-06-28 11:54:41	HUANCAYO                                          	36	2020-06-29 15:07:05	3	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	1	3	1	\N
46485289	PIZARRO	PEREZ	IRMA OLGA	MASCULINO 	SOLTERO(A)	2020-01-01	999999999	@	Chilca	Huancayo	Junin	CARRETERA PANAMERICANA	referencia	obser	2020-06-28 11:54:41	HUANCAYO                                          	40	2020-06-29 15:08:43	3	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	1	3	1	\N
44019045	CARDENAS	LUIS	WILDER	MASCULINO 	SOLTERO(A)	2020-01-01	999999999	@	Chilca	Huancayo	Junin	PASAJE SAN SILVESTRE	referencia	obser	2020-06-28 11:54:41	HUANCAYO                                          	52	2020-06-29 15:29:22	3	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	1	3	1	\N
46357683	POMA	BLANCAS	KETY JANET	MASCULINO 	SOLTERO(A)	2020-01-01	999999999	@	Chilca	Huancayo	Junin	JIRÓN CUSCO  NRO 164	referencia	obser	2020-06-28 11:54:41	HUANCAYO                                          	13	2020-06-29 12:55:57	5	0	\N	\N	22	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	1	2	1	11:53:14-05
46196324	CARDENAS	GARCIA	NELLY JUANA	MASCULINO 	SOLTERO(A)	2020-01-01	999999999	@	Chilca	Huancayo	Junin	PASAJE CHALCO  NRO 130	referencia	obser	2020-06-28 11:54:41	HUANCAYO                                          	60	2020-06-29 15:32:39	3	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	1	3	1	\N
46382516	CORDOVA	CURITIMA	KATHY	MASCULINO 	SOLTERO(A)	2020-01-01	999999999	@	Chilca	Huancayo	Junin	JIRÓN CORONEL PORTILLO  NRO 300	referencia	obser	2020-06-28 11:54:41	PUCALLPA                                          	2	2020-07-01 09:51:12	11	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	1	11	3	\N
44326038	QUISPE	VERA	TRINIDAD	MASCULINO 	SOLTERO(A)	2020-01-01	999999999	@	Chilca	Huancayo	Junin	AVENIDA 12 DE OCTUBRE	referencia	obser	2020-06-28 11:54:41	HUANCAYO                                          	71	2020-07-01 11:33:02	5	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	1	5	1	\N
44350264	CHAVEZ	CHIRINOS	JANNETH CECILIA	MASCULINO 	SOLTERO(A)	2020-01-01	999999999	@	Chilca	Huancayo	Junin	JIRÓN ANTONIO DE ZELA  NRO 560	referencia	obser	2020-06-28 11:54:41	HUANCAYO                                          	91	2020-07-07 11:19:37	5	0	\N	\N	32	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	1	5	1	13:23:23-05
44279232	HUANAY	YUPANQUI	JONATHAN	MASCULINO 	SOLTERO(A)	2020-01-01	999999999	@	Chilca	Huancayo	Junin	AVENIDA LA ESPERANZA  NRO 1024	referencia	obser	2020-06-28 11:54:41	HUANCAYO                                          	65	2020-06-30 14:40:28	5	0	\N	\N	46	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	1	5	1	12:53:01-05
46114191	CUHELO	CUHELO	JUNIOR JESUS	MASCULINO 	SOLTERO(A)	1989-04-21	990803228		Manantay	Pucallpa	Ucayali	Asentamiento Humano: Las Vias De La Amistad - Avenida Radar  Mz D	Aa.Hh. Villa De La Amistad Mz. D Lt. 17		2020-06-28 11:54:41	PUCALLPA                                          	66	2020-07-03 11:56:47	11	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	1	11	3	\N
45666252	SEGUIL	RICSE	MIGUEL ANGEL	MASCULINO 	SOLTERO(A)	2020-01-01	999999999	@	Chilca	Huancayo	Junin	JIRÓN CAHUIDE  NRO 168	referencia	obser	2020-06-28 11:54:41	HUANCAYO                                          	76	2020-07-03 11:32:28	5	0	\N	\N	28	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	1	5	1	13:15:07-05
46934472	PONCE	ANGEL	BERONICA	MASCULINO 	SOLTERO(A)	2020-01-01	999999999	@	Chilca	Huancayo	Junin	ASENTAMIENTO HUMANO: SUPTE SAN JORGE -	referencia	obser	2020-06-28 11:54:41	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	1	\N	\N	\N
47060177	VIVAS	RODRIGUES	ROSARIO PILAR	MASCULINO 	SOLTERO(A)	2020-01-01	999999999	@	Chilca	Huancayo	Junin	JIRÓN CORONEL GUERRA  NRO 277	referencia	obser	2020-06-28 11:54:41	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	1	\N	\N	\N
47698356	ACHARTE	PRADO	PETER CHANEL	MASCULINO 	SOLTERO(A)	2020-01-01	999999999	@	Chilca	Huancayo	Junin	PASAJE PIÑA	referencia	obser	2020-06-28 11:54:41	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	1	\N	\N	\N
47709769	JIBAJA	SIMON	MASSIEL	MASCULINO 	SOLTERO(A)	2020-01-01	999999999	@	Chilca	Huancayo	Junin	JIRÓN JOSE OLAYA   NRO 396	referencia	obser	2020-06-28 11:54:41	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	1	\N	\N	\N
47774754	BARBOZA	CHANCA	KETERING MARIBEL	MASCULINO 	SOLTERO(A)	2020-01-01	999999999	@	Chilca	Huancayo	Junin	PUEBLO JOVEN: AZAPAMPA - PASAJE LOS SAUCES  INT AA   MZ P	referencia	obser	2020-06-28 11:54:41	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	1	\N	\N	\N
48534394	TALLA	FASAMANDO	TANIA MARU	MASCULINO 	SOLTERO(A)	2020-01-01	999999999	@	Chilca	Huancayo	Junin	JIRÓN JUNIN  NRO 647	referencia	obser	2020-06-28 11:54:41	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	1	\N	\N	\N
48536196	CHAVEZ	QUITANILLA	JORGE ROMARIO	MASCULINO 	SOLTERO(A)	2020-01-01	999999999	@	Tingo María	Tingo María	Huánuco	AVENIDA JOSE CARLOS MAREATEGUI  NRO 699	referencia	obser	2020-06-28 11:54:41	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	1	\N	\N	\N
70295828	OLIVERA	LIMPE	AMMY MARISELA	MASCULINO 	SOLTERO(A)	2020-01-01	999999999	@	Chilca	Huancayo	Junin	AVENIDA REAL  NRO 967	referencia	obser	2020-06-28 11:54:41	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	1	\N	\N	\N
70649531	ARAUCO	CARTOLIN	ANTHONY DIEGO	MASCULINO 	SOLTERO(A)	2020-01-01	999999999	@	Chilca	Huancayo	Junin	CALLE OSWALDO BARRETO  SIGLO XX  NRO 101	referencia	obser	2020-06-28 11:54:41	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	1	\N	\N	\N
70872313	RAFAEL	ARROYO	LUCIO SERGIO	MASCULINO 	SOLTERO(A)	2020-01-01	999999999	@	Chilca	Huancayo	Junin	UNIDAD VECINAL: SAN AGUSTIN CAJAS - JIRÓN JOSE MARIA PAREDES  INT 0	referencia	obser	2020-06-28 11:54:41	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	1	\N	\N	\N
71058006	ISMINIO	FASANANDO	ALESKI SARAY	MASCULINO 	SOLTERO(A)	2020-01-01	999999999	@	Tingo María	Tingo María	Huánuco	NARANJILLO - CARRETERA INTEROCEANICA  KM 6	referencia	obser	2020-06-28 11:54:41	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	1	\N	\N	\N
71270784	ROCA	BAUTIZTA	CLAUDIO	MASCULINO 	SOLTERO(A)	2020-01-01	999999999	@	Chilca	Huancayo	Junin	AVENIDA TORRE TAGLE	referencia	obser	2020-06-28 11:54:41	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	1	\N	\N	\N
71316260	FERNANDEZ	BONIFACIO	EMERSON MIKOL	MASCULINO 	SOLTERO(A)	2020-01-01	999999999	@	Chilca	Huancayo	Junin	AVENIDA PANAMERICANA	referencia	obser	2020-06-28 11:54:41	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	1	\N	\N	\N
72100343	BECERRA	PORRAS	JIDITH KARINA	MASCULINO 	SOLTERO(A)	2020-01-01	999999999	@	Chilca	Huancayo	Junin	HUANCAYO - PASAJE ANDRES TUMA	referencia	obser	2020-06-28 11:54:41	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	1	\N	\N	\N
72461828	YSLA	BAZAN	AYRTON	MASCULINO 	SOLTERO(A)	2020-01-01	999999999	@	Tingo María	Tingo María	Huánuco	AVENIDA ALAMEDA	referencia	obser	2020-06-28 11:54:41	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	1	\N	\N	\N
74154713	BARRIAL	CALDAS	WILVER	MASCULINO 	SOLTERO(A)	2020-01-01	999999999	@	Chilca	Huancayo	Junin	CALLE RIO ALI  NRO 318	referencia	obser	2020-06-28 11:54:41	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	1	\N	\N	\N
74221628	QUICUNA	GALLARDO	JAIR FERNANDO	MASCULINO 	SOLTERO(A)	2020-01-01	999999999	@	Tingo María	Tingo María	Huánuco	AAHH  - SANTA ROSA   NRO 15	referencia	obser	2020-06-28 11:54:41	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	1	\N	\N	\N
76009062	QUISPE	EUGENIO	MEISY LORENA	MASCULINO 	SOLTERO(A)	2020-01-01	999999999	@	Chilca	Huancayo	Junin	PUEBLO JOVEN: 9 DE OCTUBRE - MZ C	referencia	obser	2020-06-28 11:54:41	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	1	\N	\N	\N
76830854	ESTRADA	HUARI	GINA ANGELICA	MASCULINO 	SOLTERO(A)	2020-01-01	999999999	@	Chilca	Huancayo	Junin	URBANIZACIÓN: CHILCA - JIRÓN AUQUIMARCA  NRO 725	referencia	obser	2020-06-28 11:54:41	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	1	\N	\N	\N
76847697	DAMAS	HUAROC	JHADIRA YESLY	MASCULINO 	SOLTERO(A)	2020-01-01	999999999	@	Chilca	Huancayo	Junin	PASAJE HUALLAGA   NRO 152	referencia	obser	2020-06-28 11:54:41	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	1	\N	\N	\N
73054005	MANRIQUE	VARILLAS	NORMAN SILVESTRE	MASCULINO 	SOLTERO(A)	2020-01-01	999999999	@	Chilca	Huancayo	Junin	AVENIDA HVCA  SECTOR 2  NRO 1340	referencia	obser	2020-06-28 11:54:41	HUANCAYO                                          	66	2020-06-30 15:11:39	5	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	1	5	1	\N
76290455	CONDORI	ATAUCUSI	JHONY	MASCULINO 	SOLTERO(A)	2020-01-01	999999999	@	Chilca	Huancayo	Junin	JIRÓN PILAR PAUCARCHUCO	referencia	obser	2020-06-28 11:54:41	HUANCAYO                                          	12	2020-06-29 12:55:36	5	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	1	2	1	\N
48142577	MARTEL	LIVIAS	MILAGROS ISABEL	MASCULINO 	SOLTERO(A)	2020-01-01	999999999	@	Chilca	Huancayo	Junin	JIRÓN SIN DIRECCION	referencia	obser	2020-06-28 11:54:41	HUANCAYO                                          	24	2020-06-29 14:52:16	3	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	1	3	1	\N
48059190	TICSE	CAMPOS	LUCERO STEFANY	MASCULINO 	SOLTERO(A)	2020-01-01	999999999	@	Chilca	Huancayo	Junin	URBANIZACIÓN: CHILCA - JIRÓN GENERAL GAMARRA  NRO 1113	referencia	obser	2020-06-28 11:54:41	HUANCAYO                                          	25	2020-06-29 14:54:05	3	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	1	3	1	\N
47858782	CAMAYO	MOLINA	DEIVES PERCY	MASCULINO 	SOLTERO(A)	2020-01-01	999999999	@	Chilca	Huancayo	Junin	JIRÓN ALEXANDER FLEMING	referencia	obser	2020-06-28 11:54:41	HUANCAYO                                          	97	2020-07-10 14:49:58	5	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	1	5	1	\N
47420055	CAPCHA	CORONACION	ALICIA	MASCULINO 	SOLTERO(A)	2020-01-01	999999999	@	Chilca	Huancayo	Junin	AVENIDA PROCERES	referencia	obser	2020-06-28 11:54:41	HUANCAYO                                          	43	2020-06-29 15:25:07	3	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	1	3	1	\N
70511276	CONOVILCA	DAMAS	ISSAMAR SORAIDA	MASCULINO 	SOLTERO(A)	2020-01-01	999999999	@	Chilca	Huancayo	Junin	JIRÓN SIN DIRECCION	referencia	obser	2020-06-28 11:54:41	HUANCAYO                                          	53	2020-06-29 15:29:40	3	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	1	3	1	\N
71544203	FABIAN	CARDENAS	LISBETH VANESSA	MASCULINO 	SOLTERO(A)	2020-01-01	999999999	@	Chilca	Huancayo	Junin	JIRÓN FRANCISCO TOLEDO  NRO 610	referencia	obser	2020-06-28 11:54:41	HUANCAYO                                          	57	2020-06-29 15:31:25	3	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	1	3	1	\N
46996710	MATAMOROS	LIZANA	SAMUEL EDILBERTO	MASCULINO 	SOLTERO(A)	2020-01-01	999999999	@	Chilca	Huancayo	Junin	PASAJE SAN SILVESTRE	referencia	obser	2020-06-28 11:54:41	HUANCAYO                                          	61	2020-06-29 15:33:00	3	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	1	3	1	\N
63693045	TELLO	CAMPOS	JESSICA LUCERO	FEMENINO  	SOLTERO(A)	1998-04-30	975843037		Chilca	Huancayo	Junin	Jirón Serafin Filomeno	Costado Del Estadio De Pucallpa		2020-06-28 11:54:41	PUCALLPA                                          	9	2020-07-01 18:51:54	11	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	1	11	3	\N
47846248	CASTRO	INGARUCA	PILAR ROSARIO	MASCULINO 	SOLTERO(A)	2020-01-01	999999999	@	Chilca	Huancayo	Junin	AVENIDA AV UNIVERSITARIA   NRO 0	referencia	obser	2020-06-28 11:54:41	HUANCAYO                                          	80	2020-07-03 13:47:53	5	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	1	2	1	\N
70020689	MAYTA	ORDOÑEZ	WENDY MERCEDES	MASCULINO 	SOLTERO(A)	2020-01-01	999999999	@	Chilca	Huancayo	Junin	CARRETERA MICAELA BASTIDAS   NRO 132	referencia	obser	2020-06-28 11:54:41	HUANCAYO                                          	83	2020-07-03 14:39:54	5	0	\N	\N	48	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	1	5	1	09:34:27-05
76969527	YALO	PARIONA	MARISELA ESTHEFANI	MASCULINO 	SOLTERO(A)	2020-01-01	999999999	@	Chilca	Huancayo	Junin	PASAJE 5 DE OCTUBRE  NRO 154	referencia	obser	2020-06-28 11:54:41	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	1	\N	\N	\N
78470302	ENCARNACION	MENDOZA	FIORELLA ELIZABETH	MASCULINO 	SOLTERO(A)	2020-01-01	999999999	@	Chilca	Huancayo	Junin	TINGO MARIA - MALECÓN LIMA  NRO 111	referencia	obser	2020-06-28 11:54:41	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	1	\N	\N	\N
80007160	AGUIRRE	SERRANO	MARDIN	MASCULINO 	SOLTERO(A)	2020-01-01	999999999	@	Chilca	Huancayo	Junin	CALLE REAL  NRO 648	referencia	obser	2020-06-28 11:54:41	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	1	\N	\N	\N
80445920	SANTIAGO	MATEO	DORIS	MASCULINO 	SOLTERO(A)	2020-01-01	999999999	@	Chilca	Huancayo	Junin	AVENIDA TITO JAIME  NRO 02	referencia	obser	2020-06-28 11:54:41	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	1	\N	\N	\N
80634363	ROJAS	ESPINOZA	RUTH ELIANA	MASCULINO 	SOLTERO(A)	2020-01-01	999999999	@	Chilca	Huancayo	Junin	JIRÓN PORVENIR	referencia	obser	2020-06-28 11:54:41	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	1	\N	\N	\N
19940774	SIERRA	BARRIENTOS	ZOZIMO	MASCULINO 	SOLTERO(A)	2020-01-01	999999999	@	Chilca	Huancayo	Junin	JIRÓN AMAZONAS  NRO 380	referencia	obser	2020-06-28 11:54:41	HUANCAYO                                          	2	2020-06-29 10:31:43	5	0	\N	\N	4	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	1	2	1	12:13:01-05
41738167	MEDINA	RIVEROS	RUBEN EFRAIN	MASCULINO 	SOLTERO(A)	2020-01-01	999999999	@	Chilca	Huancayo	Junin	SAPALLANGA - JIRÓN PUEBLO DE SAPALLANGA  NRO 0	referencia	obser	2020-06-28 11:54:41	HUANCAYO                                          	18	2020-06-29 14:46:43	3	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	1	3	1	\N
60064606	MENDEZ	BAUTISTA	BEATRIZ JERMINA	MASCULINO 	SOLTERO(A)	2020-01-01	999999999	@	Chilca	Huancayo	Junin	JIRÓN MARISCAL	referencia	obser	2020-06-28 11:54:41	HUANCAYO                                          	19	2020-06-29 14:48:02	3	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	1	3	1	\N
20099395	TOMAS	GONZALES	TITO CIRILO	MASCULINO 	SOLTERO(A)	2020-01-01	999999999	@	Chilca	Huancayo	Junin	URBANIZACIÓN: CHILCA - JIRÓN ANCASH  NRO 324   DPTO FUND	referencia	obser	2020-06-28 11:54:41	HUANCAYO                                          	23	2020-06-29 14:51:14	3	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	1	3	1	\N
46617080	CABRERA	GOMEZ	OLIVIA	MASCULINO 	SOLTERO(A)	2020-01-01	999999999	@	Chilca	Huancayo	Junin	AVENIDA CHINCHICANCHA  NRO 175	referencia	obser	2020-06-28 11:54:41	HUANCAYO                                          	28	2020-06-29 14:56:47	3	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	1	3	1	\N
61895819	HIDROGO	PEREZ	FELIPE BRYAN	MASCULINO 	SOLTERO(A)	2020-01-01	999999999	@	Chilca	Huancayo	Junin	JIRÓN 28 DE JULIO  NRO 790	referencia	obser	2020-06-28 11:54:41	HUANCAYO                                          	30	2020-06-29 14:58:27	3	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	1	3	1	\N
42780391	GUZMAN	CORDOVA	JESSBY CRISTINA	MASCULINO 	SOLTERO(A)	2020-01-01	999999999	@	Chilca	Huancayo	Junin	AVENIDA FERROCARRIL  NRO 5157	referencia	obser	2020-06-28 11:54:41	HUANCAYO                                          	31	2020-06-29 14:59:20	3	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	1	3	1	\N
77535066	MANUELO	TITO	JUDITH KARINA	MASCULINO 	SOLTERO(A)	2020-01-01	999999999	@	Chilca	Huancayo	Junin	AVENIDA HURACAN	referencia	obser	2020-06-28 11:54:41	HUANCAYO                                          	34	2020-06-29 15:05:40	3	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	1	3	1	\N
40129120	CARRIZALES	ROJAS	JORGE	MASCULINO 	SOLTERO(A)	1976-09-04	966562400		Sapllanga	Huancayo	Junin	Av. Peñaloza	A Unua Cuadra De La Loza Peñaloza	No	2020-06-29 00:01:22.427868	HUANCAYO                                          	1	2020-06-29 00:02:28	3	0	\N	\N	2	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2	2	1	16:11:29-05
44974563	SEGOVIA	BARBOZA	DENISSA ZOILA	MASCULINO 	SOLTERO(A)	2020-01-01	999999999	@	Chilca	Huancayo	Junin	PASAJE TUNALES  NRO 118	referencia	obser	2020-06-28 11:54:41	HUANCAYO                                          	3	2020-06-29 12:02:48	5	0	\N	\N	5	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	1	2	1	12:28:39-05
46041015	PEREZ	DE LA CRUZ	AMERICA NELFA	MASCULINO 	SOLTERO(A)	2020-01-01	999999999	@	Chilca	Huancayo	Junin	PASAJE AGUIRRE	referencia	obser	2020-06-28 11:54:41	HUANCAYO                                          	4	2020-06-29 12:51:56	5	0	\N	\N	6	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	1	2	1	12:30:44-05
20113864	ATENCIO	SANCHEZ	ROSA LUZ	MASCULINO 	SOLTERO(A)	2020-01-01	999999999	@	Chilca	Huancayo	Junin	PASAJE VILCAHUAMAN  NRO 169	referencia	obser	2020-06-28 11:54:41	HUANCAYO                                          	5	2020-06-29 12:52:29	5	0	\N	\N	7	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	1	2	1	12:31:30-05
42297478	ROCA	COCA	MIGUEL ANGEL	MASCULINO 	SOLTERO(A)	2020-01-01	999999999	@	Chilca	Huancayo	Junin	CARRETERA PACHITEHA	referencia	obser	2020-06-28 11:54:41	HUANCAYO                                          	7	2020-06-29 12:53:54	5	0	\N	\N	8	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	1	2	1	12:32:35-05
41123341	JEREMIAS	CALLUPE	NATALY MAGALY	MASCULINO 	SOLTERO(A)	2020-01-01	999999999	@	Chilca	Huancayo	Junin	JIRÓN LOS RETAMAS  NRO 924	referencia	obser	2020-06-28 11:54:41	HUANCAYO                                          	9	2020-06-29 12:54:28	5	0	\N	\N	9	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	1	2	1	12:33:46-05
45745478	CASTILLON	ROMAN	ROSA	MASCULINO 	SOLTERO(A)	2020-01-01	999999999	@	Chilca	Huancayo	Junin	PASAJE MIGUEL GRAU	referencia	obser	2020-06-28 11:54:41	HUANCAYO                                          	21	2020-06-29 14:49:51	3	0	\N	\N	13	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	1	3	1	14:45:07-05
80145065	PALOMINO	RIOS	FLORA MERY	MASCULINO 	SOLTERO(A)	2020-01-01	999999999	@	Chilca	Huancayo	Junin	PASAJE VILCAHUAMAN  NRO 170	referencia	obser	2020-06-28 11:54:41	HUANCAYO                                          	68	2020-06-30 15:58:52	5	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	1	5	1	\N
40893286	PORTA	CHUQUILLANQUI	JANETH BIVIANA	MASCULINO 	SOLTERO(A)	2020-01-01	999999999	@	Chilca	Huancayo	Junin	PUEBLO JOVEN: HUANCAN - CALLE HUANCAYO	referencia	obser	2020-06-28 11:54:41	HUANCAYO                                          	41	2020-06-29 15:09:05	3	0	\N	\N	19	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	1	3	1	10:13:50-05
80131906	MITIVIRI	SHAPIAMA	ELIZABETH	FEMENINO  	SOLTERO(A)	1977-06-07	966234933		Calleria	Pucallpa	Ucayali	Aa.Hh. Sgrado Corozon De Jesus - Jr. Misti Mz. J Lt. 7	A Espaldas De La Jungla De Boris		2020-06-28 11:54:41	PUCALLPA                                          	26	2020-07-02 10:30:56	11	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	1	11	3	\N
80245301	ROMERO	ESPIRITU	VIRGINIA LUZ	MASCULINO 	SOLTERO(A)	2020-01-01	999999999	@	Chilca	Huancayo	Junin	JIRÓN LOS HUANCAS  NRO 15	referencia	obser	2020-06-28 11:54:41	HUANCAYO                                          	88	2020-07-06 13:08:25	5	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	1	5	1	\N
19996893	MARQUINA	JULCA	ANA LUISA	MASCULINO 	SOLTERO(A)	2020-01-01	999999999	@	Chilca	Huancayo	Junin	AVENIDA LEONCIO PRADO  NRO 1015	referencia	obser	2020-06-28 11:54:41	HUANCAYO                                          	39	2020-06-29 15:08:22	3	0	\N	\N	27	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	1	3	1	13:13:45-05
71872891	ACOSTA	GASPAR	MARINA	MASCULINO 	SOLTERO(A)	2020-01-01	999999999	@	Chilca	Huancayo	Junin	JIRÓN PEDRO PERALTA   NRO 563	referencia	obser	2020-06-28 11:54:41	HUANCAYO                                          	8	2020-06-29 12:54:10	5	0	\N	\N	31	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	1	2	1	13:19:47-05
46657916	SEDANO	LIZANA	LUZ ANGELA	MASCULINO 	SOLTERO(A)	2020-01-01	999999999	@	Chilca	Huancayo	Junin	PASAJE SAN SILVESTRE	referencia	obser	2020-06-28 11:54:41	HUANCAYO                                          	44	2020-06-29 15:26:10	3	0	\N	\N	37	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	1	3	1	13:34:03-05
70437871	FABIAN	ARANDA	ERWIN DONALD	MASCULINO 	SOLTERO(A)	2020-01-01	999999999	@	Chilca	Huancayo	Junin	JIRÓN CUZCO   NRO 857	referencia	obser	2020-06-28 11:54:41	HUANCAYO                                          	47	2020-06-29 15:27:21	3	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	1	3	1	\N
80401963	GONZALES	MENDOZA	NILO DAVID	MASCULINO 	SOLTERO(A)	2020-01-01	999999999	@	Chilca	Huancayo	Junin	AVENIDA 9 DE DICIEMBRE  NRO 678	referencia	obser	2020-06-28 11:54:41	HUANCAYO                                          	51	2020-06-29 15:28:55	3	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	1	3	1	\N
43568148	YUPANQUI	COTERA	ERIKA	MASCULINO 	SOLTERO(A)	2020-01-01	999999999	@	Chilca	Huancayo	Junin	JIRÓN MILLER  NRO 120	referencia	obser	2020-06-28 11:54:41	HUANCAYO                                          	55	2020-06-29 15:30:35	3	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	1	3	1	\N
20065148	CAPACYACHI	PORRAS	BANZER PAUL	MASCULINO 	SOLTERO(A)	2020-01-01	999999999	@	Chilca	Huancayo	Junin	CARRETERA MANTARO  NRO 583	referencia	obser	2020-06-28 11:54:41	HUANCAYO                                          	59	2020-06-29 15:32:18	3	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	1	3	1	\N
46174277	TICSE	PAUCAR	CARLOS GUSTAVO	MASCULINO 	SOLTERO(A)	2020-06-29	943497061		Huancán	Huancayo	Junin	Jr. Junin S/N	Loza Union		2020-06-29 15:41:53.531557	HUANCAYO                                          	62	2020-06-29 15:42:47	3	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	3	3	1	\N
76621787	MESIAS	ALEGRIA	FLOR DE JESUS	MASCULINO 	SOLTERO(A)	2020-01-01	999999999	@	Chilca	Huancayo	Junin	JIRÓN MALECON  NRO 731	referencia	obser	2020-06-28 11:54:41	HUANCAYO                                          	6	2020-06-29 12:52:53	5	0	\N	\N	1	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	1	2	1	16:00:09-05
23701995	SOTO	HUARCAHUAMAN	JUANA	MASCULINO 	SOLTERO(A)	2020-01-01	999999999	@	Chilca	Huancayo	Junin	CALLE 31 DE OCTUBRE  HUANCAN	referencia	obser	2020-06-28 11:54:41	HUANCAYO                                          	14	2020-06-29 12:56:18	5	0	\N	\N	3	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	1	2	1	10:56:09-05
47475777	FLORES	MONTES	JULIO TOMAS	MASCULINO 	SOLTERO(A)	2020-01-01	999999999	@	Chilca	Huancayo	Junin	JIRÓN LOS CLAVELES  NRO 685	referencia	obser	2020-06-28 11:54:41	HUANCAYO                                          	11	2020-06-29 12:55:17	5	0	\N	\N	10	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	1	2	1	12:34:21-05
46561831	CAYSAHUANA	JACOBE	ELIZABETH DORIS	MASCULINO 	SOLTERO(A)	2020-01-01	999999999	@	Chilca	Huancayo	Junin	PASAJE COMUN	referencia	obser	2020-06-28 11:54:41	HUANCAYO                                          	54	2020-06-29 15:30:08	3	0	\N	\N	11	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	1	3	1	14:39:01-05
19942242	ROJAS	TOVAR	FILOMENA	MASCULINO 	SOLTERO(A)	2020-01-01	999999999	@	Chilca	Huancayo	Junin	UNIDAD VECINAL: HUANCAN - AVENIDA PANAMERICANA  NRO 580	referencia	obser	2020-06-28 11:54:41	HUANCAYO                                          	49	2020-06-29 15:28:04	3	0	\N	\N	12	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	1	3	1	14:40:15-05
05213767	MONROY	CANAYO	ROBINSON EMILIO	MASCULINO 	CASADO(A)	1963-05-18	920718113		Calleria	Pucallpa	Ucayali	Asentamiento Humano: Nuevo Paraiso - Pasaje Poma Rosa  Mz I Lt . 18	Por Recepciones Machaca 		2020-06-28 11:54:41	PUCALLPA                                          	4	2020-07-01 11:36:30	11	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	1	11	3	\N
00093054	CORAL	REATEGUI	FELISINDA	FEMENINO  	CASADO(A)	1971-05-19	920718113		Chupaca	Chupaca	Junin	Aa.Hh Nuevo Páraiso . Pasaje Poma Rosa Mz. I  Lt. 18	Espalda De La Discoteca Gitanos 		2020-07-01 21:01:57.67153	PUCALLPA                                          	15	2020-07-01 21:02:10	11	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	11	11	3	\N
42223746	VALDERRAMA	RAMIREZ	RONNY	MASCULINO 	SOLTERO(A)	1984-02-03	991208288		Calleria	Pucallpa	Ucayali	Jr. Espinar Mz.157 Lt. 24	Primera Casona Del Mercado La Hoyada Ala Derecha		2020-07-01 11:24:59.700617	PUCALLPA                                          	3	2020-07-01 11:26:00	11	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	11	11	3	\N
00098119	URQUIA	LOPEZ	ROSA LUCIA	FEMENINO  	SOLTERO(A)	1972-08-30	995281103		Calleria	Pucallpa	Ucayali	Asentamiento Humano: Masisea - Avenida Jhon F Kenedy 730 	Por El Mercado De Los Cunchis Jr. Ayacuch.		2020-06-28 11:54:41	PUCALLPA                                          	1	2020-06-30 23:08:39	8	0	\N	\N	1	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	1	8	3	09:58:29-05
71340438	RIOS	PEREZ	RAY NILK	MASCULINO 	SOLTERO(A)	2020-01-01	999999999		Calleria	Pucallpa	Ucayali	Jirón Atahualpa 178  Nro 178	Referencia	Obser	2020-06-28 11:54:41	PUCALLPA                                          	5	2020-07-01 18:49:33	11	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	1	11	3	\N
00054338	DEL AGUILA	VASQUEZ	EDUARDO GOMER	MASCULINO 	SOLTERO(A)	1966-01-02	975320048		Calleria	Pucallpa	Ucayali	Jr. 3 De Octubre Con Los Olmos Mz. Fl Lt. Fm	Por La Iglesia "La Morada"		2020-07-01 22:37:01.978644	PUCALLPA                                          	17	2020-07-01 22:37:24	11	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	11	11	3	\N
00061656	VELA	CORAL	GILBER ROBELTEL	MASCULINO 	CASADO(A)	1958-11-17	961671563		Calleria	Pucallpa	Ucayali	Psj.General Montero N°128	 Entru  Jr. Sucre Y  Av. San Martin  Ultima Cuadra		2020-07-01 18:57:54.429723	PUCALLPA                                          	12	2020-07-01 18:58:26	11	0	\N	\N	2	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	11	11	3	19:39:19-05
48742387	FUCHS	VELA	ISABEL	MASCULINO 	CONVIVIENTE	1988-10-06	938243875		Yarinacohca	Pucallpa	Ucayali	Aa.Hh Nuevo Jerusalen . Jr. San Lorenzo Mz.A Lt.2	A Espaldas Del Colegio Jose Olaya		2020-07-01 20:55:02.294919	PUCALLPA                                          	14	2020-07-01 20:55:36	11	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	11	11	3	\N
20064201	MOLINA	QUISPE	JOEL	MASCULINO 	SOLTERO(A)	2020-01-01	999999999	@	Chilca	Huancayo	Junin	UNIDAD VECINAL: HUANCAN - JIRÓN PORVENIR	referencia	obser	2020-06-28 11:54:41	HUANCAYO                                          	67	2020-06-30 15:29:37	5	0	\N	\N	17	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	1	5	1	14:37:34-05
80304182	TEAGUA	SALAS	TANI	FEMENINO  	SOLTERO(A)	1975-12-08	901527160		Chilca	Huancayo	Junin	Asentamiento Humano: Caoba - Jirón Caoba  Mz A Lt.10	Ingresando Por La Av. Aviacion		2020-06-28 11:54:41	PUCALLPA                                          	6	2020-07-01 18:50:27	11	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	1	11	3	\N
43525113	ABUNDO	PEÑA	KAREN MARILYN	FEMENINO  	SOLTERO(A)	1986-04-16	-		Yarinacohca	Pucallpa	Ucayali	Asentamiento Humano: Los Rosales - Jirón Vargas Guerra  Mz 2 Lt.7	Costado De Polleria Pardo´S		2020-06-28 11:54:41	PUCALLPA                                          	8	2020-07-01 18:51:40	11	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	1	11	3	\N
47795561	SABOYA	AHUANARI	MARVIN HAGLER	MASCULINO 	CONVIVIENTE	1989-12-09	995265561		Yarinacohca	Pucallpa	Ucayali	Jr. 3 De Octubre Mz. Fl Lt.Fm	De La Iglesia "La Morada " A 3 Casas		2020-07-01 22:30:18.849305	PUCALLPA                                          	16	2020-07-01 22:30:36	11	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	11	11	3	\N
20107243	BORJA	QUISPE	LIZ	FEMENINO  	CONVIVIENTE	1977-07-08	964966313	-	Chilca	Huancayo	Junin	Av. Porvenir	Poe La Loza Deportiva Del Barrio Porvenir	Ninguna	2020-07-02 09:17:51.131725	HUANCAYO                                          	\N	\N	\N	\N	\N	\N	14	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2	\N	1	09:33:15-05
80443299	AHUANARI	VARGAS	ROSA	FEMENINO  	CASADO(A)	1970-01-08	978216658		Calleria	Pucallpa	Ucayali	Jr. 3 De Octubre Mz. Fl Lt. Fm	Por La Iglesia "La Morada"		2020-07-02 09:41:55.593317	PUCALLPA                                          	18	2020-07-02 09:42:46	11	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	11	11	3	\N
42148384	SAJAMI	VASQUEZ	ERIKA LUCIA	FEMENINO  	CONVIVIENTE	1983-12-25	961597641		Calleria	Pucallpa	Ucayali	Jr. Los Frutales Mz. 358 Lt. 2	Costado Del Paredero A Campo Verde		2020-07-01 20:51:18.697907	PUCALLPA                                          	13	2020-07-01 20:51:50	11	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	11	11	3	\N
00088264	CRUZ	MAGALLANES	NEYVA	FEMENINO  	SOLTERO(A)	1969-04-25	978143856		Calleria	Pucallpa	Ucayali	Aa.Hh. 28 De  Marzo Mz. F Lt. 8  / Jr. Mariano Melgar	Costado De Una  Bodeguita		2020-07-02 09:48:27.625843	PUCALLPA                                          	19	2020-07-02 09:48:48	11	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	11	11	3	\N
00011627	FLORES	DIEGO	MARTHA ELENA	FEMENINO  	SOLTERO(A)	1965-06-14	979459327		Yarinacohca	Pucallpa	Ucayali	Jr. Mario Doley Mz.279 Lt. 7	Altura "Sakura"		2020-07-02 09:53:45.067446	PUCALLPA                                          	20	2020-07-02 09:54:06	11	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	11	11	3	\N
40001397	CARRERA	PANDURO	EDITH	FEMENINO  	CASADO(A)	1978-08-24	958453231		Calleria	Pucallpa	Ucayali	Aa.Hh. 10 De Marzo -Jr. La Molina Mz. G Lt. 12 	Por Ferretria "Emota"		2020-07-02 09:56:57.34009	PUCALLPA                                          	21	2020-07-02 09:57:12	11	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	11	11	3	\N
80487983	LOZANO	VELA	MARIA ISOLINA	FEMENINO  	CASADO(A)	1975-06-12	916771283		Calleria	Pucallpa	Ucayali	Aa.Hh Buenos Aires - La Marina Mz.3 Lt. 3	A Espaldas Del Aeropuerto		2020-07-02 10:00:35.564147	PUCALLPA                                          	22	2020-07-02 10:01:02	11	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	11	11	3	\N
40410005	ESPINOZA	PAREDES	MARIA LUZ	FEMENINO  	CASADO(A)	1979-05-31	947610821		Calleria	Pucallpa	Ucayali	Jr. Salvador Allender N°225	Frente A La Sunat		2020-07-02 10:06:02.786055	PUCALLPA                                          	23	2020-07-02 10:06:18	11	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	11	11	3	\N
71004511	SANCHEZ	VASQUEZ	PETER ESTARLEY	MASCULINO 	SOLTERO(A)	1994-03-12	916748223		Calleria	Pucallpa	Ucayali	Aa.Hh. Fraternidad - Calle Rio San Alejandro Lt. 1 Mz. 39	A Espaldas De Botica "Cosise"		2020-07-02 10:25:37.026077	PUCALLPA                                          	25	2020-07-02 10:26:04	11	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	11	11	3	\N
21148353	VADERRAMA	RIOS	MARIA	FEMENINO  	SOLTERO(A)	1969-11-01	945252796		Calleria	Pucallpa	Ucayali	Jr. Moquegua N°255	Casa Con Bodega - Frente A Casa De Dos Pisos		2020-07-02 10:35:53.329647	PUCALLPA                                          	27	2020-07-02 10:36:42	11	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	11	11	3	\N
08154317	DAVILA	GEVERA	ORFELINDA	FEMENINO  	CONVIVIENTE	1973-09-23	-		Calleria	Pucallpa	Ucayali	 Jr. Tupac Amaru Mercado Mayorisdta N°6	Seccion Productores - Ventas De Frutas		2020-07-02 10:42:50.347984	PUCALLPA                                          	28	2020-07-02 10:43:02	11	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	11	11	3	\N
44231768	RIOS	TUANAMA	VIVIAN	FEMENINO  	CONVIVIENTE	1987-01-02	946051575		Calleria	Pucallpa	Ucayali	Aa.Hh. Jaime Yoshiyama	Frente Ala Canchita De Jaume Yoshiyama		2020-07-02 10:48:20.179203	PUCALLPA                                          	29	2020-07-02 10:48:33	11	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	11	11	3	\N
05392064	RODRIGUEZ	MACUYAMA	RAUL	MASCULINO 	CONVIVIENTE	1961-03-23	9424008706		Calleria	Pucallpa	Ucayali	Aa.Hh Portocarrero . Jr.Las Vegas Mz. 34 Lt. 18	Frente A Una Bodega		2020-07-02 13:51:07.890994	PUCALLPA                                          	32	2020-07-02 13:51:24	11	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	11	11	3	\N
19856332	VILLANES	REMUZGO	ELISEO ELEDINO	MASCULINO 	SOLTERO(A)	1963-04-16	980191253		El Tambo	Huancayo	Junin	Calle Santa Rosa Mz. C Lt N°09	Calle Ferrocarril	Cliente Activo	2020-07-02 11:50:54.151297	HUANCAYO                                          	72	2020-07-02 11:51:15	3	0	\N	\N	15	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	3	3	1	11:57:06-05
20648525	REMUZGO VDA DE VILLANES	VELA	OLGA VICTORIA	FEMENINO  	SOLTERO(A)	2020-07-02	980191253		El Tambo	Huancayo	Junin	Calle Santa Rosa Mz C Lt N°09	Calle Ferrocarril	Cliente Activo	2020-07-02 11:53:17.256348	HUANCAYO                                          	73	2020-07-02 11:54:59	3	0	\N	\N	16	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	3	3	1	12:00:06-05
00016999	SERRUCHE	RUIZ	LILIAN MERCEDES	FEMENINO  	CONVIVIENTE	1959-10-14	973951576		Calleria	Pucallpa	Ucayali	Av. 1 De Mayo N°670 - Micaela Bastidas	Por Elt Anque De Agua		2020-07-02 13:03:41.013424	PUCALLPA                                          	30	2020-07-02 13:03:59	11	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	11	11	3	\N
05851902	ALAVA	RENGIFO	SIMONA	FEMENINO  	CONVIVIENTE	1956-04-21	920574016		Yarinacohca	Pucallpa	Ucayali	Jr. Ucayali / La Selva	Del Grifp "Gs" Ala Derecha		2020-07-02 13:13:27.940715	PUCALLPA                                          	31	2020-07-02 13:14:03	11	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	11	11	3	\N
00129259	RUIZ	VELA	TOMAS	MASCULINO 	SOLTERO(A)	1978-09-23	9388885982		Calleria	Pucallpa	Ucayali	Jr. Fortaleza Mz. 5 Lt.8	Frente A La Iglesia "Adventista"		2020-07-02 14:02:27.132629	PUCALLPA                                          	33	2020-07-02 14:02:49	11	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	11	11	3	\N
48826249	VASQUEZ	TARICUARIMA	SERGIO	MASCULINO 	SOLTERO(A)	1966-11-14	962019868		Calleria	Pucallpa	Ucayali	Av. 3 De Abril N°560	Frente A La Posta "Micaela"		2020-07-02 14:16:10.928989	PUCALLPA                                          	34	2020-07-02 14:16:46	11	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	11	11	3	\N
46202948	CAMPOS	SANCHEZ	CARMEN ANGELICA	FEMENINO  	CONVIVIENTE	1973-02-18	998428988		Calleria	Pucallpa	Ucayali	Jr. Zavala N°558	Del Colegio Ex Aplicaion A Media Cuadra		2020-07-02 20:30:34.209545	PUCALLPA                                          	35	2020-07-02 20:30:58	11	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	11	11	3	\N
40096014	CRISPIN	LINO	ELI	MASCULINO 	SOLTERO(A)	1978-07-28	98583185		Calleria	Pucallpa	Ucayali	Jr. Libertad #416	Frente Al Supermecado "Mitifi"		2020-07-02 20:49:38.210381	PUCALLPA                                          	36	2020-07-02 20:50:24	11	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	11	11	3	\N
00042309	PACAYA	TARICUARIMA	ROSA LILA	FEMENINO  	CASADO(A)	1958-03-17	988657309		Yarinacohca	Pucallpa	Ucayali	Jr.Cayetano Hereda Mz.5 Lt.12	Aa.Hh.Usares Del Peru		2020-07-02 20:57:35.700585	PUCALLPA                                          	37	2020-07-02 21:00:13	11	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	11	11	3	\N
46801233	SHUÑA	MARICHI	LLAJAIRA MELINA	FEMENINO  	SOLTERO(A)	1991-07-08	943413861		Yarinacohca	Pucallpa	Ucayali	Jr. Cayetano Heredia Mz. 10 Lt.14	Por El Colegio Usares Del Peru		2020-07-02 21:10:44.887359	PUCALLPA                                          	38	2020-07-02 21:11:24	11	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	11	11	3	\N
00071106	REVIER	SOLORZANO	JUAN	MASCULINO 	VIUDO(A)	2048-04-28	923437388		Yarinacohca	Pucallpa	Ucayali	Jr. Cayetano Heredia Mz. 9 Lt. 7	Aa.Hh. Usares Del Peru		2020-07-02 21:16:37.052127	PUCALLPA                                          	39	2020-07-02 21:16:55	11	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	11	11	3	\N
45443553	VELA 	ISLA	MARLY	FEMENINO  	SOLTERO(A)	1988-12-01	950463138		Calleria	Pucallpa	Ucayali	Av. San Martin N°764	2 Cudras Antes De La Av. 7 De Junio 		2020-07-02 21:26:41.934445	PUCALLPA                                          	40	2020-07-02 21:26:54	11	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	11	11	3	\N
00024673	ISLA	AMASIFUEN	VILMA	FEMENINO  	CONVIVIENTE	1954-06-15	-		Calleria	Pucallpa	Ucayali	Av. San Martin N°764	A2 Cuadras De Jr, 7 De Junio		2020-07-02 22:01:51.334087	PUCALLPA                                          	41	2020-07-02 22:02:05	11	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	11	11	3	\N
00124443	AGUILAR	YSLA	JUAN MANUEL	MASCULINO 	SOLTERO(A)	1977-06-24	961985679		Calleria	Pucallpa	Ucayali	Jr. Santa Teresa N°359	Atras De La Meypol Petrogas		2020-07-02 22:03:50.933953	PUCALLPA                                          	42	2020-07-02 22:04:05	11	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	11	11	3	\N
00089237	NUÑES	PACAYA	NORMA ALDITH	FEMENINO  	SOLTERO(A)	1968-09-09	976491045		Calleria	Pucallpa	Ucayali	Jr. El Prado Mz. 156  Lt. 3	Jr. Tarapca N°629		2020-07-02 22:06:37.953629	PUCALLPA                                          	43	2020-07-02 22:06:53	11	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	11	11	3	\N
62969316	CAHUAZA	NUÑES	VIVIAN NIEVEZ	FEMENINO  	SOLTERO(A)	2002-10-28	-		Calleria	Pucallpa	Ucayali	Jr. Tarapaca N°629	Mercado 1		2020-07-02 22:14:56.912099	PUCALLPA                                          	44	2020-07-02 22:15:27	11	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	11	11	3	\N
00015934	PEREZ	PEREZ	LAURA	FEMENINO  	CASADO(A)	1966-08-16	971141984		Calleria	Pucallpa	Ucayali	Psj. General Montero N°128	Sucre Con San Martin Ultima Cudra		2020-07-02 22:32:55.277683	PUCALLPA                                          	45	2020-07-02 22:33:09	11	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	11	11	3	\N
71048416	CASTELL	RODRIGUEZ	CRISTHIAN	MASCULINO 	SOLTERO(A)	1998-03-09	961525447		Calleria	Pucallpa	Ucayali	Jr. Santa Teresa N°359	Por Atras De La Meypol		2020-07-02 22:37:36.929985	PUCALLPA                                          	46	2020-07-02 22:39:29	11	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	11	11	3	\N
81054501	LLACTA	SHUÑA	JULIAN ALBERTO	MASCULINO 	SOLTERO(A)	2012-01-04	943413861		Yarinacohca	Pucallpa	Ucayali	Jr. Cayetano Heredia Mz.10 Lt. 14	Porla Posta Usares Del Peru		2020-07-02 22:42:17.910882	PUCALLPA                                          	47	2020-07-02 22:42:30	11	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	11	11	3	\N
00098331	YUDICHI	GARCIA	JESSICA	FEMENINO  	SOLTERO(A)	2020-07-02	940244512		Yarinacohca	Pucallpa	Ucayali	Av. Antonio Mayo De Brito N°594	Por El Colegio Shapagita		2020-07-02 22:45:10.501766	PUCALLPA                                          	48	2020-07-02 22:45:20	11	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	11	11	3	\N
62969315	CAHUAZA	NUÑES	BELVI LESMIT	FEMENINO  	SOLTERO(A)	2000-02-20	916792604		Calleria	Pucallpa	Ucayali	Jr. El Prado Mz.156 Lt.3	Mercado 1		2020-07-02 22:48:58.968972	PUCALLPA                                          	49	2020-07-02 22:49:12	11	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	11	11	3	\N
45793094	CAHUAZA	NUÑES	ALDI TATIANA	FEMENINO  	SOLTERO(A)	1988-08-18	988736140		Calleria	Pucallpa	Ucayali	Jr. Arturo Berta N°141 Barrio La Hoyada	Por La Hoyada		2020-07-02 22:51:12.264412	PUCALLPA                                          	50	2020-07-02 22:51:22	11	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	11	11	3	\N
45838625	VASQUEZ	ALVARADO	BELMA INES	FEMENINO  	SOLTERO(A)	2020-07-02	975405710		Calleria	Pucallpa	Ucayali	Aa.Hh. Nuevo Paraiso - Psj, Poma Rosa Mz. I Lt. 18	Por Recepciones "Machaca"		2020-07-02 22:56:42.136009	PUCALLPA                                          	51	2020-07-02 22:56:56	11	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	11	11	3	\N
08976036	FARIAS	DEL AGUILA	CARMEN ROSA GLADYS	FEMENINO  	SOLTERO(A)	1958-11-18	922747610-951653451		Calleria	Pucallpa	Ucayali	Jr. Micaela.	Mercado Micaela Bastidas - Ventas De Zapatos		2020-07-03 09:04:05.153272	PUCALLPA                                          	52	2020-07-03 09:04:18	11	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	11	11	3	\N
05282723	ZAMORA	FLORES	DANTE	MASCULINO 	SEPARADO(A)	1953-08-08	948653502		Calleria	Pucallpa	Ucayali	Aa.Hh . Sagrado Corozon -Jr. Salvador Allende Mz.C Lt. 16	Antes De Llegar Ala Sunat Entre El Jr. Amazonas/ Salvador Allender , Ventas De Calaminas De Segunda		2020-07-03 09:07:53.011813	PUCALLPA                                          	53	2020-07-03 09:08:05	11	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	11	11	3	\N
41014343	SHAPIAMA	DREYFFUS	IRIS LOURDES	FEMENINO  	SOLTERO(A)	1981-09-21	-		Calleria	Pucallpa	Ucayali	Aa.Hh. Isla De Manantay Mz. 6 Lt. 05	Por El Aa.Hh 17 De Setiembre. Ingresando Por El Villa El Salbador		2020-07-03 09:22:52.278724	PUCALLPA                                          	54	2020-07-03 09:23:04	11	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	11	11	3	\N
00045241	PINEDO	CORAL	CARLOTA	FEMENINO  	SOLTERO(A)	1964-08-20	923617188		Calleria	Pucallpa	Ucayali	Jr.Pedro Paiva N°258	Ingresando Por La Av.Cahuide Por El Grifo Antes De La  Marina		2020-07-03 09:52:31.711086	PUCALLPA                                          	55	2020-07-03 09:53:29	11	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	11	11	3	\N
77161207	PEREZ 	PORTA	ANYELA 	FEMENINO  	SOLTERO(A)	1998-11-03	940520842		Huancán	Huancayo	Junin	Jr Porvenir Pichas 	Entre El Giron Grau 		2020-07-03 10:11:23.107574	HUANCAYO                                          	74	2020-07-03 10:12:11	5	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	5	5	1	\N
72769249	VILLANO	CARRION	ANGELA	FEMENINO  	SOLTERO(A)	1990-07-03	-		Chilca	Huancayo	Junin	Psj. 10 De Noviembre S/N	Por La Capilla De Puzo	Actualizar Fecha De Nacimiento Y Numero De Celular	2020-07-03 10:20:29.925699	HUANCAYO                                          	75	2020-07-03 10:21:17	3	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	3	3	1	\N
42775833	ZUÑIGA	RAMIREZ DE MACEDO	DORIS LLENY	FEMENINO  	CASADO(A)	1984-10-27	9817366239		Calleria	Pucallpa	Ucayali	Aa.Hh Octavio Monteverde Mz. C Lt. 7 - Jr. Abranham Valdeloman	Ingresando Por El Frente De La Discoteca Gitanos		2020-07-03 10:28:32.620376	PUCALLPA                                          	56	2020-07-03 10:29:27	11	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	11	11	3	\N
44561695	FLORES	LAICHI	ENITH	FEMENINO  	CONVIVIENTE	1988-01-17	954867107		Calleria	Pucallpa	Ucayali	Aa.Hh. Santa Graciela Mz. B Lt, 3	Por Margarita		2020-07-03 10:36:02.868374	PUCALLPA                                          	57	2020-07-03 10:36:21	11	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	11	11	3	\N
76018133	PACAYA 	TEAGUA	MONICA GIANNINA	FEMENINO  	SOLTERO(A)	2000-11-09	936432617		Manantay	Pucallpa	Ucayali	Aa.Hh. Las Caobas Mz. A Lt. 10	Ingresando Por La Av. Aviacion, Antes De Llegar Al Aa.Hh. Laura Bozo		2020-07-03 10:41:44.200777	PUCALLPA                                          	58	2020-07-03 10:41:55	11	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	11	11	3	\N
18071809	CHAVEZ	NUNURA	MARY LUZ	FEMENINO  	SOLTERO(A)	1970-11-21	955548160		Manantay	Pucallpa	Ucayali	Aa.Hh. Heroes Del Cenepa Mz. E Lt.01	Por La Minicpalidad De Manantay		2020-07-03 10:46:13.530107	PUCALLPA                                          	59	2020-07-03 10:46:23	11	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	11	11	3	\N
05380358	PACAYA	MOZOMBITE	WALTER ENRRIQUE	MASCULINO 	SOLTERO(A)	2019-06-04	966090844		Manantay	Pucallpa	Ucayali	Aa.Hh Caobas Mz. A Lt. 10	Espaldas Del Colegio Roca Fuerte Y Videnita		2020-07-03 10:51:24.383426	PUCALLPA                                          	60	2020-07-03 10:51:34	11	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	11	11	3	\N
44554443	LOPEZ 	RUIZ	FELIDA	FEMENINO  	CONVIVIENTE	1987-07-17	44554443		Manantay	Pucallpa	Ucayali	Aa.Hh Los Jardines De Manantay Mz. 03 Lt. 09	Ingresando Por El Aa.Hh 17 De Setiembre . Sementerio De Manantay		2020-07-03 11:33:12.469936	PUCALLPA                                          	61	2020-07-03 11:33:22	11	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	11	11	3	\N
46073602	CLAUDIO	SANTAMARIA	JOVITA	FEMENINO  	CONVIVIENTE	1982-05-04	929788512		Manantay	Pucallpa	Ucayali	Aa.Hh. ,Mario Pezo Mz. 09 Lt,10	-		2020-07-03 11:37:32.697768	PUCALLPA                                          	62	2020-07-03 11:37:46	11	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	11	11	3	\N
71657173	RENGIFO	BALTAZAR	ROSSANA	FEMENINO  	SOLTERO(A)	1996-08-13	958516191		Yarinacohca	Pucallpa	Ucayali	Jr. La Selva Mz. 126 Lt. 4	Por El Colegio Diego Ferrer		2020-07-03 11:41:42.682232	PUCALLPA                                          	63	2020-07-03 11:41:58	11	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	11	11	3	\N
00074706	VARGAS	CASTRO	FRANCISCA	FEMENINO  	SOLTERO(A)	1952-07-30	937154432		Yarinacohca	Pucallpa	Ucayali	Jr. Las Maderas N°188	Por La Plaza De San Fernando		2020-07-03 11:53:43.030256	PUCALLPA                                          	65	2020-07-03 11:53:52	11	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	11	11	3	\N
00087078	RENGIFO	GUEVARA	CESAR AUGUSTO	MASCULINO 	SOLTERO(A)	1968-12-13	961994007		Calleria	Pucallpa	Ucayali	Aa.Hh. Nuevo Paraiso Mz. K Lt. 15	Mercado Mayorista 		2020-07-03 11:45:07.941728	PUCALLPA                                          	64	2020-07-03 11:45:50	11	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	11	11	3	\N
20077264	CARRION 	 RUTTE 	 ADELAIDA	MASCULINO 	SOLTERO(A)	1990-07-03	-		Chilca	Huancayo	Junin	Psj. 10 De Noviembre	Por La Capilla Puzo	Actualizar Datos De Fecha De Naciemiento Y Numero Mde Celular\r\n	2020-07-03 11:56:12.180712	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	3	\N	\N	\N
80491643	SALAS	RUIZ	DORIS	FEMENINO  	SOLTERO(A)	1978-11-20	943966266		Manantay	Pucallpa	Ucayali	Aa.Hh. Juan Carlos Chino Mz. A Lt. 04	Costado De Maestransa De Manantay		2020-07-03 11:59:39.180569	PUCALLPA                                          	67	2020-07-03 12:01:21	11	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	11	11	3	\N
00124280	SALAS	RUIZ	MARIA	FEMENINO  	CONVIVIENTE	1968-11-24	943966266		Manantay	Pucallpa	Ucayali	Aa.Hh. Martha Chavez 2 Mz. 15 Lt. 9	Espaldas De La Videna		2020-07-03 12:04:09.47564	PUCALLPA                                          	68	2020-07-03 12:04:21	11	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	11	11	3	\N
44771061	MOZOMBITE	DE LA CURZ	CETILENA	FEMENINO  	SOLTERO(A)	1986-02-10	98474220		Manantay	Pucallpa	Ucayali	Aa.Hh. Mario Pezo Mz. 8 Lt. 2	Al Culminar La Pista De La Aviacion		2020-07-03 12:06:51.188395	PUCALLPA                                          	69	2020-07-03 12:07:00	11	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	11	11	3	\N
21276834	CORDOVA	ARIAS	CRISTINA GLADYS	FEMENINO  	SOLTERO(A)	1963-09-21	987677262		El Tambo	Huancayo	Junin	Av. Ferrocarril Nº 5157	3 Cudras De La Backus		2020-07-03 12:09:57.132488	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	3	\N	\N	\N
00031572	RUIZ	RAMACHI	TESALIA MARIA	FEMENINO  	SOLTERO(A)	1951-05-27	943966266		Manantay	Pucallpa	Ucayali	Aa.Hh. Carlos Tubino Mz. R Lt, 1   -  Jr.19 De Mayo	Espaldas De La Municipalidad De Manantay		2020-07-03 12:13:53.410599	PUCALLPA                                          	70	2020-07-03 12:14:23	11	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	11	11	3	\N
42878173	MUNAYCO	BARBARAN	CONY MILAGROS	FEMENINO  	SOLTERO(A)	1989-10-19	930622964		Manantay	Pucallpa	Ucayali	Aa.Hh. Lander Tamani Gerra Mz. E Lt. 2	Mario Pezo . Por La Av. Aviacion		2020-07-03 12:21:12.408487	PUCALLPA                                          	71	2020-07-03 12:21:22	11	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	11	11	3	\N
61311977	QUISPE 	CONGORA	MAYORIT MARLIT	FEMENINO  	SOLTERO(A)	2020-07-03	-		Chilca	Huancayo	Junin	Psj Romancoto S/N	Psj Roman Coto Y 28 De Julio	Falta Actualizar Datos De Celular Y Fecha De Nacimiento	2020-07-03 12:28:49.503125	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	3	\N	\N	\N
99907227	QUISPE	MENDOZA	RAUL	MASCULINO 	SOLTERO(A)	2020-01-01	999999999	@	Chilca	Huancayo	Junin	AVENIDA 12 DE OCTUBRE  NRO 220	referencia	obser	2020-06-28 11:54:41	HUANCAYO                                          	78	2020-07-03 12:36:07	5	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	1	5	1	\N
61245494	GONZALEZ	ACHO	KAREN VIVIANA	FEMENINO  	SOLTERO(A)	2002-01-08	948045541		Calleria	Pucallpa	Ucayali	Jr. Lago Titicaca N°140	A Espaldas De La Iglesia Fray Martin		2020-07-03 12:33:10.047839	PUCALLPA                                          	72	2020-07-03 12:36:58	11	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	11	11	3	\N
45394608	HUALPA 	LONDOÑE	JELEN SUSANNE	FEMENINO  	SOLTERO(A)	1990-07-03	944629335		Chilca	Huancayo	Junin	Calle Capeña 1 Secazapa	Por La Escuela Fe Y Alegria	Falta Actualizar Fecha De Nacimiento	2020-07-03 12:42:11.037086	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	3	\N	\N	\N
66661783	QUISPE	SIERRA	FABRICIO	MASCULINO 	SOLTERO(A)	2014-01-01	957603409		Hualhuas	Huancayo	Junin	Calle Independiencia N° 256	Huanacan 		2020-07-03 12:59:33.429028	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	5	\N	\N	\N
45171756	CASTILLO	TAYPE	BENITO	MASCULINO 	SOLTERO(A)	1988-07-10	964640991		Huancán	Huancayo	Junin	Jr 13 De Octubre S/N	Por La Plaza De Huancan		2020-07-03 13:04:22.105055	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	3	\N	\N	\N
80535329	QUISPE	ROJAS	ALDO	MASCULINO 	SOLTERO(A)	1978-02-28	948761424		Huancán	Huancayo	Junin	Jr Junin Huancan	Barrio Por Venir 		2020-07-03 13:27:35.749978	HUANCAYO                                          	79	2020-07-03 13:27:50	5	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	5	5	1	\N
40144277	CONGORA	BARRETO	LUCY 	MASCULINO 	SOLTERO(A)	2020-07-03	935429663		Chilca	Huancayo	Junin	Jr Junin	Huancan - Barrio Porvenir 		2020-07-03 13:53:19.470912	HUANCAYO                                          	81	2020-07-03 13:53:48	5	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	5	5	1	\N
44642607	LOPEZ	POCOHUANCA	ERIKA YOHANA	FEMENINO  	CONVIVIENTE	1985-12-11	959438426		Manantay	Pucallpa	Ucayali	Aa.Hh. Fraternidad Mz. 1A  Lt. 2 Psj. Las Caobas	A Espaldas Del Cementerio N!5		2020-07-03 14:11:26.648238	PUCALLPA                                          	73	2020-07-03 14:11:42	11	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	11	11	3	\N
80527354	REYES	TRIGOSO	MARIA LUZ	FEMENINO  	SOLTERO(A)	1972-03-27	956040316		Calleria	Pucallpa	Ucayali	Psj. Colonizacion N°02 Mz. C Lt,03	Alfrente Del Colegio 7 De Junio		2020-07-03 14:15:13.563617	PUCALLPA                                          	74	2020-07-03 14:15:29	11	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	11	11	3	\N
20064238	BONOFACIO	PEREZ	ITA LIDIA	FEMENINO  	SOLTERO(A)	1984-05-22	917893175		Huancán	Huancayo	Junin	Carretera Central Panamericana	Antes De Cruzar El Puente Chanchas		2020-07-03 14:17:06.094069	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	3	\N	\N	\N
48024565	CONDOR	FABIAN	FABIOLA	FEMENINO  	SOLTERO(A)	1993-11-15	943982061	-	Chilca	Huancayo	Junin	Pjs. Tumi Nª123	Popr Ribaguero Y 9 Dic.		2020-07-03 14:09:44.05422	HUANCAYO                                          	82	2020-07-03 14:17:34	5	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2	2	1	\N
00027982	LUNA	VARGAS	ZOILITH	FEMENINO  	SOLTERO(A)	1962-11-25	917556962		Manantay	Pucallpa	Ucayali	Av.Bellavista N°264 Sf.	Por El Colegio San Fernando		2020-07-03 14:19:15.453935	PUCALLPA                                          	75	2020-07-03 14:19:24	11	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	11	11	3	\N
47198091	CASTRO	PUTAPAÑA	ESTEFANY GERMANY	FEMENINO  	CONVIVIENTE	1991-12-10	963409968		Calleria	Pucallpa	Ucayali	Aa.Hh. Las Maravillas Del Pantanal	A Espaldas Del Local Resel		2020-07-03 14:22:03.597571	PUCALLPA                                          	76	2020-07-03 14:22:40	11	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	11	11	3	\N
23212444	RIVEROS	BREÑA	FLORA	FEMENINO  	SOLTERO(A)	1958-12-22	952803820		El Tambo	Huancayo	Junin	Jr. Junin Nª 1780	Urbanizacion El Tambo		2020-07-03 14:31:16.837724	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	3	\N	\N	\N
46309128	DOMINGUEZ	MARINO	KAREL PATRICIA	FEMENINO  	CONVIVIENTE	1990-05-01	969028200		Calleria	Pucallpa	Ucayali	Jr. Atalaya Mz.28 Lt.9	A Espaldas De La Iglesia  Virgen De Guadalupe		2020-07-03 14:37:26.287494	PUCALLPA                                          	77	2020-07-03 14:37:44	11	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	11	11	3	\N
42580578	TRINIDAD	ESPINOZA	NOEL NILTON	MASCULINO 	CONVIVIENTE	1982-06-05	979223654		Calleria	Pucallpa	Ucayali	Av. Saparador Industrial Mz.11 Lt.12	Altura Del Km.7.000		2020-07-03 14:41:13.414589	PUCALLPA                                          	78	2020-07-03 14:41:28	11	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	11	11	3	\N
42158084	QUISPE	ROJAS	EDITH	FEMENINO  	SOLTERO(A)	2020-07-03	938460858		Calleria	Pucallpa	Ucayali	Aa.Hh. 16 De Abril Mzk Lt. 5 - Alan Sisley Km 6.800	Altura Del Grifo San Juan.		2020-07-03 14:43:42.583821	PUCALLPA                                          	79	2020-07-03 14:44:37	11	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	11	11	3	\N
00122605	MURRIETA	RODRIGEZ	ISABEL	FEMENINO  	SOLTERO(A)	1965-02-11	950819851		Calleria	Pucallpa	Ucayali	Jr. Urbanizacion Municipal Mz.F7 Lt. 12	Por El Tanque De La Urbaizacion		2020-07-03 14:47:11.975184	PUCALLPA                                          	80	2020-07-03 14:47:26	11	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	11	11	3	\N
47592383	SANCHEZ 	ARRETEA	EDELSON	MASCULINO 	CONVIVIENTE	1986-06-05	925443314		Manantay	Pucallpa	Ucayali	Jr. Los  Jardines Mz.G Lt. 12	Por El Parque Natural Km 3		2020-07-03 14:49:58.751363	PUCALLPA                                          	81	2020-07-03 14:50:12	11	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	11	11	3	\N
44495821	RENTERA	ACOSTA	RUTH ROXANA	FEMENINO  	SOLTERO(A)	1987-08-16	913914549		Calleria	Pucallpa	Ucayali	Psj. Tniente Clavero N°182	El Huequito N°1		2020-07-03 14:54:16.211813	PUCALLPA                                          	82	2020-07-03 14:54:26	11	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	11	11	3	\N
46543654	LUNA	MISHAMA	EDITH	FEMENINO  	SOLTERO(A)	1979-12-15	975326581		Calleria	Pucallpa	Ucayali	Av.Arturo Bartra N°141	Por La Hoyada . Por El Mercado		2020-07-03 14:58:38.510844	PUCALLPA                                          	83	2020-07-03 15:00:43	11	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	11	11	3	\N
43886178	GOMEZ	OLARTE	CARMEN COZORIA	FEMENINO  	SOLTERO(A)	1986-11-23	942994579		Calleria	Pucallpa	Ucayali	Jr. Atahualpa N°350	Por El Comedor		2020-07-03 15:03:02.094926	PUCALLPA                                          	84	2020-07-03 15:03:12	11	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	11	11	3	\N
80375811	MEZA	RIOJA	LITA	FEMENINO  	SOLTERO(A)	1972-01-31	965026868		Calleria	Pucallpa	Ucayali	Aa.Hh. Antonio Maya De Brito Mz.2 Lt. 20	Por El Grifo Petrogas Antes De Llegar A Miraflores.		2020-07-03 15:05:55.234655	PUCALLPA                                          	85	2020-07-03 15:06:05	11	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	11	11	3	\N
00062784	RUIZ	VDA . DE SHUÑA	GLADYS	FEMENINO  	VIUDO(A)	1959-02-13	970169985		Calleria	Pucallpa	Ucayali	Jr. Libertad N°418	Alfrente De Mitifi		2020-07-03 15:09:06.216235	PUCALLPA                                          	86	2020-07-03 15:09:20	11	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	11	11	3	\N
00092642	GARCIA	TAMANI	AMILCAR	MASCULINO 	VIUDO(A)	1978-07-06	936242294		Calleria	Pucallpa	Ucayali	A.H Roberto Ruiz Vargas Jr. Alfonzo Ugarte Mz.K Lt. 12	A Media Cuadra De La Puerta De La Escuela Mundial		2020-07-03 15:15:44.146121	PUCALLPA                                          	87	2020-07-03 15:15:54	11	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	11	11	3	\N
46992495	SALAS	RUIZ	GRETY LEIDY	FEMENINO  	SOLTERO(A)	1992-05-06	949100254		Yarinacohca	Pucallpa	Ucayali	Psj. 2 Mz. 120 Lt. 07	Alfrente Del Grifo G&S		2020-07-03 15:33:05.035247	PUCALLPA                                          	88	2020-07-03 15:33:20	11	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	11	11	3	\N
21145619	MURAYARI	PASMIÑO	ENITH	FEMENINO  	SOLTERO(A)	1969-02-17	9120533975		Calleria	Pucallpa	Ucayali	A.H. Independencia Mz.D Lt. 10- Jr, Lima/Colon	Por El Colegio Antonio Raymondi		2020-07-03 16:00:45.735054	PUCALLPA                                          	89	2020-07-03 16:01:15	11	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	11	11	3	\N
45433206	TARICUARIMA	MURAYARI	MILAGROS	FEMENINO  	CONVIVIENTE	1987-01-22	912146236		Calleria	Pucallpa	Ucayali	A.H. Independencia Mz.D Lt.10 - Jr, Lima/Colon	Por Elcolegui Antonio Raymondi		2020-07-03 16:04:26.68463	PUCALLPA                                          	90	2020-07-03 16:04:41	11	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	11	11	3	\N
00106731	SALINAS	TORRES	LENDY	FEMENINO  	SEPARADO(A)	1975-05-19	994884229		Yarinacohca	Pucallpa	Ucayali	Jr. Corpae Ma.C Lt. 07	Por El Arco De Fonavi		2020-07-03 16:08:23.27446	PUCALLPA                                          	91	2020-07-03 16:08:34	11	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	11	11	3	\N
73649262	NEIRA	SANGAMA	KEVIN JACKSON	MASCULINO 	SOLTERO(A)	1996-05-31	969088610		Yarinacohca	Pucallpa	Ucayali	Jr. Corpac Mz. C Lt. 07	Arco De Fonavi		2020-07-03 16:14:45.809746	PUCALLPA                                          	92	2020-07-03 16:15:20	11	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	11	11	3	\N
80632939	CANALES	TALAVERA	GEOVANA	FEMENINO  	SOLTERO(A)	1990-07-04	988718437	-	Huancayo	Huancayo	Junin	Av. Giraldez Nª264	-		2020-07-04 09:25:53.333647	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2	\N	\N	\N
70342553	RAMOS 	CONDORI	LIZANDRA	FEMENINO  	SOLTERO(A)	1990-07-04	973157857	-	Chilca	Huancayo	Junin	Jr. Ancash Nª1310	-		2020-07-04 09:27:34.075368	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2	\N	\N	\N
43050473	VIDAL	GARIA	CONSUELO	FEMENINO  	SOLTERO(A)	1990-07-04	999999999	-	Huancayo	Huancayo	Junin	Pjs. Cesar Vallejo Nª165	-		2020-07-04 09:29:22.939704	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2	\N	\N	\N
19829998	PAUCAR	BALDEON	VIRA	FEMENINO  	SOLTERO(A)	1990-07-04	921785326	-	Chilca	Huancayo	Junin	Pjs.Inca Pachacutec S/N	-		2020-07-04 09:46:09.536217	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2	\N	\N	\N
45879388	PEREZ	CORONEL	DELCY DINA	MASCULINO 	SOLTERO(A)	2020-01-01	999999999	@	Chilca	Huancayo	Junin	UNIDAD VECINAL: PORVENIR - JIRÓN JUNIN  NRO 148	referencia	obser	2020-06-28 11:54:41	HUANCAYO                                          	58	2020-06-29 15:32:00	3	0	\N	\N	20	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	1	3	1	10:22:25-05
76240397	ADAUTO	DE LA CRUZ	GINA SHEYLA	FEMENINO  	SOLTERO(A)	1995-05-30	959627418		Chilca	Huancayo	Junin	Av.Arterial N°310	Casa Celeste		2020-07-04 11:31:31.540759	HUANCAYO                                          	84	2020-07-04 11:33:37	3	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	3	3	1	\N
00000000	ALARCON	CASTRO	EDICA MARGOT	FEMENINO  	SOLTERO(A)	1990-07-04	982931515	-	Huancayo	Huancayo	Junin	Jr. Huancas Y Ica S/N	Maercado Mayorista		2020-07-04 13:09:35.486101	HUANCAYO                                          	\N	\N	\N	\N	\N	\N	21	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2	\N	1	13:09:57-05
46706530	CERVANTES 	VASQUEZ	SINDY	FEMENINO  	SOLTERO(A)	1990-09-09	933731557		Huancayo	Huancayo	Junin	Av Gladiolos  N°490			2020-07-06 10:50:01.306536	HUANCAYO                                          	86	2020-07-06 11:09:22	5	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	5	5	1	\N
00035433	PEREZ	PEREZ	REYNA MARIA	MASCULINO 	SOLTERO(A)	1954-10-23	992642270		Calleria	Pucallpa	Ucayali	Jr. Comandante Barrera N°595	Antes De Llegar Ala Plaza 11 De Julio 		2020-07-07 08:49:40.98254	PUCALLPA                                          	93	2020-07-07 08:51:02	11	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	11	11	3	\N
71310459	CAYSAHUANA 	JACOBE	MARILUZ	MASCULINO 	SOLTERO(A)	2020-07-06	944788586		Huancán	Huancayo	Junin	Jr La Cantuta 	Huancan 		2020-07-06 14:14:11.0759	HUANCAYO                                          	90	2020-07-06 14:15:00	5	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	5	5	1	\N
42816850	QUISPE	VERA	RAUL ELVIS	MASCULINO 	SOLTERO(A)	1990-07-07	99999999		Huancayo	Huancayo	Junin	Jr Arequiopa Sn	Port La Agencia		2020-07-07 12:11:58.40815	HUANCAYO                                          	\N	\N	\N	\N	\N	\N	23	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2	\N	1	12:14:28-05
45938581	ALANYA	JOAQUIN	RUTH GEOVANA	FEMENINO  	SOLTERO(A)	1990-07-07	99999999		Huancayo	Huancayo	Junin	Jr Arequipa S/N	Por Al Oficina		2020-07-07 12:13:15.137972	HUANCAYO                                          	\N	\N	\N	\N	\N	\N	24	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2	\N	1	12:15:23-05
20047216	MENDOZA	TOVAR	JORGE ANDRES	MASCULINO 	SOLTERO(A)	1972-02-04	064764091		Huancayo	Huancayo	Junin	Av Real N°1310			2020-07-06 13:21:59.732132	HUANCAYO                                          	89	2020-07-06 13:22:25	5	0	\N	\N	36	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	5	5	1	13:30:39-05
47323767	HUAMAN 	GARCIA	LIZ GUISELA	FEMENINO  	SOLTERO(A)	2000-01-01	969695292		Chilca	Huancayo	Junin	Jr Amazonas 	Azapampa		2020-07-09 10:41:41.147914	HUANCAYO                                          	95	2020-07-09 10:41:59	5	0	\N	\N	34	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	5	5	1	13:28:24-05
43219907	ARTICA	PERALTA	IVAN	MASCULINO 	SOLTERO(A)	1985-09-27	99999999		Sapallanga	Huancayo	Junin	Jr Imperial Nº496	Po El Parque De Zapallanga		2020-07-08 12:46:13.661133	HUANCAYO                                          	\N	\N	\N	\N	\N	\N	25	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2	\N	1	12:47:17-05
23159715	DIAZ	SIFUENTES	LUZ MARINA	FEMENINO  	CASADO(A)	1969-04-29	989309759		Calleria	Pucallpa	Ucayali	Psj. Sagrado Corazon Mz. B Lt. 14	Frente A Las Aduanas		2020-07-08 21:35:37.217844	PUCALLPA                                          	94	2020-07-08 21:35:46	11	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	11	11	3	\N
42611489	SANTIAGO	QUINTANILLA	MARICRUZ DIONISIA 	FEMENINO  	SOLTERO(A)	1975-01-01	9999999		Huancán	Huancayo	Junin	Av Independencia 	Parque De Huancan 		2020-07-09 09:54:38.888019	HUANCAYO                                          	94	2020-07-09 09:54:58	5	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	5	5	1	\N
77377731	CHUCO	ESCOBAR	JOSSEP KERICK	MASCULINO 	SOLTERO(A)	2000-01-27	980905904		El Tambo	Huancayo	Junin	Av 12 De Octubre N° 1040	Cullpa Baja		2020-07-07 12:24:25.04555	HUANCAYO                                          	92	2020-07-07 12:24:44	5	0	\N	\N	35	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	5	5	1	13:29:23-05
77381267	CHUCO	ESCOBAR	ERICK JESUS 	MASCULINO 	SOLTERO(A)	1998-09-25	980905904		El Tambo	Huancayo	Junin	Av 12 De Octubre N°1040	Cullpa Baja 		2020-07-07 12:39:32.578725	HUANCAYO                                          	93	2020-07-07 12:39:51	5	0	\N	\N	33	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	5	5	1	13:25:14-05
48522972	PARRA	MACHARI	CLARISA	FEMENINO  	SOLTERO(A)	1990-11-16	954679188		Huancán	Huancayo	Junin	Jr 7 De Octubre 	Pueblo Nuevo Huancan 		2020-07-09 13:12:17.055434	HUANCAYO                                          	96	2020-07-09 13:12:35	5	0	\N	\N	26	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	5	5	1	12:06:04-05
41278746	QUISPE	ROJAS	EDITH YENI 	FEMENINO  	SOLTERO(A)	1962-01-01	957603409		Huancán	Huancayo	Junin	Jrjunin S/N 			2020-07-03 12:06:59.707099	HUANCAYO                                          	77	2020-07-03 12:07:18	5	0	\N	\N	29	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	5	5	1	13:17:13-05
20898988	QUIQUIA	CHAGUA	TARCILA	MASCULINO 	SOLTERO(A)	2020-01-01	999999999	@	Chilca	Huancayo	Junin	CHILCA - AVENIDA LOS PROCERES  NRO 394	referencia	obser	2020-06-28 11:54:41	HUANCAYO                                          	69	2020-07-01 10:18:11	5	0	\N	\N	30	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	1	5	1	13:18:28-05
20079323	LAGONES	CRISPIN	LEONADA	FEMENINO  	CONVIVIENTE	1963-01-19	937139978		Huancayo	Huancayo	Junin	Pjs.  Francisco Nª167			2020-07-10 13:51:17.867612	HUANCAYO                                          	\N	\N	\N	\N	\N	\N	38	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2	\N	1	13:52:32-05
44274623	RAMOS	LAGONES	NORMA	FEMENINO  	SOLTERO(A)	1986-11-28	937139978		Huancayo	Huancayo	Junin	Pjs Francisco 167			2020-07-10 13:53:23.451833	HUANCAYO                                          	\N	\N	\N	\N	\N	\N	39	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2	\N	1	13:53:55-05
48117988	SEDANO	LIZANA	SHEYLA	MASCULINO 	SOLTERO(A)	2020-01-01	999999999	@	Chilca	Huancayo	Junin	UNIDAD VECINAL: PARQUE PEÑALOZA - PASAJE SAN SILVESTRE	referencia	obser	2020-06-28 11:54:41	HUANCAYO                                          	37	2020-06-29 15:07:30	3	0	\N	\N	40	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	1	3	1	13:55:09-05
41736927	RAMOS	LAGONES	PRUDENCIO TEODOR	MASCULINO 	SOLTERO(A)	1983-04-28	937139978		Huancayo	Huancayo	Junin	Pjs.San Francisco Nº167			2020-07-10 13:56:04.373832	HUANCAYO                                          	\N	\N	\N	\N	\N	\N	41	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2	\N	1	14:03:26-05
76918572	GOMEZ	QUISPE	NAYSHA EDILUZ	MASCULINO 	SOLTERO(A)	2020-01-01	999999999	@	Chilca	Huancayo	Junin	PROLONGACION PANAMA   NRO 110	referencia	obser	2020-06-28 11:54:41	HUANCAYO                                          	98	2020-07-11 10:35:21	5	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	1	5	1	\N
44100810	CABRERA	GOMEZ	BEATRIS LUCIANA 	FEMENINO  	SOLTERO(A)	1988-12-23	932631215		El Tambo	Huancayo	Junin	Calle Chicchicancha N°175	Cullpa		2020-07-13 12:53:42.968545	HUANCAYO                                          	101	2020-07-13 12:54:02	5	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	5	5	1	\N
20682065	URBANO	PAUCARCAJA	MARIA TEODORA 	FEMENINO  	SOLTERO(A)	1954-06-02	999251792		El Tambo	Huancayo	Junin	Psj Los Claveles S/N Sector 22	La Esperanza 		2020-07-13 09:47:49.726986	HUANCAYO                                          	100	2020-07-13 09:53:23	5	0	\N	\N	44	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	5	5	1	13:07:57-05
41614930	LLANCO	ORE	CARLOS EDUARDO	MASCULINO 	SOLTERO(A)	2020-07-14	942845462		Huancayo	Huancayo	Junin	Av. Huancavelica 620	Loreto Y Huancavelica		2020-07-14 09:38:08.876128	HUANCAYO                                          	102	2020-07-14 09:38:38	7	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2	2	1	\N
71065237	JUSCAMAYTA	ROMERO	XIOMARA VANESA	FEMENINO  	SOLTERO(A)	2002-07-05	984766228		El Tambo	Huancayo	Junin	Jr Los Huancas N°915	Frente Al Mercado Micaela Bastidas 		2020-07-14 10:51:17.834677	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	5	\N	\N	\N
48177932	GARCIA	BORJA	MARICRUZ CLESVI	MASCULINO 	SOLTERO(A)	2020-07-14	921552616		Huayucachi	Huancayo	Junin	Jr Ica N°1	Huayucachi		2020-07-14 11:24:01.307519	HUANCAYO                                          	103	2020-07-14 11:25:01	5	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	5	5	1	\N
76663751	RAMOS	NAVARRO	ALEJANDRA MISHEL 	FEMENINO  	SOLTERO(A)	1997-03-15	927266421		Huancayo	Huancayo	Junin	Parque Estoc N°188	San Carlos 		2020-07-11 12:48:55.659308	HUANCAYO                                          	99	2020-07-11 12:49:15	5	0	\N	\N	45	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	5	5	1	11:52:13-05
75400769	VILLANES 	CAHUAYA	JAIR	MASCULINO 	SOLTERO(A)	2010-05-30	980694958		El Tambo	Huancayo	Junin	Jr Santa Rosa Mzc 	La Esperanza 		2020-07-15 09:25:06.333154	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	5	\N	\N	\N
19882294	HILARIO	DE LA CRUZ	RITA	MASCULINO 	SOLTERO(A)	2020-07-15	939538171		Chilca	Huancayo	Junin	Chilca Sector 3	Azapmapa 		2020-07-15 09:58:44.845971	HUANCAYO                                          	104	2020-07-15 09:59:12	5	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	5	5	1	\N
21000001	POMA	BLANCAS	SARA	FEMENINO  	SOLTERO(A)	2000-01-01	981532273		Huancayo	Huancayo	Junin	Pueblo Joven Huancayo Jr La Florida	Mercado La Esperanza		2020-07-15 10:46:12.303798	HUANCAYO                                          	105	2020-07-15 10:46:33	5	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	5	5	1	\N
44063640	LOLO	CAMARENA	ANGELICA ROCSANA 	FEMENINO  	SOLTERO(A)	1996-12-17	965194472		El Tambo	Huancayo	Junin	Av La Esperanza 	Tambo		2020-07-16 09:22:59.561085	HUANCAYO                                          	\N	\N	\N	\N	\N	\N	47	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	5	\N	1	09:26:57-05
70203921	CRUZ	PINEDO	FATIMA SATOMY	FEMENINO  	SOLTERO(A)	1997-06-26	992813802		Calleria	Pucallpa	Ucayali	Av. Primavera 306.	Aa.Hh. Primavera 1 Ra Etapa		2020-07-30 15:32:56.710534	PUCALLPA                                          	95	2020-07-30 15:33:20	11	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	11	11	3	\N
\.


--
-- Data for Name: cliente_aval; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.cliente_aval (id_cliente_aval, dni, aval, parentesco) FROM stdin;
\.


--
-- Data for Name: cliente_beneficiario; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.cliente_beneficiario (id_cliente_beneficiario, dni, beneficiario, parantesco, obs, nombre_beni) FROM stdin;
382	76621787	76621787	EL MISMO	\N	MESEIAS ALEGRIA FLOR DE JESUS
383	40129120	40129120	EL MISMO	\N	CARRIZALES ROJAS JORGE
384	23701995	74422219	HIJA	\N	HERRERA SOTO JUANITA RUTH
385	19940774	19940774	EL MISMO	\N	SIERRA BARRIENTOS ZOZIMO
386	44974563	44974563	ELLA MISMA	\N	SEGOVIA BARBOZA DENISSA ZOILA
387	46041015	-       	ELLA MISMA	\N	-
388	20113864	-       	ELLA MISMA	\N	-
389	42297478	-       	EL MISMO	\N	-
390	41123341	-       	EL MISMO	\N	-
391	47475777	-       	EL MISMO	\N	-
392	46561831	-       	ELLA MISMA	\N	-
393	19942242	-       	ELLA MISMA	\N	-
394	45745478	-       	ELLA MISMA	\N	-
395	00098119	00098119	ELLA MISMA	\N	URQUIA LOPEZ ROSA LUCIA
396	00061656	00015934	ESPOSA	\N	PEREZ PEREZ LAURA
397	20107243	-       	ELLA MISMA	\N	-
398	19856332	-       	EL MISMO	\N	-
399	20648525	-       	ELLA MISMA	\N	-
400	20064201	-       	EL MISMO	\N	-
401	19944260	-       	ella misma	\N	-
402	40893286	-       	ella misma	\N	-
403	45879388	-       	-	\N	-
404	00000000	-       	ella misma	\N	-
405	46357683	-       	ELLA MISMA	\N	-
406	42816850	-       	EL MISMO	\N	-
407	45938581	-       	ELLA MISMA	\N	-
408	43219907	-       	EL MISMO	\N	-
409	48522972	-       	ELLA MISMA	\N	-
410	19996893	-       	ELLA MISMA	\N	-
411	45666252	-       	EL MISMO	\N	-
412	41278746	-       	ELLA MISMA	\N	-
413	20898988	-       	ELLA MISMA	\N	-
414	71872891	-       	ELLA MISMA	\N	-
415	44350264	-       	ELLA MISMA	\N	-
416	77381267	-       	EL MISMO	\N	-
417	47323767	-       	ELLA MISMA	\N	-
418	77377731	-       	EL MISMO	\N	-
419	20047216	-       	EL MISMO	\N	-
420	46657916	-       	ELLA MISMA	\N	-
421	20079323	-       	ELLA MISMA	\N	-
422	44274623	-       	ELLA MISMA	\N	-
423	48117988	-       	ELLA MISMA	\N	-
424	41736927	-       	EL MISMO	\N	-
425	21276617	-       	EL MISMO	\N	-
426	19946742	-       	ELLA MISMA	\N	-
427	20682065	-       	ELLA MISMA	\N	-
428	76663751	-       	ella misma	\N	-
429	44279232	-       	EL MISMO	\N	-
430	44063640	-       	ELLA MISMA	\N	-
431	70020689	-       	-	\N	ella misma
\.


--
-- Data for Name: compromiso; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.compromiso (id_compromiso, id_solicitud, tipo_compromiso, usuario, fecha_hora, fecha_compromiso, deuda_vencida, id_notificaciones, obs) FROM stdin;
\.


--
-- Data for Name: conyuge; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.conyuge (id_conyuge, dni, conyuge, estado, obs) FROM stdin;
\.


--
-- Data for Name: credito_refinanciado; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.credito_refinanciado (id_refinanciado, id_solicitud, nro_de_creditos, detalle_dreditos, fecha_hora_refinanciado, obs) FROM stdin;
\.


--
-- Data for Name: cronograma_ahorro; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.cronograma_ahorro (id_cronograma_ahorro, id_libreta_ahorro, pagado, monto_pactado, monto_descontado, nro_cuota, fecha_pactada, fecha_hora_pagado, monto_movimiento, fecha_hora_amortizacion, amortizado) FROM stdin;
37	2	f	12	f	1	2020-09-28	\N	\N	\N	0
38	3	f	16	f	1	2020-12-27	\N	\N	\N	0
39	4	f	0.599999999999999978	f	1	2020-09-28	\N	\N	\N	0
\.


--
-- Data for Name: cronograma_pagos; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.cronograma_pagos (id_cronograma_pago, id_solicitud, pagado, capital_x_cuota, interes_x_cuota, redondeo_x_cuota, penalidad_x_cuota, cuota_pagar, ii, ga, cuota_descontada, nro_cuota, fecha_vencimiento, fecha_hora_pagado, monto_capital, monto_interes, monto_redondeo, mora, otros_pagos, notificacion, fecha_hora_amortizacion, amortizado, pago_con, id_operacion) FROM stdin;
414363	15	f	7.69000000000000039	0.46000000000000002	0.0500000000000000028	0	8.19999999999999929	3	3	f	22	2020-07-25	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414364	15	f	7.69000000000000039	0.46000000000000002	0.0500000000000000028	0	8.19999999999999929	3	3	f	23	2020-07-27	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414365	15	f	7.69000000000000039	0.46000000000000002	0.0500000000000000028	0	8.19999999999999929	3	3	f	24	2020-07-28	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414366	15	f	7.69000000000000039	0.46000000000000002	0.0500000000000000028	0	8.19999999999999929	3	3	f	25	2020-07-30	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414367	15	f	7.69000000000000039	0.46000000000000002	0.0500000000000000028	0	8.19999999999999929	3	3	f	26	2020-07-31	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414342	15	t	7.69000000000000039	0.46000000000000002	0.0500000000000000028	0	8.19999999999999929	3	3	f	1	2020-07-01	2020-06-30 14:17:34.201	7.69000000000000039	0.46000000000000002	0.0500000000000000028	\N	\N	\N	2020-06-30 02:17:35	8.19999999999999929	\N	\N
414391	19	f	137.469999999999999	2.75999999999999979	0	0	140.22568656716399	3	3	f	1	2020-07-30	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414343	15	t	7.69000000000000039	0.46000000000000002	0.0500000000000000028	0	8.19999999999999929	3	3	f	2	2020-07-02	2020-06-30 14:17:34.201	7.69000000000000039	0.46000000000000002	0.0500000000000000028	\N	\N	\N	2020-06-30 02:17:36	8.19999999999999929	\N	\N
414392	19	f	138.840000000000003	1.3899999999999999	0	0	140.22568656716399	3	3	f	2	2020-08-29	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414344	15	t	7.69000000000000039	0.46000000000000002	0.0500000000000000028	0	8.19999999999999929	3	3	f	3	2020-07-03	2020-06-30 14:17:34.201	7.69000000000000039	0.46000000000000002	0.0500000000000000028	\N	\N	\N	2020-06-30 02:17:37	8.19999999999999929	\N	\N
414345	15	t	7.69000000000000039	0.46000000000000002	0.0500000000000000028	0	8.19999999999999929	3	3	f	4	2020-07-04	2020-07-02 13:09:59.395	7.69000000000000039	0.46000000000000002	0.0500000000000000028	\N	\N	\N	2020-07-02 01:10:01	8.19999999999999929	\N	\N
414371	12	f	148.52000000000001	17.8200000000000003	0.0599999999999999978	0	166.400000000000006	3	3	f	2	2020-07-30	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414372	12	f	148.52000000000001	17.8200000000000003	0.0599999999999999978	0	166.400000000000006	3	3	f	3	2020-08-14	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414373	12	f	148.52000000000001	17.8200000000000003	0.0599999999999999978	0	166.400000000000006	3	3	f	4	2020-08-29	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414374	18	f	56.6400000000000006	1.41599999999999993	0	0	58.1000000000000014	3	3	f	1	2020-07-07	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414376	18	f	56.6400000000000006	1.41599999999999993	0	0	58.1000000000000014	3	3	f	3	2020-07-21	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414377	18	f	56.6400000000000006	1.41599999999999993	0	0	58.1000000000000014	3	3	f	4	2020-07-28	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414378	18	f	56.6400000000000006	1.41599999999999993	0	0	58.1000000000000014	3	3	f	5	2020-08-04	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414379	18	f	56.6400000000000006	1.41599999999999993	0	0	58.1000000000000014	3	3	f	6	2020-08-11	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414380	18	f	56.6400000000000006	1.41599999999999993	0	0	58.1000000000000014	3	3	f	7	2020-08-18	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414381	18	f	56.6400000000000006	1.41599999999999993	0	0	58.1000000000000014	3	3	f	8	2020-08-25	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414382	18	f	56.6400000000000006	1.41599999999999993	0	0	58.1000000000000014	3	3	f	9	2020-09-01	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414383	18	f	56.6400000000000006	1.41599999999999993	0	0	58.1000000000000014	3	3	f	10	2020-09-08	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414386	26	f	351	3.50999999999999979	0.0100000000000000002	0	354.550000000000011	3	3	f	1	2020-07-07	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414388	26	f	351	3.50999999999999979	0.0100000000000000002	0	354.550000000000011	3	3	f	3	2020-07-21	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414389	26	f	351	3.50999999999999979	0.0100000000000000002	0	354.550000000000011	3	3	f	4	2020-07-28	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414385	17	f	45	0	0	0	45	3	3	f	1	2020-07-01	\N	\N	\N	\N	0.200000000000000011	\N	\N	\N	\N	\N	\N
414368	16	f	345	3	0	0	348	3	3	f	1	2020-07-01	\N	\N	\N	\N	1	\N	\N	\N	\N	\N	\N
414369	14	t	1023	61	0	0	1084	3	3	f	1	2020-07-01	2020-07-22 12:48:54.402	1023	61	0	1.5	\N	\N	2020-07-22 12:48:55	1084	\N	\N
414393	42	f	60.7000000000000028	0	0	0	60.7000000000000028	3	3	f	1	2020-07-09	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414395	42	f	60.7000000000000028	0	0	0	60.7000000000000028	3	3	f	3	2020-07-23	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414396	42	f	60.7000000000000028	0	0	0	60.7000000000000028	3	3	f	4	2020-07-30	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414397	42	f	60.7000000000000028	0	0	0	60.7000000000000028	3	3	f	5	2020-08-06	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414398	42	f	60.7000000000000028	0	0	0	60.7000000000000028	3	3	f	6	2020-08-13	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414399	42	f	60.7000000000000028	0	0	0	60.7000000000000028	3	3	f	7	2020-08-20	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414351	15	t	7.69000000000000039	0.46000000000000002	0.0500000000000000028	0	8.19999999999999929	3	3	f	10	2020-07-11	2020-07-15 14:41:07.92	7.69000000000000039	0.46000000000000002	0.0500000000000000028	\N	\N	\N	2020-07-15 02:41:09	8.19999999999999929	\N	\N
414347	15	t	7.69000000000000039	0.46000000000000002	0.0500000000000000028	0	8.19999999999999929	3	3	f	6	2020-07-07	2020-07-06 14:48:43.91	7.69000000000000039	0.46000000000000002	0.0500000000000000028	\N	\N	\N	2020-07-06 02:48:45	8.19999999999999929	\N	\N
414349	15	t	7.69000000000000039	0.46000000000000002	0.0500000000000000028	0	8.19999999999999929	3	3	f	8	2020-07-09	2020-07-10 15:05:33.622	7.69000000000000039	0.46000000000000002	0.0500000000000000028	\N	\N	\N	2020-07-10 03:05:35	8.19999999999999929	\N	\N
414350	15	t	7.69000000000000039	0.46000000000000002	0.0500000000000000028	0	8.19999999999999929	3	3	f	9	2020-07-10	2020-07-10 15:05:33.622	7.69000000000000039	0.46000000000000002	0.0500000000000000028	\N	\N	\N	2020-07-10 03:05:36	8.19999999999999929	\N	\N
414375	18	f	56.6400000000000006	1.41599999999999993	0	0	58.1000000000000014	3	3	f	2	2020-07-14	\N	\N	\N	\N	6	\N	\N	\N	\N	\N	\N
414341	11	t	350	21	0	0	371	3	3	f	2	2020-07-30	2020-07-13 11:23:16.608	350	21	0	\N	\N	\N	2020-07-13 11:23:18	371	\N	\N
414352	15	t	7.69000000000000039	0.46000000000000002	0.0500000000000000028	0	8.19999999999999929	3	3	f	11	2020-07-13	2020-07-15 14:41:07.92	7.69000000000000039	0.46000000000000002	0.0500000000000000028	\N	\N	\N	2020-07-15 02:41:09	8.19999999999999929	\N	\N
414387	26	f	351	3.50999999999999979	0.0100000000000000002	0	354.550000000000011	3	3	f	2	2020-07-14	\N	\N	\N	\N	6	\N	\N	\N	\N	\N	\N
414370	12	f	148.52000000000001	17.8200000000000003	0.0599999999999999978	0	166.400000000000006	3	3	f	1	2020-07-15	\N	\N	\N	\N	0.699999999999999956	\N	\N	\N	\N	\N	\N
414394	42	f	60.7000000000000028	0	0	0	60.7000000000000028	3	3	f	2	2020-07-16	\N	\N	\N	\N	6	\N	\N	\N	\N	\N	\N
414356	15	t	7.69000000000000039	0.46000000000000002	0.0500000000000000028	0	8.19999999999999929	3	3	f	15	2020-07-17	2020-07-24 13:55:27.777	7.69000000000000039	0.46000000000000002	0.0500000000000000028	\N	\N	\N	2020-07-24 01:55:28	8.19999999999999929	\N	\N
414353	15	t	7.69000000000000039	0.46000000000000002	0.0500000000000000028	0	8.19999999999999929	3	3	f	12	2020-07-14	2020-07-16 14:34:52.923	7.69000000000000039	0.46000000000000002	0.0500000000000000028	\N	\N	\N	2020-07-16 02:34:54	8.19999999999999929	\N	\N
414354	15	t	7.69000000000000039	0.46000000000000002	0.0500000000000000028	0	8.19999999999999929	3	3	f	13	2020-07-15	2020-07-20 13:06:45.894	7.69000000000000039	0.46000000000000002	0.0500000000000000028	\N	\N	\N	2020-07-20 01:06:47	8.19999999999999929	\N	\N
414358	15	t	7.69000000000000039	0.46000000000000002	0.0500000000000000028	0	8.19999999999999929	3	3	f	17	2020-07-20	2020-08-12 13:43:43.429	7.69000000000000039	0.46000000000000002	0.0500000000000000028	\N	\N	\N	2020-08-12 01:43:44	8.19999999999999929	\N	\N
414359	15	t	7.69000000000000039	0.46000000000000002	0.0500000000000000028	0	8.19999999999999929	3	3	f	18	2020-07-21	2020-08-12 13:43:43.429	7.69000000000000039	0.46000000000000002	0.0500000000000000028	\N	\N	\N	2020-08-12 01:43:45	8.19999999999999929	\N	\N
414360	15	t	7.69000000000000039	0.46000000000000002	0.0500000000000000028	0	8.19999999999999929	3	3	f	19	2020-07-22	2020-08-18 13:35:25.503	7.69000000000000039	0.46000000000000002	0.0500000000000000028	\N	\N	\N	2020-08-18 01:35:26	8.19999999999999929	\N	\N
414362	15	t	7.69000000000000039	0.46000000000000002	0.0500000000000000028	0	8.19999999999999929	3	3	f	21	2020-07-24	2020-08-24 11:59:30.182	7.69000000000000039	0.46000000000000002	0.0500000000000000028	\N	\N	\N	2020-08-24 11:59:30	8.19999999999999929	\N	\N
414400	42	f	60.7000000000000028	0	0	0	60.7000000000000028	3	3	f	8	2020-08-27	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414401	43	f	62.71875	0	0	0	62.75	3	3	f	1	2020-07-09	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414403	43	f	62.71875	0	0	0	62.75	3	3	f	3	2020-07-23	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414404	43	f	62.71875	0	0	0	62.75	3	3	f	4	2020-07-30	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414405	43	f	62.71875	0	0	0	62.75	3	3	f	5	2020-08-06	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414406	43	f	62.71875	0	0	0	62.75	3	3	f	6	2020-08-13	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414407	43	f	62.71875	0	0	0	62.75	3	3	f	7	2020-08-20	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414408	43	f	62.71875	0	0	0	62.75	3	3	f	8	2020-08-27	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414409	43	f	62.71875	0	0	0	62.75	3	3	f	9	2020-09-03	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414410	43	f	62.71875	0	0	0	62.75	3	3	f	10	2020-09-10	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414411	43	f	62.71875	0	0	0	62.75	3	3	f	11	2020-09-17	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414412	43	f	62.71875	0	0	0	62.75	3	3	f	12	2020-09-24	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414413	43	f	62.71875	0	0	0	62.75	3	3	f	13	2020-10-01	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414414	43	f	62.71875	0	0	0	62.75	3	3	f	14	2020-10-09	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414415	43	f	62.71875	0	0	0	62.75	3	3	f	15	2020-10-16	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414416	43	f	62.71875	0	0	0	62.75	3	3	f	16	2020-10-23	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414417	41	f	144.875	0	0	0	144.900000000000006	3	3	f	1	2020-07-09	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414419	41	f	144.875	0	0	0	144.900000000000006	3	3	f	3	2020-07-23	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414420	41	f	144.875	0	0	0	144.900000000000006	3	3	f	4	2020-07-30	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414421	39	f	44	0	0	0	44	3	3	f	1	2020-07-09	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414423	39	f	44	0	0	0	44	3	3	f	3	2020-07-23	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414424	39	f	44	0	0	0	44	3	3	f	4	2020-07-30	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414425	39	f	44	0	0	0	44	3	3	f	5	2020-08-06	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414426	39	f	44	0	0	0	44	3	3	f	6	2020-08-13	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414427	39	f	44	0	0	0	44	3	3	f	7	2020-08-20	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414428	39	f	44	0	0	0	44	3	3	f	8	2020-08-27	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414429	39	f	44	0	0	0	44	3	3	f	9	2020-09-03	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414430	39	f	44	0	0	0	44	3	3	f	10	2020-09-10	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414431	39	f	44	0	0	0	44	3	3	f	11	2020-09-17	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414432	39	f	44	0	0	0	44	3	3	f	12	2020-09-24	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414433	25	f	9.96000000000000085	0	0.0400000000000000008	0	10	3	3	f	1	2020-07-01	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414434	25	f	9.96000000000000085	0	0.0400000000000000008	0	10	3	3	f	2	2020-07-02	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414435	25	f	9.96000000000000085	0	0.0400000000000000008	0	10	3	3	f	3	2020-07-03	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414436	25	f	9.96000000000000085	0	0.0400000000000000008	0	10	3	3	f	4	2020-07-04	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414437	25	f	9.96000000000000085	0	0.0400000000000000008	0	10	3	3	f	5	2020-07-06	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414438	25	f	9.96000000000000085	0	0.0400000000000000008	0	10	3	3	f	6	2020-07-07	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414439	25	f	9.96000000000000085	0	0.0400000000000000008	0	10	3	3	f	7	2020-07-08	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414440	25	f	9.96000000000000085	0	0.0400000000000000008	0	10	3	3	f	8	2020-07-09	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414441	25	f	9.96000000000000085	0	0.0400000000000000008	0	10	3	3	f	9	2020-07-10	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414442	25	f	9.96000000000000085	0	0.0400000000000000008	0	10	3	3	f	10	2020-07-11	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414443	25	f	9.96000000000000085	0	0.0400000000000000008	0	10	3	3	f	11	2020-07-13	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414444	25	f	9.96000000000000085	0	0.0400000000000000008	0	10	3	3	f	12	2020-07-14	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414445	25	f	9.96000000000000085	0	0.0400000000000000008	0	10	3	3	f	13	2020-07-15	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414447	25	f	9.96000000000000085	0	0.0400000000000000008	0	10	3	3	f	15	2020-07-17	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414448	25	f	9.96000000000000085	0	0.0400000000000000008	0	10	3	3	f	16	2020-07-18	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414449	25	f	9.96000000000000085	0	0.0400000000000000008	0	10	3	3	f	17	2020-07-20	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414450	25	f	9.96000000000000085	0	0.0400000000000000008	0	10	3	3	f	18	2020-07-21	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414451	25	f	9.96000000000000085	0	0.0400000000000000008	0	10	3	3	f	19	2020-07-22	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414452	25	f	9.96000000000000085	0	0.0400000000000000008	0	10	3	3	f	20	2020-07-23	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414453	25	f	9.96000000000000085	0	0.0400000000000000008	0	10	3	3	f	21	2020-07-24	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414454	25	f	9.96000000000000085	0	0.0400000000000000008	0	10	3	3	f	22	2020-07-25	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414455	25	f	9.96000000000000085	0	0.0400000000000000008	0	10	3	3	f	23	2020-07-27	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414456	25	f	9.96000000000000085	0	0.0400000000000000008	0	10	3	3	f	24	2020-07-28	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414457	25	f	9.96000000000000085	0	0.0400000000000000008	0	10	3	3	f	25	2020-07-30	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414458	25	f	9.96000000000000085	0	0.0400000000000000008	0	10	3	3	f	26	2020-07-31	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414459	25	f	9.96000000000000085	0	0.0400000000000000008	0	10	3	3	f	27	2020-08-01	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414460	25	f	9.96000000000000085	0	0.0400000000000000008	0	10	3	3	f	28	2020-08-03	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414422	39	f	44	0	0	0	44	3	3	f	2	2020-07-16	\N	\N	\N	\N	6	\N	\N	\N	\N	\N	\N
414446	25	f	9.96000000000000085	0	0.0400000000000000008	0	10	3	3	f	14	2020-07-16	\N	\N	\N	\N	1	\N	\N	\N	\N	\N	\N
414418	41	f	144.875	0	0	0	144.900000000000006	3	3	f	2	2020-07-16	\N	\N	\N	\N	6	\N	\N	\N	\N	\N	\N
414461	25	f	9.96000000000000085	0	0.0400000000000000008	0	10	3	3	f	29	2020-08-04	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414462	25	f	9.96000000000000085	0	0.0400000000000000008	0	10	3	3	f	30	2020-08-05	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414463	25	f	9.96000000000000085	0	0.0400000000000000008	0	10	3	3	f	31	2020-08-06	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414464	25	f	9.96000000000000085	0	0.0400000000000000008	0	10	3	3	f	32	2020-08-07	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414465	25	f	9.96000000000000085	0	0.0400000000000000008	0	10	3	3	f	33	2020-08-08	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414466	25	f	9.96000000000000085	0	0.0400000000000000008	0	10	3	3	f	34	2020-08-10	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414467	25	f	9.96000000000000085	0	0.0400000000000000008	0	10	3	3	f	35	2020-08-11	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414468	25	f	9.96000000000000085	0	0.0400000000000000008	0	10	3	3	f	36	2020-08-12	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414469	25	f	9.96000000000000085	0	0.0400000000000000008	0	10	3	3	f	37	2020-08-13	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414470	25	f	9.96000000000000085	0	0.0400000000000000008	0	10	3	3	f	38	2020-08-14	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414471	25	f	9.96000000000000085	0	0.0400000000000000008	0	10	3	3	f	39	2020-08-15	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414472	25	f	9.96000000000000085	0	0.0400000000000000008	0	10	3	3	f	40	2020-08-17	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414473	25	f	9.96000000000000085	0	0.0400000000000000008	0	10	3	3	f	41	2020-08-18	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414474	25	f	9.96000000000000085	0	0.0400000000000000008	0	10	3	3	f	42	2020-08-19	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414475	25	f	9.96000000000000085	0	0.0400000000000000008	0	10	3	3	f	43	2020-08-20	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414476	25	f	9.96000000000000085	0	0.0400000000000000008	0	10	3	3	f	44	2020-08-21	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414477	25	f	9.96000000000000085	0	0.0400000000000000008	0	10	3	3	f	45	2020-08-22	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414478	25	f	9.96000000000000085	0	0.0400000000000000008	0	10	3	3	f	46	2020-08-24	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414479	25	f	9.96000000000000085	0	0.0400000000000000008	0	10	3	3	f	47	2020-08-25	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414480	25	f	9.96000000000000085	0	0.0400000000000000008	0	10	3	3	f	48	2020-08-26	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414481	25	f	9.96000000000000085	0	0.0400000000000000008	0	10	3	3	f	49	2020-08-27	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414482	25	f	9.96000000000000085	0	0.0400000000000000008	0	10	3	3	f	50	2020-08-28	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414483	25	f	9.96000000000000085	0	0.0400000000000000008	0	10	3	3	f	51	2020-08-29	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414484	25	f	9.96000000000000085	0	0.0400000000000000008	0	10	3	3	f	52	2020-08-31	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414485	25	f	9.96000000000000085	0	0.0400000000000000008	0	10	3	3	f	53	2020-09-01	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414486	25	f	9.96000000000000085	0	0.0400000000000000008	0	10	3	3	f	54	2020-09-02	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414487	25	f	9.96000000000000085	0	0.0400000000000000008	0	10	3	3	f	55	2020-09-03	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414488	25	f	9.96000000000000085	0	0.0400000000000000008	0	10	3	3	f	56	2020-09-04	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414489	25	f	9.96000000000000085	0	0.0400000000000000008	0	10	3	3	f	57	2020-09-05	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414490	25	f	9.96000000000000085	0	0.0400000000000000008	0	10	3	3	f	58	2020-09-07	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414491	25	f	9.96000000000000085	0	0.0400000000000000008	0	10	3	3	f	59	2020-09-08	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414492	25	f	9.96000000000000085	0	0.0400000000000000008	0	10	3	3	f	60	2020-09-09	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414493	25	f	9.96000000000000085	0	0.0400000000000000008	0	10	3	3	f	61	2020-09-10	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414494	25	f	9.96000000000000085	0	0.0400000000000000008	0	10	3	3	f	62	2020-09-11	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414495	25	f	9.96000000000000085	0	0.0400000000000000008	0	10	3	3	f	63	2020-09-12	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414496	25	f	9.96000000000000085	0	0.0400000000000000008	0	10	3	3	f	64	2020-09-14	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414497	25	f	9.96000000000000085	0	0.0400000000000000008	0	10	3	3	f	65	2020-09-15	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414498	25	f	9.96000000000000085	0	0.0400000000000000008	0	10	3	3	f	66	2020-09-16	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414499	25	f	9.96000000000000085	0	0.0400000000000000008	0	10	3	3	f	67	2020-09-17	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414500	25	f	9.96000000000000085	0	0.0400000000000000008	0	10	3	3	f	68	2020-09-18	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414501	25	f	9.96000000000000085	0	0.0400000000000000008	0	10	3	3	f	69	2020-09-19	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414502	25	f	9.96000000000000085	0	0.0400000000000000008	0	10	3	3	f	70	2020-09-21	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414503	25	f	9.96000000000000085	0	0.0400000000000000008	0	10	3	3	f	71	2020-09-22	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414504	25	f	9.96000000000000085	0	0.0400000000000000008	0	10	3	3	f	72	2020-09-23	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414505	25	f	9.96000000000000085	0	0.0400000000000000008	0	10	3	3	f	73	2020-09-24	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414506	25	f	9.96000000000000085	0	0.0400000000000000008	0	10	3	3	f	74	2020-09-25	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414508	37	f	75	9	0	0	84	3	3	f	2	2020-07-31	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414509	37	f	75	9	0	0	84	3	3	f	3	2020-08-15	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414510	37	f	75	9	0	0	84	3	3	f	4	2020-08-31	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414511	37	f	75	9	0	0	84	3	3	f	5	2020-09-15	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414512	37	f	75	9	0	0	84	3	3	f	6	2020-09-30	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414514	29	f	125	0	0	0	125	3	3	f	2	2020-07-30	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414516	33	f	139.300000000000011	0	0	0	139.300000000000011	3	3	f	2	2020-07-31	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414518	45	f	200	6	0	0	206	3	3	f	1	2020-07-18	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414517	28	t	81	1	0	0	82	3	3	f	1	2020-07-01	2020-07-03 10:36:55.254	81	1	0	\N	\N	\N	2020-07-03 10:36:55	82	\N	\N
414519	45	f	200	6	0	0	206	3	3	f	2	2020-08-03	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414520	45	f	200	6	0	0	206	3	3	f	3	2020-08-18	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414521	45	f	200	6	0	0	206	3	3	f	4	2020-09-02	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414515	33	f	139.300000000000011	0	0	0	139.300000000000011	3	3	f	1	2020-07-16	\N	\N	\N	\N	0.699999999999999956	\N	\N	\N	\N	\N	\N
414513	29	f	125	0	0	0	125	3	3	f	1	2020-07-15	\N	\N	\N	\N	0.699999999999999956	\N	\N	\N	\N	\N	\N
414522	45	f	200	6	0	0	206	3	3	f	5	2020-09-17	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414523	45	f	200	6	0	0	206	3	3	f	6	2020-10-02	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414524	49	f	330.019999999999982	10	0	0	340.022111481468016	3	3	f	1	2020-08-03	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414525	49	f	333.319999999999993	6.70000000000000018	0	0	340.022111481468016	3	3	f	2	2020-09-02	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414526	49	f	336.649999999999977	3.37000000000000011	0	0	340.022111481468016	3	3	f	3	2020-10-02	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414547	67	t	19.2300000000000004	1.35000000000000009	0.0200000000000000004	0	20.6000000000000014	3	3	f	9	2020-07-16	2020-07-20 09:24:39.384	19.2300000000000004	1.35000000000000009	0.0200000000000000004	1	\N	\N	2020-07-20 09:24:40	20.6000000000000014	\N	\N
414527	51	t	500	20	0	0	520	3	3	f	1	2020-07-18	2020-07-03 14:06:46.35	500	20	0	\N	\N	\N	2020-07-03 02:06:46	520	\N	\N
414565	72	t	38.4600000000000009	1.72999999999999998	0.0100000000000000002	0	40.2000000000000028	3	3	f	1	2020-06-27	2020-07-10 09:50:45.473	38.4600000000000009	1.72999999999999998	0.0100000000000000002	\N	\N	\N	2020-07-10 09:50:46	40.2000000000000028	\N	\N
414528	56	t	1000	80	0	0	1080	3	3	f	1	2020-08-03	2020-07-03 14:33:48.265	1000	80	0	\N	\N	\N	2020-07-03 02:33:48	1080	\N	\N
414390	27	t	780	1.94999999999999996	0.0500000000000000028	0	782	3	3	f	1	2020-07-07	2020-07-03 14:42:52.498	780	1.94999999999999996	0.0500000000000000028	\N	\N	\N	2020-07-03 02:42:53	782	\N	\N
414532	62	f	54.3400000000000034	3.39625000000000021	0	0	57.75	3	3	f	4	2020-08-01	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414533	62	f	54.3400000000000034	3.39625000000000021	0	0	57.75	3	3	f	5	2020-08-08	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414534	62	f	54.3400000000000034	3.39625000000000021	0	0	57.75	3	3	f	6	2020-08-15	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414535	62	f	54.3400000000000034	3.39625000000000021	0	0	57.75	3	3	f	7	2020-08-22	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414536	62	f	54.3400000000000034	3.39625000000000021	0	0	57.75	3	3	f	8	2020-08-29	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414537	62	f	54.3400000000000034	3.39625000000000021	0	0	57.75	3	3	f	9	2020-09-05	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414538	62	f	54.3400000000000034	3.39625000000000021	0	0	57.75	3	3	f	10	2020-09-12	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414553	67	f	19.2300000000000004	1.35000000000000009	0.0200000000000000004	0	20.6000000000000014	3	3	f	15	2020-07-23	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414554	67	f	19.2300000000000004	1.35000000000000009	0.0200000000000000004	0	20.6000000000000014	3	3	f	16	2020-07-24	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414555	67	f	19.2300000000000004	1.35000000000000009	0.0200000000000000004	0	20.6000000000000014	3	3	f	17	2020-07-25	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414556	67	f	19.2300000000000004	1.35000000000000009	0.0200000000000000004	0	20.6000000000000014	3	3	f	18	2020-07-27	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414557	67	f	19.2300000000000004	1.35000000000000009	0.0200000000000000004	0	20.6000000000000014	3	3	f	19	2020-07-28	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414558	67	f	19.2300000000000004	1.35000000000000009	0.0200000000000000004	0	20.6000000000000014	3	3	f	20	2020-07-30	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414559	67	f	19.2300000000000004	1.35000000000000009	0.0200000000000000004	0	20.6000000000000014	3	3	f	21	2020-07-31	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414560	67	f	19.2300000000000004	1.35000000000000009	0.0200000000000000004	0	20.6000000000000014	3	3	f	22	2020-08-01	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414561	67	f	19.2300000000000004	1.35000000000000009	0.0200000000000000004	0	20.6000000000000014	3	3	f	23	2020-08-03	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414562	67	f	19.2300000000000004	1.35000000000000009	0.0200000000000000004	0	20.6000000000000014	3	3	f	24	2020-08-04	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414563	67	f	19.2300000000000004	1.35000000000000009	0.0200000000000000004	0	20.6000000000000014	3	3	f	25	2020-08-05	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414564	67	f	19.2300000000000004	1.35000000000000009	0.0200000000000000004	0	20.6000000000000014	3	3	f	26	2020-08-06	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414566	72	t	38.4600000000000009	1.72999999999999998	0.0100000000000000002	0	40.2000000000000028	3	3	f	2	2020-06-30	2020-07-11 09:22:27.398	38.4600000000000009	1.72999999999999998	0.0100000000000000002	\N	\N	\N	2020-07-11 09:22:28	40.2000000000000028	\N	\N
414539	67	t	19.2300000000000004	1.35000000000000009	0.0200000000000000004	0	20.6000000000000014	3	3	f	1	2020-07-07	2020-07-11 09:21:50.594	19.2300000000000004	1.35000000000000009	0.0200000000000000004	\N	\N	\N	2020-07-11 09:21:51	20.6000000000000014	\N	\N
414567	72	t	38.4600000000000009	1.72999999999999998	0.0100000000000000002	0	40.2000000000000028	3	3	f	3	2020-07-01	2020-07-13 13:31:46.833	38.4600000000000009	1.72999999999999998	0.0100000000000000002	\N	\N	\N	2020-07-13 01:31:47	40.2000000000000028	\N	\N
414529	62	t	54.3400000000000034	3.39625000000000021	0	0	57.75	3	3	f	1	2020-07-11	2020-07-11 10:39:19.591	54.3400000000000034	3.39599999999999991	0	\N	\N	\N	2020-07-11 10:39:20	57.7359999999999971	\N	\N
414568	72	t	38.4600000000000009	1.72999999999999998	0.0100000000000000002	0	40.2000000000000028	3	3	f	4	2020-07-02	2020-07-13 13:31:46.833	38.4600000000000009	1.72999999999999998	0.0100000000000000002	\N	\N	\N	2020-07-13 01:31:48	40.2000000000000028	\N	\N
414569	72	t	38.4600000000000009	1.72999999999999998	0.0100000000000000002	0	40.2000000000000028	3	3	f	5	2020-07-03	2020-07-16 11:14:10.63	38.4600000000000009	1.72999999999999998	0.0100000000000000002	\N	\N	\N	2020-07-16 11:14:11	40.2000000000000028	\N	\N
414540	67	t	19.2300000000000004	1.35000000000000009	0.0200000000000000004	0	20.6000000000000014	3	3	f	2	2020-07-08	2020-07-13 13:32:56.153	19.2300000000000004	1.35000000000000009	0.0200000000000000004	\N	\N	\N	2020-07-13 01:32:57	20.6000000000000014	\N	\N
414541	67	t	19.2300000000000004	1.35000000000000009	0.0200000000000000004	0	20.6000000000000014	3	3	f	3	2020-07-09	2020-07-13 13:32:56.153	19.2300000000000004	1.35000000000000009	0.0200000000000000004	\N	\N	\N	2020-07-13 01:32:58	20.6000000000000014	\N	\N
414542	67	t	19.2300000000000004	1.35000000000000009	0.0200000000000000004	0	20.6000000000000014	3	3	f	4	2020-07-10	2020-07-16 11:09:12.362	19.2300000000000004	1.35000000000000009	0.0200000000000000004	\N	\N	\N	2020-07-16 11:09:13	20.6000000000000014	\N	\N
414543	67	t	19.2300000000000004	1.35000000000000009	0.0200000000000000004	0	20.6000000000000014	3	3	f	5	2020-07-11	2020-07-16 11:09:12.362	19.2300000000000004	1.35000000000000009	0.0200000000000000004	\N	\N	\N	2020-07-16 11:09:14	20.6000000000000014	\N	\N
414570	72	t	38.4600000000000009	1.72999999999999998	0.0100000000000000002	0	40.2000000000000028	3	3	f	6	2020-07-04	2020-07-16 11:14:10.63	38.4600000000000009	1.72999999999999998	0.0100000000000000002	\N	\N	\N	2020-07-16 11:14:12	40.2000000000000028	\N	\N
414571	72	t	38.4600000000000009	1.72999999999999998	0.0100000000000000002	0	40.2000000000000028	3	3	f	7	2020-07-06	2020-07-16 11:14:10.63	38.4600000000000009	1.72999999999999998	0.0100000000000000002	\N	\N	\N	2020-07-16 11:14:13	40.2000000000000028	\N	\N
414545	67	t	19.2300000000000004	1.35000000000000009	0.0200000000000000004	0	20.6000000000000014	3	3	f	7	2020-07-14	2020-07-16 13:24:13.146	19.2300000000000004	1.35000000000000009	0.0200000000000000004	\N	\N	\N	2020-07-16 01:24:13	20.6000000000000014	\N	\N
414530	62	t	54.3400000000000034	3.39625000000000021	0	0	57.75	3	3	f	2	2020-07-18	2020-07-17 10:35:54.417	54.3400000000000034	3.39599999999999991	0	\N	\N	\N	2020-07-17 10:35:55	57.7359999999999971	\N	\N
414573	72	t	38.4600000000000009	1.72999999999999998	0.0100000000000000002	0	40.2000000000000028	3	3	f	9	2020-07-08	2020-07-20 09:30:38.929	38.4600000000000009	1.72999999999999998	0.0100000000000000002	\N	\N	\N	2020-07-20 09:30:40	40.2000000000000028	\N	\N
414574	72	t	38.4600000000000009	1.72999999999999998	0.0100000000000000002	0	40.2000000000000028	3	3	f	10	2020-07-09	2020-07-20 14:39:56.119	38.4600000000000009	1.72999999999999998	0.0100000000000000002	\N	\N	\N	2020-07-20 02:39:56	40.2000000000000028	\N	\N
414575	72	t	38.4600000000000009	1.72999999999999998	0.0100000000000000002	0	40.2000000000000028	3	3	f	11	2020-07-10	2020-07-22 13:25:56.572	38.4600000000000009	1.72999999999999998	0.0100000000000000002	\N	\N	\N	2020-07-22 01:25:57	40.2000000000000028	\N	\N
414576	72	t	38.4600000000000009	1.72999999999999998	0.0100000000000000002	0	40.2000000000000028	3	3	f	12	2020-07-11	2020-07-22 13:25:56.572	38.4600000000000009	1.72999999999999998	0.0100000000000000002	\N	\N	\N	2020-07-22 01:25:58	40.2000000000000028	\N	\N
414549	67	t	19.2300000000000004	1.35000000000000009	0.0200000000000000004	0	20.6000000000000014	3	3	f	11	2020-07-18	2020-07-22 13:43:21.858	19.2300000000000004	1.35000000000000009	0.0200000000000000004	\N	\N	\N	2020-07-22 01:43:22	20.6000000000000014	\N	\N
414551	67	t	19.2300000000000004	1.35000000000000009	0.0200000000000000004	0	20.6000000000000014	3	3	f	13	2020-07-21	2020-07-24 12:27:29.954	19.2300000000000004	1.35000000000000009	0.0200000000000000004	\N	\N	\N	2020-07-24 12:27:30	20.6000000000000014	\N	\N
414552	67	t	19.2300000000000004	1.35000000000000009	0.0200000000000000004	0	20.6000000000000014	3	3	f	14	2020-07-22	2020-07-25 12:06:41.747	19.2300000000000004	1.35000000000000009	0.0200000000000000004	\N	\N	\N	2020-07-25 12:06:42	20.6000000000000014	\N	\N
414346	15	t	7.69000000000000039	0.46000000000000002	0.0500000000000000028	0	8.19999999999999929	3	3	f	5	2020-07-06	2020-07-06 14:48:43.91	7.69000000000000039	0.46000000000000002	0.0500000000000000028	\N	\N	\N	2020-07-06 02:48:44	8.19999999999999929	\N	\N
414612	84	t	8.67999999999999972	0.0400000000000000008	0	0	8.69999999999999929	3	3	f	10	2020-07-20	2020-07-08 22:30:14.892	8.67999999999999972	0.0400000000000000008	0	\N	\N	\N	2020-07-08 10:30:24	8.72000000000000064	\N	\N
414591	74	t	21.2800000000000011	0.100000000000000006	0.0200000000000000004	0	21.3999999999999986	3	3	f	1	2020-07-08	2020-07-07 11:57:04.758	21.2800000000000011	0.100000000000000006	0.0200000000000000004	\N	\N	\N	2020-07-07 11:57:05	21.3999999999999986	\N	\N
414603	84	t	8.67999999999999972	0.0400000000000000008	0	0	8.69999999999999929	3	3	f	1	2020-07-09	2020-07-08 22:30:14.892	8.67999999999999972	0.0400000000000000008	0	\N	\N	\N	2020-07-08 10:30:15	8.72000000000000064	\N	\N
414592	74	t	21.2800000000000011	0.100000000000000006	0.0200000000000000004	0	21.3999999999999986	3	3	f	2	2020-07-09	2020-07-07 11:57:04.758	21.2800000000000011	0.100000000000000006	0.0200000000000000004	\N	\N	\N	2020-07-07 11:57:06	21.3999999999999986	\N	\N
414608	84	t	8.67999999999999972	0.0400000000000000008	0	0	8.69999999999999929	3	3	f	6	2020-07-15	2020-07-08 22:30:14.892	8.67999999999999972	0.0400000000000000008	0	\N	\N	\N	2020-07-08 10:30:20	8.72000000000000064	\N	\N
414593	74	t	21.2800000000000011	0.100000000000000006	0.0200000000000000004	0	21.3999999999999986	3	3	f	3	2020-07-10	2020-07-07 11:57:04.758	21.2800000000000011	0.100000000000000006	0.0200000000000000004	\N	\N	\N	2020-07-07 11:57:07	21.3999999999999986	\N	\N
414604	84	t	8.67999999999999972	0.0400000000000000008	0	0	8.69999999999999929	3	3	f	2	2020-07-10	2020-07-08 22:30:14.892	8.67999999999999972	0.0400000000000000008	0	\N	\N	\N	2020-07-08 10:30:16	8.72000000000000064	\N	\N
414594	74	t	21.2800000000000011	0.100000000000000006	0.0200000000000000004	0	21.3999999999999986	3	3	f	4	2020-07-11	2020-07-07 11:57:04.758	21.2800000000000011	0.100000000000000006	0.0200000000000000004	\N	\N	\N	2020-07-07 11:57:08	21.3999999999999986	\N	\N
414595	74	t	21.2800000000000011	0.100000000000000006	0.0200000000000000004	0	21.3999999999999986	3	3	f	5	2020-07-13	2020-07-07 11:57:04.758	21.2800000000000011	0.100000000000000006	0.0200000000000000004	\N	\N	\N	2020-07-07 11:57:09	21.3999999999999986	\N	\N
414605	84	t	8.67999999999999972	0.0400000000000000008	0	0	8.69999999999999929	3	3	f	3	2020-07-11	2020-07-08 22:30:14.892	8.67999999999999972	0.0400000000000000008	0	\N	\N	\N	2020-07-08 10:30:17	8.72000000000000064	\N	\N
414596	74	t	21.2800000000000011	0.100000000000000006	0.0200000000000000004	0	21.3999999999999986	3	3	f	6	2020-07-14	2020-07-07 11:57:04.758	21.2800000000000011	0.100000000000000006	0.0200000000000000004	\N	\N	\N	2020-07-07 11:57:10	21.3999999999999986	\N	\N
414609	84	t	8.67999999999999972	0.0400000000000000008	0	0	8.69999999999999929	3	3	f	7	2020-07-16	2020-07-08 22:30:14.892	8.67999999999999972	0.0400000000000000008	0	\N	\N	\N	2020-07-08 10:30:21	8.72000000000000064	\N	\N
414606	84	t	8.67999999999999972	0.0400000000000000008	0	0	8.69999999999999929	3	3	f	4	2020-07-13	2020-07-08 22:30:14.892	8.67999999999999972	0.0400000000000000008	0	\N	\N	\N	2020-07-08 10:30:18	8.72000000000000064	\N	\N
414613	84	t	8.67999999999999972	0.0400000000000000008	0	0	8.69999999999999929	3	3	f	11	2020-07-21	2020-07-08 22:30:14.892	8.67999999999999972	0.0400000000000000008	0	\N	\N	\N	2020-07-08 10:30:25	8.72000000000000064	\N	\N
414607	84	t	8.67999999999999972	0.0400000000000000008	0	0	8.69999999999999929	3	3	f	5	2020-07-14	2020-07-08 22:30:14.892	8.67999999999999972	0.0400000000000000008	0	\N	\N	\N	2020-07-08 10:30:19	8.72000000000000064	\N	\N
414610	84	t	8.67999999999999972	0.0400000000000000008	0	0	8.69999999999999929	3	3	f	8	2020-07-17	2020-07-08 22:30:14.892	8.67999999999999972	0.0400000000000000008	0	\N	\N	\N	2020-07-08 10:30:22	8.72000000000000064	\N	\N
414597	74	t	21.2800000000000011	0.100000000000000006	0.0200000000000000004	0	21.3999999999999986	3	3	f	7	2020-07-15	2020-07-08 22:32:06.163	21.2800000000000011	0.100000000000000006	0.0200000000000000004	\N	\N	\N	2020-07-08 10:32:06	21.3999999999999986	\N	\N
414611	84	t	8.67999999999999972	0.0400000000000000008	0	0	8.69999999999999929	3	3	f	9	2020-07-18	2020-07-08 22:30:14.892	8.67999999999999972	0.0400000000000000008	0	\N	\N	\N	2020-07-08 10:30:23	8.72000000000000064	\N	\N
414600	74	t	21.2800000000000011	0.100000000000000006	0.0200000000000000004	0	21.3999999999999986	3	3	f	10	2020-07-18	2020-07-08 22:32:06.163	21.2800000000000011	0.100000000000000006	0.0200000000000000004	\N	\N	\N	2020-07-08 10:32:09	21.3999999999999986	\N	\N
414614	84	t	8.67999999999999972	0.0400000000000000008	0	0	8.69999999999999929	3	3	f	12	2020-07-22	2020-07-08 22:30:14.892	8.67999999999999972	0.0400000000000000008	0	\N	\N	\N	2020-07-08 10:30:26	8.72000000000000064	\N	\N
414598	74	t	21.2800000000000011	0.100000000000000006	0.0200000000000000004	0	21.3999999999999986	3	3	f	8	2020-07-16	2020-07-08 22:32:06.163	21.2800000000000011	0.100000000000000006	0.0200000000000000004	\N	\N	\N	2020-07-08 10:32:07	21.3999999999999986	\N	\N
414615	84	t	8.67999999999999972	0.0400000000000000008	0	0	8.69999999999999929	3	3	f	13	2020-07-23	2020-07-08 22:30:14.892	8.67999999999999972	0.0400000000000000008	0	\N	\N	\N	2020-07-08 10:30:27	8.72000000000000064	\N	\N
414599	74	t	21.2800000000000011	0.100000000000000006	0.0200000000000000004	0	21.3999999999999986	3	3	f	9	2020-07-17	2020-07-08 22:32:06.163	21.2800000000000011	0.100000000000000006	0.0200000000000000004	\N	\N	\N	2020-07-08 10:32:08	21.3999999999999986	\N	\N
414601	74	t	21.2800000000000011	0.100000000000000006	0.0200000000000000004	0	21.3999999999999986	3	3	f	11	2020-07-20	2020-07-08 22:32:06.163	21.2800000000000011	0.100000000000000006	0.0200000000000000004	\N	\N	\N	2020-07-08 10:32:10	21.3999999999999986	\N	\N
414602	74	t	21.2800000000000011	0.100000000000000006	0.0200000000000000004	0	21.3999999999999986	3	3	f	12	2020-07-21	2020-07-08 22:32:06.163	21.2800000000000011	0.100000000000000006	0.0200000000000000004	\N	\N	\N	2020-07-08 10:32:11	21.3999999999999986	\N	\N
414618	86	t	26.9200000000000017	3.22999999999999998	0.0500000000000000028	0	30.1999999999999993	3	3	f	3	2020-07-11	2020-07-13 16:12:56.265	26.9200000000000017	3.22999999999999998	0.0500000000000000028	\N	\N	\N	2020-07-13 04:12:56	30.1999999999999993	\N	\N
414619	86	t	26.9200000000000017	3.22999999999999998	0.0500000000000000028	0	30.1999999999999993	3	3	f	4	2020-07-13	2020-07-14 15:56:00.412	26.9200000000000017	3.22999999999999998	0.0500000000000000028	\N	\N	\N	2020-07-14 03:56:01	30.1999999999999993	\N	\N
414620	86	t	26.9200000000000017	3.22999999999999998	0.0500000000000000028	0	30.1999999999999993	3	3	f	5	2020-07-14	2020-07-15 15:55:55.313	26.9200000000000017	3.22999999999999998	0.0500000000000000028	\N	\N	\N	2020-07-15 03:55:56	30.1999999999999993	\N	\N
414581	72	t	38.4600000000000009	1.72999999999999998	0.0100000000000000002	0	40.2000000000000028	3	3	f	17	2020-07-17	2020-07-22 13:25:56.572	38.4600000000000009	1.72999999999999998	0.0100000000000000002	\N	\N	\N	2020-07-22 01:26:03	40.2000000000000028	\N	\N
414622	86	t	26.9200000000000017	3.22999999999999998	0.0500000000000000028	0	30.1999999999999993	3	3	f	7	2020-07-16	2020-07-17 18:50:09.447	26.9200000000000017	3.22999999999999998	0.0500000000000000028	\N	\N	\N	2020-07-17 06:50:10	30.1999999999999993	\N	\N
414623	86	t	26.9200000000000017	3.22999999999999998	0.0500000000000000028	0	30.1999999999999993	3	3	f	8	2020-07-17	2020-07-20 21:56:50.719	26.9200000000000017	3.22999999999999998	0.0500000000000000028	\N	\N	\N	2020-07-20 09:56:51	30.1999999999999993	\N	\N
414577	72	t	38.4600000000000009	1.72999999999999998	0.0100000000000000002	0	40.2000000000000028	3	3	f	13	2020-07-13	2020-07-22 13:25:56.572	38.4600000000000009	1.72999999999999998	0.0100000000000000002	\N	\N	\N	2020-07-22 01:25:59	40.2000000000000028	\N	\N
414578	72	t	38.4600000000000009	1.72999999999999998	0.0100000000000000002	0	40.2000000000000028	3	3	f	14	2020-07-14	2020-07-22 13:25:56.572	38.4600000000000009	1.72999999999999998	0.0100000000000000002	\N	\N	\N	2020-07-22 01:26:00	40.2000000000000028	\N	\N
414579	72	t	38.4600000000000009	1.72999999999999998	0.0100000000000000002	0	40.2000000000000028	3	3	f	15	2020-07-15	2020-07-22 13:25:56.572	38.4600000000000009	1.72999999999999998	0.0100000000000000002	\N	\N	\N	2020-07-22 01:26:01	40.2000000000000028	\N	\N
414582	72	t	38.4600000000000009	1.72999999999999998	0.0100000000000000002	0	40.2000000000000028	3	3	f	18	2020-07-18	2020-07-22 13:25:56.572	38.4600000000000009	1.72999999999999998	0.0100000000000000002	\N	\N	\N	2020-07-22 01:26:04	40.2000000000000028	\N	\N
414584	72	t	38.4600000000000009	1.72999999999999998	0.0100000000000000002	0	40.2000000000000028	3	3	f	20	2020-07-21	2020-07-22 13:25:56.572	38.4600000000000009	1.72999999999999998	0.0100000000000000002	\N	\N	\N	2020-07-22 01:26:06	40.2000000000000028	\N	\N
414585	72	t	38.4600000000000009	1.72999999999999998	0.0100000000000000002	0	40.2000000000000028	3	3	f	21	2020-07-22	2020-07-22 13:25:56.572	38.4600000000000009	1.72999999999999998	0.0100000000000000002	\N	\N	\N	2020-07-22 01:26:07	40.2000000000000028	\N	\N
414586	72	t	38.4600000000000009	1.72999999999999998	0.0100000000000000002	0	40.2000000000000028	3	3	f	22	2020-07-23	2020-07-22 13:25:56.572	38.4600000000000009	1.72999999999999998	0.0100000000000000002	\N	\N	\N	2020-07-22 01:26:08	40.2000000000000028	\N	\N
414588	72	t	38.4600000000000009	1.72999999999999998	0.0100000000000000002	0	40.2000000000000028	3	3	f	24	2020-07-25	2020-07-22 13:25:56.572	38.4600000000000009	1.72999999999999998	0.0100000000000000002	\N	\N	\N	2020-07-22 01:26:09	40.2000000000000028	\N	\N
414589	72	t	38.4600000000000009	1.72999999999999998	0.0100000000000000002	0	40.2000000000000028	3	3	f	25	2020-07-27	2020-07-22 13:25:56.572	38.4600000000000009	1.72999999999999998	0.0100000000000000002	\N	\N	\N	2020-07-22 01:26:10	40.2000000000000028	\N	\N
414590	72	f	38.4600000000000009	1.72999999999999998	0.0100000000000000002	0	40.2000000000000028	3	3	f	26	2020-07-28	2020-07-22 13:25:56.572	30.6999999999999993	\N	\N	\N	\N	\N	2020-07-22 01:26:11	30.6999999999999993	\N	\N
414626	86	t	26.9200000000000017	3.22999999999999998	0.0500000000000000028	0	30.1999999999999993	3	3	f	11	2020-07-21	2020-07-22 19:53:47.33	26.9200000000000017	3.22999999999999998	0.0500000000000000028	\N	\N	\N	2020-07-22 07:53:48	30.1999999999999993	\N	\N
414633	86	t	26.9200000000000017	3.22999999999999998	0.0500000000000000028	0	30.1999999999999993	3	3	f	18	2020-07-30	2020-07-30 16:29:56.483	26.9200000000000017	3.22999999999999998	0.0500000000000000028	\N	\N	\N	2020-07-30 04:29:57	30.1999999999999993	\N	\N
414660	85	t	19.2300000000000004	2.31000000000000005	0.0599999999999999978	0	21.6000000000000014	3	3	f	19	2020-07-31	2020-07-31 15:42:25.508	19.2300000000000004	2.31000000000000005	0.0599999999999999978	\N	\N	\N	2020-07-31 03:42:26	21.6000000000000014	\N	\N
414642	85	t	19.2300000000000004	2.31000000000000005	0.0599999999999999978	0	21.6000000000000014	3	3	f	1	2020-07-09	2020-07-10 22:11:39.689	19.2300000000000004	2.31000000000000005	0.0599999999999999978	\N	\N	\N	2020-07-10 10:11:40	21.6000000000000014	\N	\N
414644	85	t	19.2300000000000004	2.31000000000000005	0.0599999999999999978	0	21.6000000000000014	3	3	f	3	2020-07-11	2020-07-13 16:12:01.557	19.2300000000000004	2.31000000000000005	0.0599999999999999978	\N	\N	\N	2020-07-13 04:12:02	21.6000000000000014	\N	\N
414635	86	t	26.9200000000000017	3.22999999999999998	0.0500000000000000028	0	30.1999999999999993	3	3	f	20	2020-08-01	2020-08-01 18:41:38.54	26.9200000000000017	3.22999999999999998	0.0500000000000000028	\N	\N	\N	2020-08-01 06:41:38	30.1999999999999993	\N	\N
414646	85	t	19.2300000000000004	2.31000000000000005	0.0599999999999999978	0	21.6000000000000014	3	3	f	5	2020-07-14	2020-07-15 15:55:15.609	19.2300000000000004	2.31000000000000005	0.0599999999999999978	\N	\N	\N	2020-07-15 03:55:16	21.6000000000000014	\N	\N
414648	85	t	19.2300000000000004	2.31000000000000005	0.0599999999999999978	0	21.6000000000000014	3	3	f	7	2020-07-16	2020-07-17 18:48:24.575	19.2300000000000004	2.31000000000000005	0.0599999999999999978	\N	\N	\N	2020-07-17 06:48:25	21.6000000000000014	\N	\N
414662	85	t	19.2300000000000004	2.31000000000000005	0.0599999999999999978	0	21.6000000000000014	3	3	f	21	2020-08-03	2020-08-01 18:44:29.884	19.2300000000000004	2.31000000000000005	0.0599999999999999978	\N	\N	\N	2020-08-01 06:44:30	21.6000000000000014	\N	\N
414650	85	t	19.2300000000000004	2.31000000000000005	0.0599999999999999978	0	21.6000000000000014	3	3	f	9	2020-07-18	2020-07-20 21:53:28.198	19.2300000000000004	2.31000000000000005	0.0599999999999999978	\N	\N	\N	2020-07-20 09:53:29	21.6000000000000014	\N	\N
414652	85	t	19.2300000000000004	2.31000000000000005	0.0599999999999999978	0	21.6000000000000014	3	3	f	11	2020-07-21	2020-07-22 19:51:56.716	19.2300000000000004	2.31000000000000005	0.0599999999999999978	\N	\N	\N	2020-07-22 07:51:58	21.6000000000000014	\N	\N
414664	85	t	19.2300000000000004	2.31000000000000005	0.0599999999999999978	0	21.6000000000000014	3	3	f	23	2020-08-05	2020-08-01 18:44:29.884	19.2300000000000004	2.31000000000000005	0.0599999999999999978	\N	\N	\N	2020-08-01 06:44:32	21.6000000000000014	\N	\N
414627	86	t	26.9200000000000017	3.22999999999999998	0.0500000000000000028	0	30.1999999999999993	3	3	f	12	2020-07-22	2020-07-23 18:15:16.644	26.9200000000000017	3.22999999999999998	0.0500000000000000028	\N	\N	\N	2020-07-23 06:15:17	30.1999999999999993	\N	\N
414654	85	t	19.2300000000000004	2.31000000000000005	0.0599999999999999978	0	21.6000000000000014	3	3	f	13	2020-07-23	2020-07-25 17:21:06.28	19.2300000000000004	2.31000000000000005	0.0599999999999999978	\N	\N	\N	2020-07-25 05:21:06	21.6000000000000014	\N	\N
414666	85	t	19.2300000000000004	2.31000000000000005	0.0599999999999999978	0	21.6000000000000014	3	3	f	25	2020-08-07	2020-08-01 18:44:29.884	19.2300000000000004	2.31000000000000005	0.0599999999999999978	\N	\N	\N	2020-08-01 06:44:34	21.6000000000000014	\N	\N
414629	86	t	26.9200000000000017	3.22999999999999998	0.0500000000000000028	0	30.1999999999999993	3	3	f	14	2020-07-24	2020-07-25 17:22:07.51	26.9200000000000017	3.22999999999999998	0.0500000000000000028	\N	\N	\N	2020-07-25 05:22:08	30.1999999999999993	\N	\N
414656	85	t	19.2300000000000004	2.31000000000000005	0.0599999999999999978	0	21.6000000000000014	3	3	f	15	2020-07-25	2020-07-29 16:14:46.839	19.2300000000000004	2.31000000000000005	0.0599999999999999978	\N	\N	\N	2020-07-29 04:14:47	21.6000000000000014	\N	\N
414637	86	t	26.9200000000000017	3.22999999999999998	0.0500000000000000028	0	30.1999999999999993	3	3	f	22	2020-08-04	2020-08-04 20:51:46.575	26.9200000000000017	3.22999999999999998	0.0500000000000000028	\N	\N	\N	2020-08-04 08:51:47	30.1999999999999993	\N	\N
414631	86	t	26.9200000000000017	3.22999999999999998	0.0500000000000000028	0	30.1999999999999993	3	3	f	16	2020-07-27	2020-07-29 16:17:39.254	26.9200000000000017	3.22999999999999998	0.0500000000000000028	\N	\N	\N	2020-07-29 04:17:40	30.1999999999999993	\N	\N
414658	85	t	19.2300000000000004	2.31000000000000005	0.0599999999999999978	0	21.6000000000000014	3	3	f	17	2020-07-28	2020-07-30 16:25:51.996	19.2300000000000004	2.31000000000000005	0.0599999999999999978	\N	\N	\N	2020-07-30 04:25:52	21.6000000000000014	\N	\N
414639	86	t	26.9200000000000017	3.22999999999999998	0.0500000000000000028	0	30.1999999999999993	3	3	f	24	2020-08-06	2020-08-06 18:11:56.332	26.9200000000000017	3.22999999999999998	0.0500000000000000028	\N	\N	\N	2020-08-06 06:11:56	30.1999999999999993	\N	\N
414641	86	t	26.9200000000000017	3.22999999999999998	0.0500000000000000028	0	30.1999999999999993	3	3	f	26	2020-08-08	2020-08-07 17:27:27.807	26.9200000000000017	3.22999999999999998	0.0500000000000000028	\N	\N	\N	2020-08-07 05:27:29	30.1999999999999993	\N	\N
414673	97	f	50	1.5	0	0	51.5	3	3	f	1	2020-07-16	\N	\N	\N	\N	6	\N	\N	\N	\N	\N	\N
414668	87	t	300	24	0	0	324	3	3	f	1	2020-07-10	2020-07-09 13:07:22.183	300	24	0	\N	\N	\N	2020-07-09 01:07:22	324	\N	\N
414670	96	f	71.4500000000000028	5.71600000000000019	0.0100000000000000002	0	77.2000000000000028	3	3	f	2	2020-07-23	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414671	96	f	71.4500000000000028	5.71600000000000019	0.0100000000000000002	0	77.2000000000000028	3	3	f	3	2020-07-30	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414672	96	f	71.4500000000000028	5.71600000000000019	0.0100000000000000002	0	77.2000000000000028	3	3	f	4	2020-08-06	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414674	97	f	50	1.5	0	0	51.5	3	3	f	2	2020-07-23	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414675	97	f	50	1.5	0	0	51.5	3	3	f	3	2020-07-30	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414676	97	f	50	1.5	0	0	51.5	3	3	f	4	2020-08-06	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414677	97	f	50	1.5	0	0	51.5	3	3	f	5	2020-08-13	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414678	97	f	50	1.5	0	0	51.5	3	3	f	6	2020-08-20	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414679	97	f	50	1.5	0	0	51.5	3	3	f	7	2020-08-27	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414680	97	f	50	1.5	0	0	51.5	3	3	f	8	2020-09-03	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414681	97	f	50	1.5	0	0	51.5	3	3	f	9	2020-09-10	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414682	97	f	50	1.5	0	0	51.5	3	3	f	10	2020-09-17	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414683	97	f	50	1.5	0	0	51.5	3	3	f	11	2020-09-24	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414684	97	f	50	1.5	0	0	51.5	3	3	f	12	2020-10-01	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414686	63	f	106.5625	12.7874999999999996	0	0	119.349999999999994	3	3	f	2	2020-07-18	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414687	63	f	106.5625	12.7874999999999996	0	0	119.349999999999994	3	3	f	3	2020-07-25	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414688	63	f	106.5625	12.7874999999999996	0	0	119.349999999999994	3	3	f	4	2020-08-01	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414689	63	f	106.5625	12.7874999999999996	0	0	119.349999999999994	3	3	f	5	2020-08-08	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414690	63	f	106.5625	12.7874999999999996	0	0	119.349999999999994	3	3	f	6	2020-08-15	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414691	63	f	106.5625	12.7874999999999996	0	0	119.349999999999994	3	3	f	7	2020-08-22	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414692	63	f	106.5625	12.7874999999999996	0	0	119.349999999999994	3	3	f	8	2020-08-29	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414693	63	f	106.5625	12.7874999999999996	0	0	119.349999999999994	3	3	f	9	2020-09-05	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414694	63	f	106.5625	12.7874999999999996	0	0	119.349999999999994	3	3	f	10	2020-09-12	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414695	63	f	106.5625	12.7874999999999996	0	0	119.349999999999994	3	3	f	11	2020-09-19	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414696	63	f	106.5625	12.7874999999999996	0	0	119.349999999999994	3	3	f	12	2020-09-26	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414697	63	f	106.5625	12.7874999999999996	0	0	119.349999999999994	3	3	f	13	2020-10-03	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414698	63	f	106.5625	12.7874999999999996	0	0	119.349999999999994	3	3	f	14	2020-10-10	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414699	63	f	106.5625	12.7874999999999996	0	0	119.349999999999994	3	3	f	15	2020-10-17	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414700	63	f	106.5625	12.7874999999999996	0	0	119.349999999999994	3	3	f	16	2020-10-24	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414702	95	f	187.5	30	0	0	217.5	3	3	f	2	2020-07-23	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414703	95	f	187.5	30	0	0	217.5	3	3	f	3	2020-07-30	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414704	95	f	187.5	30	0	0	217.5	3	3	f	4	2020-08-06	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414705	95	f	187.5	30	0	0	217.5	3	3	f	5	2020-08-13	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414706	95	f	187.5	30	0	0	217.5	3	3	f	6	2020-08-20	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414685	63	t	106.5625	12.7874999999999996	0	0	119.349999999999994	3	3	f	1	2020-07-11	2020-07-10 14:46:31.89	106.561999999999998	12.7880000000000003	0	\N	\N	\N	2020-07-10 02:46:52	119.349999999999994	\N	\N
414643	85	t	19.2300000000000004	2.31000000000000005	0.0599999999999999978	0	21.6000000000000014	3	3	f	2	2020-07-10	2020-07-11 19:21:16.297	19.2300000000000004	2.31000000000000005	0.0599999999999999978	\N	\N	\N	2020-07-11 07:21:16	21.6000000000000014	\N	\N
414645	85	t	19.2300000000000004	2.31000000000000005	0.0599999999999999978	0	21.6000000000000014	3	3	f	4	2020-07-13	2020-07-14 15:56:26.452	19.2300000000000004	2.31000000000000005	0.0599999999999999978	\N	\N	\N	2020-07-14 03:56:27	21.6000000000000014	\N	\N
414701	95	f	187.5	30	0	0	217.5	3	3	f	1	2020-07-16	\N	\N	\N	\N	6	\N	\N	\N	\N	\N	\N
414669	96	f	71.4500000000000028	5.71600000000000019	0.0100000000000000002	0	77.2000000000000028	3	3	f	1	2020-07-16	\N	\N	\N	\N	6	\N	\N	\N	\N	\N	\N
414649	85	t	19.2300000000000004	2.31000000000000005	0.0599999999999999978	0	21.6000000000000014	3	3	f	8	2020-07-17	2020-07-20 21:53:28.198	19.2300000000000004	2.31000000000000005	0.0599999999999999978	\N	\N	\N	2020-07-20 09:53:28	21.6000000000000014	\N	\N
414653	85	t	19.2300000000000004	2.31000000000000005	0.0599999999999999978	0	21.6000000000000014	3	3	f	12	2020-07-22	2020-07-23 18:17:25.557	19.2300000000000004	2.31000000000000005	0.0599999999999999978	\N	\N	\N	2020-07-23 06:17:26	21.6000000000000014	\N	\N
414655	85	t	19.2300000000000004	2.31000000000000005	0.0599999999999999978	0	21.6000000000000014	3	3	f	14	2020-07-24	2020-07-25 17:21:06.28	19.2300000000000004	2.31000000000000005	0.0599999999999999978	\N	\N	\N	2020-07-25 05:21:07	21.6000000000000014	\N	\N
414628	86	t	26.9200000000000017	3.22999999999999998	0.0500000000000000028	0	30.1999999999999993	3	3	f	13	2020-07-23	2020-07-25 17:22:07.51	26.9200000000000017	3.22999999999999998	0.0500000000000000028	\N	\N	\N	2020-07-25 05:22:07	30.1999999999999993	\N	\N
414630	86	t	26.9200000000000017	3.22999999999999998	0.0500000000000000028	0	30.1999999999999993	3	3	f	15	2020-07-25	2020-07-29 16:17:39.254	26.9200000000000017	3.22999999999999998	0.0500000000000000028	\N	\N	\N	2020-07-29 04:17:39	30.1999999999999993	\N	\N
414632	86	t	26.9200000000000017	3.22999999999999998	0.0500000000000000028	0	30.1999999999999993	3	3	f	17	2020-07-28	2020-07-29 16:17:39.254	26.9200000000000017	3.22999999999999998	0.0500000000000000028	\N	\N	\N	2020-07-29 04:17:41	30.1999999999999993	\N	\N
414661	85	t	19.2300000000000004	2.31000000000000005	0.0599999999999999978	0	21.6000000000000014	3	3	f	20	2020-08-01	2020-07-31 15:42:25.508	19.2300000000000004	2.31000000000000005	0.0599999999999999978	\N	\N	\N	2020-07-31 03:42:27	21.6000000000000014	\N	\N
414634	86	t	26.9200000000000017	3.22999999999999998	0.0500000000000000028	0	30.1999999999999993	3	3	f	19	2020-07-31	2020-07-31 15:43:28.77	26.9200000000000017	3.22999999999999998	0.0500000000000000028	\N	\N	\N	2020-07-31 03:43:29	30.1999999999999993	\N	\N
414663	85	t	19.2300000000000004	2.31000000000000005	0.0599999999999999978	0	21.6000000000000014	3	3	f	22	2020-08-04	2020-08-01 18:44:29.884	19.2300000000000004	2.31000000000000005	0.0599999999999999978	\N	\N	\N	2020-08-01 06:44:31	21.6000000000000014	\N	\N
414667	85	t	19.2300000000000004	2.31000000000000005	0.0599999999999999978	0	21.6000000000000014	3	3	f	26	2020-08-08	2020-08-01 18:44:29.884	19.2300000000000004	2.31000000000000005	0.0599999999999999978	\N	\N	\N	2020-08-01 06:44:35	21.6000000000000014	\N	\N
414636	86	t	26.9200000000000017	3.22999999999999998	0.0500000000000000028	0	30.1999999999999993	3	3	f	21	2020-08-03	2020-08-03 21:46:16.308	26.9200000000000017	3.22999999999999998	0.0500000000000000028	\N	\N	\N	2020-08-03 09:46:16	30.1999999999999993	\N	\N
414640	86	t	26.9200000000000017	3.22999999999999998	0.0500000000000000028	0	30.1999999999999993	3	3	f	25	2020-08-07	2020-08-07 17:27:27.807	26.9200000000000017	3.22999999999999998	0.0500000000000000028	\N	\N	\N	2020-08-07 05:27:28	30.1999999999999993	\N	\N
414707	95	f	187.5	30	0	0	217.5	3	3	f	7	2020-08-27	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414708	95	f	187.5	30	0	0	217.5	3	3	f	8	2020-09-03	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414709	90	f	73.769999999999996	112	0	0	185.773023694257006	3	3	f	1	2020-08-08	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414710	90	f	79.6700000000000017	106.099999999999994	0	0	185.773023694257006	3	3	f	2	2020-09-07	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414711	90	f	86.0499999999999972	99.7199999999999989	0	0	185.773023694257006	3	3	f	3	2020-10-07	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414712	90	f	92.9300000000000068	92.8400000000000034	0	0	185.773023694257006	3	3	f	4	2020-11-06	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414713	90	f	100.359999999999999	85.4099999999999966	0	0	185.773023694257006	3	3	f	5	2020-12-07	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414714	90	f	108.390000000000001	77.3799999999999955	0	0	185.773023694257006	3	3	f	6	2021-01-06	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414715	90	f	117.060000000000002	68.7099999999999937	0	0	185.773023694257006	3	3	f	7	2021-02-05	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414716	90	f	126.430000000000007	59.3400000000000034	0	0	185.773023694257006	3	3	f	8	2021-03-08	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414717	90	f	136.539999999999992	49.2299999999999969	0	0	185.773023694257006	3	3	f	9	2021-04-07	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414718	90	f	147.469999999999999	38.2999999999999972	0	0	185.773023694257006	3	3	f	10	2021-05-07	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414719	90	f	159.259999999999991	26.5100000000000016	0	0	185.773023694257006	3	3	f	11	2021-06-07	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414720	90	f	172	13.7699999999999996	0	0	185.773023694257006	3	3	f	12	2021-07-07	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414722	93	f	62.5	20	0	0	82.5	3	3	f	2	2020-08-08	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414723	93	f	62.5	20	0	0	82.5	3	3	f	3	2020-08-24	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414724	93	f	62.5	20	0	0	82.5	3	3	f	4	2020-09-08	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414725	93	f	62.5	20	0	0	82.5	3	3	f	5	2020-09-23	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414726	93	f	62.5	20	0	0	82.5	3	3	f	6	2020-10-09	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414727	93	f	62.5	20	0	0	82.5	3	3	f	7	2020-10-24	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414728	93	f	62.5	20	0	0	82.5	3	3	f	8	2020-11-09	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414730	92	f	58.2899999999999991	18.6499999999999986	0.0599999999999999978	0	77	3	3	f	2	2020-08-08	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414731	92	f	58.2899999999999991	18.6499999999999986	0.0599999999999999978	0	77	3	3	f	3	2020-08-24	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414732	92	f	58.2899999999999991	18.6499999999999986	0.0599999999999999978	0	77	3	3	f	4	2020-09-08	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414733	92	f	58.2899999999999991	18.6499999999999986	0.0599999999999999978	0	77	3	3	f	5	2020-09-23	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414734	92	f	58.2899999999999991	18.6499999999999986	0.0599999999999999978	0	77	3	3	f	6	2020-10-09	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414735	92	f	58.2899999999999991	18.6499999999999986	0.0599999999999999978	0	77	3	3	f	7	2020-10-24	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414736	92	f	58.2899999999999991	18.6499999999999986	0.0599999999999999978	0	77	3	3	f	8	2020-11-09	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414738	94	f	65.6200000000000045	21	0.0800000000000000017	0	86.7000000000000028	3	3	f	2	2020-08-08	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414739	94	f	65.6200000000000045	21	0.0800000000000000017	0	86.7000000000000028	3	3	f	3	2020-08-24	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414740	94	f	65.6200000000000045	21	0.0800000000000000017	0	86.7000000000000028	3	3	f	4	2020-09-08	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414741	94	f	65.6200000000000045	21	0.0800000000000000017	0	86.7000000000000028	3	3	f	5	2020-09-23	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414742	94	f	65.6200000000000045	21	0.0800000000000000017	0	86.7000000000000028	3	3	f	6	2020-10-09	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414743	94	f	65.6200000000000045	21	0.0800000000000000017	0	86.7000000000000028	3	3	f	7	2020-10-24	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414744	94	f	65.6200000000000045	21	0.0800000000000000017	0	86.7000000000000028	3	3	f	8	2020-11-09	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414745	68	f	308.029999999999973	80	0	0	388.03351404632798	3	3	f	1	2020-08-05	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414746	68	f	332.670000000000016	55.3599999999999994	0	0	388.03351404632798	3	3	f	2	2020-09-04	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414747	68	f	359.29000000000002	28.7399999999999984	0	0	388.03351404632798	3	3	f	3	2020-10-05	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414749	71	f	66.6666666666666998	0.5	0.0100000000000000002	0	67.2000000000000028	3	3	f	2	2020-07-20	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414750	71	f	66.6666666666666998	0.5	0.0100000000000000002	0	67.2000000000000028	3	3	f	3	2020-07-27	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414751	88	f	176	14.0800000000000001	0.0200000000000000004	0	190.099999999999994	3	3	f	1	2020-07-24	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414752	88	f	176	14.0800000000000001	0.0200000000000000004	0	190.099999999999994	3	3	f	2	2020-08-08	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414754	89	f	125	25	0	0	150	3	3	f	2	2020-07-23	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414755	89	f	125	25	0	0	150	3	3	f	3	2020-07-30	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414756	89	f	125	25	0	0	150	3	3	f	4	2020-08-06	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414757	89	f	125	25	0	0	150	3	3	f	5	2020-08-13	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414758	89	f	125	25	0	0	150	3	3	f	6	2020-08-20	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414759	89	f	125	25	0	0	150	3	3	f	7	2020-08-27	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414760	89	f	125	25	0	0	150	3	3	f	8	2020-09-03	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414761	89	f	125	25	0	0	150	3	3	f	9	2020-09-10	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414762	89	f	125	25	0	0	150	3	3	f	10	2020-09-17	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414763	81	f	61.5	4.91999999999999993	0.0800000000000000017	0	66.5	3	3	f	1	2020-07-22	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414764	81	f	61.5	4.91999999999999993	0.0800000000000000017	0	66.5	3	3	f	2	2020-08-06	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414766	79	f	10.9250000000000007	0.873999999999999999	0	0	11.8000000000000007	3	3	f	2	2020-07-21	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414767	79	f	10.9250000000000007	0.873999999999999999	0	0	11.8000000000000007	3	3	f	3	2020-07-28	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414768	79	f	10.9250000000000007	0.873999999999999999	0	0	11.8000000000000007	3	3	f	4	2020-08-04	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414748	71	f	66.6666666666666998	0.5	0.0100000000000000002	0	67.2000000000000028	3	3	f	1	2020-07-13	\N	\N	\N	\N	6	\N	\N	\N	\N	\N	\N
414753	89	f	125	25	0	0	150	3	3	f	1	2020-07-16	\N	\N	\N	\N	6	\N	\N	\N	\N	\N	\N
414721	93	t	62.5	20	0	0	82.5	3	3	f	1	2020-07-24	2020-08-11 09:32:39.107	62.5	20	0	\N	\N	\N	2020-08-11 09:32:40	82.5	\N	\N
414737	94	t	65.6200000000000045	21	0.0800000000000000017	0	86.7000000000000028	3	3	f	1	2020-07-24	2020-08-11 09:35:41.955	65.6200000000000045	21	0.0800000000000000017	\N	\N	\N	2020-08-11 09:35:42	86.7000000000000028	\N	\N
414775	34	f	34.9166666666666998	1.0475000000000001	0	0	36	3	3	f	1	2020-07-08	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414777	34	f	34.9166666666666998	1.0475000000000001	0	0	36	3	3	f	3	2020-07-22	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414778	34	f	34.9166666666666998	1.0475000000000001	0	0	36	3	3	f	4	2020-07-30	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414779	34	f	34.9166666666666998	1.0475000000000001	0	0	36	3	3	f	5	2020-08-06	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414780	34	f	34.9166666666666998	1.0475000000000001	0	0	36	3	3	f	6	2020-08-13	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414781	34	f	34.9166666666666998	1.0475000000000001	0	0	36	3	3	f	7	2020-08-20	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414782	34	f	34.9166666666666998	1.0475000000000001	0	0	36	3	3	f	8	2020-08-27	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414783	34	f	34.9166666666666998	1.0475000000000001	0	0	36	3	3	f	9	2020-09-03	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414784	34	f	34.9166666666666998	1.0475000000000001	0	0	36	3	3	f	10	2020-09-10	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414785	34	f	34.9166666666666998	1.0475000000000001	0	0	36	3	3	f	11	2020-09-17	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414786	34	f	34.9166666666666998	1.0475000000000001	0	0	36	3	3	f	12	2020-09-24	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414787	47	f	440	4	0	0	444	3	3	f	1	2020-07-04	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414788	46	f	10.8000000000000007	0.110000000000000001	0	0	10.9000000000000004	3	3	f	1	2020-07-04	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414789	46	f	10.8000000000000007	0.110000000000000001	0	0	10.9000000000000004	3	3	f	2	2020-07-06	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414790	46	f	10.8000000000000007	0.110000000000000001	0	0	10.9000000000000004	3	3	f	3	2020-07-07	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414791	46	f	10.8000000000000007	0.110000000000000001	0	0	10.9000000000000004	3	3	f	4	2020-07-08	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414792	46	f	10.8000000000000007	0.110000000000000001	0	0	10.9000000000000004	3	3	f	5	2020-07-09	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414793	46	f	10.8000000000000007	0.110000000000000001	0	0	10.9000000000000004	3	3	f	6	2020-07-10	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414794	46	f	10.8000000000000007	0.110000000000000001	0	0	10.9000000000000004	3	3	f	7	2020-07-11	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414795	46	f	10.8000000000000007	0.110000000000000001	0	0	10.9000000000000004	3	3	f	8	2020-07-13	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414796	46	f	10.8000000000000007	0.110000000000000001	0	0	10.9000000000000004	3	3	f	9	2020-07-14	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414797	46	f	10.8000000000000007	0.110000000000000001	0	0	10.9000000000000004	3	3	f	10	2020-07-15	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414799	46	f	10.8000000000000007	0.110000000000000001	0	0	10.9000000000000004	3	3	f	12	2020-07-17	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414800	46	f	10.8000000000000007	0.110000000000000001	0	0	10.9000000000000004	3	3	f	13	2020-07-18	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414801	46	f	10.8000000000000007	0.110000000000000001	0	0	10.9000000000000004	3	3	f	14	2020-07-20	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414802	46	f	10.8000000000000007	0.110000000000000001	0	0	10.9000000000000004	3	3	f	15	2020-07-21	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414803	46	f	10.8000000000000007	0.110000000000000001	0	0	10.9000000000000004	3	3	f	16	2020-07-22	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414804	46	f	10.8000000000000007	0.110000000000000001	0	0	10.9000000000000004	3	3	f	17	2020-07-23	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414805	46	f	10.8000000000000007	0.110000000000000001	0	0	10.9000000000000004	3	3	f	18	2020-07-24	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414806	46	f	10.8000000000000007	0.110000000000000001	0	0	10.9000000000000004	3	3	f	19	2020-07-25	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414807	46	f	10.8000000000000007	0.110000000000000001	0	0	10.9000000000000004	3	3	f	20	2020-07-27	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414808	46	f	10.8000000000000007	0.110000000000000001	0	0	10.9000000000000004	3	3	f	21	2020-07-28	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414809	46	f	10.8000000000000007	0.110000000000000001	0	0	10.9000000000000004	3	3	f	22	2020-07-30	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414810	46	f	10.8000000000000007	0.110000000000000001	0	0	10.9000000000000004	3	3	f	23	2020-07-31	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414811	46	f	10.8000000000000007	0.110000000000000001	0	0	10.9000000000000004	3	3	f	24	2020-08-01	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414812	46	f	10.8000000000000007	0.110000000000000001	0	0	10.9000000000000004	3	3	f	25	2020-08-03	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414813	46	f	10.8000000000000007	0.110000000000000001	0	0	10.9000000000000004	3	3	f	26	2020-08-04	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414815	66	f	140	0	0	0	140	3	3	f	2	2020-07-20	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414816	66	f	140	0	0	0	140	3	3	f	3	2020-07-27	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414817	66	f	140	0	0	0	140	3	3	f	4	2020-08-03	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414348	15	t	7.69000000000000039	0.46000000000000002	0.0500000000000000028	0	8.19999999999999929	3	3	f	7	2020-07-08	2020-07-10 15:05:33.622	7.69000000000000039	0.46000000000000002	0.0500000000000000028	\N	\N	\N	2020-07-10 03:05:34	8.19999999999999929	\N	\N
414616	86	t	26.9200000000000017	3.22999999999999998	0.0500000000000000028	0	30.1999999999999993	3	3	f	1	2020-07-09	2020-07-10 22:12:21.138	26.9200000000000017	3.22999999999999998	0.0500000000000000028	\N	\N	\N	2020-07-10 10:12:21	30.1999999999999993	\N	\N
414773	55	t	210	2.10000000000000009	0	0	212.099999999999994	3	3	f	1	2020-07-18	2020-07-13 10:33:25.19	210	2.10000000000000009	0	\N	\N	\N	2020-07-13 10:33:26	212.099999999999994	\N	\N
414774	55	f	210	2.10000000000000009	0	0	212.099999999999994	3	3	f	2	2020-08-03	2020-07-13 10:33:25.19	133.900000000000006	\N	\N	\N	\N	\N	2020-07-13 10:33:27	133.900000000000006	\N	\N
414819	102	t	26.9200000000000017	3.22999999999999998	0.0500000000000000028	0	30.1999999999999993	3	3	f	2	2020-07-13	2020-07-14 15:55:30.724	26.9200000000000017	3.22999999999999998	0.0500000000000000028	\N	\N	\N	2020-07-14 03:55:31	30.1999999999999993	\N	\N
414820	102	t	26.9200000000000017	3.22999999999999998	0.0500000000000000028	0	30.1999999999999993	3	3	f	3	2020-07-14	2020-07-15 15:56:32.744	26.9200000000000017	3.22999999999999998	0.0500000000000000028	\N	\N	\N	2020-07-15 03:56:33	30.1999999999999993	\N	\N
414798	46	f	10.8000000000000007	0.110000000000000001	0	0	10.9000000000000004	3	3	f	11	2020-07-16	\N	\N	\N	\N	1	\N	\N	\N	\N	\N	\N
414814	66	f	140	0	0	0	140	3	3	f	1	2020-07-13	\N	\N	\N	\N	6	\N	\N	\N	\N	\N	\N
414776	34	f	34.9166666666666998	1.0475000000000001	0	0	36	3	3	f	2	2020-07-15	\N	\N	\N	\N	6	\N	\N	\N	\N	\N	\N
414770	77	t	60	2.39999999999999991	0	0	62.3999999999999986	3	3	f	2	2020-07-21	2020-07-17 09:30:59.136	60	2.39999999999999991	0	\N	\N	\N	2020-07-17 09:31:00	62.3999999999999986	\N	\N
414771	77	t	60	2.39999999999999991	0	0	62.3999999999999986	3	3	f	3	2020-07-28	2020-07-17 09:30:59.136	60	2.39999999999999991	0	\N	\N	\N	2020-07-17 09:31:01	62.3999999999999986	\N	\N
414821	102	t	26.9200000000000017	3.22999999999999998	0.0500000000000000028	0	30.1999999999999993	3	3	f	4	2020-07-15	2020-07-17 18:51:13.247	26.9200000000000017	3.22999999999999998	0.0500000000000000028	\N	\N	\N	2020-07-17 06:51:14	30.1999999999999993	\N	\N
414822	102	t	26.9200000000000017	3.22999999999999998	0.0500000000000000028	0	30.1999999999999993	3	3	f	5	2020-07-16	2020-07-17 18:51:13.247	26.9200000000000017	3.22999999999999998	0.0500000000000000028	\N	\N	\N	2020-07-17 06:51:15	30.1999999999999993	\N	\N
414823	102	t	26.9200000000000017	3.22999999999999998	0.0500000000000000028	0	30.1999999999999993	3	3	f	6	2020-07-17	2020-07-20 21:55:12.63	26.9200000000000017	3.22999999999999998	0.0500000000000000028	\N	\N	\N	2020-07-20 09:55:12	30.1999999999999993	\N	\N
414825	102	t	26.9200000000000017	3.22999999999999998	0.0500000000000000028	0	30.1999999999999993	3	3	f	8	2020-07-20	2020-07-22 19:52:41.57	26.9200000000000017	3.22999999999999998	0.0500000000000000028	\N	\N	\N	2020-07-22 07:52:42	30.1999999999999993	\N	\N
414826	102	t	26.9200000000000017	3.22999999999999998	0.0500000000000000028	0	30.1999999999999993	3	3	f	9	2020-07-21	2020-07-22 19:52:41.57	26.9200000000000017	3.22999999999999998	0.0500000000000000028	\N	\N	\N	2020-07-22 07:52:43	30.1999999999999993	\N	\N
414828	102	t	26.9200000000000017	3.22999999999999998	0.0500000000000000028	0	30.1999999999999993	3	3	f	11	2020-07-23	2020-07-22 19:52:41.57	26.9200000000000017	3.22999999999999998	0.0500000000000000028	\N	\N	\N	2020-07-22 07:52:45	30.1999999999999993	\N	\N
414830	102	t	26.9200000000000017	3.22999999999999998	0.0500000000000000028	0	30.1999999999999993	3	3	f	13	2020-07-25	2020-07-23 18:16:25.844	26.9200000000000017	3.22999999999999998	0.0500000000000000028	\N	\N	\N	2020-07-23 06:16:27	30.1999999999999993	\N	\N
414832	102	t	26.9200000000000017	3.22999999999999998	0.0500000000000000028	0	30.1999999999999993	3	3	f	15	2020-07-28	2020-07-25 17:22:58.899	26.9200000000000017	3.22999999999999998	0.0500000000000000028	\N	\N	\N	2020-07-25 05:22:59	30.1999999999999993	\N	\N
414834	102	t	26.9200000000000017	3.22999999999999998	0.0500000000000000028	0	30.1999999999999993	3	3	f	17	2020-07-31	2020-07-25 17:22:58.899	26.9200000000000017	3.22999999999999998	0.0500000000000000028	\N	\N	\N	2020-07-25 05:23:01	30.1999999999999993	\N	\N
414836	102	t	26.9200000000000017	3.22999999999999998	0.0500000000000000028	0	30.1999999999999993	3	3	f	19	2020-08-03	2020-07-25 17:22:58.899	26.9200000000000017	3.22999999999999998	0.0500000000000000028	\N	\N	\N	2020-07-25 05:23:03	30.1999999999999993	\N	\N
414838	102	t	26.9200000000000017	3.22999999999999998	0.0500000000000000028	0	30.1999999999999993	3	3	f	21	2020-08-05	2020-07-29 16:19:07.876	26.9200000000000017	3.22999999999999998	0.0500000000000000028	\N	\N	\N	2020-07-29 04:19:08	30.1999999999999993	\N	\N
414840	102	t	26.9200000000000017	3.22999999999999998	0.0500000000000000028	0	30.1999999999999993	3	3	f	23	2020-08-07	2020-07-29 16:19:07.876	26.9200000000000017	3.22999999999999998	0.0500000000000000028	\N	\N	\N	2020-07-29 04:19:10	30.1999999999999993	\N	\N
414842	102	t	26.9200000000000017	3.22999999999999998	0.0500000000000000028	0	30.1999999999999993	3	3	f	25	2020-08-10	2020-07-29 16:19:07.876	26.9200000000000017	3.22999999999999998	0.0500000000000000028	\N	\N	\N	2020-07-29 04:19:12	30.1999999999999993	\N	\N
414870	103	t	168	0	0	0	168	3	3	f	1	2020-07-12	2020-07-11 12:28:11.482	168	0	0	\N	\N	\N	2020-07-11 12:28:12	168	\N	\N
414871	106	f	203	0.761249999999999982	0.0400000000000000008	0	203.800000000000011	3	3	f	1	2020-07-18	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414617	86	t	26.9200000000000017	3.22999999999999998	0.0500000000000000028	0	30.1999999999999993	3	3	f	2	2020-07-10	2020-07-11 19:22:40.765	26.9200000000000017	3.22999999999999998	0.0500000000000000028	\N	\N	\N	2020-07-11 07:22:41	30.1999999999999993	\N	\N
414872	108	t	11.5399999999999991	0.92000000000000004	0.0400000000000000008	0	12.5	3	3	f	1	2020-07-14	2020-07-13 10:51:44.971	11.5399999999999991	0.92000000000000004	0.0400000000000000008	\N	\N	\N	2020-07-13 10:51:45	12.5	\N	\N
414873	108	t	11.5399999999999991	0.92000000000000004	0.0400000000000000008	0	12.5	3	3	f	2	2020-07-15	2020-07-13 13:00:06.769	11.5399999999999991	0.92000000000000004	0.0400000000000000008	\N	\N	\N	2020-07-13 01:00:07	12.5	\N	\N
414844	101	t	19.2300000000000004	1.54000000000000004	0.0299999999999999989	0	20.8000000000000007	3	3	f	1	2020-07-11	2020-07-13 16:14:18.207	19.2300000000000004	1.54000000000000004	0.0299999999999999989	\N	\N	\N	2020-07-13 04:14:18	20.8000000000000007	\N	\N
414845	101	t	19.2300000000000004	1.54000000000000004	0.0299999999999999989	0	20.8000000000000007	3	3	f	2	2020-07-13	2020-07-14 15:54:58.119	19.2300000000000004	1.54000000000000004	0.0299999999999999989	\N	\N	\N	2020-07-14 03:54:58	20.8000000000000007	\N	\N
414875	108	t	11.5399999999999991	0.92000000000000004	0.0400000000000000008	0	12.5	3	3	f	4	2020-07-17	2020-07-15 12:34:58.875	11.5399999999999991	0.92000000000000004	0.0400000000000000008	\N	\N	\N	2020-07-15 12:34:59	12.5	\N	\N
414846	101	t	19.2300000000000004	1.54000000000000004	0.0299999999999999989	0	20.8000000000000007	3	3	f	3	2020-07-14	2020-07-15 15:57:59.552	19.2300000000000004	1.54000000000000004	0.0299999999999999989	\N	\N	\N	2020-07-15 03:58:00	20.8000000000000007	\N	\N
414847	101	t	19.2300000000000004	1.54000000000000004	0.0299999999999999989	0	20.8000000000000007	3	3	f	4	2020-07-15	2020-07-15 15:57:59.552	19.2300000000000004	1.54000000000000004	0.0299999999999999989	\N	\N	\N	2020-07-15 03:58:01	20.8000000000000007	\N	\N
414877	108	t	11.5399999999999991	0.92000000000000004	0.0400000000000000008	0	12.5	3	3	f	6	2020-07-20	2020-07-16 09:58:12.533	11.5399999999999991	0.92000000000000004	0.0400000000000000008	\N	\N	\N	2020-07-16 09:58:14	12.5	\N	\N
414878	108	t	11.5399999999999991	0.92000000000000004	0.0400000000000000008	0	12.5	3	3	f	7	2020-07-21	2020-07-16 09:58:12.533	11.5399999999999991	0.92000000000000004	0.0400000000000000008	\N	\N	\N	2020-07-16 09:58:15	12.5	\N	\N
414880	108	t	11.5399999999999991	0.92000000000000004	0.0400000000000000008	0	12.5	3	3	f	9	2020-07-23	2020-07-16 09:58:12.533	11.5399999999999991	0.92000000000000004	0.0400000000000000008	\N	\N	\N	2020-07-16 09:58:17	12.5	\N	\N
414881	108	t	11.5399999999999991	0.92000000000000004	0.0400000000000000008	0	12.5	3	3	f	10	2020-07-24	2020-07-16 09:58:12.533	11.5399999999999991	0.92000000000000004	0.0400000000000000008	\N	\N	\N	2020-07-16 09:58:18	12.5	\N	\N
414882	108	t	11.5399999999999991	0.92000000000000004	0.0400000000000000008	0	12.5	3	3	f	11	2020-07-25	2020-07-16 09:58:12.533	11.5399999999999991	0.92000000000000004	0.0400000000000000008	\N	\N	\N	2020-07-16 09:58:19	12.5	\N	\N
414884	108	t	11.5399999999999991	0.92000000000000004	0.0400000000000000008	0	12.5	3	3	f	13	2020-07-28	2020-07-16 09:58:12.533	11.5399999999999991	0.92000000000000004	0.0400000000000000008	\N	\N	\N	2020-07-16 09:58:21	12.5	\N	\N
414885	108	t	11.5399999999999991	0.92000000000000004	0.0400000000000000008	0	12.5	3	3	f	14	2020-07-30	2020-07-16 09:58:12.533	11.5399999999999991	0.92000000000000004	0.0400000000000000008	\N	\N	\N	2020-07-16 09:58:22	12.5	\N	\N
414887	108	t	11.5399999999999991	0.92000000000000004	0.0400000000000000008	0	12.5	3	3	f	16	2020-08-01	2020-07-16 09:58:12.533	11.5399999999999991	0.92000000000000004	0.0400000000000000008	\N	\N	\N	2020-07-16 09:58:24	12.5	\N	\N
414888	108	t	11.5399999999999991	0.92000000000000004	0.0400000000000000008	0	12.5	3	3	f	17	2020-08-03	2020-07-16 09:58:12.533	11.5399999999999991	0.92000000000000004	0.0400000000000000008	\N	\N	\N	2020-07-16 09:58:25	12.5	\N	\N
414889	108	t	11.5399999999999991	0.92000000000000004	0.0400000000000000008	0	12.5	3	3	f	18	2020-08-04	2020-07-16 09:58:12.533	11.5399999999999991	0.92000000000000004	0.0400000000000000008	\N	\N	\N	2020-07-16 09:58:26	12.5	\N	\N
414891	108	t	11.5399999999999991	0.92000000000000004	0.0400000000000000008	0	12.5	3	3	f	20	2020-08-06	2020-07-16 09:58:12.533	11.5399999999999991	0.92000000000000004	0.0400000000000000008	\N	\N	\N	2020-07-16 09:58:28	12.5	\N	\N
414848	101	t	19.2300000000000004	1.54000000000000004	0.0299999999999999989	0	20.8000000000000007	3	3	f	5	2020-07-16	2020-07-16 19:30:48.132	19.2300000000000004	1.54000000000000004	0.0299999999999999989	\N	\N	\N	2020-07-16 07:30:48	20.8000000000000007	\N	\N
414850	101	t	19.2300000000000004	1.54000000000000004	0.0299999999999999989	0	20.8000000000000007	3	3	f	7	2020-07-18	2020-07-20 21:54:25.774	19.2300000000000004	1.54000000000000004	0.0299999999999999989	\N	\N	\N	2020-07-20 09:54:27	20.8000000000000007	\N	\N
414851	101	t	19.2300000000000004	1.54000000000000004	0.0299999999999999989	0	20.8000000000000007	3	3	f	8	2020-07-20	2020-07-20 21:54:25.774	19.2300000000000004	1.54000000000000004	0.0299999999999999989	\N	\N	\N	2020-07-20 09:54:28	20.8000000000000007	\N	\N
414827	102	t	26.9200000000000017	3.22999999999999998	0.0500000000000000028	0	30.1999999999999993	3	3	f	10	2020-07-22	2020-07-22 19:52:41.57	26.9200000000000017	3.22999999999999998	0.0500000000000000028	\N	\N	\N	2020-07-22 07:52:44	30.1999999999999993	\N	\N
414853	101	t	19.2300000000000004	1.54000000000000004	0.0299999999999999989	0	20.8000000000000007	3	3	f	10	2020-07-22	2020-07-23 18:14:35.493	19.2300000000000004	1.54000000000000004	0.0299999999999999989	\N	\N	\N	2020-07-23 06:14:36	20.8000000000000007	\N	\N
414829	102	t	26.9200000000000017	3.22999999999999998	0.0500000000000000028	0	30.1999999999999993	3	3	f	12	2020-07-24	2020-07-23 18:16:25.844	26.9200000000000017	3.22999999999999998	0.0500000000000000028	\N	\N	\N	2020-07-23 06:16:26	30.1999999999999993	\N	\N
414833	102	t	26.9200000000000017	3.22999999999999998	0.0500000000000000028	0	30.1999999999999993	3	3	f	16	2020-07-30	2020-07-25 17:22:58.899	26.9200000000000017	3.22999999999999998	0.0500000000000000028	\N	\N	\N	2020-07-25 05:23:00	30.1999999999999993	\N	\N
414835	102	t	26.9200000000000017	3.22999999999999998	0.0500000000000000028	0	30.1999999999999993	3	3	f	18	2020-08-01	2020-07-25 17:22:58.899	26.9200000000000017	3.22999999999999998	0.0500000000000000028	\N	\N	\N	2020-07-25 05:23:02	30.1999999999999993	\N	\N
414854	101	t	19.2300000000000004	1.54000000000000004	0.0299999999999999989	0	20.8000000000000007	3	3	f	11	2020-07-23	2020-07-25 17:23:31.322	19.2300000000000004	1.54000000000000004	0.0299999999999999989	\N	\N	\N	2020-07-25 05:23:31	20.8000000000000007	\N	\N
414855	101	t	19.2300000000000004	1.54000000000000004	0.0299999999999999989	0	20.8000000000000007	3	3	f	12	2020-07-24	2020-07-25 17:23:51.43	19.2300000000000004	1.54000000000000004	0.0299999999999999989	\N	\N	\N	2020-07-25 05:23:51	20.8000000000000007	\N	\N
414856	101	t	19.2300000000000004	1.54000000000000004	0.0299999999999999989	0	20.8000000000000007	3	3	f	13	2020-07-25	2020-07-29 16:15:59.388	19.2300000000000004	1.54000000000000004	0.0299999999999999989	\N	\N	\N	2020-07-29 04:16:00	20.8000000000000007	\N	\N
414858	101	t	19.2300000000000004	1.54000000000000004	0.0299999999999999989	0	20.8000000000000007	3	3	f	15	2020-07-28	2020-07-29 16:15:59.388	19.2300000000000004	1.54000000000000004	0.0299999999999999989	\N	\N	\N	2020-07-29 04:16:01	20.8000000000000007	\N	\N
414839	102	t	26.9200000000000017	3.22999999999999998	0.0500000000000000028	0	30.1999999999999993	3	3	f	22	2020-08-06	2020-07-29 16:19:07.876	26.9200000000000017	3.22999999999999998	0.0500000000000000028	\N	\N	\N	2020-07-29 04:19:09	30.1999999999999993	\N	\N
414843	102	t	26.9200000000000017	3.22999999999999998	0.0500000000000000028	0	30.1999999999999993	3	3	f	26	2020-08-11	2020-07-29 16:19:07.876	26.9200000000000017	3.22999999999999998	0.0500000000000000028	\N	\N	\N	2020-07-29 04:19:13	30.1999999999999993	\N	\N
414859	101	t	19.2300000000000004	1.54000000000000004	0.0299999999999999989	0	20.8000000000000007	3	3	f	16	2020-07-30	2020-07-30 16:29:09.675	19.2300000000000004	1.54000000000000004	0.0299999999999999989	\N	\N	\N	2020-07-30 04:29:10	20.8000000000000007	\N	\N
414860	101	t	19.2300000000000004	1.54000000000000004	0.0299999999999999989	0	20.8000000000000007	3	3	f	17	2020-07-31	2020-07-31 15:43:01.328	19.2300000000000004	1.54000000000000004	0.0299999999999999989	\N	\N	\N	2020-07-31 03:43:01	20.8000000000000007	\N	\N
414862	101	t	19.2300000000000004	1.54000000000000004	0.0299999999999999989	0	20.8000000000000007	3	3	f	19	2020-08-03	2020-08-03 21:47:15.84	19.2300000000000004	1.54000000000000004	0.0299999999999999989	\N	\N	\N	2020-08-03 09:47:15	20.8000000000000007	\N	\N
414863	101	t	19.2300000000000004	1.54000000000000004	0.0299999999999999989	0	20.8000000000000007	3	3	f	20	2020-08-04	2020-08-05 21:29:43.528	19.2300000000000004	1.54000000000000004	0.0299999999999999989	\N	\N	\N	2020-08-05 09:29:44	20.8000000000000007	\N	\N
414865	101	t	19.2300000000000004	1.54000000000000004	0.0299999999999999989	0	20.8000000000000007	3	3	f	22	2020-08-06	2020-08-06 18:12:38.596	19.2300000000000004	1.54000000000000004	0.0299999999999999989	\N	\N	\N	2020-08-06 06:12:39	20.8000000000000007	\N	\N
414866	101	t	19.2300000000000004	1.54000000000000004	0.0299999999999999989	0	20.8000000000000007	3	3	f	23	2020-08-07	2020-08-07 17:25:46.146	19.2300000000000004	1.54000000000000004	0.0299999999999999989	\N	\N	\N	2020-08-07 05:25:46	20.8000000000000007	\N	\N
414867	101	t	19.2300000000000004	1.54000000000000004	0.0299999999999999989	0	20.8000000000000007	3	3	f	24	2020-08-08	2020-08-07 17:25:46.146	19.2300000000000004	1.54000000000000004	0.0299999999999999989	\N	\N	\N	2020-08-07 05:25:47	20.8000000000000007	\N	\N
414869	101	t	19.2300000000000004	1.54000000000000004	0.0299999999999999989	0	20.8000000000000007	3	3	f	26	2020-08-11	2020-08-11 17:51:32.101	19.2300000000000004	1.54000000000000004	0.0299999999999999989	\N	\N	\N	2020-08-11 05:51:32	20.8000000000000007	\N	\N
414892	108	t	11.5399999999999991	0.92000000000000004	0.0400000000000000008	0	12.5	3	3	f	21	2020-08-07	2020-07-16 09:58:12.533	11.5399999999999991	0.92000000000000004	0.0400000000000000008	\N	\N	\N	2020-07-16 09:58:29	12.5	\N	\N
414894	108	t	11.5399999999999991	0.92000000000000004	0.0400000000000000008	0	12.5	3	3	f	23	2020-08-10	2020-07-16 09:58:12.533	11.5399999999999991	0.92000000000000004	0.0400000000000000008	\N	\N	\N	2020-07-16 09:58:31	12.5	\N	\N
414896	108	t	11.5399999999999991	0.92000000000000004	0.0400000000000000008	0	12.5	3	3	f	25	2020-08-12	2020-07-17 13:12:11.407	11.5399999999999991	0.92000000000000004	0.0400000000000000008	\N	\N	\N	2020-07-17 01:12:12	12.5	\N	\N
414340	11	t	350	21	0	0	371	3	3	f	1	2020-07-15	2020-07-13 11:23:16.608	350	21	0	\N	\N	\N	2020-07-13 11:23:17	371	\N	\N
414903	111	f	500	35	0	0	535	3	3	f	2	2020-08-12	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414818	102	t	26.9200000000000017	3.22999999999999998	0.0500000000000000028	0	30.1999999999999993	3	3	f	1	2020-07-11	2020-07-13 16:13:39.272	26.9200000000000017	3.22999999999999998	0.0500000000000000028	\N	\N	\N	2020-07-13 04:13:39	30.1999999999999993	\N	\N
414917	113	f	7.69000000000000039	0.689999999999999947	0.0200000000000000004	0	8.40000000000000036	3	3	f	14	2020-07-31	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414918	113	f	7.69000000000000039	0.689999999999999947	0.0200000000000000004	0	8.40000000000000036	3	3	f	15	2020-08-01	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414919	113	f	7.69000000000000039	0.689999999999999947	0.0200000000000000004	0	8.40000000000000036	3	3	f	16	2020-08-03	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414920	113	f	7.69000000000000039	0.689999999999999947	0.0200000000000000004	0	8.40000000000000036	3	3	f	17	2020-08-04	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414921	113	f	7.69000000000000039	0.689999999999999947	0.0200000000000000004	0	8.40000000000000036	3	3	f	18	2020-08-05	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414922	113	f	7.69000000000000039	0.689999999999999947	0.0200000000000000004	0	8.40000000000000036	3	3	f	19	2020-08-06	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414923	113	f	7.69000000000000039	0.689999999999999947	0.0200000000000000004	0	8.40000000000000036	3	3	f	20	2020-08-07	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414924	113	f	7.69000000000000039	0.689999999999999947	0.0200000000000000004	0	8.40000000000000036	3	3	f	21	2020-08-08	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414925	113	f	7.69000000000000039	0.689999999999999947	0.0200000000000000004	0	8.40000000000000036	3	3	f	22	2020-08-10	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414926	113	f	7.69000000000000039	0.689999999999999947	0.0200000000000000004	0	8.40000000000000036	3	3	f	23	2020-08-11	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414927	113	f	7.69000000000000039	0.689999999999999947	0.0200000000000000004	0	8.40000000000000036	3	3	f	24	2020-08-12	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414928	113	f	7.69000000000000039	0.689999999999999947	0.0200000000000000004	0	8.40000000000000036	3	3	f	25	2020-08-13	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414929	113	f	7.69000000000000039	0.689999999999999947	0.0200000000000000004	0	8.40000000000000036	3	3	f	26	2020-08-14	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414930	70	f	491.819999999999993	19.9699999999999989	0	0	511.793344541586009	3	3	f	1	2020-08-05	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414931	70	f	496.740000000000009	15.0500000000000007	0	0	511.793344541586009	3	3	f	2	2020-09-04	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414932	70	f	501.70999999999998	10.0800000000000001	0	0	511.793344541586009	3	3	f	3	2020-10-05	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414933	70	f	506.720000000000027	5.07000000000000028	0	0	511.793344541586009	3	3	f	4	2020-11-04	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414934	5	f	105.974999999999994	2.11949999999999994	0	0	108.099999999999994	3	3	f	1	2020-07-06	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414936	5	f	105.974999999999994	2.11949999999999994	0	0	108.099999999999994	3	3	f	3	2020-07-20	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414937	5	f	105.974999999999994	2.11949999999999994	0	0	108.099999999999994	3	3	f	4	2020-07-27	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414938	5	f	105.974999999999994	2.11949999999999994	0	0	108.099999999999994	3	3	f	5	2020-08-03	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414939	5	f	105.974999999999994	2.11949999999999994	0	0	108.099999999999994	3	3	f	6	2020-08-10	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414940	5	f	105.974999999999994	2.11949999999999994	0	0	108.099999999999994	3	3	f	7	2020-08-17	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414941	5	f	105.974999999999994	2.11949999999999994	0	0	108.099999999999994	3	3	f	8	2020-08-24	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414943	7	f	490.300000000000011	4.90000000000000036	0	0	495.199999999999989	3	3	f	2	2020-07-30	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414874	108	t	11.5399999999999991	0.92000000000000004	0.0400000000000000008	0	12.5	3	3	f	3	2020-07-16	2020-07-15 09:06:31.436	11.5399999999999991	0.92000000000000004	0.0400000000000000008	\N	\N	\N	2020-07-15 09:06:32	12.5	\N	\N
414950	118	f	11.5399999999999991	0.92000000000000004	0.0400000000000000008	0	12.5	3	3	f	7	2020-07-23	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414951	118	f	11.5399999999999991	0.92000000000000004	0.0400000000000000008	0	12.5	3	3	f	8	2020-07-24	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414935	5	f	105.974999999999994	2.11949999999999994	0	0	108.099999999999994	3	3	f	2	2020-07-13	\N	\N	\N	\N	6	\N	\N	\N	\N	\N	\N
414910	113	t	7.69000000000000039	0.689999999999999947	0.0200000000000000004	0	8.40000000000000036	3	3	f	7	2020-07-22	2020-07-24 12:26:34.331	7.69000000000000039	0.689999999999999947	0.0200000000000000004	\N	\N	\N	2020-07-24 12:26:35	8.40000000000000036	\N	\N
414944	118	t	11.5399999999999991	0.92000000000000004	0.0400000000000000008	0	12.5	3	3	f	1	2020-07-16	2020-07-17 09:24:46.444	11.5399999999999991	0.92000000000000004	0.0400000000000000008	1	\N	\N	2020-07-17 09:24:47	12.5	\N	\N
414906	113	t	7.69000000000000039	0.689999999999999947	0.0200000000000000004	0	8.40000000000000036	3	3	f	3	2020-07-17	2020-07-17 13:11:21.223	7.69000000000000039	0.689999999999999947	0.0200000000000000004	\N	\N	\N	2020-07-17 01:11:24	8.40000000000000036	\N	\N
414895	108	t	11.5399999999999991	0.92000000000000004	0.0400000000000000008	0	12.5	3	3	f	24	2020-08-11	2020-07-16 13:25:52.697	11.5399999999999991	0.92000000000000004	0.0400000000000000008	\N	\N	\N	2020-07-16 01:25:53	12.5	\N	\N
414904	113	t	7.69000000000000039	0.689999999999999947	0.0200000000000000004	0	8.40000000000000036	3	3	f	1	2020-07-15	2020-07-17 13:11:21.223	7.69000000000000039	0.689999999999999947	0.0200000000000000004	\N	\N	\N	2020-07-17 01:11:22	8.40000000000000036	\N	\N
414907	113	t	7.69000000000000039	0.689999999999999947	0.0200000000000000004	0	8.40000000000000036	3	3	f	4	2020-07-18	2020-07-17 13:11:21.223	7.69000000000000039	0.689999999999999947	0.0200000000000000004	\N	\N	\N	2020-07-17 01:11:25	8.40000000000000036	\N	\N
414909	113	t	7.69000000000000039	0.689999999999999947	0.0200000000000000004	0	8.40000000000000036	3	3	f	6	2020-07-21	2020-07-17 13:11:21.223	7.69000000000000039	0.689999999999999947	0.0200000000000000004	\N	\N	\N	2020-07-17 01:11:27	8.40000000000000036	\N	\N
414897	108	t	11.5399999999999991	0.92000000000000004	0.0400000000000000008	0	12.5	3	3	f	26	2020-08-13	2020-07-18 11:06:16.927	11.5399999999999991	0.92000000000000004	0.0400000000000000008	\N	\N	\N	2020-07-18 11:06:18	12.5	\N	\N
414945	118	t	11.5399999999999991	0.92000000000000004	0.0400000000000000008	0	12.5	3	3	f	2	2020-07-17	2020-07-22 12:56:07.241	11.5399999999999991	0.92000000000000004	0.0400000000000000008	\N	\N	\N	2020-07-22 12:56:07	12.5	\N	\N
414946	118	t	11.5399999999999991	0.92000000000000004	0.0400000000000000008	0	12.5	3	3	f	3	2020-07-18	2020-07-22 12:56:07.241	11.5399999999999991	0.92000000000000004	0.0400000000000000008	\N	\N	\N	2020-07-22 12:56:08	12.5	\N	\N
414947	118	t	11.5399999999999991	0.92000000000000004	0.0400000000000000008	0	12.5	3	3	f	4	2020-07-20	2020-07-22 12:56:07.241	11.5399999999999991	0.92000000000000004	0.0400000000000000008	\N	\N	\N	2020-07-22 12:56:09	12.5	\N	\N
414949	118	t	11.5399999999999991	0.92000000000000004	0.0400000000000000008	0	12.5	3	3	f	6	2020-07-22	2020-07-22 12:56:07.241	11.5399999999999991	0.92000000000000004	0.0400000000000000008	\N	\N	\N	2020-07-22 12:56:11	12.5	\N	\N
414911	113	t	7.69000000000000039	0.689999999999999947	0.0200000000000000004	0	8.40000000000000036	3	3	f	8	2020-07-23	2020-07-24 12:26:34.331	7.69000000000000039	0.689999999999999947	0.0200000000000000004	\N	\N	\N	2020-07-24 12:26:36	8.40000000000000036	\N	\N
414913	113	t	7.69000000000000039	0.689999999999999947	0.0200000000000000004	0	8.40000000000000036	3	3	f	10	2020-07-25	2020-07-24 12:26:34.331	7.69000000000000039	0.689999999999999947	0.0200000000000000004	\N	\N	\N	2020-07-24 12:26:37	8.40000000000000036	\N	\N
414914	113	t	7.69000000000000039	0.689999999999999947	0.0200000000000000004	0	8.40000000000000036	3	3	f	11	2020-07-27	2020-07-24 12:26:34.331	7.69000000000000039	0.689999999999999947	0.0200000000000000004	\N	\N	\N	2020-07-24 12:26:38	8.40000000000000036	\N	\N
414916	113	f	7.69000000000000039	0.689999999999999947	0.0200000000000000004	0	8.40000000000000036	3	3	f	13	2020-07-30	2020-07-24 12:26:34.331	0.100000000000000006	\N	\N	\N	\N	\N	2020-07-24 12:26:40	0.100000000000000006	\N	\N
414899	107	t	125	10	0	0	135	3	3	f	2	2020-07-25	2020-07-29 16:18:08.751	125	10	0	\N	\N	\N	2020-07-29 04:18:09	135	\N	\N
414900	107	t	125	10	0	0	135	3	3	f	3	2020-08-01	2020-08-01 18:45:19.525	125	10	0	\N	\N	\N	2020-08-01 06:45:20	135	\N	\N
414902	111	t	500	35	0	0	535	3	3	f	1	2020-07-28	2020-08-10 10:14:06.269	500	35	0	\N	\N	\N	2020-08-10 10:14:07	535	\N	\N
414952	118	f	11.5399999999999991	0.92000000000000004	0.0400000000000000008	0	12.5	3	3	f	9	2020-07-25	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414953	118	f	11.5399999999999991	0.92000000000000004	0.0400000000000000008	0	12.5	3	3	f	10	2020-07-27	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414954	118	f	11.5399999999999991	0.92000000000000004	0.0400000000000000008	0	12.5	3	3	f	11	2020-07-28	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414955	118	f	11.5399999999999991	0.92000000000000004	0.0400000000000000008	0	12.5	3	3	f	12	2020-07-30	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414956	118	f	11.5399999999999991	0.92000000000000004	0.0400000000000000008	0	12.5	3	3	f	13	2020-07-31	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414957	118	f	11.5399999999999991	0.92000000000000004	0.0400000000000000008	0	12.5	3	3	f	14	2020-08-01	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414958	118	f	11.5399999999999991	0.92000000000000004	0.0400000000000000008	0	12.5	3	3	f	15	2020-08-03	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414959	118	f	11.5399999999999991	0.92000000000000004	0.0400000000000000008	0	12.5	3	3	f	16	2020-08-04	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414960	118	f	11.5399999999999991	0.92000000000000004	0.0400000000000000008	0	12.5	3	3	f	17	2020-08-05	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414961	118	f	11.5399999999999991	0.92000000000000004	0.0400000000000000008	0	12.5	3	3	f	18	2020-08-06	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414962	118	f	11.5399999999999991	0.92000000000000004	0.0400000000000000008	0	12.5	3	3	f	19	2020-08-07	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414963	118	f	11.5399999999999991	0.92000000000000004	0.0400000000000000008	0	12.5	3	3	f	20	2020-08-08	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414964	118	f	11.5399999999999991	0.92000000000000004	0.0400000000000000008	0	12.5	3	3	f	21	2020-08-10	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414965	118	f	11.5399999999999991	0.92000000000000004	0.0400000000000000008	0	12.5	3	3	f	22	2020-08-11	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414966	118	f	11.5399999999999991	0.92000000000000004	0.0400000000000000008	0	12.5	3	3	f	23	2020-08-12	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414967	118	f	11.5399999999999991	0.92000000000000004	0.0400000000000000008	0	12.5	3	3	f	24	2020-08-13	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414968	118	f	11.5399999999999991	0.92000000000000004	0.0400000000000000008	0	12.5	3	3	f	25	2020-08-14	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414969	118	f	11.5399999999999991	0.92000000000000004	0.0400000000000000008	0	12.5	3	3	f	26	2020-08-15	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414942	7	f	490.300000000000011	4.90000000000000036	0	0	495.199999999999989	3	3	f	1	2020-07-14	\N	\N	\N	\N	1	\N	\N	\N	\N	\N	\N
414402	43	f	62.71875	0	0	0	62.75	3	3	f	2	2020-07-16	\N	\N	\N	\N	6	\N	\N	\N	\N	\N	\N
414765	79	f	10.9250000000000007	0.873999999999999999	0	0	11.8000000000000007	3	3	f	1	2020-07-14	\N	\N	\N	\N	6	\N	\N	\N	\N	\N	\N
414876	108	t	11.5399999999999991	0.92000000000000004	0.0400000000000000008	0	12.5	3	3	f	5	2020-07-18	2020-07-16 09:58:12.533	11.5399999999999991	0.92000000000000004	0.0400000000000000008	\N	\N	\N	2020-07-16 09:58:13	12.5	\N	\N
414879	108	t	11.5399999999999991	0.92000000000000004	0.0400000000000000008	0	12.5	3	3	f	8	2020-07-22	2020-07-16 09:58:12.533	11.5399999999999991	0.92000000000000004	0.0400000000000000008	\N	\N	\N	2020-07-16 09:58:16	12.5	\N	\N
414883	108	t	11.5399999999999991	0.92000000000000004	0.0400000000000000008	0	12.5	3	3	f	12	2020-07-27	2020-07-16 09:58:12.533	11.5399999999999991	0.92000000000000004	0.0400000000000000008	\N	\N	\N	2020-07-16 09:58:20	12.5	\N	\N
414886	108	t	11.5399999999999991	0.92000000000000004	0.0400000000000000008	0	12.5	3	3	f	15	2020-07-31	2020-07-16 09:58:12.533	11.5399999999999991	0.92000000000000004	0.0400000000000000008	\N	\N	\N	2020-07-16 09:58:23	12.5	\N	\N
414890	108	t	11.5399999999999991	0.92000000000000004	0.0400000000000000008	0	12.5	3	3	f	19	2020-08-05	2020-07-16 09:58:12.533	11.5399999999999991	0.92000000000000004	0.0400000000000000008	\N	\N	\N	2020-07-16 09:58:27	12.5	\N	\N
414893	108	t	11.5399999999999991	0.92000000000000004	0.0400000000000000008	0	12.5	3	3	f	22	2020-08-08	2020-07-16 09:58:12.533	11.5399999999999991	0.92000000000000004	0.0400000000000000008	\N	\N	\N	2020-07-16 09:58:30	12.5	\N	\N
414544	67	t	19.2300000000000004	1.35000000000000009	0.0200000000000000004	0	20.6000000000000014	3	3	f	6	2020-07-13	2020-07-16 11:09:12.362	19.2300000000000004	1.35000000000000009	0.0200000000000000004	\N	\N	\N	2020-07-16 11:09:15	20.6000000000000014	\N	\N
414384	22	t	226.300000000000011	0.565749999999999975	0.0299999999999999989	0	226.900000000000006	3	3	f	1	2020-07-07	2020-07-16 13:04:50.313	226.300000000000011	0.565999999999999948	0.0299999999999999989	\N	\N	\N	2020-07-16 01:04:51	226.895999999999987	\N	\N
414970	117	f	529.940000000000055	137.629999999999995	0	0	667.572857565303025	3	3	f	1	2020-08-13	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414971	117	f	572.330000000000041	95.2399999999999949	0	0	667.572857565303025	3	3	f	2	2020-09-12	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414972	117	f	618.120000000000005	49.4500000000000028	0	0	667.572857565303025	3	3	f	3	2020-10-12	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414981	115	f	9.69999999999999929	0.110000000000000001	0	0	9.80000000000000071	3	3	f	9	2020-07-24	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414982	115	f	9.69999999999999929	0.110000000000000001	0	0	9.80000000000000071	3	3	f	10	2020-07-25	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414983	115	f	9.69999999999999929	0.110000000000000001	0	0	9.80000000000000071	3	3	f	11	2020-07-27	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414984	115	f	9.69999999999999929	0.110000000000000001	0	0	9.80000000000000071	3	3	f	12	2020-07-28	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414985	115	f	9.69999999999999929	0.110000000000000001	0	0	9.80000000000000071	3	3	f	13	2020-07-30	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414986	115	f	9.69999999999999929	0.110000000000000001	0	0	9.80000000000000071	3	3	f	14	2020-07-31	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414987	115	f	9.69999999999999929	0.110000000000000001	0	0	9.80000000000000071	3	3	f	15	2020-08-01	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414988	115	f	9.69999999999999929	0.110000000000000001	0	0	9.80000000000000071	3	3	f	16	2020-08-03	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414989	115	f	9.69999999999999929	0.110000000000000001	0	0	9.80000000000000071	3	3	f	17	2020-08-04	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414990	115	f	9.69999999999999929	0.110000000000000001	0	0	9.80000000000000071	3	3	f	18	2020-08-05	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414991	115	f	9.69999999999999929	0.110000000000000001	0	0	9.80000000000000071	3	3	f	19	2020-08-06	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414992	115	f	9.69999999999999929	0.110000000000000001	0	0	9.80000000000000071	3	3	f	20	2020-08-07	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414993	115	f	9.69999999999999929	0.110000000000000001	0	0	9.80000000000000071	3	3	f	21	2020-08-08	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414994	115	f	9.69999999999999929	0.110000000000000001	0	0	9.80000000000000071	3	3	f	22	2020-08-10	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414995	115	f	9.69999999999999929	0.110000000000000001	0	0	9.80000000000000071	3	3	f	23	2020-08-11	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414973	115	t	9.69999999999999929	0.110000000000000001	0	0	9.80000000000000071	3	3	f	1	2020-07-15	2020-07-17 13:13:45.686	9.69999999999999929	0.110000000000000001	0	\N	\N	\N	2020-07-17 01:13:46	9.8100000000000005	\N	\N
414974	115	t	9.69999999999999929	0.110000000000000001	0	0	9.80000000000000071	3	3	f	2	2020-07-16	2020-07-20 09:21:12.194	9.69999999999999929	0.110000000000000001	0	\N	\N	\N	2020-07-20 09:21:13	9.8100000000000005	\N	\N
414975	115	t	9.69999999999999929	0.110000000000000001	0	0	9.80000000000000071	3	3	f	3	2020-07-17	2020-07-20 13:10:30.188	9.69999999999999929	0.110000000000000001	0	\N	\N	\N	2020-07-20 01:10:30	9.8100000000000005	\N	\N
414507	37	t	75	9	0	0	84	3	3	f	1	2020-07-16	2020-07-22 13:41:41.97	75	9	0	0.5	\N	\N	2020-07-22 01:41:42	84	\N	\N
414977	115	t	9.69999999999999929	0.110000000000000001	0	0	9.80000000000000071	3	3	f	5	2020-07-20	2020-07-22 13:42:29.699	9.69999999999999929	0.110000000000000001	0	\N	\N	\N	2020-07-22 01:42:30	9.8100000000000005	\N	\N
414978	115	t	9.69999999999999929	0.110000000000000001	0	0	9.80000000000000071	3	3	f	6	2020-07-21	2020-07-24 12:29:11.577	9.69999999999999929	0.110000000000000001	0	\N	\N	\N	2020-07-24 12:29:12	9.8100000000000005	\N	\N
414980	115	t	9.69999999999999929	0.110000000000000001	0	0	9.80000000000000071	3	3	f	8	2020-07-23	2020-07-25 12:07:43.308	9.69999999999999929	0.110000000000000001	0	\N	\N	\N	2020-07-25 12:07:43	9.8100000000000005	\N	\N
414996	115	f	9.69999999999999929	0.110000000000000001	0	0	9.80000000000000071	3	3	f	24	2020-08-12	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414997	115	f	9.69999999999999929	0.110000000000000001	0	0	9.80000000000000071	3	3	f	25	2020-08-13	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414998	115	f	9.69999999999999929	0.110000000000000001	0	0	9.80000000000000071	3	3	f	26	2020-08-14	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414999	115	f	9.69999999999999929	0.110000000000000001	0	0	9.80000000000000071	3	3	f	27	2020-08-15	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415000	115	f	9.69999999999999929	0.110000000000000001	0	0	9.80000000000000071	3	3	f	28	2020-08-17	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415001	115	f	9.69999999999999929	0.110000000000000001	0	0	9.80000000000000071	3	3	f	29	2020-08-18	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415002	115	f	9.69999999999999929	0.110000000000000001	0	0	9.80000000000000071	3	3	f	30	2020-08-19	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415005	116	f	10.6899999999999995	0.0599999999999999978	0.0500000000000000028	0	10.8000000000000007	3	3	f	3	2020-07-17	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415006	116	f	10.6899999999999995	0.0599999999999999978	0.0500000000000000028	0	10.8000000000000007	3	3	f	4	2020-07-18	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415007	116	f	10.6899999999999995	0.0599999999999999978	0.0500000000000000028	0	10.8000000000000007	3	3	f	5	2020-07-20	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415008	116	f	10.6899999999999995	0.0599999999999999978	0.0500000000000000028	0	10.8000000000000007	3	3	f	6	2020-07-21	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415009	116	f	10.6899999999999995	0.0599999999999999978	0.0500000000000000028	0	10.8000000000000007	3	3	f	7	2020-07-22	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415010	116	f	10.6899999999999995	0.0599999999999999978	0.0500000000000000028	0	10.8000000000000007	3	3	f	8	2020-07-23	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415011	116	f	10.6899999999999995	0.0599999999999999978	0.0500000000000000028	0	10.8000000000000007	3	3	f	9	2020-07-24	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415012	116	f	10.6899999999999995	0.0599999999999999978	0.0500000000000000028	0	10.8000000000000007	3	3	f	10	2020-07-25	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415013	116	f	10.6899999999999995	0.0599999999999999978	0.0500000000000000028	0	10.8000000000000007	3	3	f	11	2020-07-27	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415014	116	f	10.6899999999999995	0.0599999999999999978	0.0500000000000000028	0	10.8000000000000007	3	3	f	12	2020-07-28	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415015	116	f	10.6899999999999995	0.0599999999999999978	0.0500000000000000028	0	10.8000000000000007	3	3	f	13	2020-07-30	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415016	116	f	10.6899999999999995	0.0599999999999999978	0.0500000000000000028	0	10.8000000000000007	3	3	f	14	2020-07-31	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415017	61	f	103.310000000000002	8.25999999999999979	0.0299999999999999989	0	111.599999999999994	3	3	f	1	2020-07-20	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415018	61	f	103.310000000000002	8.25999999999999979	0.0299999999999999989	0	111.599999999999994	3	3	f	2	2020-08-04	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415019	61	f	103.310000000000002	8.25999999999999979	0.0299999999999999989	0	111.599999999999994	3	3	f	3	2020-08-19	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415020	61	f	103.310000000000002	8.25999999999999979	0.0299999999999999989	0	111.599999999999994	3	3	f	4	2020-09-03	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415021	61	f	103.310000000000002	8.25999999999999979	0.0299999999999999989	0	111.599999999999994	3	3	f	5	2020-09-18	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415022	61	f	103.310000000000002	8.25999999999999979	0.0299999999999999989	0	111.599999999999994	3	3	f	6	2020-10-03	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415023	61	f	103.310000000000002	8.25999999999999979	0.0299999999999999989	0	111.599999999999994	3	3	f	7	2020-10-19	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415024	61	f	103.310000000000002	8.25999999999999979	0.0299999999999999989	0	111.599999999999994	3	3	f	8	2020-11-03	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415025	99	f	138.550000000000011	33.25	0	0	171.800000000000011	3	3	f	1	2020-07-25	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415026	99	f	138.550000000000011	33.25	0	0	171.800000000000011	3	3	f	2	2020-08-10	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415027	99	f	138.550000000000011	33.25	0	0	171.800000000000011	3	3	f	3	2020-08-25	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415028	99	f	138.550000000000011	33.25	0	0	171.800000000000011	3	3	f	4	2020-09-09	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415029	99	f	138.550000000000011	33.25	0	0	171.800000000000011	3	3	f	5	2020-09-24	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415030	99	f	138.550000000000011	33.25	0	0	171.800000000000011	3	3	f	6	2020-10-09	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415031	105	f	27.7800000000000011	0.770000000000000018	0.0500000000000000028	0	28.6000000000000014	3	3	f	1	2020-07-13	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415032	105	f	27.7800000000000011	0.770000000000000018	0.0500000000000000028	0	28.6000000000000014	3	3	f	2	2020-07-14	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415033	105	f	27.7800000000000011	0.770000000000000018	0.0500000000000000028	0	28.6000000000000014	3	3	f	3	2020-07-15	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415034	105	f	27.7800000000000011	0.770000000000000018	0.0500000000000000028	0	28.6000000000000014	3	3	f	4	2020-07-16	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415035	105	f	27.7800000000000011	0.770000000000000018	0.0500000000000000028	0	28.6000000000000014	3	3	f	5	2020-07-17	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415036	105	f	27.7800000000000011	0.770000000000000018	0.0500000000000000028	0	28.6000000000000014	3	3	f	6	2020-07-18	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415037	105	f	27.7800000000000011	0.770000000000000018	0.0500000000000000028	0	28.6000000000000014	3	3	f	7	2020-07-20	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415038	105	f	27.7800000000000011	0.770000000000000018	0.0500000000000000028	0	28.6000000000000014	3	3	f	8	2020-07-21	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415039	105	f	27.7800000000000011	0.770000000000000018	0.0500000000000000028	0	28.6000000000000014	3	3	f	9	2020-07-22	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415040	105	f	27.7800000000000011	0.770000000000000018	0.0500000000000000028	0	28.6000000000000014	3	3	f	10	2020-07-23	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415041	105	f	27.7800000000000011	0.770000000000000018	0.0500000000000000028	0	28.6000000000000014	3	3	f	11	2020-07-24	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415042	105	f	27.7800000000000011	0.770000000000000018	0.0500000000000000028	0	28.6000000000000014	3	3	f	12	2020-07-25	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415043	105	f	27.7800000000000011	0.770000000000000018	0.0500000000000000028	0	28.6000000000000014	3	3	f	13	2020-07-27	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415044	105	f	27.7800000000000011	0.770000000000000018	0.0500000000000000028	0	28.6000000000000014	3	3	f	14	2020-07-28	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415045	105	f	27.7800000000000011	0.770000000000000018	0.0500000000000000028	0	28.6000000000000014	3	3	f	15	2020-07-30	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415046	105	f	27.7800000000000011	0.770000000000000018	0.0500000000000000028	0	28.6000000000000014	3	3	f	16	2020-07-31	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415047	105	f	27.7800000000000011	0.770000000000000018	0.0500000000000000028	0	28.6000000000000014	3	3	f	17	2020-08-01	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415048	105	f	27.7800000000000011	0.770000000000000018	0.0500000000000000028	0	28.6000000000000014	3	3	f	18	2020-08-03	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415049	105	f	27.7800000000000011	0.770000000000000018	0.0500000000000000028	0	28.6000000000000014	3	3	f	19	2020-08-04	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415050	105	f	27.7800000000000011	0.770000000000000018	0.0500000000000000028	0	28.6000000000000014	3	3	f	20	2020-08-05	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415051	105	f	27.7800000000000011	0.770000000000000018	0.0500000000000000028	0	28.6000000000000014	3	3	f	21	2020-08-06	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415052	105	f	27.7800000000000011	0.770000000000000018	0.0500000000000000028	0	28.6000000000000014	3	3	f	22	2020-08-07	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415053	105	f	27.7800000000000011	0.770000000000000018	0.0500000000000000028	0	28.6000000000000014	3	3	f	23	2020-08-08	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415054	105	f	27.7800000000000011	0.770000000000000018	0.0500000000000000028	0	28.6000000000000014	3	3	f	24	2020-08-10	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415055	105	f	27.7800000000000011	0.770000000000000018	0.0500000000000000028	0	28.6000000000000014	3	3	f	25	2020-08-11	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415056	105	f	27.7800000000000011	0.770000000000000018	0.0500000000000000028	0	28.6000000000000014	3	3	f	26	2020-08-12	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415004	116	t	10.6899999999999995	0.0599999999999999978	0.0500000000000000028	0	10.8000000000000007	3	3	f	2	2020-07-16	2020-07-17 09:27:13.112	10.6899999999999995	0.0599999999999999978	0.0500000000000000028	\N	\N	\N	2020-07-17 09:27:15	10.8000000000000007	\N	\N
415057	105	f	27.7800000000000011	0.770000000000000018	0.0500000000000000028	0	28.6000000000000014	3	3	f	27	2020-08-13	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415058	105	f	27.7800000000000011	0.770000000000000018	0.0500000000000000028	0	28.6000000000000014	3	3	f	28	2020-08-14	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415059	105	f	27.7800000000000011	0.770000000000000018	0.0500000000000000028	0	28.6000000000000014	3	3	f	29	2020-08-15	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415060	105	f	27.7800000000000011	0.770000000000000018	0.0500000000000000028	0	28.6000000000000014	3	3	f	30	2020-08-17	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415061	105	f	27.7800000000000011	0.770000000000000018	0.0500000000000000028	0	28.6000000000000014	3	3	f	31	2020-08-18	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415062	105	f	27.7800000000000011	0.770000000000000018	0.0500000000000000028	0	28.6000000000000014	3	3	f	32	2020-08-19	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415063	105	f	27.7800000000000011	0.770000000000000018	0.0500000000000000028	0	28.6000000000000014	3	3	f	33	2020-08-20	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415064	105	f	27.7800000000000011	0.770000000000000018	0.0500000000000000028	0	28.6000000000000014	3	3	f	34	2020-08-21	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415065	105	f	27.7800000000000011	0.770000000000000018	0.0500000000000000028	0	28.6000000000000014	3	3	f	35	2020-08-22	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415066	105	f	27.7800000000000011	0.770000000000000018	0.0500000000000000028	0	28.6000000000000014	3	3	f	36	2020-08-24	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415067	105	f	27.7800000000000011	0.770000000000000018	0.0500000000000000028	0	28.6000000000000014	3	3	f	37	2020-08-25	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415068	105	f	27.7800000000000011	0.770000000000000018	0.0500000000000000028	0	28.6000000000000014	3	3	f	38	2020-08-26	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415069	105	f	27.7800000000000011	0.770000000000000018	0.0500000000000000028	0	28.6000000000000014	3	3	f	39	2020-08-27	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415070	105	f	27.7800000000000011	0.770000000000000018	0.0500000000000000028	0	28.6000000000000014	3	3	f	40	2020-08-28	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415071	105	f	27.7800000000000011	0.770000000000000018	0.0500000000000000028	0	28.6000000000000014	3	3	f	41	2020-08-29	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415072	105	f	27.7800000000000011	0.770000000000000018	0.0500000000000000028	0	28.6000000000000014	3	3	f	42	2020-08-31	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415073	105	f	27.7800000000000011	0.770000000000000018	0.0500000000000000028	0	28.6000000000000014	3	3	f	43	2020-09-01	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415074	105	f	27.7800000000000011	0.770000000000000018	0.0500000000000000028	0	28.6000000000000014	3	3	f	44	2020-09-02	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415075	105	f	27.7800000000000011	0.770000000000000018	0.0500000000000000028	0	28.6000000000000014	3	3	f	45	2020-09-03	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415076	105	f	27.7800000000000011	0.770000000000000018	0.0500000000000000028	0	28.6000000000000014	3	3	f	46	2020-09-04	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415077	105	f	27.7800000000000011	0.770000000000000018	0.0500000000000000028	0	28.6000000000000014	3	3	f	47	2020-09-05	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415078	105	f	27.7800000000000011	0.770000000000000018	0.0500000000000000028	0	28.6000000000000014	3	3	f	48	2020-09-07	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415079	105	f	27.7800000000000011	0.770000000000000018	0.0500000000000000028	0	28.6000000000000014	3	3	f	49	2020-09-08	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415080	105	f	27.7800000000000011	0.770000000000000018	0.0500000000000000028	0	28.6000000000000014	3	3	f	50	2020-09-09	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415081	105	f	27.7800000000000011	0.770000000000000018	0.0500000000000000028	0	28.6000000000000014	3	3	f	51	2020-09-10	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415082	105	f	27.7800000000000011	0.770000000000000018	0.0500000000000000028	0	28.6000000000000014	3	3	f	52	2020-09-11	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415083	105	f	27.7800000000000011	0.770000000000000018	0.0500000000000000028	0	28.6000000000000014	3	3	f	53	2020-09-12	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415084	105	f	27.7800000000000011	0.770000000000000018	0.0500000000000000028	0	28.6000000000000014	3	3	f	54	2020-09-14	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415085	105	f	27.7800000000000011	0.770000000000000018	0.0500000000000000028	0	28.6000000000000014	3	3	f	55	2020-09-15	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415086	105	f	27.7800000000000011	0.770000000000000018	0.0500000000000000028	0	28.6000000000000014	3	3	f	56	2020-09-16	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415087	105	f	27.7800000000000011	0.770000000000000018	0.0500000000000000028	0	28.6000000000000014	3	3	f	57	2020-09-17	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415088	105	f	27.7800000000000011	0.770000000000000018	0.0500000000000000028	0	28.6000000000000014	3	3	f	58	2020-09-18	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415089	105	f	27.7800000000000011	0.770000000000000018	0.0500000000000000028	0	28.6000000000000014	3	3	f	59	2020-09-19	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415090	105	f	27.7800000000000011	0.770000000000000018	0.0500000000000000028	0	28.6000000000000014	3	3	f	60	2020-09-21	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415091	105	f	27.7800000000000011	0.770000000000000018	0.0500000000000000028	0	28.6000000000000014	3	3	f	61	2020-09-22	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415092	105	f	27.7800000000000011	0.770000000000000018	0.0500000000000000028	0	28.6000000000000014	3	3	f	62	2020-09-23	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415093	105	f	27.7800000000000011	0.770000000000000018	0.0500000000000000028	0	28.6000000000000014	3	3	f	63	2020-09-24	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415094	105	f	27.7800000000000011	0.770000000000000018	0.0500000000000000028	0	28.6000000000000014	3	3	f	64	2020-09-25	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415095	105	f	27.7800000000000011	0.770000000000000018	0.0500000000000000028	0	28.6000000000000014	3	3	f	65	2020-09-26	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415096	105	f	27.7800000000000011	0.770000000000000018	0.0500000000000000028	0	28.6000000000000014	3	3	f	66	2020-09-28	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415097	105	f	27.7800000000000011	0.770000000000000018	0.0500000000000000028	0	28.6000000000000014	3	3	f	67	2020-09-29	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415098	105	f	27.7800000000000011	0.770000000000000018	0.0500000000000000028	0	28.6000000000000014	3	3	f	68	2020-09-30	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415099	105	f	27.7800000000000011	0.770000000000000018	0.0500000000000000028	0	28.6000000000000014	3	3	f	69	2020-10-01	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415100	105	f	27.7800000000000011	0.770000000000000018	0.0500000000000000028	0	28.6000000000000014	3	3	f	70	2020-10-02	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415101	105	f	27.7800000000000011	0.770000000000000018	0.0500000000000000028	0	28.6000000000000014	3	3	f	71	2020-10-03	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415102	105	f	27.7800000000000011	0.770000000000000018	0.0500000000000000028	0	28.6000000000000014	3	3	f	72	2020-10-05	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415103	104	f	57.0125000000000028	1.25427500000000003	0	0	58.2999999999999972	3	3	f	1	2020-07-18	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415104	104	f	57.0125000000000028	1.25427500000000003	0	0	58.2999999999999972	3	3	f	2	2020-07-25	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415105	104	f	57.0125000000000028	1.25427500000000003	0	0	58.2999999999999972	3	3	f	3	2020-08-01	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415106	104	f	57.0125000000000028	1.25427500000000003	0	0	58.2999999999999972	3	3	f	4	2020-08-08	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415107	104	f	57.0125000000000028	1.25427500000000003	0	0	58.2999999999999972	3	3	f	5	2020-08-15	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415108	104	f	57.0125000000000028	1.25427500000000003	0	0	58.2999999999999972	3	3	f	6	2020-08-22	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415109	104	f	57.0125000000000028	1.25427500000000003	0	0	58.2999999999999972	3	3	f	7	2020-08-29	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415110	104	f	57.0125000000000028	1.25427500000000003	0	0	58.2999999999999972	3	3	f	8	2020-09-05	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415111	69	f	74.0300000000000011	0	0.0700000000000000067	0	74.0999999999999943	3	3	f	1	2020-07-07	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415112	69	f	74.0300000000000011	0	0.0700000000000000067	0	74.0999999999999943	3	3	f	2	2020-07-08	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415113	69	f	74.0300000000000011	0	0.0700000000000000067	0	74.0999999999999943	3	3	f	3	2020-07-09	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415114	69	f	74.0300000000000011	0	0.0700000000000000067	0	74.0999999999999943	3	3	f	4	2020-07-10	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415115	69	f	74.0300000000000011	0	0.0700000000000000067	0	74.0999999999999943	3	3	f	5	2020-07-11	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415116	69	f	74.0300000000000011	0	0.0700000000000000067	0	74.0999999999999943	3	3	f	6	2020-07-13	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415117	69	f	74.0300000000000011	0	0.0700000000000000067	0	74.0999999999999943	3	3	f	7	2020-07-14	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415118	69	f	74.0300000000000011	0	0.0700000000000000067	0	74.0999999999999943	3	3	f	8	2020-07-15	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415120	69	f	74.0300000000000011	0	0.0700000000000000067	0	74.0999999999999943	3	3	f	10	2020-07-17	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415122	69	f	74.0300000000000011	0	0.0700000000000000067	0	74.0999999999999943	3	3	f	12	2020-07-20	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415124	69	f	74.0300000000000011	0	0.0700000000000000067	0	74.0999999999999943	3	3	f	14	2020-07-22	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415126	69	f	74.0300000000000011	0	0.0700000000000000067	0	74.0999999999999943	3	3	f	16	2020-07-24	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415128	69	f	74.0300000000000011	0	0.0700000000000000067	0	74.0999999999999943	3	3	f	18	2020-07-27	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415130	69	f	74.0300000000000011	0	0.0700000000000000067	0	74.0999999999999943	3	3	f	20	2020-07-30	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415132	69	f	74.0300000000000011	0	0.0700000000000000067	0	74.0999999999999943	3	3	f	22	2020-08-01	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415134	69	f	74.0300000000000011	0	0.0700000000000000067	0	74.0999999999999943	3	3	f	24	2020-08-04	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415119	69	f	74.0300000000000011	0	0.0700000000000000067	0	74.0999999999999943	3	3	f	9	2020-07-16	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415121	69	f	74.0300000000000011	0	0.0700000000000000067	0	74.0999999999999943	3	3	f	11	2020-07-18	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415123	69	f	74.0300000000000011	0	0.0700000000000000067	0	74.0999999999999943	3	3	f	13	2020-07-21	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415125	69	f	74.0300000000000011	0	0.0700000000000000067	0	74.0999999999999943	3	3	f	15	2020-07-23	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415127	69	f	74.0300000000000011	0	0.0700000000000000067	0	74.0999999999999943	3	3	f	17	2020-07-25	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415129	69	f	74.0300000000000011	0	0.0700000000000000067	0	74.0999999999999943	3	3	f	19	2020-07-28	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415131	69	f	74.0300000000000011	0	0.0700000000000000067	0	74.0999999999999943	3	3	f	21	2020-07-31	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415133	69	f	74.0300000000000011	0	0.0700000000000000067	0	74.0999999999999943	3	3	f	23	2020-08-03	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415135	69	f	74.0300000000000011	0	0.0700000000000000067	0	74.0999999999999943	3	3	f	25	2020-08-05	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415136	69	f	74.0300000000000011	0	0.0700000000000000067	0	74.0999999999999943	3	3	f	26	2020-08-06	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414647	85	t	19.2300000000000004	2.31000000000000005	0.0599999999999999978	0	21.6000000000000014	3	3	f	6	2020-07-15	2020-07-16 19:29:05.234	19.2300000000000004	2.31000000000000005	0.0599999999999999978	\N	\N	\N	2020-07-16 07:29:05	21.6000000000000014	\N	\N
414621	86	t	26.9200000000000017	3.22999999999999998	0.0500000000000000028	0	30.1999999999999993	3	3	f	6	2020-07-15	2020-07-16 19:29:46.772	26.9200000000000017	3.22999999999999998	0.0500000000000000028	\N	\N	\N	2020-07-16 07:29:47	30.1999999999999993	\N	\N
415003	116	t	10.6899999999999995	0.0599999999999999978	0.0500000000000000028	0	10.8000000000000007	3	3	f	1	2020-07-15	2020-07-17 09:27:13.112	10.6899999999999995	0.0599999999999999978	0.0500000000000000028	\N	\N	\N	2020-07-17 09:27:14	10.8000000000000007	\N	\N
414769	77	t	60	2.39999999999999991	0	0	62.3999999999999986	3	3	f	1	2020-07-14	2020-07-17 09:30:59.136	60	2.39999999999999991	0	6	\N	\N	2020-07-17 09:30:59	62.3999999999999986	\N	\N
414772	77	t	60	2.39999999999999991	0	0	62.3999999999999986	3	3	f	4	2020-08-04	2020-07-17 09:30:59.136	60	2.39999999999999991	0	\N	\N	\N	2020-07-17 09:31:02	62.3999999999999986	\N	\N
414572	72	t	38.4600000000000009	1.72999999999999998	0.0100000000000000002	0	40.2000000000000028	3	3	f	8	2020-07-07	2020-07-17 12:45:49.85	38.4600000000000009	1.72999999999999998	0.0100000000000000002	\N	\N	\N	2020-07-17 12:45:49	40.2000000000000028	\N	\N
414905	113	t	7.69000000000000039	0.689999999999999947	0.0200000000000000004	0	8.40000000000000036	3	3	f	2	2020-07-16	2020-07-17 13:11:21.223	7.69000000000000039	0.689999999999999947	0.0200000000000000004	1	\N	\N	2020-07-17 01:11:23	8.40000000000000036	\N	\N
414908	113	t	7.69000000000000039	0.689999999999999947	0.0200000000000000004	0	8.40000000000000036	3	3	f	5	2020-07-20	2020-07-17 13:11:21.223	7.69000000000000039	0.689999999999999947	0.0200000000000000004	\N	\N	\N	2020-07-17 01:11:26	8.40000000000000036	\N	\N
414546	67	t	19.2300000000000004	1.35000000000000009	0.0200000000000000004	0	20.6000000000000014	3	3	f	8	2020-07-15	2020-07-17 13:13:06.973	19.2300000000000004	1.35000000000000009	0.0200000000000000004	\N	\N	\N	2020-07-17 01:13:07	20.6000000000000014	\N	\N
415142	124	f	11.5399999999999991	0.92000000000000004	0.0400000000000000008	0	12.5	3	3	f	6	2020-07-25	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415143	124	f	11.5399999999999991	0.92000000000000004	0.0400000000000000008	0	12.5	3	3	f	7	2020-07-27	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415144	124	f	11.5399999999999991	0.92000000000000004	0.0400000000000000008	0	12.5	3	3	f	8	2020-07-28	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415145	124	f	11.5399999999999991	0.92000000000000004	0.0400000000000000008	0	12.5	3	3	f	9	2020-07-30	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415146	124	f	11.5399999999999991	0.92000000000000004	0.0400000000000000008	0	12.5	3	3	f	10	2020-07-31	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415147	124	f	11.5399999999999991	0.92000000000000004	0.0400000000000000008	0	12.5	3	3	f	11	2020-08-01	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415148	124	f	11.5399999999999991	0.92000000000000004	0.0400000000000000008	0	12.5	3	3	f	12	2020-08-03	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415149	124	f	11.5399999999999991	0.92000000000000004	0.0400000000000000008	0	12.5	3	3	f	13	2020-08-04	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415150	124	f	11.5399999999999991	0.92000000000000004	0.0400000000000000008	0	12.5	3	3	f	14	2020-08-05	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415151	124	f	11.5399999999999991	0.92000000000000004	0.0400000000000000008	0	12.5	3	3	f	15	2020-08-06	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415152	124	f	11.5399999999999991	0.92000000000000004	0.0400000000000000008	0	12.5	3	3	f	16	2020-08-07	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415153	124	f	11.5399999999999991	0.92000000000000004	0.0400000000000000008	0	12.5	3	3	f	17	2020-08-08	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415154	124	f	11.5399999999999991	0.92000000000000004	0.0400000000000000008	0	12.5	3	3	f	18	2020-08-10	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415155	124	f	11.5399999999999991	0.92000000000000004	0.0400000000000000008	0	12.5	3	3	f	19	2020-08-11	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415156	124	f	11.5399999999999991	0.92000000000000004	0.0400000000000000008	0	12.5	3	3	f	20	2020-08-12	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415157	124	f	11.5399999999999991	0.92000000000000004	0.0400000000000000008	0	12.5	3	3	f	21	2020-08-13	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415158	124	f	11.5399999999999991	0.92000000000000004	0.0400000000000000008	0	12.5	3	3	f	22	2020-08-14	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415159	124	f	11.5399999999999991	0.92000000000000004	0.0400000000000000008	0	12.5	3	3	f	23	2020-08-15	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415160	124	f	11.5399999999999991	0.92000000000000004	0.0400000000000000008	0	12.5	3	3	f	24	2020-08-17	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415161	124	f	11.5399999999999991	0.92000000000000004	0.0400000000000000008	0	12.5	3	3	f	25	2020-08-18	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415162	124	f	11.5399999999999991	0.92000000000000004	0.0400000000000000008	0	12.5	3	3	f	26	2020-08-19	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415163	52	f	96.158333333333303	23.0779999999999994	0	0	119.25	3	3	f	1	2020-07-10	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415164	52	f	96.158333333333303	23.0779999999999994	0	0	119.25	3	3	f	2	2020-07-17	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415165	52	f	96.158333333333303	23.0779999999999994	0	0	119.25	3	3	f	3	2020-07-24	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415166	52	f	96.158333333333303	23.0779999999999994	0	0	119.25	3	3	f	4	2020-07-31	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415167	52	f	96.158333333333303	23.0779999999999994	0	0	119.25	3	3	f	5	2020-08-07	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415168	52	f	96.158333333333303	23.0779999999999994	0	0	119.25	3	3	f	6	2020-08-14	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415169	52	f	96.158333333333303	23.0779999999999994	0	0	119.25	3	3	f	7	2020-08-21	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415137	124	t	11.5399999999999991	0.92000000000000004	0.0400000000000000008	0	12.5	3	3	f	1	2020-07-20	2020-07-20 13:12:19.229	11.5399999999999991	0.92000000000000004	0.0400000000000000008	\N	\N	\N	2020-07-20 01:12:20	12.5	\N	\N
415138	124	t	11.5399999999999991	0.92000000000000004	0.0400000000000000008	0	12.5	3	3	f	2	2020-07-21	2020-07-22 13:17:18.623	11.5399999999999991	0.92000000000000004	0.0400000000000000008	\N	\N	\N	2020-07-22 01:17:19	12.5	\N	\N
415139	124	t	11.5399999999999991	0.92000000000000004	0.0400000000000000008	0	12.5	3	3	f	3	2020-07-22	2020-07-23 13:31:48.13	11.5399999999999991	0.92000000000000004	0.0400000000000000008	\N	\N	\N	2020-07-23 01:31:48	12.5	\N	\N
415140	124	t	11.5399999999999991	0.92000000000000004	0.0400000000000000008	0	12.5	3	3	f	4	2020-07-23	2020-07-24 12:28:24.554	11.5399999999999991	0.92000000000000004	0.0400000000000000008	\N	\N	\N	2020-07-24 12:28:25	12.5	\N	\N
415141	124	t	11.5399999999999991	0.92000000000000004	0.0400000000000000008	0	12.5	3	3	f	5	2020-07-24	2020-07-25 12:03:30.645	11.5399999999999991	0.92000000000000004	0.0400000000000000008	\N	\N	\N	2020-07-25 12:03:31	12.5	\N	\N
415170	52	f	96.158333333333303	23.0779999999999994	0	0	119.25	3	3	f	8	2020-08-28	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415172	52	f	96.158333333333303	23.0779999999999994	0	0	119.25	3	3	f	10	2020-09-11	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415174	52	f	96.158333333333303	23.0779999999999994	0	0	119.25	3	3	f	12	2020-09-25	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415171	52	f	96.158333333333303	23.0779999999999994	0	0	119.25	3	3	f	9	2020-09-04	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415173	52	f	96.158333333333303	23.0779999999999994	0	0	119.25	3	3	f	11	2020-09-18	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414548	67	t	19.2300000000000004	1.35000000000000009	0.0200000000000000004	0	20.6000000000000014	3	3	f	10	2020-07-17	2020-07-20 13:13:05.364	19.2300000000000004	1.35000000000000009	0.0200000000000000004	\N	\N	\N	2020-07-20 01:13:06	20.6000000000000014	\N	\N
414898	107	t	125	10	0	0	135	3	3	f	1	2020-07-18	2020-07-20 21:52:18.345	125	10	0	\N	\N	\N	2020-07-20 09:52:19	135	\N	\N
414849	101	t	19.2300000000000004	1.54000000000000004	0.0299999999999999989	0	20.8000000000000007	3	3	f	6	2020-07-17	2020-07-20 21:54:25.774	19.2300000000000004	1.54000000000000004	0.0299999999999999989	\N	\N	\N	2020-07-20 09:54:26	20.8000000000000007	\N	\N
414824	102	t	26.9200000000000017	3.22999999999999998	0.0500000000000000028	0	30.1999999999999993	3	3	f	7	2020-07-18	2020-07-20 21:55:39.998	26.9200000000000017	3.22999999999999998	0.0500000000000000028	\N	\N	\N	2020-07-20 09:55:40	30.1999999999999993	\N	\N
414624	86	t	26.9200000000000017	3.22999999999999998	0.0500000000000000028	0	30.1999999999999993	3	3	f	9	2020-07-18	2020-07-20 21:56:50.719	26.9200000000000017	3.22999999999999998	0.0500000000000000028	\N	\N	\N	2020-07-20 09:56:52	30.1999999999999993	\N	\N
415177	127	f	16.3500000000000014	0.650000000000000022	0	0	17	3	3	f	3	2020-07-25	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415178	127	f	16.3500000000000014	0.650000000000000022	0	0	17	3	3	f	4	2020-07-27	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415179	127	f	16.3500000000000014	0.650000000000000022	0	0	17	3	3	f	5	2020-07-28	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415180	127	f	16.3500000000000014	0.650000000000000022	0	0	17	3	3	f	6	2020-07-30	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415181	127	f	16.3500000000000014	0.650000000000000022	0	0	17	3	3	f	7	2020-07-31	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415182	127	f	16.3500000000000014	0.650000000000000022	0	0	17	3	3	f	8	2020-08-01	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415183	127	f	16.3500000000000014	0.650000000000000022	0	0	17	3	3	f	9	2020-08-03	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415184	127	f	16.3500000000000014	0.650000000000000022	0	0	17	3	3	f	10	2020-08-04	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415185	127	f	16.3500000000000014	0.650000000000000022	0	0	17	3	3	f	11	2020-08-05	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415186	127	f	16.3500000000000014	0.650000000000000022	0	0	17	3	3	f	12	2020-08-06	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415187	127	f	16.3500000000000014	0.650000000000000022	0	0	17	3	3	f	13	2020-08-07	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415188	127	f	16.3500000000000014	0.650000000000000022	0	0	17	3	3	f	14	2020-08-08	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415189	127	f	16.3500000000000014	0.650000000000000022	0	0	17	3	3	f	15	2020-08-10	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415190	127	f	16.3500000000000014	0.650000000000000022	0	0	17	3	3	f	16	2020-08-11	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415191	127	f	16.3500000000000014	0.650000000000000022	0	0	17	3	3	f	17	2020-08-12	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415192	127	f	16.3500000000000014	0.650000000000000022	0	0	17	3	3	f	18	2020-08-13	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415193	127	f	16.3500000000000014	0.650000000000000022	0	0	17	3	3	f	19	2020-08-14	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415194	127	f	16.3500000000000014	0.650000000000000022	0	0	17	3	3	f	20	2020-08-15	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415195	127	f	16.3500000000000014	0.650000000000000022	0	0	17	3	3	f	21	2020-08-17	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415196	127	f	16.3500000000000014	0.650000000000000022	0	0	17	3	3	f	22	2020-08-18	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415197	127	f	16.3500000000000014	0.650000000000000022	0	0	17	3	3	f	23	2020-08-19	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415198	127	f	16.3500000000000014	0.650000000000000022	0	0	17	3	3	f	24	2020-08-20	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415199	127	f	16.3500000000000014	0.650000000000000022	0	0	17	3	3	f	25	2020-08-21	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415200	127	f	16.3500000000000014	0.650000000000000022	0	0	17	3	3	f	26	2020-08-22	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415201	127	f	16.3500000000000014	0.650000000000000022	0	0	17	3	3	f	27	2020-08-24	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415202	127	f	16.3500000000000014	0.650000000000000022	0	0	17	3	3	f	28	2020-08-25	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415203	127	f	16.3500000000000014	0.650000000000000022	0	0	17	3	3	f	29	2020-08-26	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415204	127	f	16.3500000000000014	0.650000000000000022	0	0	17	3	3	f	30	2020-08-27	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415205	127	f	16.3500000000000014	0.650000000000000022	0	0	17	3	3	f	31	2020-08-28	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415206	127	f	16.3500000000000014	0.650000000000000022	0	0	17	3	3	f	32	2020-08-29	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415207	127	f	16.3500000000000014	0.650000000000000022	0	0	17	3	3	f	33	2020-08-31	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415208	127	f	16.3500000000000014	0.650000000000000022	0	0	17	3	3	f	34	2020-09-01	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415209	127	f	16.3500000000000014	0.650000000000000022	0	0	17	3	3	f	35	2020-09-02	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415210	127	f	16.3500000000000014	0.650000000000000022	0	0	17	3	3	f	36	2020-09-03	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415211	127	f	16.3500000000000014	0.650000000000000022	0	0	17	3	3	f	37	2020-09-04	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415212	127	f	16.3500000000000014	0.650000000000000022	0	0	17	3	3	f	38	2020-09-05	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415213	127	f	16.3500000000000014	0.650000000000000022	0	0	17	3	3	f	39	2020-09-07	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415214	127	f	16.3500000000000014	0.650000000000000022	0	0	17	3	3	f	40	2020-09-08	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415215	127	f	16.3500000000000014	0.650000000000000022	0	0	17	3	3	f	41	2020-09-09	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415216	127	f	16.3500000000000014	0.650000000000000022	0	0	17	3	3	f	42	2020-09-10	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415217	127	f	16.3500000000000014	0.650000000000000022	0	0	17	3	3	f	43	2020-09-11	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415218	127	f	16.3500000000000014	0.650000000000000022	0	0	17	3	3	f	44	2020-09-12	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415219	127	f	16.3500000000000014	0.650000000000000022	0	0	17	3	3	f	45	2020-09-14	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415220	127	f	16.3500000000000014	0.650000000000000022	0	0	17	3	3	f	46	2020-09-15	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415221	127	f	16.3500000000000014	0.650000000000000022	0	0	17	3	3	f	47	2020-09-16	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415222	127	f	16.3500000000000014	0.650000000000000022	0	0	17	3	3	f	48	2020-09-17	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415223	127	f	16.3500000000000014	0.650000000000000022	0	0	17	3	3	f	49	2020-09-18	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415224	127	f	16.3500000000000014	0.650000000000000022	0	0	17	3	3	f	50	2020-09-19	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415225	127	f	16.3500000000000014	0.650000000000000022	0	0	17	3	3	f	51	2020-09-21	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415226	127	f	16.3500000000000014	0.650000000000000022	0	0	17	3	3	f	52	2020-09-22	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415176	127	t	16.3500000000000014	0.650000000000000022	0	0	17	3	3	f	2	2020-07-24	2020-07-24 13:49:53.914	16.3500000000000014	0.650000000000000022	0	\N	\N	\N	2020-07-24 01:49:55	17	\N	\N
414355	15	t	7.69000000000000039	0.46000000000000002	0.0500000000000000028	0	8.19999999999999929	3	3	f	14	2020-07-16	2020-07-22 11:53:52.28	7.69000000000000039	0.46000000000000002	0.0500000000000000028	1	\N	\N	2020-07-22 11:53:52	8.19999999999999929	\N	\N
414948	118	t	11.5399999999999991	0.92000000000000004	0.0400000000000000008	0	12.5	3	3	f	5	2020-07-21	2020-07-22 12:56:07.241	11.5399999999999991	0.92000000000000004	0.0400000000000000008	\N	\N	\N	2020-07-22 12:56:10	12.5	\N	\N
414976	115	t	9.69999999999999929	0.110000000000000001	0	0	9.80000000000000071	3	3	f	4	2020-07-18	2020-07-22 12:58:03.785	9.69999999999999929	0.110000000000000001	0	\N	\N	\N	2020-07-22 12:58:04	9.8100000000000005	\N	\N
414580	72	t	38.4600000000000009	1.72999999999999998	0.0100000000000000002	0	40.2000000000000028	3	3	f	16	2020-07-16	2020-07-22 13:25:56.572	38.4600000000000009	1.72999999999999998	0.0100000000000000002	1	\N	\N	2020-07-22 01:26:02	40.2000000000000028	\N	\N
414583	72	t	38.4600000000000009	1.72999999999999998	0.0100000000000000002	0	40.2000000000000028	3	3	f	19	2020-07-20	2020-07-22 13:25:56.572	38.4600000000000009	1.72999999999999998	0.0100000000000000002	\N	\N	\N	2020-07-22 01:26:05	40.2000000000000028	\N	\N
414587	72	t	38.4600000000000009	1.72999999999999998	0.0100000000000000002	0	40.2000000000000028	3	3	f	23	2020-07-24	2020-07-22 13:25:56.572	38.4600000000000009	1.72999999999999998	0.0100000000000000002	\N	\N	\N	2020-07-22 01:26:08	40.2000000000000028	\N	\N
414651	85	t	19.2300000000000004	2.31000000000000005	0.0599999999999999978	0	21.6000000000000014	3	3	f	10	2020-07-20	2020-07-22 19:51:56.716	19.2300000000000004	2.31000000000000005	0.0599999999999999978	\N	\N	\N	2020-07-22 07:51:57	21.6000000000000014	\N	\N
414852	101	t	19.2300000000000004	1.54000000000000004	0.0299999999999999989	0	20.8000000000000007	3	3	f	9	2020-07-21	2020-07-22 19:53:18.985	19.2300000000000004	1.54000000000000004	0.0299999999999999989	\N	\N	\N	2020-07-22 07:53:19	20.8000000000000007	\N	\N
414625	86	t	26.9200000000000017	3.22999999999999998	0.0500000000000000028	0	30.1999999999999993	3	3	f	10	2020-07-20	2020-07-22 19:53:47.33	26.9200000000000017	3.22999999999999998	0.0500000000000000028	\N	\N	\N	2020-07-22 07:53:47	30.1999999999999993	\N	\N
414550	67	t	19.2300000000000004	1.35000000000000009	0.0200000000000000004	0	20.6000000000000014	3	3	f	12	2020-07-20	2020-07-23 13:34:46.485	19.2300000000000004	1.35000000000000009	0.0200000000000000004	\N	\N	\N	2020-07-23 01:34:47	20.6000000000000014	\N	\N
414831	102	t	26.9200000000000017	3.22999999999999998	0.0500000000000000028	0	30.1999999999999993	3	3	f	14	2020-07-27	2020-07-23 18:16:25.844	26.9200000000000017	3.22999999999999998	0.0500000000000000028	\N	\N	\N	2020-07-23 06:16:28	30.1999999999999993	\N	\N
414912	113	t	7.69000000000000039	0.689999999999999947	0.0200000000000000004	0	8.40000000000000036	3	3	f	9	2020-07-24	2020-07-24 12:26:34.331	7.69000000000000039	0.689999999999999947	0.0200000000000000004	\N	\N	\N	2020-07-24 12:26:36	8.40000000000000036	\N	\N
414915	113	t	7.69000000000000039	0.689999999999999947	0.0200000000000000004	0	8.40000000000000036	3	3	f	12	2020-07-28	2020-07-24 12:26:34.331	7.69000000000000039	0.689999999999999947	0.0200000000000000004	\N	\N	\N	2020-07-24 12:26:39	8.40000000000000036	\N	\N
414979	115	t	9.69999999999999929	0.110000000000000001	0	0	9.80000000000000071	3	3	f	7	2020-07-22	2020-07-24 12:29:11.577	9.69999999999999929	0.110000000000000001	0	\N	\N	\N	2020-07-24 12:29:13	9.8100000000000005	\N	\N
415175	127	t	16.3500000000000014	0.650000000000000022	0	0	17	3	3	f	1	2020-07-23	2020-07-24 13:49:53.914	16.3500000000000014	0.650000000000000022	0	\N	\N	\N	2020-07-24 01:49:54	17	\N	\N
414357	15	t	7.69000000000000039	0.46000000000000002	0.0500000000000000028	0	8.19999999999999929	3	3	f	16	2020-07-18	2020-07-24 13:55:27.777	7.69000000000000039	0.46000000000000002	0.0500000000000000028	\N	\N	\N	2020-07-24 01:55:29	8.19999999999999929	\N	\N
414531	62	t	54.3400000000000034	3.39625000000000021	0	0	57.75	3	3	f	3	2020-07-25	2020-07-25 12:11:24.436	54.3400000000000034	3.39599999999999991	0	\N	\N	\N	2020-07-25 12:11:25	57.7359999999999971	\N	\N
414837	102	t	26.9200000000000017	3.22999999999999998	0.0500000000000000028	0	30.1999999999999993	3	3	f	20	2020-08-04	2020-07-25 17:22:58.899	26.9200000000000017	3.22999999999999998	0.0500000000000000028	\N	\N	\N	2020-07-25 05:23:04	30.1999999999999993	\N	\N
414657	85	t	19.2300000000000004	2.31000000000000005	0.0599999999999999978	0	21.6000000000000014	3	3	f	16	2020-07-27	2020-07-29 16:14:46.839	19.2300000000000004	2.31000000000000005	0.0599999999999999978	\N	\N	\N	2020-07-29 04:14:48	21.6000000000000014	\N	\N
414857	101	t	19.2300000000000004	1.54000000000000004	0.0299999999999999989	0	20.8000000000000007	3	3	f	14	2020-07-27	2020-07-29 16:15:59.388	19.2300000000000004	1.54000000000000004	0.0299999999999999989	\N	\N	\N	2020-07-29 04:16:01	20.8000000000000007	\N	\N
415228	125	t	200	24	0	0	224	3	3	f	2	2020-08-04	2020-08-04 20:52:11.438	200	24	0	\N	\N	\N	2020-08-04 08:52:12	224	\N	\N
415227	125	t	200	24	0	0	224	3	3	f	1	2020-07-28	2020-07-29 16:16:43.284	200	24	0	\N	\N	\N	2020-07-29 04:16:43	224	\N	\N
414841	102	t	26.9200000000000017	3.22999999999999998	0.0500000000000000028	0	30.1999999999999993	3	3	f	24	2020-08-08	2020-07-29 16:19:07.876	26.9200000000000017	3.22999999999999998	0.0500000000000000028	\N	\N	\N	2020-07-29 04:19:11	30.1999999999999993	\N	\N
414659	85	t	19.2300000000000004	2.31000000000000005	0.0599999999999999978	0	21.6000000000000014	3	3	f	18	2020-07-30	2020-07-30 16:25:51.996	19.2300000000000004	2.31000000000000005	0.0599999999999999978	\N	\N	\N	2020-07-30 04:25:53	21.6000000000000014	\N	\N
415231	128	t	38.4600000000000009	4.62000000000000011	0.0200000000000000004	0	43.1000000000000014	3	3	f	1	2020-07-31	2020-08-01 18:43:00.77	38.4600000000000009	4.62000000000000011	0.0200000000000000004	\N	\N	\N	2020-08-01 06:43:00	43.1000000000000014	\N	\N
415233	128	t	38.4600000000000009	4.62000000000000011	0.0200000000000000004	0	43.1000000000000014	3	3	f	3	2020-08-03	2020-08-04 20:43:00.351	38.4600000000000009	4.62000000000000011	0.0200000000000000004	\N	\N	\N	2020-08-04 08:43:01	43.1000000000000014	\N	\N
415232	128	t	38.4600000000000009	4.62000000000000011	0.0200000000000000004	0	43.1000000000000014	3	3	f	2	2020-08-01	2020-08-03 21:46:47.996	38.4600000000000009	4.62000000000000011	0.0200000000000000004	\N	\N	\N	2020-08-03 09:46:48	43.1000000000000014	\N	\N
415234	128	t	38.4600000000000009	4.62000000000000011	0.0200000000000000004	0	43.1000000000000014	3	3	f	4	2020-08-04	2020-08-05 21:29:04.601	38.4600000000000009	4.62000000000000011	0.0200000000000000004	\N	\N	\N	2020-08-05 09:29:05	43.1000000000000014	\N	\N
415235	128	t	38.4600000000000009	4.62000000000000011	0.0200000000000000004	0	43.1000000000000014	3	3	f	5	2020-08-05	2020-08-07 17:26:35.679	38.4600000000000009	4.62000000000000011	0.0200000000000000004	\N	\N	\N	2020-08-07 05:26:36	43.1000000000000014	\N	\N
415236	128	t	38.4600000000000009	4.62000000000000011	0.0200000000000000004	0	43.1000000000000014	3	3	f	6	2020-08-06	2020-08-07 17:26:35.679	38.4600000000000009	4.62000000000000011	0.0200000000000000004	\N	\N	\N	2020-08-07 05:26:37	43.1000000000000014	\N	\N
415242	128	t	38.4600000000000009	4.62000000000000011	0.0200000000000000004	0	43.1000000000000014	3	3	f	12	2020-08-13	2020-08-18 17:57:52.241	38.4600000000000009	4.62000000000000011	0.0200000000000000004	\N	\N	\N	2020-08-18 05:57:53	43.1000000000000014	\N	\N
415237	128	t	38.4600000000000009	4.62000000000000011	0.0200000000000000004	0	43.1000000000000014	3	3	f	7	2020-08-07	2020-08-10 19:17:08.706	38.4600000000000009	4.62000000000000011	0.0200000000000000004	\N	\N	\N	2020-08-10 07:17:09	43.1000000000000014	\N	\N
415229	125	t	200	24	0	0	224	3	3	f	3	2020-08-11	2020-08-11 17:53:06.167	200	24	0	\N	\N	\N	2020-08-11 05:53:06	224	\N	\N
415238	128	t	38.4600000000000009	4.62000000000000011	0.0200000000000000004	0	43.1000000000000014	3	3	f	8	2020-08-08	2020-08-11 17:53:40.868	38.4600000000000009	4.62000000000000011	0.0200000000000000004	\N	\N	\N	2020-08-11 05:53:41	43.1000000000000014	\N	\N
415239	128	t	38.4600000000000009	4.62000000000000011	0.0200000000000000004	0	43.1000000000000014	3	3	f	9	2020-08-10	2020-08-14 18:16:13.257	38.4600000000000009	4.62000000000000011	0.0200000000000000004	\N	\N	\N	2020-08-14 06:16:13	43.1000000000000014	\N	\N
415240	128	t	38.4600000000000009	4.62000000000000011	0.0200000000000000004	0	43.1000000000000014	3	3	f	10	2020-08-11	2020-08-14 18:16:13.257	38.4600000000000009	4.62000000000000011	0.0200000000000000004	\N	\N	\N	2020-08-14 06:16:14	43.1000000000000014	\N	\N
415241	128	t	38.4600000000000009	4.62000000000000011	0.0200000000000000004	0	43.1000000000000014	3	3	f	11	2020-08-12	2020-08-14 18:16:13.257	38.4600000000000009	4.62000000000000011	0.0200000000000000004	\N	\N	\N	2020-08-14 06:16:15	43.1000000000000014	\N	\N
415243	128	t	38.4600000000000009	4.62000000000000011	0.0200000000000000004	0	43.1000000000000014	3	3	f	13	2020-08-14	2020-08-18 17:57:52.241	38.4600000000000009	4.62000000000000011	0.0200000000000000004	\N	\N	\N	2020-08-18 05:57:54	43.1000000000000014	\N	\N
415230	125	t	200	24	0	0	224	3	3	f	4	2020-08-18	2020-08-18 17:59:53.482	200	24	0	\N	\N	\N	2020-08-18 05:59:54	224	\N	\N
415244	128	t	38.4600000000000009	4.62000000000000011	0.0200000000000000004	0	43.1000000000000014	3	3	f	14	2020-08-15	2020-08-23 10:41:02.963	38.4600000000000009	4.62000000000000011	0.0200000000000000004	\N	\N	\N	2020-08-23 10:41:03	43.1000000000000014	\N	\N
415245	128	t	38.4600000000000009	4.62000000000000011	0.0200000000000000004	0	43.1000000000000014	3	3	f	15	2020-08-17	2020-08-23 10:41:02.963	38.4600000000000009	4.62000000000000011	0.0200000000000000004	\N	\N	\N	2020-08-23 10:41:04	43.1000000000000014	\N	\N
415246	128	t	38.4600000000000009	4.62000000000000011	0.0200000000000000004	0	43.1000000000000014	3	3	f	16	2020-08-18	2020-08-23 10:41:02.963	38.4600000000000009	4.62000000000000011	0.0200000000000000004	\N	\N	\N	2020-08-23 10:41:06	43.1000000000000014	\N	\N
415247	128	t	38.4600000000000009	4.62000000000000011	0.0200000000000000004	0	43.1000000000000014	3	3	f	17	2020-08-19	2020-08-23 10:41:02.963	38.4600000000000009	4.62000000000000011	0.0200000000000000004	\N	\N	\N	2020-08-23 10:41:07	43.1000000000000014	\N	\N
415249	128	f	38.4600000000000009	4.62000000000000011	0.0200000000000000004	0	43.1000000000000014	3	3	f	19	2020-08-21	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415250	128	f	38.4600000000000009	4.62000000000000011	0.0200000000000000004	0	43.1000000000000014	3	3	f	20	2020-08-22	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415251	128	f	38.4600000000000009	4.62000000000000011	0.0200000000000000004	0	43.1000000000000014	3	3	f	21	2020-08-24	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415252	128	f	38.4600000000000009	4.62000000000000011	0.0200000000000000004	0	43.1000000000000014	3	3	f	22	2020-08-25	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415253	128	f	38.4600000000000009	4.62000000000000011	0.0200000000000000004	0	43.1000000000000014	3	3	f	23	2020-08-26	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415254	128	f	38.4600000000000009	4.62000000000000011	0.0200000000000000004	0	43.1000000000000014	3	3	f	24	2020-08-27	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415255	128	f	38.4600000000000009	4.62000000000000011	0.0200000000000000004	0	43.1000000000000014	3	3	f	25	2020-08-28	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415256	128	f	38.4600000000000009	4.62000000000000011	0.0200000000000000004	0	43.1000000000000014	3	3	f	26	2020-08-29	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414861	101	t	19.2300000000000004	1.54000000000000004	0.0299999999999999989	0	20.8000000000000007	3	3	f	18	2020-08-01	2020-08-01 18:42:23.28	19.2300000000000004	1.54000000000000004	0.0299999999999999989	\N	\N	\N	2020-08-01 06:42:23	20.8000000000000007	\N	\N
415273	130	t	19.2300000000000004	2.31000000000000005	0.0599999999999999978	0	21.6000000000000014	3	3	f	2	2020-08-04	2020-08-05 21:25:50.812	19.2300000000000004	2.31000000000000005	0.0599999999999999978	\N	\N	\N	2020-08-05 09:25:52	21.6000000000000014	\N	\N
415257	129	t	20	1.14999999999999991	0.0500000000000000028	0	21.1999999999999993	3	3	f	1	2020-07-31	2020-08-01 18:43:30.61	20	1.14999999999999991	0.0500000000000000028	\N	\N	\N	2020-08-01 06:43:30	21.1999999999999993	\N	\N
414665	85	t	19.2300000000000004	2.31000000000000005	0.0599999999999999978	0	21.6000000000000014	3	3	f	24	2020-08-06	2020-08-01 18:44:29.884	19.2300000000000004	2.31000000000000005	0.0599999999999999978	\N	\N	\N	2020-08-01 06:44:33	21.6000000000000014	\N	\N
415272	130	t	19.2300000000000004	2.31000000000000005	0.0599999999999999978	0	21.6000000000000014	3	3	f	1	2020-08-03	2020-08-05 21:25:50.812	19.2300000000000004	2.31000000000000005	0.0599999999999999978	\N	\N	\N	2020-08-05 09:25:51	21.6000000000000014	\N	\N
415258	129	t	20	1.14999999999999991	0.0500000000000000028	0	21.1999999999999993	3	3	f	2	2020-08-01	2020-08-03 21:47:42.192	20	1.14999999999999991	0.0500000000000000028	\N	\N	\N	2020-08-03 09:47:42	21.1999999999999993	\N	\N
414864	101	t	19.2300000000000004	1.54000000000000004	0.0299999999999999989	0	20.8000000000000007	3	3	f	21	2020-08-05	2020-08-05 21:29:43.528	19.2300000000000004	1.54000000000000004	0.0299999999999999989	\N	\N	\N	2020-08-05 09:29:45	20.8000000000000007	\N	\N
415259	129	t	20	1.14999999999999991	0.0500000000000000028	0	21.1999999999999993	3	3	f	3	2020-08-03	2020-08-04 20:43:39.358	20	1.14999999999999991	0.0500000000000000028	\N	\N	\N	2020-08-04 08:43:40	21.1999999999999993	\N	\N
415288	130	f	19.2300000000000004	2.31000000000000005	0.0599999999999999978	0	21.6000000000000014	3	3	f	17	2020-08-21	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415289	130	f	19.2300000000000004	2.31000000000000005	0.0599999999999999978	0	21.6000000000000014	3	3	f	18	2020-08-22	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415290	130	f	19.2300000000000004	2.31000000000000005	0.0599999999999999978	0	21.6000000000000014	3	3	f	19	2020-08-24	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415291	130	f	19.2300000000000004	2.31000000000000005	0.0599999999999999978	0	21.6000000000000014	3	3	f	20	2020-08-25	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415292	130	f	19.2300000000000004	2.31000000000000005	0.0599999999999999978	0	21.6000000000000014	3	3	f	21	2020-08-26	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415293	130	f	19.2300000000000004	2.31000000000000005	0.0599999999999999978	0	21.6000000000000014	3	3	f	22	2020-08-27	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415294	130	f	19.2300000000000004	2.31000000000000005	0.0599999999999999978	0	21.6000000000000014	3	3	f	23	2020-08-28	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415295	130	f	19.2300000000000004	2.31000000000000005	0.0599999999999999978	0	21.6000000000000014	3	3	f	24	2020-08-29	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415296	130	f	19.2300000000000004	2.31000000000000005	0.0599999999999999978	0	21.6000000000000014	3	3	f	25	2020-08-31	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415297	130	f	19.2300000000000004	2.31000000000000005	0.0599999999999999978	0	21.6000000000000014	3	3	f	26	2020-09-01	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414638	86	t	26.9200000000000017	3.22999999999999998	0.0500000000000000028	0	30.1999999999999993	3	3	f	23	2020-08-05	2020-08-05 21:28:11.777	26.9200000000000017	3.22999999999999998	0.0500000000000000028	\N	\N	\N	2020-08-05 09:28:12	30.1999999999999993	\N	\N
414901	107	t	125	10	0	0	135	3	3	f	4	2020-08-08	2020-08-06 18:14:20.339	125	10	0	\N	\N	\N	2020-08-06 06:14:20	135	\N	\N
415260	129	t	20	1.14999999999999991	0.0500000000000000028	0	21.1999999999999993	3	3	f	4	2020-08-04	2020-08-05 21:30:14.746	20	1.14999999999999991	0.0500000000000000028	\N	\N	\N	2020-08-05 09:30:15	21.1999999999999993	\N	\N
415261	129	t	20	1.14999999999999991	0.0500000000000000028	0	21.1999999999999993	3	3	f	5	2020-08-05	2020-08-06 18:11:16.158	20	1.14999999999999991	0.0500000000000000028	\N	\N	\N	2020-08-06 06:11:16	21.1999999999999993	\N	\N
415274	130	t	19.2300000000000004	2.31000000000000005	0.0599999999999999978	0	21.6000000000000014	3	3	f	3	2020-08-05	2020-08-06 18:13:15.708	19.2300000000000004	2.31000000000000005	0.0599999999999999978	\N	\N	\N	2020-08-06 06:13:16	21.6000000000000014	\N	\N
415285	130	t	19.2300000000000004	2.31000000000000005	0.0599999999999999978	0	21.6000000000000014	3	3	f	14	2020-08-18	2020-08-23 10:39:10.258	19.2300000000000004	2.31000000000000005	0.0599999999999999978	\N	\N	\N	2020-08-23 10:39:11	21.6000000000000014	\N	\N
415262	129	t	20	1.14999999999999991	0.0500000000000000028	0	21.1999999999999993	3	3	f	6	2020-08-06	2020-08-07 17:28:52.95	20	1.14999999999999991	0.0500000000000000028	\N	\N	\N	2020-08-07 05:28:52	21.1999999999999993	\N	\N
415276	130	t	19.2300000000000004	2.31000000000000005	0.0599999999999999978	0	21.6000000000000014	3	3	f	5	2020-08-07	2020-08-10 19:18:17.889	19.2300000000000004	2.31000000000000005	0.0599999999999999978	\N	\N	\N	2020-08-10 07:18:18	21.6000000000000014	\N	\N
415277	130	t	19.2300000000000004	2.31000000000000005	0.0599999999999999978	0	21.6000000000000014	3	3	f	6	2020-08-08	2020-08-10 19:18:17.889	19.2300000000000004	2.31000000000000005	0.0599999999999999978	\N	\N	\N	2020-08-10 07:18:19	21.6000000000000014	\N	\N
415263	129	t	20	1.14999999999999991	0.0500000000000000028	0	21.1999999999999993	3	3	f	7	2020-08-07	2020-08-10 19:19:40.721	20	1.14999999999999991	0.0500000000000000028	\N	\N	\N	2020-08-10 07:19:41	21.1999999999999993	\N	\N
415264	129	t	20	1.14999999999999991	0.0500000000000000028	0	21.1999999999999993	3	3	f	8	2020-08-08	2020-08-11 17:50:43.953	20	1.14999999999999991	0.0500000000000000028	\N	\N	\N	2020-08-11 05:50:45	21.1999999999999993	\N	\N
415265	129	t	20	1.14999999999999991	0.0500000000000000028	0	21.1999999999999993	3	3	f	9	2020-08-10	2020-08-11 17:50:43.953	20	1.14999999999999991	0.0500000000000000028	\N	\N	\N	2020-08-11 05:50:46	21.1999999999999993	\N	\N
415279	130	t	19.2300000000000004	2.31000000000000005	0.0599999999999999978	0	21.6000000000000014	3	3	f	8	2020-08-11	2020-08-14 18:13:43.117	19.2300000000000004	2.31000000000000005	0.0599999999999999978	\N	\N	\N	2020-08-14 06:13:44	21.6000000000000014	\N	\N
415280	130	t	19.2300000000000004	2.31000000000000005	0.0599999999999999978	0	21.6000000000000014	3	3	f	9	2020-08-12	2020-08-14 18:13:43.117	19.2300000000000004	2.31000000000000005	0.0599999999999999978	\N	\N	\N	2020-08-14 06:13:45	21.6000000000000014	\N	\N
415266	129	t	20	1.14999999999999991	0.0500000000000000028	0	21.1999999999999993	3	3	f	10	2020-08-11	2020-08-14 18:15:19.258	20	1.14999999999999991	0.0500000000000000028	\N	\N	\N	2020-08-14 06:15:20	21.1999999999999993	\N	\N
415281	130	t	19.2300000000000004	2.31000000000000005	0.0599999999999999978	0	21.6000000000000014	3	3	f	10	2020-08-13	2020-08-18 18:00:39.745	19.2300000000000004	2.31000000000000005	0.0599999999999999978	\N	\N	\N	2020-08-18 06:00:40	21.6000000000000014	\N	\N
415282	130	t	19.2300000000000004	2.31000000000000005	0.0599999999999999978	0	21.6000000000000014	3	3	f	11	2020-08-14	2020-08-18 18:00:39.745	19.2300000000000004	2.31000000000000005	0.0599999999999999978	\N	\N	\N	2020-08-18 06:00:41	21.6000000000000014	\N	\N
415284	130	t	19.2300000000000004	2.31000000000000005	0.0599999999999999978	0	21.6000000000000014	3	3	f	13	2020-08-17	2020-08-18 18:00:39.745	19.2300000000000004	2.31000000000000005	0.0599999999999999978	\N	\N	\N	2020-08-18 06:00:43	21.6000000000000014	\N	\N
415271	129	t	20	1.14999999999999991	0.0500000000000000028	0	21.1999999999999993	3	3	f	15	2020-08-17	2020-08-23 10:38:00.439	20	1.14999999999999991	0.0500000000000000028	\N	\N	\N	2020-08-23 10:38:01	21.1999999999999993	\N	\N
415268	129	t	20	1.14999999999999991	0.0500000000000000028	0	21.1999999999999993	3	3	f	12	2020-08-13	2020-08-18 18:02:45.81	20	1.14999999999999991	0.0500000000000000028	\N	\N	\N	2020-08-18 06:02:46	21.1999999999999993	\N	\N
415270	129	t	20	1.14999999999999991	0.0500000000000000028	0	21.1999999999999993	3	3	f	14	2020-08-15	2020-08-18 18:02:45.81	20	1.14999999999999991	0.0500000000000000028	\N	\N	\N	2020-08-18 06:02:48	21.1999999999999993	\N	\N
415287	130	t	19.2300000000000004	2.31000000000000005	0.0599999999999999978	0	21.6000000000000014	3	3	f	16	2020-08-20	2020-08-23 10:39:10.258	19.2300000000000004	2.31000000000000005	0.0599999999999999978	\N	\N	\N	2020-08-23 10:39:13	21.6000000000000014	\N	\N
415303	131	f	150	12	0	0	162	3	3	f	2	2020-08-20	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415304	131	f	150	12	0	0	162	3	3	f	3	2020-08-27	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415305	131	f	150	12	0	0	162	3	3	f	4	2020-09-03	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415275	130	t	19.2300000000000004	2.31000000000000005	0.0599999999999999978	0	21.6000000000000014	3	3	f	4	2020-08-06	2020-08-07 17:28:11.151	19.2300000000000004	2.31000000000000005	0.0599999999999999978	\N	\N	\N	2020-08-07 05:28:11	21.6000000000000014	\N	\N
415318	132	f	30.7699999999999996	3.68999999999999995	0.0400000000000000008	0	34.5	3	3	f	13	2020-08-22	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415319	132	f	30.7699999999999996	3.68999999999999995	0.0400000000000000008	0	34.5	3	3	f	14	2020-08-24	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415320	132	f	30.7699999999999996	3.68999999999999995	0.0400000000000000008	0	34.5	3	3	f	15	2020-08-25	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415321	132	f	30.7699999999999996	3.68999999999999995	0.0400000000000000008	0	34.5	3	3	f	16	2020-08-26	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415322	132	f	30.7699999999999996	3.68999999999999995	0.0400000000000000008	0	34.5	3	3	f	17	2020-08-27	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415323	132	f	30.7699999999999996	3.68999999999999995	0.0400000000000000008	0	34.5	3	3	f	18	2020-08-28	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415324	132	f	30.7699999999999996	3.68999999999999995	0.0400000000000000008	0	34.5	3	3	f	19	2020-08-29	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415325	132	f	30.7699999999999996	3.68999999999999995	0.0400000000000000008	0	34.5	3	3	f	20	2020-08-31	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415326	132	f	30.7699999999999996	3.68999999999999995	0.0400000000000000008	0	34.5	3	3	f	21	2020-09-01	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415327	132	f	30.7699999999999996	3.68999999999999995	0.0400000000000000008	0	34.5	3	3	f	22	2020-09-02	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415328	132	f	30.7699999999999996	3.68999999999999995	0.0400000000000000008	0	34.5	3	3	f	23	2020-09-03	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415329	132	f	30.7699999999999996	3.68999999999999995	0.0400000000000000008	0	34.5	3	3	f	24	2020-09-04	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415330	132	f	30.7699999999999996	3.68999999999999995	0.0400000000000000008	0	34.5	3	3	f	25	2020-09-05	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
415331	132	f	30.7699999999999996	3.68999999999999995	0.0400000000000000008	0	34.5	3	3	f	26	2020-09-07	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
414868	101	t	19.2300000000000004	1.54000000000000004	0.0299999999999999989	0	20.8000000000000007	3	3	f	25	2020-08-10	2020-08-10 19:16:29.181	19.2300000000000004	1.54000000000000004	0.0299999999999999989	\N	\N	\N	2020-08-10 07:16:29	20.8000000000000007	\N	\N
415314	132	t	30.7699999999999996	3.68999999999999995	0.0400000000000000008	0	34.5	3	3	f	9	2020-08-18	2020-08-23 10:40:03.697	30.7699999999999996	3.68999999999999995	0.0400000000000000008	\N	\N	\N	2020-08-23 10:40:05	34.5	\N	\N
415306	132	t	30.7699999999999996	3.68999999999999995	0.0400000000000000008	0	34.5	3	3	f	1	2020-08-08	2020-08-10 19:18:53.401	30.7699999999999996	3.68999999999999995	0.0400000000000000008	\N	\N	\N	2020-08-10 07:18:54	34.5	\N	\N
414729	92	t	58.2899999999999991	18.6499999999999986	0.0599999999999999978	0	77	3	3	f	1	2020-07-24	2020-08-11 09:34:17.564	58.2899999999999991	18.6499999999999986	0.0599999999999999978	\N	\N	\N	2020-08-11 09:34:18	77	\N	\N
415278	130	t	19.2300000000000004	2.31000000000000005	0.0599999999999999978	0	21.6000000000000014	3	3	f	7	2020-08-10	2020-08-11 17:52:32.613	19.2300000000000004	2.31000000000000005	0.0599999999999999978	\N	\N	\N	2020-08-11 05:52:33	21.6000000000000014	\N	\N
415311	132	t	30.7699999999999996	3.68999999999999995	0.0400000000000000008	0	34.5	3	3	f	6	2020-08-14	2020-08-18 17:59:00.49	30.7699999999999996	3.68999999999999995	0.0400000000000000008	\N	\N	\N	2020-08-18 05:59:01	34.5	\N	\N
415307	132	t	30.7699999999999996	3.68999999999999995	0.0400000000000000008	0	34.5	3	3	f	2	2020-08-10	2020-08-11 17:54:17.894	30.7699999999999996	3.68999999999999995	0.0400000000000000008	\N	\N	\N	2020-08-11 05:54:18	34.5	\N	\N
415308	132	t	30.7699999999999996	3.68999999999999995	0.0400000000000000008	0	34.5	3	3	f	3	2020-08-11	2020-08-14 18:12:18.665	30.7699999999999996	3.68999999999999995	0.0400000000000000008	\N	\N	\N	2020-08-14 06:12:19	34.5	\N	\N
415312	132	t	30.7699999999999996	3.68999999999999995	0.0400000000000000008	0	34.5	3	3	f	7	2020-08-15	2020-08-18 17:59:00.49	30.7699999999999996	3.68999999999999995	0.0400000000000000008	\N	\N	\N	2020-08-18 05:59:02	34.5	\N	\N
415309	132	t	30.7699999999999996	3.68999999999999995	0.0400000000000000008	0	34.5	3	3	f	4	2020-08-12	2020-08-14 18:12:18.665	30.7699999999999996	3.68999999999999995	0.0400000000000000008	\N	\N	\N	2020-08-14 06:12:20	34.5	\N	\N
415283	130	t	19.2300000000000004	2.31000000000000005	0.0599999999999999978	0	21.6000000000000014	3	3	f	12	2020-08-15	2020-08-18 18:00:39.745	19.2300000000000004	2.31000000000000005	0.0599999999999999978	\N	\N	\N	2020-08-18 06:00:42	21.6000000000000014	\N	\N
415310	132	t	30.7699999999999996	3.68999999999999995	0.0400000000000000008	0	34.5	3	3	f	5	2020-08-13	2020-08-14 18:12:18.665	30.7699999999999996	3.68999999999999995	0.0400000000000000008	\N	\N	\N	2020-08-14 06:12:21	34.5	\N	\N
415267	129	t	20	1.14999999999999991	0.0500000000000000028	0	21.1999999999999993	3	3	f	11	2020-08-12	2020-08-14 18:15:19.258	20	1.14999999999999991	0.0500000000000000028	\N	\N	\N	2020-08-14 06:15:21	21.1999999999999993	\N	\N
414361	15	t	7.69000000000000039	0.46000000000000002	0.0500000000000000028	0	8.19999999999999929	3	3	f	20	2020-07-23	2020-08-18 13:37:27.2	7.69000000000000039	0.46000000000000002	0.0500000000000000028	\N	\N	\N	2020-08-18 01:37:27	8.19999999999999929	\N	\N
415315	132	t	30.7699999999999996	3.68999999999999995	0.0400000000000000008	0	34.5	3	3	f	10	2020-08-19	2020-08-23 10:40:03.697	30.7699999999999996	3.68999999999999995	0.0400000000000000008	\N	\N	\N	2020-08-23 10:40:06	34.5	\N	\N
415302	131	t	150	12	0	0	162	3	3	f	1	2020-08-13	2020-08-18 18:01:25.193	150	12	0	\N	\N	\N	2020-08-18 06:01:25	162	\N	\N
415269	129	t	20	1.14999999999999991	0.0500000000000000028	0	21.1999999999999993	3	3	f	13	2020-08-14	2020-08-18 18:02:45.81	20	1.14999999999999991	0.0500000000000000028	\N	\N	\N	2020-08-18 06:02:47	21.1999999999999993	\N	\N
415286	130	t	19.2300000000000004	2.31000000000000005	0.0599999999999999978	0	21.6000000000000014	3	3	f	15	2020-08-19	2020-08-23 10:39:10.258	19.2300000000000004	2.31000000000000005	0.0599999999999999978	\N	\N	\N	2020-08-23 10:39:12	21.6000000000000014	\N	\N
415313	132	t	30.7699999999999996	3.68999999999999995	0.0400000000000000008	0	34.5	3	3	f	8	2020-08-17	2020-08-23 10:40:03.697	30.7699999999999996	3.68999999999999995	0.0400000000000000008	\N	\N	\N	2020-08-23 10:40:04	34.5	\N	\N
415316	132	t	30.7699999999999996	3.68999999999999995	0.0400000000000000008	0	34.5	3	3	f	11	2020-08-20	2020-08-23 10:40:03.697	30.7699999999999996	3.68999999999999995	0.0400000000000000008	\N	\N	\N	2020-08-23 10:40:07	34.5	\N	\N
415317	132	t	30.7699999999999996	3.68999999999999995	0.0400000000000000008	0	34.5	3	3	f	12	2020-08-21	2020-08-23 10:40:03.697	30.7699999999999996	3.68999999999999995	0.0400000000000000008	\N	\N	\N	2020-08-23 10:40:08	34.5	\N	\N
415248	128	f	38.4600000000000009	4.62000000000000011	0.0200000000000000004	0	43.1000000000000014	3	3	f	18	2020-08-20	2020-08-23 10:41:02.963	0	\N	\N	\N	\N	\N	\N	\N	\N	\N
\.


--
-- Data for Name: departamento; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.departamento (id_departamento, departamento) FROM stdin;
1	JUNIN
2	LIMA
3	HUANUCO
4	UCAYALI
\.


--
-- Data for Name: detalle_negocio; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.detalle_negocio (id_detalle_negocio, id_negocio, tipo_negocio, cantidad, descripcion, frecuencia_mes, precio_compra, precio_venta, total_compra, total_venta, usuario, agencia, fecha_hora, estado) FROM stdin;
\.


--
-- Data for Name: dias_festivos; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.dias_festivos (id_dia_festivo, dia, mes, anio, titulo, id_agencia) FROM stdin;
1	1	1	0	n	1
2	29	7	0	n	1
4	25	12	0	n	1
5	29	6	0	n	1
6	8	10	0	n	1
7	9	4	0	n	1
3	1	5	0	n	1
\.


--
-- Data for Name: distrito; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.distrito (id_distrito, id_provincia, distrito) FROM stdin;
1	1	CHANCHAMAYO
2	1	PERENE
3	1	PICHANAQUI
4	1	SAN LUIS DE CHUARO
5	1	SAN RAMÒN
6	1	VITOC
7	2	AHUAC
8	2	CHONGOS BAJO
9	2	CHUPACA
10	2	HUACHAC
11	2	HUAMANCACA CHICO
12	2	SAN JUAN DE JARPA
13	2	SAN JUAN DE ISCOS
14	2	TRES DE DICIEMBRE
15	2	YANACANCHA
16	3	ANDAMARCA
17	3	ACO
18	3	CHAMBARA
19	3	COCHAS
20	3	COMAS
21	3	CONCEPCIÒN
22	3	HEROINAS TOLEDO
23	3	MANZANARES
24	3	MATAHUASI
25	3	MITO
26	3	NUEVE DE JULIO
27	3	ORCOTUNA
28	3	SAN JOSE DE QUERO
29	3	SANTA ROSA DE OCOPA
30	4	HUANCAYO
31	4	CARHUACALLAGA
32	4	CHACAPAMPA
33	4	CHICCHE
34	4	CHILCA
35	4	CHONGOS ALTO
36	4	CHUPURO
37	4	COLCA
38	4	CULLHUAS
39	4	EL TAMBO
40	4	HUACRAPUQUIO
41	4	HUALHUAS
42	4	HUANCÁN
43	4	HUASICANCHA
44	4	HUAYUCACHI
45	4	INGENIO
46	4	PARIAHUANCA
47	4	PILCOMAYO
48	4	PUCARÁ
49	4	QUICHUAY
50	4	QUILCAS
51	4	SAN AGUSTÍN DE CAJAS
52	4	SAN JERÓNIMO DE TUNÁN
53	4	SAN PEDRO DE SAÑOS
54	4	SANTO DOMINGO DE ACOB.
56	4	SICAYA
57	4	VIQUES
58	5	JAUJA
59	6	SATIPO
60	7	TARMA
55	4	SAPALLANGA
61	8	YAULI
62	9	CASTILLO GRANDE
63	9	JOSE CRESPO Y CASTILLO
64	9	PUCAYACU
65	9	PUEBLO NUEVO
66	9	DANIEL ALOMIA ROBLES
67	9	LUYANDO
68	9	RUPA-RUPA
69	9	HERMILIO VALDIZAN
70	9	MARIANO DAMAZO BERAUN
71	9	SANTO DOMINGO DE ANDA
72	10	RAYMONDI
73	10	SEPAHUA
74	10	TAHUANIA
75	10	YURUA
76	10	CALLERIA
77	10	CAMPOVERDE
78	10	IPARIA
79	10	MANANTAY
80	10	MASISEA
81	10	NUEVA REQUENA
82	10	YARINACOHCA
83	10	CURIMANÁ
84	10	IRZOLA
85	10	PADRE ABAD
86	10	PURÚS
\.


--
-- Data for Name: empresa; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.empresa (id_empresa, nombre_empresa, codigo_empresa, detalles_empresa, nombre_corto) FROM stdin;
1	COOP. DE SERVICIOS MÚLTIPLES PROINVERSIONES RUC:20603098553	1	detalles	COOP. PROINVERSIONES RUC:20603098553
\.


--
-- Data for Name: equipos; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.equipos (id_aquipos, id_agencia, nombre_equipo, area) FROM stdin;
1	1	DESKTOP-CQ135NL	INFORMATICA
2	1	DJ-XAVEX	GERENCIA
3	1	DESKTOP-2COHNSG	ADMINISTRACION
4	1	DESKTOP-RD921RC	ASESOR01
5	1	DESKTOP-PDVNOOT	ASESOR02
6	1	DESKTOP-6HII7R6	CAJA
7	3	ADMINIPUCALLPA	CAJA-ASESOR
\.


--
-- Data for Name: ficha_domiciliaria; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.ficha_domiciliaria (id_ficha_domiciliaria, id_analista, id_solicitud, agencia, dni_cliente, zonificacion, t_inmueble, t_edificacion, t_zona, t_construccion, est_inmueble, zona, serv_basicos, grado_peligro, num_depend, name_conyuge, nivel_instruccion, acupacion, direccion_correcta, jardin, cochera, pisos_nro, acabados, conservacion, seguridad, ubicacion, vigilancia, pista, vereda, atendio, residencia, condicion, t_residencia, estado_civil, habita, medio_comunicacion, name_informante, documento_dni, parentesco, telefono_infor, cerco, fachada, paredes, techo, piso, puertas, ventana, suministro, coment_primera_v, coment_segunda_v, nombre_vecino, obs_vecino, estado_formulario, fecha_hora_ficha, fecha_hora_modi, id_agencia, obs) FROM stdin;
0	2	\N	HUANCAYO	19940774	Alejada	Casa		Urbana	Noble	Bueno	Buena	Luz, Agua y Desague		0		SECUNDARIA	Independiente	JR CUSCO 1985 LA RIVERA	923360616 		3								SI	Permanente	Familiar	6 AÑOS	Soltero(a)		Personalmente	CON EL MISMO				\N	\N	\N	\N	\N	\N	\N	67557200	VERIFICACION CORRECTA	CASAS DE RUTH 			\N	2020-6-29 11:40:53.598	\N	\N	\N
1	2	\N	HUANCAYO	44974563	Alejada	Casa		Urbana	Noble	Bueno	Buena	Luz, Agua y Desague		2		SECUNDARIA	Independiente	JR TUNALES N°118	9999999 		2								SI	Permanente	Alquilado	1 AÑO 	Soltero(a)		Personalmente	Personalmente				\N	\N	\N	\N	\N	\N	\N	67421561	QUE SI REALIZA U ACTIVIDADES CON FRECUENCIA	RESERVOIRO DE SEDAM 			2	29/06/2020 12:25:49	29/06/2020 12:25:49	\N	\N
2	2	\N	HUANCAYO	23701995	Centrico	Casa		Comercial	Noble	Bueno	Buena	Luz, Agua y Desague		1		SECUNDARIA	Independiente	JR CUSCO N°1985	923360616 		4								SI	Permanente	Familiar				Personalmente	ELLA MISMA				\N	\N	\N	\N	\N	\N	\N	67168060	VIVIENDA FAMILIAR	CUSCO CATALINA HUANCA MISMA ESQUINA			\N	2020-6-29 13:3:50.301	\N	\N	\N
3	7	\N	HUANCAYO	20113864	Aledaña	Casa		Urbana	Noble	Aceptable	Regular	Luz, Agua y Desague		1		SECUNDARIA	Independiente	PJ.VILCAHUAMAN N°169	969658443 		2											 			Personalmente	Personalmente				\N	\N	\N	\N	\N	\N	\N	67948286	QUE SI SE ENCONTRABA EN SU DOMICILIO	A ESPLADAS DEL CUARTEL			7	29/06/2020 14:31:59	29/06/2020 14:31:59	\N	\N
4	7	\N	HUANCAYO	76621787	Alejada	Casa		Urbana	Noble	Muy bueno	Buena	Luz, Agua y Desague		1		SECUNDARIA	Independiente	JR MALECON	976480066 		2								NO	Permanente	Familiar	NINGUNO	Soltero(a)		Personalmente	ELLA MISMA 				\N	\N	\N	\N	\N	\N	\N	78329310	SE ENCONTRABA EN EL DOMICILIO	FRENTE AL LOCAL MIL MARAVILLAS 			\N	2020-6-29 14:58:16.163	\N	\N	\N
5	7	\N	HUANCAYO	71872891	Alejada	Casa		Urbana	Noble	Muy bueno	Buena	Luz, Agua y Desague		0		SUPERIOR	Dependiente	JR PEDRO PERALTA N°563 	999999999 		3								NO	Permanente	Familiar	NINGUNO	Soltero(a)		Personalmente	ELLA MISMA 				\N	\N	\N	\N	\N	\N	\N	1052153	ESTA PRESENTE EN SU DOMICILIO	ENTRE REAL Y PEDRO PERALTA 			\N	2020-6-29 15:23:5.579	\N	\N	\N
6	7	\N	HUANCAYO	19999596	Alejada	Casa		Rural	Noble	Aceptable	Regular	Solo Luz		0		SECUNDARIA	Dependiente	BARRIOS CHACLAS 	999999999 		1								NO	Permanente	Propia	1 AÑO 	Soltero(a)		Personalmente					\N	\N	\N	\N	\N	\N	\N	5912456	EN EL DOMICILIO SE ENCONTRABA ELLA MISMA 	PUENTES CHACLAS 			\N	2020-6-29 15:38:18.811	\N	\N	\N
7	7	\N	HUANCAYO	46357683	Centrico	Casa		Urbana	Noble	Muy bueno	Buena	Solo Luz		1		SECUNDARIA	Independiente	JR CUSCO N°164	940544290 		3								NO	Permanente	Familiar	1 AÑO	Soltero(a)		Por telefono					\N	\N	\N	\N	\N	\N	\N	0000000	SE ENCONTRABA EN SU DOMICILIO	LA FLORIDA			\N	2020-6-29 18:58:52.304	\N	\N	\N
8	3	\N	HUANCAYO	80401963	Centrico	Casa		Urbana	Noble	Aceptable	Buena	Luz, Agua y Desague		1		SECUNDARIA	Independiente	AV. 9  DE DICIEMBRE N°678	932325773 		2								SI	Permanente	Familiar	10 AÑOS	Soltero(a)		Personalmente	MENDOZA DE GONZALES JACINTA		MAMÁ		\N	\N	\N	\N	\N	\N	\N	1063826	EL SEÑOR TRABAJA CON MICRO Y TIENE UNA MECANICA	AV. PANAMERICANA			\N	2020-6-29 16:22:21.170	\N	\N	\N
9	7	\N	HUANCAYO	47475777	Centrico	Casa		Urbana	Noble	Muy bueno	Buena	Luz, Agua y Desague		0		SECUNDARIA	Independiente	JR LOS CLAVELES N°365	970191859 		4								NO	Permanente	Familiar	5 AÑOS 	Soltero(a)		Personalmente					\N	\N	\N	\N	\N	\N	\N	67772280	SE ENCONTRABA EN EL DOMICILIO 	LORETO Y CLAVELES			\N	2020-6-29 19:28:31.397	\N	\N	\N
10	5	\N	HUANCAYO	23701995	Alejada	Casa		Urbana	Noble	Muy bueno	Buena	Luz, Agua y Desague		0		SECUNDARIA	Independiente	JR AMAZONAS N° 0380	923360616 		2								NO	Permanente	Familiar	7 AÑOS 	Soltero(a)		Personalmente					\N	\N	\N	\N	\N	\N	\N	67557200	SE ENCONTRABA ELLA MISMA EN LA VIVIENDA 	A DOS CUADRAS DE LA CARRETERA CENTRAL			\N	2020-6-30 13:34:28.903	\N	\N	\N
11	5	\N	HUANCAYO	76290455	Alejada	Casa		Urbana	Noble	Muy bueno	Buena	Luz, Agua y Desague		1		SECUNDARIA	Independiente	PILAR PAUCARCHUCO S/N	990120152 		2								NO	Permanente	Familiar	3 AÑOS	Soltero(a)		Personalmente					\N	\N	\N	\N	\N	\N	\N	67875410	SE ENCONTRABA ELLA MISMA EN EL DOMICILIO 	POR EL MERCADO DE AURAY 			\N	2020-6-30 15:18:38.436	\N	\N	\N
12	3	\N	HUANCAYO	40129120	Alejada	Casa		Rural	Noble	Aceptable	Regular	Luz, Agua y Desague		5		SECUNDARIA		JR. ODONOVAN N°416	966562400 		1								SI	Permanente	Familiar	5 AÑOS	Soltero(a)		Por telefono					\N	\N	\N	\N	\N	\N	\N	00000000000	EL SEÑOR ES PUNTUAL CON SUS CUOTAS	A UNA CUADRA DE LA PLAZA 			\N	2020-6-30 12:48:25.676	\N	\N	\N
13	5	\N	HUANCAYO	42297478	Centrico	Casa		Urbana	Rústico	Muy bueno	Buena	Luz, Agua y Desague		1		SECUNDARIA	Independiente	JR SANTOS CHOCANO N°496	999336147 		2								NO	Permanente	Familiar	2 AÑOS	Soltero(a)		Personalmente					\N	\N	\N	\N	\N	\N	\N	67474823	SE ENCONTRABA EN EL DOMICILIO	CENTRO COMERCIAL METRO 			\N	2020-6-30 15:57:22.71	\N	\N	\N
14	5	\N	HUANCAYO	19940220	Alejada	Casa		Urbana	Rústico	Muy bueno	Buena	Solo Luz		0		SECUNDARIA	Independiente	JR JOSE OLAYA S/N	958660511 		1								NO	Permanente	Familiar	5 AÑOS	Soltero(a)		Personalmente					\N	\N	\N	\N	\N	\N	\N	67931574	ELLA SE ENCONTRABA EN LA VIVIENDA 	A DOS CUADRAS DEL PARQUE DE HUANCAN 			\N	2020-6-30 16:26:20.755	\N	\N	\N
15	5	\N	HUANCAYO	16134843	Alejada	Casa		Urbana	Noble	Muy bueno	Buena	Solo Agua y Desague		2		SECUNDARIA		JR JUNIN S/N 	902516506 		1								SI	Permanente	Familiar	3 AÑOS	Soltero(a)		Personalmente					\N	\N	\N	\N	\N	\N	\N	676738369	SE ENCONTRABA EN LA VIVIENDA 	PARQUE HUANCAN 			\N	2020-6-30 17:1:10.766	\N	\N	\N
16	3	\N	HUANCAYO	60064606	Centrico	Casa		Urbana	Noble	Aceptable	Regular	Luz, Agua y Desague		1		SECUNDARIA	Independiente	jr. mariscal castillaSN	967504512 		1								SI	Ocasional	Familiar	4 AÑOS	Soltero(a)		Por telefono					\N	\N	\N	\N	\N	\N	\N	666666661	LA SEÑORA NO PARA EN SU NEGOCIO	AV. HUANCAVELICA			\N	2020-6-30 14:12:14.326	\N	\N	\N
17	5	\N	HUANCAYO	41123341	Centrico	Casa		Urbana	Noble	Muy bueno	Buena	Luz, Agua y Desague		0		SECUNDARIA	Independiente	AV HUANCAVELICA Y LORETO 	959436411 		2								SI	Permanente	Familiar	5 AÑOS 	Soltero(a)		Personalmente					\N	\N	\N	\N	\N	\N	\N	00000000	ELLA SE ENCONTRAVA EN LA VIVIENDA 	AL FRENTE DE LA BOTICA 			\N	2020-6-30 17:27:9.526	\N	\N	\N
18	5	\N	HUANCAYO	44279232	Alejada	Casa		Urbana	Noble	Muy bueno	Buena	Luz, Agua y Desague		1		SECUNDARIA	Independiente	AV LA ESPERANZA N°1024	961616212 		3								SI	Permanente	Familiar	5AÑOS	Soltero(a)		Personalmente					\N	\N	\N	\N	\N	\N	\N	67513799	SE ENCONTRABA ERN LA VIVIENDA 	LA ULTIMA BODEGA 			\N	2020-6-30 17:47:46.166	\N	\N	\N
19	3	\N	HUANCAYO	45745478	Alejada	Casa		Urbana	Noble	Aceptable	Buena	Luz, Agua y Desague		1		SECUNDARIA	Independiente	MIGUEL GRAU SN	914488260 		1								SI	Permanente	Familiar	10 AÑOS	Soltero(a)		Por telefono					\N	\N	\N	\N	\N	\N	\N	78851239	VENTA DE JUGOS Y ABARROTES EN EL MERCADO	JUGUERIA			\N	2020-6-30 15:11:37.838	\N	\N	\N
20	5	\N	HUANCAYO	73054005	Centrico	Casa		Urbana	Noble	Bueno	Buena	Solo Luz		1		SECUNDARIA	Independiente	AV HUANCAVELICA N°1340	918768896 		2								SI	Permanente	Familiar	1 AÑO	Soltero(a)		Personalmente					\N	\N	\N	\N	\N	\N	\N	67255460	SE ENCONTRABA EN LA VIVIENDA	AV HUANCAVELICA 			\N	2020-6-30 18:16:20.130	\N	\N	\N
21	3	\N	HUANCAYO	42831861	Alejada	Casa		Rural	Rústico	Deteriorado	Mala	Luz, Agua y Desague		4		SECUNDARIA	Independiente	PSJ SANTA ROSA SN	918161994 		2								NO						Por telefono					\N	\N	\N	\N	\N	\N	\N	77893913	CLIENTE MOROSA	LAS LOMAS			\N	2020-6-30 15:37:27.93	\N	\N	\N
22	5	\N	HUANCAYO	20064201	Alejada	Casa		Urbana	Noble	Muy bueno	Buena	Luz, Agua y Desague		2		SECUNDARIA	Independiente	JR JUNIN S/N POR VENIR 	930623448 		2								SI	Permanente	Familiar	1 AÑO	Soltero(a)		Personalmente					\N	\N	\N	\N	\N	\N	\N	00000000	SE ENCONTRABA EN LA VIVIENDA 	HUANCAN 			\N	2020-6-30 18:39:29.565	\N	\N	\N
23	3	\N	HUANCAYO	20099395	Centrico	Casa		Comercial	Rústico	Aceptable	Regular	Luz, Agua y Desague		2		SECUNDARIA	Independiente	JR. ANCASH N°324	955556687 		2								NO						Por telefono					\N	\N	\N	\N	\N	\N	\N	6666665	EL SEÑOR SE ENCUENTRA PUESTO A PAGAR	AL COSTADO DE LA MECANICA			\N	2020-6-30 15:54:38.740	\N	\N	\N
24	5	\N	HUANCAYO	80145065	Alejada	Casa		Urbana	Noble	Muy bueno	Buena	Luz, Agua y Desague		0		SECUNDARIA	Dependiente	PJ VILCAHUAMAN N°170	9677778275 		2								SI	Permanente	Familiar	6 AÑOS	Soltero(a)		Personalmente					\N	\N	\N	\N	\N	\N	\N	67282252	ELLA SE ENCONTRABA EN LA VIVIENDA 	DE3TRAS DEL CUARTES DE CHILCA 			\N	2020-6-30 19:3:26.437	\N	\N	\N
25	3	\N	HUANCAYO	41773365	Centrico	Casa		Urbana	Rústico	Bueno	Buena	Luz, Agua y Desague		2		SECUNDARIA	Independiente	JR. ALFONSO UGARTE N°212	981816282 		2								NO						Por telefono					\N	\N	\N	\N	\N	\N	\N	666662	CLIENTE PERMANECE CONSTANTEMENTE EN SU TRABAJO				\N	2020-6-30 16:10:6.221	\N	\N	\N
26	11	\N	PUCALLPA	00098119	Aledaña	Casa		Urbana	Noble	Aceptable	Buena	Luz, Agua y Desague		4		SECUNDARIA	Independiente	AA.HH MASISEA - AV. JHON F KENEDY N°730	995281103 		1								SI	Permanente	Familiar	2 AÑOS			Personalmente	GILBER		HIJO		\N	\N	\N	\N	\N	\N	\N	289320-00	BUENO MI MAMA SU VTA A BAJADO UN POCO PERO SI TIENE INGRESOS DIARIOS PARA PAGAR.	POR EL MERCADO DE LOS CUNCHIS			\N	2020-7-1 9:28:9.120	\N	\N	\N
27	5	\N	HUANCAYO	20898988	Alejada	Casa		Urbana	Noble	Muy bueno	Buena	Luz, Agua y Desague		0		SECUNDARIA	Independiente	AV LOS PROCERES N° 394	974412863 		2								SI	Permanente	Familiar	1 AÑO	Soltero(a)		Personalmente					\N	\N	\N	\N	\N	\N	\N	1045969	ELLA MISMA SE ENCONTRABA EN LA VIVIENDA 	A.V LA REAL 			\N	2020-7-1 13:26:29.420	\N	\N	\N
28	5	\N	HUANCAYO	41274111	Alejada	Casa		Urbana	Noble	Muy bueno	Buena	Luz, Agua y Desague		2		SECUNDARIA	Independiente	AV HEROES DE LA BREÑA N°266	935891719 		2								SI	Permanente	Familiar	1 AÑO	Soltero(a)		Personalmente					\N	\N	\N	\N	\N	\N	\N	00000000	SE ENCONTRABA EN EL LA VIVIENDA 	PASANDO EL PUENTE CHANCHAS 			\N	2020-7-1 14:1:49.503	\N	\N	\N
29	3	\N	HUANCAYO	21276617	Alejada	Casa		Urbana	Rústico	Aceptable	Regular	Luz, Agua y Desague		6		SECUNDARIA	Dependiente	AV.FERROCARRIL N°5157	986379970 		2								NO	Ocasional	Familiar				Por telefono					\N	\N	\N	\N	\N	\N	\N	67730656|	cliente activo	EL TAMBO			\N	2020-7-1 11:19:56.343	\N	\N	\N
30	5	\N	HUANCAYO	44326038	Alejada	Casa		Urbana	Noble	Muy bueno	Buena	Luz, Agua y Desague		2		SECUNDARIA	Independiente	PJ TUPAC AMARU  N°220	972586290 		2								SI	Permanente	Familiar	1 AÑO 	Soltero(a)		Personalmente					\N	\N	\N	\N	\N	\N	\N	67360466	SE ENCONTRABA EN LA VIVIENDA 	AV 12 DE OCTUBRE 			\N	2020-7-1 14:44:4.160	\N	\N	\N
31	11	\N	PUCALLPA	00061656	Centrico	Casa		Urbana	Noble	Bueno	Buena	Luz, Agua y Desague		4		SUPERIOR	Independiente	PSJ.GENERAL MONTERO N° 128	985900883 		1								SI	Permanente	Familiar	22 AÑOS	Soltero(a)	SI	Personalmente	KEWIN GILBER VELA PEREZ		HIJO		\N	\N	\N	\N	\N	\N	\N	896818	QUE ES MUY BUENO PAGANDO Y ES PUNTUAL	ENTRE SUCRE Y SAN MARTIN ULTIMA CUDRA			\N	2020-7-1 19:9:9.574	\N	\N	\N
32	11	\N	PUCALLPA	00061656	Centrico	Casa		Urbana	Noble	Bueno	Buena	Luz, Agua y Desague		4		SUPERIOR	Independiente	PSJ.GENERAL MONTERO N° 128	985900883 		1								SI	Permanente	Familiar	22 AÑOS	Soltero(a)	SI	Personalmente	KEWIN GILBER VELA PEREZ		HIJO		\N	\N	\N	\N	\N	\N	\N	896818	QUE ES MUY BUENO PAGANDO Y ES PUNTUAL	ENTRE SUCRE Y SAN MARTIN ULTIMA CUDRA			\N	2020-7-1 19:9:42.12	\N	\N	\N
33	11	\N	PUCALLPA	71340438	Aledaña	Casa		Rural	Noble	Aceptable	Regular	Ninguno		1		SUPERIOR	Independiente	JR. ATAHUALPA N°178	9711720169 		1								SI	Permanente	Familiar	18 AÑOS	Soltero(a)	SI	Personalmente	RIOS PEREZ RAY NILK		-		\N	\N	\N	\N	\N	\N	\N	339377	TRABAJA EN VENTAS DE MADERAS 	1.ra , CUADEA DE JR. ATAHUALPA			\N	2020-7-1 19:22:45.517	\N	\N	\N
34	3	\N	HUANCAYO	46485289	Alejada	Casa		Rural	Noble	Aceptable	Regular	Luz, Agua y Desague		5		SECUNDARIA	Independiente	JR. PORVENIR PICHAS	981782923 		1								NO						Por telefono					\N	\N	\N	\N	\N	\N	\N	66476260	LA SEÑORA RECIBE PENSION DE SUS MENORES HIJOS	CASA BLANCA			\N	2020-7-2 9:7:55.535	\N	\N	\N
35	3	\N	HUANCAYO	40893286	Alejada	Casa		Rural	Noble	Aceptable	Regular	Luz, Agua y Desague		2		SECUNDARIA	Independiente	CALLE HUANCAYO SN	943170739 		1								NO						Por telefono					\N	\N	\N	\N	\N	\N	\N	666667	EL CLIENTE SE SUSTENTA SOLO DE SU NEGOCIO	HUANCAN			\N	2020-7-2 9:45:35.218	\N	\N	\N
36	3	\N	HUANCAYO	19942242	Alejada	Casa		Rural	Rústico	Aceptable	Buena	Luz, Agua y Desague		0		SECUNDARIA	Independiente	JR. JUNIN SN	972803121 		3														Por telefono					\N	\N	\N	\N	\N	\N	\N	66474140	cliente activo	loza deportiva de huancan			\N	2020-7-2 11:23:22.651	\N	\N	\N
37	3	\N	HUANCAYO	19944388	Alejada	Casa		Rural	Rústico	Aceptable	Regular	Luz, Agua y Desague		1		SECUNDARIA	Independiente	JR.JUNIN SN	952415835 		1								NO						Por telefono					\N	\N	\N	\N	\N	\N	\N	66474293	CLIENTE ACTIVO	ANTES DEL PARADERO DE LOS ROJOS			\N	2020-7-2 12:32:13.733	\N	\N	\N
38	5	\N	HUANCAYO	77161207	Alejada	Casa		Urbana	Noble	Muy bueno	Buena	Luz, Agua y Desague		0		SECUNDARIA	Independiente	JR PORVENIR PICHAS 	940520842 		2								SI	Permanente	Familiar	5 AÑOS	Soltero(a)		Personalmente					\N	\N	\N	\N	\N	\N	\N	66474186	SE ENCONTRABA EN LA VIVIENDA 	JR GRAU 			\N	2020-7-3 13:18:55.530	\N	\N	\N
39	5	\N	HUANCAYO	45666252	Alejada	Casa		Urbana	Noble	Muy bueno	Buena	Luz, Agua y Desague		0		SECUNDARIA	Independiente	jr cahuide n°168	955118865 		2								SI	Permanente	Familiar	1 año	Soltero(a)		Personalmente					\N	\N	\N	\N	\N	\N	\N	67446758	se encontraba en la vivienda 	a.v cahuide 			\N	2020-7-3 14:52:3.64	\N	\N	\N
40	5	\N	HUANCAYO	41278746	Alejada	Casa		Urbana	Noble	Muy bueno	Buena	Luz, Agua y Desague		1		SECUNDARIA	Independiente	jr junin s/n	957603409 		2								SI	Permanente	Familiar	1 año	Soltero(a)		Personalmente					\N	\N	\N	\N	\N	\N	\N	66470848	se encon traba en la vivienda 	huancan			\N	2020-7-3 15:10:58.45	\N	\N	\N
41	5	\N	HUANCAYO	99907227	Alejada	Casa		Urbana	Noble	Muy bueno	Buena	Luz, Agua y Desague		0		SECUNDARIA	Independiente	PJS TUPAC AMARU N°220	064582145 		2								SI	Permanente	Familiar	5 AÑOS	Soltero(a)		Personalmente					\N	\N	\N	\N	\N	\N	\N	67360466	SE ENCONTRABA EN LA VIVIENDA 	CULLPA BAJA 			\N	2020-7-3 15:44:18.457	\N	\N	\N
42	5	\N	HUANCAYO	80535329	Alejada	Casa		Urbana	Noble	Muy bueno	Buena	Luz, Agua y Desague		0		SECUNDARIA	Independiente	JR JUNIN 	948761424 		2								SI	Permanente	Familiar	1 AÑO	Soltero(a)		Personalmente					\N	\N	\N	\N	\N	\N	\N	76304297	SE ENCONTRABA EN LA VIVIENDA 	BARRIO PORVENIR HUANCAN			\N	2020-7-3 16:33:17.879	\N	\N	\N
43	2	\N	HUANCAYO	20065148	Aledaña	Casa		Urbana	Noble	Bueno	Buena	Luz, Agua y Desague		2		SECUNDARIA	Independiente	JR HUMBOLT 1373 - LA VICTORIA	987322778 		2								SI	Permanente	Propia	15 AÑOS	Soltero(a)	NO	Personalmente	EL MISMO		-		\N	\N	\N	\N	\N	\N	\N	66488473	TODO LA CONFECCION SU VTA ES SEMANAL Y COBRO QUINCENAL 	DEL PARQUE PRINCIAAL HACIA ABAJO 2 CDRS			\N	2020-7-3 13:40:26.100	\N	\N	\N
44	2	\N	HUANCAYO	47846248	Aledaña	Casa		Urbana	Noble	Bueno	Buena	Luz, Agua y Desague		0		SUPERIOR	Dependiente	AV. UNIVERSITARIA S/N	979029419 		3														Personalmente					\N	\N	\N	\N	\N	\N	\N	6879563	TRABAJO NOMBRADO EN LA UGEL HYO	DE LA UNCP HACIA ARRIBA 4 CRDS			\N	2020-7-3 13:51:16.544	\N	\N	\N
45	5	\N	HUANCAYO	40144277	Alejada	Casa		Urbana	Noble	Muy bueno	Buena	Luz, Agua y Desague		0		SECUNDARIA	Independiente	JR JUNIN - BARRIO POR VENIR 	935429663 		2								SI	Permanente	Familiar	1 AÑO	Soltero(a)		Personalmente					\N	\N	\N	\N	\N	\N	\N	76304297	SE ENCONTRABA EN LA VIVIENDA 	HUANCAN 			\N	2020-7-3 17:1:12.923	\N	\N	\N
46	2	\N	HUANCAYO	48024565	Alejada	Casa		Urbana	Noble	Bueno	Buena	Luz, Agua y Desague		0		SUPERIOR	Dependiente	PJS TUMI Nª123	 		2								SI	Ocasional	Familiar	17 AÑOS	Soltero(a)		Personalmente			ELLA MISMO		\N	\N	\N	\N	\N	\N	\N	76243549	TRABAJA DE LUNES A VIERNES 8:30 - 2:30	POR RIVAGUERO Y 9 DIC.			\N	2020-7-3 14:23:42.551	\N	\N	\N
47	3	\N	HUANCAYO	46196324	Alejada	Casa		Rural	Noble	Aceptable	Regular	Solo Luz		2		SECUNDARIA	Independiente	JR. GENERAL GAMARRA N°CDRA 2	PSJ. CHALCO N°130 		1								NO						Por telefono					\N	\N	\N	\N	\N	\N	\N	77171909	EL CLIENTE PEEMANECE EN SU NEGOCIO	PASAJE LLENERA			\N	2020-7-4 9:36:40.287	\N	\N	\N
48	3	\N	HUANCAYO	20999895	Centrico	Casa		Urbana	Rústico	Muy bueno	Regular	Luz, Agua y Desague		1		SECUNDARIA	Independiente	PSJ. VILCAHUAMAN SN	981813300 		2								SI	Ocasional	Familiar	4 AÑOS	Soltero(a)		Personalmente	VILCAHUAMAN MEDINA ALEJANDRO HECTOR		PAREJA		\N	\N	\N	\N	\N	\N	\N	78544354	CLIENTE ACTIVO	A ESPALDAS DEL RESTAURANT EL COMPADRE			\N	2020-7-4 10:17:31.602	\N	\N	\N
49	3	\N	HUANCAYO	42388030	Alejada	Casa		Urbana	Rústico	Aceptable	Regular	Luz, Agua y Desague		0		SECUNDARIA	Independiente	PANAMERICANA SUR. PARADERO 3 SN	900765419 		2								SI	Permanente	Familiar		Soltero(a)		Personalmente					\N	\N	\N	\N	\N	\N	\N	67700236	CLIENTE ACTIVO	BARRIO ALATA			\N	2020-7-4 10:51:55.627	\N	\N	\N
50	3	\N	HUANCAYO	42388030	Alejada	Casa		Urbana	Rústico	Aceptable	Regular	Luz, Agua y Desague		0		SECUNDARIA	Independiente	PANAMERICANA SUR. PARADERO 3 SN	900765419 		2								SI	Permanente	Familiar		Soltero(a)		Personalmente					\N	\N	\N	\N	\N	\N	\N	67700236	CLIENTE ACTIVO	BARRIO ALATA			\N	2020-7-4 10:52:11.618	\N	\N	\N
51	3	\N	HUANCAYO	19946742	Alejada	Casa		Rural	Rústico	Muy bueno	Regular	Luz, Agua y Desague		0		SECUNDARIA	Independiente	AV. DANIEL A. CARRION	971403629 		2								NO						Por telefono					\N	\N	\N	\N	\N	\N	\N	66480236	CLIENTE ACTIVO	PLAZA DE HUAMANMARCA			\N	2020-7-4 11:18:20.162	\N	\N	\N
52	5	\N	HUANCAYO	19870079	Alejada	Casa		Urbana	Noble	Muy bueno	Buena	Luz, Agua y Desague		2		SECUNDARIA	Independiente	AV PANAMERICA SUR 	920654319 		2								SI	Permanente	Familiar	1 AÑO	Soltero(a)		Personalmente					\N	\N	\N	\N	\N	\N	\N	66476430	SE ENCONTRABA EN LA VIVIENDA 	UNIDAD VECINAL HUANCAN 			\N	2020-7-4 15:34:35.302	\N	\N	\N
53	5	\N	HUANCAYO	19996893	Alejada	Casa		Urbana	Noble	Muy bueno	Buena	Luz, Agua y Desague		0		SECUNDARIA	Independiente	LEONCIO PRADO N° 1015	972079273 		2								SI	Permanente	Familiar	5 AÑOS 	Soltero(a)		Personalmente					\N	\N	\N	\N	\N	\N	\N	67477001	SE ENCONTRABA EN LA VIVIENDA 	ENTRE JACINTO IBARRA 			\N	2020-7-6 12:49:9.831	\N	\N	\N
54	5	\N	HUANCAYO	19996893	Alejada	Casa		Urbana	Noble	Muy bueno	Buena	Luz, Agua y Desague		0		SECUNDARIA	Independiente	LEONCIO PRADO N° 1015	972079273 		2								SI	Permanente	Familiar	5 AÑOS 	Soltero(a)		Personalmente					\N	\N	\N	\N	\N	\N	\N	67477001	SE ENCONTRABA EN LA VIVIENDA 	ENTRE JACINTO IBARRA 			\N	2020-7-6 12:49:19.852	\N	\N	\N
55	5	\N	HUANCAYO	45879388	Alejada	Casa		Urbana	Noble	Muy bueno	Buena	Luz, Agua y Desague		0		SECUNDARIA	Independiente	JR JUNIN N°148	954429172 		2								SI	Permanente	Familiar	1 AÑO	Soltero(a)		Personalmente					\N	\N	\N	\N	\N	\N	\N	77121934	SE ENCONTRABA EN LA VIVIENDA 	BARRIO POR VENIR 			\N	2020-7-6 13:36:44.34	\N	\N	\N
56	5	\N	HUANCAYO	70437871	Alejada	Casa		Urbana	Noble	Muy bueno	Buena	Luz, Agua y Desague		0		SECUNDARIA	Independiente	JR GRAU N°575	944908077 		1								SI	Permanente	Familiar	1 AÑO	Soltero(a)		Personalmente					\N	\N	\N	\N	\N	\N	\N	75045744	SE ENCONTRABA EN LA VIVIENDA 	EL TAMBO			\N	2020-7-6 14:2:33.992	\N	\N	\N
57	5	\N	HUANCAYO	44729185	Alejada	Casa		Urbana	Noble	Muy bueno	Buena	Luz, Agua y Desague		0		SUPERIOR	Dependiente	PJ AUGUSTO PEÑALOSA N°173	936751390 		1								SI	Permanente	Familiar	5 AÑOS 	Soltero(a)		Personalmente					\N	\N	\N	\N	\N	\N	\N	250874	S ENCONTRABA EN LA VIVIENDA 	PEÑALOSA			\N	2020-7-6 15:25:48.689	\N	\N	\N
58	5	\N	HUANCAYO	20047216	Alejada	Casa		Urbana	Noble	Muy bueno	Buena	Luz, Agua y Desague		0		SECUNDARIA	Independiente	AV REAL N°1310	064764091 		2								SI	Permanente	Familiar	1 AÑO	Soltero(a)		Personalmente					\N	\N	\N	\N	\N	\N	\N	66435197	SE ENCONTRABA EN LA VIVIENDA 	AZAPAMPA			\N	2020-7-6 16:26:23.501	\N	\N	\N
59	2	\N	HUANCAYO	47420055	Alejada	Casa		Urbana	Noble	Bueno	Buena	Luz, Agua y Desague		2		SUPERIOR	Independiente	PROLONGACION PROCERES 00S/. INT. 101 PUEBLO AZAPAMPA	966274622 		1														Personalmente					\N	\N	\N	\N	\N	\N	\N	68051779	DEJA A PERSONAL LABORANDO  	ESQUINA DEL CANAL JOSE OLAYA Y TAHUANTINSUYO			\N	2020-7-6 13:56:50.82	\N	\N	\N
60	2	\N	HUANCAYO	47420055	Alejada	Casa		Urbana	Noble	Bueno	Buena	Luz, Agua y Desague		2		SUPERIOR	Independiente	PROLONGACION PROCERES 00S/. INT. 101 PUEBLO AZAPAMPA	966274622 		1								SI	Permanente	Familiar	2 AÑOS			Personalmente					\N	\N	\N	\N	\N	\N	\N	68051779	DEJA A PERSONAL LABORANDO  	ESQUINA DEL CANAL JOSE OLAYA Y TAHUANTINSUYO			\N	2020-7-6 13:58:36.15	\N	\N	\N
61	5	\N	HUANCAYO	70020689	Alejada	Casa		Urbana	Noble	Muy bueno	Buena	Luz, Agua y Desague		1		SUPERIOR	Independiente	JR MICAELA BASTIDAS N°132	978112425 		2								SI	Permanente	Familiar	1 AÑO	Soltero(a)		Personalmente					\N	\N	\N	\N	\N	\N	\N	752224941	SE ENCONTRABA EN LA VIVIENDA 	INTERNET DAD"			\N	2020-7-6 16:59:36.69	\N	\N	\N
62	5	\N	HUANCAYO	80245301	Alejada	Casa		Urbana	Noble	Muy bueno	Buena	Luz, Agua y Desague		2		SECUNDARIA	Independiente	AV MARIA ELENA MOYA 	984766228 		3								SI	Permanente	Familiar	5 AÑOS 	Soltero(a)		Personalmente					\N	\N	\N	\N	\N	\N	\N	67954720	SE ENCONTRABA EN LA VIVIENDA 	FRENTE AL MERCADO MICAELA BASTIDAS 			\N	2020-7-7 14:2:18.460	\N	\N	\N
63	5	\N	HUANCAYO	44350264	Alejada	Casa		Urbana	Noble	Muy bueno	Buena	Luz, Agua y Desague		0		SECUNDARIA	Independiente	JR ANTONIO ZELA N° 560	99999999 		2								SI	Permanente	Familiar	1 AÑO	Soltero(a)		Personalmente					\N	\N	\N	\N	\N	\N	\N	00000000	SE ENCONTRABA EN LA VIVIENDA 	ENTRE ZELA Y ANCASH 			\N	2020-7-7 14:27:53.665	\N	\N	\N
64	5	\N	HUANCAYO	19944260	Alejada	Casa		Urbana	Noble	Muy bueno	Buena	Luz, Agua y Desague		0		SECUNDARIA	Independiente	AV ALEXANDER FLEMIX 	931801602 		2								SI	Permanente	Familiar	1 AÑO	Soltero(a)		Personalmente					\N	\N	\N	\N	\N	\N	\N	66582835	SE ENCONTRABA EN LA VIVIENDA 	1 AÑO			\N	2020-7-7 14:47:32.305	\N	\N	\N
65	11	\N	PUCALLPA	05213767	Aledaña	Casa		Urbana	Noble	Aceptable	Regular	Solo Agua y Desague		3		SECUNDARIA	Independiente	AA.HH. NUEVO PARAISO MZ. I LT.18	920718113 		1								SI	Ocasional	Propia	10 AÑOS	Casado(a)	SI	Personalmente	MONROY CAMAYO ROBINSON				\N	\N	\N	\N	\N	\N	\N	514033	EL NEGOCIO ESTA EN BUENAS CONDICIONES 	POR EL PUENTE CAÑO NATURAL			\N	2020-7-7 12:21:49.172	\N	\N	\N
66	5	\N	HUANCAYO	77377731	Alejada	Casa		Urbana	Noble	Muy bueno	Buena	Luz, Agua y Desague		0		SECUNDARIA	Independiente	AV 12 DE OCTUBRE N°1040	980905904 		1								SI	Permanente	Familiar	1 AÑO	Soltero(a)		Personalmente					\N	\N	\N	\N	\N	\N	\N	0000000000	S EENCONTRABA EN LA VIVIENDA 	CULLPA BAJA 			\N	2020-7-7 15:31:16.114	\N	\N	\N
67	5	\N	HUANCAYO	77381267	Alejada	Casa		Urbana	Noble	Bueno	Regular	Luz, Agua y Desague		0		SUPERIOR	Independiente	AV 12 DE OCTUBRE N°1040	00000000 		1								SI	Permanente	Familiar	1 AÑO	Soltero(a)		Personalmente					\N	\N	\N	\N	\N	\N	\N	00000000	SE ENCONTRABA EN LA VIVIENDA 	CULLPA BAJA 			\N	2020-7-7 15:43:36.102	\N	\N	\N
68	9	\N	PUCALLPA	00093054	Aledaña	Casa		Rural	Rústico	Aceptable	Regular	Solo Agua y Desague		2		SECUNDARIA	Independiente	AA.HH. NUEVO PARAISO MZ. I LT. 18	920718113 		1								SI	Permanente	Familiar	8 AÑOS	Casado(a)	SI	Personalmente	CORAL REATEQUI FELISINDA				\N	\N	\N	\N	\N	\N	\N	514033	ES COMPRENCIBLWE Y BUENA PÁGADORA	ESPALDA DE BAR GUITANOS			\N	2020-7-8 22:50:11.282	\N	\N	\N
69	11	\N	PUCALLPA	00035433	Centrico	Casa		Urbana	Rústico	Aceptable	Regular	Solo Luz		2		SECUNDARIA	Independiente	JR. COMANDANTE BARRERA N°595	 		1								SI	Permanente	Propia	25 AÑOS	Soltero(a)	SI	Personalmente	PEREZ PEREZ REYNA				\N	\N	\N	\N	\N	\N	\N	352963	ESTA DISPOCICON  DE PAGAR AL DIA	POR LA 11 DE JULIO PARQUE 			\N	2020-7-8 23:2:7.664	\N	\N	\N
70	11	\N	PUCALLPA	00035433	Centrico	Casa		Urbana	Rústico	Aceptable	Regular	Solo Luz		2		SECUNDARIA	Independiente	JR. COMANDANTE BARRERA N°595	 		1								SI	Permanente	Propia	25 AÑOS	Soltero(a)	SI	Personalmente	PEREZ PEREZ REYNA				\N	\N	\N	\N	\N	\N	\N	352963	ESTA DISPOCICON  DE PAGAR AL DIA	POR LA 11 DE JULIO PARQUE 			\N	2020-7-8 23:2:13.6	\N	\N	\N
71	5	\N	HUANCAYO	42611489	Alejada	Casa		Urbana	Noble	Muy bueno	Buena	Luz, Agua y Desague		0		SECUNDARIA	Independiente	AV INDEPENDENCIA 	999999998 		1								SI	Permanente	Familiar	4 AÑOS	Soltero(a)		Personalmente					\N	\N	\N	\N	\N	\N	\N	68044943	SE ENCONTRABA EN LA VIVIENDA 	HUANCAN 			\N	2020-7-9 9:57:39.320	\N	\N	\N
72	5	\N	HUANCAYO	43568148	Centrico	Casa		Urbana	Noble	Muy bueno	Buena	Luz, Agua y Desague		1		SECUNDARIA	Independiente	JR MILLER N° 120	935534539 		1								SI	Permanente	Familiar	4 AÑOS	Soltero(a)		Personalmente					\N	\N	\N	\N	\N	\N	\N	67273398	SE ENCONTRABA EN LA VIVIENDA 	AV OCOPILLA Y MILLER 			\N	2020-7-9 10:23:24.402	\N	\N	\N
73	5	\N	HUANCAYO	47323767	Alejada	Casa		Urbana	Noble	Muy bueno	Buena	Luz, Agua y Desague		0		SECUNDARIA	Independiente	JR AMAZONAS N°1327	969695292 		1								SI	Permanente	Familiar	1 AÑO	Soltero(a)		Personalmente					\N	\N	\N	\N	\N	\N	\N	68116630	SE ENCONTRABA EN LA VIVIENDA 	1 AÑO			\N	2020-7-9 10:45:41.508	\N	\N	\N
74	5	\N	HUANCAYO	44931763	Centrico	Casa		Urbana	Noble	Muy bueno	Buena	Luz, Agua y Desague		1		SECUNDARIA	Independiente	JIRON 28 DE JULIO N°790	940650654 		3								SI	Permanente	Familiar	4 AÑOS	Soltero(a)		Personalmente					\N	\N	\N	\N	\N	\N	\N	67394277	SE ENCONTRABA EN LA VIVIENDA 	LA CASA DE PADRES 			\N	2020-7-9 11:16:26.979	\N	\N	\N
75	5	\N	HUANCAYO	46657916	Alejada	Casa		Urbana	Noble	Muy bueno	Buena	Luz, Agua y Desague		0		SECUNDARIA	Independiente	PJ SAN SILVESTRE	913972442 		1								SI	Permanente	Familiar	4 AÑOS	Soltero(a)		Personalmente					\N	\N	\N	\N	\N	\N	\N	68000555	SE ENCONTRABA EN LA VIVIENDA	SU VIVIENDA 			\N	2020-7-9 11:38:40.76	\N	\N	\N
76	5	\N	HUANCAYO	48117988	Alejada	Casa		Urbana	Noble	Muy bueno	Buena	Luz, Agua y Desague		0		SECUNDARIA	Independiente	PJ SAN SILVESTRE 	930316299 		1								SI	Permanente	Familiar	5 AÑOS	Soltero(a)		Personalmente					\N	\N	\N	\N	\N	\N	\N	68000555	SE ENCONTRABA EN LA VIVIENDA 	1 AÑO			\N	2020-7-9 11:55:13.628	\N	\N	\N
77	5	\N	HUANCAYO	44019045	Alejada	Casa		Urbana	Noble	Muy bueno	Buena	Luz, Agua y Desague		1		SECUNDARIA	Independiente	PS SAN SILVESTRE 	978068499 		1								SI	Permanente	Familiar	4 AÑOS	Soltero(a)		Personalmente					\N	\N	\N	\N	\N	\N	\N	68000555	SE ENCONTRABA EN LA VIVIENDA 	1 AÑO 			\N	2020-7-9 12:14:11.629	\N	\N	\N
78	5	\N	HUANCAYO	46706530	Alejada	Casa		Urbana	Noble	Muy bueno	Buena	Luz, Agua y Desague		0		SECUNDARIA	Independiente	CALLE GLADIOLOS N°490	933731557 		1								SI	Permanente	Familiar	4 AÑOS	Soltero(a)		Personalmente					\N	\N	\N	\N	\N	\N	\N	78156845	SE ENCONTRABA EN LA VIVIENDA 	1 AÑO			\N	2020-7-9 12:43:38.13	\N	\N	\N
79	5	\N	HUANCAYO	48522972	Alejada	Casa		Urbana	Noble	Muy bueno	Buena	Luz, Agua y Desague		0		SECUNDARIA	Independiente	JR 7 DE OCTUBRE 	954679188 		1								SI	Permanente	Familiar	4 AÑOS	Soltero(a)		Personalmente					\N	\N	\N	\N	\N	\N	\N	67699082	SE ENCONTRABA EN SU VIVIENDA 	1 AÑO			\N	2020-7-9 13:17:49.628	\N	\N	\N
80	5	\N	HUANCAYO	80401963	Centrico	Casa		Urbana	Noble	Muy bueno	Buena	Luz, Agua y Desague		1		SECUNDARIA	Independiente	AV 9 DE DICIEMBRE Nº1277	932325773 		1								SI	Permanente	Familiar	4 AÑOS	Soltero(a)		Personalmente					\N	\N	\N	\N	\N	\N	\N	1063829	SE ENCONTRABA EN LA VIVIENDA 	CUARTEL DE CHILCA 			\N	2020-7-9 13:39:4.733	\N	\N	\N
81	5	\N	HUANCAYO	42781214	Alejada	Casa		Urbana	Noble	Muy bueno	Buena	Luz, Agua y Desague		0		SECUNDARIA	Independiente	PJ MUCHAS N° LT-5	915976043 		1								SI	Permanente	Familiar	4 AÑOS	Soltero(a)		Personalmente					\N	\N	\N	\N	\N	\N	\N	77053146	SE ENCONTRABA EN LA VIVIVENDA 	AZAPAMPA			\N	2020-7-10 9:6:24.467	\N	\N	\N
82	5	\N	HUANCAYO	47858782	Alejada	Casa		Urbana	Noble	Muy bueno	Buena	Luz, Agua y Desague		1		SECUNDARIA	Independiente	JR ALEXANDER FLEMING	929287025 		2								SI	Permanente	Familiar	4 AÑOS	Soltero(a)		Personalmente					\N	\N	\N	\N	\N	\N	\N	66582835	SE ENCONTRABA EN LA VIVIENDA 	BARRIO POR VENIR 			\N	2020-7-10 14:54:35.305	\N	\N	\N
83	11	\N	PUCALLPA	42148384	Centrico	Casa		Comercial	Noble	Bueno	Buena	Luz, Agua y Desague		3		SECUNDARIA	Independiente	JR. LOS FRUTALES MZ. 358 LT. 2	 		1								SI	Permanente	Alquilado		Soltero(a)		Personalmente	SAJAMI VASQUEZ ERIKA LUCIA				\N	\N	\N	\N	\N	\N	\N	512749	 QUE ESTA DISPUESTA APAGAR SUS CREDITO	POR EL PARADERO DE RUTA DE CAMPO VERDE			\N	2020-7-10 21:7:17.815	\N	\N	\N
84	11	\N	PUCALLPA	42148384	Centrico	Casa		Comercial	Noble	Bueno	Buena	Luz, Agua y Desague		3		SECUNDARIA	Independiente	JR. LOS FRUTALES MZ. 358 LT. 2	 		1								SI	Permanente	Alquilado		Soltero(a)		Personalmente	SAJAMI VASQUEZ ERIKA LUCIA				\N	\N	\N	\N	\N	\N	\N	512749	 QUE ESTA DISPUESTA APAGAR SUS CREDITO	POR EL PARADERO DE RUTA DE CAMPO VERDE			\N	2020-7-10 21:7:31.203	\N	\N	\N
85	5	\N	HUANCAYO	76918572	Centrico	Casa		Urbana	Noble	Muy bueno	Buena	Luz, Agua y Desague		1		SECUNDARIA	Independiente	JR PANAMA N°110	917013626 		1								SI	Permanente	Alquilado	5 AÑO 	Soltero(a)		Personalmente					\N	\N	\N	\N	\N	\N	\N	66512090	SE ENCONTRABA EN LA VIVIENDA 	A ESPALDAS DEL COLISEO HUANCA 			\N	2020-7-11 10:46:51.610	\N	\N	\N
86	5	\N	HUANCAYO	48059190	Alejada	Casa		Urbana	Noble	Bueno	Buena	Luz, Agua y Desague		2		SECUNDARIA	Independiente	JR GENERAL GAMARRA 	916289500 		3								SI	Permanente	Familiar	4 AÑOS	Soltero(a)		Personalmente					\N	\N	\N	\N	\N	\N	\N	67683369	SE ENCONTRABA EN LA VIVIENDA 	ENTRE JACINTO IBARRA 			\N	2020-7-11 12:10:35.708	\N	\N	\N
87	5	\N	HUANCAYO	76663751	Centrico	Casa		Urbana	Noble	Bueno	Buena	Luz, Agua y Desague		0		SUPERIOR	Independiente	PARQUE STOC N°188	927266421 		1								SI	Permanente	Familiar	4 AÑOS	Soltero(a)		Personalmente					\N	\N	\N	\N	\N	\N	\N	0000000	SE ENCONTRABA EN LA VIVIENDA 	SAN CARLOS 			\N	2020-7-11 12:55:15.664	\N	\N	\N
88	5	\N	HUANCAYO	76663751	Centrico	Casa		Urbana	Noble	Bueno	Buena	Luz, Agua y Desague		0		SUPERIOR	Independiente	PARQUE STOC N°188	927266421 		1								SI	Permanente	Familiar	4 AÑOS	Soltero(a)		Personalmente					\N	\N	\N	\N	\N	\N	\N	0000000	SE ENCONTRABA EN LA VIVIENDA 	SAN CARLOS 			\N	2020-7-11 12:55:22.479	\N	\N	\N
89	11	\N	PUCALLPA	00124443	Centrico	Casa		Rural	Rústico	Aceptable	Regular	Luz, Agua y Desague		3		SECUNDARIA	Independiente	JR. STA. TERESA N°359	 		1								SI	Permanente	Propia	20 AÑOS	Soltero(a)	SI	Por telefono					\N	\N	\N	\N	\N	\N	\N	248792	QUE ESTA DISPUESTO APAGRA EL CREDITO PUNTUAL	POR ATRAS DE LA MEYPOL			\N	2020-7-11 18:11:11.585	\N	\N	\N
90	5	\N	HUANCAYO	20682065	Alejada	Casa		Urbana	Noble	Bueno	Buena	Luz, Agua y Desague		0		SECUNDARIA	Independiente	PSJ LOS CLAVELES S/N SECTOR 	999251795 		2								SI	Permanente	Familiar	4 ÑOS 	Soltero(a)		Personalmente					\N	\N	\N	\N	\N	\N	\N	67827191	SE ENCONTRABA EN LA VIVIENDA 	LA ULTIMA BODEGA 			\N	2020-7-13 9:57:28.980	\N	\N	\N
91	5	\N	HUANCAYO	44100810	Aledaña	Casa		Urbana	Rústico	Bueno	Buena	Luz, Agua y Desague		0		SECUNDARIA	Independiente	CALLE CHCICCHICANCHA 	932631215 		1								SI	Permanente	Familiar	4 AÑOS 	Soltero(a)		Personalmente					\N	\N	\N	\N	\N	\N	\N	67355608	SE ENCONTRABA EN LA VIVIENDA 	CULLPA BAJA 			\N	2020-7-13 12:57:42.801	\N	\N	\N
92	2	\N	HUANCAYO	41614930	Alejada	Casa		Comercial	Noble	Bueno	Buena	Luz, Agua y Desague		1		SUPERIOR	Independiente	AV. HUANCAVELICA 620	942845462 		2								SI	Permanente	Alquilado	2 AÑOS	Soltero(a)		Personalmente	MOZO		CONOCIDO		\N	\N	\N	\N	\N	\N	\N	67111200	SE LE ENCONTRO EN SU LABORES	LORETO Y HUANCAVELICA			\N	2020-7-14 9:42:35.120	\N	\N	\N
93	5	\N	HUANCAYO	48177932	Alejada	Casa		Urbana	Noble	Bueno	Buena	Luz, Agua y Desague		0		SECUNDARIA	Independiente	JR ICA N°1	921552616 		1								SI	Permanente	Familiar	4 AÑOS	Soltero(a)		Personalmente					\N	\N	\N	\N	\N	\N	\N	66483050	SE ENCONTRABA EN LA VIVIENDA 	HUAYUCACHI			\N	2020-7-14 11:29:42.119	\N	\N	\N
94	5	\N	HUANCAYO	48142577	Alejada	Casa		Urbana	Noble	Muy bueno	Buena	Luz, Agua y Desague		0		SUPERIOR	Dependiente	SIN DIRRECION 	992535194 		1								SI	Permanente	Familiar	1 AÑO	Soltero(a)		Personalmente					\N	\N	\N	\N	\N	\N	\N	00000000	SE ENCONTRABA EN LA VIVIENDA 	-			\N	2020-7-14 12:38:25.308	\N	\N	\N
95	5	\N	HUANCAYO	19882294	Alejada	Casa		Urbana	Noble	Muy bueno	Buena	Luz, Agua y Desague		1		SECUNDARIA	Independiente	JR SECTOR 3 DE AZAPAMPA 	939538171 		1								SI	Permanente	Familiar	5 AÑOS	Soltero(a)		Personalmente					\N	\N	\N	\N	\N	\N	\N	66436962	SE ENCONTRABA EN LA VIVIENDA 	AZPAMPA 			\N	2020-7-15 10:9:35.587	\N	\N	\N
96	5	\N	HUANCAYO	21000001	Alejada	Casa		Urbana	Noble	Muy bueno	Buena	Luz, Agua y Desague		0		SECUNDARIA	Independiente	PUEBLO JOVEN  JR LA FLORIDA 	981532273 		3								SI	Permanente	Familiar	4 AÑOS 	Soltero(a)		Personalmente					\N	\N	\N	\N	\N	\N	\N	0000000	SE ENCONTRABA EN LA VIVIENDA 	MERCADO LA ESPERANZA 			\N	2020-7-15 10:51:26.14	\N	\N	\N
97	5	\N	HUANCAYO	41738167	Alejada	Casa		Urbana	Noble	Muy bueno	Buena	Luz, Agua y Desague		1		SECUNDARIA	Independiente	PJ ALEGRIA N° S/N PUEBLO SAPALLANGA	966297863 		2								SI	Permanente	Familiar	4 AÑOS	Soltero(a)		Personalmente					\N	\N	\N	\N	\N	\N	\N	76715470	SE ENCONTRABA EN LA VIVIENDA 	PUEBLO SAPALLANGA 			\N	2020-7-15 12:16:11.310	\N	\N	\N
98	11	\N	PUCALLPA	48742387	Alejada	Casa		Rural	Rústico	Aceptable	Regular	Ninguno		2		SECUNDARIA	Independiente	AA.HH. NUEVO JERUSALEN MZ.A LT. 2	 		1								SI	Permanente	Propia	10 AÑOS	Casado(a)	SI	Personalmente					\N	\N	\N	\N	\N	\N	\N	000874	ESTA DISPUESTA A PAGAR SU CREDITO	A ESPALDAS DEL COLEGIO JOSE OLAYA			\N	2020-7-21 10:42:5.917	\N	\N	\N
99	11	\N	PUCALLPA	70203921	Centrico	Casa		Rural	Rústico	Bueno	Buena	Solo Agua y Desague		1		SUPERIOR	Independiente	JR.COMANDNTE BARRERA .	 		1								SI	Permanente	Familiar	2 AÑOS	Soltero(a)	SI	Personalmente					\N	\N	\N	\N	\N	\N	\N	188777	QUE ES BUENA PAGADORA DE CREDITO	11 DE JULIO			\N	2020-7-30 16:16:18.108	\N	\N	\N
\.


--
-- Data for Name: ficha_economica; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.ficha_economica (id_ficha, dni_cliente, ingreso_uno, ingreso_dos, ingreso_tres, carga_familar, gastos_alimentacion, gastos_estudio, servicios_familiar, otros_familiar, capital_inicial, ventas_negocio, otro_venta_negocio, personal_negocio, alquiler_negocio, servicios_negocio, otros_negocio_salida, resultado_economico, fecha_hora_ficha, monto_aprobado, fecha_hora_modificacion, obs, estado_ficha, monto_inicial, cambio_monto, incremento, fecha_limite, sw, agencia, login, obs_mod) FROM stdin;
0	19940774	2000	100	0	25000	0	120	0	0	3450	350	100	232	300	1500	\N	\N	2020-06-29 00:00:00	1254.40000000000009	2020-06-29 00:00:00		f	1	\N	\N	\N	\N	HUANCAYO	FMARTINEZ17	TRANSPORTE 
1	44974563	500	250	1500	800	0	0	150	700	2500	250	150	150	100	50	\N	\N	2020-06-29 00:00:00	1600	2020-06-29 00:00:00	VERIFICACION CONSTANTE	f	2	\N	\N	\N	\N	HUANCAYO	FMARTINEZ17	PASTELERIA
2	23701995	1500	800	1000	100000	0	500	400	0	4500	2000	150	420	600	0	\N	\N	2020-06-29 00:00:00	2024	2020-06-29 00:00:00		f	3	\N	\N	\N	\N	HUANCAYO	FMARTINEZ17	MULTIPLEX
3	23701995	1500	800	1000	100000	0	1000	1800	0	4500	800	150	420	600	0	\N	\N	2020-06-29 00:00:00	2984	2020-06-29 00:00:00		f	3	\N	\N	\N	\N	HUANCAYO	FMARTINEZ17	MULTIPLEX
4	23701995	1500	800	1000	100000	0	1000	1800	0	4500	800	150	420	600	0	\N	\N	2020-06-29 00:00:00	2984	2020-06-29 00:00:00		f	3	\N	\N	\N	\N	HUANCAYO	FMARTINEZ17	MULTIPLEX
5	23701995	1500	800	1000	15000	500	1000	1800	100	6000	1000	150	420	1000	100	\N	\N	2020-06-29 00:00:00	4264	2020-06-29 00:00:00	NINGUNO	f	3	\N	\N	\N	\N	HUANCAYO	FMARTINEZ17	MULTIPLEX
6	20113864	420	0	100	2500	0	200	20	1552.90000000000009	800	250	80	20	2500	150	\N	\N	2020-06-29 00:00:00	2240	2020-06-29 00:00:00		f	5	\N	\N	\N	\N	HUANCAYO	FMARTINEZ17	POSTRES
7	76621787	5000	0	1000	15000	0	500	16.3000000000000007	847.799999999999955	5000	100	100	16.3000000000000007	500	150	\N	\N	2020-06-29 00:00:00	4106.96000000000004	2020-06-29 00:00:00		f	6	\N	\N	\N	\N	HUANCAYO	JHERRERA	GRAS SISTETICO COLCABAMBINO
8	71872891	5000	0	300	1200	0	300	110	220	1200	100	100	110	600	100	\N	\N	2020-06-29 00:00:00	1112	2020-06-29 00:00:00		f	7	\N	\N	\N	\N	HUANCAYO	JHERRERA	INGENIERA DE SISTEMAS 
9	19999596	6000	300	100	15000	0	600	245.150000000000006	980.600000000000023	5000	100	50	9809.60000000000036	7000	100	\N	\N	2020-06-29 00:00:00	1552.31999999999994	2020-06-29 00:00:00		f	8	\N	\N	\N	\N	HUANCAYO	JHERRERA	PERSONAL DE LIMPIEZA
10	46357683	1000	200	6000	1500	0	900	1022.29999999999995	1022.29999999999995	2000	200	100	100	900	50	\N	\N	2020-06-29 00:00:00	1960	2020-06-29 00:00:00		f	9	\N	\N	\N	\N	HUANCAYO	JHERRERA	VENTA DE FRUTAS 
11	46357683	1000	200	6000	1500	0	900	1022.29999999999995	1022.29999999999995	2000	200	100	100	900	50	\N	\N	2020-06-29 00:00:00	1960	2020-06-29 00:00:00		f	9	\N	\N	\N	\N	HUANCAYO	LHERRERA	VENTA DE FRUTAS 
12	47475777	10000	5000	1200	50000	0	3000	297.100000000000023	594.100000000000023	7000	100	50	100	500	50	\N	\N	2020-06-29 00:00:00	5760	2020-06-29 00:00:00		f	11	\N	\N	\N	\N	HUANCAYO	JHERRERA	CONSTRUCCION 
13	47475777	10000	5000	1200	50000	0	3000	297.100000000000023	594.100000000000023	7000	100	50	100	500	50	\N	\N	2020-06-29 00:00:00	5760	2020-06-29 00:00:00		f	11	\N	\N	\N	\N	HUANCAYO	JHERRERA	CONSTRUCCION 
14	76290455	5000	3000	600	20000	0	5000	12.6999999999999993	224.199999999999989	3000	300	100	200	600	300	\N	\N	2020-06-30 00:00:00	2160	2020-06-30 00:00:00		f	13	\N	\N	\N	\N	HUANCAYO	LHERRERA	TAXISTA
15	76290455	5000	3000	600	20000	0	5000	12.6999999999999993	224.199999999999989	3000	300	100	200	600	300	\N	\N	2020-06-30 00:00:00	2160	2020-06-30 00:00:00		f	13	\N	\N	\N	\N	HUANCAYO	LHERRERA	TAXISTA
16	40129120	1000	500	0	3000	0	0	0	500	2000	1000	200	0	1000	1000	\N	\N	2020-06-30 00:00:00	640	2020-06-30 00:00:00	EL SEÑOR CUENTA CON SOLVENCIA MEDIA	f	14	\N	\N	\N	\N	HUANCAYO	MCHUQUILLANQUI	TRANSPORTE
17	42297478	6000	2000	3000	10000	0	600	78.7999999999999972	345	6000	100	100	200	78.7999999999999972	345	\N	\N	2020-06-30 00:00:00	4267.03999999999996	2020-06-30 00:00:00		f	15	\N	\N	\N	\N	HUANCAYO	LHERRERA	SOLDADURIA
18	19940220	10000	5000	3000	1000	0	500	45	45	15000	2000	100	100	300	300	\N	\N	2020-06-30 00:00:00	10240	2020-06-30 00:00:00		f	16	\N	\N	\N	\N	HUANCAYO	LHERRERA	COMERCIANTE
19	16134843	5000	0	2500	15000	2000	1000	56.7000000000000028	566.399999999999977	2000	200	100	50	900	300	\N	\N	2020-06-30 00:00:00	1800	2020-06-30 00:00:00		f	17	\N	\N	\N	\N	HUANCAYO	LHERRERA	MUNICIPALIDAD DE HUANCAN 
20	60064606	1000	300	500	500	500	0	0	1000	2500	800	300	500	0	500	\N	\N	2020-06-30 00:00:00	320	2020-06-30 00:00:00	CLIENTE SUSTENTABLE	f	18	\N	\N	\N	\N	HUANCAYO	MCHUQUILLANQUI	TIENDA
21	41123341	1000	3000	1800	20000	1000	5000	80.5999999999999943	80.5999999999999943	6000	300	100	100	6000	1000	\N	\N	2020-06-30 00:00:00	8400	2020-06-30 00:00:00		f	19	\N	\N	\N	\N	HUANCAYO	LHERRERA	RESTAURANT NATALY
22	44279232	9000	1000	7000	25000	0	5000	19.1999999999999993	226.300000000000011	3000	200	1000	100	6000	200	\N	\N	2020-06-30 00:00:00	6000	2020-06-30 00:00:00		f	20	\N	\N	\N	\N	HUANCAYO	LHERRERA	FABRIGA DE TELAS 
23	45745478	1000	500	0	3000	10000	0	500	0	1500	500	100	300	200	200	\N	\N	2020-06-30 00:00:00	480	2020-06-30 00:00:00	CLIENTE AMORTIZA DEUDA ANTIGUA	f	21	\N	\N	\N	\N	HUANCAYO	MCHUQUILLANQUI	JUGOS
24	73054005	5000	6000	7000	10000	0	3000104	351	1404	5000	1000	100	200	6000	300	\N	\N	2020-06-30 00:00:00	7520	2020-06-30 00:00:00		f	22	\N	\N	\N	\N	HUANCAYO	LHERRERA	VENTAS 
25	42831861	500	500	0	1500	6000	0	500	0	1500	0	100	500	200	500	\N	\N	2020-06-30 00:00:00	480	2020-06-30 00:00:00	CLIENTE NO SUSTENTABLE	f	23	\N	\N	\N	\N	HUANCAYO	MCHUQUILLANQUI	EVENTOS
26	20064201	10000	5000	1000	20000	0	500	780	780	5000	100	100	100	500	100	\N	\N	2020-06-30 00:00:00	4080	2020-06-30 00:00:00		f	24	\N	\N	\N	\N	HUANCAYO	LHERRERA	CONSTRUCCION
27	20099395	1500	500	0	5000	6000	0	500	0	2000	1000	200	100	200	500	\N	\N	2020-06-30 00:00:00	320	2020-06-30 00:00:00	REPROGRAMACION	f	25	\N	\N	\N	\N	HUANCAYO	MCHUQUILLANQUI	HERRERO
28	80145065	9000	6000	6000	15000	0	600	500	2343	9000	2000	100	100	7000	300	\N	\N	2020-06-30 00:00:00	10800	2020-06-30 00:00:00		f	26	\N	\N	\N	\N	HUANCAYO	LHERRERA	PERSONAL DE LIMPIEZA
29	41773365	1500	500	0	3000	5000	0	500	0	4000	1000	1000	500	0	200	\N	\N	2020-06-30 00:00:00	1040	2020-06-30 00:00:00	CLIENTE ACTIVO	f	27	\N	\N	\N	\N	HUANCAYO	MCHUQUILLANQUI	MELAMINAS JHAIR
30	00098119	1000	200	500	22000	2500	100	500	8000	250	40	50	21.3999999999999986	800	100	\N	\N	2020-07-01 00:00:00	670.879999999999995	2020-07-01 00:00:00		f	28	\N	\N	\N	\N	PUCALLPA	VELAPEREZ	RESTAURANTE
31	00098119	1000	200	500	22000	2500	100	500	8000	600	40	50	21.3999999999999986	1000	100	\N	\N	2020-07-01 00:00:00	1110.88000000000011	2020-07-01 00:00:00		f	28	\N	\N	\N	\N	PUCALLPA	VELAPEREZ	RESTAURANTE
32	20898988	5000	6000	5000	20000	0	4000	378	1512	5000	150	200	100	6000	200	\N	\N	2020-07-01 00:00:00	8280	2020-07-01 00:00:00		f	29	\N	\N	\N	\N	HUANCAYO	LHERRERA	COMERCIANTE
33	41274111	8000	6000	6000	15000	0	6000	200	1153.90000000000009	9000	350	6000	1500	6500	300	\N	\N	2020-07-01 00:00:00	5880	2020-07-01 00:00:00		f	30	\N	\N	\N	\N	HUANCAYO	LHERRERA	COMERCIANTE
34	21276617	1000	500	0	1000	6000	0	500	0	2500	0	500	500	0	500	\N	\N	2020-07-01 00:00:00	800	2020-07-01 00:00:00		f	31	\N	\N	\N	\N	HUANCAYO	MCHUQUILLANQUI	MINA
35	44326038	9000	6000	5000	15000	0	1000	181.5	746	9000	1000	6000	100	50	100	\N	\N	2020-07-01 00:00:00	1480	2020-07-01 00:00:00		f	32	\N	\N	\N	\N	HUANCAYO	LHERRERA	 COSTURERIA 
36	44326038	9000	6000	5000	15000	0	1000	181.5	746	9000	1000	6000	100	50	100	\N	\N	2020-07-01 00:00:00	1480	2020-07-01 00:00:00		f	32	\N	\N	\N	\N	HUANCAYO	LHERRERA	 COSTURERIA 
37	00061656	20500	500	5000	1200	800	100	100	1000	1500	100	100	500	100	50	\N	\N	2020-07-01 00:00:00	680	2020-07-01 00:00:00		f	33	\N	\N	\N	\N	PUCALLPA	VELAPEREZ	SERRAGERIA
38	71340438	1800	500	1000	500	300	100	400	500	720	200	100	100	80	40	\N	\N	2020-07-01 00:00:00	288	2020-07-01 00:00:00		f	34	\N	\N	\N	\N	PUCALLPA	VELAPEREZ	RIOS PEREZ RAY NILK
39	71340438	1800	500	1000	500	300	100	400	500	850	100	50	50	350	50	\N	\N	2020-07-01 00:00:00	760	2020-07-01 00:00:00		f	34	\N	\N	\N	\N	PUCALLPA	VELAPEREZ	RIOS PEREZ RAY NILK
40	46485289	1500	500	0	3000	5000	0	1000	0	2000	1000	100	100	200	300	\N	\N	2020-07-02 00:00:00	560	2020-07-02 00:00:00	CLIENTE ACTIVO	f	35	\N	\N	\N	\N	HUANCAYO	MCHUQUILLANQUI	COMERCIO
41	40893286	1000	500	0	3000	5000	0	500	1000	2500	1000	100	500	0	300	\N	\N	2020-07-02 00:00:00	480	2020-07-02 00:00:00		f	36	\N	\N	\N	\N	HUANCAYO	MCHUQUILLANQUI	JANET
42	40893286	1000	500	0	3000	5000	0	500	1000	2500	1000	100	500	400	300	\N	\N	2020-07-02 00:00:00	800	2020-07-02 00:00:00		f	36	\N	\N	\N	\N	HUANCAYO	MCHUQUILLANQUI	JANET
43	19942242	2000	1000	0	1000	10000	0	1000	500	2000	700	500	500	1000	200	\N	\N	2020-07-02 00:00:00	880	2020-07-02 00:00:00	cliente activo	f	37	\N	\N	\N	\N	HUANCAYO	MCHUQUILLANQUI	FILOMENA
44	19944388	1000	1000	0	1500	6000	0	500	0	1000	200	100	200	800	100	\N	\N	2020-07-02 00:00:00	960	2020-07-02 00:00:00		f	38	\N	\N	\N	\N	HUANCAYO	MCHUQUILLANQUI	GLORIA
45	77161207	15000	3000	5000	1500	0	5000	200	1200	8000	2000	100	100	600	200	\N	\N	2020-07-03 00:00:00	4960	2020-07-03 00:00:00		f	39	\N	\N	\N	\N	HUANCAYO	LHERRERA	VENTA DE LANA 
46	45666252	8000	600	6000	15000	0	600	10.8000000000000007	280	5000	350	100	500	500	200	\N	\N	2020-07-03 00:00:00	3480	2020-07-03 00:00:00		f	40	\N	\N	\N	\N	HUANCAYO	LHERRERA	VENTA DE POLLOS 
47	41278746	5000	7000	9000	15000	0	600	0	0	9000	2000	500	100	300	200	\N	\N	2020-07-03 00:00:00	5200	2020-07-03 00:00:00		f	41	\N	\N	\N	\N	HUANCAYO	LHERRERA	LIBRERIA 
48	99907227	5000	5000	10000	15000	0	2000	97.7999999999999972	97.7999999999999972	15000	5000	100	100	5000	3000	\N	\N	2020-07-03 00:00:00	9440	2020-07-03 00:00:00		f	42	\N	\N	\N	\N	HUANCAYO	LHERRERA	BODEGA
49	80535329	5000	2000	1500	2500	0	1500	97.7999999999999972	97.7999999999999972	5000	200	50	80	500	200	\N	\N	2020-07-03 00:00:00	3976	2020-07-03 00:00:00		f	43	\N	\N	\N	\N	HUANCAYO	LHERRERA	TAXIA VEA
50	20065148	9300	16500	12000	65000	18000	0	4320	1500	8500	120	100	650	2500	100	\N	\N	2020-07-03 00:00:00	8024	2020-07-03 00:00:00		f	44	\N	\N	\N	\N	HUANCAYO	FMARTINEZ17	TEXTIL
51	47846248	15000	3500	0	32000	0	0	325	1500	4500	0	100	175	1500	150	\N	\N	2020-07-03 00:00:00	4460	2020-07-03 00:00:00		f	45	\N	\N	\N	\N	HUANCAYO	FMARTINEZ17	ASISTENTE ADMINISTRATIVO
52	40144277	8000	5000	6000	15000	0	3000	792	792	8000	300	100	100	3000	100	\N	\N	2020-07-03 00:00:00	8320	2020-07-03 00:00:00		f	46	\N	\N	\N	\N	HUANCAYO	LHERRERA	COSTURA
53	48024565	7500	2000	0	1000	0	0	895	6000	2000	0	50	550	500	50	\N	\N	2020-07-03 00:00:00	1480	2020-07-03 00:00:00		f	47	\N	\N	\N	\N	HUANCAYO	FMARTINEZ17	PROFESORA
54	46196324	1000	200	0	3000	2000	0	200	800	3000	0	1500	200	700	400	\N	\N	2020-07-04 00:00:00	1280	2020-07-04 00:00:00		f	49	\N	\N	\N	\N	HUANCAYO	MCHUQUILLANQUI	CARDENAS
55	20999895	2000	1000	0	2000	10000	0	500	0	2000	1000	200	100	800	400	\N	\N	2020-07-04 00:00:00	880	2020-07-04 00:00:00		f	50	\N	\N	\N	\N	HUANCAYO	MCHUQUILLANQUI	FOURLIFE
56	42388030	0	200	0	5000	10000	0	100	0	800	0	100	100	500	100	\N	\N	2020-07-04 00:00:00	800	2020-07-04 00:00:00		f	51	\N	\N	\N	\N	HUANCAYO	MCHUQUILLANQUI	ETUPSA
57	19946742	2000	500	0	2000	15000	0	1700	0	2500	1000	200	100	1200	100	\N	\N	2020-07-04 00:00:00	1840	2020-07-04 00:00:00		f	52	\N	\N	\N	\N	HUANCAYO	MCHUQUILLANQUI	MARMOLEJO
58	19870079	6000	4000	3000	25000	0	3000	340.199999999999989	340.199999999999989	6000	150	100	100	5000	300	\N	\N	2020-07-04 00:00:00	8280	2020-07-04 00:00:00		f	53	\N	\N	\N	\N	HUANCAYO	LHERRERA	RESTAURANT
59	19996893	15000	2000	1500	15000	0	500	560.5	560.5	15000	3000	100	100	1500	200	\N	\N	2020-07-06 00:00:00	10480	2020-07-06 00:00:00		f	54	\N	\N	\N	\N	HUANCAYO	LHERRERA	LIBRERIA 
60	19996893	15000	2000	1500	15000	0	500	560.5	560.5	15000	3000	100	100	1500	200	\N	\N	2020-07-06 00:00:00	10480	2020-07-06 00:00:00		f	54	\N	\N	\N	\N	HUANCAYO	LHERRERA	LIBRERIA 
61	45879388	9000	5000	1500	20000	0	500	1000	1000	15000	5000	100	100	5000	2000	\N	\N	2020-07-06 00:00:00	10240	2020-07-06 00:00:00		f	55	\N	\N	\N	\N	HUANCAYO	LHERRERA	INTERNET
62	70437871	9000	3000	3000	15000	0	9000	1924.70000000000005	1924.70000000000005	5000	500	100	100	2000	1000	\N	\N	2020-07-06 00:00:00	4240	2020-07-06 00:00:00		f	56	\N	\N	\N	\N	HUANCAYO	LHERRERA	CONSULTORIA 
63	70437871	9000	3000	3000	15000	0	9000	1924.70000000000005	1924.70000000000005	5000	500	100	100	2000	1000	\N	\N	2020-07-06 00:00:00	4240	2020-07-06 00:00:00		f	56	\N	\N	\N	\N	HUANCAYO	LHERRERA	CONSULTORIA 
64	44729185	15000	6000	2000	20000	0	2000	2047	2047	15000	3000	100	100	3000	1000	\N	\N	2020-07-06 00:00:00	11040	2020-07-06 00:00:00		f	57	\N	\N	\N	\N	HUANCAYO	LHERRERA	DOCENTE
65	20047216	15000	10000	1000	20000	0	2000	211.900000000000006	211.900000000000006	20000	1500	100	100	10000	2000	\N	\N	2020-07-06 00:00:00	21040	2020-07-06 00:00:00		f	58	\N	\N	\N	\N	HUANCAYO	LHERRERA	DECORACION
66	47420055	5300	100	14500	8000	0	0	1290	8000	3200	0	100	1250	5000	50	\N	\N	2020-07-06 00:00:00	5440	2020-07-06 00:00:00		f	59	\N	\N	\N	\N	HUANCAYO	FMARTINEZ17	BOTICA
67	70020689	9000	6000	15000	20000	0	10000	365.600000000000023	365.600000000000023	15000	500	100	100	6000	300	\N	\N	2020-07-06 00:00:00	16000	2020-07-06 00:00:00		f	48	\N	\N	\N	\N	HUANCAYO	LHERRERA	INTERNET MAGALY
68	80245301	10000	5000	25000	25000	0	5000	425.300000000000011	425.300000000000011	9000	1000	100	100	3000	100	\N	\N	2020-07-07 00:00:00	8560	2020-07-07 00:00:00		f	60	\N	\N	\N	\N	HUANCAYO	LHERRERA	COMERCIANTE
69	44350264	15000	1500	1500	25000	0	5000	143.300000000000011	143.300000000000011	5000	700	100	100	150	50	\N	\N	2020-07-07 00:00:00	3360	2020-07-07 00:00:00		f	61	\N	\N	\N	\N	HUANCAYO	LHERRERA	COOPEADORA
70	19944260	15000	1500	12000	25000	0	1000	62.3999999999999986	62.3999999999999986	15000	5000	100	100	2000	100	\N	\N	2020-07-07 00:00:00	9360	2020-07-07 00:00:00		f	62	\N	\N	\N	\N	HUANCAYO	LHERRERA	BODEGA
71	77377731	10000	5000	15000	10000	1000	1500	352	352	15000	1000	100	100	1000	100	\N	\N	2020-07-07 00:00:00	11760	2020-07-07 00:00:00		f	64	\N	\N	\N	\N	HUANCAYO	LHERRERA	GANADERIA 
72	77381267	10000	10000	5000	15000	0	10000	123	123	10000	2000	100	100	2000	100	\N	\N	2020-07-07 00:00:00	7760	2020-07-07 00:00:00		f	65	\N	\N	\N	\N	HUANCAYO	LHERRERA	GANADERIA
73	05213767	1600	300	500	1000	500	200	100	300	1500	500	100	200	100	200	\N	\N	2020-07-08 00:00:00	480	2020-07-08 00:00:00		f	63	\N	\N	\N	\N	PUCALLPA	VELAPEREZ	MONROY CAMAYO ROBINSON
74	05213767	1600	300	500	1000	500	200	100	300	1500	100	100	200	500	200	\N	\N	2020-07-08 00:00:00	1120	2020-07-08 00:00:00		f	63	\N	\N	\N	\N	PUCALLPA	VELAPEREZ	MONROY CAMAYO ROBINSON
75	00093054	500	200	100	500	100	100	50	200	1500	500	100	100	100	100	\N	\N	2020-07-08 00:00:00	640	2020-07-08 00:00:00		f	66	\N	\N	\N	\N	PUCALLPA	LARIAS	CORAL REATEGUI FELISINDA
76	00035433	500	200	1000	700	500	100	0	500	1500	500	150	300	300	150	\N	\N	2020-07-08 00:00:00	560	2020-07-08 00:00:00		f	67	\N	\N	\N	\N	PUCALLPA	VELAPEREZ	PEREZPEREZ REYNA MARINA
77	00035433	500	200	1000	700	500	350	0	500	1500	100	150	300	400	150	\N	\N	2020-07-08 00:00:00	960	2020-07-08 00:00:00		f	67	\N	\N	\N	\N	PUCALLPA	VELAPEREZ	PEREZPEREZ REYNA MARINA
78	00093054	1500	200	100	500	100	300	50	200	1500	500	100	100	100	100	\N	\N	2020-07-08 00:00:00	640	2020-07-08 00:00:00		f	66	\N	\N	\N	\N	PUCALLPA	VELAPEREZ	CORAL REATEGUI FELISINDA
79	00093054	1500	200	100	1000	100	300	50	200	1500	500	100	100	100	100	\N	\N	2020-07-08 00:00:00	640	2020-07-08 00:00:00		f	66	\N	\N	\N	\N	PUCALLPA	VELAPEREZ	CORAL REATEGUI FELISINDA
80	00093054	1500	200	500	500	500	300	0	200	2000	500	100	100	200	100	\N	\N	2020-07-08 00:00:00	1120	2020-07-08 00:00:00		f	66	\N	\N	\N	\N	PUCALLPA	VELAPEREZ	CORAL REATEGUI FELISINDA
81	42611489	10000	6000	9000	15000	0	5000	324	324	15000	3000	100	100	5000	1000	\N	\N	2020-07-09 00:00:00	12640	2020-07-09 00:00:00		f	68	\N	\N	\N	\N	HUANCAYO	LHERRERA	COMERCIANTE
82	43568148	9000	5000	12000	15000	0	5000	918.899999999999977	918.899999999999977	15000	5000	100	100	10000	100	\N	\N	2020-07-09 00:00:00	15760	2020-07-09 00:00:00		f	69	\N	\N	\N	\N	HUANCAYO	LHERRERA	BODEGA Y RESTAUTANT
83	47323767	10000	6000	3500	15000	0	15000	918.899999999999977	918.899999999999977	15000	1000	100	100	1000	100	\N	\N	2020-07-09 00:00:00	11760	2020-07-09 00:00:00		f	70	\N	\N	\N	\N	HUANCAYO	LHERRERA	TRANSPORTE
84	44931763	10000	5500	15000	15000	0	6000	2200	2200	15000	3000	100	100	3000	100	\N	\N	2020-07-09 00:00:00	11760	2020-07-09 00:00:00		f	71	\N	\N	\N	\N	HUANCAYO	LHERRERA	LIMPIEZA
85	46657916	10000	1000	15000	15000	0	3000	725.5	725.5	15000	5000	100	100	6000	100	\N	\N	2020-07-09 00:00:00	12560	2020-07-09 00:00:00		f	72	\N	\N	\N	\N	HUANCAYO	LHERRERA	GRANJA 
86	48117988	15000	10000	10000	15000	0	5000	466.300000000000011	466.300000000000011	10000	2000	100	100	10000	100	\N	\N	2020-07-09 00:00:00	14160	2020-07-09 00:00:00		f	73	\N	\N	\N	\N	HUANCAYO	LHERRERA	COMERCIANTE
87	44019045	10000	15000	10000	15000	0	5000	500	500	15000	3000	100	150	1500	100	\N	\N	2020-07-09 00:00:00	10520	2020-07-09 00:00:00		f	74	\N	\N	\N	\N	HUANCAYO	LHERRERA	CONDUCTOR
88	46706530	15000	15000	15000	15000	0	5000	1500	1500	15000	3500	100	100	1500	100	\N	\N	2020-07-09 00:00:00	10160	2020-07-09 00:00:00		f	75	\N	\N	\N	\N	HUANCAYO	LHERRERA	COSTURERIA
89	48522972	15000	6000	10000	15000	0	1000	285.800000000000011	285.800000000000011	15000	3500	100	150	15000	5000	\N	\N	2020-07-09 00:00:00	17000	2020-07-09 00:00:00		f	76	\N	\N	\N	\N	HUANCAYO	LHERRERA	TRANSPORTE
90	80401963	15000	1000	1000	15000	0	5000	330	330	15000	5500	100	100	5000	200	\N	\N	2020-07-09 00:00:00	11280	2020-07-09 00:00:00		f	10	\N	\N	\N	\N	HUANCAYO	LHERRERA	TRANSPORTE
91	42781214	15000	6000	15000	15000	0	6000	831.299999999999955	831.299999999999955	15000	3500	100	100	5000	100	\N	\N	2020-07-10 00:00:00	12960	2020-07-10 00:00:00		f	77	\N	\N	\N	\N	HUANCAYO	LHERRERA	COMERCIANTE
92	47858782	10000	6500	15000	15000	0	3500	168.300000000000011	168.300000000000011	15000	5000	100	100	100	100	\N	\N	2020-07-10 00:00:00	7840	2020-07-10 00:00:00		f	78	\N	\N	\N	\N	HUANCAYO	LHERRERA	CONDUCTOR
93	42148384	1000	300	150	500	300	100	100	500	2500	150	600	500	300	300	\N	\N	2020-07-10 00:00:00	1000	2020-07-10 00:00:00		f	79	\N	\N	\N	\N	PUCALLPA	VELAPEREZ	SAJAMI VASQUEZ ERIKA LUCIA
94	76918572	15000	1000	15000	15000	0	5000	456.100000000000023	456.100000000000023	15000	2500	100	100	1000	100	\N	\N	2020-07-11 00:00:00	10560	2020-07-11 00:00:00		f	81	\N	\N	\N	\N	HUANCAYO	LHERRERA	COMERCIANTE
95	48059190	15000	5500	2000	15000	0	1000	2000	2000	15000	3500	100	100	35000	100	\N	\N	2020-07-11 00:00:00	36960	2020-07-11 00:00:00		f	82	\N	\N	\N	\N	HUANCAYO	LHERRERA	COMERCIANTE
96	76663751	15000	2500	24000	15000	0	3500	204.5	204.5	15000	3500	100	100	3500	100	\N	\N	2020-07-11 00:00:00	11760	2020-07-11 00:00:00		f	83	\N	\N	\N	\N	HUANCAYO	LHERRERA	COMERCIANTE
97	00124443	1000	300	600	1000	500	100	100	600	2800	500	300	600	100	300	\N	\N	2020-07-11 00:00:00	960	2020-07-11 00:00:00		f	84	\N	\N	\N	\N	PUCALLPA	VELAPEREZ	AGUILAR YSLA JUAN MANUEL
98	20682065	15000	5500	12000	20000	0	10000	300	300	12000	3500	100	100	100	100	\N	\N	2020-07-13 00:00:00	6640	2020-07-13 00:00:00		f	85	\N	\N	\N	\N	HUANCAYO	LHERRERA	COMERCIANTE
99	44100810	10000	5500	3500	25000	0	10000	291.600000000000023	291.600000000000023	15000	3500	100	150	3500	100	\N	\N	2020-07-13 00:00:00	11720	2020-07-13 00:00:00		f	86	\N	\N	\N	\N	HUANCAYO	LHERRERA	COMERCIANTE
100	41614930	5000	500	500	500	500	0	500	500	1800	100	100	0	350	50	\N	\N	2020-07-14 00:00:00	1520	2020-07-14 00:00:00		f	87	\N	\N	\N	\N	HUANCAYO	FMARTINEZ17	COCINERO
101	48177932	15000	3500	5000	15000	0	3600	149.699999999999989	149.699999999999989	15000	3500	100	149.699999999999989	1000	100	\N	\N	2020-07-14 00:00:00	9720.23999999999978	2020-07-14 00:00:00		f	88	\N	\N	\N	\N	HUANCAYO	LHERRERA	COMERCIANTE
102	48142577	15000	1500	1500	15000	0	1500	1720	1720	1500	300	100	100	100	100	\N	\N	2020-07-14 00:00:00	800	2020-07-14 00:00:00		f	89	\N	\N	\N	\N	HUANCAYO	LHERRERA	CONTABILIDAD
103	19882294	15000	2000	15000	25000	0	15000	300	300	5000	1500	100	100	1000	100	\N	\N	2020-07-15 00:00:00	3360	2020-07-15 00:00:00		f	90	\N	\N	\N	\N	HUANCAYO	LHERRERA	COMERCIANTE
104	21000001	10000	3500	5500	15000	1000	3500	1000	1000	16500	2500	100	3500	1000	100	\N	\N	2020-07-15 00:00:00	9040	2020-07-15 00:00:00		f	91	\N	\N	\N	\N	HUANCAYO	LHERRERA	COMERCIANTE
105	41738167	15000	3500	25000	15000	0	3500	150	150	25000	3500	100	100	1500	350	\N	\N	2020-07-15 00:00:00	17960	2020-07-15 00:00:00		f	92	\N	\N	\N	\N	HUANCAYO	LHERRERA	ALBAÑIL
106	48742387	2000	300	500	1000	500	300	300	500	3600	500	100	270	100	570	\N	\N	2020-07-21 00:00:00	1808	2020-07-21 00:00:00		f	94	\N	\N	\N	\N	PUCALLPA	VELAPEREZ	FUCHS VELA ISABEL
107	00098119	1000	200	500	22000	2500	100	500	8000	1200	60	50	22	1000	100	\N	\N	2020-07-30 00:00:00	1574.40000000000009	2020-07-30 00:00:00		f	28	\N	\N	\N	\N	PUCALLPA	VELAPEREZ	RESTAURANTE
108	00098119	1000	200	500	22000	2500	100	500	8000	1200	60	40	50	1800	100	\N	\N	2020-07-30 00:00:00	2200	2020-07-30 00:00:00		f	28	\N	\N	\N	\N	PUCALLPA	VELAPEREZ	RESTAURANTE
109	70203921	500	100	300	500	300	100	100	300	2000	500	100	100	300	100	\N	\N	2020-07-30 00:00:00	1200	2020-07-30 00:00:00		f	95	\N	\N	\N	\N	PUCALLPA	VELAPEREZ	FATIMA SATOMY CRUZ PINEDO
\.


--
-- Data for Name: fotos; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.fotos (codigo, dni, tipo, descripcion, imagen, fecha, agencia) FROM stdin;
\.


--
-- Data for Name: gastos_operativos; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.gastos_operativos (id_gastos_operativos, id_negocio, descripcion, monto, usuario, agencia, fecha_hora, estado) FROM stdin;
\.


--
-- Data for Name: grantia_real; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.grantia_real (id_grantia_real, dni, bien_real, caracteristicas, comprobante, serie, estado, valor_compra, valor_residual, antiguedad, expectativa, id_usuario, fecha_hora, agencia, valor_real, credito, valor_vendido, almacen, id_usuario_venta, fecha_venta) FROM stdin;
\.


--
-- Data for Name: historial_manipulacion; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.historial_manipulacion (id_historial_manipulacion, id_usuario, operacion, time_ingreso, maquina, agencia) FROM stdin;
82184	1	INGRESO AL SISTEMA	2020-06-28 09:37:02	DESKTOP-CQ135NL	HUANCAYO
82185	1	INGRESO AL SISTEMA	2020-06-28 09:54:51	DESKTOP-CQ135NL	HUANCAYO
82186	1	INGRESO AL SISTEMA	2020-06-28 10:01:22	DESKTOP-CQ135NL	HUANCAYO
82187	1	INGRESO AL SISTEMA	2020-06-28 10:10:29	DESKTOP-CQ135NL	HUANCAYO
82188	1	INGRESO AL SISTEMA	2020-06-28 10:38:13	DJ-XAVEX	HUANCAYO
82189	1	INGRESO AL SISTEMA	2020-06-28 10:45:19	DESKTOP-CQ135NL	TINGOMARIA
82190	1	INGRESO AL SISTEMA	2020-06-28 16:47:30	DESKTOP-CQ135NL	TINGOMARIA
82191	1	INGRESO AL SISTEMA	2020-06-28 17:08:55	DESKTOP-CQ135NL	TINGOMARIA
82192	2	INGRESO AL SISTEMA	2020-06-28 23:40:37	DJ-XAVEX	HUANCAYO
82193	2	INGRESO AL SISTEMA	2020-06-29 09:59:35	DJ-XAVEX	HUANCAYO
82194	1	INGRESO AL SISTEMA	2020-06-29 11:26:43	DESKTOP-CQ135NL	HUNACAYO
82195	1	INGRESO AL SISTEMA	2020-06-29 13:40:48	DESKTOP-2COHNSG	HUANCAYO
82196	1	INGRESO AL SISTEMA	2020-06-29 13:46:16	DESKTOP-RD921RC	HUANCAYO
82197	1	INGRESO AL SISTEMA	2020-06-29 13:50:20	DESKTOP-PDVNOOT	HUANCAYO
82198	2	INGRESO AL SISTEMA	2020-06-29 13:54:50	DESKTOP-PDVNOOT	HUANCAYO
82199	7	INGRESO AL SISTEMA	2020-06-29 14:03:28	DESKTOP-RD921RC	HUANCAYO
82200	7	INGRESO AL SISTEMA	2020-06-29 14:27:37	DJ-XAVEX	HUANCAYO
82201	3	INGRESO AL SISTEMA	2020-06-29 14:38:31	DESKTOP-PDVNOOT	HUANCAYO
82202	3	INGRESO AL SISTEMA	2020-06-29 14:44:24	DESKTOP-PDVNOOT	HUANCAYO
82203	4	INGRESO AL SISTEMA	2020-06-29 14:48:44	DESKTOP-6HII7R6	HUANCAYO
82204	6	INGRESO AL SISTEMA	2020-06-29 14:51:39	DESKTOP-2COHNSG	HUANCAYO
82205	1	INGRESO AL SISTEMA	2020-06-29 15:31:32	DESKTOP-6HII7R6	HUANCAYO
82206	7	INGRESO AL SISTEMA	2020-06-29 15:31:56	DESKTOP-RD921RC	HUANCAYO
82207	7	INGRESO AL SISTEMA	2020-06-29 15:33:24	DESKTOP-6HII7R6	HUANCAYO
82208	6	INGRESO AL SISTEMA	2020-06-29 15:45:11	DESKTOP-2COHNSG	HUANCAYO
82209	7	INGRESO AL SISTEMA	2020-06-29 15:49:33	DESKTOP-RD921RC	HUANCAYO
82210	4	INGRESO AL SISTEMA	2020-06-29 15:54:29	DESKTOP-6HII7R6	HUANCAYO
82211	5	INGRESO AL SISTEMA	2020-06-29 16:07:48	DESKTOP-RD921RC	HUANCAYO
82212	5	INGRESO AL SISTEMA	2020-06-29 16:19:08	DESKTOP-RD921RC	HUANCAYO
82213	7	INGRESO AL SISTEMA	2020-06-29 16:20:44	DESKTOP-RD921RC	HUANCAYO
82214	4	INGRESO AL SISTEMA	2020-06-29 16:26:00	DESKTOP-6HII7R6	HUANCAYO
82215	1	INGRESO AL SISTEMA	2020-06-29 16:33:24	DESKTOP-CQ135NL	HUANCAYO
82216	4	INGRESO AL SISTEMA	2020-06-29 16:34:17	DESKTOP-CQ135NL	HUANCAYO
82217	1	INGRESO AL SISTEMA	2020-06-29 20:42:58	DESKTOP-CQ135NL	HUANCAYO
82218	1	INGRESO AL SISTEMA	2020-06-29 20:50:54	DESKTOP-CQ135NL	HUANCAYO
82219	1	INGRESO AL SISTEMA	2020-06-29 20:54:03	DESKTOP-CQ135NL	HUANCAYO
82220	4	INGRESO AL SISTEMA	2020-06-30 10:06:05	DESKTOP-6HII7R6	HUANCAYO
82221	6	INGRESO AL SISTEMA	2020-06-30 10:10:01	DESKTOP-2COHNSG	HUANCAYO
82222	5	INGRESO AL SISTEMA	2020-06-30 10:12:53	DESKTOP-RD921RC	HUANCAYO
82223	2	INGRESO AL SISTEMA	2020-06-30 10:18:14	DJ-XAVEX	HUANCAYO
82224	6	INGRESO AL SISTEMA	2020-06-30 10:23:18	DESKTOP-2COHNSG	HUANCAYO
82225	5	INGRESO AL SISTEMA	2020-06-30 10:24:48	DESKTOP-RD921RC	HUANCAYO
82226	4	INGRESO AL SISTEMA	2020-06-30 10:27:39	DESKTOP-6HII7R6	HUANCAYO
82227	6	INGRESO AL SISTEMA	2020-06-30 10:50:20	DESKTOP-2COHNSG	HUANCAYO
82228	3	INGRESO AL SISTEMA	2020-06-30 10:52:38	DESKTOP-2COHNSG	HUANCAYO
82229	6	INGRESO AL SISTEMA	2020-06-30 10:58:27	DESKTOP-2COHNSG	HUANCAYO
82230	5	INGRESO AL SISTEMA	2020-06-30 11:00:00	DESKTOP-RD921RC	HUANCAYO
82231	6	INGRESO AL SISTEMA	2020-06-30 11:08:33	DESKTOP-2COHNSG	HUANCAYO
82232	4	INGRESO AL SISTEMA	2020-06-30 11:25:36	DESKTOP-6HII7R6	HUANCAYO
82233	5	INGRESO AL SISTEMA	2020-06-30 11:55:34	DESKTOP-RD921RC	HUANCAYO
82234	5	INGRESO AL SISTEMA	2020-06-30 12:15:46	DESKTOP-RD921RC	HUANCAYO
82235	3	INGRESO AL SISTEMA	2020-06-30 12:22:41	DESKTOP-PDVNOOT	HUANCAYO
82236	4	INGRESO AL SISTEMA	2020-06-30 12:56:00	DESKTOP-6HII7R6	HUANCAYO
82237	4	INGRESO AL SISTEMA	2020-06-30 12:58:20	DESKTOP-CQ135NL	HUANCAYO
82238	4	INGRESO AL SISTEMA	2020-06-30 13:00:31	DESKTOP-CQ135NL	HUANCAYO
82239	4	INGRESO AL SISTEMA	2020-06-30 13:02:41	DESKTOP-CQ135NL	HUANCAYO
82240	4	INGRESO AL SISTEMA	2020-06-30 13:06:16	DESKTOP-6HII7R6	HUANCAYO
82241	2	INGRESO AL SISTEMA	2020-06-30 13:08:32	DESKTOP-6HII7R6	HUANCAYO
82242	5	INGRESO AL SISTEMA	2020-06-30 13:09:48	DESKTOP-RD921RC	HUANCAYO
82243	6	INGRESO AL SISTEMA	2020-06-30 13:58:14	DESKTOP-2COHNSG	HUANCAYO
82244	5	INGRESO AL SISTEMA	2020-06-30 14:50:42	DESKTOP-RD921RC	HUANCAYO
82245	5	INGRESO AL SISTEMA	2020-06-30 16:09:50	DESKTOP-RD921RC	HUANCAYO
82246	1	INGRESO AL SISTEMA	2020-06-30 22:21:55	ADMINIPUCALLPA	HUANCAYO
82247	1	INGRESO AL SISTEMA	2020-06-30 22:25:37	DESKTOP-CQ135NL	PUCALLPA
82248	1	INGRESO AL SISTEMA	2020-06-30 22:31:08	ADMINIPUCALLPA	PUCALLPA
82249	1	INGRESO AL SISTEMA	2020-06-30 22:44:09	ADMINIPUCALLPA	PUCALLPA
82250	2	INGRESO AL SISTEMA	2020-06-30 22:49:30	DJ-XAVEX	HUANCAYO
82251	8	INGRESO AL SISTEMA	2020-06-30 23:02:33	ADMINIPUCALLPA	PUCALLPA
82252	2	INGRESO AL SISTEMA	2020-06-30 23:25:25	DJ-XAVEX	HUANCAYO
82253	11	INGRESO AL SISTEMA	2020-06-30 23:32:32	ADMINIPUCALLPA	PUCALLPA
82254	2	INGRESO AL SISTEMA	2020-06-30 23:41:25	DJ-XAVEX	HUANCAYO
82255	2	INGRESO AL SISTEMA	2020-06-30 23:42:01	DJ-XAVEX	HUANCAYO
82256	2	INGRESO AL SISTEMA	2020-06-30 23:42:40	DJ-XAVEX	HUANCAYO
82257	11	INGRESO AL SISTEMA	2020-07-01 09:12:04	ADMINIPUCALLPA	PUCALLPA
82258	9	INGRESO AL SISTEMA	2020-07-01 09:55:25	ADMINIPUCALLPA	PUCALLPA
82259	3	INGRESO AL SISTEMA	2020-07-01 10:06:30	DESKTOP-PDVNOOT	HUANCAYO
82260	2	INGRESO AL SISTEMA	2020-07-01 10:07:39	DJ-XAVEX	HUANCAYO
82261	2	INGRESO AL SISTEMA	2020-07-01 10:17:14	DJ-XAVEX	HUANCAYO
82262	5	INGRESO AL SISTEMA	2020-07-01 10:17:24	DESKTOP-RD921RC	HUANCAYO
82263	11	INGRESO AL SISTEMA	2020-07-01 10:17:56	ADMINIPUCALLPA	PUCALLPA
82264	11	INGRESO AL SISTEMA	2020-07-01 10:59:10	ADMINIPUCALLPA	PUCALLPA
82265	2	INGRESO AL SISTEMA	2020-07-01 11:04:16	DJ-XAVEX	HUANCAYO
82266	11	INGRESO AL SISTEMA	2020-07-01 11:10:41	ADMINIPUCALLPA	PUCALLPA
82267	11	INGRESO AL SISTEMA	2020-07-01 11:32:34	ADMINIPUCALLPA	PUCALLPA
82268	4	INGRESO AL SISTEMA	2020-07-01 11:42:36	DESKTOP-6HII7R6	HUANCAYO
82269	4	INGRESO AL SISTEMA	2020-07-01 11:47:02	DESKTOP-6HII7R6	HUANCAYO
82270	5	INGRESO AL SISTEMA	2020-07-01 13:51:50	DESKTOP-RD921RC	HUANCAYO
82271	5	INGRESO AL SISTEMA	2020-07-01 14:21:05	DESKTOP-RD921RC	HUANCAYO
82272	4	INGRESO AL SISTEMA	2020-07-01 14:43:06	DJ-XAVEX	HUANCAYO
82273	2	INGRESO AL SISTEMA	2020-07-01 17:28:46	DJ-XAVEX	HUANCAYO
82274	4	INGRESO AL SISTEMA	2020-07-01 17:30:40	DJ-XAVEX	HUANCAYO
82275	4	INGRESO AL SISTEMA	2020-07-01 18:15:24	DESKTOP-CQ135NL	HUANCAYO
82276	4	INGRESO AL SISTEMA	2020-07-01 18:31:26	DJ-XAVEX	HUANCAYO
82277	11	INGRESO AL SISTEMA	2020-07-01 18:41:18	ADMINIPUCALLPA	PUCALLPA
82278	2	INGRESO AL SISTEMA	2020-07-01 19:14:33	DJ-XAVEX	HUANCAYO
82279	9	INGRESO AL SISTEMA	2020-07-01 19:37:26	ADMINIPUCALLPA	PUCALLPA
82280	2	INGRESO AL SISTEMA	2020-07-01 21:24:04	DJ-XAVEX	HUANCAYO
82281	1	INGRESO AL SISTEMA	2020-07-01 23:02:20	DESKTOP-CQ135NL	HUANCAYO
82282	11	INGRESO AL SISTEMA	2020-07-01 23:28:45	ADMINIPUCALLPA	PUCALLPA
82283	11	INGRESO AL SISTEMA	2020-07-02 08:47:36	ADMINIPUCALLPA	PUCALLPA
82284	4	INGRESO AL SISTEMA	2020-07-02 08:47:37	DESKTOP-6HII7R6	HUANCAYO
82285	3	INGRESO AL SISTEMA	2020-07-02 08:57:40	DESKTOP-PDVNOOT	HUANCAYO
82286	2	INGRESO AL SISTEMA	2020-07-02 08:59:05	DESKTOP-2COHNSG	HUANCAYO
82287	2	INGRESO AL SISTEMA	2020-07-02 11:31:26	DESKTOP-2COHNSG	HUANCAYO
82288	2	INGRESO AL SISTEMA	2020-07-02 12:01:49	DESKTOP-2COHNSG	HUANCAYO
82289	2	INGRESO AL SISTEMA	2020-07-02 13:33:46	DESKTOP-2COHNSG	HUANCAYO
82290	11	INGRESO AL SISTEMA	2020-07-02 13:41:41	ADMINIPUCALLPA	PUCALLPA
82291	4	INGRESO AL SISTEMA	2020-07-02 13:43:27	DESKTOP-6HII7R6	HUANCAYO
82292	2	INGRESO AL SISTEMA	2020-07-02 20:21:15	DJ-XAVEX	HUANCAYO
82293	11	INGRESO AL SISTEMA	2020-07-02 22:39:09	ADMINIPUCALLPA	PUCALLPA
82294	4	INGRESO AL SISTEMA	2020-07-03 08:57:13	DESKTOP-6HII7R6	HUANCAYO
82295	11	INGRESO AL SISTEMA	2020-07-03 09:01:09	ADMINIPUCALLPA	PUCALLPA
82296	5	INGRESO AL SISTEMA	2020-07-03 09:03:30	DESKTOP-RD921RC	HUANCAYO
82297	2	INGRESO AL SISTEMA	2020-07-03 09:21:10	DESKTOP-6HII7R6	HUANCAYO
82298	3	INGRESO AL SISTEMA	2020-07-03 09:56:18	DESKTOP-2COHNSG	HUANCAYO
82299	4	INGRESO AL SISTEMA	2020-07-03 10:22:07	DESKTOP-6HII7R6	HUANCAYO
82300	5	INGRESO AL SISTEMA	2020-07-03 10:22:18	DESKTOP-RD921RC	HUANCAYO
82301	3	INGRESO AL SISTEMA	2020-07-03 10:26:10	DESKTOP-2COHNSG	HUANCAYO
82302	11	INGRESO AL SISTEMA	2020-07-03 12:01:05	ADMINIPUCALLPA	PUCALLPA
82303	5	INGRESO AL SISTEMA	2020-07-03 12:15:52	DESKTOP-RD921RC	HUANCAYO
82304	5	INGRESO AL SISTEMA	2020-07-03 12:20:14	DESKTOP-RD921RC	HUANCAYO
82305	5	INGRESO AL SISTEMA	2020-07-03 12:35:37	DESKTOP-RD921RC	HUANCAYO
82306	5	INGRESO AL SISTEMA	2020-07-03 12:48:04	DESKTOP-RD921RC	HUANCAYO
82307	2	INGRESO AL SISTEMA	2020-07-03 13:02:14	DESKTOP-6HII7R6	HUANCAYO
82308	3	INGRESO AL SISTEMA	2020-07-03 14:18:00	DESKTOP-2COHNSG	HUANCAYO
82309	11	INGRESO AL SISTEMA	2020-07-03 15:00:24	ADMINIPUCALLPA	PUCALLPA
82310	3	INGRESO AL SISTEMA	2020-07-04 09:19:32	DESKTOP-PDVNOOT	HUANCAYO
82311	2	INGRESO AL SISTEMA	2020-07-04 09:23:42	DESKTOP-2COHNSG	HUANCAYO
82312	3	INGRESO AL SISTEMA	2020-07-04 09:29:35	DESKTOP-PDVNOOT	HUANCAYO
82313	4	INGRESO AL SISTEMA	2020-07-04 09:34:36	DESKTOP-6HII7R6	HUANCAYO
82314	4	INGRESO AL SISTEMA	2020-07-04 09:46:53	DESKTOP-2COHNSG	HUANCAYO
82315	5	INGRESO AL SISTEMA	2020-07-04 11:16:41	DESKTOP-RD921RC	HUANCAYO
82316	5	INGRESO AL SISTEMA	2020-07-04 11:55:00	DESKTOP-RD921RC	HUANCAYO
82317	4	INGRESO AL SISTEMA	2020-07-06 09:17:56	DESKTOP-6HII7R6	HUANCAYO
82318	5	INGRESO AL SISTEMA	2020-07-06 09:28:35	DESKTOP-RD921RC	HUANCAYO
82319	2	INGRESO AL SISTEMA	2020-07-06 09:57:36	DESKTOP-2COHNSG	HUANCAYO
82320	5	INGRESO AL SISTEMA	2020-07-06 10:40:04	DESKTOP-RD921RC	HUANCAYO
82321	5	INGRESO AL SISTEMA	2020-07-06 12:29:13	DESKTOP-RD921RC	HUANCAYO
82322	5	INGRESO AL SISTEMA	2020-07-06 12:30:31	DESKTOP-RD921RC	HUANCAYO
82323	5	INGRESO AL SISTEMA	2020-07-06 12:33:11	DESKTOP-RD921RC	HUANCAYO
82324	5	INGRESO AL SISTEMA	2020-07-06 12:35:28	DESKTOP-RD921RC	HUANCAYO
82325	5	INGRESO AL SISTEMA	2020-07-06 12:42:46	DESKTOP-RD921RC	HUANCAYO
82326	5	INGRESO AL SISTEMA	2020-07-06 13:30:30	DESKTOP-RD921RC	HUANCAYO
82327	5	INGRESO AL SISTEMA	2020-07-06 13:56:07	DESKTOP-RD921RC	HUANCAYO
82328	11	INGRESO AL SISTEMA	2020-07-07 08:26:59	ADMINIPUCALLPA	PUCALLPA
82329	2	INGRESO AL SISTEMA	2020-07-07 09:19:45	DESKTOP-6HII7R6	HUANCAYO
82330	4	INGRESO AL SISTEMA	2020-07-07 09:19:56	DESKTOP-6HII7R6	HUANCAYO
82331	9	INGRESO AL SISTEMA	2020-07-07 09:55:52	ADMINIPUCALLPA	PUCALLPA
82332	5	INGRESO AL SISTEMA	2020-07-07 10:56:55	DESKTOP-RD921RC	HUANCAYO
82333	9	INGRESO AL SISTEMA	2020-07-07 11:49:21	DESKTOP-6HII7R6	HUANCAYO
82334	9	INGRESO AL SISTEMA	2020-07-07 11:50:14	ADMINIPUCALLPA	PUCALLPA
82335	5	INGRESO AL SISTEMA	2020-07-07 13:35:50	DESKTOP-RD921RC	HUANCAYO
82336	11	INGRESO AL SISTEMA	2020-07-07 15:38:52	ADMINIPUCALLPA	PUCALLPA
82337	4	INGRESO AL SISTEMA	2020-07-08 09:07:19	DESKTOP-6HII7R6	HUANCAYO
82338	4	INGRESO AL SISTEMA	2020-07-08 12:34:24	DESKTOP-6HII7R6	HUANCAYO
82339	2	INGRESO AL SISTEMA	2020-07-08 12:43:49	DESKTOP-2COHNSG	HUANCAYO
82340	9	INGRESO AL SISTEMA	2020-07-08 12:52:43	DESKTOP-2COHNSG	HUANCAYO
82341	11	INGRESO AL SISTEMA	2020-07-08 15:32:45	ADMINIPUCALLPA	PUCALLPA
82342	11	INGRESO AL SISTEMA	2020-07-08 21:12:31	ADMINIPUCALLPA	PUCALLPA
82343	9	INGRESO AL SISTEMA	2020-07-08 21:59:23	ADMINIPUCALLPA	PUCALLPA
82344	2	INGRESO AL SISTEMA	2020-07-08 22:14:44	DJ-XAVEX	HUANCAYO
82345	9	INGRESO AL SISTEMA	2020-07-08 22:22:46	DJ-XAVEX	HUANCAYO
82346	9	INGRESO AL SISTEMA	2020-07-08 23:49:59	ADMINIPUCALLPA	PUCALLPA
82347	5	INGRESO AL SISTEMA	2020-07-09 09:32:16	DESKTOP-PDVNOOT	HUANCAYO
82348	5	INGRESO AL SISTEMA	2020-07-09 09:50:57	DESKTOP-PDVNOOT	HUANCAYO
82349	6	INGRESO AL SISTEMA	2020-07-09 10:25:03	DESKTOP-2COHNSG	HUANCAYO
82350	11	INGRESO AL SISTEMA	2020-07-09 10:50:09	ADMINIPUCALLPA	PUCALLPA
82351	4	INGRESO AL SISTEMA	2020-07-09 11:50:19	DESKTOP-6HII7R6	HUANCAYO
82352	2	INGRESO AL SISTEMA	2020-07-09 12:08:36	DESKTOP-6HII7R6	HUANCAYO
82353	9	INGRESO AL SISTEMA	2020-07-09 12:17:16	ADMINIPUCALLPA	PUCALLPA
82354	9	INGRESO AL SISTEMA	2020-07-09 12:34:28	ADMINIPUCALLPA	PUCALLPA
82355	5	INGRESO AL SISTEMA	2020-07-10 08:58:44	DESKTOP-PDVNOOT	HUANCAYO
82356	4	INGRESO AL SISTEMA	2020-07-10 09:23:39	DESKTOP-6HII7R6	HUANCAYO
82357	9	INGRESO AL SISTEMA	2020-07-10 11:36:11	ADMINIPUCALLPA	PUCALLPA
82358	2	INGRESO AL SISTEMA	2020-07-10 11:41:50	DESKTOP-2COHNSG	HUANCAYO
82359	4	INGRESO AL SISTEMA	2020-07-10 13:09:46	DESKTOP-6HII7R6	HUANCAYO
82360	5	INGRESO AL SISTEMA	2020-07-10 13:21:24	DESKTOP-PDVNOOT	HUANCAYO
82361	11	INGRESO AL SISTEMA	2020-07-10 19:44:33	ADMINIPUCALLPA	PUCALLPA
82362	11	INGRESO AL SISTEMA	2020-07-10 21:10:47	ADMINIPUCALLPA	PUCALLPA
82363	11	INGRESO AL SISTEMA	2020-07-10 21:22:05	ADMINIPUCALLPA	PUCALLPA
82364	9	INGRESO AL SISTEMA	2020-07-10 22:06:49	ADMINIPUCALLPA	PUCALLPA
82365	2	INGRESO AL SISTEMA	2020-07-10 22:07:05	DJ-XAVEX	HUANCAYO
82366	11	INGRESO AL SISTEMA	2020-07-10 22:35:04	ADMINIPUCALLPA	PUCALLPA
82367	4	INGRESO AL SISTEMA	2020-07-11 09:18:29	DESKTOP-6HII7R6	HUANCAYO
82368	2	INGRESO AL SISTEMA	2020-07-11 09:23:06	DESKTOP-6HII7R6	HUANCAYO
82369	5	INGRESO AL SISTEMA	2020-07-11 09:25:38	DESKTOP-PDVNOOT	HUANCAYO
82370	11	INGRESO AL SISTEMA	2020-07-11 18:06:29	ADMINIPUCALLPA	PUCALLPA
82371	11	INGRESO AL SISTEMA	2020-07-11 18:15:29	ADMINIPUCALLPA	PUCALLPA
82372	11	INGRESO AL SISTEMA	2020-07-11 18:18:53	ADMINIPUCALLPA	PUCALLPA
82373	9	INGRESO AL SISTEMA	2020-07-11 18:22:30	ADMINIPUCALLPA	PUCALLPA
82374	4	INGRESO AL SISTEMA	2020-07-13 09:08:02	DESKTOP-6HII7R6	HUANCAYO
82375	5	INGRESO AL SISTEMA	2020-07-13 09:42:50	DESKTOP-PDVNOOT	HUANCAYO
82376	2	INGRESO AL SISTEMA	2020-07-13 10:15:59	DESKTOP-2COHNSG	HUANCAYO
82377	5	INGRESO AL SISTEMA	2020-07-13 11:04:15	DESKTOP-PDVNOOT	HUANCAYO
82378	9	INGRESO AL SISTEMA	2020-07-13 11:27:05	ADMINIPUCALLPA	PUCALLPA
82379	5	INGRESO AL SISTEMA	2020-07-13 13:57:31	DESKTOP-6HII7R6	HUANCAYO
82380	4	INGRESO AL SISTEMA	2020-07-13 13:58:33	DESKTOP-6HII7R6	HUANCAYO
82381	4	INGRESO AL SISTEMA	2020-07-13 14:17:04	DESKTOP-6HII7R6	HUANCAYO
82382	11	INGRESO AL SISTEMA	2020-07-13 16:11:03	ADMINIPUCALLPA	PUCALLPA
82383	9	INGRESO AL SISTEMA	2020-07-13 16:11:28	ADMINIPUCALLPA	PUCALLPA
82384	5	INGRESO AL SISTEMA	2020-07-14 09:01:35	DESKTOP-PDVNOOT	HUANCAYO
82385	2	INGRESO AL SISTEMA	2020-07-14 09:35:55	DESKTOP-2COHNSG	HUANCAYO
82386	4	INGRESO AL SISTEMA	2020-07-14 09:54:30	DESKTOP-6HII7R6	HUANCAYO
82387	4	INGRESO AL SISTEMA	2020-07-14 11:56:33	DESKTOP-6HII7R6	HUANCAYO
82388	4	INGRESO AL SISTEMA	2020-07-14 11:59:20	DESKTOP-6HII7R6	HUANCAYO
82389	5	INGRESO AL SISTEMA	2020-07-14 12:18:25	DESKTOP-PDVNOOT	HUANCAYO
82390	4	INGRESO AL SISTEMA	2020-07-14 13:16:07	DESKTOP-6HII7R6	HUANCAYO
82391	9	INGRESO AL SISTEMA	2020-07-14 15:53:02	ADMINIPUCALLPA	PUCALLPA
82392	4	INGRESO AL SISTEMA	2020-07-15 09:03:43	DESKTOP-6HII7R6	HUANCAYO
82393	5	INGRESO AL SISTEMA	2020-07-15 09:05:21	DESKTOP-PDVNOOT	HUANCAYO
82394	5	INGRESO AL SISTEMA	2020-07-15 10:29:54	DESKTOP-PDVNOOT	HUANCAYO
82395	6	INGRESO AL SISTEMA	2020-07-15 11:29:20	DESKTOP-2COHNSG	HUANCAYO
82396	9	INGRESO AL SISTEMA	2020-07-15 15:52:16	ADMINIPUCALLPA	PUCALLPA
82397	5	INGRESO AL SISTEMA	2020-07-16 09:10:12	DESKTOP-PDVNOOT	HUANCAYO
82398	4	INGRESO AL SISTEMA	2020-07-16 09:15:04	DESKTOP-6HII7R6	HUANCAYO
82399	2	INGRESO AL SISTEMA	2020-07-16 09:38:48	DESKTOP-2COHNSG	HUANCAYO
82400	2	INGRESO AL SISTEMA	2020-07-16 12:43:27	DESKTOP-2COHNSG	HUANCAYO
82401	2	INGRESO AL SISTEMA	2020-07-16 13:53:35	DESKTOP-2COHNSG	HUANCAYO
82402	9	INGRESO AL SISTEMA	2020-07-16 19:21:40	ADMINIPUCALLPA	PUCALLPA
82403	4	INGRESO AL SISTEMA	2020-07-17 09:11:30	DESKTOP-6HII7R6	HUANCAYO
82404	5	INGRESO AL SISTEMA	2020-07-17 09:25:05	DESKTOP-PDVNOOT	HUANCAYO
82405	9	INGRESO AL SISTEMA	2020-07-17 18:38:06	ADMINIPUCALLPA	PUCALLPA
82406	9	INGRESO AL SISTEMA	2020-07-17 18:59:01	ADMINIPUCALLPA	PUCALLPA
82407	4	INGRESO AL SISTEMA	2020-07-17 20:46:59	DJ-XAVEX	HUANCAYO
82408	5	INGRESO AL SISTEMA	2020-07-18 09:18:13	DESKTOP-PDVNOOT	HUANCAYO
82409	4	INGRESO AL SISTEMA	2020-07-18 10:45:52	DESKTOP-6HII7R6	HUANCAYO
82410	2	INGRESO AL SISTEMA	2020-07-18 11:14:35	DESKTOP-6HII7R6	HUANCAYO
82411	4	INGRESO AL SISTEMA	2020-07-20 09:18:02	DESKTOP-6HII7R6	HUANCAYO
82412	5	INGRESO AL SISTEMA	2020-07-20 09:45:57	DESKTOP-PDVNOOT	HUANCAYO
82413	4	INGRESO AL SISTEMA	2020-07-20 12:51:24	DESKTOP-2COHNSG	HUANCAYO
82414	4	INGRESO AL SISTEMA	2020-07-20 13:45:42	DESKTOP-6HII7R6	HUANCAYO
82415	9	INGRESO AL SISTEMA	2020-07-20 21:48:45	ADMINIPUCALLPA	PUCALLPA
82416	11	INGRESO AL SISTEMA	2020-07-20 22:05:31	ADMINIPUCALLPA	PUCALLPA
82417	11	INGRESO AL SISTEMA	2020-07-21 10:36:25	ADMINIPUCALLPA	PUCALLPA
82418	9	INGRESO AL SISTEMA	2020-07-21 10:51:36	ADMINIPUCALLPA	PUCALLPA
82419	2	INGRESO AL SISTEMA	2020-07-21 19:52:24	DJ-XAVEX	HUANCAYO
82420	5	INGRESO AL SISTEMA	2020-07-22 09:15:01	DESKTOP-PDVNOOT	HUANCAYO
82421	4	INGRESO AL SISTEMA	2020-07-22 09:23:08	DESKTOP-6HII7R6	HUANCAYO
82422	5	INGRESO AL SISTEMA	2020-07-22 11:07:32	DESKTOP-6HII7R6	HUANCAYO
82423	6	INGRESO AL SISTEMA	2020-07-22 11:12:17	DESKTOP-6HII7R6	HUANCAYO
82424	4	INGRESO AL SISTEMA	2020-07-22 11:28:11	DESKTOP-6HII7R6	HUANCAYO
82425	9	INGRESO AL SISTEMA	2020-07-22 12:48:12	ADMINIPUCALLPA	PUCALLPA
82426	11	INGRESO AL SISTEMA	2020-07-22 19:29:39	ADMINIPUCALLPA	PUCALLPA
82427	9	INGRESO AL SISTEMA	2020-07-22 19:50:47	ADMINIPUCALLPA	PUCALLPA
82428	2	INGRESO AL SISTEMA	2020-07-22 22:23:05	DJ-XAVEX	HUANCAYO
82429	4	INGRESO AL SISTEMA	2020-07-23 09:17:23	DESKTOP-6HII7R6	HUANCAYO
82430	2	INGRESO AL SISTEMA	2020-07-23 09:19:56	DESKTOP-6HII7R6	HUANCAYO
82431	3	INGRESO AL SISTEMA	2020-07-23 10:59:47	DESKTOP-PDVNOOT	HUANCAYO
82432	2	INGRESO AL SISTEMA	2020-07-23 12:03:14	DESKTOP-6HII7R6	HUANCAYO
82433	2	INGRESO AL SISTEMA	2020-07-23 13:38:49	DESKTOP-6HII7R6	HUANCAYO
82434	9	INGRESO AL SISTEMA	2020-07-23 18:13:01	ADMINIPUCALLPA	PUCALLPA
82435	4	INGRESO AL SISTEMA	2020-07-24 09:56:34	DESKTOP-6HII7R6	HUANCAYO
82436	4	INGRESO AL SISTEMA	2020-07-24 10:31:35	DESKTOP-6HII7R6	HUANCAYO
82437	2	INGRESO AL SISTEMA	2020-07-24 13:34:43	DESKTOP-6HII7R6	HUANCAYO
82438	4	INGRESO AL SISTEMA	2020-07-24 13:37:46	DESKTOP-6HII7R6	HUANCAYO
82439	2	INGRESO AL SISTEMA	2020-07-24 13:43:28	DESKTOP-6HII7R6	HUANCAYO
82440	4	INGRESO AL SISTEMA	2020-07-24 13:45:57	DESKTOP-6HII7R6	HUANCAYO
82441	4	INGRESO AL SISTEMA	2020-07-25 12:01:05	DESKTOP-6HII7R6	HUANCAYO
82442	9	INGRESO AL SISTEMA	2020-07-25 17:14:34	ADMINIPUCALLPA	PUCALLPA
82443	4	INGRESO AL SISTEMA	2020-07-27 11:32:41	DESKTOP-6HII7R6	HUANCAYO
82444	9	INGRESO AL SISTEMA	2020-07-29 16:11:58	ADMINIPUCALLPA	PUCALLPA
82445	11	INGRESO AL SISTEMA	2020-07-30 15:01:18	ADMINIPUCALLPA	PUCALLPA
82446	9	INGRESO AL SISTEMA	2020-07-30 15:34:16	ADMINIPUCALLPA	PUCALLPA
82447	11	INGRESO AL SISTEMA	2020-07-30 15:34:41	ADMINIPUCALLPA	PUCALLPA
82448	9	INGRESO AL SISTEMA	2020-07-31 14:14:05	ADMINIPUCALLPA	PUCALLPA
82449	2	INGRESO AL SISTEMA	2020-07-31 14:19:18	ADMINIPUCALLPA	PUCALLPA
82450	9	INGRESO AL SISTEMA	2020-08-01 18:24:57	ADMINIPUCALLPA	PUCALLPA
82451	11	INGRESO AL SISTEMA	2020-08-01 19:03:31	ADMINIPUCALLPA	PUCALLPA
82452	9	INGRESO AL SISTEMA	2020-08-03 21:36:35	ADMINIPUCALLPA	PUCALLPA
82453	9	INGRESO AL SISTEMA	2020-08-04 20:40:53	ADMINIPUCALLPA	PUCALLPA
82454	9	INGRESO AL SISTEMA	2020-08-04 20:51:19	ADMINIPUCALLPA	PUCALLPA
82455	2	INGRESO AL SISTEMA	2020-08-04 21:41:23	ADMINIPUCALLPA	PUCALLPA
82456	9	INGRESO AL SISTEMA	2020-08-05 21:11:40	ADMINIPUCALLPA	PUCALLPA
82457	9	INGRESO AL SISTEMA	2020-08-06 18:08:56	ADMINIPUCALLPA	PUCALLPA
82458	11	INGRESO AL SISTEMA	2020-08-06 18:15:11	ADMINIPUCALLPA	PUCALLPA
82459	2	INGRESO AL SISTEMA	2020-08-06 21:35:22	ADMINIPUCALLPA	PUCALLPA
82460	9	INGRESO AL SISTEMA	2020-08-07 09:22:29	ADMINIPUCALLPA	PUCALLPA
82461	9	INGRESO AL SISTEMA	2020-08-07 09:25:32	ADMINIPUCALLPA	PUCALLPA
82462	11	INGRESO AL SISTEMA	2020-08-07 16:29:48	ADMINIPUCALLPA	PUCALLPA
82463	9	INGRESO AL SISTEMA	2020-08-07 17:01:13	ADMINIPUCALLPA	PUCALLPA
82464	11	INGRESO AL SISTEMA	2020-08-07 17:31:12	ADMINIPUCALLPA	PUCALLPA
82465	2	INGRESO AL SISTEMA	2020-08-07 20:51:02	ADMINIPUCALLPA	PUCALLPA
82466	9	INGRESO AL SISTEMA	2020-08-08 12:08:32	ADMINIPUCALLPA	PUCALLPA
82467	4	INGRESO AL SISTEMA	2020-08-10 08:52:36	DESKTOP-6HII7R6	HUANCAYO
82468	5	INGRESO AL SISTEMA	2020-08-10 09:08:29	DESKTOP-PDVNOOT	HUANCAYO
82469	5	INGRESO AL SISTEMA	2020-08-10 09:30:48	DESKTOP-PDVNOOT	HUANCAYO
82470	5	INGRESO AL SISTEMA	2020-08-10 10:10:38	DESKTOP-PDVNOOT	HUANCAYO
82471	9	INGRESO AL SISTEMA	2020-08-10 13:10:19	ADMINIPUCALLPA	PUCALLPA
82472	9	INGRESO AL SISTEMA	2020-08-10 18:55:53	ADMINIPUCALLPA	PUCALLPA
82473	4	INGRESO AL SISTEMA	2020-08-11 09:17:32	DESKTOP-6HII7R6	HUANCAYO
82474	5	INGRESO AL SISTEMA	2020-08-11 09:27:51	DESKTOP-PDVNOOT	HUANCAYO
82475	9	INGRESO AL SISTEMA	2020-08-11 17:43:11	ADMINIPUCALLPA	PUCALLPA
82476	4	INGRESO AL SISTEMA	2020-08-12 09:26:38	DESKTOP-6HII7R6	HUANCAYO
82477	5	INGRESO AL SISTEMA	2020-08-12 10:18:44	DESKTOP-PDVNOOT	HUANCAYO
82478	4	INGRESO AL SISTEMA	2020-08-12 12:40:39	DESKTOP-6HII7R6	HUANCAYO
82479	4	INGRESO AL SISTEMA	2020-08-14 09:01:37	DESKTOP-6HII7R6	HUANCAYO
82480	5	INGRESO AL SISTEMA	2020-08-14 09:14:29	DESKTOP-PDVNOOT	HUANCAYO
82481	9	INGRESO AL SISTEMA	2020-08-14 18:00:08	ADMINIPUCALLPA	PUCALLPA
82482	4	INGRESO AL SISTEMA	2020-08-18 09:20:30	DESKTOP-6HII7R6	HUANCAYO
82483	5	INGRESO AL SISTEMA	2020-08-18 10:32:27	DESKTOP-PDVNOOT	HUANCAYO
82484	9	INGRESO AL SISTEMA	2020-08-18 17:50:46	ADMINIPUCALLPA	PUCALLPA
82485	5	INGRESO AL SISTEMA	2020-08-19 09:43:58	DESKTOP-PDVNOOT	HUANCAYO
82486	6	INGRESO AL SISTEMA	2020-08-19 10:05:39	DESKTOP-PDVNOOT	HUANCAYO
82487	5	INGRESO AL SISTEMA	2020-08-19 12:33:35	DESKTOP-PDVNOOT	HUANCAYO
82488	4	INGRESO AL SISTEMA	2020-08-20 09:29:36	DESKTOP-6HII7R6	HUANCAYO
82489	5	INGRESO AL SISTEMA	2020-08-21 09:46:56	DESKTOP-PDVNOOT	HUANCAYO
82490	4	INGRESO AL SISTEMA	2020-08-22 09:36:55	DESKTOP-6HII7R6	HUANCAYO
82491	9	INGRESO AL SISTEMA	2020-08-23 10:27:57	ADMINIPUCALLPA	PUCALLPA
82492	9	INGRESO AL SISTEMA	2020-08-23 10:50:41	ADMINIPUCALLPA	PUCALLPA
82493	4	INGRESO AL SISTEMA	2020-08-24 09:30:24	DESKTOP-6HII7R6	HUANCAYO
82494	4	INGRESO AL SISTEMA	2020-08-24 13:35:13	DESKTOP-6HII7R6	HUANCAYO
\.


--
-- Data for Name: historial_visitas; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.historial_visitas (id_historial_visitas, dni, tipo_cliente, nombre_completo, fecha_hora_visita, operacion_realizar, id_usuario, credito, ahorro, detalle_operacion, sacar_cita, cita_analista, resultado_cita, fecha_cita_realizada, fecha_acitar, blokear, agencia, id_agencia) FROM stdin;
7927	40129120	\N		2020-06-28 10:53:15.137	REGISTRADO	1	\N	\N	FALTA VERIFICAACION DOMICILIARIA	f	\N	\N	\N	\N	\N	\N	\N
7928	71872891	\N		2020-06-28 10:55:37.234	REGISTRADO	1	\N	\N	ACTUALIZAR VERIFICACION	f	\N	\N	\N	\N	\N	\N	\N
7929	44974563	\N		2020-06-28 10:59:17.575	REGISTRADO	1	\N	\N	VERIFICACION CONSTANTE	f	\N	\N	\N	\N	\N	\N	\N
7930	19995968	\N		2020-06-28 11:04:17.879	REGISTRADO	1	\N	\N	VERIFICAICION ACTUALIZADA	f	\N	\N	\N	\N	\N	\N	\N
7931	76290456	\N		2020-06-28 11:28:31.939	REGISTRADO	1	\N	\N		f	\N	\N	\N	\N	\N	\N	\N
7932	74608015	\N		2020-06-28 11:31:37.24	REGISTRADO	1	\N	\N		f	\N	\N	\N	\N	\N	\N	\N
7933	46196324	\N		2020-06-28 11:48:59.194	REGISTRADO	1	\N	\N		f	\N	\N	\N	\N	\N	\N	\N
7934	44550477	\N		2020-06-28 11:51:13.254	REGISTRADO	1	\N	\N		f	\N	\N	\N	\N	\N	\N	\N
7935	19940774	\N		2020-06-28 11:54:40.213	REGISTRADO	1	\N	\N		f	\N	\N	\N	\N	\N	\N	\N
7936	47475777	\N		2020-06-28 11:57:49.317	REGISTRADO	1	\N	\N		f	\N	\N	\N	\N	\N	\N	\N
7937	47475777	\N		2020-06-28 11:58:00.546	MODIFICO DATOS	1	\N	\N		f	\N	\N	\N	\N	\N	\N	\N
7938	44892319	\N		2020-06-28 16:48:13.481	REGISTRADO	1	\N	\N	NINGUNA	f	\N	\N	\N	\N	\N	\N	\N
7939	40129120	\N		2020-06-29 00:01:22.709	REGISTRADO	2	\N	\N	NO	f	\N	\N	\N	\N	\N	\N	\N
7940	46174277	\N	TICSE	2020-06-29 15:41:54.283	REGISTRADO	3	\N	\N		f	\N	\N	\N	\N	\N	\N	\N
7941	42223746	\N	VALDERRAMA	2020-07-01 11:24:58.54	REGISTRADO	11	\N	\N		f	\N	\N	\N	\N	\N	\N	\N
7942	00061656	\N		2020-07-01 18:57:54.612	REGISTRADO	11	\N	\N		f	\N	\N	\N	\N	\N	\N	\N
7943	42148384	\N	SAJAMI	2020-07-01 20:51:18.777	REGISTRADO	11	\N	\N		f	\N	\N	\N	\N	\N	\N	\N
7944	48742387	\N	SAJAMI VASQUEZ ERIKA LUCIA	2020-07-01 20:55:02.371	REGISTRADO	11	\N	\N		f	\N	\N	\N	\N	\N	\N	\N
7945	00093054	\N	CORAL	2020-07-01 21:01:57.739	REGISTRADO	11	\N	\N		f	\N	\N	\N	\N	\N	\N	\N
7946	47795561	\N	SABOYA	2020-07-01 22:30:18.851	REGISTRADO	11	\N	\N		f	\N	\N	\N	\N	\N	\N	\N
7947	00054338	\N	DEL AGUILA	2020-07-01 22:37:01.976	REGISTRADO	11	\N	\N		f	\N	\N	\N	\N	\N	\N	\N
7948	00098119	\N	URQUIA LOPEZ ROSA LUCIA	2020-07-01 22:43:40.94	MODIFICO DATOS	2	\N	\N	obser	f	\N	\N	\N	\N	\N	\N	\N
7949	00061656	\N	VELA	2020-07-01 22:46:02.922	MODIFICO DATOS	11	\N	\N		f	\N	\N	\N	\N	\N	\N	\N
7950	48742387	\N		2020-07-01 22:57:53.782	MODIFICO DATOS	11	\N	\N		f	\N	\N	\N	\N	\N	\N	\N
7951	42148384	\N		2020-07-01 23:01:16.476	MODIFICO DATOS	11	\N	\N		f	\N	\N	\N	\N	\N	\N	\N
7952	71340438	\N		2020-07-01 23:02:59.815	MODIFICO DATOS	11	\N	\N	obser	f	\N	\N	\N	\N	\N	\N	\N
7953	00098119	\N		2020-07-01 23:07:09.879	MODIFICO DATOS	11	\N	\N		f	\N	\N	\N	\N	\N	\N	\N
7954	42223746	\N	VALDERRAMA RAMIREZ RONNY	2020-07-01 23:11:49.32	MODIFICO DATOS	11	\N	\N		f	\N	\N	\N	\N	\N	\N	\N
7955	05213767	\N	VALDERRAMA RAMIREZ RONNY	2020-07-01 23:14:56.204	MODIFICO DATOS	11	\N	\N		f	\N	\N	\N	\N	\N	\N	\N
7956	00074623	\N	VALDERRAMA RAMIREZ RONNY	2020-07-01 23:17:13.828	MODIFICO DATOS	11	\N	\N		f	\N	\N	\N	\N	\N	\N	\N
7957	80304182	\N	VALDERRAMA RAMIREZ RONNY	2020-07-01 23:19:25.212	MODIFICO DATOS	11	\N	\N		f	\N	\N	\N	\N	\N	\N	\N
7958	43525113	\N	VALDERRAMA RAMIREZ RONNY	2020-07-01 23:21:19.862	MODIFICO DATOS	11	\N	\N		f	\N	\N	\N	\N	\N	\N	\N
7959	63693045	\N	VALDERRAMA RAMIREZ RONNY	2020-07-01 23:23:14.229	MODIFICO DATOS	11	\N	\N		f	\N	\N	\N	\N	\N	\N	\N
7960	21142620	\N	VALDERRAMA RAMIREZ RONNY	2020-07-01 23:25:22.707	MODIFICO DATOS	11	\N	\N		f	\N	\N	\N	\N	\N	\N	\N
7961	00053944	\N		2020-07-01 23:30:15.681	MODIFICO DATOS	11	\N	\N		f	\N	\N	\N	\N	\N	\N	\N
7962	47795561	\N	SABOYA AHUANARI MARVIN HAGLER	2020-07-02 08:49:06.385	MODIFICO DATOS	11	\N	\N		f	\N	\N	\N	\N	\N	\N	\N
7963	00054338	\N	SABOYA AHUANARI MARVIN HAGLER	2020-07-02 08:50:05.983	MODIFICO DATOS	11	\N	\N		f	\N	\N	\N	\N	\N	\N	\N
7964	20107243	\N		2020-07-02 09:17:53.643	REGISTRADO	2	\N	\N	NINGUNA	f	\N	\N	\N	\N	\N	\N	\N
7965	80443299	\N	AHUANARI	2020-07-02 09:41:53.533	REGISTRADO	11	\N	\N		f	\N	\N	\N	\N	\N	\N	\N
7966	00088264	\N		2020-07-02 09:48:25.536	REGISTRADO	11	\N	\N		f	\N	\N	\N	\N	\N	\N	\N
7967	00011627	\N	FLORES	2020-07-02 09:53:42.974	REGISTRADO	11	\N	\N		f	\N	\N	\N	\N	\N	\N	\N
7968	40001397	\N		2020-07-02 09:56:55.24	REGISTRADO	11	\N	\N		f	\N	\N	\N	\N	\N	\N	\N
7969	80487983	\N	LOZANO	2020-07-02 10:00:33.466	REGISTRADO	11	\N	\N		f	\N	\N	\N	\N	\N	\N	\N
7970	40410005	\N	ESPINOZA	2020-07-02 10:06:00.679	REGISTRADO	11	\N	\N		f	\N	\N	\N	\N	\N	\N	\N
7971	00125733	\N	RENGIFO CABRERA SANDRA	2020-07-02 10:21:57.298	MODIFICO DATOS	11	\N	\N		f	\N	\N	\N	\N	\N	\N	\N
7972	71004511	\N	SANCHEZ	2020-07-02 10:25:34.903	REGISTRADO	11	\N	\N		f	\N	\N	\N	\N	\N	\N	\N
7973	80131906	\N	MITIVIRI SHAPIAMA ELIZABETH	2020-07-02 10:30:38.277	MODIFICO DATOS	11	\N	\N		f	\N	\N	\N	\N	\N	\N	\N
7974	21148353	\N	VALDERRAMA	2020-07-02 10:35:51.198	REGISTRADO	11	\N	\N		f	\N	\N	\N	\N	\N	\N	\N
7975	08154317	\N	DAVILA	2020-07-02 10:42:48.542	REGISTRADO	11	\N	\N		f	\N	\N	\N	\N	\N	\N	\N
7976	44231768	\N	RIOS	2020-07-02 10:48:18.39	REGISTRADO	11	\N	\N		f	\N	\N	\N	\N	\N	\N	\N
7977	19856332	\N		2020-07-02 11:50:53.211	REGISTRADO	3	\N	\N	CLIENTE ACTIVO	f	\N	\N	\N	\N	\N	\N	\N
7978	20648525	\N	VILLANES REMUZGO ELISEO ELEDINO	2020-07-02 11:53:17	REGISTRADO	3	\N	\N	CLIENTE ACTIVO	f	\N	\N	\N	\N	\N	\N	\N
7979	00016999	\N		2020-07-02 13:03:38.765	REGISTRADO	11	\N	\N		f	\N	\N	\N	\N	\N	\N	\N
7980	00074623	\N	MONTELUISA	2020-07-02 13:07:10.66	MODIFICO DATOS	11	\N	\N		f	\N	\N	\N	\N	\N	\N	\N
7981	05851902	\N	ALAVA	2020-07-02 13:13:25.68	REGISTRADO	11	\N	\N		f	\N	\N	\N	\N	\N	\N	\N
7982	42148384	\N	SAJAMI VASQUEZ ERIKA LUCIA	2020-07-02 13:45:47.627	MODIFICO DATOS	11	\N	\N		f	\N	\N	\N	\N	\N	\N	\N
7983	05392064	\N	RODRIGEZ	2020-07-02 13:51:05.637	REGISTRADO	11	\N	\N		f	\N	\N	\N	\N	\N	\N	\N
7984	00129259	\N	RUIZ	2020-07-02 14:02:24.83	REGISTRADO	11	\N	\N		f	\N	\N	\N	\N	\N	\N	\N
7985	48826249	\N	VASQUEZ	2020-07-02 14:16:08.622	REGISTRADO	11	\N	\N		f	\N	\N	\N	\N	\N	\N	\N
7986	48826249	\N	VASQUEZ	2020-07-02 14:16:25.962	MODIFICO DATOS	11	\N	\N		f	\N	\N	\N	\N	\N	\N	\N
7987	46202948	\N	CAMPOS	2020-07-02 20:30:34.352	REGISTRADO	11	\N	\N		f	\N	\N	\N	\N	\N	\N	\N
7988	40096014	\N	CRISPIN 	2020-07-02 20:49:38.326	REGISTRADO	11	\N	\N		f	\N	\N	\N	\N	\N	\N	\N
7989	00042309	\N	PACAYA	2020-07-02 20:57:35.811	REGISTRADO	11	\N	\N		f	\N	\N	\N	\N	\N	\N	\N
7990	46801233	\N	SHUÑA	2020-07-02 21:10:44.986	REGISTRADO	11	\N	\N		f	\N	\N	\N	\N	\N	\N	\N
7991	00071106	\N	REVIER	2020-07-02 21:16:37.15	REGISTRADO	11	\N	\N		f	\N	\N	\N	\N	\N	\N	\N
7992	45443553	\N		2020-07-02 21:26:42.26	REGISTRADO	11	\N	\N		f	\N	\N	\N	\N	\N	\N	\N
7993	00024673	\N	ISLA	2020-07-02 22:01:51.408	REGISTRADO	11	\N	\N		f	\N	\N	\N	\N	\N	\N	\N
7994	00124443	\N		2020-07-02 22:03:50.989	REGISTRADO	11	\N	\N		f	\N	\N	\N	\N	\N	\N	\N
7995	00089237	\N	NUÑES	2020-07-02 22:06:38.7	REGISTRADO	11	\N	\N		f	\N	\N	\N	\N	\N	\N	\N
7996	62969316	\N	CAHUAZA	2020-07-02 22:14:56.959	REGISTRADO	11	\N	\N		f	\N	\N	\N	\N	\N	\N	\N
7997	62969316	\N	CAHUAZA NUÑES VIVIAN NIEVEZZ	2020-07-02 22:15:14.219	MODIFICO DATOS	11	\N	\N		f	\N	\N	\N	\N	\N	\N	\N
7998	00015934	\N	PEREZ	2020-07-02 22:32:55.31	REGISTRADO	11	\N	\N		f	\N	\N	\N	\N	\N	\N	\N
7999	71048416	\N		2020-07-02 22:37:36.959	REGISTRADO	11	\N	\N		f	\N	\N	\N	\N	\N	\N	\N
8000	81054501	\N	LLACTA	2020-07-02 22:42:17.971	REGISTRADO	11	\N	\N		f	\N	\N	\N	\N	\N	\N	\N
8001	00098331	\N	YUDICHI	2020-07-02 22:45:10.525	REGISTRADO	11	\N	\N		f	\N	\N	\N	\N	\N	\N	\N
8002	62969315	\N	CAHUAZA	2020-07-02 22:48:58.988	REGISTRADO	11	\N	\N		f	\N	\N	\N	\N	\N	\N	\N
8003	45793094	\N	CAHUAZA 	2020-07-02 22:51:12.283	REGISTRADO	11	\N	\N		f	\N	\N	\N	\N	\N	\N	\N
8004	45838625	\N	VASQUEZ	2020-07-02 22:56:42.152	REGISTRADO	11	\N	\N		f	\N	\N	\N	\N	\N	\N	\N
8005	08976036	\N	FARIAS	2020-07-03 09:04:03.2	REGISTRADO	11	\N	\N		f	\N	\N	\N	\N	\N	\N	\N
8006	05282723	\N	ZAMORA	2020-07-03 09:07:50.841	REGISTRADO	11	\N	\N		f	\N	\N	\N	\N	\N	\N	\N
8007	41014343	\N	SHAPIAMA	2020-07-03 09:22:50.96	REGISTRADO	11	\N	\N		f	\N	\N	\N	\N	\N	\N	\N
8008	00045241	\N	PINEDO	2020-07-03 09:52:29.53	REGISTRADO	11	\N	\N		f	\N	\N	\N	\N	\N	\N	\N
8009	77161207	\N	PEREZ	2020-07-03 13:11:23.424	REGISTRADO	5	\N	\N		f	\N	\N	\N	\N	\N	\N	\N
8010	72769249	\N	VILLANO	2020-07-03 10:20:30.387	REGISTRADO	3	\N	\N	actualizar fecha de nacimiento y numero de celular	f	\N	\N	\N	\N	\N	\N	\N
8011	42775833	\N		2020-07-03 10:28:30.39	REGISTRADO	11	\N	\N		f	\N	\N	\N	\N	\N	\N	\N
8012	42775833	\N	ZUÑIGA RAMIRES DE MACEDO DORIS LLENY	2020-07-03 10:29:04.158	MODIFICO DATOS	11	\N	\N		f	\N	\N	\N	\N	\N	\N	\N
8013	44561695	\N	FLORES	2020-07-03 10:36:00.625	REGISTRADO	11	\N	\N		f	\N	\N	\N	\N	\N	\N	\N
8014	76018133	\N	PACAYA	2020-07-03 10:41:41.954	REGISTRADO	11	\N	\N		f	\N	\N	\N	\N	\N	\N	\N
8015	18071809	\N	CHAVEZ	2020-07-03 10:46:11.28	REGISTRADO	11	\N	\N		f	\N	\N	\N	\N	\N	\N	\N
8016	05380358	\N	PACAYA	2020-07-03 10:51:22.129	REGISTRADO	11	\N	\N		f	\N	\N	\N	\N	\N	\N	\N
8017	44554443	\N	LOPEZ	2020-07-03 11:33:10.185	REGISTRADO	11	\N	\N		f	\N	\N	\N	\N	\N	\N	\N
8018	46073602	\N		2020-07-03 11:37:30.406	REGISTRADO	11	\N	\N		f	\N	\N	\N	\N	\N	\N	\N
8019	71657173	\N		2020-07-03 11:41:40.382	REGISTRADO	11	\N	\N		f	\N	\N	\N	\N	\N	\N	\N
8020	00087078	\N	RENGIFO 	2020-07-03 11:45:05.641	REGISTRADO	11	\N	\N		f	\N	\N	\N	\N	\N	\N	\N
8021	00087078	\N	RENGIFO	2020-07-03 11:45:38.188	MODIFICO DATOS	11	\N	\N		f	\N	\N	\N	\N	\N	\N	\N
8022	00074706	\N	VARGAS	2020-07-03 11:53:40.721	REGISTRADO	11	\N	\N		f	\N	\N	\N	\N	\N	\N	\N
8023	20077264	\N	CARRION	2020-07-03 11:56:12.614	REGISTRADO	3	\N	\N	ACTUALIZAR DATOS DE FECHA DE NACIEMIENTO Y NUMERO MDE CELULAR\r\n	f	\N	\N	\N	\N	\N	\N	\N
8024	46114191	\N		2020-07-03 11:56:20.68	MODIFICO DATOS	11	\N	\N		f	\N	\N	\N	\N	\N	\N	\N
8025	80491643	\N	SALAS	2020-07-03 11:59:36.865	REGISTRADO	11	\N	\N		f	\N	\N	\N	\N	\N	\N	\N
8026	00124280	\N	SALAS RUIZ DORIS	2020-07-03 12:04:07.193	REGISTRADO	11	\N	\N		f	\N	\N	\N	\N	\N	\N	\N
8027	44771061	\N	MOZOMBITE	2020-07-03 12:06:48.867	REGISTRADO	11	\N	\N		f	\N	\N	\N	\N	\N	\N	\N
8028	41278746	\N	QUISPE ROJAS	2020-07-03 15:07:00.106	REGISTRADO	5	\N	\N		f	\N	\N	\N	\N	\N	\N	\N
8029	21276834	\N	CORDOVA	2020-07-03 12:09:57.562	REGISTRADO	3	\N	\N		f	\N	\N	\N	\N	\N	\N	\N
8030	42223746	\N	RUIZ	2020-07-03 12:11:55.702	MODIFICO DATOS	11	\N	\N		f	\N	\N	\N	\N	\N	\N	\N
8031	00031572	\N	RUIZ	2020-07-03 12:13:51.84	REGISTRADO	11	\N	\N		f	\N	\N	\N	\N	\N	\N	\N
8032	42878173	\N	MUNAYCO	2020-07-03 12:21:10.78	REGISTRADO	11	\N	\N		f	\N	\N	\N	\N	\N	\N	\N
8033	61311977	\N	QUISPE	2020-07-03 12:28:49.928	REGISTRADO	3	\N	\N	FALTA ACTUALIZAR DATOS DE CELULAR Y FECHA DE NACIMIENTO	f	\N	\N	\N	\N	\N	\N	\N
8034	61245494	\N	GONZALES	2020-07-03 12:33:07.705	REGISTRADO	11	\N	\N		f	\N	\N	\N	\N	\N	\N	\N
8035	61245494	\N	GONZALES	2020-07-03 12:33:34.874	MODIFICO DATOS	11	\N	\N		f	\N	\N	\N	\N	\N	\N	\N
8036	45394608	\N	QUISPE	2020-07-03 12:42:11.48	REGISTRADO	3	\N	\N	FALTA ACTUALIZAR FECHA DE NACIMIENTO	f	\N	\N	\N	\N	\N	\N	\N
8037	66661783	\N	SIERRA	2020-07-03 15:59:33.792	REGISTRADO	5	\N	\N		f	\N	\N	\N	\N	\N	\N	\N
8038	45171756	\N	CASTILLO	2020-07-03 13:04:22.537	REGISTRADO	3	\N	\N		f	\N	\N	\N	\N	\N	\N	\N
8039	80535329	\N	QUISPE ROJAS	2020-07-03 16:27:36.123	REGISTRADO	5	\N	\N		f	\N	\N	\N	\N	\N	\N	\N
8040	40144277	\N	CONGORA 	2020-07-03 16:53:19.844	REGISTRADO	5	\N	\N		f	\N	\N	\N	\N	\N	\N	\N
8041	48024565	\N	CONDORI ATAUCUSI JHONY	2020-07-03 14:09:44.172	REGISTRADO	2	\N	\N		f	\N	\N	\N	\N	\N	\N	\N
8042	44642607	\N	LOPEZ	2020-07-03 14:11:24.22	REGISTRADO	11	\N	\N		f	\N	\N	\N	\N	\N	\N	\N
8043	80527354	\N		2020-07-03 14:15:11.132	REGISTRADO	11	\N	\N		f	\N	\N	\N	\N	\N	\N	\N
8044	20064238	\N	CASTILLO	2020-07-03 14:17:06.543	REGISTRADO	3	\N	\N		f	\N	\N	\N	\N	\N	\N	\N
8045	00027982	\N	LUNA	2020-07-03 14:19:13.2	REGISTRADO	11	\N	\N		f	\N	\N	\N	\N	\N	\N	\N
8046	47198091	\N		2020-07-03 14:22:01.163	REGISTRADO	11	\N	\N		f	\N	\N	\N	\N	\N	\N	\N
8047	23212444	\N		2020-07-03 14:31:17.317	REGISTRADO	3	\N	\N		f	\N	\N	\N	\N	\N	\N	\N
8048	46309128	\N	CASTRO PUTAPAÑA ESTEFANY GERMANY	2020-07-03 14:37:23.838	REGISTRADO	11	\N	\N		f	\N	\N	\N	\N	\N	\N	\N
8049	42580578	\N		2020-07-03 14:41:10.961	REGISTRADO	11	\N	\N		f	\N	\N	\N	\N	\N	\N	\N
8050	42158084	\N	QUISPE	2020-07-03 14:43:40.129	REGISTRADO	11	\N	\N		f	\N	\N	\N	\N	\N	\N	\N
8051	00122605	\N		2020-07-03 14:47:09.518	REGISTRADO	11	\N	\N		f	\N	\N	\N	\N	\N	\N	\N
8052	47592383	\N	SANCHEZ	2020-07-03 14:49:56.294	REGISTRADO	11	\N	\N		f	\N	\N	\N	\N	\N	\N	\N
8053	44495821	\N	RENTERA	2020-07-03 14:54:13.748	REGISTRADO	11	\N	\N		f	\N	\N	\N	\N	\N	\N	\N
8054	46543654	\N	LUNA	2020-07-03 14:58:36.46	REGISTRADO	11	\N	\N		f	\N	\N	\N	\N	\N	\N	\N
8055	43886178	\N	GOMEZ	2020-07-03 15:02:59.677	REGISTRADO	11	\N	\N		f	\N	\N	\N	\N	\N	\N	\N
8056	80375811	\N	MEZA	2020-07-03 15:05:52.764	REGISTRADO	11	\N	\N		f	\N	\N	\N	\N	\N	\N	\N
8057	00062784	\N	RUIZ	2020-07-03 15:09:03.74	REGISTRADO	11	\N	\N		f	\N	\N	\N	\N	\N	\N	\N
8058	00092642	\N	GARCIA	2020-07-03 15:15:41.664	REGISTRADO	11	\N	\N		f	\N	\N	\N	\N	\N	\N	\N
8059	46992495	\N	SALAS	2020-07-03 15:33:02.539	REGISTRADO	11	\N	\N		f	\N	\N	\N	\N	\N	\N	\N
8060	21145619	\N	MARUYARI	2020-07-03 16:00:45.965	REGISTRADO	11	\N	\N		f	\N	\N	\N	\N	\N	\N	\N
8061	45433206	\N		2020-07-03 16:04:26.909	REGISTRADO	11	\N	\N		f	\N	\N	\N	\N	\N	\N	\N
8062	00106731	\N	SALINAS	2020-07-03 16:08:23.496	REGISTRADO	11	\N	\N		f	\N	\N	\N	\N	\N	\N	\N
8063	73649262	\N		2020-07-03 16:14:46.29	REGISTRADO	11	\N	\N		f	\N	\N	\N	\N	\N	\N	\N
8064	73649262	\N	NEIRA	2020-07-03 16:15:08.387	MODIFICO DATOS	11	\N	\N		f	\N	\N	\N	\N	\N	\N	\N
8065	80632939	\N	CANALES	2020-07-04 09:25:54.46	REGISTRADO	2	\N	\N		f	\N	\N	\N	\N	\N	\N	\N
8066	70342553	\N	CANALES	2020-07-04 09:27:35.52	REGISTRADO	2	\N	\N		f	\N	\N	\N	\N	\N	\N	\N
8067	43050473	\N		2020-07-04 09:29:24.35	REGISTRADO	2	\N	\N		f	\N	\N	\N	\N	\N	\N	\N
8068	19829998	\N		2020-07-04 09:46:10.628	REGISTRADO	2	\N	\N		f	\N	\N	\N	\N	\N	\N	\N
8069	76240397	\N	ADAUTO	2020-07-04 11:31:30.324	REGISTRADO	3	\N	\N		f	\N	\N	\N	\N	\N	\N	\N
8070	00000000	\N		2020-07-04 13:09:35.513	REGISTRADO	2	\N	\N		f	\N	\N	\N	\N	\N	\N	\N
8071	46706530	\N	C	2020-07-06 13:50:01.614	REGISTRADO	5	\N	\N		f	\N	\N	\N	\N	\N	\N	\N
8072	20047216	\N	MENDOZA	2020-07-06 16:22:00.502	REGISTRADO	5	\N	\N		f	\N	\N	\N	\N	\N	\N	\N
8073	71310459	\N	CAY	2020-07-06 17:14:11.443	REGISTRADO	5	\N	\N		f	\N	\N	\N	\N	\N	\N	\N
8074	00035433	\N		2020-07-07 08:49:41.233	REGISTRADO	11	\N	\N		f	\N	\N	\N	\N	\N	\N	\N
8075	42816850	\N	QUISPE VERA	2020-07-07 12:11:57.634	REGISTRADO	2	\N	\N		f	\N	\N	\N	\N	\N	\N	\N
8076	45938581	\N	QUISPE VERA	2020-07-07 12:13:14.34	REGISTRADO	2	\N	\N		f	\N	\N	\N	\N	\N	\N	\N
8077	77377731	\N	CHUCO	2020-07-07 15:24:25.454	REGISTRADO	5	\N	\N		f	\N	\N	\N	\N	\N	\N	\N
8078	77381267	\N		2020-07-07 15:39:33.317	REGISTRADO	5	\N	\N		f	\N	\N	\N	\N	\N	\N	\N
8079	43219907	\N		2020-07-08 12:46:14.718	REGISTRADO	2	\N	\N		f	\N	\N	\N	\N	\N	\N	\N
8080	23159715	\N	DIAZ	2020-07-08 21:35:36.524	REGISTRADO	11	\N	\N		f	\N	\N	\N	\N	\N	\N	\N
8081	42611489	\N		2020-07-09 09:54:37.667	REGISTRADO	5	\N	\N		f	\N	\N	\N	\N	\N	\N	\N
8082	47323767	\N	HUAMAN 	2020-07-09 10:41:39.911	REGISTRADO	5	\N	\N		f	\N	\N	\N	\N	\N	\N	\N
8083	48522972	\N	PARRA	2020-07-09 13:12:15.887	REGISTRADO	5	\N	\N		f	\N	\N	\N	\N	\N	\N	\N
8084	20079323	\N		2020-07-10 13:51:19.526	REGISTRADO	2	\N	\N		f	\N	\N	\N	\N	\N	\N	\N
8085	44274623	\N		2020-07-10 13:53:25.33	REGISTRADO	2	\N	\N		f	\N	\N	\N	\N	\N	\N	\N
8086	41736927	\N		2020-07-10 13:56:05.952	REGISTRADO	2	\N	\N		f	\N	\N	\N	\N	\N	\N	\N
8087	76663751	\N	RAMOS	2020-07-11 12:48:53.911	REGISTRADO	5	\N	\N		f	\N	\N	\N	\N	\N	\N	\N
8088	20682065	\N		2020-07-13 09:47:47.761	REGISTRADO	5	\N	\N		f	\N	\N	\N	\N	\N	\N	\N
8089	44100810	\N	CABRERA	2020-07-13 12:53:43.29	REGISTRADO	5	\N	\N		f	\N	\N	\N	\N	\N	\N	\N
8090	41614930	\N		2020-07-14 09:38:10.79	REGISTRADO	2	\N	\N		f	\N	\N	\N	\N	\N	\N	\N
8091	71065237	\N	JUSCA	2020-07-14 10:51:17.709	REGISTRADO	5	\N	\N		f	\N	\N	\N	\N	\N	\N	\N
8092	48177932	\N	GARCIA	2020-07-14 11:24:01.184	REGISTRADO	5	\N	\N		f	\N	\N	\N	\N	\N	\N	\N
8093	75400769	\N	VILLANES	2020-07-15 09:25:06.327	REGISTRADO	5	\N	\N		f	\N	\N	\N	\N	\N	\N	\N
8094	19882294	\N	HILARIO	2020-07-15 09:58:44.821	REGISTRADO	5	\N	\N		f	\N	\N	\N	\N	\N	\N	\N
8095	21000001	\N	POMA BLANCAS KETY JANET	2020-07-15 10:46:12.325	REGISTRADO	5	\N	\N		f	\N	\N	\N	\N	\N	\N	\N
8096	44063640	\N	LOLO	2020-07-16 09:22:58.739	REGISTRADO	5	\N	\N		f	\N	\N	\N	\N	\N	\N	\N
8097	20648525	\N	REZMUGO	2020-07-16 10:29:34.895	REGISTRADO	5	\N	\N		f	\N	\N	\N	\N	\N	\N	\N
8098	00035433	\N	PEREZ PEREZ REYNA MARINA	2020-07-22 19:51:37.72	MODIFICO DATOS	9	\N	\N		f	\N	\N	\N	\N	\N	\N	\N
8099	70203921	\N		2020-07-30 15:32:53.957	REGISTRADO	11	\N	\N		f	\N	\N	\N	\N	\N	\N	\N
\.


--
-- Data for Name: ingreso_egreso; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.ingreso_egreso (id_ingreso_egreso, id_caja_apertura_cierre, tipo, monto, solicitante, concepto, confirmar, sustento, fecha_hora_operacion, agencia, id_agencia, tipo_operacion, numero_documento) FROM stdin;
1	3850	1	60	GIRON QUISPE YULY 	COMBUSTIBLE	t	48121718 - de la semana	2020-07-01 13:57:45.026235	\N	1	1	\N
2	3850	2	800	MARTINEZ YUPANQUI FRANK	OTROS	t	47286951 - vta de conmputadora	2020-07-01 18:10:07.517552	\N	1	0	\N
3	3850	1	6000	gerente general - frank martinez	INGRESO A BOVEDA  	t	DNI:47286951	2020-07-01 18:14:27.836147	\N	1	0	\N
4	3852	1	60	MARTINEZ YUPANQUI FRANK	OTROS	t	47286951 - acondicionamiento de local	2020-07-02 08:51:10.354563	\N	1	1	\N
5	3852	2	150	MARTINEZ YUPANQUI FRANK	OTROS	t	47286951 - venta de melamina	2020-07-02 08:52:22.288446	\N	1	0	\N
6	3853	1	14	MARTINEZ YUPANQUI FRANK	OTROS	t	47286951 - COMPRA DE PISARDOR Y ESPONJA	2020-07-03 14:46:28.716068	\N	1	1	\N
7	3853	1	2000	FRANK MARTINEZ	INGRESO A BOVEDA  	t	DNI:47286951	2020-07-03 14:47:55.344783	\N	1	0	\N
8	3854	1	140	MARTINEZ YUPANQUI FRANK	OTROS	t	47286951 - PUEBA MOLECULAR  (MELISSA COVID)	2020-07-04 11:36:35.503697	\N	1	1	\N
9	3855	2	5000	MARTINEZ YUPANQUI FRANK	OTROS	t	47286951 - PARA DESEMBOLSO	2020-07-06 13:11:29.173276	\N	1	0	\N
10	3856	1	20	MARTINEZ YUPANQUI FRANK	SERVICIOS	t	47286951 - camara de moto	2020-07-07 11:09:12.559181	\N	1	1	\N
11	3856	1	10	MARTINEZ YUPANQUI FRANK	OTROS	t	47286951 - mano de obra de cambiado de camara	2020-07-07 11:09:58.008734	\N	1	1	\N
12	3858	1	450	MARTINEZ YUPANQUI FRANK	SERVICIOS	t	47286951 - PAGO DE ALQUILER	2020-07-08 09:30:55.907526	\N	1	1	\N
13	3858	1	10	MARTINEZ YUPANQUI FRANK	SERVICIOS	t	47286951 - PAGO DE AGUA	2020-07-08 10:04:18.125118	\N	1	1	\N
14	3858	1	36	MARTINEZ YUPANQUI FRANK	OTROS	t	47286951 - PAGO DE INTERNET COMPARTIDO POR 18 DIAS	2020-07-08 10:04:57.480793	\N	1	1	\N
15	3858	1	80	MARTINEZ YUPANQUI FRANK	SERVICIOS	t	47286951 - PAGO DEL CUARTO DE ALMACEN	2020-07-08 10:05:27.746735	\N	1	1	\N
16	3858	1	24.6000000000000014	MARTINEZ YUPANQUI FRANK	SERVICIOS	t	47286951 - PAGO DE LUZ	2020-07-08 10:05:55.868013	\N	1	1	\N
17	3858	1	43	MARTINEZ YUPANQUI FRANK	OTROS	t	47286951 - COMPRA DE MANGAS Y PROTECTOR	2020-07-08 12:35:42.699285	\N	1	1	\N
18	3858	1	11	MARTINEZ YUPANQUI FRANK	OTROS	t	47286951 - COMPRA DE AVISOS DE AMERGENCIAS	2020-07-08 12:40:40.547461	\N	1	1	\N
19	3860	1	180	MARTINEZ YUPANQUI FRANK	SERVICIOS	t	47286951 - un millar de folder	2020-07-09 13:35:46.116127	\N	1	1	\N
20	3860	1	50	MARTINEZ YUPANQUI FRANK	OTROS	t	47286951 - adelanto de reitro de letrero	2020-07-09 13:37:27.761662	\N	1	1	\N
21	3860	1	16	MARTINEZ YUPANQUI FRANK	OTROS	t	47286951 - compra de foco	2020-07-09 14:24:59.201998	\N	1	1	\N
22	3862	2	1243	MARTINEZ YUPANQUI FRANK	OTROS	t	47286951 - COBRANZA	2020-07-10 13:38:19.923378	\N	1	0	\N
23	3862	1	4150	gerencia	INGRESO A BOVEDA  	t	DNI:47286951	2020-07-10 14:57:45.507445	\N	1	0	\N
24	3863	1	50	VELA PEREZ KEWIN GILBER	PAGOS	t	76238544 - pago de mudanza	2020-07-10 22:09:36.965335	\N	3	1	\N
25	3863	1	70	VELA PEREZ KEWIN GILBER	SERVICIOS	t	76238544 - tecnico instalacion y configuracion de impresora	2020-07-10 22:10:22.352176	\N	3	1	\N
26	3864	1	115	MARTINEZ YUPANQUI FRANK	ÚTILES_ESCRITORIO	t	47286951 - papel , faster	2020-07-11 10:30:28.115322	\N	1	1	\N
27	3864	1	15000	gerencia	INGRESO A BOVEDA  	t	DNI:47286951	2020-07-11 13:11:07.918675	\N	1	0	\N
28	3866	2	552.299999999999955	IPARRAGUIRRE MARMOLEJO BETSY ERMEE	OTROS	t	48003256 - cobranza	2020-07-13 12:38:45.781827	\N	1	0	\N
29	3866	1	10	IPARRAGUIRRE MARMOLEJO BETSY ERMEE	COMBUSTIBLE	t	48003256 - moto deybi	2020-07-13 12:39:08.883538	\N	1	1	\N
30	3866	1	60	MARTINEZ YUPANQUI FRANK	OTROS	t	47286951 - sacada de letrero	2020-07-13 12:39:29.683799	\N	1	1	\N
31	3866	1	500	MARTINEZ YUPANQUI, FRANK	INGRESO A BOVEDA  	t	DNI:47286951	2020-07-13 14:18:54.700465	\N	1	0	\N
32	3866	1	7000	MARTINEZ  YUPANQUI, FRANK	INGRESO A BOVEDA  	t	DNI:47286951	2020-07-13 14:55:55.015713	\N	1	0	\N
33	3868	2	4329.60000000000036	MARTINEZ YUPANQUI FRANK	OTROS	t	47286951 - habilitacion para desenbolso	2020-07-14 13:41:32.175365	\N	1	0	\N
34	3870	1	7000	GERENCIA	INGRESO A BOVEDA  	t	DNI:47286951	2020-07-15 14:54:43.005607	\N	1	0	\N
35	3872	2	13653	MARTINEZ YUPANQUI FRANK	OTROS	t	47286951 - PARA DESENBOLSO	2020-07-16 14:08:45.562114	\N	1	0	\N
36	3872	1	30	IPARRAGUIRRE MARMOLEJO BETSY ERMEE	COMBUSTIBLE	t	48003256 - COMBUSTIBLE	2020-07-16 14:44:34.409021	\N	1	1	\N
37	3872	1	600	MARTINEZ YUPANQUI FRANK	OTROS	t	47286951 - SISTEMA SOLINTEL	2020-07-16 14:53:21.297363	\N	1	1	\N
38	3872	1	4800	GERENCIA	INGRESO A BOVEDA  	t	DNI:47286951	2020-07-16 14:55:32.336315	\N	1	0	\N
39	3874	1	350	GIRON QUISPE YULY 	PAGOS	t	48121718 - pagoa de luz y agua de local real	2020-07-17 09:20:34.37033	\N	1	1	\N
40	3877	2	1575	MARTINEZ YUPANQUI FRANK	OTROS	t	47286951 - devolcuion	2020-07-20 13:47:00.208928	\N	1	0	\N
41	3877	2	977.799999999999955	MARTINEZ YUPANQUI FRANK	OTROS	t	47286951 - DEVOLUCION PARA CREDITO	2020-07-20 15:03:28.750481	\N	1	0	\N
42	3879	2	55	MARTINEZ YUPANQUI FRANK	OTROS	t	47286951 - venta dde nativa	2020-07-22 09:35:53.393984	\N	1	0	\N
43	3879	2	939.200000000000045	IPARRAGUIRRE MARMOLEJO BETSY ERMEE	OTROS	t	48003256 - ENTRAGO DE COBRANZA DE DEYBI	2020-07-22 14:23:58.465796	\N	1	0	\N
44	3879	1	500	GEREMCIA	INGRESO A BOVEDA  	t	DNI:47286951	2020-07-22 14:24:30.169287	\N	1	0	\N
45	3880	1	30	kewin gilber vela perez	COMBUSTIBLE	t	76238544 - movilidad de   azesor	2020-07-22 20:03:02.451744	\N	3	1	\N
46	3883	2	500	MARTINEZ YUPANQUI FRANK	OTROS	t	47286951 -  devolucion de credito	2020-07-24 10:33:35.664037	\N	1	0	\N
47	3883	1	30	PINEDO CARHUAMACA JOSE LUIS	OTROS	t	45824125 - COMBUSTIBLE	2020-07-24 13:38:19.668291	\N	1	1	\N
48	3883	1	10	MARTINEZ YUPANQUI FRANK	OTROS	t	47286951 - PASAJE	2020-07-24 13:39:35.067016	\N	1	1	\N
49	3883	1	900	GERENCIA	INGRESO A BOVEDA  	t	DNI:47280651	2020-07-24 14:26:49.805692	\N	1	0	\N
50	3886	2	5000	MARTINEZ YUPANQUI FRANK	OTROS	t	47286951 - inyeccion de capital	2020-07-27 11:33:59.676911	\N	1	0	\N
51	3886	2	5000	MARTINEZ YUPANQUI FRANK	OTROS	t	47286951 - INYECCION DE CAPITAL	2020-07-27 09:13:51.207032	\N	1	0	\N
52	3886	1	10000	GERENCIA	INGRESO A BOVEDA  	t	DNI:47286951	2020-07-27 09:14:27.078076	\N	1	0	\N
53	3893	1	30	kewingilber vela perez 	COMBUSTIBLE	t	76238544 - habilitacion de combustible 	2020-08-05 21:32:28.025042	\N	3	1	\N
54	3893	1	120	gilber vela coral	OTROS	t	00061656 - pago de alquiler 	2020-08-05 21:33:15.818946	\N	3	1	\N
55	3899	2	100	MARTINEZ YUPANQUI FRANK	OTROS	t	47286951 - ingreso de gerencia	2020-08-11 13:08:30.955202	\N	1	0	\N
56	3901	2	460	MARTINEZ YUPANQUI FRANK	OTROS	t	47286951 - INGRESO A GERENCIA	2020-08-12 13:46:55.745023	\N	1	0	\N
57	3901	1	20	RIVEROS PUMACAHUA DEIBY	COMBUSTIBLE	t	48190126 - PARA SU CONBUSTIBLE	2020-08-12 13:47:34.446801	\N	1	1	\N
58	3902	1	20	MARTINEZ YUPANQUI FRANK	PAGOS	t	47286951 - pago de agua 	2020-08-14 09:22:25.816844	\N	1	1	\N
59	3902	2	20	MARTINEZ YUPANQUI FRANK	OTROS	t	47286951 - cuadre	2020-08-14 13:24:13.604317	\N	1	0	\N
60	3908	1	30	VELA PEREZ KEWIN GILBER	COMBUSTIBLE	t	76238544 - combustible 	2020-08-23 10:44:31.8764	\N	3	1	\N
61	3909	2	570	MARTINEZ YUPANQUI FRANK	OTROS	t	47286951 - devolucion de ahorros	2020-08-24 13:14:13.043869	\N	1	0	\N
62	3909	1	150	MARTINEZ YUPANQUI FRANK	OTROS	t	47286951 - devolucion de ahorro	2020-08-24 13:35:58.647328	\N	1	1	\N
63	3909	1	38	IPARRAGUIRRE MARMOLEJO BETSY ERMEE	OTROS	t	48003256 - PAGO DE SUELDO	2020-08-24 13:47:29.777324	\N	1	1	\N
\.


--
-- Data for Name: insumo_negocio; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.insumo_negocio (id_insumo_negocio, id_detalle_negocio, cantidad, descripcion, precio, total, usuario, agencia, fecha_hora, estado) FROM stdin;
\.


--
-- Data for Name: inventario_bienes; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.inventario_bienes (id_inventario_bienes, dni, bien, caracteristica, valorizado, usuario, fecha_hora_ingreso, fecha_hora_manipulacion) FROM stdin;
\.


--
-- Data for Name: libreta_ahorro; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.libreta_ahorro (id_libreta_ahorro, dni, nombre_oficina, id_cajera, id_administrador, id_benificiario, id_tipo_ahorro, fecha_apertura, monto_apertura, monto_actual, interes_actual, tiempo_plazo, monto_meta, bloquear, obs, fecha_a_retirar, monto_financiado, fecha_retiro, id_cajera_retiro, tea, interes_generado, monto_retirado, ultimo_movimiento, monto_pactado, dni_benificiario, tipo_campana, fin_campana, producto, usuario_ope, id_analista, usuario_analista, estado, id_agencia, periodo, p_retiro) FROM stdin;
12	40129120	HUANCAYO	\N	6	\N	\N	2020-06-30	0	340	1.73999999999999999	3	0	f		2020-09-28	0	\N	\N	4	0	0	2020-08-18	0	\N	0  	\N	LIBRE	\N	4	BIM	1	1	MESES	0
3	23701995	HUANCAYO	\N	6	\N	\N	2020-06-30	0	400	0	6	0	f		2020-12-27	0	\N	\N	4	0	0	2020-06-30	0	\N	0  	\N	CON INTERES PROGRAMADO	\N	4	BIM	1	1	MESES	180
4	19940774	HUANCAYO	\N	6	\N	\N	2020-06-30	0	15	0	3	0	f		2020-09-28	0	\N	\N	4	0	0	2020-06-30	0	\N	0  	\N	CON INTERES PROGRAMADO	\N	4	BIM	1	1	MESES	90
5	44974563	HUANCAYO	\N	6	\N	\N	2020-06-30	0	0	0	3	0	f		2020-09-28	0	\N	\N	4	0	0	2020-06-30	0	\N	0  	\N	CON INTERES PROGRAMADO	\N	4	BIM	1	1	MESES	90
6	46041015	HUANCAYO	\N	6	\N	\N	2020-06-30	0	0	0	3	0	f		2020-09-28	0	\N	\N	4	0	0	2020-06-30	0	\N	0  	\N	CON INTERES PROGRAMADO	\N	4	BIM	1	1	MESES	90
7	20113864	HUANCAYO	\N	6	\N	\N	2020-06-30	0	0	0	3	0	f		2020-09-28	0	\N	\N	4	0	0	2020-06-30	0	\N	0  	\N	CON INTERES PROGRAMADO	\N	4	BIM	1	1	MESES	90
8	76621787	HUANCAYO	\N	6	\N	\N	2020-06-30	0	0	0	3	0	f		2020-09-28	0	\N	\N	4	0	0	2020-06-30	0	\N	0  	\N	CON INTERES PROGRAMADO	\N	4	BIM	1	1	MESES	90
9	42297478	HUANCAYO	\N	6	\N	\N	2020-06-30	0	0	0	3	0	f		2020-09-28	0	\N	\N	4	0	0	2020-06-30	0	\N	0  	\N	CON INTERES PROGRAMADO	\N	4	BIM	1	1	MESES	90
10	41123341	HUANCAYO	\N	6	\N	\N	2020-06-30	0	0	0	3	0	f		2020-09-28	0	\N	\N	4	0	0	2020-06-30	0	\N	0  	\N	CON INTERES PROGRAMADO	\N	4	BIM	1	1	MESES	90
11	47475777	HUANCAYO	\N	6	\N	\N	2020-06-30	0	0	0	3	0	f		2020-09-28	0	\N	\N	4	0	0	2020-06-30	0	\N	0  	\N	CON INTERES PROGRAMADO	\N	4	BIM	1	1	MESES	90
2	40129120	HUANCAYO	\N	6	\N	\N	2020-06-30	0	300	0	3	0	f		2020-09-28	0	\N	\N	4	0	0	2020-06-30	0	\N	0  	\N	CON INTERES PROGRAMADO	\N	4	BIM	1	1	MESES	90
24	00061656	PUCALLPA	\N	\N	\N	\N	2020-07-01	0	0	0	0	0	f		2020-07-01	0	\N	\N	0	0	0	2020-07-01	0	\N	0  	\N	PLAZO FIJO	\N	9	LARIAS	0	3		0
18	42297478	HUANCAYO	\N	6	\N	\N	2020-06-30	0	355	0	3	0	f		2020-09-28	0	\N	\N	4	0	0	2020-06-30	0	\N	0  	\N	LIBRE	\N	4	BIM	1	1	MESES	0
14	44974563	HUANCAYO	\N	6	\N	\N	2020-06-30	0	0	0	3	0	f		2020-09-28	0	\N	\N	4	0	0	2020-06-30	0	\N	0  	\N	LIBRE	\N	4	BIM	1	1	MESES	0
19	41123341	HUANCAYO	\N	6	\N	\N	2020-06-30	0	603	0.23000000000000001	3	0	f		2020-09-28	0	\N	\N	4	0	0	2020-07-03	0	\N	0  	\N	LIBRE	\N	4	BIM	1	1	MESES	0
20	47475777	HUANCAYO	\N	6	\N	\N	2020-06-30	0	5	0	3	0	f		2020-09-28	0	\N	\N	4	0	0	2020-06-30	0	\N	0  	\N	LIBRE	\N	4	BIM	1	1	MESES	0
33	41123341	HUANCAYO	\N	2	\N	\N	2020-07-03	0	0	0	1	0	f		2020-08-02	0	\N	\N	4	0	0	2020-07-03	0	\N	0  	\N	LIBRE	\N	4	BIM	1	1	MESES	0
13	19940774	HUANCAYO	\N	6	\N	\N	2020-06-30	0	20	0	3	0	f		2020-09-28	0	\N	\N	4	0	0	2020-06-30	0	\N	0  	\N	LIBRE	\N	4	BIM	1	1	MESES	0
15	46041015	HUANCAYO	\N	6	\N	\N	2020-06-30	0	826.100000000000023	0	3	0	f		2020-09-28	0	\N	\N	4	0	0	2020-06-30	0	\N	0  	\N	LIBRE	\N	4	BIM	1	1	MESES	0
16	20113864	HUANCAYO	\N	6	\N	\N	2020-06-30	0	420	0	3	0	f		2020-09-28	0	\N	\N	4	0	0	2020-06-30	0	\N	0  	\N	LIBRE	\N	4	BIM	1	1	MESES	0
17	76621787	HUANCAYO	\N	6	\N	\N	2020-06-30	0	90	0	3	0	f		2020-09-28	0	\N	\N	4	0	0	2020-06-30	0	\N	0  	\N	LIBRE	\N	4	BIM	1	1	MESES	0
25	00061656	PUCALLPA	\N	2	\N	\N	2020-07-01	0	20500	3075	8	0	f		2021-02-26	0	\N	\N	15	0	0	2020-07-01	0	\N	0  	\N	PLAZO FIJO	\N	9	LARIAS	1	3	MESES	0
26	00098119	PUCALLPA	\N	\N	\N	\N	2020-07-01	0	0	0	3	0	f		2023-07-01	0	\N	\N	4	0	0	2020-07-01	0	\N	0  	\N	LIBRE	\N	9	LARIAS	0	3	AÑOS	0
23	45745478	HUANCAYO	\N	6	\N	\N	2020-06-30	0	570.42999999999995	0	1	0	f		2021-06-30	0	\N	\N	4	0	0	2020-06-30	0	\N	0  	\N	LIBRE	\N	4	BIM	1	1	AÑOS	0
22	19942242	HUANCAYO	\N	6	\N	\N	2020-06-30	0	1128.79999999999995	0	1	0	f		2021-06-30	0	\N	\N	4	0	0	2020-06-30	0	\N	0  	\N	LIBRE	\N	4	BIM	1	1	AÑOS	0
21	46561831	HUANCAYO	\N	6	\N	\N	2020-06-30	0	802	0	1	0	f		2021-06-30	0	\N	\N	4	0	0	2020-06-30	0	\N	0  	\N	LIBRE	\N	4	BIM	1	1	AÑOS	0
1	40129120	HUANCAYO	\N	6	\N	\N	2020-06-29	0	0	0	3	0	f		2020-09-27	0	\N	\N	4	0	0	2020-06-29	0	\N	0  	\N	CON INTERES PROGRAMADO	\N	4	BIM	1	1	MESES	0
27	00098119	PUCALLPA	\N	2	\N	\N	2020-07-01	0	0	0	3	0	f		2023-07-01	0	\N	\N	48	0	0	2020-07-01	0	\N	0  	\N	LIBRE	\N	9	LARIAS	1	3	AÑOS	0
31	19856332	HUANCAYO	\N	2	\N	\N	2020-07-02	0	10	0	6	0	f		2020-12-29	0	\N	\N	4	0	0	2020-07-02	0	\N	0  	\N	LIBRE	\N	4	BIM	1	1	MESES	0
30	20107243	HUANCAYO	\N	2	\N	\N	2020-07-02	0	480	0	3	0	f		2020-09-30	0	\N	\N	4	0	0	2020-07-02	0	\N	0  	\N	LIBRE	\N	4	BIM	1	1	MESES	0
28	20107243	HUANCAYO	\N	2	\N	\N	2020-07-02	0	20000	3000	10	0	f		2021-04-28	0	\N	\N	15	0	0	2020-07-02	0	\N	0  	\N	PLAZO FIJO	\N	4	BIM	1	1	MESES	0
29	20107243	HUANCAYO	\N	2	\N	\N	2020-07-02	0	2000	0	1	0	f		2020-08-01	0	\N	\N	0	0	0	2020-07-02	0	\N	0  	\N	LIBRE	\N	4	BIM	1	1	MESES	0
35	19944260	HUANCAYO	\N	2	\N	\N	2020-07-04	0	0	0	1	0	f		2020-07-05	0	\N	\N	4	0	0	2020-07-04	0	\N	0  	\N	LIBRE	\N	4	BIM	1	1	DIAS	0
34	20064201	HUANCAYO	\N	2	\N	\N	2020-07-03	0	4220	126.599999999999994	3	0	f		2020-10-01	0	\N	\N	3	0	0	2020-07-03	0	\N	0  	\N	PLAZO FIJO	\N	4	BIM	1	1	MESES	0
32	20648525	HUANCAYO	\N	2	\N	\N	2020-07-02	0	245	0.0500000000000000028	6	0	f		2020-12-29	0	\N	\N	4	0	0	2020-07-16	0	\N	0  	\N	LIBRE	\N	4	BIM	1	1	MESES	0
36	00000000	HUANCAYO	\N	2	\N	\N	2020-07-04	0	925	0	9	0	f		2021-03-31	0	\N	\N	4	0	0	2020-07-04	0	\N	0  	\N	LIBRE	\N	4	BIM	1	1	MESES	0
58	44279232	HUANCAYO	\N	2	\N	\N	2020-07-16	0	0	0	6	0	f		2021-01-12	0	\N	\N	4	0	0	2020-07-16	0	\N	0  	\N	LIBRE	\N	4	BIM	1	1	MESES	0
41	19996893	HUANCAYO	\N	2	\N	\N	2020-07-10	0	0	0	6	0	f		2021-01-06	0	\N	\N	4	0	0	2020-07-10	0	\N	0  	\N	LIBRE	\N	4	BIM	1	1	MESES	0
39	45938581	HUANCAYO	\N	2	\N	\N	2020-07-07	0	100	0	3	0	f		2020-10-05	0	\N	\N	4	0	0	2020-07-07	0	\N	0  	\N	LIBRE	\N	4	BIM	1	1	MESES	0
38	42816850	HUANCAYO	\N	2	\N	\N	2020-07-07	0	100	0	3	0	f		2020-10-05	0	\N	\N	4	0	0	2020-07-07	0	\N	0  	\N	LIBRE	\N	4	BIM	1	1	MESES	0
50	46657916	HUANCAYO	\N	2	\N	\N	2020-07-10	0	0	0	6	0	f		2021-01-06	0	\N	\N	4	0	0	2020-07-10	0	\N	0  	\N	LIBRE	\N	4	BIM	1	1	MESES	0
57	76663751	HUANCAYO	\N	2	\N	\N	2020-07-14	0	580	0	6	0	f		2021-01-10	0	\N	\N	4	0	0	2020-07-14	0	\N	0  	\N	LIBRE	\N	4	BIM	1	1	MESES	0
37	46357683	HUANCAYO	\N	2	\N	\N	2020-07-06	0	0	0	6	0	f		2021-01-02	0	\N	\N	4	0	0	2020-07-06	0	\N	0  	\N	LIBRE	\N	4	BIM	1	1	MESES	0
42	45666252	HUANCAYO	\N	2	\N	\N	2020-07-10	0	0	0	6	0	f		2021-01-06	0	\N	\N	4	0	0	2020-07-10	0	\N	0  	\N	LIBRE	\N	4	BIM	1	1	MESES	0
52	44274623	HUANCAYO	\N	2	\N	\N	2020-07-10	0	0	0	6	0	f		2021-01-06	0	\N	\N	4	0	0	2020-07-10	0	\N	0  	\N	LIBRE	\N	4	BIM	1	1	MESES	0
51	20079323	HUANCAYO	\N	2	\N	\N	2020-07-10	0	0	0	6	0	f		2021-01-06	0	\N	\N	4	0	0	2020-07-10	0	\N	0  	\N	LIBRE	\N	4	BIM	1	1	MESES	0
49	20047216	HUANCAYO	\N	2	\N	\N	2020-07-10	0	0	0	6	0	f		2021-01-06	0	\N	\N	4	0	0	2020-07-10	0	\N	0  	\N	LIBRE	\N	4	BIM	1	1	MESES	0
54	41736927	HUANCAYO	\N	2	\N	\N	2020-07-10	0	5754	4.79999999999999982	6	0	f		2021-01-06	0	\N	\N	4	0	0	2020-07-20	0	\N	0  	\N	LIBRE	\N	4	BIM	1	1	MESES	0
60	70020689	HUANCAYO	\N	2	\N	\N	2020-07-23	0	0	0	6	0	f		2021-01-19	0	\N	\N	4	0	0	2020-07-23	0	\N	0  	\N	LIBRE	\N	4	BIM	1	1	MESES	0
45	71872891	HUANCAYO	\N	2	\N	\N	2020-07-10	0	0	0	6	0	f		2021-01-06	0	\N	\N	4	0	0	2020-07-10	0	\N	0  	\N	LIBRE	\N	4	BIM	1	1	MESES	0
55	19946742	HUANCAYO	\N	2	\N	\N	2020-07-13	0	0	0	6	0	f		2021-01-09	0	\N	\N	4	0	0	2020-07-13	0	\N	0  	\N	LIBRE	\N	4	BIM	1	1	MESES	0
46	44350264	HUANCAYO	\N	2	\N	\N	2020-07-10	0	0	0	6	0	f		2021-01-06	0	\N	\N	4	0	0	2020-07-10	0	\N	0  	\N	LIBRE	\N	4	BIM	1	1	MESES	0
48	77377731	HUANCAYO	\N	2	\N	\N	2020-07-10	0	0	0	6	0	f		2021-01-06	0	\N	\N	4	0	0	2020-07-10	0	\N	0  	\N	LIBRE	\N	4	BIM	1	1	MESES	0
47	77381267	HUANCAYO	\N	2	\N	\N	2020-07-10	0	0	0	6	0	f		2021-01-06	0	\N	\N	4	0	0	2020-07-10	0	\N	0  	\N	LIBRE	\N	4	BIM	1	1	MESES	0
43	41278746	HUANCAYO	\N	2	\N	\N	2020-07-10	0	0	0	6	0	f		2021-01-06	0	\N	\N	4	0	0	2020-07-10	0	\N	0  	\N	LIBRE	\N	4	BIM	1	1	MESES	0
44	20898988	HUANCAYO	\N	2	\N	\N	2020-07-10	0	0	0	6	0	f		2021-01-06	0	\N	\N	4	0	0	2020-07-10	0	\N	0  	\N	LIBRE	\N	4	BIM	1	1	MESES	0
53	48117988	HUANCAYO	\N	2	\N	\N	2020-07-10	0	0	0	6	0	f		2021-01-06	0	\N	\N	4	0	0	2020-07-10	0	\N	0  	\N	LIBRE	\N	4	BIM	1	1	MESES	0
56	20682065	HUANCAYO	\N	2	\N	\N	2020-07-13	0	460	0.100000000000000006	6	0	f		2021-01-09	0	\N	\N	4	0	0	2020-07-25	0	\N	0  	\N	LIBRE	\N	4	BIM	1	1	MESES	0
59	44063640	HUANCAYO	\N	2	\N	\N	2020-07-20	0	15	0	6	0	f		2021-01-16	0	\N	\N	4	0	0	2020-07-25	0	\N	0  	\N	LIBRE	\N	4	BIM	1	1	MESES	0
40	43219907	HUANCAYO	\N	2	\N	\N	2020-07-08	0	100	0.719999999999999973	3	0	f		2020-10-06	0	\N	\N	4	0	0	2020-08-10	0	\N	0  	\N	LIBRE	\N	4	BIM	1	1	MESES	0
\.


--
-- Data for Name: metas_personal; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.metas_personal (id_metas_personal, fecha_hora, usuario, nro_creditos, nro_clientes, monto_otrogado, interes_otrogado, redondeo_otrogado, monto_saldo, interes_saldo, redondeo_saldo, monto_cobrado, interes_cobrado, redondeo_cobrado, monto_vencido, porcentaje, mora, noti, otros, nro_creditos_fin, nro_clientes_fin, monto_otrogado_fin, interes_otrogado_fin, redondeo_otrogado_fin, monto_saldo_fin, interes_saldo_fin, redondeo_saldo_fin, monto_cobrado_fin, interes_cobrado_fin, redondeo_cobrado_fin, monto_vencido_fin, porcentaje_fin, mora_cobrado, noti_cobrado, otros_cobrado, colacar_meta, saldo_meta, monto_vencido_meta, porcentaje_meta, creditos_meta, clientes_meta, cumplimiento, fecha_hora_cierre, id_usuario, id_agencia) FROM stdin;
\.


--
-- Data for Name: movimiento; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.movimiento (id_movimiento, id_libreta_ahorro, id_cajera, fecha_hora_movimiento, tipo_operacion, monto_movimiento, interes_movimiento, redondeo, obs, id_agencia, agencia, puntos, id_caja_apertura_cierre, cajera, estado, obs_cancelacion, concepto) FROM stdin;
8786	2	4	2020-06-30 10:12:01	DEPOSITO  	300	\N	\N	\N	0	HUANCAYO                      	1	3848	BIM                 	1	\N	CAPITAL
8787	3	4	2020-06-30 10:59:37	DEPOSITO  	400	\N	\N	\N	1	HUANCAYO                      	1	3848	BIM                 	1	\N	CAPITAL
8788	4	4	2020-06-30 12:18:20	DEPOSITO  	15	\N	\N	\N	1	HUANCAYO                      	1	3848	BIM                 	1	\N	CAPITAL
8789	12	4	2020-06-30 01:31:31	DEPOSITO  	300	\N	\N	\N	1	HUANCAYO                      	1	3848	BIM                 	1	\N	CAPITAL
8790	12	4	2020-06-30 01:31:52	DEPOSITO  	10	\N	\N	\N	1	HUANCAYO                      	1	3848	BIM                 	1	\N	CAPITAL
8791	13	4	2020-06-30 02:04:21	DEPOSITO  	15	\N	\N	\N	1	HUANCAYO                      	1	3848	BIM                 	1	\N	CAPITAL
8792	13	4	2020-06-30 02:04:55	DEPOSITO  	20	\N	\N	\N	1	HUANCAYO                      	1	3848	BIM                 	1	\N	CAPITAL
8793	15	4	2020-06-30 02:06:17	DEPOSITO  	826.100000000000023	\N	\N	\N	1	HUANCAYO                      	1	3848	BIM                 	1	\N	CAPITAL
8794	16	4	2020-06-30 02:07:44	DEPOSITO  	90	\N	\N	\N	1	HUANCAYO                      	1	3848	BIM                 	1	\N	CAPITAL
8795	16	4	2020-06-30 02:10:35	DEPOSITO  	330	\N	\N	\N	1	HUANCAYO                      	1	3848	BIM                 	1	\N	CAPITAL
8796	17	4	2020-06-30 02:11:56	DEPOSITO  	90	\N	\N	\N	1	HUANCAYO                      	1	3848	BIM                 	1	\N	CAPITAL
8797	18	4	2020-06-30 02:12:34	DEPOSITO  	355	\N	\N	\N	1	HUANCAYO                      	1	3848	BIM                 	1	\N	CAPITAL
8798	19	4	2020-06-30 02:13:22	DEPOSITO  	685	\N	\N	\N	1	HUANCAYO                      	1	3848	BIM                 	1	\N	CAPITAL
8799	20	4	2020-06-30 02:13:57	DEPOSITO  	5	\N	\N	\N	1	HUANCAYO                      	1	3848	BIM                 	1	\N	CAPITAL
8800	12	4	2020-06-30 02:15:52	DEPOSITO  	300	\N	\N	\N	1	HUANCAYO                      	1	3848	BIM                 	1	\N	CAPITAL
8801	23	4	2020-06-30 02:55:52	DEPOSITO  	570.42999999999995	\N	\N	\N	1	HUANCAYO                      	1	3848	BIM                 	1	\N	CAPITAL
8802	22	4	2020-06-30 02:58:51	DEPOSITO  	1128.79999999999995	\N	\N	\N	1	HUANCAYO                      	1	3848	BIM                 	1	\N	CAPITAL
8803	21	4	2020-06-30 03:00:07	DEPOSITO  	802	\N	\N	\N	1	HUANCAYO                      	1	3848	BIM                 	1	\N	CAPITAL
8804	25	9	2020-07-01 07:45:51	DEPOSITO  	20500	\N	\N	\N	3	PUCALLPA                      	1	3851	LARIAS              	1	\N	CAPITAL
8805	30	4	2020-07-02 09:42:38	DEPOSITO  	480	\N	\N	\N	1	HUANCAYO                      	1	3852	BIM                 	1	\N	CAPITAL
8806	29	4	2020-07-02 09:42:57	DEPOSITO  	4000	\N	\N	\N	1	HUANCAYO                      	1	3852	BIM                 	1	\N	CAPITAL
8807	28	4	2020-07-02 09:43:14	DEPOSITO  	20000	\N	\N	\N	1	HUANCAYO                      	1	3852	BIM                 	1	\N	CAPITAL
8808	29	4	2020-07-02 09:45:36	RETIRO    	2000	\N	\N	\N	1	HUANCAYO                      	1	3852	BIM                 	1	\N	
8809	31	4	2020-07-02 12:01:41	DEPOSITO  	10	\N	\N	\N	1	HUANCAYO                      	1	3852	BIM                 	1	\N	CAPITAL
8810	32	4	2020-07-02 12:09:01	DEPOSITO  	35	\N	\N	\N	1	HUANCAYO                      	1	3852	BIM                 	1	\N	CAPITAL
8811	12	4	2020-07-02 01:11:47	DEPOSITO  	10	\N	\N	\N	1	HUANCAYO                      	1	3852	BIM                 	1	\N	CAPITAL
8812	19	4	2020-07-03 09:44:40	RETIRO    	80.5999999999999943	\N	\N	\N	1	HUANCAYO                      	1	3853	BIM                 	1	\N	
8813	19	4	2020-07-03 10:37:58	RETIRO    	1.39999999999999991	\N	\N	\N	1	HUANCAYO                      	1	3853	BIM                 	1	\N	
8814	34	4	2020-07-03 02:42:09	DEPOSITO  	4220	\N	\N	\N	1	HUANCAYO                      	1	3853	BIM                 	1	\N	CAPITAL
8815	36	4	2020-07-04 01:35:08	DEPOSITO  	925	\N	\N	\N	1	HUANCAYO                      	1	3854	BIM                 	1	\N	CAPITAL
8816	39	4	2020-07-07 12:17:24	DEPOSITO  	300	\N	\N	\N	1	HUANCAYO                      	1	3856	BIM                 	1	\N	CAPITAL
8817	39	4	2020-07-07 12:17:36	DEPOSITO  	100	\N	\N	\N	1	HUANCAYO                      	1	3856	BIM                 	1	\N	CAPITAL
8818	38	4	2020-07-07 12:18:06	DEPOSITO  	300	\N	\N	\N	1	HUANCAYO                      	1	3856	BIM                 	1	\N	CAPITAL
8819	38	4	2020-07-07 12:18:16	DEPOSITO  	100	\N	\N	\N	1	HUANCAYO                      	1	3856	BIM                 	1	\N	CAPITAL
8820	40	4	2020-07-08 12:48:33	DEPOSITO  	300	\N	\N	\N	1	HUANCAYO                      	1	3858	BIM                 	1	\N	CAPITAL
8821	40	4	2020-07-08 12:49:38	RETIRO    	100	\N	\N	\N	1	HUANCAYO                      	1	3858	BIM                 	1	\N	
8822	51	4	2020-07-10 01:59:08	DEPOSITO  	300	\N	\N	\N	1	HUANCAYO                      	1	3862	BIM                 	1	\N	CAPITAL
8823	52	4	2020-07-10 02:00:44	DEPOSITO  	345	\N	\N	\N	1	HUANCAYO                      	1	3862	BIM                 	1	\N	CAPITAL
8824	52	4	2020-07-10 02:05:25	RETIRO    	345	\N	\N	\N	1	HUANCAYO                      	1	3862	BIM                 	1	\N	
8825	51	4	2020-07-10 02:07:11	RETIRO    	300	\N	\N	\N	1	HUANCAYO                      	1	3862	BIM                 	1	\N	
8826	54	4	2020-07-13 11:39:56	DEPOSITO  	6754	\N	\N	\N	1	HUANCAYO                      	1	3866	BIM                 	1	\N	CAPITAL
8827	54	4	2020-07-13 11:40:39	RETIRO    	500	\N	\N	\N	1	HUANCAYO                      	1	3866	BIM                 	1	\N	
8828	57	4	2020-07-14 12:05:31	DEPOSITO  	580	\N	\N	\N	1	HUANCAYO                      	1	3868	BIM                 	1	\N	CAPITAL
8829	32	4	2020-07-16 10:34:32	DEPOSITO  	210	\N	\N	\N	1	HUANCAYO                      	1	3872	BIM                 	1	\N	CAPITAL
8830	54	4	2020-07-20 01:51:04	RETIRO    	500	\N	\N	\N	1	HUANCAYO                      	1	3877	BIM                 	1	\N	
8831	56	4	2020-07-23 09:29:03	DEPOSITO  	450	\N	\N	\N	1	HUANCAYO                      	1	3881	BIM                 	1	\N	CAPITAL
8832	59	4	2020-07-23 09:31:08	DEPOSITO  	460	\N	\N	\N	1	HUANCAYO                      	1	3881	BIM                 	1	\N	CAPITAL
8833	59	4	2020-07-23 09:31:21	DEPOSITO  	10	\N	\N	\N	1	HUANCAYO                      	1	3881	BIM                 	1	\N	CAPITAL
8834	59	4	2020-07-23 09:31:33	DEPOSITO  	5	\N	\N	\N	1	HUANCAYO                      	1	3881	BIM                 	1	\N	CAPITAL
8835	59	4	2020-07-23 09:31:48	DEPOSITO  	5	\N	\N	\N	1	HUANCAYO                      	1	3881	BIM                 	1	\N	CAPITAL
8836	59	4	2020-07-23 09:32:00	DEPOSITO  	5	\N	\N	\N	1	HUANCAYO                      	1	3881	BIM                 	1	\N	CAPITAL
8837	59	4	2020-07-23 09:32:09	DEPOSITO  	5	\N	\N	\N	1	HUANCAYO                      	1	3881	BIM                 	1	\N	CAPITAL
8838	59	4	2020-07-23 09:32:19	DEPOSITO  	5	\N	\N	\N	1	HUANCAYO                      	1	3881	BIM                 	1	\N	CAPITAL
8839	59	4	2020-07-23 09:32:30	DEPOSITO  	5	\N	\N	\N	1	HUANCAYO                      	1	3881	BIM                 	1	\N	CAPITAL
8840	59	4	2020-07-23 09:32:44	DEPOSITO  	5	\N	\N	\N	1	HUANCAYO                      	1	3881	BIM                 	1	\N	CAPITAL
8841	59	4	2020-07-23 09:32:53	DEPOSITO  	5	\N	\N	\N	1	HUANCAYO                      	1	3881	BIM                 	1	\N	CAPITAL
8842	59	4	2020-07-23 09:33:02	DEPOSITO  	5	\N	\N	\N	1	HUANCAYO                      	1	3881	BIM                 	1	\N	CAPITAL
8843	59	4	2020-07-23 09:33:43	DEPOSITO  	5	\N	\N	\N	1	HUANCAYO                      	1	3881	BIM                 	1	\N	CAPITAL
8844	56	4	2020-07-23 01:31:23	DEPOSITO  	5	\N	\N	\N	1	HUANCAYO                      	1	3881	BIM                 	1	\N	CAPITAL
8845	56	4	2020-07-23 01:33:57	DEPOSITO  	5	\N	\N	\N	1	HUANCAYO                      	1	3881	BIM                 	1	\N	CAPITAL
8846	59	4	2020-07-24 02:20:23	DEPOSITO  	5	\N	\N	\N	1	HUANCAYO                      	1	3883	BIM                 	1	\N	CAPITAL
8847	12	4	2020-07-24 02:22:22	DEPOSITO  	10	\N	\N	\N	1	HUANCAYO                      	1	3883	BIM                 	1	\N	CAPITAL
8848	56	4	2020-07-25 12:05:12	DEPOSITO  	5	\N	\N	\N	1	HUANCAYO                      	1	3884	BIM                 	1	\N	CAPITAL
8849	59	4	2020-07-25 12:08:10	DEPOSITO  	5	\N	\N	\N	1	HUANCAYO                      	1	3884	BIM                 	1	\N	CAPITAL
8850	40	4	2020-08-10 01:16:37	RETIRO    	100	\N	\N	\N	1	HUANCAYO                      	1	3897	BIM                 	1	\N	
8851	12	4	2020-08-18 01:38:37	DEPOSITO  	10	\N	\N	\N	1	HUANCAYO                      	1	3904	BIM                 	1	\N	CAPITAL
\.


--
-- Data for Name: movimiento_usuario; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.movimiento_usuario (id_movimiento_usuario, id_usuario, id_cajera, fecha_hora_movimiento, operacion, monto_operacion, sustento, agencia) FROM stdin;
\.


--
-- Data for Name: negocio; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.negocio (id_negocio, dni, nombre_actividad, ciiu, distrito, provincia, departamento, direccion, referencia, valorizacion, obs, estado_negocio, id_agencia, obs_modificacion) FROM stdin;
1	19940774	TRANSPORTE 	SERVICIO	HUANCÁN	HUANCAYO	JUNIN	JR AMAZONAS N°380	DE LA CERRETERA CENTRAL 3 CRDS HACIA ABAJO	25000	AUTOCOLECTIVO	\N	\N	\N
2	44974563	PASTELERIA	COMERCIO	EL TAMBO	HUANCAYO	JUNIN	LOS TUNALES N°118	POR EL RESERVORIO DE AGUA - POR LA CALAVERA	1500	VTA DE TORTAS	\N	\N	\N
3	23701995	MULTIPLEX	COMERCIO	HUANCAYO	HUANCAYO	JUNIN	JR. CUSCO N°1985	CUSCO Y CATALINA HUANCA PARA ABAJO 3 CASAS	2500	VTA ROPA,HOJA VERDE, CAÑA	\N	\N	\N
4	23701995	VTA DE LICORES	COMERCIO	HUANCAYO	HUANCAYO	JUNIN	JR CUSCO N°1985	JR CUSCO Y CATALINA HUANCA	5500	RON CAÑA CERVEZA	\N	\N	\N
5	20113864	POSTRES	COMERCIO	CHILCA	HUANCAYO	JUNIN	PASAJE VILCAHUAMAN N°169	A ESPALDAS DEL CUARTEL DE CHILCA	1200	VTA AMBULATORIA	\N	\N	\N
6	76621787	GRAS SISTETICO COLCABAMBINO	SERVICIO	CHILCA	HUANCAYO	JUNIN	JR MALECON S	FRENTE AL LOCAL MIL MARAVILLAS 	2000	ALQUILER DE LOSA DEPORTIVA 	\N	\N	\N
7	71872891	INGENIERA DE SISTEMAS 	SERVICIO	HUANCAYO	HUANCAYO	JUNIN	JR PEDRO PERALTA 	PEDRO PERALTA Y REAL	3500	CONSULTA DE SISTEMAS DE EMISION ELCTROMICA 	\N	\N	\N
8	19999596	PERSONAL DE LIMPIEZA	SERVICIO	HUANCAYO	HUANCAYO	JUNIN	JR ICA Y FERROCARIL	COMNERCIALES HUANCA 	1200	PERSONAL DE LIMPIEZA EN CENTRO COMERCIAL HUANCA  	\N	\N	\N
9	46357683	VENTA DE FRUTAS 	COMERCIO	HUANCAYO	HUANCAYO	JUNIN	JR ICA NUEVA Y HUANCAS 	MERCADO MALTERIA 	5000	MERCADO MALTERIA 	\N	\N	\N
10	80401963	TRANSPORTE	SERVICIO	HUANCÁN	HUANCAYO	JUNIN	JR. JUNIN	PARADERO DE LOS MICROS ROJOS	2000	TRANSPORTE INTERDISTRITAL	\N	\N	\N
11	47475777	CONSTRUCCION 	SERVICIO	HUANCAYO	HUANCAYO	JUNIN	JR LOS CLAVELES N° 365	LORETO Y CLAVELES 	8000	CONSTRUCTOR 	\N	\N	\N
12	23701995	VENTA DE  BEBIDAS	COMERCIO	HUANCAYO	HUANCAYO	JUNIN	JR CUSCO N°1985	ENTRE CUSCO Y CATALINA HUANCA 	10000	VENTA HOJA VERDE, CAÑA, BEBIDAS 	\N	\N	\N
13	76290455	TAXISTA	SERVICIO	HUANCAYO	HUANCAYO	JUNIN	PILAR PAUCARCHUCO	EN EL MERCADO AURAY	1500	CONDUCTOR	\N	\N	\N
14	40129120	TRANSPORTE	SERVICIO	SAPLLANGA	HUANCAYO	JUNIN	JR ODONOVAN N°416	A UNA CUADRA DE LA PLAZA	3000	TRANSPORTE DE CARGA	\N	\N	\N
15	42297478	SOLDADURIA	COMERCIO	HUANCAYO	HUANCAYO	JUNIN	SANTOS CHOCANO N°496	EN CENTRO COMERCIAL METRO 	5000	SOLDADOR 	\N	\N	\N
16	19940220	COMERCIANTE	COMERCIO	HUANCÁN	HUANCAYO	JUNIN	JR JOSE OLAYA S/N	A DOS CUADRAS DEL PARQUE DE HUANCAN 	6000	VENTA DE VERDURAS 	\N	\N	\N
17	16134843	MUNICIPALIDAD DE HUANCAN 	SERVICIO	HUANCÁN	HUANCAYO	JUNIN	JR JUNIN S/N 	PARQUE DE HUANCAN 	1200	GESTOR DE COBRANZAS 	\N	\N	\N
18	60064606	TIENDA	COMERCIO	CHILCA	HUANCAYO	JUNIN	JR. MARISCAL CASTILLA	ENTRE AMAZONAS Y ANCASH	2000	VENTA DE CAL	\N	\N	\N
19	41123341	RESTAURANT NATALY	SERVICIO	HUANCAYO	HUANCAYO	JUNIN	AV HUANCAVELICA Y LORETO 	AL FRENTE DE LA BOTICA LOPEZ 	5000	RESTAURANT	\N	\N	\N
20	44279232	FABRIGA DE TELAS 	SERVICIO	HUANCAYO	HUANCAYO	JUNIN	AV LA ESPERANZA 	BODEGA LA ULTIMA 	2300	COSTURERO	\N	\N	\N
21	45745478	JUGOS	COMERCIO	CHILCA	HUANCAYO	JUNIN	PSJ MIGUEL GRAU SN	AUQUIMARCA	2000	VENTA DE JUGOS Y ABARROTES	\N	\N	\N
22	73054005	VENTAS 	COMERCIO	HUANCAYO	HUANCAYO	JUNIN	AV HUANCAVELICA N°1340	AV HUANCAVELICA 	1000	VENTA DE AYUDIN	\N	\N	\N
23	42831861	EVENTOS	SERVICIO	CHILCA	HUANCAYO	JUNIN	PSJ SANTA ROSA SN	LAS LOMAS	2000	VENTA DE CERVEZA	\N	\N	\N
24	20064201	CONSTRUCCION	SERVICIO	HUANCAYO	HUANCAYO	JUNIN	JR JUNIN S/N POR VENIR 	HUANCAN 	5000	CONSTRUCCTOR	\N	\N	\N
25	20099395	HERRERO	SERVICIO	CHILCA	HUANCAYO	JUNIN	JR. ANCASH N°324	AL COSTADO DE MECANICA	2000	HERRERO	\N	\N	\N
26	80145065	PERSONAL DE LIMPIEZA	SERVICIO	CHILCA	HUANCAYO	JUNIN	PJ VILCAHUAMAN N°170	DETRAS DEL CUARTEL DE CHILCA 	5000	LIMPIEZA	\N	\N	\N
27	41773365	MELAMINAS JHAIR	SERVICIO	HUANCAYO	HUANCAYO	JUNIN	JR. LA LIBERTAD N°1092	FRENTE A CEPREUNCP	3000	FABRICACION DE MUEBLES EN MELAMINA	\N	\N	\N
28	00098119	RESTAURANTE	COMERCIO	HUANCAYO	HUANCAYO	JUNIN	AA.HH MASISEA - AV.JHON F KENEDY N°730	POR EL MERCADO DE LOS CUNCHIS	1500	VTA DE COMIDA	\N	\N	\N
29	20898988	COMERCIANTE	COMERCIO	CHILCA	HUANCAYO	JUNIN	AV LOS PROCERES N°394	ENTRE LA REAL 	5000	VENTA 	\N	\N	\N
30	41274111	COMERCIANTE	COMERCIO	HUANCAYO	HUANCAYO	JUNIN	AV HEROES DE LA BREÑA N°266	PASANDO EL PUENTE CHANCHAS 	6000	COMERCIANTE	\N	\N	\N
31	21276617	MINA	SERVICIO	EL TAMBO	HUANCAYO	JUNIN	AV.FERROCARRIL N°5157	FERROCARRIL	4000	PRESTACION DE SERVICIOS EN LA MINA	\N	\N	\N
32	44326038	 COSTURERIA 	SERVICIO	EL TAMBO	HUANCAYO	JUNIN	PSJ TUPAC AMARU CULLPA BAJA 	AV 12 DE OCTUBRE 	6000	COSTURA	\N	\N	\N
33	00061656	SERRAGERIA	PRODUCCIÓN	HUACHAC	CHUPACA	JUNIN	PSJ.GENERAL MONTERO N°128	SUCRE  CON SAN MARTIN ULTIMA CUDAR	1500	ELABORACION DE PUERTAS DE FIERRO	\N	\N	\N
34	71340438	RIOS PEREZ RAY NILK	COMERCIO	HUACHAC	CHUPACA	JUNIN	JR. ATAHUALPAN 178	PRIMERA CUADRAN DE JR. ATAHUALPA 	1800	 VENTA DE MADERAS	\N	\N	\N
35	46485289	COMERCIO	COMERCIO	HUANCÁN	HUANCAYO	JUNIN	AV. PANAMERICANA	FERIA DE HUAYUCACHI	6000	VENTA DE ANIMALES	\N	\N	\N
36	40893286	JANET	COMERCIO	CHILCA	HUANCAYO	JUNIN	AV. GENERAL CORDOVA SN	ENTRE AMAZONAS Y 28 DE JULIO	5000	VENTA DE ABARROTES	\N	\N	\N
37	19942242	FILOMENA	COMERCIO	HUANCÁN	HUANCAYO	JUNIN	JR. JUNIN SN	LOZA DEPORTIVA DE HUANCAN	3000	VENTA DE ABARROTES	\N	\N	\N
38	19944388	GLORIA	COMERCIO	HUANCAYO	HUANCAYO	JUNIN	JR. JUNIN SN	ANTES DEL PAARDERO DE LOS ROJOS	1500	VENTA DE ABARROTES	\N	\N	\N
39	77161207	VENTA DE LANA 	COMERCIO	HUANCÁN	HUANCAYO	JUNIN	JR PORVENIR PICHAS 	AV GRAU 	5000	VENTA DE LANA 	\N	\N	\N
40	45666252	VENTA DE POLLOS 	COMERCIO	EL TAMBO	HUANCAYO	JUNIN	JR CAHUIDE N°168	SECTOR 11 EL TAMBO 	3000	POLLERO 	\N	\N	\N
41	41278746	LIBRERIA 	COMERCIO	HUANCÁN	HUANCAYO	JUNIN	JR JUNIN S/N	HUANCAN	9000	LIBRERIA 	\N	\N	\N
42	99907227	BODEGA	COMERCIO	EL TAMBO	HUANCAYO	JUNIN	PSJ TUPAC AMARU 	CULLPA BAJA 	9500	BODEGERO 	\N	\N	\N
43	80535329	TAXIA VEA	SERVICIO	HUANCAYO	HUANCAYO	JUNIN	JR JUNIN 	BARRIO POR VENIR	3000	TAXISTA 	\N	\N	\N
44	20065148	TEXTIL	COMERCIO	HUAYUCACHI	HUANCAYO	JUNIN	JR  HUMBOLT 1373 - LA VICTORIA	DEL PARQUE HACIA ABAJO 2 CUADRAS	65000	FABRICACION DE FUSTANES	\N	\N	\N
45	47846248	ASISTENTE ADMINISTRATIVO	SERVICIO	EL TAMBO	HUANCAYO	JUNIN	AV. UNIVERSITARIA S/N	DEL AL UNCO HACIA ARRIBA 4 CUADRAS	3500	UGEL HYO	\N	\N	\N
46	40144277	COSTURA	COMERCIO	HUANCÁN	HUANCAYO	JUNIN	JR JUNIN - HUANCAN	BARRIO POR VENIR 	9000	COSTURERIA 	\N	\N	\N
47	48024565	PROFESORA	SERVICIO	HUANCAYO	HUANCAYO	JUNIN	JR.HUANCAS Nª1785	HUANCAS BY URUGUAY	2000	PROFESORA DE INICIA	\N	\N	\N
48	70020689	INTERNET MAGALY	SERVICIO	EL TAMBO	HUANCAYO	JUNIN	JR MICAELA BASTIDAS N°132	INTERNET DAB 	5000	INTERNET 	\N	\N	\N
49	46196324	CARDENAS	SERVICIO	CHILCA	HUANCAYO	JUNIN	JR.GENERAL GAMARRA  N°CDRA 2	COCHERA	2000	COCHERA	\N	\N	\N
50	20999895	FOURLIFE	COMERCIO	CHILCA	HUANCAYO	JUNIN	PSJ.VILCAHUAMAN SN	A ESPALDAS DEL RESTAURANTE EL COMPADRE	5000	VENTA DE SUPLEMENTOS	\N	\N	\N
51	42388030	ETUPSA	SERVICIO	HUANCÁN	HUANCAYO	JUNIN	JR.JUNIN	PARADERO DE LOS ROJOS	5000	TRANSPORTE	\N	\N	\N
52	19946742	MARMOLEJO	COMERCIO	HUANCÁN	HUANCAYO	JUNIN	AV. DANIEL A. CARRION SN	POR LA PLAZA DE HUAMANMARCA	5000	VENTA DE ANIMALES 	\N	\N	\N
53	19870079	RESTAURANT	COMERCIO	HUANCÁN	HUANCAYO	JUNIN	AV PANAMERICA SUR 	UNIDAD VECINAL 	5000	RESTAURANTE	\N	\N	\N
54	19996893	LIBRERIA 	COMERCIO	CHILCA	HUANCAYO	JUNIN	LEONCIO PRADO N° 1015	LEONCIO PRADO Y JACINTO IBARRA 	9000	LIBRERIA MARQUINA	\N	\N	\N
55	45879388	INTERNET	SERVICIO	HUANCÁN	HUANCAYO	JUNIN	JR JUNIN N°48	BARRIO POR VENIR 	9000	INTERNET DELCY	\N	\N	\N
56	70437871	CONSULTORIA 	SERVICIO	EL TAMBO	HUANCAYO	JUNIN	JR GRAU N° 575	EL TAMBO 	9000	CONSULTORIA ACADEMICO 	\N	\N	\N
57	44729185	DOCENTE	SERVICIO	CHILCA	HUANCAYO	JUNIN	PJ AUGUSTO PEÑALOSA N°173	PARQUE PEÑALOSA 	9000	DOCNETE	\N	\N	\N
58	20047216	DECORACION	COMERCIO	CHILCA	HUANCAYO	JUNIN	AV REAL °1310	AZAPAMPA	9000	DECORACION	\N	\N	\N
59	47420055	BOTICA	COMERCIO	CHILCA	HUANCAYO	JUNIN	PASAJE CANIPACO Nª	ESQUINA DEL CANALA JOSE OLAYA Y TAHUANTINSUYO	8500	MEDICAMENTOS EN GENERAL	\N	\N	\N
60	80245301	COMERCIANTE	COMERCIO	EL TAMBO	HUANCAYO	JUNIN	A.V MARIA ELENA MOYA 	FRENTE AL MERCADO MICAELA BASTIDAS 	10000	VENTA DE ROPA Y ACESSESORIOS 	\N	\N	\N
61	44350264	COOPEADORA	COMERCIO	CHILCA	HUANCAYO	JUNIN	JR ANTONIO ZELA N°560	ENTRE ZELA Y ANCASH PORTON PLOMO	9000	NOTARIA ALELUYA 	\N	\N	\N
62	19944260	BODEGA	COMERCIO	HUANCÁN	HUANCAYO	JUNIN	AV ALEXANDER FLEMIX	BARRIO POR VENIR 	15000	BODEGERA 	\N	\N	\N
63	05213767	MONROY CAMAYO ROBINSON	COMERCIO	CALLERIA	PUCALLPA	UCAYALI	AA.HH. NUEVO PARAISO MZ.I LT. 18	POR EL PUESTO CAÑO NATURAL	1500	VENTA DE PRODEUCTOS	\N	\N	\N
64	77377731	GANADERIA 	COMERCIO	EL TAMBO	HUANCAYO	JUNIN	AV 12 DE OCTUBRE N°1040	CULLPA BAJA 	9000	CRIANSA DE AMIMALES 	\N	\N	\N
65	77381267	GANADERIA	COMERCIO	EL TAMBO	HUANCAYO	JUNIN	AV 12 DE OCTUBRE N° 1040	CULLPA BAJA 	9000	CRIANZA DE ANIMALES 	\N	\N	\N
66	00093054	CORAL REATEGUI FELISINDA	COMERCIO	CALLERIA	PUCALLPA	UCAYALI	AA.HH. NUEVO PARAISO MZ. I LT. 18	ESOALDA DE BAR GITANOS	3000	BODEGA - TIENDA	\N	\N	\N
67	00035433	PEREZPEREZ REYNA MARINA	COMERCIO	CALLERIA	PUCALLPA	UCAYALI	JR. COMANDANTE BARRERA N°595	POR EL PARQUE 11 DE JULIO	2500	BODEGA 	\N	\N	\N
68	42611489	COMERCIANTE	COMERCIO	HUANCÁN	HUANCAYO	JUNIN	AV INDEPENDENCIA 	HUANCAN 	9000	VENTA DE ROPA	\N	\N	\N
69	43568148	BODEGA Y RESTAUTANT	COMERCIO	HUANCAYO	HUANCAYO	JUNIN	JR MILLER N°120	AV OCOPILLA Y MILLER 	9000	BODEGUERA Y RESTAURAMNTE 	\N	\N	\N
70	47323767	TRANSPORTE	SERVICIO	CHILCA	HUANCAYO	JUNIN	JR AMAZONAS N°1327	AZAPAMPA	9000	EMPRESA TAURUS	\N	\N	\N
71	44931763	LIMPIEZA	SERVICIO	CHILCA	HUANCAYO	JUNIN	JR 28 DE JULIO	CHILCA	9000	PERSONAL DE LIMPIEZA 	\N	\N	\N
72	46657916	GRANJA 	COMERCIO	CHILCA	HUANCAYO	JUNIN	PJ SAN SILVESTRE 	PARQUE PEÑALOSA 	9000	VENTA DE POLLOS 	\N	\N	\N
73	48117988	COMERCIANTE	COMERCIO	CHILCA	HUANCAYO	JUNIN	PS SAN SILVESTRE 	PARQUE PEÑALOSA 	9000	VENTA DE POLLOS 	\N	\N	\N
74	44019045	CONDUCTOR	SERVICIO	CHILCA	HUANCAYO	JUNIN	PJ SAN SILVESTRE 	PARQUE PEÑALOZA 	9000	TAXISTA 	\N	\N	\N
75	46706530	COSTURERIA	COMERCIO	HUANCAYO	HUANCAYO	JUNIN	CALLE GLADIOLOS N°490	HUANCAYO	10000	COSTURA	\N	\N	\N
76	48522972	TRANSPORTE	SERVICIO	HUANCÁN	HUANCAYO	JUNIN	JR 7 DE OCTUBRE 	PUEBLO JOVEN 	9000	TRANSPORTISTA 	\N	\N	\N
77	42781214	COMERCIANTE	COMERCIO	CHILCA	HUANCAYO	JUNIN	PJ MUCHAS N° LT-5	AZAPAMPA 	9000	VENTA DE ZAPATOS	\N	\N	\N
78	47858782	CONDUCTOR	SERVICIO	HUANCÁN	HUANCAYO	JUNIN	JR ALEXANDER FLEMING	HUANCAN 	9000	TAXISTA 	\N	\N	\N
79	42148384	SAJAMI VASQUEZ ERIKA LUCIA	COMERCIO	CALLERIA	PUCALLPA	UCAYALI	JR. LOS FRUTALES MZ. 358 LT. 2	POR EL PARADERO DE RUTA A CAMPO VERDE	1000	VENTA DE COMIDA	\N	\N	\N
80	47858782	CONDUCTOR	SERVICIO	HUANCÁN	HUANCAYO	JUNIN	BARRIO POR VENIR 	HUANCAN 	9000	TAXISTA	\N	\N	\N
81	76918572	COMERCIANTE	COMERCIO	HUANCAYO	HUANCAYO	JUNIN	JR PANAMA N°110	ESPALDAS DEL COLISEO HUANCA 	9000	VENTA DE POLLOS 	\N	\N	\N
82	48059190	COMERCIANTE	COMERCIO	CHILCA	HUANCAYO	JUNIN	 JR GENERAL GAMARRA N°1105	JACINTO IBARRA Y GENERAL GAMARRA 	9000	VNTA DE FRUTAS Y JUGOS 	\N	\N	\N
83	76663751	COMERCIANTE	COMERCIO	EL TAMBO	HUANCAYO	JUNIN	JR 13 DE OCTUBRE 	REAL Y 13	9000	TIENDA DE VESTIDOS 	\N	\N	\N
84	00124443	AGUILAR YSLA JUAN MANUEL	COMERCIO	CALLERIA	PUCALLPA	UCAYALI	JR. STA. TERESA N°359	POR ATRAS DE LA MEYPOL	2500	BODEGA	\N	\N	\N
85	20682065	COMERCIANTE	COMERCIO	EL TAMBO	HUANCAYO	JUNIN	PSJ LOS CLAVELES S/N SECTOR 5	LA ESPERANZA 	9000	BODEGA 	\N	\N	\N
86	44100810	COMERCIANTE	COMERCIO	EL TAMBO	HUANCAYO	JUNIN	CALLE CHICCHICANCHA N° 175	CULLPA BAJA 	9000	CRIANZA DE CUY 	\N	\N	\N
87	41614930	COCINERO	SERVICIO	HUANCAYO	HUANCAYO	JUNIN	AV.HUANCAVELICA 620	LORETO Y HUANCAVELICA	1800	COCINERO EN RESTAURANTES	\N	\N	\N
88	48177932	COMERCIANTE	COMERCIO	HUANCAYO	HUANCAYO	JUNIN	JR ICA Y GUIDO	MERCADO MAYORISTA	9000	VRNTS DE CARNES	\N	\N	\N
89	48142577	CONTABILIDAD	SERVICIO	HUANCAYO	HUANCAYO	JUNIN	JR SIN DIRRECION	SIN REFERENCIA	9000	CONTABLE	\N	\N	\N
90	19882294	COMERCIANTE	COMERCIO	CHILCA	HUANCAYO	JUNIN	JR ICA	FRENTE AL MERCADO MAYORISTA 	9000	VENTA QUION 	\N	\N	\N
91	21000001	COMERCIANTE	COMERCIO	HUANCAYO	HUANCAYO	JUNIN	JR LA FLORIDA	MERCADO LA ESPERANZA 	9000	VENTA DE FRUTAS Y VERDURAS 	\N	\N	\N
92	41738167	ALBAÑIL	PRODUCCIÓN	SAPALLANGA	HUANCAYO	JUNIN	PJ ALEGRIA SN 	PUEBLO DE SAPALLANGA 	9000	ALBAÑIL 	\N	\N	\N
93	20079039	COMERCIANTE	COMERCIO	CHILCA	HUANCAYO	JUNIN	PROLONGACION LA ESPERANZA 185	LA ESPERANZA	9000	COMERCIANTE	\N	\N	\N
94	48742387	FUCHS VELA ISABEL	COMERCIO	CALLERIA	PUCALLPA	UCAYALI	AA.HH NUEVO JERUSALEN MZ. A LT. 2	A ESPALDAS DEL COEGIO JOSE OLAYA	3000	BOFEGA	\N	\N	\N
95	70203921	FATIMA SATOMY CRUZ PINEDO	COMERCIO	CALLERIA	PUCALLPA	UCAYALI	JR.COMANDANTE BARRERA.	POR 11 DE JULIO	1500	BODEGA	\N	\N	\N
\.


--
-- Data for Name: nivel_usuario; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.nivel_usuario (id_nivel_usuario, funcion, funciones) FROM stdin;
1	PLATAFORMA	PLATAFORMA
2	CAJA-PLATAFORMA	CAJA-PLATAFORMA
3	CAJA	CAJA
4	ANALISTA	ANALISTA
5	RECUPERADOR	RECUPERADOR
6	ASESOR LEGAL	ASESOR LEGAL
7	ADMINISTRADOR	ADMINISTRADOR
8	CONTABILIDAD	CONTABILIDAD
9	GERENCIA	GERENCIA
10	INFORMATICA	INFORMATICA
\.


--
-- Data for Name: notificaciones; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.notificaciones (id_notificaciones, id_solicitud, monto_vencido, dias_retraso, id_analista, id_tipo_notifi, fecha_gestion, costo, accion, fecha_accion, respuesta, observacion, usuario, telefono, tipo_direccion, direcccion, dni, id_agencia) FROM stdin;
\.


--
-- Data for Name: operacion; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.operacion (id_operacion, id_operacion_pago, id_solicitud, id_cajera, id_impresion, fecha_hora, concepto, monto, de_cuota, a_cuota, id_analista, id_agencia, capital, interes, redondeo) FROM stdin;
1	1	15	4	0	2020-06-30 02:17:40	CREDITO	24.6000004	3	0	\N	\N	\N	\N	\N
2	2	15	4	0	2020-07-02 01:10:03	CREDITO	8.19999981	4	0	\N	\N	\N	\N	\N
3	3	28	4	0	2020-07-03 10:36:58	CREDITO	82	1	0	\N	\N	\N	\N	\N
4	4	51	4	0	2020-07-03 02:06:49	CREDITO	520	1	0	\N	\N	\N	\N	\N
5	5	56	4	0	2020-07-03 02:33:50	CREDITO	1080	1	0	\N	\N	\N	\N	\N
6	6	27	4	0	2020-07-03 02:42:56	CREDITO	782	1	0	\N	\N	\N	\N	\N
7	6	27	4	0	2020-07-03 02:42:56	OTROS	0.5	1	0	\N	\N	\N	\N	\N
8	6	27	4	0	2020-07-03 02:42:56	DES_PRECANCELACION	0.5	1	0	\N	\N	\N	\N	\N
9	7	15	4	0	2020-07-06 02:48:47	CREDITO	16.3999996	6	0	\N	\N	\N	\N	\N
10	8	74	9	0	2020-07-07 11:57:11	CREDITO	128.399994	6	0	\N	\N	\N	\N	\N
11	9	84	9	0	2020-07-08 10:30:29	CREDITO	113.099998	13	0	\N	\N	\N	\N	\N
12	9	84	9	0	2020-07-08 10:30:29	OTROS	0.5	13	0	\N	\N	\N	\N	\N
13	9	84	9	0	2020-07-08 10:30:29	DES_PRECANCELACION	0.100000001	13	0	\N	\N	\N	\N	\N
14	10	74	9	0	2020-07-08 10:32:13	CREDITO	128.399994	12	0	\N	\N	\N	\N	\N
15	10	74	9	0	2020-07-08 10:32:13	OTROS	0.5	12	0	\N	\N	\N	\N	\N
16	10	74	9	0	2020-07-08 10:32:13	DES_PRECANCELACION	0.200000003	12	0	\N	\N	\N	\N	\N
17	11	85	9	0	2020-07-09 12:39:12	CREDITO	0.100000001	0	0	\N	\N	\N	\N	\N
18	12	87	4	0	2020-07-09 01:07:24	CREDITO	324	1	0	\N	\N	\N	\N	\N
19	13	72	4	0	2020-07-10 09:50:49	CREDITO	80	1	0	\N	\N	\N	\N	\N
20	14	63	4	0	2020-07-10 02:46:53	CREDITO	119.349998	1	0	\N	\N	\N	\N	\N
21	15	15	4	0	2020-07-10 03:05:38	CREDITO	24.6000004	9	0	\N	\N	\N	\N	\N
22	16	85	9	0	2020-07-10 10:11:41	CREDITO	21.5	1	0	\N	\N	\N	\N	\N
23	17	86	9	0	2020-07-10 10:12:23	CREDITO	30.2000008	1	0	\N	\N	\N	\N	\N
24	18	67	4	0	2020-07-11 09:21:53	CREDITO	20.6000004	1	0	\N	\N	\N	\N	\N
25	19	72	4	0	2020-07-11 09:22:30	CREDITO	40	2	0	\N	\N	\N	\N	\N
26	20	62	4	0	2020-07-11 10:39:22	CREDITO	57.75	1	0	\N	\N	\N	\N	\N
27	21	103	4	0	2020-07-11 12:28:14	CREDITO	168	1	0	\N	\N	\N	\N	\N
28	21	103	4	0	2020-07-11 12:28:14	OTROS	0.5	1	0	\N	\N	\N	\N	\N
29	22	85	9	0	2020-07-11 07:21:18	CREDITO	21.6000004	2	0	\N	\N	\N	\N	\N
30	23	86	9	0	2020-07-11 07:22:42	CREDITO	30.2000008	2	0	\N	\N	\N	\N	\N
31	24	55	4	0	2020-07-13 10:33:28	CREDITO	346	1	0	\N	\N	\N	\N	\N
32	25	108	4	0	2020-07-13 10:51:47	CREDITO	12.5	1	0	\N	\N	\N	\N	\N
33	26	11	4	0	2020-07-13 11:23:20	CREDITO	742	2	0	\N	\N	\N	\N	\N
34	27	108	4	0	2020-07-13 01:00:09	CREDITO	12.5	2	0	\N	\N	\N	\N	\N
35	28	72	4	0	2020-07-13 01:31:51	CREDITO	80	4	0	\N	\N	\N	\N	\N
36	29	67	4	0	2020-07-13 01:32:59	CREDITO	41.2000008	3	0	\N	\N	\N	\N	\N
37	30	85	9	0	2020-07-13 04:12:03	CREDITO	21.6000004	3	0	\N	\N	\N	\N	\N
38	31	86	9	0	2020-07-13 04:12:58	CREDITO	30.2000008	3	0	\N	\N	\N	\N	\N
39	32	102	9	0	2020-07-13 04:13:41	CREDITO	30.2000008	1	0	\N	\N	\N	\N	\N
40	33	101	9	0	2020-07-13 04:14:20	CREDITO	20.7999992	1	0	\N	\N	\N	\N	\N
41	34	101	9	0	2020-07-14 03:55:00	CREDITO	20.7999992	2	0	\N	\N	\N	\N	\N
42	35	102	9	0	2020-07-14 03:55:33	CREDITO	30.2000008	2	0	\N	\N	\N	\N	\N
43	36	86	9	0	2020-07-14 03:56:02	CREDITO	30.2000008	4	0	\N	\N	\N	\N	\N
44	37	85	9	0	2020-07-14 03:56:28	CREDITO	21.6000004	4	0	\N	\N	\N	\N	\N
45	38	108	4	0	2020-07-15 09:06:33	CREDITO	12.5	3	0	\N	\N	\N	\N	\N
46	39	108	4	0	2020-07-15 12:35:00	CREDITO	12.5	4	0	\N	\N	\N	\N	\N
47	40	15	4	0	2020-07-15 02:41:11	CREDITO	16.3999996	11	0	\N	\N	\N	\N	\N
48	41	85	9	0	2020-07-15 03:55:18	CREDITO	21.6000004	5	0	\N	\N	\N	\N	\N
49	42	86	9	0	2020-07-15 03:55:57	CREDITO	30.2000008	5	0	\N	\N	\N	\N	\N
50	43	102	9	0	2020-07-15 03:56:35	CREDITO	30.2000008	3	0	\N	\N	\N	\N	\N
51	44	101	9	0	2020-07-15 03:58:03	CREDITO	41.5999985	4	0	\N	\N	\N	\N	\N
52	45	108	4	0	2020-07-16 09:58:32	CREDITO	237.5	23	0	\N	\N	\N	\N	\N
53	46	67	4	0	2020-07-16 11:09:16	CREDITO	61.7999992	6	0	\N	\N	\N	\N	\N
54	47	72	4	0	2020-07-16 11:14:15	CREDITO	120	7	0	\N	\N	\N	\N	\N
55	48	22	4	0	2020-07-16 01:04:53	CREDITO	226.899994	1	0	\N	\N	\N	\N	\N
56	48	22	4	0	2020-07-16 01:04:53	OTROS	9	1	0	\N	\N	\N	\N	\N
57	48	22	4	0	2020-07-16 01:04:53	DES_PRECANCELACION	0.100000001	1	0	\N	\N	\N	\N	\N
58	48	22	4	0	2020-07-16 01:04:53	DES_CONDONACION	5.4000001	1	0	\N	\N	\N	\N	\N
59	49	67	4	0	2020-07-16 01:24:15	CREDITO	20.6000004	7	0	\N	\N	\N	\N	\N
60	50	108	4	0	2020-07-16 01:25:55	CREDITO	12.5	24	0	\N	\N	\N	\N	\N
61	51	15	4	0	2020-07-16 02:34:55	CREDITO	8.19999981	12	0	\N	\N	\N	\N	\N
62	52	85	9	0	2020-07-16 07:29:07	CREDITO	21.6000004	6	0	\N	\N	\N	\N	\N
63	53	86	9	0	2020-07-16 07:29:48	CREDITO	30.2000008	6	0	\N	\N	\N	\N	\N
64	54	101	9	0	2020-07-16 07:30:50	CREDITO	20.7999992	5	0	\N	\N	\N	\N	\N
65	55	118	4	0	2020-07-17 09:24:48	CREDITO	12.5	1	0	\N	\N	\N	\N	\N
66	56	116	4	0	2020-07-17 09:27:16	CREDITO	21.6000004	2	0	\N	\N	\N	\N	\N
67	57	77	4	0	2020-07-17 09:31:04	CREDITO	249.600006	4	0	\N	\N	\N	\N	\N
68	57	77	4	0	2020-07-17 09:31:04	MORAS	6	4	0	\N	\N	\N	\N	\N
69	57	77	4	0	2020-07-17 09:31:04	OTROS	0.5	4	0	\N	\N	\N	\N	\N
70	58	62	4	0	2020-07-17 10:35:56	CREDITO	57.75	2	0	\N	\N	\N	\N	\N
71	59	72	4	0	2020-07-17 12:45:52	CREDITO	40	8	0	\N	\N	\N	\N	\N
72	60	113	4	0	2020-07-17 01:11:29	CREDITO	50.4000015	6	0	\N	\N	\N	\N	\N
73	61	108	4	0	2020-07-17 01:12:13	CREDITO	12.5	25	0	\N	\N	\N	\N	\N
74	62	67	4	0	2020-07-17 01:13:09	CREDITO	20.6000004	8	0	\N	\N	\N	\N	\N
75	63	115	4	0	2020-07-17 01:13:48	CREDITO	9.80000019	1	0	\N	\N	\N	\N	\N
76	64	85	9	0	2020-07-17 06:48:27	CREDITO	21.6000004	7	0	\N	\N	\N	\N	\N
77	65	86	9	0	2020-07-17 06:50:11	CREDITO	30.2000008	7	0	\N	\N	\N	\N	\N
78	66	102	9	0	2020-07-17 06:51:16	CREDITO	60.4000015	5	0	\N	\N	\N	\N	\N
79	67	108	4	0	2020-07-18 11:06:19	CREDITO	12.5	26	0	\N	\N	\N	\N	\N
80	68	115	4	0	2020-07-20 09:21:14	CREDITO	9.80000019	2	0	\N	\N	\N	\N	\N
81	69	67	4	0	2020-07-20 09:24:41	CREDITO	20.6000004	9	0	\N	\N	\N	\N	\N
82	70	72	4	0	2020-07-20 09:30:42	CREDITO	40	9	0	\N	\N	\N	\N	\N
83	71	15	4	0	2020-07-20 01:06:48	CREDITO	8.19999981	13	0	\N	\N	\N	\N	\N
84	72	115	4	0	2020-07-20 01:10:32	CREDITO	9.80000019	3	0	\N	\N	\N	\N	\N
85	73	124	4	0	2020-07-20 01:12:21	CREDITO	12.5	1	0	\N	\N	\N	\N	\N
86	74	67	4	0	2020-07-20 01:13:07	CREDITO	20.6000004	10	0	\N	\N	\N	\N	\N
87	75	72	4	0	2020-07-20 02:39:59	CREDITO	40	10	0	\N	\N	\N	\N	\N
88	76	107	9	0	2020-07-20 09:52:20	CREDITO	135	1	0	\N	\N	\N	\N	\N
89	77	85	9	0	2020-07-20 09:53:31	CREDITO	43.2000008	9	0	\N	\N	\N	\N	\N
90	78	101	9	0	2020-07-20 09:54:29	CREDITO	62.4000015	8	0	\N	\N	\N	\N	\N
91	79	102	9	0	2020-07-20 09:55:14	CREDITO	30.2000008	6	0	\N	\N	\N	\N	\N
92	80	102	9	0	2020-07-20 09:55:42	CREDITO	30.2000008	7	0	\N	\N	\N	\N	\N
93	81	86	9	0	2020-07-20 09:56:53	CREDITO	60.4000015	9	0	\N	\N	\N	\N	\N
94	82	108	4	0	2020-07-22 09:27:19	OTROS	1	26	0	\N	\N	\N	\N	\N
95	83	118	4	0	2020-07-22 09:38:43	CREDITO	9.80000019	1	0	\N	\N	\N	\N	\N
96	84	15	4	0	2020-07-22 11:53:54	CREDITO	8.19999981	14	0	\N	\N	\N	\N	\N
97	85	14	4	0	2020-07-22 12:48:27	CREDITO	943	0	0	\N	\N	\N	\N	\N
98	86	14	4	0	2020-07-22 12:48:56	CREDITO	141	1	0	\N	\N	\N	\N	\N
99	86	14	4	0	2020-07-22 12:48:56	MORAS	1.5	1	0	\N	\N	\N	\N	\N
100	86	14	4	0	2020-07-22 12:48:56	OTROS	15	1	0	\N	\N	\N	\N	\N
101	87	118	4	0	2020-07-22 12:56:13	CREDITO	52.7000008	6	0	\N	\N	\N	\N	\N
102	88	115	4	0	2020-07-22 12:58:05	CREDITO	9.80000019	4	0	\N	\N	\N	\N	\N
103	89	124	4	0	2020-07-22 01:17:20	CREDITO	12.5	2	0	\N	\N	\N	\N	\N
104	90	72	4	0	2020-07-22 01:26:13	CREDITO	595.700012	25	0	\N	\N	\N	\N	\N
105	91	37	4	0	2020-07-22 01:41:44	CREDITO	84	1	0	\N	\N	\N	\N	\N
106	92	115	4	0	2020-07-22 01:42:31	CREDITO	9.80000019	5	0	\N	\N	\N	\N	\N
107	93	67	4	0	2020-07-22 01:43:23	CREDITO	20.6000004	11	0	\N	\N	\N	\N	\N
108	94	85	9	0	2020-07-22 07:51:59	CREDITO	43.2000008	11	0	\N	\N	\N	\N	\N
109	95	102	9	0	2020-07-22 07:52:46	CREDITO	120.800003	11	0	\N	\N	\N	\N	\N
110	96	101	9	0	2020-07-22 07:53:21	CREDITO	20.7999992	9	0	\N	\N	\N	\N	\N
111	97	86	9	0	2020-07-22 07:53:50	CREDITO	60.4000015	11	0	\N	\N	\N	\N	\N
112	98	124	4	0	2020-07-23 01:31:50	CREDITO	12.5	3	0	\N	\N	\N	\N	\N
113	99	67	4	0	2020-07-23 01:34:48	CREDITO	20.6000004	12	0	\N	\N	\N	\N	\N
114	100	101	9	0	2020-07-23 06:14:37	CREDITO	20.7999992	10	0	\N	\N	\N	\N	\N
115	101	86	9	0	2020-07-23 06:15:18	CREDITO	30.2000008	12	0	\N	\N	\N	\N	\N
116	102	102	9	0	2020-07-23 06:16:30	CREDITO	90.5999985	14	0	\N	\N	\N	\N	\N
117	103	85	9	0	2020-07-23 06:17:27	CREDITO	21.6000004	12	0	\N	\N	\N	\N	\N
118	104	113	4	0	2020-07-24 12:26:42	CREDITO	50.5	12	0	\N	\N	\N	\N	\N
119	105	67	4	0	2020-07-24 12:27:32	CREDITO	20.6000004	13	0	\N	\N	\N	\N	\N
120	106	124	4	0	2020-07-24 12:28:26	CREDITO	12.5	4	0	\N	\N	\N	\N	\N
121	107	115	4	0	2020-07-24 12:29:14	CREDITO	19.6000004	7	0	\N	\N	\N	\N	\N
122	108	127	4	0	2020-07-24 01:49:57	CREDITO	34	2	0	\N	\N	\N	\N	\N
123	109	15	4	0	2020-07-24 01:55:30	CREDITO	16.3999996	16	0	\N	\N	\N	\N	\N
124	110	124	4	0	2020-07-25 12:03:32	CREDITO	12.5	5	0	\N	\N	\N	\N	\N
125	111	67	4	0	2020-07-25 12:06:43	CREDITO	20.6000004	14	0	\N	\N	\N	\N	\N
126	112	115	4	0	2020-07-25 12:07:45	CREDITO	9.80000019	8	0	\N	\N	\N	\N	\N
127	113	62	4	0	2020-07-25 12:11:26	CREDITO	57.75	3	0	\N	\N	\N	\N	\N
128	114	85	9	0	2020-07-25 05:21:09	CREDITO	43.2000008	14	0	\N	\N	\N	\N	\N
129	115	86	9	0	2020-07-25 05:22:10	CREDITO	60.4000015	14	0	\N	\N	\N	\N	\N
130	116	102	9	0	2020-07-25 05:23:05	CREDITO	181.199997	20	0	\N	\N	\N	\N	\N
131	117	101	9	0	2020-07-25 05:23:33	CREDITO	20.7999992	11	0	\N	\N	\N	\N	\N
132	118	101	9	0	2020-07-25 05:23:53	CREDITO	20.7999992	12	0	\N	\N	\N	\N	\N
133	119	85	9	0	2020-07-29 04:14:49	CREDITO	43.2000008	16	0	\N	\N	\N	\N	\N
134	120	101	9	0	2020-07-29 04:16:03	CREDITO	62.4000015	15	0	\N	\N	\N	\N	\N
135	121	125	9	0	2020-07-29 04:16:45	CREDITO	224	1	0	\N	\N	\N	\N	\N
136	122	86	9	0	2020-07-29 04:17:43	CREDITO	90.5999985	17	0	\N	\N	\N	\N	\N
137	123	107	9	0	2020-07-29 04:18:10	CREDITO	135	2	0	\N	\N	\N	\N	\N
138	124	102	9	0	2020-07-29 04:19:15	CREDITO	181.199997	26	0	\N	\N	\N	\N	\N
139	125	85	9	0	2020-07-30 04:25:55	CREDITO	43.2000008	18	0	\N	\N	\N	\N	\N
140	126	101	9	0	2020-07-30 04:29:12	CREDITO	20.7999992	16	0	\N	\N	\N	\N	\N
141	127	86	9	0	2020-07-30 04:29:58	CREDITO	30.2000008	18	0	\N	\N	\N	\N	\N
142	128	85	9	0	2020-07-31 03:42:28	CREDITO	43.2000008	20	0	\N	\N	\N	\N	\N
143	129	101	9	0	2020-07-31 03:43:03	CREDITO	20.7999992	17	0	\N	\N	\N	\N	\N
144	130	86	9	0	2020-07-31 03:43:30	CREDITO	30.2000008	19	0	\N	\N	\N	\N	\N
145	131	86	9	0	2020-08-01 06:41:40	CREDITO	30.2000008	20	0	\N	\N	\N	\N	\N
146	132	101	9	0	2020-08-01 06:42:25	CREDITO	20.7999992	18	0	\N	\N	\N	\N	\N
147	133	128	9	0	2020-08-01 06:43:02	CREDITO	43.0999985	1	0	\N	\N	\N	\N	\N
148	134	129	9	0	2020-08-01 06:43:32	CREDITO	21.2000008	1	0	\N	\N	\N	\N	\N
149	135	85	9	0	2020-08-01 06:44:37	CREDITO	129.600006	26	0	\N	\N	\N	\N	\N
150	136	107	9	0	2020-08-01 06:45:21	CREDITO	135	3	0	\N	\N	\N	\N	\N
151	137	86	9	0	2020-08-03 09:46:18	CREDITO	30.2000008	21	0	\N	\N	\N	\N	\N
152	138	128	9	0	2020-08-03 09:46:50	CREDITO	43.0999985	2	0	\N	\N	\N	\N	\N
153	139	101	9	0	2020-08-03 09:47:17	CREDITO	20.7999992	19	0	\N	\N	\N	\N	\N
154	140	129	9	0	2020-08-03 09:47:44	CREDITO	21.2000008	2	0	\N	\N	\N	\N	\N
155	141	128	9	0	2020-08-04 08:43:02	CREDITO	43.0999985	3	0	\N	\N	\N	\N	\N
156	142	129	9	0	2020-08-04 08:43:41	CREDITO	21.2000008	3	0	\N	\N	\N	\N	\N
157	143	86	9	0	2020-08-04 08:51:48	CREDITO	30.2000008	22	0	\N	\N	\N	\N	\N
158	144	125	9	0	2020-08-04 08:52:13	CREDITO	224	2	0	\N	\N	\N	\N	\N
159	145	130	9	0	2020-08-05 09:25:53	CREDITO	43.2000008	2	0	\N	\N	\N	\N	\N
160	146	86	9	0	2020-08-05 09:28:13	CREDITO	30.2000008	23	0	\N	\N	\N	\N	\N
161	147	128	9	0	2020-08-05 09:29:06	CREDITO	43.0999985	4	0	\N	\N	\N	\N	\N
162	148	101	9	0	2020-08-05 09:29:46	CREDITO	41.5999985	21	0	\N	\N	\N	\N	\N
163	149	129	9	0	2020-08-05 09:30:16	CREDITO	21.2000008	4	0	\N	\N	\N	\N	\N
164	150	129	9	0	2020-08-06 06:11:18	CREDITO	21.2000008	5	0	\N	\N	\N	\N	\N
165	151	86	9	0	2020-08-06 06:11:58	CREDITO	30.2000008	24	0	\N	\N	\N	\N	\N
166	152	101	9	0	2020-08-06 06:12:40	CREDITO	20.7999992	22	0	\N	\N	\N	\N	\N
167	153	130	9	0	2020-08-06 06:13:17	CREDITO	21.6000004	3	0	\N	\N	\N	\N	\N
168	154	107	9	0	2020-08-06 06:14:22	CREDITO	135	4	0	\N	\N	\N	\N	\N
169	155	101	9	0	2020-08-07 05:25:49	CREDITO	41.5999985	24	0	\N	\N	\N	\N	\N
170	156	128	9	0	2020-08-07 05:26:38	CREDITO	86.1999969	6	0	\N	\N	\N	\N	\N
171	157	86	9	0	2020-08-07 05:27:31	CREDITO	60.4000015	26	0	\N	\N	\N	\N	\N
172	158	130	9	0	2020-08-07 05:28:13	CREDITO	21.6000004	4	0	\N	\N	\N	\N	\N
173	159	129	9	0	2020-08-07 05:28:54	CREDITO	21.2000008	6	0	\N	\N	\N	\N	\N
174	160	111	4	0	2020-08-10 10:14:09	CREDITO	535	1	0	\N	\N	\N	\N	\N
175	161	101	9	0	2020-08-10 07:16:31	CREDITO	20.7999992	25	0	\N	\N	\N	\N	\N
176	162	128	9	0	2020-08-10 07:17:11	CREDITO	43.0999985	7	0	\N	\N	\N	\N	\N
177	163	130	9	0	2020-08-10 07:18:21	CREDITO	43.2000008	6	0	\N	\N	\N	\N	\N
178	164	132	9	0	2020-08-10 07:18:56	CREDITO	34.5	1	0	\N	\N	\N	\N	\N
179	165	129	9	0	2020-08-10 07:19:42	CREDITO	21.2000008	7	0	\N	\N	\N	\N	\N
180	166	93	4	0	2020-08-11 09:32:42	CREDITO	82.5	1	0	\N	\N	\N	\N	\N
181	167	92	4	0	2020-08-11 09:34:19	CREDITO	77	1	0	\N	\N	\N	\N	\N
182	168	94	4	0	2020-08-11 09:35:44	CREDITO	86.6999969	1	0	\N	\N	\N	\N	\N
183	169	129	9	0	2020-08-11 05:50:47	CREDITO	42.4000015	9	0	\N	\N	\N	\N	\N
184	170	101	9	0	2020-08-11 05:51:34	CREDITO	20.7999992	26	0	\N	\N	\N	\N	\N
185	171	130	9	0	2020-08-11 05:52:34	CREDITO	21.6000004	7	0	\N	\N	\N	\N	\N
186	172	125	9	0	2020-08-11 05:53:08	CREDITO	224	3	0	\N	\N	\N	\N	\N
187	173	128	9	0	2020-08-11 05:53:43	CREDITO	43.0999985	8	0	\N	\N	\N	\N	\N
188	174	132	9	0	2020-08-11 05:54:20	CREDITO	34.5	2	0	\N	\N	\N	\N	\N
189	175	15	4	0	2020-08-12 01:43:47	CREDITO	16.3999996	18	0	\N	\N	\N	\N	\N
190	176	132	9	0	2020-08-14 06:12:23	CREDITO	103.5	5	0	\N	\N	\N	\N	\N
191	177	130	9	0	2020-08-14 06:13:47	CREDITO	43.2000008	9	0	\N	\N	\N	\N	\N
192	178	129	9	0	2020-08-14 06:15:22	CREDITO	42.4000015	11	0	\N	\N	\N	\N	\N
193	179	128	9	0	2020-08-14 06:16:17	CREDITO	129.300003	11	0	\N	\N	\N	\N	\N
194	180	15	4	0	2020-08-18 01:35:27	CREDITO	8.19999981	19	0	\N	\N	\N	\N	\N
195	181	15	4	0	2020-08-18 01:37:29	CREDITO	8.19999981	20	0	\N	\N	\N	\N	\N
196	182	128	9	0	2020-08-18 05:57:55	CREDITO	86.1999969	13	0	\N	\N	\N	\N	\N
197	183	132	9	0	2020-08-18 05:59:03	CREDITO	69	7	0	\N	\N	\N	\N	\N
198	184	125	9	0	2020-08-18 05:59:56	CREDITO	224	4	0	\N	\N	\N	\N	\N
199	185	130	9	0	2020-08-18 06:00:45	CREDITO	86.4000015	13	0	\N	\N	\N	\N	\N
200	186	131	9	0	2020-08-18 06:01:27	CREDITO	162	1	0	\N	\N	\N	\N	\N
201	187	129	9	0	2020-08-18 06:02:51	CREDITO	63.5999985	14	0	\N	\N	\N	\N	\N
202	188	129	9	0	2020-08-23 10:38:03	CREDITO	21.2000008	15	0	\N	\N	\N	\N	\N
203	189	130	9	0	2020-08-23 10:39:14	CREDITO	64.8000031	16	0	\N	\N	\N	\N	\N
204	190	132	9	0	2020-08-23 10:40:09	CREDITO	172.5	12	0	\N	\N	\N	\N	\N
205	191	128	9	0	2020-08-23 10:41:09	CREDITO	172.399994	17	0	\N	\N	\N	\N	\N
206	192	15	4	0	2020-08-24 11:59:32	CREDITO	8.19999981	21	0	\N	\N	\N	\N	\N
\.


--
-- Data for Name: operacion_banco; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.operacion_banco (id_operacion_banco, id_banco, operacion, monto, fecha_hora, usuario, agencia, cod_apertura, obs) FROM stdin;
\.


--
-- Data for Name: operacion_pago; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.operacion_pago (id_operacion_pago, id_caja_apertura_cierre, fecha_hora, total_pagar, pago_con, vuelto, agencia, puntos, id_agencia, obs_cancelacion) FROM stdin;
1	3848	2020-06-30 02:17:40	24.6000004	30	5.4000001	HUANCAYO                      	0	1	\N
2	3852	2020-07-02 01:10:03	8.19999981	10	1.79999995	HUANCAYO                      	0	1	\N
3	3853	2020-07-03 10:36:58	82	82	0	HUANCAYO                      	0	1	\N
4	3853	2020-07-03 02:06:49	520	520	0	HUANCAYO                      	0	1	\N
5	3853	2020-07-03 02:33:50	1080	1080	0	HUANCAYO                      	0	1	\N
6	3853	2020-07-03 02:42:56	782	782	0	HUANCAYO                      	0	1	\N
7	3855	2020-07-06 02:48:47	16.3999996	16.5	0.100000001	HUANCAYO                      	0	1	\N
8	3857	2020-07-07 11:57:11	128.399994	128.399994	0	PUCALLPA                      	0	3	\N
9	3859	2020-07-08 10:30:29	113.5	113.5	0	PUCALLPA                      	0	3	\N
10	3859	2020-07-08 10:32:13	128.699997	128.699997	0	PUCALLPA                      	0	3	\N
11	3861	2020-07-09 12:39:12	0.100000001	0.100000001	0	PUCALLPA                      	0	3	\N
12	3860	2020-07-09 01:07:24	324	324	0	HUANCAYO                      	0	1	\N
13	3862	2020-07-10 09:50:49	80	80	0	HUANCAYO                      	0	1	\N
14	3862	2020-07-10 02:46:53	119.349998	120	0.649999976	HUANCAYO                      	0	1	\N
15	3862	2020-07-10 03:05:38	24.6000004	24.6000004	0	HUANCAYO                      	0	1	\N
16	3863	2020-07-10 10:11:41	21.5	21.5	0	PUCALLPA                      	0	3	\N
17	3863	2020-07-10 10:12:23	30.2000008	30.2000008	0	PUCALLPA                      	0	3	\N
18	3864	2020-07-11 09:21:53	20.6000004	20.6000004	0	HUANCAYO                      	0	1	\N
19	3864	2020-07-11 09:22:30	40	40	0	HUANCAYO                      	0	1	\N
20	3864	2020-07-11 10:39:22	57.75	60	2.25	HUANCAYO                      	0	1	\N
21	3864	2020-07-11 12:28:14	168.5	168.5	0	HUANCAYO                      	0	1	\N
22	3865	2020-07-11 07:21:18	21.6000004	21.6000004	0	PUCALLPA                      	0	3	\N
23	3865	2020-07-11 07:22:42	30.2000008	30.2000008	0	PUCALLPA                      	0	3	\N
24	3866	2020-07-13 10:33:28	346	346	0	HUANCAYO                      	0	1	\N
25	3866	2020-07-13 10:51:47	12.5	12.5	0	HUANCAYO                      	0	1	\N
26	3866	2020-07-13 11:23:20	742	750	8	HUANCAYO                      	0	1	\N
27	3866	2020-07-13 01:00:09	12.5	12.5	0	HUANCAYO                      	0	1	\N
28	3866	2020-07-13 01:31:51	80	80	0	HUANCAYO                      	0	1	\N
29	3866	2020-07-13 01:32:59	41.2000008	41.2000008	0	HUANCAYO                      	0	1	\N
30	3867	2020-07-13 04:12:03	21.6000004	21.6000004	0	PUCALLPA                      	0	3	\N
31	3867	2020-07-13 04:12:58	30.2000008	30.2000008	0	PUCALLPA                      	0	3	\N
32	3867	2020-07-13 04:13:41	30.2000008	30.2000008	0	PUCALLPA                      	0	3	\N
33	3867	2020-07-13 04:14:20	20.7999992	20.7999992	0	PUCALLPA                      	0	3	\N
34	3869	2020-07-14 03:55:00	20.7999992	20.7999992	0	PUCALLPA                      	0	3	\N
35	3869	2020-07-14 03:55:33	30.2000008	30.2000008	0	PUCALLPA                      	0	3	\N
36	3869	2020-07-14 03:56:02	30.2000008	30.2000008	0	PUCALLPA                      	0	3	\N
37	3869	2020-07-14 03:56:28	21.6000004	21.6000004	0	PUCALLPA                      	0	3	\N
38	3870	2020-07-15 09:06:33	12.5	12.5	0	HUANCAYO                      	0	1	\N
39	3870	2020-07-15 12:35:00	12.5	12.5	0	HUANCAYO                      	0	1	\N
40	3870	2020-07-15 02:41:11	16.3999996	16.5	0.100000001	HUANCAYO                      	0	1	\N
41	3871	2020-07-15 03:55:18	21.6000004	21.6000004	0	PUCALLPA                      	0	3	\N
42	3871	2020-07-15 03:55:57	30.2000008	30.2000008	0	PUCALLPA                      	0	3	\N
43	3871	2020-07-15 03:56:35	30.2000008	30.2000008	0	PUCALLPA                      	0	3	\N
44	3871	2020-07-15 03:58:03	41.5999985	41.5999985	0	PUCALLPA                      	0	3	\N
45	3872	2020-07-16 09:58:32	237.5	237.5	0	HUANCAYO                      	0	1	\N
46	3872	2020-07-16 11:09:16	61.7999992	61.7999992	0	HUANCAYO                      	0	1	\N
47	3872	2020-07-16 11:14:15	120	120	0	HUANCAYO                      	0	1	\N
48	3872	2020-07-16 01:04:53	230.399994	230.399994	0	HUANCAYO                      	0	1	\N
49	3872	2020-07-16 01:24:15	20.6000004	20.6000004	0	HUANCAYO                      	0	1	\N
50	3872	2020-07-16 01:25:55	12.5	12.5	0	HUANCAYO                      	0	1	\N
51	3872	2020-07-16 02:34:55	8.19999981	8.19999981	0	HUANCAYO                      	0	1	\N
52	3873	2020-07-16 07:29:07	21.6000004	21.6000004	0	PUCALLPA                      	0	3	\N
53	3873	2020-07-16 07:29:48	30.2000008	30.2000008	0	PUCALLPA                      	0	3	\N
54	3873	2020-07-16 07:30:50	20.7999992	20.7999992	0	PUCALLPA                      	0	3	\N
55	3874	2020-07-17 09:24:48	12.5	12.5	0	HUANCAYO                      	0	1	\N
56	3874	2020-07-17 09:27:16	21.6000004	21.6000004	0	HUANCAYO                      	0	1	\N
57	3874	2020-07-17 09:31:04	256.100006	256.100006	0	HUANCAYO                      	0	1	\N
58	3874	2020-07-17 10:35:56	57.75	60	2.25	HUANCAYO                      	0	1	\N
59	3874	2020-07-17 12:45:52	40	40	0	HUANCAYO                      	0	1	\N
60	3874	2020-07-17 01:11:29	50.4000015	50.4000015	0	HUANCAYO                      	0	1	\N
61	3874	2020-07-17 01:12:13	12.5	12.5	0	HUANCAYO                      	0	1	\N
62	3874	2020-07-17 01:13:09	20.6000004	20.6000004	0	HUANCAYO                      	0	1	\N
63	3874	2020-07-17 01:13:48	9.80000019	9.80000019	0	HUANCAYO                      	0	1	\N
64	3875	2020-07-17 06:48:27	21.6000004	21.6000004	0	PUCALLPA                      	0	3	\N
65	3875	2020-07-17 06:50:11	30.2000008	30.2000008	0	PUCALLPA                      	0	3	\N
66	3875	2020-07-17 06:51:16	60.4000015	60.4000015	0	PUCALLPA                      	0	3	\N
67	3876	2020-07-18 11:06:19	12.5	12.5	0	HUANCAYO                      	0	1	\N
68	3877	2020-07-20 09:21:14	9.80000019	9.80000019	0	HUANCAYO                      	0	1	\N
69	3877	2020-07-20 09:24:41	20.6000004	20.6000004	0	HUANCAYO                      	0	1	\N
70	3877	2020-07-20 09:30:42	40	40	0	HUANCAYO                      	0	1	\N
71	3877	2020-07-20 01:06:48	8.19999981	8.5	0.300000012	HUANCAYO                      	0	1	\N
72	3877	2020-07-20 01:10:32	9.80000019	9.80000019	0	HUANCAYO                      	0	1	\N
73	3877	2020-07-20 01:12:21	12.5	12.5	0	HUANCAYO                      	0	1	\N
74	3877	2020-07-20 01:13:07	20.6000004	20.6000004	0	HUANCAYO                      	0	1	\N
75	3877	2020-07-20 02:39:59	40	40	0	HUANCAYO                      	0	1	\N
76	3878	2020-07-20 09:52:20	135	135	0	PUCALLPA                      	0	3	\N
77	3878	2020-07-20 09:53:31	43.2000008	43.2000008	0	PUCALLPA                      	0	3	\N
78	3878	2020-07-20 09:54:29	62.4000015	62.4000015	0	PUCALLPA                      	0	3	\N
79	3878	2020-07-20 09:55:14	30.2000008	30.2000008	0	PUCALLPA                      	0	3	\N
80	3878	2020-07-20 09:55:42	30.2000008	30.2000008	0	PUCALLPA                      	0	3	\N
81	3878	2020-07-20 09:56:53	60.4000015	60.4000015	0	PUCALLPA                      	0	3	\N
82	3879	2020-07-22 09:27:19	1	1	0	HUANCAYO                      	0	1	\N
83	3879	2020-07-22 09:38:43	9.80000019	9.80000019	0	HUANCAYO                      	0	1	\N
84	3879	2020-07-22 11:53:54	8.19999981	8.5	0.300000012	HUANCAYO                      	0	1	\N
85	3879	2020-07-22 12:48:27	943	943	0	HUANCAYO                      	0	1	\N
86	3879	2020-07-22 12:48:56	157.5	157.5	0	HUANCAYO                      	0	1	\N
87	3879	2020-07-22 12:56:13	52.7000008	52.7000008	0	HUANCAYO                      	0	1	\N
88	3879	2020-07-22 12:58:05	9.80000019	9.80000019	0	HUANCAYO                      	0	1	\N
89	3879	2020-07-22 01:17:20	12.5	12.5	0	HUANCAYO                      	0	1	\N
90	3879	2020-07-22 01:26:13	595.700012	595.700012	0	HUANCAYO                      	0	1	\N
91	3879	2020-07-22 01:41:44	84	84	0	HUANCAYO                      	0	1	\N
92	3879	2020-07-22 01:42:31	9.80000019	9.80000019	0	HUANCAYO                      	0	1	\N
93	3879	2020-07-22 01:43:23	20.6000004	20.6000004	0	HUANCAYO                      	0	1	\N
94	3880	2020-07-22 07:51:59	43.2000008	43.2000008	0	PUCALLPA                      	0	3	\N
95	3880	2020-07-22 07:52:46	120.800003	120.800003	0	PUCALLPA                      	0	3	\N
96	3880	2020-07-22 07:53:21	20.7999992	20.7999992	0	PUCALLPA                      	0	3	\N
97	3880	2020-07-22 07:53:50	60.4000015	60.4000015	0	PUCALLPA                      	0	3	\N
98	3881	2020-07-23 01:31:50	12.5	12.5	0	HUANCAYO                      	0	1	\N
99	3881	2020-07-23 01:34:48	20.6000004	20.6000004	0	HUANCAYO                      	0	1	\N
100	3882	2020-07-23 06:14:37	20.7999992	20.7999992	0	PUCALLPA                      	0	3	\N
101	3882	2020-07-23 06:15:18	30.2000008	30.2000008	0	PUCALLPA                      	0	3	\N
102	3882	2020-07-23 06:16:30	90.5999985	90.5999985	0	PUCALLPA                      	0	3	\N
103	3882	2020-07-23 06:17:27	21.6000004	21.6000004	0	PUCALLPA                      	0	3	\N
104	3883	2020-07-24 12:26:42	50.5	50.5	0	HUANCAYO                      	0	1	\N
105	3883	2020-07-24 12:27:32	20.6000004	20.6000004	0	HUANCAYO                      	0	1	\N
106	3883	2020-07-24 12:28:26	12.5	12.5	0	HUANCAYO                      	0	1	\N
107	3883	2020-07-24 12:29:14	19.6000004	19.6000004	0	HUANCAYO                      	0	1	\N
108	3883	2020-07-24 01:49:57	34	34	0	HUANCAYO                      	0	1	\N
109	3883	2020-07-24 01:55:30	16.3999996	16.3999996	0	HUANCAYO                      	0	1	\N
110	3884	2020-07-25 12:03:32	12.5	12.5	0	HUANCAYO                      	0	1	\N
111	3884	2020-07-25 12:06:43	20.6000004	20.6000004	0	HUANCAYO                      	0	1	\N
112	3884	2020-07-25 12:07:45	9.80000019	9.80000019	0	HUANCAYO                      	0	1	\N
113	3884	2020-07-25 12:11:26	57.75	57.75	0	HUANCAYO                      	0	1	\N
114	3885	2020-07-25 05:21:09	43.2000008	43.2000008	0	PUCALLPA                      	0	3	\N
115	3885	2020-07-25 05:22:10	60.4000015	60.4000015	0	PUCALLPA                      	0	3	\N
116	3885	2020-07-25 05:23:05	181.199997	181.199997	0	PUCALLPA                      	0	3	\N
117	3885	2020-07-25 05:23:33	20.7999992	20.7999992	0	PUCALLPA                      	0	3	\N
118	3885	2020-07-25 05:23:53	20.7999992	20.7999992	0	PUCALLPA                      	0	3	\N
119	3887	2020-07-29 04:14:49	43.2000008	43.5999985	0.400000006	PUCALLPA                      	0	3	\N
120	3887	2020-07-29 04:16:03	62.4000015	62.4000015	0	PUCALLPA                      	0	3	\N
121	3887	2020-07-29 04:16:45	224	224	0	PUCALLPA                      	0	3	\N
122	3887	2020-07-29 04:17:43	90.5999985	90.5999985	0	PUCALLPA                      	0	3	\N
123	3887	2020-07-29 04:18:10	135	135	0	PUCALLPA                      	0	3	\N
124	3887	2020-07-29 04:19:15	181.199997	181.199997	0	PUCALLPA                      	0	3	\N
125	3888	2020-07-30 04:25:55	43.2000008	43.2000008	0	PUCALLPA                      	0	3	\N
126	3888	2020-07-30 04:29:12	20.7999992	20.7999992	0	PUCALLPA                      	0	3	\N
127	3888	2020-07-30 04:29:58	30.2000008	30.2000008	0	PUCALLPA                      	0	3	\N
128	3889	2020-07-31 03:42:28	43.2000008	43.2000008	0	PUCALLPA                      	0	3	\N
129	3889	2020-07-31 03:43:03	20.7999992	20.7999992	0	PUCALLPA                      	0	3	\N
130	3889	2020-07-31 03:43:30	30.2000008	30.2000008	0	PUCALLPA                      	0	3	\N
131	3890	2020-08-01 06:41:40	30.2000008	30.2000008	0	PUCALLPA                      	0	3	\N
132	3890	2020-08-01 06:42:25	20.7999992	20.7999992	0	PUCALLPA                      	0	3	\N
133	3890	2020-08-01 06:43:02	43.0999985	43.0999985	0	PUCALLPA                      	0	3	\N
134	3890	2020-08-01 06:43:32	21.2000008	21.2000008	0	PUCALLPA                      	0	3	\N
135	3890	2020-08-01 06:44:37	129.600006	129.600006	0	PUCALLPA                      	0	3	\N
136	3890	2020-08-01 06:45:21	135	135	0	PUCALLPA                      	0	3	\N
137	3891	2020-08-03 09:46:18	30.2000008	30.2000008	0	PUCALLPA                      	0	3	\N
138	3891	2020-08-03 09:46:50	43.0999985	43.0999985	0	PUCALLPA                      	0	3	\N
139	3891	2020-08-03 09:47:17	20.7999992	20.7999992	0	PUCALLPA                      	0	3	\N
140	3891	2020-08-03 09:47:44	21.2000008	21.2000008	0	PUCALLPA                      	0	3	\N
141	3892	2020-08-04 08:43:02	43.0999985	43.0999985	0	PUCALLPA                      	0	3	\N
142	3892	2020-08-04 08:43:41	21.2000008	21.2000008	0	PUCALLPA                      	0	3	\N
143	3892	2020-08-04 08:51:48	30.2000008	30.2000008	0	PUCALLPA                      	0	3	\N
144	3892	2020-08-04 08:52:13	224	224	0	PUCALLPA                      	0	3	\N
145	3893	2020-08-05 09:25:53	43.2000008	43.2000008	0	PUCALLPA                      	0	3	\N
146	3893	2020-08-05 09:28:13	30.2000008	30.2000008	0	PUCALLPA                      	0	3	\N
147	3893	2020-08-05 09:29:06	43.0999985	43.0999985	0	PUCALLPA                      	0	3	\N
148	3893	2020-08-05 09:29:46	41.5999985	41.5999985	0	PUCALLPA                      	0	3	\N
149	3893	2020-08-05 09:30:16	21.2000008	21.2000008	0	PUCALLPA                      	0	3	\N
150	3894	2020-08-06 06:11:18	21.2000008	21.2000008	0	PUCALLPA                      	0	3	\N
151	3894	2020-08-06 06:11:58	30.2000008	30.2000008	0	PUCALLPA                      	0	3	\N
152	3894	2020-08-06 06:12:40	20.7999992	20.7999992	0	PUCALLPA                      	0	3	\N
153	3894	2020-08-06 06:13:17	21.6000004	21.6000004	0	PUCALLPA                      	0	3	\N
154	3894	2020-08-06 06:14:22	135	135	0	PUCALLPA                      	0	3	\N
155	3895	2020-08-07 05:25:49	41.5999985	41.5999985	0	PUCALLPA                      	0	3	\N
156	3895	2020-08-07 05:26:38	86.1999969	86.1999969	0	PUCALLPA                      	0	3	\N
157	3895	2020-08-07 05:27:31	60.4000015	60.4000015	0	PUCALLPA                      	0	3	\N
158	3895	2020-08-07 05:28:13	21.6000004	21.6000004	0	PUCALLPA                      	0	3	\N
159	3895	2020-08-07 05:28:54	21.2000008	21.2000008	0	PUCALLPA                      	0	3	\N
160	3897	2020-08-10 10:14:09	535	535	0	HUANCAYO                      	0	1	\N
161	3898	2020-08-10 07:16:31	20.7999992	20.7999992	0	PUCALLPA                      	0	3	\N
162	3898	2020-08-10 07:17:11	43.0999985	43.0999985	0	PUCALLPA                      	0	3	\N
163	3898	2020-08-10 07:18:21	43.2000008	43.2000008	0	PUCALLPA                      	0	3	\N
164	3898	2020-08-10 07:18:56	34.5	34.5	0	PUCALLPA                      	0	3	\N
165	3898	2020-08-10 07:19:42	21.2000008	21.2000008	0	PUCALLPA                      	0	3	\N
166	3899	2020-08-11 09:32:42	82.5	82.5	0	HUANCAYO                      	0	1	\N
167	3899	2020-08-11 09:34:19	77	77	0	HUANCAYO                      	0	1	\N
168	3899	2020-08-11 09:35:44	86.6999969	86.6999969	0	HUANCAYO                      	0	1	\N
169	3900	2020-08-11 05:50:47	42.4000015	42.4000015	0	PUCALLPA                      	0	3	\N
170	3900	2020-08-11 05:51:34	20.7999992	20.7999992	0	PUCALLPA                      	0	3	\N
171	3900	2020-08-11 05:52:34	21.6000004	21.6000004	0	PUCALLPA                      	0	3	\N
172	3900	2020-08-11 05:53:08	224	224	0	PUCALLPA                      	0	3	\N
173	3900	2020-08-11 05:53:43	43.0999985	43.0999985	0	PUCALLPA                      	0	3	\N
174	3900	2020-08-11 05:54:20	34.5	34.5	0	PUCALLPA                      	0	3	\N
175	3901	2020-08-12 01:43:47	16.3999996	20	3.5999999	HUANCAYO                      	0	1	\N
176	3903	2020-08-14 06:12:23	103.5	103.5	0	PUCALLPA                      	0	3	\N
177	3903	2020-08-14 06:13:47	43.2000008	43.2000008	0	PUCALLPA                      	0	3	\N
178	3903	2020-08-14 06:15:22	42.4000015	42.4000015	0	PUCALLPA                      	0	3	\N
179	3903	2020-08-14 06:16:17	129.300003	129.300003	0	PUCALLPA                      	0	3	\N
180	3904	2020-08-18 01:35:27	8.19999981	8.19999981	0	HUANCAYO                      	0	1	\N
181	3904	2020-08-18 01:37:29	8.19999981	8.19999981	0	HUANCAYO                      	0	1	\N
182	3905	2020-08-18 05:57:55	86.1999969	86.1999969	0	PUCALLPA                      	0	3	\N
183	3905	2020-08-18 05:59:03	69	69	0	PUCALLPA                      	0	3	\N
184	3905	2020-08-18 05:59:56	224	224	0	PUCALLPA                      	0	3	\N
185	3905	2020-08-18 06:00:45	86.4000015	86.4000015	0	PUCALLPA                      	0	3	\N
186	3905	2020-08-18 06:01:27	162	162	0	PUCALLPA                      	0	3	\N
187	3905	2020-08-18 06:02:51	63.5999985	63.5999985	0	PUCALLPA                      	0	3	\N
188	3908	2020-08-23 10:38:03	21.2000008	21.2000008	0	PUCALLPA                      	0	3	\N
189	3908	2020-08-23 10:39:14	64.8000031	64.8000031	0	PUCALLPA                      	0	3	\N
190	3908	2020-08-23 10:40:09	172.5	172.5	0	PUCALLPA                      	0	3	\N
191	3908	2020-08-23 10:41:09	172.399994	172.399994	0	PUCALLPA                      	0	3	\N
192	3909	2020-08-24 11:59:32	8.19999981	8.19999981	0	HUANCAYO                      	0	1	\N
\.


--
-- Data for Name: pariente; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.pariente (id_pariente, dniuno, nombreuno, parentescouno, dnidos, nombredos, parentescodos, estado) FROM stdin;
\.


--
-- Data for Name: planilla; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.planilla (id_planilla, cod_generacion, cod_aprobacion, cod_planilla, fecha_hora, personal, cargo, dni, sueldo, incentivos, bonificacion, faltantes, movil, tardanza, adelantos, uniforme, permisos, sanciones, otros, aporte_afp, aporte_onp, desc_total, neto_pagar, estado, oficina, fecha_hora_ejecutado) FROM stdin;
\.


--
-- Data for Name: provincia; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.provincia (id_provincia, id_departamento, provincia) FROM stdin;
1	1	CHANCHAMAYO
2	1	CHUPACA
3	1	CONCEPCIÓN
4	1	HUANCAYO
5	1	JAUJA
6	1	SATIPO
7	1	TARMA
8	1	YAULI
9	3	LEONCIO PRADO
10	4	PUCALLPA
\.


--
-- Data for Name: rpt_cobranza_diaria; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.rpt_cobranza_diaria (id_rpt_cobranza_diaria, fecha_hora, id_apertura, nombre_user, monto_inicio_total, monto_inicio_hoy, monto_fin_total, interes_fin_hoy, creditos_total, creditos_hoy, id_agencia) FROM stdin;
1	2020-06-29 03:57:05	4	4	0	0	\N	\N	\N	\N	\N
2	2020-06-30 10:06:44	4	4	0	0	\N	\N	\N	\N	\N
3	2020-07-01 09:56:15	9	9	1477	1477	\N	\N	\N	\N	\N
4	2020-07-02 08:48:55	4	4	1477	0	\N	\N	\N	\N	\N
5	2020-07-03 09:25:40	4	4	1507	10	\N	\N	\N	\N	\N
6	2020-07-04 09:40:21	4	4	1517	10	\N	\N	\N	\N	\N
7	2020-07-06 09:18:33	4	4	1535.20000000000005	18.1999999999999993	\N	\N	\N	\N	\N
8	2020-07-07 09:21:13	4	4	2518.75	710.350000000000023	\N	\N	\N	\N	\N
9	2020-07-08 09:15:52	4	4	2597.75	79	\N	\N	\N	\N	\N
10	2020-07-09 12:04:30	4	4	3040.90000000000009	443.149999999999977	\N	\N	\N	\N	\N
11	2020-07-10 09:38:43	4	4	3171.59999999999991	130.800000000000011	\N	\N	\N	\N	\N
12	2020-07-11 09:19:22	4	4	3811.15000000000009	250.449999999999989	\N	\N	\N	\N	\N
13	2020-07-13 09:10:28	4	4	4040.90000000000009	399.899999999999977	\N	\N	\N	\N	\N
14	2020-07-14 09:56:36	4	4	4496.44999999999982	679.549999999999955	\N	\N	\N	\N	\N
15	2020-07-15 09:05:39	4	4	5633.55000000000018	528.5	\N	\N	\N	\N	\N
16	2020-07-16 09:19:19	4	4	6739	1245.45000000000005	\N	\N	\N	\N	\N
17	2020-07-17 09:12:46	4	4	7388.30000000000018	336.899999999999977	\N	\N	\N	\N	\N
18	2020-07-18 10:47:35	4	4	8134.94999999999982	1050.95000000000005	\N	\N	\N	\N	\N
19	2020-07-20 09:18:50	4	4	8902.85000000000036	767.899999999999977	\N	\N	\N	\N	\N
20	2020-07-22 09:24:03	4	4	9835.79999999999927	451.899999999999977	\N	\N	\N	\N	\N
21	2020-07-23 09:18:56	4	4	9029.75	1104.54999999999995	\N	\N	\N	\N	\N
22	2020-07-24 10:01:44	4	4	9775.60000000000036	851.549999999999955	\N	\N	\N	\N	\N
23	2020-07-25 12:01:19	4	4	10477.1000000000004	829.799999999999955	\N	\N	\N	\N	\N
24	2020-07-27 11:32:59	4	4	10834.1499999999996	602.899999999999977	\N	\N	\N	\N	\N
25	2020-07-29 04:13:53	9	9	12314.7000000000007	0	\N	\N	\N	\N	\N
26	2020-07-30 04:25:01	9	9	13826.7800000000007	2067.2800000000002	\N	\N	\N	\N	\N
27	2020-07-31 02:14:48	9	9	14371.1299999999992	638.549999999999955	\N	\N	\N	\N	\N
28	2020-08-01 06:26:07	9	9	15052.9300000000003	690.100000000000023	\N	\N	\N	\N	\N
29	2020-08-03 09:38:22	9	9	15994.6499999999996	1192.01999999999998	\N	\N	\N	\N	\N
30	2020-08-04 08:41:53	9	9	16604.5499999999993	725.200000000000045	\N	\N	\N	\N	\N
31	2020-08-05 09:13:06	9	9	17494.6699999999983	1208.63000000000011	\N	\N	\N	\N	\N
32	2020-08-06 06:10:11	9	9	18476.7200000000012	1096.54999999999995	\N	\N	\N	\N	\N
33	2020-08-07 09:23:07	9	9	18737.869999999999	354.949999999999989	\N	\N	\N	\N	\N
34	2020-08-08 12:09:17	9	9	19600.0499999999993	1042.17000000000007	\N	\N	\N	\N	\N
35	2020-08-10 08:55:08	4	4	20154.4500000000007	519.899999999999977	\N	\N	\N	\N	\N
36	2020-08-11 09:18:12	4	4	19978.75	522.100000000000023	\N	\N	\N	\N	\N
37	2020-08-12 09:27:03	4	4	20100.3499999999985	754.200000000000045	\N	\N	\N	\N	\N
38	2020-08-14 09:01:56	4	4	22260.0200000000004	504.850000000000023	\N	\N	\N	\N	\N
39	2020-08-18 09:21:10	4	4	23443.4199999999983	665.200000000000045	\N	\N	\N	\N	\N
40	2020-08-20 09:30:10	4	4	23963.7700000000004	939.25	\N	\N	\N	\N	\N
41	2020-08-22 09:37:36	4	4	24628.0200000000004	390.199999999999989	\N	\N	\N	\N	\N
42	2020-08-23 10:29:56	9	9	24628.0200000000004	0	\N	\N	\N	\N	\N
43	2020-08-24 09:31:02	4	4	24706.2200000000012	509.100000000000023	\N	\N	\N	\N	\N
\.


--
-- Data for Name: rpt_creditos_colocados_historial; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.rpt_creditos_colocados_historial (id_rpt_creditos_colocados_historial, fecha_hora, id_analista, nombre_analista, nro_creditos, nro_clientes, monto_otrogado, interes_otrogado, redondeo_otrogado, monto_saldo, interes_saldo, redondeo_saldo, monto_cobrado, interes_cobrado, redondeo_cobrado, diario_1_7, diario_8_30, diario_mas_30, total_diario, semanal_1_7, semanal_8_30, semanal_mas_30, total_semanal, total_general, id_agencia) FROM stdin;
\.


--
-- Data for Name: rpt_movimientos_historial; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.rpt_movimientos_historial (id_rpt_movimientos_historial, fecha_hora, nro_creditos, nro_clientes, monto_otrogado, interes_otrogado, redondeo_otrogado, monto_saldo, interes_saldo, redondeo_saldo, monto_cobrado, interes_cobrado, redondeo_cobrado, monto_vencido, porcentaje, mora_generadas, mora_pagada, mora_saldo, noti_generadas, noti_pagada, noti_saldo, otros_generadas, otros_pagada, otros_saldo, id_agencia) FROM stdin;
\.


--
-- Data for Name: sectores; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.sectores (id_sector, sector, nombre, id_analista, obs) FROM stdin;
\.


--
-- Data for Name: solicitud_credito; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.solicitud_credito (id_solicitud, nombre_analista, id_analista, id_administrador, fecha_hora_solicitud, tipo_credito, producto_credito, dni, id_conyuge, tipo_garantia_cliente, descripcion_garantia_cliente, valorizacion_garantia_cliente, id_cliente_aval, tipo_garantia_aval, descripcion_garantia_aval, valorizacion_garantia_aval, id_negocio, interes, monto_interes, periodo, meses, penalidad, fecha_inicio_pago, monto_prestado, nro_cuotas, redondeo_total, fecha_finalizacion, obs, obs_internas, estado, nro_credito, con_boleta, nro_boleta, fecha_hora_desembolso, id_cajera, estado_credito, capital_x_cuota, interes_x_cuota, redondeo_x_cuota, cuota_a_pagar, capital_x_pagar, interes_x_pagar, redondeo_x_pagar, penalidad_x_pagar, cuotas_x_pagar, monto_x_pagar, cuotas_vencidas, monto_vencido, moras, moras_pagadas, notificaciones, notificaciones_pagadas, otros_pagos, otros_pagos_pagadas, ult_dia_pago, id_admin_condonacion, fecha_hora_condonacion, monto_condonacion, detalle_condonacion, descuento_precancelacion, id_ficha, dias, noti_acomulada, oficina, calificacion, area, fecha_cierre, contrato, agencia, penalidades_pagadas, hora_pagar, pago_en, obs_negacion, id_caja_apertura_cierre, serie) FROM stdin;
107	KEWIN GILBER VELA PEREZ	11	2	2020-07-11 06:20:41	PRINCIPAL	CAPITAL DE TRABAJO	00124443	0	NINGUNO	-	0	0	NINGUNO		0	84	8	40	1	1	0	2020-07-18	500	4	0	2020-08-08		\N	1	1372020107208   	f	\N	2020-07-13 11:41:35.946	9	2	125	10	0	135	0	0	0	0	0	0	0	0	0	0	0	0	0.5	0	2020-08-06 18:14:21.648	\N	\N	0	\N	0	89	\N	\N	PUCALLPA                                          	\N	\N	\N	CONSTE POR EL PRESENTE CONTRATO DE COMPRA  Y VENTA QUE OTORGA DE UNA PARTE, COMO VENDEDOR (CONTRAYENTE DE DEUDA) SEÑOR(A): AGUILAR YSLA JUAN MANUEL, IDENTIFICADO (A) CON DNI: 00124443,  DOMICILIADO EN ,  DISTRITO DE , PROVINCIA DE , DEPARTAMENTO DE , Y DE  OTRA PARTE EN CALIDAD DE COMPRADOR EL SEÑOR ALAIN RAVICHAGUA SOTELO, IDENTIFICADO CON DNI: 44892319 DOMICILIADO EN PROG. CUZCO N° 2309, DISTRITO DE HUANCAYO, PROVINCIA DE HUANCAYO, DEPARTAMENTO DE JUNÍN. BAJO LOS TÉRMINOS Y CONDICIONES SIGUIENTES:	\N	\N	03:00:00-05	0         	\N	3867	\N
127	JUANITA RUTH HERRERA SOTO	5	6	2020-07-22 11:08:45	PRINCIPAL	CAPITAL DE TRABAJO	19944260	0	NINGUNO		0	0	NINGUNO		0	62	2	34	0	2	0	2020-07-23	850	52	0	2020-09-22		\N	1	2272020127551   	f	\N	2020-07-22 11:28:40.972	4	0	16.3500000000000014	0.650000000000000022	0	17	817.5	32.5	0	0	50	850	0	0	0	0	0	0	0.5	0	2020-07-24 13:49:56.237	\N	\N	0	\N	0	64	\N	\N	HUANCAYO                                          	\N	\N	\N	CONSTE POR EL PRESENTE CONTRATO DE COMPRA  Y VENTA QUE OTORGA DE UNA PARTE, COMO VENDEDOR (CONTRAYENTE DE DEUDA) SEÑOR(A): MOLINA QUISPE VERONICA TRINIDAD, IDENTIFICADO (A) CON DNI: 19944260,  DOMICILIADO EN ,  DISTRITO DE , PROVINCIA DE , DEPARTAMENTO DE , Y DE  OTRA PARTE EN CALIDAD DE COMPRADOR EL SEÑOR ALAIN RAVICHAGUA SOTELO, IDENTIFICADO CON DNI: 44892319 DOMICILIADO EN PROG. CUZCO N° 2309, DISTRITO DE HUANCAYO, PROVINCIA DE HUANCAYO, DEPARTAMENTO DE JUNÍN. BAJO LOS TÉRMINOS Y CONDICIONES SIGUIENTES:	\N	\N	12:30:00-05	1         	\N	3879	\N
86	KEWIN GILBER VELA PEREZ	11	2	2020-07-08 11:44:25	PRINCIPAL	CAPITAL DE TRABAJO	00093054	0	NINGUNO	0	0	0	NINGUNO		0	66	12	84	0	1	0	2020-07-09	700	26	1.30000000000000004	2020-08-08	ALA ESPERA DE LA APROBACION	\N	1	87202086986     	f	\N	2020-07-08 23:50:24.338	9	2	26.9200000000000017	3.22999999999999998	0.0500000000000000028	30.1999999999999993	0	0	0	0	0	0	0	0	0	0	0	0	0.5	0	2020-08-07 17:27:30.71	\N	\N	0	\N	0	68	\N	\N	PUCALLPA                                          	\N	\N	\N	CONSTE POR EL PRESENTE CONTRATO DE COMPRA  Y VENTA QUE OTORGA DE UNA PARTE, COMO VENDEDOR (CONTRAYENTE DE DEUDA) SEÑOR(A): CORAL REATEGUI FELISINDA, IDENTIFICADO (A) CON DNI: 00093054,  DOMICILIADO EN ,  DISTRITO DE , PROVINCIA DE , DEPARTAMENTO DE , Y DE  OTRA PARTE EN CALIDAD DE COMPRADOR EL SEÑOR ALAIN RAVICHAGUA SOTELO, IDENTIFICADO CON DNI: 44892319 DOMICILIADO EN PROG. CUZCO N° 2309, DISTRITO DE HUANCAYO, PROVINCIA DE HUANCAYO, DEPARTAMENTO DE JUNÍN. BAJO LOS TÉRMINOS Y CONDICIONES SIGUIENTES:	\N	\N	04:00:00-05	0         	\N	3859	\N
120	JUANITA RUTH HERRERA SOTO	5	2	2020-07-16 09:31:51	PRINCIPAL	CAPITAL DE TRABAJO	70020689	0	NINGUNO		0	0	NINGUNO		0	48	1	0.510000000000000009	0	0	0	2020-07-17	110	12	0	2020-07-31		\N	0	\N	\N	\N	\N	\N	\N	9.16999999999999993	0.0400000000000000008	0	9.19999999999999929	110	0.510000000000000009	0	0	12	110.400000000000006	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	61	\N	\N	HUANCAYO                                          	\N	\N	\N	CONSTE POR EL PRESENTE CONTRATO DE COMPRA  Y VENTA QUE OTORGA DE UNA PARTE, COMO VENDEDOR (CONTRAYENTE DE DEUDA) SEÑOR(A): MAYTA ORDOÑEZ WENDY MERCEDES, IDENTIFICADO (A) CON DNI: 70020689,  DOMICILIADO EN ,  DISTRITO DE , PROVINCIA DE , DEPARTAMENTO DE , Y DE  OTRA PARTE EN CALIDAD DE COMPRADOR EL SEÑOR ALAIN RAVICHAGUA SOTELO, IDENTIFICADO CON DNI: 44892319 DOMICILIADO EN PROG. CUZCO N° 2309, DISTRITO DE HUANCAYO, PROVINCIA DE HUANCAYO, DEPARTAMENTO DE JUNÍN. BAJO LOS TÉRMINOS Y CONDICIONES SIGUIENTES:	\N	\N	12:30:00-05	1         	\N	\N	\N
1	FRANK MARTINEZ YUPANQUI	2	0	2020-06-29 11:55:41	PRINCIPAL	CAPITAL DE TRABAJO	19940774	0	GARANTIAS FLOTANTE	garantia de ahorro progrado	428.279999	0	NINGUNO		0	1	6	13.9320000000000004	4	0	0	2020-07-04	232.199999999999989	1	0	2020-07-04	cancelacion	\N	2	\N	\N	\N	\N	\N	\N	232	14	0	246	232.199999999999989	13.9320000000000004	0	0	1	246	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	0	\N	\N	HUANCAYO                                          	\N	\N	\N	CONSTE POR EL PRESENTE CONTRATO DE COMPRA  Y VENTA QUE OTORGA DE UNA PARTE, COMO VENDEDOR (CONTRAYENTE DE DEUDA) SEÑOR(A): SIERRA BARRIENTOS ZOZIMO, IDENTIFICADO (A) CON DNI: 19940774,  DOMICILIADO EN ,  DISTRITO DE , PROVINCIA DE , DEPARTAMENTO DE , Y DE  OTRA PARTE EN CALIDAD DE COMPRADOR EL SEÑOR ALAIN RAVICHAGUA SOTELO, IDENTIFICADO CON DNI: 44892319 DOMICILIADO EN PROG. CUZCO N° 2309, DISTRITO DE HUANCAYO, PROVINCIA DE HUANCAYO, DEPARTAMENTO DE JUNÍN. BAJO LOS TÉRMINOS Y CONDICIONES SIGUIENTES:	\N	\N	09:00:00-05	1         	falta de informacion	\N	\N
2	FRANK MARTINEZ YUPANQUI	2	6	2020-06-29 12:36:37	PRINCIPAL	CAPITAL DE TRABAJO	44974563	0	GARANTIAS FLOTANTE	CREDI TELEVISOR	1647.71997	0	NINGUNO		0	2	1	19.629999999999999	0	2	0	2020-06-30	981.299999999999955	52	2.60000000000000009	2020-08-29	CONSTANTE VERIFICACION	\N	0	\N	\N	\N	\N	\N	\N	18.870000000000001	0.380000000000000004	0.0500000000000000028	19.3000000000000007	981.299999999999955	19.629999999999999	2.60000000000000009	0	52	1003.60000000000002	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	1	\N	\N	HUANCAYO                                          	\N	\N	\N	CONSTE POR EL PRESENTE CONTRATO DE COMPRA  Y VENTA QUE OTORGA DE UNA PARTE, COMO VENDEDOR (CONTRAYENTE DE DEUDA) SEÑOR(A): SEGOVIA BARBOZA DENISSA ZOILA, IDENTIFICADO (A) CON DNI: 44974563,  DOMICILIADO EN ,  DISTRITO DE , PROVINCIA DE , DEPARTAMENTO DE , Y DE  OTRA PARTE EN CALIDAD DE COMPRADOR EL SEÑOR ALAIN RAVICHAGUA SOTELO, IDENTIFICADO CON DNI: 44892319 DOMICILIADO EN PROG. CUZCO N° 2309, DISTRITO DE HUANCAYO, PROVINCIA DE HUANCAYO, DEPARTAMENTO DE JUNÍN. BAJO LOS TÉRMINOS Y CONDICIONES SIGUIENTES:	\N	\N	12:30:00-05	0         	\N	\N	\N
6	JUANITA RUTH HERRERAV SOTO	7	0	2020-06-29 03:29:40	PRINCIPAL	CAPITAL DE TRABAJO	71872891	0	NINGUNO		0	0	NINGUNO		0	7	1	4.20000000000000018	2	1	0	2020-07-14	420	2	0	2020-07-30		\N	2	\N	\N	\N	\N	\N	\N	210	2.10000000000000009	0	212.099999999999994	420	4.20000000000000018	0	0	2	424.199999999999989	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	5	\N	\N	HUANCAYO                                          	\N	\N	\N	CONSTE POR EL PRESENTE CONTRATO DE COMPRA  Y VENTA QUE OTORGA DE UNA PARTE, COMO VENDEDOR (CONTRAYENTE DE DEUDA) SEÑOR(A): ACOSTA GASPAR MARINA, IDENTIFICADO (A) CON DNI: 71872891,  DOMICILIADO EN ,  DISTRITO DE , PROVINCIA DE , DEPARTAMENTO DE , Y DE  OTRA PARTE EN CALIDAD DE COMPRADOR EL SEÑOR ALAIN RAVICHAGUA SOTELO, IDENTIFICADO CON DNI: 44892319 DOMICILIADO EN PROG. CUZCO N° 2309, DISTRITO DE HUANCAYO, PROVINCIA DE HUANCAYO, DEPARTAMENTO DE JUNÍN. BAJO LOS TÉRMINOS Y CONDICIONES SIGUIENTES:	\N	\N	12:30:00-05	0         	error en el monto 	\N	\N
3	FRANK MARTINEZ YUPANQUI	2	2	2020-06-29 12:49:59		CAPITAL DE TRABAJO	44974563	0	GARANTIAS FLOTANTE	TELEVISOR	1500	0	NINGUNO		0	2	1	19.629999999999999	0	2	0.400000000000000022	2020-07-01	981.299999999999955	52	2.60000000000000009	2020-08-31	PASARA COBRAR A DOMICILIO	\N	2	\N	\N	\N	\N	\N	\N	18.870000000000001	0.380000000000000004	0.0500000000000000028	19.3000000000000007	981.299999999999955	19.629999999999999	2.60000000000000009	0	52	1003.60000000000002	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	1	\N	\N	HUANCAYO                                          	\N	\N	\N	CONSTE POR EL PRESENTE CONTRATO DE COMPRA  Y VENTA QUE OTORGA DE UNA PARTE, COMO VENDEDOR (CONTRAYENTE DE DEUDA) SEÑOR(A): SEGOVIA BARBOZA DENISSA ZOILA, IDENTIFICADO (A) CON DNI: 44974563,  DOMICILIADO EN ,  DISTRITO DE , PROVINCIA DE , DEPARTAMENTO DE , Y DE  OTRA PARTE EN CALIDAD DE COMPRADOR EL SEÑOR ALAIN RAVICHAGUA SOTELO, IDENTIFICADO CON DNI: 44892319 DOMICILIADO EN PROG. CUZCO N° 2309, DISTRITO DE HUANCAYO, PROVINCIA DE HUANCAYO, DEPARTAMENTO DE JUNÍN. BAJO LOS TÉRMINOS Y CONDICIONES SIGUIENTES:	\N	\N	12:30:00-05	0         	se duplico al migrar	\N	\N
4	FRANK MARTINEZ YUPANQUI	2	6	2020-06-29 01:25:20	PRINCIPAL	CAPITAL DE TRABAJO	23701995	0	NINGUNO	GARANTIA DE AHORRO	420	0	NINGUNO		0	4	1	1.07000000000000006	1	0	0	2020-07-06	428	1	0.0299999999999999989	2020-07-06		\N	2	\N	\N	\N	\N	\N	\N	428	1.07000000000000006	0.0299999999999999989	429.100000000000023	428	1.07000000000000006	0.0299999999999999989	0	1	429.100000000000023	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2	\N	\N	HUANCAYO                                          	\N	\N	\N	CONSTE POR EL PRESENTE CONTRATO DE COMPRA  Y VENTA QUE OTORGA DE UNA PARTE, COMO VENDEDOR (CONTRAYENTE DE DEUDA) SEÑOR(A): SOTO HUARCAHUAMAN JUANA, IDENTIFICADO (A) CON DNI: 23701995,  DOMICILIADO EN ,  DISTRITO DE , PROVINCIA DE , DEPARTAMENTO DE , Y DE  OTRA PARTE EN CALIDAD DE COMPRADOR EL SEÑOR ALAIN RAVICHAGUA SOTELO, IDENTIFICADO CON DNI: 44892319 DOMICILIADO EN PROG. CUZCO N° 2309, DISTRITO DE HUANCAYO, PROVINCIA DE HUANCAYO, DEPARTAMENTO DE JUNÍN. BAJO LOS TÉRMINOS Y CONDICIONES SIGUIENTES:	\N	\N	12:30:00-05	0         	equivocacion	\N	\N
8	JUANITA RUTH HERRERA SOTO	5	6	2020-06-30 01:46:35	PRINCIPAL	CAPITAL DE TRABAJO	23701995	0	GARANTIAS FLOTANTE	AHORRO PROGRAMADO 	400	0	NINGUNO		0	12	7.5	52.5	2	1	0	2020-07-15	700	2	0.100000000000000006	2020-07-30		\N	2	\N	\N	\N	\N	\N	\N	350	26.25	0.0500000000000000028	376.300000000000011	700	52.5	0.100000000000000006	0	2	752.600000000000023	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	10	\N	\N	HUANCAYO                                          	\N	\N	\N	CONSTE POR EL PRESENTE CONTRATO DE COMPRA  Y VENTA QUE OTORGA DE UNA PARTE, COMO VENDEDOR (CONTRAYENTE DE DEUDA) SEÑOR(A): SOTO HUARCAHUAMAN JUANA, IDENTIFICADO (A) CON DNI: 23701995,  DOMICILIADO EN ,  DISTRITO DE , PROVINCIA DE , DEPARTAMENTO DE , Y DE  OTRA PARTE EN CALIDAD DE COMPRADOR EL SEÑOR ALAIN RAVICHAGUA SOTELO, IDENTIFICADO CON DNI: 44892319 DOMICILIADO EN PROG. CUZCO N° 2309, DISTRITO DE HUANCAYO, PROVINCIA DE HUANCAYO, DEPARTAMENTO DE JUNÍN. BAJO LOS TÉRMINOS Y CONDICIONES SIGUIENTES:	\N	\N	12:30:00-05	0         	equivocacion	\N	\N
9	JUANITA RUTH HERRERA SOTO	5	0	2020-06-30 01:50:33	PRINCIPAL	CAPITAL DE TRABAJO	23701995	0	GARANTIAS FLOTANTE	GARANTIA DE AHORRO PROGRAMADO	420	0	NINGUNO		0	12	6	42	2	1	0	2020-07-15	700	2	0	2020-07-30		\N	2	\N	\N	\N	\N	\N	\N	350	21	0	371	700	42	0	0	2	742	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	10	\N	\N	HUANCAYO                                          	\N	\N	\N	CONSTE POR EL PRESENTE CONTRATO DE COMPRA  Y VENTA QUE OTORGA DE UNA PARTE, COMO VENDEDOR (CONTRAYENTE DE DEUDA) SEÑOR(A): SOTO HUARCAHUAMAN JUANA, IDENTIFICADO (A) CON DNI: 23701995,  DOMICILIADO EN ,  DISTRITO DE , PROVINCIA DE , DEPARTAMENTO DE , Y DE  OTRA PARTE EN CALIDAD DE COMPRADOR EL SEÑOR ALAIN RAVICHAGUA SOTELO, IDENTIFICADO CON DNI: 44892319 DOMICILIADO EN PROG. CUZCO N° 2309, DISTRITO DE HUANCAYO, PROVINCIA DE HUANCAYO, DEPARTAMENTO DE JUNÍN. BAJO LOS TÉRMINOS Y CONDICIONES SIGUIENTES:	\N	\N	12:30:00-05	0         	equivocacion	\N	\N
10	JUANITA RUTH HERRERA SOTO	5	0	2020-06-30 01:52:53	PRINCIPAL	CAPITAL DE TRABAJO	23701995	0	GARANTIAS FLOTANTE	GARANTIA DE AHORRO PROMAGO	450	0	NINGUNO		0	12	6	42	2	1	0	2020-07-15	700	2	0	2020-07-30	PAGOS PUNTUALES	\N	2	\N	\N	\N	\N	\N	\N	350	21	0	371	700	42	0	0	2	742	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	10	\N	\N	HUANCAYO                                          	\N	\N	\N	CONSTE POR EL PRESENTE CONTRATO DE COMPRA  Y VENTA QUE OTORGA DE UNA PARTE, COMO VENDEDOR (CONTRAYENTE DE DEUDA) SEÑOR(A): SOTO HUARCAHUAMAN JUANA, IDENTIFICADO (A) CON DNI: 23701995,  DOMICILIADO EN ,  DISTRITO DE , PROVINCIA DE , DEPARTAMENTO DE , Y DE  OTRA PARTE EN CALIDAD DE COMPRADOR EL SEÑOR ALAIN RAVICHAGUA SOTELO, IDENTIFICADO CON DNI: 44892319 DOMICILIADO EN PROG. CUZCO N° 2309, DISTRITO DE HUANCAYO, PROVINCIA DE HUANCAYO, DEPARTAMENTO DE JUNÍN. BAJO LOS TÉRMINOS Y CONDICIONES SIGUIENTES:	\N	\N	12:30:00-05	1         	equivocacion	\N	\N
11	JUANITA RUTH HERRERA SOTO	5	6	2020-06-30 02:03:46	PRINCIPAL	CAPITAL DE TRABAJO	23701995	0	GARANTIAS FLOTANTE	GARANTIA DE AHORRO PROGRAMADOS	400	0	NINGUNO		0	12	6	42	2	1	0	2020-07-15	700	2	0	2020-07-30	CREDITO PUNTUAL 	\N	1	306202011180    	f	\N	2020-06-30 11:28:21.633	4	2	350	21	0	371	0	0	0	0	0	0	0	0	0	0	0	0	0.5	0	2020-07-13 11:23:19.365	2	2020-07-13 11:26:24	0.5	PRECANCELACION	0	10	\N	\N	HUANCAYO                                          	\N	\N	\N	CONSTE POR EL PRESENTE CONTRATO DE COMPRA  Y VENTA QUE OTORGA DE UNA PARTE, COMO VENDEDOR (CONTRAYENTE DE DEUDA) SEÑOR(A): SOTO HUARCAHUAMAN JUANA, IDENTIFICADO (A) CON DNI: 23701995,  DOMICILIADO EN ,  DISTRITO DE , PROVINCIA DE , DEPARTAMENTO DE , Y DE  OTRA PARTE EN CALIDAD DE COMPRADOR EL SEÑOR ALAIN RAVICHAGUA SOTELO, IDENTIFICADO CON DNI: 44892319 DOMICILIADO EN PROG. CUZCO N° 2309, DISTRITO DE HUANCAYO, PROVINCIA DE HUANCAYO, DEPARTAMENTO DE JUNÍN. BAJO LOS TÉRMINOS Y CONDICIONES SIGUIENTES:	\N	\N	12:30:00-05	1         	\N	3848	\N
5	JUANITA RUTH HERRERAV SOTO	7	6	2020-06-29 03:08:52	PARALELO	CAPITAL DE TRABAJO	76621787	0	GARANTIAS FLOTANTE	AHORRO PROGRAMADON 	90	0	NINGUNO		0	6	1	16.9559999999999995	1	2	0	2020-07-06	847.799999999999955	8	0	2020-08-24		\N	1	14720205283     	f	\N	2020-07-14 12:17:41.666	4	0	105.974999999999994	2.11949999999999994	0	108.099999999999994	847.799999999999955	16.9559999999999995	0	0	8	864.799999999999955	2	216.199999999999989	6	0	0	0	0.5	0	2020-07-14 12:17:41.666	\N	\N	0	\N	0	4	\N	\N	HUANCAYO                                          	\N	\N	\N	CONSTE POR EL PRESENTE CONTRATO DE COMPRA  Y VENTA QUE OTORGA DE UNA PARTE, COMO VENDEDOR (CONTRAYENTE DE DEUDA) SEÑOR(A): MESIAS ALEGRIA FLOR DE JESUS, IDENTIFICADO (A) CON DNI: 76621787,  DOMICILIADO EN ,  DISTRITO DE , PROVINCIA DE , DEPARTAMENTO DE , Y DE  OTRA PARTE EN CALIDAD DE COMPRADOR EL SEÑOR ALAIN RAVICHAGUA SOTELO, IDENTIFICADO CON DNI: 44892319 DOMICILIADO EN PROG. CUZCO N° 2309, DISTRITO DE HUANCAYO, PROVINCIA DE HUANCAYO, DEPARTAMENTO DE JUNÍN. BAJO LOS TÉRMINOS Y CONDICIONES SIGUIENTES:	\N	\N	12:30:00-05	0         	\N	3868	\N
7	JUANITA RUTH HERRERAV SOTO	7	2	2020-06-29 03:45:19	PRINCIPAL	CAPITAL DE TRABAJO	19999596	0	NINGUNO		0	0	NINGUNO		0	8	1	9.80599999999999916	2	1	0	2020-07-14	980.600000000000023	2	0	2020-07-30		\N	1	14720207718     	f	\N	2020-07-14 12:25:10.26	4	0	490.300000000000011	4.90000000000000036	0	495.199999999999989	980.600000000000023	9.80599999999999916	0	0	2	990.399999999999977	1	495.199999999999989	1	0	0	0	0.5	0	2020-07-14 12:25:10.26	\N	\N	0	\N	0	6	\N	\N	HUANCAYO                                          	\N	\N	\N	CONSTE POR EL PRESENTE CONTRATO DE COMPRA  Y VENTA QUE OTORGA DE UNA PARTE, COMO VENDEDOR (CONTRAYENTE DE DEUDA) SEÑOR(A): NARBAIZA VILELA ELIBERTA, IDENTIFICADO (A) CON DNI: 19999596,  DOMICILIADO EN ,  DISTRITO DE , PROVINCIA DE , DEPARTAMENTO DE , Y DE  OTRA PARTE EN CALIDAD DE COMPRADOR EL SEÑOR ALAIN RAVICHAGUA SOTELO, IDENTIFICADO CON DNI: 44892319 DOMICILIADO EN PROG. CUZCO N° 2309, DISTRITO DE HUANCAYO, PROVINCIA DE HUANCAYO, DEPARTAMENTO DE JUNÍN. BAJO LOS TÉRMINOS Y CONDICIONES SIGUIENTES:	\N	\N	12:30:00-05	0         	\N	3868	\N
12	JUANITA RUTH HERRERA SOTO	5	2	2020-06-30 02:22:53	PRINCIPAL	CAPITAL DE TRABAJO	47475777	0	NINGUNO		0	0	NINGUNO		0	11	6	71.2920000000000016	2	2	0	2020-07-15	594.100000000000023	4	0.239999999999999991	2020-08-29		\N	1	306202012209    	f	\N	2020-06-30 15:20:47.277	4	0	148.52000000000001	17.8200000000000003	0.0599999999999999978	166.400000000000006	594.100000000000023	71.2920000000000016	0.239999999999999991	0	4	665.600000000000023	1	166.400000000000006	0.699999999999999956	0	0	0	0.5	0	2020-06-30 15:20:47.277	\N	\N	0	\N	0	9	\N	\N	HUANCAYO                                          	\N	\N	\N	CONSTE POR EL PRESENTE CONTRATO DE COMPRA  Y VENTA QUE OTORGA DE UNA PARTE, COMO VENDEDOR (CONTRAYENTE DE DEUDA) SEÑOR(A): FLORES MONTES JULIO TOMAS, IDENTIFICADO (A) CON DNI: 47475777,  DOMICILIADO EN ,  DISTRITO DE , PROVINCIA DE , DEPARTAMENTO DE , Y DE  OTRA PARTE EN CALIDAD DE COMPRADOR EL SEÑOR ALAIN RAVICHAGUA SOTELO, IDENTIFICADO CON DNI: 44892319 DOMICILIADO EN PROG. CUZCO N° 2309, DISTRITO DE HUANCAYO, PROVINCIA DE HUANCAYO, DEPARTAMENTO DE JUNÍN. BAJO LOS TÉRMINOS Y CONDICIONES SIGUIENTES:	\N	\N	12:30:00-05	1         	\N	3848	\N
109	JUANITA RUTH HERRERA SOTO	5	0	2020-07-13 10:49:43	PRINCIPAL	CAPITAL DE TRABAJO	40144277	0	GARANTIAS FLOTANTE	AHORRO PROGRAMADO 	65	0	NINGUNO		0	46	8	96	2	2	0	2020-07-28	600	4	0	2020-09-11		\N	0	\N	\N	\N	\N	\N	\N	150	24	0	174	600	96	0	0	4	696	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	45	\N	\N	HUANCAYO                                          	\N	\N	\N	CONSTE POR EL PRESENTE CONTRATO DE COMPRA  Y VENTA QUE OTORGA DE UNA PARTE, COMO VENDEDOR (CONTRAYENTE DE DEUDA) SEÑOR(A): CONGORA BARRETO LUCY , IDENTIFICADO (A) CON DNI: 40144277,  DOMICILIADO EN ,  DISTRITO DE , PROVINCIA DE , DEPARTAMENTO DE , Y DE  OTRA PARTE EN CALIDAD DE COMPRADOR EL SEÑOR ALAIN RAVICHAGUA SOTELO, IDENTIFICADO CON DNI: 44892319 DOMICILIADO EN PROG. CUZCO N° 2309, DISTRITO DE HUANCAYO, PROVINCIA DE HUANCAYO, DEPARTAMENTO DE JUNÍN. BAJO LOS TÉRMINOS Y CONDICIONES SIGUIENTES:	\N	\N	12:30:00-05	1         	\N	\N	\N
17	JUANITA RUTH HERRERA SOTO	5	2	2020-06-30 04:29:41	PRINCIPAL	CAPITAL DE TRABAJO	19940220	0	NINGUNO		0	0	NINGUNO		0	16	1	0.450000000000000011	4	0	0	2020-07-01	45	1	0	2020-07-01		\N	1	306202017033    	f	\N	2020-06-30 15:30:54.445	4	0	45	0	0	45	45	0.450000000000000011	0	0	1	45	1	45	0.200000000000000011	0	0	0	15	0	2020-06-30 15:30:54.445	\N	\N	0	\N	0	14	\N	\N	HUANCAYO                                          	\N	\N	\N	CONSTE POR EL PRESENTE CONTRATO DE COMPRA  Y VENTA QUE OTORGA DE UNA PARTE, COMO VENDEDOR (CONTRAYENTE DE DEUDA) SEÑOR(A): CARHUAMACA PEREZ TORIBIA TRINIDAD, IDENTIFICADO (A) CON DNI: 19940220,  DOMICILIADO EN ,  DISTRITO DE , PROVINCIA DE , DEPARTAMENTO DE , Y DE  OTRA PARTE EN CALIDAD DE COMPRADOR EL SEÑOR ALAIN RAVICHAGUA SOTELO, IDENTIFICADO CON DNI: 44892319 DOMICILIADO EN PROG. CUZCO N° 2309, DISTRITO DE HUANCAYO, PROVINCIA DE HUANCAYO, DEPARTAMENTO DE JUNÍN. BAJO LOS TÉRMINOS Y CONDICIONES SIGUIENTES:	\N	\N	12:30:00-05	1         	\N	3848	\N
18	JUANITA RUTH HERRERA SOTO	5	2	2020-06-30 05:09:16	PRINCIPAL	CAPITAL DE TRABAJO	16134843	0	NINGUNO		0	0	NINGUNO		0	17	1	14.1600000000000001	1	2	0	2020-07-07	566.399999999999977	10	0	2020-09-08	LA SEÑORA SE COMPROMETE A PAGAR SEMANALMENTE PUNTUAL CON SU CRONOGRAMA 	\N	1	306202018107    	f	\N	2020-06-30 15:25:45.237	4	0	56.6400000000000006	1.41599999999999993	0	58.1000000000000014	566.399999999999977	14.1600000000000001	0	0	10	581	2	116.200000000000003	6	0	0	0	0.5	0	2020-06-30 15:25:45.237	\N	\N	0	\N	0	15	\N	\N	HUANCAYO                                          	\N	\N	\N	CONSTE POR EL PRESENTE CONTRATO DE COMPRA  Y VENTA QUE OTORGA DE UNA PARTE, COMO VENDEDOR (CONTRAYENTE DE DEUDA) SEÑOR(A): SOTO AYLAS ISABEL, IDENTIFICADO (A) CON DNI: 16134843,  DOMICILIADO EN ,  DISTRITO DE , PROVINCIA DE , DEPARTAMENTO DE , Y DE  OTRA PARTE EN CALIDAD DE COMPRADOR EL SEÑOR ALAIN RAVICHAGUA SOTELO, IDENTIFICADO CON DNI: 44892319 DOMICILIADO EN PROG. CUZCO N° 2309, DISTRITO DE HUANCAYO, PROVINCIA DE HUANCAYO, DEPARTAMENTO DE JUNÍN. BAJO LOS TÉRMINOS Y CONDICIONES SIGUIENTES:	\N	\N	12:30:00-05	1         	\N	3848	\N
16	JUANITA RUTH HERRERA SOTO	5	2	2020-06-30 04:13:51	PRINCIPAL	CAPITAL DE TRABAJO	42297478	0	GARANTIAS FLOTANTE	AHORRO PROGRAMADO 	355	0	NINGUNO		0	15	1	3.45000000000000018	4	0	0	2020-07-01	345	1	0	2020-07-01		\N	1	306202016454    	f	\N	2020-06-30 15:12:27.149	4	0	345	3	0	348	345	3.45000000000000018	0	0	1	348	1	348	1	0	0	0	15	0	2020-06-30 15:12:27.149	\N	\N	0	\N	0	13	\N	\N	HUANCAYO                                          	\N	\N	\N	CONSTE POR EL PRESENTE CONTRATO DE COMPRA  Y VENTA QUE OTORGA DE UNA PARTE, COMO VENDEDOR (CONTRAYENTE DE DEUDA) SEÑOR(A): ROCA COCA MIGUEL ANGEL, IDENTIFICADO (A) CON DNI: 42297478,  DOMICILIADO EN ,  DISTRITO DE , PROVINCIA DE , DEPARTAMENTO DE , Y DE  OTRA PARTE EN CALIDAD DE COMPRADOR EL SEÑOR ALAIN RAVICHAGUA SOTELO, IDENTIFICADO CON DNI: 44892319 DOMICILIADO EN PROG. CUZCO N° 2309, DISTRITO DE HUANCAYO, PROVINCIA DE HUANCAYO, DEPARTAMENTO DE JUNÍN. BAJO LOS TÉRMINOS Y CONDICIONES SIGUIENTES:	\N	\N	12:30:00-05	1         	\N	3848	\N
20	JUANITA RUTH HERRERA SOTO	5	0	2020-06-30 05:34:43	PRINCIPAL	CAPITAL DE TRABAJO	41123341	0	GARANTIAS FLOTANTE	AHORRO PROGRAMADO 	604.400024	0	NINGUNO		0	19	6	4.8360000000000003	4	0	0	2020-07-01	80.5999999999999943	1	0	2020-07-01	Pagar con sus ahorro 	\N	0	\N	\N	\N	\N	\N	\N	81	5	0	86	80.5999999999999943	4.8360000000000003	0	0	1	86	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	17	\N	\N	HUANCAYO                                          	\N	\N	\N	CONSTE POR EL PRESENTE CONTRATO DE COMPRA  Y VENTA QUE OTORGA DE UNA PARTE, COMO VENDEDOR (CONTRAYENTE DE DEUDA) SEÑOR(A): JEREMIAS CALLUPE NATALY MAGALY, IDENTIFICADO (A) CON DNI: 41123341,  DOMICILIADO EN ,  DISTRITO DE , PROVINCIA DE , DEPARTAMENTO DE , Y DE  OTRA PARTE EN CALIDAD DE COMPRADOR EL SEÑOR ALAIN RAVICHAGUA SOTELO, IDENTIFICADO CON DNI: 44892319 DOMICILIADO EN PROG. CUZCO N° 2309, DISTRITO DE HUANCAYO, PROVINCIA DE HUANCAYO, DEPARTAMENTO DE JUNÍN. BAJO LOS TÉRMINOS Y CONDICIONES SIGUIENTES:	\N	\N	12:30:00-05	1         	\N	\N	\N
25	MELISA DINA CHUQUILLANQUI CUNO 	3	2	2020-06-30 03:23:14	PRINCIPAL	CAPITAL DE TRABAJO	45745478	0	GARANTIAS FLOTANTE	AHORRO EN COOPERATIVA	470.399994	0	NINGUNO		0	21	0	0	0	3	0	2020-07-01	736.700000000000045	74	2.95999999999999996	2020-09-25	CLIENTE ESTA AMORTIZANDO	\N	1	27202025174     	f	\N	2020-07-02 14:01:44.431	4	0	9.96000000000000085	0	0.0400000000000000008	10	736.700000000000045	0	2.95999999999999996	0	74	740	14	140	1	0	0	0	0.5	0	2020-07-02 14:01:44.431	\N	\N	0	\N	0	19	\N	\N	HUANCAYO                                          	\N	\N	\N	CONSTE POR EL PRESENTE CONTRATO DE COMPRA  Y VENTA QUE OTORGA DE UNA PARTE, COMO VENDEDOR (CONTRAYENTE DE DEUDA) SEÑOR(A): CASTILLON ROMAN ROSA, IDENTIFICADO (A) CON DNI: 45745478,  DOMICILIADO EN ,  DISTRITO DE , PROVINCIA DE , DEPARTAMENTO DE , Y DE  OTRA PARTE EN CALIDAD DE COMPRADOR EL SEÑOR ALAIN RAVICHAGUA SOTELO, IDENTIFICADO CON DNI: 44892319 DOMICILIADO EN PROG. CUZCO N° 2309, DISTRITO DE HUANCAYO, PROVINCIA DE HUANCAYO, DEPARTAMENTO DE JUNÍN. BAJO LOS TÉRMINOS Y CONDICIONES SIGUIENTES:	\N	\N	12:30:00-05	1         	\N	3852	\N
21	JUANITA RUTH HERRERA SOTO	5	2	2020-06-30 05:54:42	PRINCIPAL	CAPITAL DE TRABAJO	44279232	0	GARANTIAS FLOTANTE	AHORRO PROGRAMADO	50	0	NINGUNO		0	20	6	4.70000000000000018	0	0	0	2020-07-01	226.300000000000011	9	0.270000000000000018	2020-07-10		\N	0	\N	\N	\N	\N	\N	\N	25.1400000000000006	0.520000000000000018	0.0299999999999999989	25.6999999999999993	226.300000000000011	4.70000000000000018	0.270000000000000018	0	9	231.300000000000011	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	18	\N	\N	HUANCAYO                                          	\N	\N	\N	CONSTE POR EL PRESENTE CONTRATO DE COMPRA  Y VENTA QUE OTORGA DE UNA PARTE, COMO VENDEDOR (CONTRAYENTE DE DEUDA) SEÑOR(A): HUANAY YUPANQUI JONATHAN, IDENTIFICADO (A) CON DNI: 44279232,  DOMICILIADO EN ,  DISTRITO DE , PROVINCIA DE , DEPARTAMENTO DE , Y DE  OTRA PARTE EN CALIDAD DE COMPRADOR EL SEÑOR ALAIN RAVICHAGUA SOTELO, IDENTIFICADO CON DNI: 44892319 DOMICILIADO EN PROG. CUZCO N° 2309, DISTRITO DE HUANCAYO, PROVINCIA DE HUANCAYO, DEPARTAMENTO DE JUNÍN. BAJO LOS TÉRMINOS Y CONDICIONES SIGUIENTES:	\N	\N	12:30:00-05	1         	\N	\N	\N
23	JUANITA RUTH HERRERA SOTO	5	0	2020-06-30 06:20:29	PRINCIPAL	CAPITAL DE TRABAJO	73054005	0	NINGUNO		0	0	NINGUNO		0	22	6	84.2399999999999949	1	1	0	2020-07-07	1404	4	0.0400000000000000008	2020-07-28		\N	0	\N	\N	\N	\N	\N	\N	351	21.0599999999999987	0.0100000000000000002	372.100000000000023	1404	84.2399999999999949	0.0400000000000000008	0	4	1488.40000000000009	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	20	\N	\N	HUANCAYO                                          	\N	\N	\N	CONSTE POR EL PRESENTE CONTRATO DE COMPRA  Y VENTA QUE OTORGA DE UNA PARTE, COMO VENDEDOR (CONTRAYENTE DE DEUDA) SEÑOR(A): MANRIQUE VARILLAS NORMAN SILVESTRE, IDENTIFICADO (A) CON DNI: 73054005,  DOMICILIADO EN ,  DISTRITO DE , PROVINCIA DE , DEPARTAMENTO DE , Y DE  OTRA PARTE EN CALIDAD DE COMPRADOR EL SEÑOR ALAIN RAVICHAGUA SOTELO, IDENTIFICADO CON DNI: 44892319 DOMICILIADO EN PROG. CUZCO N° 2309, DISTRITO DE HUANCAYO, PROVINCIA DE HUANCAYO, DEPARTAMENTO DE JUNÍN. BAJO LOS TÉRMINOS Y CONDICIONES SIGUIENTES:	\N	\N	12:30:00-05	1         	\N	\N	\N
24	JUANITA RUTH HERRERA SOTO	5	0	2020-06-30 06:22:21	PRINCIPAL	CAPITAL DE TRABAJO	73054005	0	NINGUNO		0	0	NINGUNO		0	22	1	14.0399999999999991	1	1	0	2020-07-07	1404	4	0.0400000000000000008	2020-07-28		\N	0	\N	\N	\N	\N	\N	\N	351	3.50999999999999979	0.0100000000000000002	354.550000000000011	1404	14.0399999999999991	0.0400000000000000008	0	4	1418.20000000000005	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	20	\N	\N	HUANCAYO                                          	\N	\N	\N	CONSTE POR EL PRESENTE CONTRATO DE COMPRA  Y VENTA QUE OTORGA DE UNA PARTE, COMO VENDEDOR (CONTRAYENTE DE DEUDA) SEÑOR(A): MANRIQUE VARILLAS NORMAN SILVESTRE, IDENTIFICADO (A) CON DNI: 73054005,  DOMICILIADO EN ,  DISTRITO DE , PROVINCIA DE , DEPARTAMENTO DE , Y DE  OTRA PARTE EN CALIDAD DE COMPRADOR EL SEÑOR ALAIN RAVICHAGUA SOTELO, IDENTIFICADO CON DNI: 44892319 DOMICILIADO EN PROG. CUZCO N° 2309, DISTRITO DE HUANCAYO, PROVINCIA DE HUANCAYO, DEPARTAMENTO DE JUNÍN. BAJO LOS TÉRMINOS Y CONDICIONES SIGUIENTES:	\N	\N	12:30:00-05	1         	\N	\N	\N
123	JUANITA RUTH HERRERA SOTO	5	2	2020-07-18 10:22:12	PRINCIPAL	CAPITAL DE TRABAJO	20682065	0	GARANTIAS FLOTANTE	AHORRA PROGRAMADO 	400	0	NINGUNO		0	85	8	24	0	1	0	2020-07-20	300	26	1.04000000000000004	2020-08-19		\N	0	\N	\N	\N	\N	\N	\N	11.5399999999999991	0.92000000000000004	0.0400000000000000008	12.5	300	24	1.04000000000000004	0	26	325	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	90	\N	\N	HUANCAYO                                          	\N	\N	\N	CONSTE POR EL PRESENTE CONTRATO DE COMPRA  Y VENTA QUE OTORGA DE UNA PARTE, COMO VENDEDOR (CONTRAYENTE DE DEUDA) SEÑOR(A): URBANO PAUCARCAJA MARIA TEODORA , IDENTIFICADO (A) CON DNI: 20682065,  DOMICILIADO EN ,  DISTRITO DE , PROVINCIA DE , DEPARTAMENTO DE , Y DE  OTRA PARTE EN CALIDAD DE COMPRADOR EL SEÑOR ALAIN RAVICHAGUA SOTELO, IDENTIFICADO CON DNI: 44892319 DOMICILIADO EN PROG. CUZCO N° 2309, DISTRITO DE HUANCAYO, PROVINCIA DE HUANCAYO, DEPARTAMENTO DE JUNÍN. BAJO LOS TÉRMINOS Y CONDICIONES SIGUIENTES:	\N	\N	12:30:00-05	1         	\N	\N	\N
110	JUANITA RUTH HERRERA SOTO	5	0	2020-07-13 11:19:14	PRINCIPAL	CAPITAL DE TRABAJO	40144277	0	GARANTIAS FLOTANTE	AHORRO PROGRAMADO 	65	0	NINGUNO		0	46	6	72	2	2	0	2020-07-28	600	4	0	2020-09-11		\N	0	\N	\N	\N	\N	\N	\N	150	18	0	168	600	72	0	0	4	672	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	45	\N	\N	HUANCAYO                                          	\N	\N	\N	CONSTE POR EL PRESENTE CONTRATO DE COMPRA  Y VENTA QUE OTORGA DE UNA PARTE, COMO VENDEDOR (CONTRAYENTE DE DEUDA) SEÑOR(A): CONGORA BARRETO LUCY , IDENTIFICADO (A) CON DNI: 40144277,  DOMICILIADO EN ,  DISTRITO DE , PROVINCIA DE , DEPARTAMENTO DE , Y DE  OTRA PARTE EN CALIDAD DE COMPRADOR EL SEÑOR ALAIN RAVICHAGUA SOTELO, IDENTIFICADO CON DNI: 44892319 DOMICILIADO EN PROG. CUZCO N° 2309, DISTRITO DE HUANCAYO, PROVINCIA DE HUANCAYO, DEPARTAMENTO DE JUNÍN. BAJO LOS TÉRMINOS Y CONDICIONES SIGUIENTES:	\N	\N	12:30:00-05	1         	\N	\N	\N
96	JUANITA RUTH HERRERA SOTO	5	2	2020-07-09 01:24:19	PRINCIPAL	CAPITAL DE TRABAJO	48522972	0	NINGUNO		0	0	NINGUNO		0	76	8	22.8640000000000008	1	1	0	2020-07-16	285.800000000000011	4	0.0400000000000000008	2020-08-06		\N	1	107202096374    	f	\N	2020-07-10 12:15:37.506	4	0	71.4500000000000028	5.71600000000000019	0.0100000000000000002	77.2000000000000028	285.800000000000011	22.8640000000000008	0.0400000000000000008	0	4	308.800000000000011	1	77.2000000000000028	6	0	0	0	0.5	0	2020-07-10 12:15:37.506	\N	\N	0	\N	0	79	\N	\N	HUANCAYO                                          	\N	\N	\N	CONSTE POR EL PRESENTE CONTRATO DE COMPRA  Y VENTA QUE OTORGA DE UNA PARTE, COMO VENDEDOR (CONTRAYENTE DE DEUDA) SEÑOR(A): PARRA MACHARI CLARISA, IDENTIFICADO (A) CON DNI: 48522972,  DOMICILIADO EN ,  DISTRITO DE , PROVINCIA DE , DEPARTAMENTO DE , Y DE  OTRA PARTE EN CALIDAD DE COMPRADOR EL SEÑOR ALAIN RAVICHAGUA SOTELO, IDENTIFICADO CON DNI: 44892319 DOMICILIADO EN PROG. CUZCO N° 2309, DISTRITO DE HUANCAYO, PROVINCIA DE HUANCAYO, DEPARTAMENTO DE JUNÍN. BAJO LOS TÉRMINOS Y CONDICIONES SIGUIENTES:	\N	\N	12:30:00-05	1         	\N	3862	\N
19	MELISA DINA CHUQUILLANQUI CUNO 	3	2	2020-06-30 02:28:49	PRINCIPAL	CAPITAL DE TRABAJO	60064606	0	GARANTIAS FLOTANTE	AHORRO EN LA COOPERATIVA	244	0	NINGUNO		0	18	1	4.15137313432830979	3	2	0	2020-07-30	276.300000000000011	2	0	2020-08-29	REPROGRAMACION DE CREDITO	\N	1	306202019682    	f	\N	2020-06-30 15:55:22.38	4	0	276.300000000000011	2.08000000000000007	0	140.22568656716399	276.300000000000011	4.15137313432830979	0	0	2	280.45137313432798	0	0	0	0	0	0	0.5	0	2020-06-30 15:55:22.38	\N	\N	0	\N	0	16	\N	\N	HUANCAYO                                          	\N	\N	\N	CONSTE POR EL PRESENTE CONTRATO DE COMPRA  Y VENTA QUE OTORGA DE UNA PARTE, COMO VENDEDOR (CONTRAYENTE DE DEUDA) SEÑOR(A): MENDEZ BAUTISTA BEATRIZ JERMINA, IDENTIFICADO (A) CON DNI: 60064606,  DOMICILIADO EN ,  DISTRITO DE , PROVINCIA DE , DEPARTAMENTO DE , Y DE  OTRA PARTE EN CALIDAD DE COMPRADOR EL SEÑOR ALAIN RAVICHAGUA SOTELO, IDENTIFICADO CON DNI: 44892319 DOMICILIADO EN PROG. CUZCO N° 2309, DISTRITO DE HUANCAYO, PROVINCIA DE HUANCAYO, DEPARTAMENTO DE JUNÍN. BAJO LOS TÉRMINOS Y CONDICIONES SIGUIENTES:	\N	\N	12:30:00-05	1         	\N	3848	\N
29	MELISA DINA CHUQUILLANQUI CUNO 	3	2	2020-06-30 04:01:39	PRINCIPAL	CAPITAL DE TRABAJO	20099395	0	GARANTIAS FLOTANTE	AHORRO EN CUENTA	100	0	NINGUNO		0	25	0	0	2	1	0	2020-07-15	250	2	0	2020-07-30	CLIENTE FIRMO COMPROMISO DE PAGO, SOLO ADEUDA 150 	\N	1	27202029068     	f	\N	2020-07-02 14:07:55.887	4	0	125	0	0	125	250	0	0	0	2	250	1	125	0.699999999999999956	0	0	0	0.5	0	2020-07-02 14:07:55.887	\N	\N	0	\N	0	23	\N	\N	HUANCAYO                                          	\N	\N	\N	CONSTE POR EL PRESENTE CONTRATO DE COMPRA  Y VENTA QUE OTORGA DE UNA PARTE, COMO VENDEDOR (CONTRAYENTE DE DEUDA) SEÑOR(A): TOMAS GONZALES TITO CIRILO, IDENTIFICADO (A) CON DNI: 20099395,  DOMICILIADO EN ,  DISTRITO DE , PROVINCIA DE , DEPARTAMENTO DE , Y DE  OTRA PARTE EN CALIDAD DE COMPRADOR EL SEÑOR ALAIN RAVICHAGUA SOTELO, IDENTIFICADO CON DNI: 44892319 DOMICILIADO EN PROG. CUZCO N° 2309, DISTRITO DE HUANCAYO, PROVINCIA DE HUANCAYO, DEPARTAMENTO DE JUNÍN. BAJO LOS TÉRMINOS Y CONDICIONES SIGUIENTES:	\N	\N	12:30:00-05	1         	\N	3852	\N
26	JUANITA RUTH HERRERA SOTO	5	2	2020-06-30 06:26:54	PRINCIPAL	CAPITAL DE TRABAJO	73054005	0	NINGUNO		0	0	NINGUNO		0	22	1	14.0399999999999991	1	1	0	2020-07-07	1404	4	0.0400000000000000008	2020-07-28		\N	1	306202026589    	f	\N	2020-06-30 15:50:47.717	4	0	351	3.50999999999999979	0.0100000000000000002	354.550000000000011	1404	14.0399999999999991	0.0400000000000000008	0	4	1418.20000000000005	2	709.100000000000023	6	0	0	0	0.5	0	2020-06-30 15:50:47.717	\N	\N	0	\N	0	20	\N	\N	HUANCAYO                                          	\N	\N	\N	CONSTE POR EL PRESENTE CONTRATO DE COMPRA  Y VENTA QUE OTORGA DE UNA PARTE, COMO VENDEDOR (CONTRAYENTE DE DEUDA) SEÑOR(A): MANRIQUE VARILLAS NORMAN SILVESTRE, IDENTIFICADO (A) CON DNI: 73054005,  DOMICILIADO EN ,  DISTRITO DE , PROVINCIA DE , DEPARTAMENTO DE , Y DE  OTRA PARTE EN CALIDAD DE COMPRADOR EL SEÑOR ALAIN RAVICHAGUA SOTELO, IDENTIFICADO CON DNI: 44892319 DOMICILIADO EN PROG. CUZCO N° 2309, DISTRITO DE HUANCAYO, PROVINCIA DE HUANCAYO, DEPARTAMENTO DE JUNÍN. BAJO LOS TÉRMINOS Y CONDICIONES SIGUIENTES:	\N	\N	12:30:00-05	1         	\N	3848	\N
27	JUANITA RUTH HERRERA SOTO	5	2	2020-06-30 06:43:52		CAPITAL DE TRABAJO	20064201	0	GARANTIAS FLOTANTE	AHORRO PROGRAMADO 	5030	0	NINGUNO		0	24	1	1.94999999999999996	1	0	0	2020-07-07	780	1	0.0500000000000000028	2020-07-07		\N	1	306202027040    	f	\N	2020-06-30 15:53:47.636	4	1	780	1.94999999999999996	0.0500000000000000028	782	0	0	0	0	0	0	0	0	0	0	0	0	0.5	0.5	2020-07-03 14:42:55.368	\N	\N	0	\N	0.5	22	\N	\N	HUANCAYO                                          	\N	\N	\N	CONSTE POR EL PRESENTE CONTRATO DE COMPRA  Y VENTA QUE OTORGA DE UNA PARTE, COMO VENDEDOR (CONTRAYENTE DE DEUDA) SEÑOR(A): MOLINA QUISPE JOEL, IDENTIFICADO (A) CON DNI: 20064201,  DOMICILIADO EN ,  DISTRITO DE , PROVINCIA DE , DEPARTAMENTO DE , Y DE  OTRA PARTE EN CALIDAD DE COMPRADOR EL SEÑOR ALAIN RAVICHAGUA SOTELO, IDENTIFICADO CON DNI: 44892319 DOMICILIADO EN PROG. CUZCO N° 2309, DISTRITO DE HUANCAYO, PROVINCIA DE HUANCAYO, DEPARTAMENTO DE JUNÍN. BAJO LOS TÉRMINOS Y CONDICIONES SIGUIENTES:	\N	\N	12:30:00-05	1         	\N	3848	\N
28	JUANITA RUTH HERRERA SOTO	5	2	2020-06-30 06:49:34	PRINCIPAL	CAPITAL DE TRABAJO	41123341	0	GARANTIAS FLOTANTE	AHORRO PROGRAMADO	685	0	NINGUNO		0	19	1	0.80600000000000005	4	0	0	2020-07-01	80.5999999999999943	1	0	2020-07-01		\N	1	37202028965     	f	\N	2020-07-03 10:34:09.527	4	2	81	1	0	82	0	0	0	0	0	0	0	0	0	0	0	0	0.5	0	2020-07-03 10:36:56.584	\N	\N	0	\N	0	17	\N	\N	HUANCAYO                                          	\N	\N	\N	CONSTE POR EL PRESENTE CONTRATO DE COMPRA  Y VENTA QUE OTORGA DE UNA PARTE, COMO VENDEDOR (CONTRAYENTE DE DEUDA) SEÑOR(A): JEREMIAS CALLUPE NATALY MAGALY, IDENTIFICADO (A) CON DNI: 41123341,  DOMICILIADO EN ,  DISTRITO DE , PROVINCIA DE , DEPARTAMENTO DE , Y DE  OTRA PARTE EN CALIDAD DE COMPRADOR EL SEÑOR ALAIN RAVICHAGUA SOTELO, IDENTIFICADO CON DNI: 44892319 DOMICILIADO EN PROG. CUZCO N° 2309, DISTRITO DE HUANCAYO, PROVINCIA DE HUANCAYO, DEPARTAMENTO DE JUNÍN. BAJO LOS TÉRMINOS Y CONDICIONES SIGUIENTES:	\N	\N	12:30:00-05	1         	\N	3853	\N
30	JUANITA RUTH HERRERA SOTO	5	0	2020-06-30 07:06:50	PRINCIPAL	CAPITAL DE TRABAJO	80145065	0	NINGUNO		0	0	NINGUNO		0	26	1	23.4299999999999997	1	1	0	2020-07-07	2343	4	0	2020-07-28		\N	0	\N	\N	\N	\N	\N	\N	585.75	5.85749999999999993	0	591.600000000000023	2343	23.4299999999999997	0	0	4	2366.40000000000009	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	24	\N	\N	HUANCAYO                                          	\N	\N	\N	CONSTE POR EL PRESENTE CONTRATO DE COMPRA  Y VENTA QUE OTORGA DE UNA PARTE, COMO VENDEDOR (CONTRAYENTE DE DEUDA) SEÑOR(A): PALOMINO RIOS FLORA MERY, IDENTIFICADO (A) CON DNI: 80145065,  DOMICILIADO EN ,  DISTRITO DE , PROVINCIA DE , DEPARTAMENTO DE , Y DE  OTRA PARTE EN CALIDAD DE COMPRADOR EL SEÑOR ALAIN RAVICHAGUA SOTELO, IDENTIFICADO CON DNI: 44892319 DOMICILIADO EN PROG. CUZCO N° 2309, DISTRITO DE HUANCAYO, PROVINCIA DE HUANCAYO, DEPARTAMENTO DE JUNÍN. BAJO LOS TÉRMINOS Y CONDICIONES SIGUIENTES:	\N	\N	12:30:00-05	1         	\N	\N	\N
32	KEWIN GILBER VELA PEREZ	11	0	2020-07-01 09:44:42	PRINCIPAL	CAPITAL DE TRABAJO	00098119	0	NINGUNO	NINGUNO	0	0	NINGUNO		0	28	1	1.11699999999999999	2	0	0	2020-07-16	223.400000000000006	1	0.0800000000000000017	2020-07-16		\N	0	\N	\N	\N	\N	\N	\N	223.400000000000006	1.12000000000000011	0.0800000000000000017	224.599999999999994	223.400000000000006	1.11699999999999999	0.0800000000000000017	0	1	224.599999999999994	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	26	\N	\N	PUCALLPA                                          	\N	\N	\N	CONSTE POR EL PRESENTE CONTRATO DE COMPRA  Y VENTA QUE OTORGA DE UNA PARTE, COMO VENDEDOR (CONTRAYENTE DE DEUDA) SEÑOR(A): URQUIA LOPEZ ROSA LUCIA, IDENTIFICADO (A) CON DNI: 00098119,  DOMICILIADO EN ,  DISTRITO DE , PROVINCIA DE , DEPARTAMENTO DE , Y DE  OTRA PARTE EN CALIDAD DE COMPRADOR EL SEÑOR ALAIN RAVICHAGUA SOTELO, IDENTIFICADO CON DNI: 44892319 DOMICILIADO EN PROG. CUZCO N° 2309, DISTRITO DE HUANCAYO, PROVINCIA DE HUANCAYO, DEPARTAMENTO DE JUNÍN. BAJO LOS TÉRMINOS Y CONDICIONES SIGUIENTES:	\N	\N	01:30:00-05	1         	\N	\N	\N
36	JUANITA RUTH HERRERA SOTO	5	0	2020-07-01 02:12:27	PRINCIPAL	CAPITAL DE TRABAJO	41274111	0	GARANTIAS FLOTANTE	AHORRO DE PLAZO FIJO 	20000	0	NINGUNO		0	30	1	34.6169999999999973	1	3	0	2020-07-08	1153.90000000000009	12	0	2020-09-24		\N	0	\N	\N	\N	\N	\N	\N	96.158333333333303	2.88474999999999993	0	99.0499999999999972	1153.90000000000009	34.6169999999999973	0	0	12	1188.59999999999991	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	28	\N	\N	HUANCAYO                                          	\N	\N	\N	CONSTE POR EL PRESENTE CONTRATO DE COMPRA  Y VENTA QUE OTORGA DE UNA PARTE, COMO VENDEDOR (CONTRAYENTE DE DEUDA) SEÑOR(A): ROJAS VILCAHUAMAN ORLANDINI, IDENTIFICADO (A) CON DNI: 41274111,  DOMICILIADO EN ,  DISTRITO DE , PROVINCIA DE , DEPARTAMENTO DE , Y DE  OTRA PARTE EN CALIDAD DE COMPRADOR EL SEÑOR ALAIN RAVICHAGUA SOTELO, IDENTIFICADO CON DNI: 44892319 DOMICILIADO EN PROG. CUZCO N° 2309, DISTRITO DE HUANCAYO, PROVINCIA DE HUANCAYO, DEPARTAMENTO DE JUNÍN. BAJO LOS TÉRMINOS Y CONDICIONES SIGUIENTES:	\N	\N	12:30:00-05	1         	\N	\N	\N
31	MELISA DINA CHUQUILLANQUI CUNO 	3	0	2020-06-30 04:16:04	PRINCIPAL	CAPITAL DE TRABAJO	41773365	0	NINGUNO		0	0	NINGUNO		0	27	6	16.7160000000000011	2	1	0	2020-07-15	278.600000000000023	2	0.0800000000000000017	2020-07-30	CLIENTE ACTIVO	\N	2	\N	\N	\N	\N	\N	\N	139.300000000000011	8.35999999999999943	0.0400000000000000008	147.699999999999989	278.600000000000023	16.7160000000000011	0.0800000000000000017	0	2	295.399999999999977	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	25	\N	\N	HUANCAYO                                          	\N	\N	\N	CONSTE POR EL PRESENTE CONTRATO DE COMPRA  Y VENTA QUE OTORGA DE UNA PARTE, COMO VENDEDOR (CONTRAYENTE DE DEUDA) SEÑOR(A): VASQUEZ QUERRE LIZ CATHERINE, IDENTIFICADO (A) CON DNI: 41773365,  DOMICILIADO EN ,  DISTRITO DE , PROVINCIA DE , DEPARTAMENTO DE , Y DE  OTRA PARTE EN CALIDAD DE COMPRADOR EL SEÑOR ALAIN RAVICHAGUA SOTELO, IDENTIFICADO CON DNI: 44892319 DOMICILIADO EN PROG. CUZCO N° 2309, DISTRITO DE HUANCAYO, PROVINCIA DE HUANCAYO, DEPARTAMENTO DE JUNÍN. BAJO LOS TÉRMINOS Y CONDICIONES SIGUIENTES:	\N	\N	12:30:00-05	1         	COPIA	\N	\N
35	MELISA DINA CHUQUILLANQUI CUNO 	3	0	2020-07-01 10:46:14	PRINCIPAL	CAPITAL DE TRABAJO	41773365	0	NINGUNO		0	0	NINGUNO		0	27	6	16.7160000000000011	2	1	0	2020-07-16	278.600000000000023	2	0.0800000000000000017	2020-07-31	CLIENTE ACTIVO 	\N	2	\N	\N	\N	\N	\N	\N	139.300000000000011	8.35999999999999943	0.0400000000000000008	147.699999999999989	278.600000000000023	16.7160000000000011	0.0800000000000000017	0	2	295.399999999999977	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	25	\N	\N	HUANCAYO                                          	\N	\N	\N	CONSTE POR EL PRESENTE CONTRATO DE COMPRA  Y VENTA QUE OTORGA DE UNA PARTE, COMO VENDEDOR (CONTRAYENTE DE DEUDA) SEÑOR(A): VASQUEZ QUERRE LIZ CATHERINE, IDENTIFICADO (A) CON DNI: 41773365,  DOMICILIADO EN ,  DISTRITO DE , PROVINCIA DE , DEPARTAMENTO DE , Y DE  OTRA PARTE EN CALIDAD DE COMPRADOR EL SEÑOR ALAIN RAVICHAGUA SOTELO, IDENTIFICADO CON DNI: 44892319 DOMICILIADO EN PROG. CUZCO N° 2309, DISTRITO DE HUANCAYO, PROVINCIA DE HUANCAYO, DEPARTAMENTO DE JUNÍN. BAJO LOS TÉRMINOS Y CONDICIONES SIGUIENTES:	\N	\N	12:30:00-05	1         	COPIA	\N	\N
34	JUANITA RUTH HERRERA SOTO	5	2	2020-07-01 01:45:00	PRINCIPAL	CAPITAL DE TRABAJO	20898988	0	NINGUNO		0	0	NINGUNO		0	29	1	12.5700000000000003	1	3	0	2020-07-08	419	12	0	2020-09-24		\N	1	107202034257    	f	\N	2020-07-10 13:05:18.991	4	0	34.9166666666666998	1.0475000000000001	0	36	419	12.5700000000000003	0	0	12	432	2	72	6	0	0	0	0.5	0	2020-07-10 13:05:18.991	\N	\N	0	\N	0	27	\N	\N	HUANCAYO                                          	\N	\N	\N	CONSTE POR EL PRESENTE CONTRATO DE COMPRA  Y VENTA QUE OTORGA DE UNA PARTE, COMO VENDEDOR (CONTRAYENTE DE DEUDA) SEÑOR(A): QUIQUIA CHAGUA TARCILA, IDENTIFICADO (A) CON DNI: 20898988,  DOMICILIADO EN ,  DISTRITO DE , PROVINCIA DE , DEPARTAMENTO DE , Y DE  OTRA PARTE EN CALIDAD DE COMPRADOR EL SEÑOR ALAIN RAVICHAGUA SOTELO, IDENTIFICADO CON DNI: 44892319 DOMICILIADO EN PROG. CUZCO N° 2309, DISTRITO DE HUANCAYO, PROVINCIA DE HUANCAYO, DEPARTAMENTO DE JUNÍN. BAJO LOS TÉRMINOS Y CONDICIONES SIGUIENTES:	\N	\N	12:30:00-05	1         	\N	3862	\N
33	MELISA DINA CHUQUILLANQUI CUNO 	3	2	2020-07-01 10:26:50	PRINCIPAL	CAPITAL DE TRABAJO	41773365	0	NINGUNO		0	0	NINGUNO		0	27	0	0	2	1	0	2020-07-16	278.600000000000023	2	0	2020-07-31	cliente activo para pagar	\N	1	27202033561     	f	\N	2020-07-02 14:11:08.751	4	0	139.300000000000011	0	0	139.300000000000011	278.600000000000023	0	0	0	2	278.600000000000023	1	139.300000000000011	0.699999999999999956	0	0	0	0.5	0	2020-07-02 14:11:08.751	\N	\N	0	\N	0	25	\N	\N	HUANCAYO                                          	\N	\N	\N	CONSTE POR EL PRESENTE CONTRATO DE COMPRA  Y VENTA QUE OTORGA DE UNA PARTE, COMO VENDEDOR (CONTRAYENTE DE DEUDA) SEÑOR(A): VASQUEZ QUERRE LIZ CATHERINE, IDENTIFICADO (A) CON DNI: 41773365,  DOMICILIADO EN ,  DISTRITO DE , PROVINCIA DE , DEPARTAMENTO DE , Y DE  OTRA PARTE EN CALIDAD DE COMPRADOR EL SEÑOR ALAIN RAVICHAGUA SOTELO, IDENTIFICADO CON DNI: 44892319 DOMICILIADO EN PROG. CUZCO N° 2309, DISTRITO DE HUANCAYO, PROVINCIA DE HUANCAYO, DEPARTAMENTO DE JUNÍN. BAJO LOS TÉRMINOS Y CONDICIONES SIGUIENTES:	\N	\N	12:30:00-05	1         	\N	3852	\N
38	KEWIN GILBER VELA PEREZ	11	2	2020-07-01 07:35:53	PRINCIPAL	CAPITAL DE TRABAJO	71340438	0	NINGUNO	ninguno	0	0	NINGUNO		0	34	6	30.870000000000001	0	1	0	2020-07-02	535	25	1.75	2020-07-31		\N	0	\N	\N	\N	\N	\N	\N	21.3999999999999986	1.22999999999999998	0.0700000000000000067	22.6999999999999993	535	30.870000000000001	1.75	0	25	567.5	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	33	\N	\N	PUCALLPA                                          	\N	\N	\N	CONSTE POR EL PRESENTE CONTRATO DE COMPRA  Y VENTA QUE OTORGA DE UNA PARTE, COMO VENDEDOR (CONTRAYENTE DE DEUDA) SEÑOR(A): RIOS PEREZ RAY NILK, IDENTIFICADO (A) CON DNI: 71340438,  DOMICILIADO EN ,  DISTRITO DE , PROVINCIA DE , DEPARTAMENTO DE , Y DE  OTRA PARTE EN CALIDAD DE COMPRADOR EL SEÑOR ALAIN RAVICHAGUA SOTELO, IDENTIFICADO CON DNI: 44892319 DOMICILIADO EN PROG. CUZCO N° 2309, DISTRITO DE HUANCAYO, PROVINCIA DE HUANCAYO, DEPARTAMENTO DE JUNÍN. BAJO LOS TÉRMINOS Y CONDICIONES SIGUIENTES:	\N	\N	02:30:00-05	0         	\N	\N	\N
40	MELISA DINA CHUQUILLANQUI CUNO 	3	0	2020-07-02 09:59:14	PRINCIPAL	CAPITAL DE TRABAJO	40893286	0	NINGUNO		0	0	NINGUNO		0	36	0	0	1	1	0	2020-07-09	139.5	4	0	2020-07-30	cliente activo	\N	0	\N	\N	\N	\N	\N	\N	34.875	0	0	34.8999999999999986	139.5	0	0	0	4	139.599999999999994	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	35	\N	\N	HUANCAYO                                          	\N	\N	\N	CONSTE POR EL PRESENTE CONTRATO DE COMPRA  Y VENTA QUE OTORGA DE UNA PARTE, COMO VENDEDOR (CONTRAYENTE DE DEUDA) SEÑOR(A): PORTA CHUQUILLANQUI JANETH BIVIANA, IDENTIFICADO (A) CON DNI: 40893286,  DOMICILIADO EN ,  DISTRITO DE , PROVINCIA DE , DEPARTAMENTO DE , Y DE  OTRA PARTE EN CALIDAD DE COMPRADOR EL SEÑOR ALAIN RAVICHAGUA SOTELO, IDENTIFICADO CON DNI: 44892319 DOMICILIADO EN PROG. CUZCO N° 2309, DISTRITO DE HUANCAYO, PROVINCIA DE HUANCAYO, DEPARTAMENTO DE JUNÍN. BAJO LOS TÉRMINOS Y CONDICIONES SIGUIENTES:	\N	\N	12:30:00-05	1         	\N	\N	\N
13	JUANITA RUTH HERRERA SOTO	5	2	2020-06-30 03:26:03	PRINCIPAL	CAPITAL DE TRABAJO	76290455	0	NINGUNO		0	0	NINGUNO		0	13	6	13.4499999999999993	0	1	2.5	2020-07-01	224.199999999999989	26	1.56000000000000005	2020-07-31		\N	1	306202013813    	f	\N	2020-06-30 15:11:56.852	4	0	8.61999999999999922	0.520000000000000018	0.0599999999999999978	9.19999999999999929	224.199999999999989	13.4499999999999993	1.56000000000000005	0	26	239.199999999999989	0	0	0	0	0	0	0.5	0	2020-06-30 15:11:56.852	\N	\N	0	\N	0	11	\N	\N	HUANCAYO                                          	\N	\N	\N	CONSTE POR EL PRESENTE CONTRATO DE COMPRA  Y VENTA QUE OTORGA DE UNA PARTE, COMO VENDEDOR (CONTRAYENTE DE DEUDA) SEÑOR(A): CONDORI ATAUCUSI JHONY, IDENTIFICADO (A) CON DNI: 76290455,  DOMICILIADO EN ,  DISTRITO DE , PROVINCIA DE , DEPARTAMENTO DE , Y DE  OTRA PARTE EN CALIDAD DE COMPRADOR EL SEÑOR ALAIN RAVICHAGUA SOTELO, IDENTIFICADO CON DNI: 44892319 DOMICILIADO EN PROG. CUZCO N° 2309, DISTRITO DE HUANCAYO, PROVINCIA DE HUANCAYO, DEPARTAMENTO DE JUNÍN. BAJO LOS TÉRMINOS Y CONDICIONES SIGUIENTES:	\N	\N	12:30:00-05	1         	\N	3848	\N
22	JUANITA RUTH HERRERA SOTO	5	2	2020-06-30 05:58:58	PRINCIPAL	CAPITAL DE TRABAJO	44279232	0	GARANTIAS FLOTANTE	AHORRO PROGRAMADO 	50	0	NINGUNO		0	20	1	0.565749999999999975	1	0	0	2020-07-07	226.300000000000011	1	0.0299999999999999989	2020-07-07		\N	1	306202022341    	f	\N	2020-06-30 15:28:25.148	4	1	226.300000000000011	0.565749999999999975	0.0299999999999999989	226.900000000000006	0	0	0	0	0	0	0	0	0	0	0	0	9	9	2020-07-16 13:04:52.554	2	2020-07-16 01:04:07	5.40000000000000036	PRECANCELACION	0.100000000000000006	18	\N	\N	HUANCAYO                                          	\N	\N	\N	CONSTE POR EL PRESENTE CONTRATO DE COMPRA  Y VENTA QUE OTORGA DE UNA PARTE, COMO VENDEDOR (CONTRAYENTE DE DEUDA) SEÑOR(A): HUANAY YUPANQUI JONATHAN, IDENTIFICADO (A) CON DNI: 44279232,  DOMICILIADO EN ,  DISTRITO DE , PROVINCIA DE , DEPARTAMENTO DE , Y DE  OTRA PARTE EN CALIDAD DE COMPRADOR EL SEÑOR ALAIN RAVICHAGUA SOTELO, IDENTIFICADO CON DNI: 44892319 DOMICILIADO EN PROG. CUZCO N° 2309, DISTRITO DE HUANCAYO, PROVINCIA DE HUANCAYO, DEPARTAMENTO DE JUNÍN. BAJO LOS TÉRMINOS Y CONDICIONES SIGUIENTES:	\N	\N	12:30:00-05	1         	\N	3848	\N
39	MELISA DINA CHUQUILLANQUI CUNO 	3	2	2020-07-02 09:22:17	PRINCIPAL	CAPITAL DE TRABAJO	46485289	0	NINGUNO		0	0	NINGUNO		0	35	0	0	1	3	0	2020-07-09	528	12	0	2020-09-24	CLIENTE TIENE DIFICULTADES CON EL PAGO, POR ESO SE LE PROGRAMO SEMANAL	\N	1	27202039520     	f	\N	2020-07-02 13:57:07.199	4	0	44	0	0	44	528	0	0	0	12	528	2	88	6	0	0	0	0.5	0	2020-07-02 13:57:07.199	\N	\N	0	\N	0	34	\N	\N	HUANCAYO                                          	\N	\N	\N	CONSTE POR EL PRESENTE CONTRATO DE COMPRA  Y VENTA QUE OTORGA DE UNA PARTE, COMO VENDEDOR (CONTRAYENTE DE DEUDA) SEÑOR(A): PIZARRO PEREZ IRMA OLGA, IDENTIFICADO (A) CON DNI: 46485289,  DOMICILIADO EN ,  DISTRITO DE , PROVINCIA DE , DEPARTAMENTO DE , Y DE  OTRA PARTE EN CALIDAD DE COMPRADOR EL SEÑOR ALAIN RAVICHAGUA SOTELO, IDENTIFICADO CON DNI: 44892319 DOMICILIADO EN PROG. CUZCO N° 2309, DISTRITO DE HUANCAYO, PROVINCIA DE HUANCAYO, DEPARTAMENTO DE JUNÍN. BAJO LOS TÉRMINOS Y CONDICIONES SIGUIENTES:	\N	\N	12:30:00-05	1         	\N	3852	\N
111	JUANITA RUTH HERRERA SOTO	5	2	2020-07-13 11:28:08	PRINCIPAL	CAPITAL DE TRABAJO	23701995	0	GARANTIAS FLOTANTE	AHORRO PROGRAMADO	400	0	NINGUNO		0	12	7	70	2	1	0	2020-07-28	1000	2	0	2020-08-12		\N	1	1372020111781   	f	\N	2020-07-13 11:51:54.973	4	0	500	35	0	535	500	35	0	0	1	535	0	0	0	0	0	0	0.5	0	2020-08-10 10:14:08.327	\N	\N	0	\N	0	10	\N	\N	HUANCAYO                                          	\N	\N	\N	CONSTE POR EL PRESENTE CONTRATO DE COMPRA  Y VENTA QUE OTORGA DE UNA PARTE, COMO VENDEDOR (CONTRAYENTE DE DEUDA) SEÑOR(A): SOTO HUARCAHUAMAN JUANA, IDENTIFICADO (A) CON DNI: 23701995,  DOMICILIADO EN ,  DISTRITO DE , PROVINCIA DE , DEPARTAMENTO DE , Y DE  OTRA PARTE EN CALIDAD DE COMPRADOR EL SEÑOR ALAIN RAVICHAGUA SOTELO, IDENTIFICADO CON DNI: 44892319 DOMICILIADO EN PROG. CUZCO N° 2309, DISTRITO DE HUANCAYO, PROVINCIA DE HUANCAYO, DEPARTAMENTO DE JUNÍN. BAJO LOS TÉRMINOS Y CONDICIONES SIGUIENTES:	\N	\N	12:30:00-05	1         	\N	3866	\N
14	JUANITA RUTH HERRERA SOTO	5	2	2020-06-30 03:47:53	PRINCIPAL	CAPITAL DE TRABAJO	46357683	0	GARANTIAS FLOTANTE	GARANTIA DE AHORRO PROGRAMADO 	600	0	NINGUNO		0	9	6	61.3740000000000023	4	0	0	2020-07-01	1022.89999999999998	1	0	2020-07-01		\N	1	306202014802    	f	\N	2020-06-30 15:16:42.101	4	1	1023	61	0	1084	0	0	0	0	0	0	0	0	1.5	1.5	0	0	15	15	2020-07-22 12:48:56.174	\N	\N	0	\N	0	7	\N	\N	HUANCAYO                                          	\N	\N	\N	CONSTE POR EL PRESENTE CONTRATO DE COMPRA  Y VENTA QUE OTORGA DE UNA PARTE, COMO VENDEDOR (CONTRAYENTE DE DEUDA) SEÑOR(A): POMA BLANCAS KETY JANET, IDENTIFICADO (A) CON DNI: 46357683,  DOMICILIADO EN ,  DISTRITO DE , PROVINCIA DE , DEPARTAMENTO DE , Y DE  OTRA PARTE EN CALIDAD DE COMPRADOR EL SEÑOR ALAIN RAVICHAGUA SOTELO, IDENTIFICADO CON DNI: 44892319 DOMICILIADO EN PROG. CUZCO N° 2309, DISTRITO DE HUANCAYO, PROVINCIA DE HUANCAYO, DEPARTAMENTO DE JUNÍN. BAJO LOS TÉRMINOS Y CONDICIONES SIGUIENTES:	\N	\N	12:30:00-05	1         	\N	3848	\N
37	MELISA DINA CHUQUILLANQUI CUNO 	3	2	2020-07-01 11:36:03	PRINCIPAL	CAPITAL DE TRABAJO	21276617	0	GARANTIAS FLOTANTE	ahorro a plazo fijo	550	0	NINGUNO		0	31	4	54	2	3	0	2020-07-16	450	6	0	2020-09-30	cliente activo	\N	1	27202037352     	f	\N	2020-07-02 14:05:41.247	4	0	75	9	0	84	375	45	0	0	5	420	0	0	0.5	0	0	0	0.5	0	2020-07-22 13:41:43.278	\N	\N	0	\N	0	29	\N	\N	HUANCAYO                                          	\N	\N	\N	CONSTE POR EL PRESENTE CONTRATO DE COMPRA  Y VENTA QUE OTORGA DE UNA PARTE, COMO VENDEDOR (CONTRAYENTE DE DEUDA) SEÑOR(A): GUZMAN ESTRADA EDILBERTO, IDENTIFICADO (A) CON DNI: 21276617,  DOMICILIADO EN ,  DISTRITO DE , PROVINCIA DE , DEPARTAMENTO DE , Y DE  OTRA PARTE EN CALIDAD DE COMPRADOR EL SEÑOR ALAIN RAVICHAGUA SOTELO, IDENTIFICADO CON DNI: 44892319 DOMICILIADO EN PROG. CUZCO N° 2309, DISTRITO DE HUANCAYO, PROVINCIA DE HUANCAYO, DEPARTAMENTO DE JUNÍN. BAJO LOS TÉRMINOS Y CONDICIONES SIGUIENTES:	\N	\N	12:30:00-05	1         	\N	3852	\N
42	MELISA DINA CHUQUILLANQUI CUNO 	3	2	2020-07-02 11:30:03	PRINCIPAL	CAPITAL DE TRABAJO	19942242	0	GARANTIAS FLOTANTE	ahorros programados	942	0	NINGUNO		0	37	0	0	1	2	0	2020-07-09	485.600000000000023	8	0	2020-08-27	cliente activo	\N	1	27202042357     	f	\N	2020-07-02 13:45:11.554	4	0	60.7000000000000028	0	0	60.7000000000000028	485.600000000000023	0	0	0	8	485.600000000000023	2	121.400000000000006	6	0	0	0	0.5	0	2020-07-02 13:45:11.554	\N	\N	0	\N	0	36	\N	\N	HUANCAYO                                          	\N	\N	\N	CONSTE POR EL PRESENTE CONTRATO DE COMPRA  Y VENTA QUE OTORGA DE UNA PARTE, COMO VENDEDOR (CONTRAYENTE DE DEUDA) SEÑOR(A): ROJAS TOVAR FILOMENA, IDENTIFICADO (A) CON DNI: 19942242,  DOMICILIADO EN ,  DISTRITO DE , PROVINCIA DE , DEPARTAMENTO DE , Y DE  OTRA PARTE EN CALIDAD DE COMPRADOR EL SEÑOR ALAIN RAVICHAGUA SOTELO, IDENTIFICADO CON DNI: 44892319 DOMICILIADO EN PROG. CUZCO N° 2309, DISTRITO DE HUANCAYO, PROVINCIA DE HUANCAYO, DEPARTAMENTO DE JUNÍN. BAJO LOS TÉRMINOS Y CONDICIONES SIGUIENTES:	\N	\N	12:30:00-05	1         	\N	3852	\N
41	MELISA DINA CHUQUILLANQUI CUNO 	3	2	2020-07-02 10:41:24	PRINCIPAL	CAPITAL DE TRABAJO	40893286	0	GARANTIAS FLOTANTE	ahorros programados	440	0	NINGUNO		0	36	0	0	1	1	0	2020-07-09	579.5	4	0	2020-07-30	cliente activo	\N	1	27202041871     	f	\N	2020-07-02 13:54:04.992	4	0	144.875	0	0	144.900000000000006	579.5	0	0	0	4	579.600000000000023	2	289.800000000000011	6	0	0	0	0.5	0	2020-07-02 13:54:04.992	\N	\N	0	\N	0	35	\N	\N	HUANCAYO                                          	\N	\N	\N	CONSTE POR EL PRESENTE CONTRATO DE COMPRA  Y VENTA QUE OTORGA DE UNA PARTE, COMO VENDEDOR (CONTRAYENTE DE DEUDA) SEÑOR(A): PORTA CHUQUILLANQUI JANETH BIVIANA, IDENTIFICADO (A) CON DNI: 40893286,  DOMICILIADO EN ,  DISTRITO DE , PROVINCIA DE , DEPARTAMENTO DE , Y DE  OTRA PARTE EN CALIDAD DE COMPRADOR EL SEÑOR ALAIN RAVICHAGUA SOTELO, IDENTIFICADO CON DNI: 44892319 DOMICILIADO EN PROG. CUZCO N° 2309, DISTRITO DE HUANCAYO, PROVINCIA DE HUANCAYO, DEPARTAMENTO DE JUNÍN. BAJO LOS TÉRMINOS Y CONDICIONES SIGUIENTES:	\N	\N	12:30:00-05	1         	\N	3852	\N
43	MELISA DINA CHUQUILLANQUI CUNO 	3	2	2020-07-02 01:14:55	PRINCIPAL	CAPITAL DE TRABAJO	19944388	0	GARANTIAS FLOTANTE	ahorros programados	357.299988	0	NINGUNO		0	38	0	0	1	4	0	2020-07-09	1003.5	16	0	2020-10-23	cliente activo	\N	1	27202043255     	f	\N	2020-07-02 13:49:54.975	4	0	62.71875	0	0	62.75	1003.5	0	0	0	16	1004	2	125.5	6	0	0	0	0.5	0	2020-07-02 13:49:54.975	\N	\N	0	\N	0	37	\N	\N	HUANCAYO                                          	\N	\N	\N	CONSTE POR EL PRESENTE CONTRATO DE COMPRA  Y VENTA QUE OTORGA DE UNA PARTE, COMO VENDEDOR (CONTRAYENTE DE DEUDA) SEÑOR(A): PORTA ADAUTO GLORIA BERNARDINA, IDENTIFICADO (A) CON DNI: 19944388,  DOMICILIADO EN ,  DISTRITO DE , PROVINCIA DE , DEPARTAMENTO DE , Y DE  OTRA PARTE EN CALIDAD DE COMPRADOR EL SEÑOR ALAIN RAVICHAGUA SOTELO, IDENTIFICADO CON DNI: 44892319 DOMICILIADO EN PROG. CUZCO N° 2309, DISTRITO DE HUANCAYO, PROVINCIA DE HUANCAYO, DEPARTAMENTO DE JUNÍN. BAJO LOS TÉRMINOS Y CONDICIONES SIGUIENTES:	\N	\N	12:30:00-05	1         	\N	3852	\N
44	JUANITA RUTH HERRERA SOTO	5	0	2020-07-03 12:28:36	PRINCIPAL	CAPITAL DE TRABAJO	44326038	0	GARANTIAS FLOTANTE	AHORRO PROGRAMADO	150.100006	0	NINGUNO		0	32	1	18.2402967194614014	3	4	0	2020-08-03	726	4	0	2020-11-02		\N	0	\N	\N	\N	\N	\N	\N	726	4.55999999999999961	0	186.060074179865012	726	18.2402967194614014	0	0	4	744.240296719460957	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	30	\N	\N	HUANCAYO                                          	\N	\N	\N	CONSTE POR EL PRESENTE CONTRATO DE COMPRA  Y VENTA QUE OTORGA DE UNA PARTE, COMO VENDEDOR (CONTRAYENTE DE DEUDA) SEÑOR(A): QUISPE VERA TRINIDAD, IDENTIFICADO (A) CON DNI: 44326038,  DOMICILIADO EN ,  DISTRITO DE , PROVINCIA DE , DEPARTAMENTO DE , Y DE  OTRA PARTE EN CALIDAD DE COMPRADOR EL SEÑOR ALAIN RAVICHAGUA SOTELO, IDENTIFICADO CON DNI: 44892319 DOMICILIADO EN PROG. CUZCO N° 2309, DISTRITO DE HUANCAYO, PROVINCIA DE HUANCAYO, DEPARTAMENTO DE JUNÍN. BAJO LOS TÉRMINOS Y CONDICIONES SIGUIENTES:	\N	\N	12:30:00-05	1         	\N	\N	\N
45	JUANITA RUTH HERRERA SOTO	5	2	2020-07-03 01:28:36	PRINCIPAL	CAPITAL DE TRABAJO	77161207	0	NINGUNO		0	0	NINGUNO		0	39	1	36	2	3	0	2020-07-18	1200	6	0	2020-10-02		\N	1	37202045134     	f	\N	2020-07-03 13:19:57.976	4	0	200	6	0	206	1200	36	0	0	6	1236	0	0	0	0	0	0	0.5	0	2020-07-03 13:19:57.976	\N	\N	0	\N	0	38	\N	\N	HUANCAYO                                          	\N	\N	\N	CONSTE POR EL PRESENTE CONTRATO DE COMPRA  Y VENTA QUE OTORGA DE UNA PARTE, COMO VENDEDOR (CONTRAYENTE DE DEUDA) SEÑOR(A): PEREZ  PORTA ANYELA , IDENTIFICADO (A) CON DNI: 77161207,  DOMICILIADO EN ,  DISTRITO DE , PROVINCIA DE , DEPARTAMENTO DE , Y DE  OTRA PARTE EN CALIDAD DE COMPRADOR EL SEÑOR ALAIN RAVICHAGUA SOTELO, IDENTIFICADO CON DNI: 44892319 DOMICILIADO EN PROG. CUZCO N° 2309, DISTRITO DE HUANCAYO, PROVINCIA DE HUANCAYO, DEPARTAMENTO DE JUNÍN. BAJO LOS TÉRMINOS Y CONDICIONES SIGUIENTES:	\N	\N	12:30:00-05	1         	\N	3853	\N
48	JUANITA RUTH HERRERA SOTO	5	0	2020-07-03 03:52:39	PRINCIPAL	CAPITAL DE TRABAJO	99907227	0	NINGUNO		0	0	NINGUNO		0	42	1	0.244499999999999995	1	0	0	2020-07-10	97.7999999999999972	1	0.0100000000000000002	2020-07-10		\N	0	\N	\N	\N	\N	\N	\N	97.7999999999999972	0.244499999999999995	0.0100000000000000002	98.0499999999999972	97.7999999999999972	0.244499999999999995	0.0100000000000000002	0	1	98.0499999999999972	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	41	\N	\N	HUANCAYO                                          	\N	\N	\N	CONSTE POR EL PRESENTE CONTRATO DE COMPRA  Y VENTA QUE OTORGA DE UNA PARTE, COMO VENDEDOR (CONTRAYENTE DE DEUDA) SEÑOR(A): QUISPE MENDOZA RAUL, IDENTIFICADO (A) CON DNI: 99907227,  DOMICILIADO EN ,  DISTRITO DE , PROVINCIA DE , DEPARTAMENTO DE , Y DE  OTRA PARTE EN CALIDAD DE COMPRADOR EL SEÑOR ALAIN RAVICHAGUA SOTELO, IDENTIFICADO CON DNI: 44892319 DOMICILIADO EN PROG. CUZCO N° 2309, DISTRITO DE HUANCAYO, PROVINCIA DE HUANCAYO, DEPARTAMENTO DE JUNÍN. BAJO LOS TÉRMINOS Y CONDICIONES SIGUIENTES:	\N	\N	12:30:00-05	1         	\N	\N	\N
49	FRANK MARTINEZ YUPANQUI	2	2	2020-07-03 01:43:26	PRINCIPAL	CAPITAL DE TRABAJO	20065148	0	NINGUNO	NINGUNO	0	0	NINGUNO		0	44	1	20.0663344444032994	3	3	0	2020-08-03	1000	3	0	2020-10-02		\N	1	37202049556     	f	\N	2020-07-03 13:46:04.272	4	0	1000	13.4199999999999999	0	340.022111481468016	1000	20.0663344444032994	0	0	3	1020.06633444440001	0	0	0	0	0	0	0.5	0	2020-07-03 13:46:04.272	\N	\N	0	\N	0	43	\N	\N	HUANCAYO                                          	\N	\N	\N	CONSTE POR EL PRESENTE CONTRATO DE COMPRA  Y VENTA QUE OTORGA DE UNA PARTE, COMO VENDEDOR (CONTRAYENTE DE DEUDA) SEÑOR(A): CAPACYACHI PORRAS BANZER PAUL, IDENTIFICADO (A) CON DNI: 20065148,  DOMICILIADO EN ,  DISTRITO DE , PROVINCIA DE , DEPARTAMENTO DE , Y DE  OTRA PARTE EN CALIDAD DE COMPRADOR EL SEÑOR ALAIN RAVICHAGUA SOTELO, IDENTIFICADO CON DNI: 44892319 DOMICILIADO EN PROG. CUZCO N° 2309, DISTRITO DE HUANCAYO, PROVINCIA DE HUANCAYO, DEPARTAMENTO DE JUNÍN. BAJO LOS TÉRMINOS Y CONDICIONES SIGUIENTES:	\N	\N	01:30:00-05	0         	\N	3853	\N
50	JUANITA RUTH HERRERA SOTO	5	0	2020-07-03 04:46:30	PRINCIPAL	CAPITAL DE TRABAJO	80535329	0	GARANTIAS FLOTANTE	AHORRO PROGRAMADO 	65	0	NINGUNO		0	43	1	0.417499999999999982	1	0	0	2020-07-10	83.5	2	0.0400000000000000008	2020-07-17		\N	0	\N	\N	\N	\N	\N	\N	41.75	0.208749999999999991	0.0200000000000000004	42	83.5	0.417499999999999982	0.0400000000000000008	0	2	84	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	42	\N	\N	HUANCAYO                                          	\N	\N	\N	CONSTE POR EL PRESENTE CONTRATO DE COMPRA  Y VENTA QUE OTORGA DE UNA PARTE, COMO VENDEDOR (CONTRAYENTE DE DEUDA) SEÑOR(A): QUISPE ROJAS ALDO, IDENTIFICADO (A) CON DNI: 80535329,  DOMICILIADO EN ,  DISTRITO DE , PROVINCIA DE , DEPARTAMENTO DE , Y DE  OTRA PARTE EN CALIDAD DE COMPRADOR EL SEÑOR ALAIN RAVICHAGUA SOTELO, IDENTIFICADO CON DNI: 44892319 DOMICILIADO EN PROG. CUZCO N° 2309, DISTRITO DE HUANCAYO, PROVINCIA DE HUANCAYO, DEPARTAMENTO DE JUNÍN. BAJO LOS TÉRMINOS Y CONDICIONES SIGUIENTES:	\N	\N	12:30:00-05	1         	\N	\N	\N
55	JUANITA RUTH HERRERA SOTO	5	2	2020-07-03 05:23:47	PRINCIPAL	CAPITAL DE TRABAJO	71872891	0	NINGUNO		0	0	NINGUNO		0	7	1	4.20000000000000018	2	1	0	2020-07-18	420	2	0	2020-08-03		\N	1	107202055759    	f	\N	2020-07-10 13:04:01.904	4	0	210	2.10000000000000009	0	212.099999999999994	76.0999999999999943	2.10000000000000009	0	0	1	78.2000000000000028	0	0	0	0	0	0	0.5	0	2020-07-13 10:33:27.744	\N	\N	0	\N	0	5	\N	\N	HUANCAYO                                          	\N	\N	\N	CONSTE POR EL PRESENTE CONTRATO DE COMPRA  Y VENTA QUE OTORGA DE UNA PARTE, COMO VENDEDOR (CONTRAYENTE DE DEUDA) SEÑOR(A): ACOSTA GASPAR MARINA, IDENTIFICADO (A) CON DNI: 71872891,  DOMICILIADO EN ,  DISTRITO DE , PROVINCIA DE , DEPARTAMENTO DE , Y DE  OTRA PARTE EN CALIDAD DE COMPRADOR EL SEÑOR ALAIN RAVICHAGUA SOTELO, IDENTIFICADO CON DNI: 44892319 DOMICILIADO EN PROG. CUZCO N° 2309, DISTRITO DE HUANCAYO, PROVINCIA DE HUANCAYO, DEPARTAMENTO DE JUNÍN. BAJO LOS TÉRMINOS Y CONDICIONES SIGUIENTES:	\N	\N	12:30:00-05	1         	\N	3862	\N
46	JUANITA RUTH HERRERA SOTO	5	2	2020-07-03 02:56:27	PRINCIPAL	CAPITAL DE TRABAJO	45666252	0	GARANTIAS FLOTANTE	ahorro programado	5	0	NINGUNO		0	40	1	2.81000000000000005	0	1	0	2020-07-04	280.800000000000011	26	0	2020-08-04		\N	1	107202046781    	f	\N	2020-07-10 13:08:04.936	4	0	10.8000000000000007	0.110000000000000001	0	10.9000000000000004	280.800000000000011	2.81000000000000005	0	0	26	283.399999999999977	11	119.900000000000006	1	0	0	0	0.5	0	2020-07-10 13:08:04.936	\N	\N	0	\N	0	39	\N	\N	HUANCAYO                                          	\N	\N	\N	CONSTE POR EL PRESENTE CONTRATO DE COMPRA  Y VENTA QUE OTORGA DE UNA PARTE, COMO VENDEDOR (CONTRAYENTE DE DEUDA) SEÑOR(A): SEGUIL RICSE MIGUEL ANGEL, IDENTIFICADO (A) CON DNI: 45666252,  DOMICILIADO EN ,  DISTRITO DE , PROVINCIA DE , DEPARTAMENTO DE , Y DE  OTRA PARTE EN CALIDAD DE COMPRADOR EL SEÑOR ALAIN RAVICHAGUA SOTELO, IDENTIFICADO CON DNI: 44892319 DOMICILIADO EN PROG. CUZCO N° 2309, DISTRITO DE HUANCAYO, PROVINCIA DE HUANCAYO, DEPARTAMENTO DE JUNÍN. BAJO LOS TÉRMINOS Y CONDICIONES SIGUIENTES:	\N	\N	12:30:00-05	1         	\N	3862	\N
51	FRANK MARTINEZ YUPANQUI	2	2	2020-07-03 01:55:19	PRINCIPAL	CAPITAL DE TRABAJO	47846248	0	PRENDIARIOS	LAPTOP HP + CARGADOR	550	0	NINGUNO		0	45	8	20	2	0	0	2020-07-18	500	1	0	2020-07-18		\N	1	37202051028     	f	\N	2020-07-03 14:05:27.8	4	2	500	20	0	520	0	0	0	0	0	0	0	0	0	0	0	0	0.5	0	2020-07-03 14:06:47.401	\N	\N	0	\N	0	44	\N	\N	HUANCAYO                                          	\N	\N	\N	CONSTE POR EL PRESENTE CONTRATO DE COMPRA  Y VENTA QUE OTORGA DE UNA PARTE, COMO VENDEDOR (CONTRAYENTE DE DEUDA) SEÑOR(A): CASTRO INGARUCA PILAR ROSARIO, IDENTIFICADO (A) CON DNI: 47846248,  DOMICILIADO EN ,  DISTRITO DE , PROVINCIA DE , DEPARTAMENTO DE , Y DE  OTRA PARTE EN CALIDAD DE COMPRADOR EL SEÑOR ALAIN RAVICHAGUA SOTELO, IDENTIFICADO CON DNI: 44892319 DOMICILIADO EN PROG. CUZCO N° 2309, DISTRITO DE HUANCAYO, PROVINCIA DE HUANCAYO, DEPARTAMENTO DE JUNÍN. BAJO LOS TÉRMINOS Y CONDICIONES SIGUIENTES:	\N	\N	01:30:00-05	0         	\N	3853	\N
53	JUANITA RUTH HERRERA SOTO	5	0	2020-07-03 05:18:53	PRINCIPAL	CAPITAL DE TRABAJO	44326038	0	NINGUNO		0	0	NINGUNO		0	32	8	150.778016134529992	3	4	0	2020-08-03	726	4	0	2020-11-02		\N	0	\N	\N	\N	\N	\N	\N	726	0	0	219.194504033632001	726	150.778016134529992	0	0	4	876.778016134530048	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	30	\N	\N	HUANCAYO                                          	\N	\N	\N	CONSTE POR EL PRESENTE CONTRATO DE COMPRA  Y VENTA QUE OTORGA DE UNA PARTE, COMO VENDEDOR (CONTRAYENTE DE DEUDA) SEÑOR(A): QUISPE VERA TRINIDAD, IDENTIFICADO (A) CON DNI: 44326038,  DOMICILIADO EN ,  DISTRITO DE , PROVINCIA DE , DEPARTAMENTO DE , Y DE  OTRA PARTE EN CALIDAD DE COMPRADOR EL SEÑOR ALAIN RAVICHAGUA SOTELO, IDENTIFICADO CON DNI: 44892319 DOMICILIADO EN PROG. CUZCO N° 2309, DISTRITO DE HUANCAYO, PROVINCIA DE HUANCAYO, DEPARTAMENTO DE JUNÍN. BAJO LOS TÉRMINOS Y CONDICIONES SIGUIENTES:	\N	\N	12:30:00-05	1         	\N	\N	\N
54	JUANITA RUTH HERRERA SOTO	5	0	2020-07-03 05:21:01	PRINCIPAL	CAPITAL DE TRABAJO	80145065	0	NINGUNO		0	0	NINGUNO		0	26	8	187.439999999999998	1	1	0	2020-07-10	2343	4	0	2020-07-31		\N	0	\N	\N	\N	\N	\N	\N	585.75	46.8599999999999994	0	632.600000000000023	2343	187.439999999999998	0	0	4	2530.40000000000009	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	24	\N	\N	HUANCAYO                                          	\N	\N	\N	CONSTE POR EL PRESENTE CONTRATO DE COMPRA  Y VENTA QUE OTORGA DE UNA PARTE, COMO VENDEDOR (CONTRAYENTE DE DEUDA) SEÑOR(A): PALOMINO RIOS FLORA MERY, IDENTIFICADO (A) CON DNI: 80145065,  DOMICILIADO EN ,  DISTRITO DE , PROVINCIA DE , DEPARTAMENTO DE , Y DE  OTRA PARTE EN CALIDAD DE COMPRADOR EL SEÑOR ALAIN RAVICHAGUA SOTELO, IDENTIFICADO CON DNI: 44892319 DOMICILIADO EN PROG. CUZCO N° 2309, DISTRITO DE HUANCAYO, PROVINCIA DE HUANCAYO, DEPARTAMENTO DE JUNÍN. BAJO LOS TÉRMINOS Y CONDICIONES SIGUIENTES:	\N	\N	12:30:00-05	1         	\N	\N	\N
52	JUANITA RUTH HERRERA SOTO	5	2	2020-07-03 05:15:47	PRINCIPAL	CAPITAL DE TRABAJO	41274111	0	NINGUNO		0	0	NINGUNO		0	30	8	276.935999999999979	1	3	0	2020-07-10	1153.90000000000009	12	0	2020-09-25		\N	1	207202052624    	f	\N	2020-07-20 12:14:29.929	4	0	96.158333333333303	23.0779999999999994	0	119.25	1153.90000000000009	276.935999999999979	0	0	12	1431	0	0	0	0	0	0	0.5	0	2020-07-20 12:14:29.929	\N	\N	0	\N	0	28	\N	\N	HUANCAYO                                          	\N	\N	\N	CONSTE POR EL PRESENTE CONTRATO DE COMPRA  Y VENTA QUE OTORGA DE UNA PARTE, COMO VENDEDOR (CONTRAYENTE DE DEUDA) SEÑOR(A): ROJAS VILCAHUAMAN ORLANDINI, IDENTIFICADO (A) CON DNI: 41274111,  DOMICILIADO EN ,  DISTRITO DE , PROVINCIA DE , DEPARTAMENTO DE , Y DE  OTRA PARTE EN CALIDAD DE COMPRADOR EL SEÑOR ALAIN RAVICHAGUA SOTELO, IDENTIFICADO CON DNI: 44892319 DOMICILIADO EN PROG. CUZCO N° 2309, DISTRITO DE HUANCAYO, PROVINCIA DE HUANCAYO, DEPARTAMENTO DE JUNÍN. BAJO LOS TÉRMINOS Y CONDICIONES SIGUIENTES:	\N	\N	12:30:00-05	1         	\N	3877	\N
57	JUANITA RUTH HERRERA SOTO	5	0	2020-07-03 05:27:39	PRINCIPAL	CAPITAL DE TRABAJO	44974563	0	NINGUNO		0	0	NINGUNO		0	2	8	235.512	1	3	0	2020-07-10	981.299999999999955	12	0	2020-09-25		\N	0	\N	\N	\N	\N	\N	\N	81.7750000000000057	19.6260000000000012	0	101.400000000000006	981.299999999999955	235.512	0	0	12	1216.79999999999995	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	1	\N	\N	HUANCAYO                                          	\N	\N	\N	CONSTE POR EL PRESENTE CONTRATO DE COMPRA  Y VENTA QUE OTORGA DE UNA PARTE, COMO VENDEDOR (CONTRAYENTE DE DEUDA) SEÑOR(A): SEGOVIA BARBOZA DENISSA ZOILA, IDENTIFICADO (A) CON DNI: 44974563,  DOMICILIADO EN ,  DISTRITO DE , PROVINCIA DE , DEPARTAMENTO DE , Y DE  OTRA PARTE EN CALIDAD DE COMPRADOR EL SEÑOR ALAIN RAVICHAGUA SOTELO, IDENTIFICADO CON DNI: 44892319 DOMICILIADO EN PROG. CUZCO N° 2309, DISTRITO DE HUANCAYO, PROVINCIA DE HUANCAYO, DEPARTAMENTO DE JUNÍN. BAJO LOS TÉRMINOS Y CONDICIONES SIGUIENTES:	\N	\N	12:30:00-05	1         	\N	\N	\N
112	JUANITA RUTH HERRERA SOTO	5	0	2020-07-13 01:06:48	PRINCIPAL	CAPITAL DE TRABAJO	44100810	0	NINGUNO		0	0	NINGUNO		0	86	8	46.6559999999999988	1	2	0	2020-07-20	291.600000000000023	8	0	2020-09-07		\N	0	\N	\N	\N	\N	\N	\N	36.4500000000000028	5.83199999999999985	0	42.2999999999999972	291.600000000000023	46.6559999999999988	0	0	8	338.399999999999977	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	91	\N	\N	HUANCAYO                                          	\N	\N	\N	CONSTE POR EL PRESENTE CONTRATO DE COMPRA  Y VENTA QUE OTORGA DE UNA PARTE, COMO VENDEDOR (CONTRAYENTE DE DEUDA) SEÑOR(A): CABRERA GOMEZ BEATRIS LUCIANA , IDENTIFICADO (A) CON DNI: 44100810,  DOMICILIADO EN ,  DISTRITO DE , PROVINCIA DE , DEPARTAMENTO DE , Y DE  OTRA PARTE EN CALIDAD DE COMPRADOR EL SEÑOR ALAIN RAVICHAGUA SOTELO, IDENTIFICADO CON DNI: 44892319 DOMICILIADO EN PROG. CUZCO N° 2309, DISTRITO DE HUANCAYO, PROVINCIA DE HUANCAYO, DEPARTAMENTO DE JUNÍN. BAJO LOS TÉRMINOS Y CONDICIONES SIGUIENTES:	\N	\N	12:30:00-05	1         	\N	\N	\N
116	JUANITA RUTH HERRERA SOTO	5	2	2020-07-14 11:38:41	PRINCIPAL	CAPITAL DE TRABAJO	48177932	0	GARANTIAS FLOTANTE	AHORRO PROGRAMADO 	75	0	NINGUNO		0	88	1	0.810000000000000053	0	1	0	2020-07-15	149.699999999999989	14	0.699999999999999956	2020-07-31		\N	1	1672020116866   	f	\N	2020-07-16 14:26:10.511	4	0	10.6899999999999995	0.0599999999999999978	0.0500000000000000028	10.8000000000000007	128.280000000000001	0.719999999999999973	0.599999999999999978	0	12	129.599999999999994	0	0	0	0	0	0	0.5	0	2020-07-17 09:27:15.831	\N	\N	0	\N	0	93	\N	\N	HUANCAYO                                          	\N	\N	\N	CONSTE POR EL PRESENTE CONTRATO DE COMPRA  Y VENTA QUE OTORGA DE UNA PARTE, COMO VENDEDOR (CONTRAYENTE DE DEUDA) SEÑOR(A): GARCIA BORJA MARICRUZ CLESVI, IDENTIFICADO (A) CON DNI: 48177932,  DOMICILIADO EN ,  DISTRITO DE , PROVINCIA DE , DEPARTAMENTO DE , Y DE  OTRA PARTE EN CALIDAD DE COMPRADOR EL SEÑOR ALAIN RAVICHAGUA SOTELO, IDENTIFICADO CON DNI: 44892319 DOMICILIADO EN PROG. CUZCO N° 2309, DISTRITO DE HUANCAYO, PROVINCIA DE HUANCAYO, DEPARTAMENTO DE JUNÍN. BAJO LOS TÉRMINOS Y CONDICIONES SIGUIENTES:	\N	\N	12:30:00-05	1         	\N	3872	\N
56	FRANK MARTINEZ YUPANQUI	2	2	2020-07-03 02:25:46	PRINCIPAL	CAPITAL DE TRABAJO	48024565	0	NINGUNO	NINGUNO	0	0	NINGUNO		0	47	8	79.9999999999991047	3	1	0	2020-08-03	1000	1	0	2020-08-03		\N	1	37202056795     	f	\N	2020-07-03 14:32:41.383	4	2	1000	75	0	1080	0	0	0	0	0	0	0	0	0	0	0	0	0.5	0	2020-07-03 14:33:49.592	\N	\N	0	\N	0	46	\N	\N	HUANCAYO                                          	\N	\N	\N	CONSTE POR EL PRESENTE CONTRATO DE COMPRA  Y VENTA QUE OTORGA DE UNA PARTE, COMO VENDEDOR (CONTRAYENTE DE DEUDA) SEÑOR(A): CONDOR FABIAN FABIOLA, IDENTIFICADO (A) CON DNI: 48024565,  DOMICILIADO EN ,  DISTRITO DE , PROVINCIA DE , DEPARTAMENTO DE , Y DE  OTRA PARTE EN CALIDAD DE COMPRADOR EL SEÑOR ALAIN RAVICHAGUA SOTELO, IDENTIFICADO CON DNI: 44892319 DOMICILIADO EN PROG. CUZCO N° 2309, DISTRITO DE HUANCAYO, PROVINCIA DE HUANCAYO, DEPARTAMENTO DE JUNÍN. BAJO LOS TÉRMINOS Y CONDICIONES SIGUIENTES:	\N	\N	01:30:00-05	0         	\N	3853	\N
58	JUANITA RUTH HERRERA SOTO	5	0	2020-07-03 05:38:14	PRINCIPAL	CAPITAL DE TRABAJO	20898988	0	NINGUNO		0	0	NINGUNO		0	29	8	100.560000000000002	1	3	0	2020-07-10	419	12	0	2020-09-25		\N	0	\N	\N	\N	\N	\N	\N	34.9166666666666998	8.38000000000000078	0	43.2999999999999972	419	100.560000000000002	0	0	12	519.600000000000023	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	27	\N	\N	HUANCAYO                                          	\N	\N	\N	CONSTE POR EL PRESENTE CONTRATO DE COMPRA  Y VENTA QUE OTORGA DE UNA PARTE, COMO VENDEDOR (CONTRAYENTE DE DEUDA) SEÑOR(A): QUIQUIA CHAGUA TARCILA, IDENTIFICADO (A) CON DNI: 20898988,  DOMICILIADO EN ,  DISTRITO DE , PROVINCIA DE , DEPARTAMENTO DE , Y DE  OTRA PARTE EN CALIDAD DE COMPRADOR EL SEÑOR ALAIN RAVICHAGUA SOTELO, IDENTIFICADO CON DNI: 44892319 DOMICILIADO EN PROG. CUZCO N° 2309, DISTRITO DE HUANCAYO, PROVINCIA DE HUANCAYO, DEPARTAMENTO DE JUNÍN. BAJO LOS TÉRMINOS Y CONDICIONES SIGUIENTES:	\N	\N	12:30:00-05	1         	\N	\N	\N
59	MELISA DINA CHUQUILLANQUI CUNO 	3	0	2020-07-04 09:42:43	PRINCIPAL	CAPITAL DE TRABAJO	46196324	0	GARANTIAS FLOTANTE	PLAZO FIJO 	506	0	NINGUNO		0	49	7	210	2	3	0	2020-07-20	1000	6	0.179999999999999993	2020-10-03	CLIENTE ACTIVO	\N	0	\N	\N	\N	\N	\N	\N	166.669999999999987	35	0.0299999999999999989	201.699999999999989	1000	210	0.179999999999999993	0	6	1210.20000000000005	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	47	\N	\N	HUANCAYO                                          	\N	\N	\N	CONSTE POR EL PRESENTE CONTRATO DE COMPRA  Y VENTA QUE OTORGA DE UNA PARTE, COMO VENDEDOR (CONTRAYENTE DE DEUDA) SEÑOR(A): CARDENAS GARCIA NELLY JUANA, IDENTIFICADO (A) CON DNI: 46196324,  DOMICILIADO EN ,  DISTRITO DE , PROVINCIA DE , DEPARTAMENTO DE , Y DE  OTRA PARTE EN CALIDAD DE COMPRADOR EL SEÑOR ALAIN RAVICHAGUA SOTELO, IDENTIFICADO CON DNI: 44892319 DOMICILIADO EN PROG. CUZCO N° 2309, DISTRITO DE HUANCAYO, PROVINCIA DE HUANCAYO, DEPARTAMENTO DE JUNÍN. BAJO LOS TÉRMINOS Y CONDICIONES SIGUIENTES:	\N	\N	12:30:00-05	1         	\N	\N	\N
60	MELISA DINA CHUQUILLANQUI CUNO 	3	0	2020-07-04 09:48:56	PRINCIPAL	CAPITAL DE TRABAJO	46196324	0	GARANTIAS FLOTANTE	AHORRO PROGRAMADO	506	0	NINGUNO		0	49	7	210	2	3	0	2020-07-20	1000	6	0.179999999999999993	2020-10-03	CLIENTE ACTIVO	\N	0	\N	\N	\N	\N	\N	\N	166.669999999999987	35	0.0299999999999999989	201.699999999999989	1000	210	0.179999999999999993	0	6	1210.20000000000005	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	47	\N	\N	HUANCAYO                                          	\N	\N	\N	CONSTE POR EL PRESENTE CONTRATO DE COMPRA  Y VENTA QUE OTORGA DE UNA PARTE, COMO VENDEDOR (CONTRAYENTE DE DEUDA) SEÑOR(A): CARDENAS GARCIA NELLY JUANA, IDENTIFICADO (A) CON DNI: 46196324,  DOMICILIADO EN ,  DISTRITO DE , PROVINCIA DE , DEPARTAMENTO DE , Y DE  OTRA PARTE EN CALIDAD DE COMPRADOR EL SEÑOR ALAIN RAVICHAGUA SOTELO, IDENTIFICADO CON DNI: 44892319 DOMICILIADO EN PROG. CUZCO N° 2309, DISTRITO DE HUANCAYO, PROVINCIA DE HUANCAYO, DEPARTAMENTO DE JUNÍN. BAJO LOS TÉRMINOS Y CONDICIONES SIGUIENTES:	\N	\N	12:30:00-05	1         	\N	\N	\N
64	JUANITA RUTH HERRERA SOTO	5	0	2020-07-04 02:23:12	PRINCIPAL	CAPITAL DE TRABAJO	41123341	0	GARANTIAS FLOTANTE	AHORRO PROGRAMADO 	680	0	NINGUNO		0	19	7	35	0	1	0	2020-07-06	500	26	0.520000000000000018	2020-08-05		\N	0	\N	\N	\N	\N	\N	\N	19.2300000000000004	1.35000000000000009	0.0200000000000000004	20.6000000000000014	500	35	0.520000000000000018	0	26	535.600000000000023	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	17	\N	\N	HUANCAYO                                          	\N	\N	\N	CONSTE POR EL PRESENTE CONTRATO DE COMPRA  Y VENTA QUE OTORGA DE UNA PARTE, COMO VENDEDOR (CONTRAYENTE DE DEUDA) SEÑOR(A): JEREMIAS CALLUPE NATALY MAGALY, IDENTIFICADO (A) CON DNI: 41123341,  DOMICILIADO EN ,  DISTRITO DE , PROVINCIA DE , DEPARTAMENTO DE , Y DE  OTRA PARTE EN CALIDAD DE COMPRADOR EL SEÑOR ALAIN RAVICHAGUA SOTELO, IDENTIFICADO CON DNI: 44892319 DOMICILIADO EN PROG. CUZCO N° 2309, DISTRITO DE HUANCAYO, PROVINCIA DE HUANCAYO, DEPARTAMENTO DE JUNÍN. BAJO LOS TÉRMINOS Y CONDICIONES SIGUIENTES:	\N	\N	12:00:00-05	1         	\N	\N	\N
65	JUANITA RUTH HERRERA SOTO	5	0	2020-07-04 02:57:49	PRINCIPAL	CAPITAL DE TRABAJO	41123341	0	GARANTIAS FLOTANTE	AHORRO PROGRAMADO	680	0	NINGUNO		0	19	7	35	0	1	0	2020-07-06	500	26	0.520000000000000018	2020-08-05		\N	0	\N	\N	\N	\N	\N	\N	19.2300000000000004	1.35000000000000009	0.0200000000000000004	20.6000000000000014	500	35	0.520000000000000018	0	26	535.600000000000023	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	17	\N	\N	HUANCAYO                                          	\N	\N	\N	CONSTE POR EL PRESENTE CONTRATO DE COMPRA  Y VENTA QUE OTORGA DE UNA PARTE, COMO VENDEDOR (CONTRAYENTE DE DEUDA) SEÑOR(A): JEREMIAS CALLUPE NATALY MAGALY, IDENTIFICADO (A) CON DNI: 41123341,  DOMICILIADO EN ,  DISTRITO DE , PROVINCIA DE , DEPARTAMENTO DE , Y DE  OTRA PARTE EN CALIDAD DE COMPRADOR EL SEÑOR ALAIN RAVICHAGUA SOTELO, IDENTIFICADO CON DNI: 44892319 DOMICILIADO EN PROG. CUZCO N° 2309, DISTRITO DE HUANCAYO, PROVINCIA DE HUANCAYO, DEPARTAMENTO DE JUNÍN. BAJO LOS TÉRMINOS Y CONDICIONES SIGUIENTES:	\N	\N	12:30:00-05	1         	\N	\N	\N
121	JUANITA RUTH HERRERA SOTO	5	0	2020-07-17 09:52:08	PRINCIPAL	CAPITAL DE TRABAJO	99907227	0	NINGUNO		0	0	NINGUNO		0	42	1	0.48899999999999999	2	0	0	2020-08-01	97.7999999999999972	1	0.0100000000000000002	2020-08-01		\N	0	\N	\N	\N	\N	\N	\N	97.7999999999999972	0.489999999999999991	0.0100000000000000002	98.2999999999999972	97.7999999999999972	0.48899999999999999	0.0100000000000000002	0	1	98.2999999999999972	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	41	\N	\N	HUANCAYO                                          	\N	\N	\N	CONSTE POR EL PRESENTE CONTRATO DE COMPRA  Y VENTA QUE OTORGA DE UNA PARTE, COMO VENDEDOR (CONTRAYENTE DE DEUDA) SEÑOR(A): QUISPE MENDOZA RAUL, IDENTIFICADO (A) CON DNI: 99907227,  DOMICILIADO EN ,  DISTRITO DE , PROVINCIA DE , DEPARTAMENTO DE , Y DE  OTRA PARTE EN CALIDAD DE COMPRADOR EL SEÑOR ALAIN RAVICHAGUA SOTELO, IDENTIFICADO CON DNI: 44892319 DOMICILIADO EN PROG. CUZCO N° 2309, DISTRITO DE HUANCAYO, PROVINCIA DE HUANCAYO, DEPARTAMENTO DE JUNÍN. BAJO LOS TÉRMINOS Y CONDICIONES SIGUIENTES:	\N	\N	12:30:00-05	1         	\N	\N	\N
61	MELISA DINA CHUQUILLANQUI CUNO 	3	2	2020-07-04 10:31:32	PRINCIPAL	CAPITAL DE TRABAJO	20999895	0	NINGUNO		0	0	NINGUNO		0	50	2	66.1200000000000045	2	4	0	2020-07-20	826.5	8	0.239999999999999991	2020-11-03	CLIENTE MEDIAMENTE ACTIVO	\N	1	167202061474    	f	\N	2020-07-16 14:28:09.84	4	0	103.310000000000002	8.25999999999999979	0.0299999999999999989	111.599999999999994	826.5	66.1200000000000045	0.239999999999999991	0	8	892.799999999999955	0	0	0	0	0	0	0.5	0	2020-07-16 14:28:09.84	\N	\N	0	\N	0	48	\N	\N	HUANCAYO                                          	\N	\N	\N	CONSTE POR EL PRESENTE CONTRATO DE COMPRA  Y VENTA QUE OTORGA DE UNA PARTE, COMO VENDEDOR (CONTRAYENTE DE DEUDA) SEÑOR(A): ORE BECERRA NORMA GLADYS, IDENTIFICADO (A) CON DNI: 20999895,  DOMICILIADO EN ,  DISTRITO DE , PROVINCIA DE , DEPARTAMENTO DE , Y DE  OTRA PARTE EN CALIDAD DE COMPRADOR EL SEÑOR ALAIN RAVICHAGUA SOTELO, IDENTIFICADO CON DNI: 44892319 DOMICILIADO EN PROG. CUZCO N° 2309, DISTRITO DE HUANCAYO, PROVINCIA DE HUANCAYO, DEPARTAMENTO DE JUNÍN. BAJO LOS TÉRMINOS Y CONDICIONES SIGUIENTES:	\N	\N	12:30:00-05	1         	\N	3872	\N
113	FRANK MARTINEZ YUPANQUI	2	2	2020-07-14 09:44:26	PRINCIPAL	CAPITAL DE TRABAJO	41614930	0	NINGUNO	NINGUNA	0	0	NINGUNO		0	87	9	18	0	1	0	2020-07-15	200	26	0.520000000000000018	2020-08-14		\N	1	1472020113128   	f	\N	2020-07-14 10:08:55.845	4	0	7.69000000000000039	0.689999999999999947	0.0200000000000000004	8.40000000000000036	107.560000000000002	9.66000000000000014	0.280000000000000027	0	14	117.5	0	0	1	0	0	0	0.5	0	2020-07-24 12:26:41.598	\N	\N	0	\N	0	92	\N	\N	HUANCAYO                                          	\N	\N	\N	CONSTE POR EL PRESENTE CONTRATO DE COMPRA  Y VENTA QUE OTORGA DE UNA PARTE, COMO VENDEDOR (CONTRAYENTE DE DEUDA) SEÑOR(A): LLANCO ORE CARLOS EDUARDO, IDENTIFICADO (A) CON DNI: 41614930,  DOMICILIADO EN ,  DISTRITO DE , PROVINCIA DE , DEPARTAMENTO DE , Y DE  OTRA PARTE EN CALIDAD DE COMPRADOR EL SEÑOR ALAIN RAVICHAGUA SOTELO, IDENTIFICADO CON DNI: 44892319 DOMICILIADO EN PROG. CUZCO N° 2309, DISTRITO DE HUANCAYO, PROVINCIA DE HUANCAYO, DEPARTAMENTO DE JUNÍN. BAJO LOS TÉRMINOS Y CONDICIONES SIGUIENTES:	\N	\N	01:30:00-05	0         	\N	3868	\N
67	JUANITA RUTH HERRERA SOTO	5	2	2020-07-06 01:29:33	PRINCIPAL	CAPITAL DE TRABAJO	41123341	0	GARANTIAS FLOTANTE	AHORRO PROGRAMADO 	500	0	NINGUNO		0	19	7	35	0	1	0	2020-07-07	500	26	0.520000000000000018	2020-08-06		\N	1	67202067536     	f	\N	2020-07-06 12:44:00.419	4	0	19.2300000000000004	1.35000000000000009	0.0200000000000000004	20.6000000000000014	230.759999999999991	16.1999999999999993	0.239999999999999991	0	12	247.199999999999989	2	41.2000000000000028	1	0	0	0	0.5	0	2020-07-25 12:06:43.61	\N	\N	0	\N	0	17	\N	\N	HUANCAYO                                          	\N	\N	\N	CONSTE POR EL PRESENTE CONTRATO DE COMPRA  Y VENTA QUE OTORGA DE UNA PARTE, COMO VENDEDOR (CONTRAYENTE DE DEUDA) SEÑOR(A): JEREMIAS CALLUPE NATALY MAGALY, IDENTIFICADO (A) CON DNI: 41123341,  DOMICILIADO EN ,  DISTRITO DE , PROVINCIA DE , DEPARTAMENTO DE , Y DE  OTRA PARTE EN CALIDAD DE COMPRADOR EL SEÑOR ALAIN RAVICHAGUA SOTELO, IDENTIFICADO CON DNI: 44892319 DOMICILIADO EN PROG. CUZCO N° 2309, DISTRITO DE HUANCAYO, PROVINCIA DE HUANCAYO, DEPARTAMENTO DE JUNÍN. BAJO LOS TÉRMINOS Y CONDICIONES SIGUIENTES:	\N	\N	12:30:00-05	1         	\N	3855	\N
71	JUANITA RUTH HERRERA SOTO	5	2	2020-07-06 04:44:20	PRINCIPAL	CAPITAL DE TRABAJO	20047216	0	NINGUNO		0	0	NINGUNO		0	58	1	1.5	1	1	0	2020-07-13	200	3	0.0299999999999999989	2020-07-27		\N	1	107202071200    	f	\N	2020-07-10 12:45:30.6	4	0	66.6666666666666998	0.5	0.0100000000000000002	67.2000000000000028	200	1.5	0.0299999999999999989	0	3	201.599999999999994	1	67.2000000000000028	6	0	0	0	0.5	0	2020-07-10 12:45:30.6	\N	\N	0	\N	0	58	\N	\N	HUANCAYO                                          	\N	\N	\N	CONSTE POR EL PRESENTE CONTRATO DE COMPRA  Y VENTA QUE OTORGA DE UNA PARTE, COMO VENDEDOR (CONTRAYENTE DE DEUDA) SEÑOR(A): MENDOZA TOVAR JORGE ANDRES, IDENTIFICADO (A) CON DNI: 20047216,  DOMICILIADO EN ,  DISTRITO DE , PROVINCIA DE , DEPARTAMENTO DE , Y DE  OTRA PARTE EN CALIDAD DE COMPRADOR EL SEÑOR ALAIN RAVICHAGUA SOTELO, IDENTIFICADO CON DNI: 44892319 DOMICILIADO EN PROG. CUZCO N° 2309, DISTRITO DE HUANCAYO, PROVINCIA DE HUANCAYO, DEPARTAMENTO DE JUNÍN. BAJO LOS TÉRMINOS Y CONDICIONES SIGUIENTES:	\N	\N	12:30:00-05	1         	\N	3862	\N
69	JUANITA RUTH HERRERA SOTO	5	2	2020-07-06 02:05:59	PRINCIPAL	CAPITAL DE TRABAJO	70437871	0	NINGUNO		0	0	NINGUNO		0	56	0	0	0	1	0	2020-07-07	1924.70000000000005	26	1.82000000000000006	2020-08-06		\N	1	167202069417    	f	\N	2020-07-16 14:45:36.8	4	0	74.0300000000000011	0	0.0700000000000000067	74.0999999999999943	1924.70000000000005	0	1.82000000000000006	0	26	1926.59999999999991	0	0	0	0	0	0	0.5	0	2020-07-16 14:45:36.8	\N	\N	0	\N	0	56	\N	\N	HUANCAYO                                          	\N	\N	\N	CONSTE POR EL PRESENTE CONTRATO DE COMPRA  Y VENTA QUE OTORGA DE UNA PARTE, COMO VENDEDOR (CONTRAYENTE DE DEUDA) SEÑOR(A): FABIAN ARANDA ERWIN DONALD, IDENTIFICADO (A) CON DNI: 70437871,  DOMICILIADO EN ,  DISTRITO DE , PROVINCIA DE , DEPARTAMENTO DE , Y DE  OTRA PARTE EN CALIDAD DE COMPRADOR EL SEÑOR ALAIN RAVICHAGUA SOTELO, IDENTIFICADO CON DNI: 44892319 DOMICILIADO EN PROG. CUZCO N° 2309, DISTRITO DE HUANCAYO, PROVINCIA DE HUANCAYO, DEPARTAMENTO DE JUNÍN. BAJO LOS TÉRMINOS Y CONDICIONES SIGUIENTES:	\N	\N	12:30:00-05	1         	\N	3872	\N
72	FRANK MARTINEZ YUPANQUI	2	2	2020-06-26 02:02:58	PRINCIPAL	CAPITAL DE TRABAJO	47420055	0	GARANTIAS FLOTANTE	AHORRO PROGRAMADO	0	0	NINGUNO		0	59	4.5	45	0	1	0	2020-06-27	1000	26	0.260000000000000009	2020-07-28		\N	1	67202072569     	f	\N	2020-07-06 14:06:58.83	4	0	38.4600000000000009	1.72999999999999998	0.0100000000000000002	40.2000000000000028	7.75999999999999979	1.72999999999999998	0.0100000000000000002	0	1	9.5	0	0	1	0	0	0	0.5	0	2020-07-22 13:26:12.597	\N	\N	0	\N	0	60	\N	\N	HUANCAYO                                          	\N	\N	\N	CONSTE POR EL PRESENTE CONTRATO DE COMPRA  Y VENTA QUE OTORGA DE UNA PARTE, COMO VENDEDOR (CONTRAYENTE DE DEUDA) SEÑOR(A): CAPCHA CORONACION ALICIA, IDENTIFICADO (A) CON DNI: 47420055,  DOMICILIADO EN ,  DISTRITO DE , PROVINCIA DE , DEPARTAMENTO DE , Y DE  OTRA PARTE EN CALIDAD DE COMPRADOR EL SEÑOR ALAIN RAVICHAGUA SOTELO, IDENTIFICADO CON DNI: 44892319 DOMICILIADO EN PROG. CUZCO N° 2309, DISTRITO DE HUANCAYO, PROVINCIA DE HUANCAYO, DEPARTAMENTO DE JUNÍN. BAJO LOS TÉRMINOS Y CONDICIONES SIGUIENTES:	\N	\N	01:30:00-05	0         	\N	3855	\N
122	JUANITA RUTH HERRERA SOTO	5	0	2020-07-17 10:18:22	PRINCIPAL	CAPITAL DE TRABAJO	44100810	0	NINGUNO		0	0	NINGUNO		0	86	8	152.183999999999997	2	3	0	2020-08-01	634.100000000000023	6	0.359999999999999987	2020-10-16		\N	0	\N	\N	\N	\N	\N	\N	105.680000000000007	25.3599999999999994	0.0599999999999999978	131.099999999999994	634.100000000000023	152.183999999999997	0.359999999999999987	0	6	786.600000000000023	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	91	\N	\N	HUANCAYO                                          	\N	\N	\N	CONSTE POR EL PRESENTE CONTRATO DE COMPRA  Y VENTA QUE OTORGA DE UNA PARTE, COMO VENDEDOR (CONTRAYENTE DE DEUDA) SEÑOR(A): CABRERA GOMEZ BEATRIS LUCIANA , IDENTIFICADO (A) CON DNI: 44100810,  DOMICILIADO EN ,  DISTRITO DE , PROVINCIA DE , DEPARTAMENTO DE , Y DE  OTRA PARTE EN CALIDAD DE COMPRADOR EL SEÑOR ALAIN RAVICHAGUA SOTELO, IDENTIFICADO CON DNI: 44892319 DOMICILIADO EN PROG. CUZCO N° 2309, DISTRITO DE HUANCAYO, PROVINCIA DE HUANCAYO, DEPARTAMENTO DE JUNÍN. BAJO LOS TÉRMINOS Y CONDICIONES SIGUIENTES:	\N	\N	12:30:00-05	1         	\N	\N	\N
114	JUANITA RUTH HERRERA SOTO	5	0	2020-07-14 10:30:05	PRINCIPAL	CAPITAL DE TRABAJO	80245301	0	NINGUNO		0	0	NINGUNO		0	60	1	5.26999999999999957	0	1	0	2020-07-15	380.899999999999977	36	2.52000000000000002	2020-08-26		\N	0	\N	\N	\N	\N	\N	\N	10.5800000000000001	0.149999999999999994	0.0700000000000000067	10.8000000000000007	380.899999999999977	5.26999999999999957	2.52000000000000002	0	36	388.800000000000011	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	62	\N	\N	HUANCAYO                                          	\N	\N	\N	CONSTE POR EL PRESENTE CONTRATO DE COMPRA  Y VENTA QUE OTORGA DE UNA PARTE, COMO VENDEDOR (CONTRAYENTE DE DEUDA) SEÑOR(A): ROMERO ESPIRITU VIRGINIA LUZ, IDENTIFICADO (A) CON DNI: 80245301,  DOMICILIADO EN ,  DISTRITO DE , PROVINCIA DE , DEPARTAMENTO DE , Y DE  OTRA PARTE EN CALIDAD DE COMPRADOR EL SEÑOR ALAIN RAVICHAGUA SOTELO, IDENTIFICADO CON DNI: 44892319 DOMICILIADO EN PROG. CUZCO N° 2309, DISTRITO DE HUANCAYO, PROVINCIA DE HUANCAYO, DEPARTAMENTO DE JUNÍN. BAJO LOS TÉRMINOS Y CONDICIONES SIGUIENTES:	\N	\N	12:30:00-05	1         	\N	\N	\N
78	JUANITA RUTH HERRERA SOTO	5	0	2020-07-07 02:59:00	PRINCIPAL	CAPITAL DE TRABAJO	80245301	0	NINGUNO		0	0	NINGUNO		0	60	4	15.1999999999999993	0	1	0	2020-07-08	380.100000000000023	26	0	2020-08-07		\N	0	\N	\N	\N	\N	\N	\N	14.6199999999999992	0.57999999999999996	0	15.1999999999999993	380.100000000000023	15.1999999999999993	0	0	26	395.199999999999989	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	62	\N	\N	HUANCAYO                                          	\N	\N	\N	CONSTE POR EL PRESENTE CONTRATO DE COMPRA  Y VENTA QUE OTORGA DE UNA PARTE, COMO VENDEDOR (CONTRAYENTE DE DEUDA) SEÑOR(A): ROMERO ESPIRITU VIRGINIA LUZ, IDENTIFICADO (A) CON DNI: 80245301,  DOMICILIADO EN ,  DISTRITO DE , PROVINCIA DE , DEPARTAMENTO DE , Y DE  OTRA PARTE EN CALIDAD DE COMPRADOR EL SEÑOR ALAIN RAVICHAGUA SOTELO, IDENTIFICADO CON DNI: 44892319 DOMICILIADO EN PROG. CUZCO N° 2309, DISTRITO DE HUANCAYO, PROVINCIA DE HUANCAYO, DEPARTAMENTO DE JUNÍN. BAJO LOS TÉRMINOS Y CONDICIONES SIGUIENTES:	\N	\N	12:30:00-05	1         	\N	\N	\N
79	JUANITA RUTH HERRERA SOTO	5	2	2020-07-07 03:06:17	PRINCIPAL	CAPITAL DE TRABAJO	44350264	0	NINGUNO		0	0	NINGUNO		0	61	8	3.496	1	1	0	2020-07-14	43.7000000000000028	4	0	2020-08-04		\N	1	107202079239    	f	\N	2020-07-10 12:59:57.56	4	0	10.9250000000000007	0.873999999999999999	0	11.8000000000000007	43.7000000000000028	3.496	0	0	4	47.2000000000000028	1	11.8000000000000007	6	0	0	0	0.5	0	2020-07-10 12:59:57.56	\N	\N	0	\N	0	63	\N	\N	HUANCAYO                                          	\N	\N	\N	CONSTE POR EL PRESENTE CONTRATO DE COMPRA  Y VENTA QUE OTORGA DE UNA PARTE, COMO VENDEDOR (CONTRAYENTE DE DEUDA) SEÑOR(A): CHAVEZ CHIRINOS JANNETH CECILIA, IDENTIFICADO (A) CON DNI: 44350264,  DOMICILIADO EN ,  DISTRITO DE , PROVINCIA DE , DEPARTAMENTO DE , Y DE  OTRA PARTE EN CALIDAD DE COMPRADOR EL SEÑOR ALAIN RAVICHAGUA SOTELO, IDENTIFICADO CON DNI: 44892319 DOMICILIADO EN PROG. CUZCO N° 2309, DISTRITO DE HUANCAYO, PROVINCIA DE HUANCAYO, DEPARTAMENTO DE JUNÍN. BAJO LOS TÉRMINOS Y CONDICIONES SIGUIENTES:	\N	\N	12:30:00-05	1         	\N	3862	\N
77	JUANITA RUTH HERRERA SOTO	5	2	2020-07-07 02:52:10	PRINCIPAL	CAPITAL DE TRABAJO	19944260	0	NINGUNO		0	0	NINGUNO		0	62	4	9.59999999999999964	1	1	0	2020-07-14	240	4	0	2020-08-04		\N	1	107202077307    	f	\N	2020-07-10 13:02:32.496	4	1	60	2.39999999999999991	0	62.3999999999999986	0	0	0	0	0	0	0	0	6	6	0	0	0.5	0.5	2020-07-17 09:31:03.791	\N	\N	0	\N	0	64	\N	\N	HUANCAYO                                          	\N	\N	\N	CONSTE POR EL PRESENTE CONTRATO DE COMPRA  Y VENTA QUE OTORGA DE UNA PARTE, COMO VENDEDOR (CONTRAYENTE DE DEUDA) SEÑOR(A): MOLINA QUISPE VERONICA TRINIDAD, IDENTIFICADO (A) CON DNI: 19944260,  DOMICILIADO EN ,  DISTRITO DE , PROVINCIA DE , DEPARTAMENTO DE , Y DE  OTRA PARTE EN CALIDAD DE COMPRADOR EL SEÑOR ALAIN RAVICHAGUA SOTELO, IDENTIFICADO CON DNI: 44892319 DOMICILIADO EN PROG. CUZCO N° 2309, DISTRITO DE HUANCAYO, PROVINCIA DE HUANCAYO, DEPARTAMENTO DE JUNÍN. BAJO LOS TÉRMINOS Y CONDICIONES SIGUIENTES:	\N	\N	12:30:00-05	1         	\N	3862	\N
75	JUANITA RUTH HERRERA SOTO	5	0	2020-07-07 02:09:44	PRINCIPAL	CAPITAL DE TRABAJO	80245301	0	NINGUNO		0	0	NINGUNO		0	60	4	15.1999999999999993	0	1	0	2020-07-08	380.100000000000023	26	0	2020-08-07		\N	0	\N	\N	\N	\N	\N	\N	14.6199999999999992	0.57999999999999996	0	15.1999999999999993	380.100000000000023	15.1999999999999993	0	0	26	395.199999999999989	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	62	\N	\N	HUANCAYO                                          	\N	\N	\N	CONSTE POR EL PRESENTE CONTRATO DE COMPRA  Y VENTA QUE OTORGA DE UNA PARTE, COMO VENDEDOR (CONTRAYENTE DE DEUDA) SEÑOR(A): ROMERO ESPIRITU VIRGINIA LUZ, IDENTIFICADO (A) CON DNI: 80245301,  DOMICILIADO EN ,  DISTRITO DE , PROVINCIA DE , DEPARTAMENTO DE , Y DE  OTRA PARTE EN CALIDAD DE COMPRADOR EL SEÑOR ALAIN RAVICHAGUA SOTELO, IDENTIFICADO CON DNI: 44892319 DOMICILIADO EN PROG. CUZCO N° 2309, DISTRITO DE HUANCAYO, PROVINCIA DE HUANCAYO, DEPARTAMENTO DE JUNÍN. BAJO LOS TÉRMINOS Y CONDICIONES SIGUIENTES:	\N	\N	12:30:00-05	1         	\N	\N	\N
76	JUANITA RUTH HERRERA SOTO	5	0	2020-07-07 02:32:18	PRINCIPAL	CAPITAL DE TRABAJO	44350264	0	NINGUNO		0	0	NINGUNO		0	61	6	2.62199999999999989	1	1	0	2020-07-14	43.7000000000000028	4	0	2020-08-04		\N	0	\N	\N	\N	\N	\N	\N	10.9250000000000007	0.655499999999999972	0	11.5999999999999996	43.7000000000000028	2.62199999999999989	0	0	4	46.3999999999999986	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	63	\N	\N	HUANCAYO                                          	\N	\N	\N	CONSTE POR EL PRESENTE CONTRATO DE COMPRA  Y VENTA QUE OTORGA DE UNA PARTE, COMO VENDEDOR (CONTRAYENTE DE DEUDA) SEÑOR(A): CHAVEZ CHIRINOS JANNETH CECILIA, IDENTIFICADO (A) CON DNI: 44350264,  DOMICILIADO EN ,  DISTRITO DE , PROVINCIA DE , DEPARTAMENTO DE , Y DE  OTRA PARTE EN CALIDAD DE COMPRADOR EL SEÑOR ALAIN RAVICHAGUA SOTELO, IDENTIFICADO CON DNI: 44892319 DOMICILIADO EN PROG. CUZCO N° 2309, DISTRITO DE HUANCAYO, PROVINCIA DE HUANCAYO, DEPARTAMENTO DE JUNÍN. BAJO LOS TÉRMINOS Y CONDICIONES SIGUIENTES:	\N	\N	12:30:00-05	1         	\N	\N	\N
73	JUANITA RUTH HERRERA SOTO	5	2	2020-07-06 05:05:00	PRINCIPAL	CAPITAL DE TRABAJO	70020689	0	NINGUNO		0	0	NINGUNO		0	48	3	7.58999999999999986	0	1	0	2020-07-07	365.600000000000023	18	1.26000000000000001	2020-07-27		\N	0	\N	\N	\N	\N	\N	\N	20.3099999999999987	0.419999999999999984	0.0700000000000000067	20.8000000000000007	365.600000000000023	7.58999999999999986	1.26000000000000001	0	18	374.399999999999977	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	61	\N	\N	HUANCAYO                                          	\N	\N	\N	CONSTE POR EL PRESENTE CONTRATO DE COMPRA  Y VENTA QUE OTORGA DE UNA PARTE, COMO VENDEDOR (CONTRAYENTE DE DEUDA) SEÑOR(A): MAYTA ORDOÑEZ WENDY MERCEDES, IDENTIFICADO (A) CON DNI: 70020689,  DOMICILIADO EN ,  DISTRITO DE , PROVINCIA DE , DEPARTAMENTO DE , Y DE  OTRA PARTE EN CALIDAD DE COMPRADOR EL SEÑOR ALAIN RAVICHAGUA SOTELO, IDENTIFICADO CON DNI: 44892319 DOMICILIADO EN PROG. CUZCO N° 2309, DISTRITO DE HUANCAYO, PROVINCIA DE HUANCAYO, DEPARTAMENTO DE JUNÍN. BAJO LOS TÉRMINOS Y CONDICIONES SIGUIENTES:	\N	\N	12:30:00-05	1         	\N	\N	\N
83	KEWIN GILBER VELA PEREZ	11	2	2020-07-08 10:20:03	PRINCIPAL	CAPITAL DE TRABAJO	05213767	0	NINGUNO	-	0	0	NINGUNO		0	63	1	0.560000000000000053	0	0	0	2020-07-09	112.799999999999997	13	0	2020-07-23		\N	0	\N	\N	\N	\N	\N	\N	8.67999999999999972	0.0400000000000000008	0	8.69999999999999929	112.799999999999997	0.560000000000000053	0	0	13	113.099999999999994	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	65	\N	\N	PUCALLPA                                          	\N	\N	\N	CONSTE POR EL PRESENTE CONTRATO DE COMPRA  Y VENTA QUE OTORGA DE UNA PARTE, COMO VENDEDOR (CONTRAYENTE DE DEUDA) SEÑOR(A): MONROY CANAYO ROBINSON EMILIO, IDENTIFICADO (A) CON DNI: 05213767,  DOMICILIADO EN ,  DISTRITO DE , PROVINCIA DE , DEPARTAMENTO DE , Y DE  OTRA PARTE EN CALIDAD DE COMPRADOR EL SEÑOR ALAIN RAVICHAGUA SOTELO, IDENTIFICADO CON DNI: 44892319 DOMICILIADO EN PROG. CUZCO N° 2309, DISTRITO DE HUANCAYO, PROVINCIA DE HUANCAYO, DEPARTAMENTO DE JUNÍN. BAJO LOS TÉRMINOS Y CONDICIONES SIGUIENTES:	\N	\N	12:30:00-05	0         	\N	\N	\N
84	KEWIN GILBER VELA PEREZ	11	2	2020-07-08 10:26:23	PRINCIPAL	CAPITAL DE TRABAJO	05213767	0	NINGUNO	-	0	0	NINGUNO		0	63	1	0.560000000000000053	0	0	0	2020-07-09	112.799999999999997	13	0	2020-07-23		\N	1	87202084923     	f	\N	2020-07-08 22:28:38.951	9	1	8.67999999999999972	0.0400000000000000008	0	8.69999999999999929	0	0	0	0	0	0	0	0	0	0	0	0	0.5	0.5	2020-07-08 22:30:28.42	\N	\N	0	\N	0.100000000000000006	65	\N	\N	PUCALLPA                                          	\N	\N	\N	CONSTE POR EL PRESENTE CONTRATO DE COMPRA  Y VENTA QUE OTORGA DE UNA PARTE, COMO VENDEDOR (CONTRAYENTE DE DEUDA) SEÑOR(A): MONROY CANAYO ROBINSON EMILIO, IDENTIFICADO (A) CON DNI: 05213767,  DOMICILIADO EN ,  DISTRITO DE , PROVINCIA DE , DEPARTAMENTO DE , Y DE  OTRA PARTE EN CALIDAD DE COMPRADOR EL SEÑOR ALAIN RAVICHAGUA SOTELO, IDENTIFICADO CON DNI: 44892319 DOMICILIADO EN PROG. CUZCO N° 2309, DISTRITO DE HUANCAYO, PROVINCIA DE HUANCAYO, DEPARTAMENTO DE JUNÍN. BAJO LOS TÉRMINOS Y CONDICIONES SIGUIENTES:	\N	\N	11:30:00-05	0         	\N	3859	\N
81	JUANITA RUTH HERRERA SOTO	5	2	2020-07-07 03:45:59	PRINCIPAL	CAPITAL DE TRABAJO	77381267	0	NINGUNO		0	0	NINGUNO		0	65	8	9.83999999999999986	2	1	0	2020-07-22	123	2	0.160000000000000003	2020-08-06		\N	1	107202081219    	f	\N	2020-07-10 12:57:09.28	4	0	61.5	4.91999999999999993	0.0800000000000000017	66.5	123	9.83999999999999986	0.160000000000000003	0	2	133	0	0	0	0	0	0	0.5	0	2020-07-10 12:57:09.28	\N	\N	0	\N	0	67	\N	\N	HUANCAYO                                          	\N	\N	\N	CONSTE POR EL PRESENTE CONTRATO DE COMPRA  Y VENTA QUE OTORGA DE UNA PARTE, COMO VENDEDOR (CONTRAYENTE DE DEUDA) SEÑOR(A): CHUCO ESCOBAR ERICK JESUS , IDENTIFICADO (A) CON DNI: 77381267,  DOMICILIADO EN ,  DISTRITO DE , PROVINCIA DE , DEPARTAMENTO DE , Y DE  OTRA PARTE EN CALIDAD DE COMPRADOR EL SEÑOR ALAIN RAVICHAGUA SOTELO, IDENTIFICADO CON DNI: 44892319 DOMICILIADO EN PROG. CUZCO N° 2309, DISTRITO DE HUANCAYO, PROVINCIA DE HUANCAYO, DEPARTAMENTO DE JUNÍN. BAJO LOS TÉRMINOS Y CONDICIONES SIGUIENTES:	\N	\N	12:30:00-05	1         	\N	3862	\N
74	KEWIN GILBER VELA PEREZ	11	2	2020-07-07 09:53:27	PRINCIPAL	CAPITAL DE TRABAJO	00098119	0	NINGUNO	-	0	0	NINGUNO		0	28	1	1.17999999999999994	0	0	0	2020-07-08	255.400000000000006	12	0.239999999999999991	2020-07-21		\N	1	77202074418     	f	\N	2020-07-07 11:50:54.595	9	1	21.2800000000000011	0.100000000000000006	0.0200000000000000004	21.3999999999999986	0	0	0	0	0	0	0	0	0	0	0	0	0.5	0.5	2020-07-08 22:32:12.799	\N	\N	0	\N	0.200000000000000011	26	\N	\N	PUCALLPA                                          	\N	\N	\N	CONSTE POR EL PRESENTE CONTRATO DE COMPRA  Y VENTA QUE OTORGA DE UNA PARTE, COMO VENDEDOR (CONTRAYENTE DE DEUDA) SEÑOR(A): URQUIA LOPEZ ROSA LUCIA, IDENTIFICADO (A) CON DNI: 00098119,  DOMICILIADO EN ,  DISTRITO DE , PROVINCIA DE , DEPARTAMENTO DE , Y DE  OTRA PARTE EN CALIDAD DE COMPRADOR EL SEÑOR ALAIN RAVICHAGUA SOTELO, IDENTIFICADO CON DNI: 44892319 DOMICILIADO EN PROG. CUZCO N° 2309, DISTRITO DE HUANCAYO, PROVINCIA DE HUANCAYO, DEPARTAMENTO DE JUNÍN. BAJO LOS TÉRMINOS Y CONDICIONES SIGUIENTES:	\N	\N	03:45:00-05	0         	\N	3857	\N
80	JUANITA RUTH HERRERA SOTO	5	2	2020-07-07 03:34:37	PRINCIPAL	CAPITAL DE TRABAJO	77377731	0	GARANTIAS FLOTANTE	AHORRO PROGRAMADO 	180	0	NINGUNO		0	64	8	56.3200000000000003	2	2	0	2020-07-22	352	4	0.0800000000000000017	2020-09-05		\N	0	\N	\N	\N	\N	\N	\N	88	14.0800000000000001	0.0200000000000000004	102.099999999999994	352	56.3200000000000003	0.0800000000000000017	0	4	408.399999999999977	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	66	\N	\N	HUANCAYO                                          	\N	\N	\N	CONSTE POR EL PRESENTE CONTRATO DE COMPRA  Y VENTA QUE OTORGA DE UNA PARTE, COMO VENDEDOR (CONTRAYENTE DE DEUDA) SEÑOR(A): CHUCO ESCOBAR JOSSEP KERICK, IDENTIFICADO (A) CON DNI: 77377731,  DOMICILIADO EN ,  DISTRITO DE , PROVINCIA DE , DEPARTAMENTO DE , Y DE  OTRA PARTE EN CALIDAD DE COMPRADOR EL SEÑOR ALAIN RAVICHAGUA SOTELO, IDENTIFICADO CON DNI: 44892319 DOMICILIADO EN PROG. CUZCO N° 2309, DISTRITO DE HUANCAYO, PROVINCIA DE HUANCAYO, DEPARTAMENTO DE JUNÍN. BAJO LOS TÉRMINOS Y CONDICIONES SIGUIENTES:	\N	\N	12:30:00-05	1         	\N	\N	\N
82	JUANITA RUTH HERRERA SOTO	5	2	2020-07-07 04:39:29	PRINCIPAL	CAPITAL DE TRABAJO	77377731	0	NINGUNO		0	0	NINGUNO		0	64	8	28.1600000000000001	2	1	0	2020-07-22	352	2	0.0400000000000000008	2020-08-06		\N	0	\N	\N	\N	\N	\N	\N	176	14.0800000000000001	0.0200000000000000004	190.099999999999994	352	28.1600000000000001	0.0400000000000000008	0	2	380.199999999999989	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	66	\N	\N	HUANCAYO                                          	\N	\N	\N	CONSTE POR EL PRESENTE CONTRATO DE COMPRA  Y VENTA QUE OTORGA DE UNA PARTE, COMO VENDEDOR (CONTRAYENTE DE DEUDA) SEÑOR(A): CHUCO ESCOBAR JOSSEP KERICK, IDENTIFICADO (A) CON DNI: 77377731,  DOMICILIADO EN ,  DISTRITO DE , PROVINCIA DE , DEPARTAMENTO DE , Y DE  OTRA PARTE EN CALIDAD DE COMPRADOR EL SEÑOR ALAIN RAVICHAGUA SOTELO, IDENTIFICADO CON DNI: 44892319 DOMICILIADO EN PROG. CUZCO N° 2309, DISTRITO DE HUANCAYO, PROVINCIA DE HUANCAYO, DEPARTAMENTO DE JUNÍN. BAJO LOS TÉRMINOS Y CONDICIONES SIGUIENTES:	\N	\N	12:30:00-05	1         	\N	\N	\N
88	JUANITA RUTH HERRERA SOTO	5	2	2020-07-09 10:12:14	PRINCIPAL	CAPITAL DE TRABAJO	77377731	0	GARANTIAS FLOTANTE	AHORRO PROGRANADO 	180	0	NINGUNO		0	64	8	28.1600000000000001	2	1	0	2020-07-24	352	2	0.0400000000000000008	2020-08-08		\N	1	107202088889    	f	\N	2020-07-10 12:53:47.503	4	0	176	14.0800000000000001	0.0200000000000000004	190.099999999999994	352	28.1600000000000001	0.0400000000000000008	0	2	380.199999999999989	0	0	0	0	0	0	0.5	0	2020-07-10 12:53:47.503	\N	\N	0	\N	0	66	\N	\N	HUANCAYO                                          	\N	\N	\N	CONSTE POR EL PRESENTE CONTRATO DE COMPRA  Y VENTA QUE OTORGA DE UNA PARTE, COMO VENDEDOR (CONTRAYENTE DE DEUDA) SEÑOR(A): CHUCO ESCOBAR JOSSEP KERICK, IDENTIFICADO (A) CON DNI: 77377731,  DOMICILIADO EN ,  DISTRITO DE , PROVINCIA DE , DEPARTAMENTO DE , Y DE  OTRA PARTE EN CALIDAD DE COMPRADOR EL SEÑOR ALAIN RAVICHAGUA SOTELO, IDENTIFICADO CON DNI: 44892319 DOMICILIADO EN PROG. CUZCO N° 2309, DISTRITO DE HUANCAYO, PROVINCIA DE HUANCAYO, DEPARTAMENTO DE JUNÍN. BAJO LOS TÉRMINOS Y CONDICIONES SIGUIENTES:	\N	\N	12:30:00-05	1         	\N	3862	\N
91	JUANITA RUTH HERRERA SOTO	5	2	2020-07-09 11:45:01	PRINCIPAL	CAPITAL DE TRABAJO	46657916	0	NINGUNO		0	0	NINGUNO		0	72	4	58.0399999999999991	1	2	0	2020-07-16	725.5	8	0.0800000000000000017	2020-09-03		\N	0	\N	\N	\N	\N	\N	\N	90.6875	7.25499999999999989	0.0100000000000000002	98	725.5	58.0399999999999991	0.0800000000000000017	0	8	784	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	75	\N	\N	HUANCAYO                                          	\N	\N	\N	CONSTE POR EL PRESENTE CONTRATO DE COMPRA  Y VENTA QUE OTORGA DE UNA PARTE, COMO VENDEDOR (CONTRAYENTE DE DEUDA) SEÑOR(A): SEDANO LIZANA LUZ ANGELA, IDENTIFICADO (A) CON DNI: 46657916,  DOMICILIADO EN ,  DISTRITO DE , PROVINCIA DE , DEPARTAMENTO DE , Y DE  OTRA PARTE EN CALIDAD DE COMPRADOR EL SEÑOR ALAIN RAVICHAGUA SOTELO, IDENTIFICADO CON DNI: 44892319 DOMICILIADO EN PROG. CUZCO N° 2309, DISTRITO DE HUANCAYO, PROVINCIA DE HUANCAYO, DEPARTAMENTO DE JUNÍN. BAJO LOS TÉRMINOS Y CONDICIONES SIGUIENTES:	\N	\N	12:30:00-05	0         	\N	\N	\N
90	JUANITA RUTH HERRERA SOTO	5	2	2020-07-09 11:20:23	PRINCIPAL	CAPITAL DE TRABAJO	44931763	0	NINGUNO		0	0	NINGUNO		0	71	8	829.27628433108805	3	12	0	2020-08-08	1400	12	0	2021-07-07		\N	1	107202090201    	f	\N	2020-07-10 12:34:16.71	4	0	1400	74.0400000000000063	0	185.773023694257006	1400	829.27628433108805	0	0	12	2229.27628433109021	0	0	0	0	0	0	0.5	0	2020-07-10 12:34:16.71	\N	\N	0	\N	0	74	\N	\N	HUANCAYO                                          	\N	\N	\N	CONSTE POR EL PRESENTE CONTRATO DE COMPRA  Y VENTA QUE OTORGA DE UNA PARTE, COMO VENDEDOR (CONTRAYENTE DE DEUDA) SEÑOR(A): PEREZ PADILLA LOIDA EUNICE, IDENTIFICADO (A) CON DNI: 44931763,  DOMICILIADO EN ,  DISTRITO DE , PROVINCIA DE , DEPARTAMENTO DE , Y DE  OTRA PARTE EN CALIDAD DE COMPRADOR EL SEÑOR ALAIN RAVICHAGUA SOTELO, IDENTIFICADO CON DNI: 44892319 DOMICILIADO EN PROG. CUZCO N° 2309, DISTRITO DE HUANCAYO, PROVINCIA DE HUANCAYO, DEPARTAMENTO DE JUNÍN. BAJO LOS TÉRMINOS Y CONDICIONES SIGUIENTES:	\N	\N	12:30:00-05	1         	\N	3862	\N
62	MELISA DINA CHUQUILLANQUI CUNO 	3	2	2020-07-04 10:55:57	PRINCIPAL	CAPITAL DE TRABAJO	42388030	0	NINGUNO		0	0	NINGUNO		0	51	2.5	33.9624999999999986	1	2	0	2020-07-11	543.399999999999977	10	0	2020-09-12	CLIENTE ACTIVO	\N	1	47202062315     	f	\N	2020-07-04 11:00:06.144	4	0	54.3400000000000034	3.39625000000000021	0	57.75	380.379999999999995	23.7719999999999985	0	0	7	404.151999999999987	0	0	0	0	0	0	0.5	0	2020-07-25 12:11:25.76	\N	\N	0	\N	0	50	\N	\N	HUANCAYO                                          	\N	\N	\N	CONSTE POR EL PRESENTE CONTRATO DE COMPRA  Y VENTA QUE OTORGA DE UNA PARTE, COMO VENDEDOR (CONTRAYENTE DE DEUDA) SEÑOR(A): BONIFACIO SINCHE EDSON GULLERMO, IDENTIFICADO (A) CON DNI: 42388030,  DOMICILIADO EN ,  DISTRITO DE , PROVINCIA DE , DEPARTAMENTO DE , Y DE  OTRA PARTE EN CALIDAD DE COMPRADOR EL SEÑOR ALAIN RAVICHAGUA SOTELO, IDENTIFICADO CON DNI: 44892319 DOMICILIADO EN PROG. CUZCO N° 2309, DISTRITO DE HUANCAYO, PROVINCIA DE HUANCAYO, DEPARTAMENTO DE JUNÍN. BAJO LOS TÉRMINOS Y CONDICIONES SIGUIENTES:	\N	\N	12:30:00-05	1         	\N	3854	\N
85	KEWIN GILBER VELA PEREZ	11	2	2020-07-08 11:27:29	PRINCIPAL	CAPITAL DE TRABAJO	00035433	0	NINGUNO	-	0	0	NINGUNO	-	0	67	12	60	0	1	0	2020-07-09	500	26	1.56000000000000005	2020-08-08	LA ESPERA DE LA APROBACION	\N	1	87202085383     	f	\N	2020-07-08 23:52:12.806	9	2	19.2300000000000004	2.31000000000000005	0.0599999999999999978	21.6000000000000014	0	0	0	0	0	0	0	0	0	0	0	0	0.5	0	2020-08-01 18:44:36.434	\N	\N	0	\N	0	70	\N	\N	PUCALLPA                                          	\N	\N	\N	CONSTE POR EL PRESENTE CONTRATO DE COMPRA  Y VENTA QUE OTORGA DE UNA PARTE, COMO VENDEDOR (CONTRAYENTE DE DEUDA) SEÑOR(A): PEREZ PEREZ REYNA MARINA, IDENTIFICADO (A) CON DNI: 00035433,  DOMICILIADO EN ,  DISTRITO DE , PROVINCIA DE , DEPARTAMENTO DE , Y DE  OTRA PARTE EN CALIDAD DE COMPRADOR EL SEÑOR ALAIN RAVICHAGUA SOTELO, IDENTIFICADO CON DNI: 44892319 DOMICILIADO EN PROG. CUZCO N° 2309, DISTRITO DE HUANCAYO, PROVINCIA DE HUANCAYO, DEPARTAMENTO DE JUNÍN. BAJO LOS TÉRMINOS Y CONDICIONES SIGUIENTES:	\N	\N	04:00:00-05	0         	\N	3859	\N
93	JUANITA RUTH HERRERA SOTO	5	2	2020-07-09 12:18:01	PRINCIPAL	CAPITAL DE TRABAJO	44019045	0	NINGUNO		0	0	NINGUNO		0	74	8	160	2	4	0	2020-07-24	500	8	0	2020-11-09		\N	1	107202093006    	f	\N	2020-07-10 12:36:14.471	4	0	62.5	20	0	82.5	437.5	140	0	0	7	577.5	1	82.5	0	0	0	0	0.5	0	2020-08-11 09:32:41.192	\N	\N	0	\N	0	77	\N	\N	HUANCAYO                                          	\N	\N	\N	CONSTE POR EL PRESENTE CONTRATO DE COMPRA  Y VENTA QUE OTORGA DE UNA PARTE, COMO VENDEDOR (CONTRAYENTE DE DEUDA) SEÑOR(A): CARDENAS LUIS WILDER, IDENTIFICADO (A) CON DNI: 44019045,  DOMICILIADO EN ,  DISTRITO DE , PROVINCIA DE , DEPARTAMENTO DE , Y DE  OTRA PARTE EN CALIDAD DE COMPRADOR EL SEÑOR ALAIN RAVICHAGUA SOTELO, IDENTIFICADO CON DNI: 44892319 DOMICILIADO EN PROG. CUZCO N° 2309, DISTRITO DE HUANCAYO, PROVINCIA DE HUANCAYO, DEPARTAMENTO DE JUNÍN. BAJO LOS TÉRMINOS Y CONDICIONES SIGUIENTES:	\N	\N	12:30:00-05	0         	\N	3862	\N
95	JUANITA RUTH HERRERA SOTO	5	2	2020-07-09 12:51:51	PRINCIPAL	CAPITAL DE TRABAJO	46706530	0	NINGUNO		0	0	NINGUNO		0	75	8	240	1	2	0	2020-07-16	1500	8	0	2020-09-03		\N	1	107202095953    	f	\N	2020-07-10 12:26:30.832	4	0	187.5	30	0	217.5	1500	240	0	0	8	1740	1	217.5	6	0	0	0	0.5	0	2020-07-10 12:26:30.832	\N	\N	0	\N	0	78	\N	\N	HUANCAYO                                          	\N	\N	\N	CONSTE POR EL PRESENTE CONTRATO DE COMPRA  Y VENTA QUE OTORGA DE UNA PARTE, COMO VENDEDOR (CONTRAYENTE DE DEUDA) SEÑOR(A): CERVANTES  VASQUEZ SINDY, IDENTIFICADO (A) CON DNI: 46706530,  DOMICILIADO EN ,  DISTRITO DE , PROVINCIA DE , DEPARTAMENTO DE , Y DE  OTRA PARTE EN CALIDAD DE COMPRADOR EL SEÑOR ALAIN RAVICHAGUA SOTELO, IDENTIFICADO CON DNI: 44892319 DOMICILIADO EN PROG. CUZCO N° 2309, DISTRITO DE HUANCAYO, PROVINCIA DE HUANCAYO, DEPARTAMENTO DE JUNÍN. BAJO LOS TÉRMINOS Y CONDICIONES SIGUIENTES:	\N	\N	12:30:00-05	1         	\N	3862	\N
70	JUANITA RUTH HERRERA SOTO	5	2	2020-07-06 03:55:03	PRINCIPAL	CAPITAL DE TRABAJO	44729185	0	NINGUNO		0	0	NINGUNO		0	57	1	50.1733781663424026	3	4	0	2020-08-05	1997	4	0	2020-11-04		\N	1	147202070442    	f	\N	2020-07-14 12:03:20.964	4	0	1997	12.5299999999999994	0	511.793344541586009	1997	50.1733781663424026	0	0	4	2047.17337816633994	0	0	0	0	0	0	0.5	0	2020-07-14 12:03:20.964	\N	\N	0	\N	0	57	\N	\N	HUANCAYO                                          	\N	\N	\N	CONSTE POR EL PRESENTE CONTRATO DE COMPRA  Y VENTA QUE OTORGA DE UNA PARTE, COMO VENDEDOR (CONTRAYENTE DE DEUDA) SEÑOR(A): MULLISACA ENCISO ELIZABETH PAMELA, IDENTIFICADO (A) CON DNI: 44729185,  DOMICILIADO EN ,  DISTRITO DE , PROVINCIA DE , DEPARTAMENTO DE , Y DE  OTRA PARTE EN CALIDAD DE COMPRADOR EL SEÑOR ALAIN RAVICHAGUA SOTELO, IDENTIFICADO CON DNI: 44892319 DOMICILIADO EN PROG. CUZCO N° 2309, DISTRITO DE HUANCAYO, PROVINCIA DE HUANCAYO, DEPARTAMENTO DE JUNÍN. BAJO LOS TÉRMINOS Y CONDICIONES SIGUIENTES:	\N	\N	12:30:00-05	1         	\N	3868	\N
132	KEWIN GILBER VELA PEREZ	11	2	2020-08-07 05:36:40	PARALELO	CAPITAL DE TRABAJO	00093054	0	NINGUNO	0\r\n	0	0	NINGUNO		0	66	12	96	0	1	0	2020-08-08	800	26	1.04000000000000004	2020-09-07		\N	1	882020132531    	f	\N	2020-08-08 12:09:38.182	9	0	30.7699999999999996	3.68999999999999995	0.0400000000000000008	34.5	430.779999999999973	51.6599999999999966	0.560000000000000053	0	14	483	1	34.5	0	0	0	0	0.5	0	2020-08-23 10:40:09.283	\N	\N	0	\N	0	68	\N	\N	PUCALLPA                                          	\N	\N	\N	CONSTE POR EL PRESENTE CONTRATO DE COMPRA  Y VENTA QUE OTORGA DE UNA PARTE, COMO VENDEDOR (CONTRAYENTE DE DEUDA) SEÑOR(A): CORAL REATEGUI FELISINDA, IDENTIFICADO (A) CON DNI: 00093054,  DOMICILIADO EN ,  DISTRITO DE , PROVINCIA DE , DEPARTAMENTO DE , Y DE  OTRA PARTE EN CALIDAD DE COMPRADOR EL SEÑOR ALAIN RAVICHAGUA SOTELO, IDENTIFICADO CON DNI: 44892319 DOMICILIADO EN PROG. CUZCO N° 2309, DISTRITO DE HUANCAYO, PROVINCIA DE HUANCAYO, DEPARTAMENTO DE JUNÍN. BAJO LOS TÉRMINOS Y CONDICIONES SIGUIENTES:	\N	\N	03:00:00-05	0         	\N	3896	\N
115	JUANITA RUTH HERRERA SOTO	5	2	2020-07-14 10:44:45	PRINCIPAL	CAPITAL DE TRABAJO	80245301	0	NINGUNO		0	0	NINGUNO		0	60	1	3.35999999999999988	0	1	0	2020-07-15	290.899999999999977	30	0	2020-08-19		\N	1	1672020115274   	f	\N	2020-07-16 14:20:15.967	4	0	9.69999999999999929	0.110000000000000001	0	9.80000000000000071	213.400000000000006	2.41999999999999993	0	0	22	215.819999999999993	1	9.80000000000000071	0	0	0	0	0.5	0	2020-07-25 12:07:44.612	\N	\N	0	\N	0	62	\N	\N	HUANCAYO                                          	\N	\N	\N	CONSTE POR EL PRESENTE CONTRATO DE COMPRA  Y VENTA QUE OTORGA DE UNA PARTE, COMO VENDEDOR (CONTRAYENTE DE DEUDA) SEÑOR(A): ROMERO ESPIRITU VIRGINIA LUZ, IDENTIFICADO (A) CON DNI: 80245301,  DOMICILIADO EN ,  DISTRITO DE , PROVINCIA DE , DEPARTAMENTO DE , Y DE  OTRA PARTE EN CALIDAD DE COMPRADOR EL SEÑOR ALAIN RAVICHAGUA SOTELO, IDENTIFICADO CON DNI: 44892319 DOMICILIADO EN PROG. CUZCO N° 2309, DISTRITO DE HUANCAYO, PROVINCIA DE HUANCAYO, DEPARTAMENTO DE JUNÍN. BAJO LOS TÉRMINOS Y CONDICIONES SIGUIENTES:	\N	\N	12:30:00-05	1         	\N	3872	\N
94	JUANITA RUTH HERRERA SOTO	5	2	2020-07-09 12:23:35	PRINCIPAL	CAPITAL DE TRABAJO	46657916	0	NINGUNO		0	0	NINGUNO		0	72	8	168	2	4	0	2020-07-24	525	8	0.640000000000000013	2020-11-09		\N	1	107202094286    	f	\N	2020-07-10 12:40:54.31	4	0	65.6200000000000045	21	0.0800000000000000017	86.7000000000000028	459.339999999999975	147	0.560000000000000053	0	7	606.899999999999977	1	86.7000000000000028	0	0	0	0	0.5	0	2020-08-11 09:35:43.299	\N	\N	0	\N	0	75	\N	\N	HUANCAYO                                          	\N	\N	\N	CONSTE POR EL PRESENTE CONTRATO DE COMPRA  Y VENTA QUE OTORGA DE UNA PARTE, COMO VENDEDOR (CONTRAYENTE DE DEUDA) SEÑOR(A): SEDANO LIZANA LUZ ANGELA, IDENTIFICADO (A) CON DNI: 46657916,  DOMICILIADO EN ,  DISTRITO DE , PROVINCIA DE , DEPARTAMENTO DE , Y DE  OTRA PARTE EN CALIDAD DE COMPRADOR EL SEÑOR ALAIN RAVICHAGUA SOTELO, IDENTIFICADO CON DNI: 44892319 DOMICILIADO EN PROG. CUZCO N° 2309, DISTRITO DE HUANCAYO, PROVINCIA DE HUANCAYO, DEPARTAMENTO DE JUNÍN. BAJO LOS TÉRMINOS Y CONDICIONES SIGUIENTES:	\N	\N	12:30:00-05	1         	\N	3862	\N
128	KEWIN GILBER VELA PEREZ	11	2	2020-07-30 04:05:14	REFINANCIADO	CAPITAL DE TRABAJO	00098119	0	NINGUNO	-	0	0	NINGUNO		0	28	12	120	0	1	0	2020-07-31	1000	26	0.520000000000000018	2020-08-29		\N	1	3172020128187   	f	\N	2020-07-31 14:23:47.997	9	0	38.4600000000000009	4.62000000000000011	0.0200000000000000004	43.1000000000000014	346.139999999999986	41.5799999999999983	0.179999999999999993	0	9	387.899999999999977	3	129.300000000000011	0	0	0	0	0.5	0	2020-08-23 10:41:08.691	\N	\N	0	\N	0	0	\N	\N	PUCALLPA                                          	\N	\N	\N	CONSTE POR EL PRESENTE CONTRATO DE COMPRA  Y VENTA QUE OTORGA DE UNA PARTE, COMO VENDEDOR (CONTRAYENTE DE DEUDA) SEÑOR(A): URQUIA LOPEZ ROSA LUCIA, IDENTIFICADO (A) CON DNI: 00098119,  DOMICILIADO EN ,  DISTRITO DE , PROVINCIA DE , DEPARTAMENTO DE , Y DE  OTRA PARTE EN CALIDAD DE COMPRADOR EL SEÑOR ALAIN RAVICHAGUA SOTELO, IDENTIFICADO CON DNI: 44892319 DOMICILIADO EN PROG. CUZCO N° 2309, DISTRITO DE HUANCAYO, PROVINCIA DE HUANCAYO, DEPARTAMENTO DE JUNÍN. BAJO LOS TÉRMINOS Y CONDICIONES SIGUIENTES:	\N	\N	15:30:00-05	0         	\N	3889	\N
87	JUANITA RUTH HERRERA SOTO	5	6	2020-07-09 10:00:24	PRINCIPAL	CAPITAL DE TRABAJO	42611489	0	NINGUNO		0	0	NINGUNO		0	68	8	24	4	0	0	2020-07-10	300	1	0	2020-07-10		\N	1	97202087038     	f	\N	2020-07-09 13:04:54.87	4	2	300	24	0	324	0	0	0	0	0	0	0	0	0	0	0	0	0.5	0	2020-07-09 13:07:23.511	\N	\N	0	\N	0	71	\N	\N	HUANCAYO                                          	\N	\N	\N	CONSTE POR EL PRESENTE CONTRATO DE COMPRA  Y VENTA QUE OTORGA DE UNA PARTE, COMO VENDEDOR (CONTRAYENTE DE DEUDA) SEÑOR(A): SANTIAGO QUINTANILLA MARICRUZ DIONISIA , IDENTIFICADO (A) CON DNI: 42611489,  DOMICILIADO EN ,  DISTRITO DE , PROVINCIA DE , DEPARTAMENTO DE , Y DE  OTRA PARTE EN CALIDAD DE COMPRADOR EL SEÑOR ALAIN RAVICHAGUA SOTELO, IDENTIFICADO CON DNI: 44892319 DOMICILIADO EN PROG. CUZCO N° 2309, DISTRITO DE HUANCAYO, PROVINCIA DE HUANCAYO, DEPARTAMENTO DE JUNÍN. BAJO LOS TÉRMINOS Y CONDICIONES SIGUIENTES:	\N	\N	12:30:00-05	1         	\N	3860	\N
117	JUANITA RUTH HERRERA SOTO	5	2	2020-07-14 12:44:06	PRINCIPAL	CAPITAL DE TRABAJO	48142577	0	NINGUNO		0	0	NINGUNO		0	89	8	282.318572695908017	3	3	0	2020-08-13	1720.40000000000009	3	0	2020-10-12		\N	1	1672020117194   	f	\N	2020-07-16 14:11:59.835	4	0	1720.40000000000009	0	0	667.572857565303025	1720.40000000000009	282.318572695908017	0	0	3	2002.7185726959101	0	0	0	0	0	0	0.5	0	2020-07-16 14:11:59.835	\N	\N	0	\N	0	94	\N	\N	HUANCAYO                                          	\N	\N	\N	CONSTE POR EL PRESENTE CONTRATO DE COMPRA  Y VENTA QUE OTORGA DE UNA PARTE, COMO VENDEDOR (CONTRAYENTE DE DEUDA) SEÑOR(A): MARTEL LIVIAS MILAGROS ISABEL, IDENTIFICADO (A) CON DNI: 48142577,  DOMICILIADO EN ,  DISTRITO DE , PROVINCIA DE , DEPARTAMENTO DE , Y DE  OTRA PARTE EN CALIDAD DE COMPRADOR EL SEÑOR ALAIN RAVICHAGUA SOTELO, IDENTIFICADO CON DNI: 44892319 DOMICILIADO EN PROG. CUZCO N° 2309, DISTRITO DE HUANCAYO, PROVINCIA DE HUANCAYO, DEPARTAMENTO DE JUNÍN. BAJO LOS TÉRMINOS Y CONDICIONES SIGUIENTES:	\N	\N	12:30:00-05	1         	\N	3872	\N
97	JUANITA RUTH HERRERA SOTO	5	2	2020-07-09 01:48:58	PRINCIPAL	CAPITAL DE TRABAJO	80401963	0	NINGUNO		0	0	NINGUNO		0	10	1	18	1	3	0	2020-07-16	600	12	0	2020-10-01		\N	1	107202097353    	f	\N	2020-07-10 12:17:52.432	4	0	50	1.5	0	51.5	600	18	0	0	12	618	1	51.5	6	0	0	0	0.5	0	2020-07-10 12:17:52.432	\N	\N	0	\N	0	80	\N	\N	HUANCAYO                                          	\N	\N	\N	CONSTE POR EL PRESENTE CONTRATO DE COMPRA  Y VENTA QUE OTORGA DE UNA PARTE, COMO VENDEDOR (CONTRAYENTE DE DEUDA) SEÑOR(A): GONZALES MENDOZA NILO DAVID, IDENTIFICADO (A) CON DNI: 80401963,  DOMICILIADO EN ,  DISTRITO DE , PROVINCIA DE , DEPARTAMENTO DE , Y DE  OTRA PARTE EN CALIDAD DE COMPRADOR EL SEÑOR ALAIN RAVICHAGUA SOTELO, IDENTIFICADO CON DNI: 44892319 DOMICILIADO EN PROG. CUZCO N° 2309, DISTRITO DE HUANCAYO, PROVINCIA DE HUANCAYO, DEPARTAMENTO DE JUNÍN. BAJO LOS TÉRMINOS Y CONDICIONES SIGUIENTES:	\N	\N	12:30:00-05	1         	\N	3862	\N
98	JUANITA RUTH HERRERA SOTO	5	2	2020-07-10 09:13:43	PRINCIPAL	CAPITAL DE TRABAJO	42781214	0	NINGUNO		0	0	NINGUNO		0	77	8	199.512	2	3	0	2020-07-25	831.299999999999955	6	0	2020-10-09		\N	0	\N	\N	\N	\N	\N	\N	138.550000000000011	33.25	0	171.800000000000011	831.299999999999955	199.512	0	0	6	1030.79999999999995	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	81	\N	\N	HUANCAYO                                          	\N	\N	\N	CONSTE POR EL PRESENTE CONTRATO DE COMPRA  Y VENTA QUE OTORGA DE UNA PARTE, COMO VENDEDOR (CONTRAYENTE DE DEUDA) SEÑOR(A): AVILA ROJAS KARINA, IDENTIFICADO (A) CON DNI: 42781214,  DOMICILIADO EN ,  DISTRITO DE , PROVINCIA DE , DEPARTAMENTO DE , Y DE  OTRA PARTE EN CALIDAD DE COMPRADOR EL SEÑOR ALAIN RAVICHAGUA SOTELO, IDENTIFICADO CON DNI: 44892319 DOMICILIADO EN PROG. CUZCO N° 2309, DISTRITO DE HUANCAYO, PROVINCIA DE HUANCAYO, DEPARTAMENTO DE JUNÍN. BAJO LOS TÉRMINOS Y CONDICIONES SIGUIENTES:	\N	\N	12:30:00-05	1         	\N	\N	\N
99	JUANITA RUTH HERRERA SOTO	5	2	2020-07-10 09:25:05	PRINCIPAL	CAPITAL DE TRABAJO	42781214	0	NINGUNO		0	0	NINGUNO		0	77	8	199.512	2	3	0	2020-07-25	831.299999999999955	6	0	2020-10-09		\N	1	167202099372    	f	\N	2020-07-16 14:29:34.472	4	0	138.550000000000011	33.25	0	171.800000000000011	831.299999999999955	199.512	0	0	6	1030.79999999999995	0	0	0	0	0	0	0.5	0	2020-07-16 14:29:34.472	\N	\N	0	\N	0	81	\N	\N	HUANCAYO                                          	\N	\N	\N	CONSTE POR EL PRESENTE CONTRATO DE COMPRA  Y VENTA QUE OTORGA DE UNA PARTE, COMO VENDEDOR (CONTRAYENTE DE DEUDA) SEÑOR(A): AVILA ROJAS KARINA, IDENTIFICADO (A) CON DNI: 42781214,  DOMICILIADO EN ,  DISTRITO DE , PROVINCIA DE , DEPARTAMENTO DE , Y DE  OTRA PARTE EN CALIDAD DE COMPRADOR EL SEÑOR ALAIN RAVICHAGUA SOTELO, IDENTIFICADO CON DNI: 44892319 DOMICILIADO EN PROG. CUZCO N° 2309, DISTRITO DE HUANCAYO, PROVINCIA DE HUANCAYO, DEPARTAMENTO DE JUNÍN. BAJO LOS TÉRMINOS Y CONDICIONES SIGUIENTES:	\N	\N	12:30:00-05	1         	\N	3872	\N
129	KEWIN GILBER VELA PEREZ	11	2	2020-07-30 04:19:21	PRINCIPAL	CAPITAL DE TRABAJO	70203921	0	NINGUNO	0	0	0	NINGUNO		0	95	10	17.3099999999999987	0	1	0	2020-07-31	300	15	0.75	2020-08-17	AL ESPERA DE LA APROBACION	\N	1	3172020129975   	f	\N	2020-07-31 14:25:36.98	9	2	20	1.14999999999999991	0.0500000000000000028	21.1999999999999993	0	0	0	0	0	0	0	0	0	0	0	0	0.5	0	2020-08-23 10:38:01.822	\N	\N	0	\N	0	99	\N	\N	PUCALLPA                                          	\N	\N	\N	CONSTE POR EL PRESENTE CONTRATO DE COMPRA  Y VENTA QUE OTORGA DE UNA PARTE, COMO VENDEDOR (CONTRAYENTE DE DEUDA) SEÑOR(A): CRUZ PINEDO FATIMA SATOMY, IDENTIFICADO (A) CON DNI: 70203921,  DOMICILIADO EN ,  DISTRITO DE , PROVINCIA DE , DEPARTAMENTO DE , Y DE  OTRA PARTE EN CALIDAD DE COMPRADOR EL SEÑOR ALAIN RAVICHAGUA SOTELO, IDENTIFICADO CON DNI: 44892319 DOMICILIADO EN PROG. CUZCO N° 2309, DISTRITO DE HUANCAYO, PROVINCIA DE HUANCAYO, DEPARTAMENTO DE JUNÍN. BAJO LOS TÉRMINOS Y CONDICIONES SIGUIENTES:	\N	\N	03:00:00-05	0         	\N	3889	\N
68	JUANITA RUTH HERRERA SOTO	5	2	2020-07-06 01:43:01	PRINCIPAL	CAPITAL DE TRABAJO	45879388	0	GARANTIAS FLOTANTE	AHORRO PROGRAMADO	376.799988	0	NINGUNO		0	55	8	164.100542138983997	3	3	0	2020-08-05	1000	3	0	2020-10-05		\N	1	107202068101    	f	\N	2020-07-10 12:43:01.104	4	0	1000	5.46999999999999975	0	388.03351404632798	1000	164.100542138983997	0	0	3	1164.10054213898002	0	0	0	0	0	0	0.5	0	2020-07-10 12:43:01.104	\N	\N	0	\N	0	55	\N	\N	HUANCAYO                                          	\N	\N	\N	CONSTE POR EL PRESENTE CONTRATO DE COMPRA  Y VENTA QUE OTORGA DE UNA PARTE, COMO VENDEDOR (CONTRAYENTE DE DEUDA) SEÑOR(A): PEREZ CORONEL DELCY DINA, IDENTIFICADO (A) CON DNI: 45879388,  DOMICILIADO EN ,  DISTRITO DE , PROVINCIA DE , DEPARTAMENTO DE , Y DE  OTRA PARTE EN CALIDAD DE COMPRADOR EL SEÑOR ALAIN RAVICHAGUA SOTELO, IDENTIFICADO CON DNI: 44892319 DOMICILIADO EN PROG. CUZCO N° 2309, DISTRITO DE HUANCAYO, PROVINCIA DE HUANCAYO, DEPARTAMENTO DE JUNÍN. BAJO LOS TÉRMINOS Y CONDICIONES SIGUIENTES:	\N	\N	12:30:00-05	1         	\N	3862	\N
47	JUANITA RUTH HERRERA SOTO	5	2	2020-07-03 03:24:10	PRINCIPAL	CAPITAL DE TRABAJO	41278746	0	GARANTIAS FLOTANTE	AHORRO PROGRAMADO 	445.299988	0	NINGUNO		0	41	1	4.40299999999999958	4	0	0	2020-07-04	440.300000000000011	1	0	2020-07-04		\N	1	107202047600    	f	\N	2020-07-10 13:06:51.561	4	0	440	4	0	444	440.300000000000011	4.40299999999999958	0	0	1	444	1	444	0	0	0	0	12	0	2020-07-10 13:06:51.561	\N	\N	0	\N	0	40	\N	\N	HUANCAYO                                          	\N	\N	\N	CONSTE POR EL PRESENTE CONTRATO DE COMPRA  Y VENTA QUE OTORGA DE UNA PARTE, COMO VENDEDOR (CONTRAYENTE DE DEUDA) SEÑOR(A): QUISPE ROJAS EDITH YENI , IDENTIFICADO (A) CON DNI: 41278746,  DOMICILIADO EN ,  DISTRITO DE , PROVINCIA DE , DEPARTAMENTO DE , Y DE  OTRA PARTE EN CALIDAD DE COMPRADOR EL SEÑOR ALAIN RAVICHAGUA SOTELO, IDENTIFICADO CON DNI: 44892319 DOMICILIADO EN PROG. CUZCO N° 2309, DISTRITO DE HUANCAYO, PROVINCIA DE HUANCAYO, DEPARTAMENTO DE JUNÍN. BAJO LOS TÉRMINOS Y CONDICIONES SIGUIENTES:	\N	\N	12:30:00-05	1         	\N	3862	\N
89	JUANITA RUTH HERRERA SOTO	5	2	2020-07-09 10:53:54	PRINCIPAL	CAPITAL DE TRABAJO	47323767	0	NINGUNO		0	0	NINGUNO		0	70	8	250	1	2	0	2020-07-16	1250	10	0	2020-09-17		\N	1	107202089415    	f	\N	2020-07-10 12:55:26.344	4	0	125	25	0	150	1250	250	0	0	10	1500	1	150	6	0	0	0	0.5	0	2020-07-10 12:55:26.344	\N	\N	0	\N	0	73	\N	\N	HUANCAYO                                          	\N	\N	\N	CONSTE POR EL PRESENTE CONTRATO DE COMPRA  Y VENTA QUE OTORGA DE UNA PARTE, COMO VENDEDOR (CONTRAYENTE DE DEUDA) SEÑOR(A): HUAMAN  GARCIA LIZ GUISELA, IDENTIFICADO (A) CON DNI: 47323767,  DOMICILIADO EN ,  DISTRITO DE , PROVINCIA DE , DEPARTAMENTO DE , Y DE  OTRA PARTE EN CALIDAD DE COMPRADOR EL SEÑOR ALAIN RAVICHAGUA SOTELO, IDENTIFICADO CON DNI: 44892319 DOMICILIADO EN PROG. CUZCO N° 2309, DISTRITO DE HUANCAYO, PROVINCIA DE HUANCAYO, DEPARTAMENTO DE JUNÍN. BAJO LOS TÉRMINOS Y CONDICIONES SIGUIENTES:	\N	\N	12:30:00-05	1         	\N	3862	\N
66	JUANITA RUTH HERRERA SOTO	5	2	2020-07-06 12:55:13	PRINCIPAL	CAPITAL DE TRABAJO	19996893	0	GARANTIAS FLOTANTE	AHORRO PROGRAMADO 	1226.20996	0	NINGUNO		0	54	0	0	1	1	0	2020-07-13	560	4	0	2020-08-03		\N	1	107202066557    	f	\N	2020-07-10 13:11:27.138	4	0	140	0	0	140	560	0	0	0	4	560	1	140	6	0	0	0	0.5	0	2020-07-10 13:11:27.138	\N	\N	0	\N	0	54	\N	\N	HUANCAYO                                          	\N	\N	\N	CONSTE POR EL PRESENTE CONTRATO DE COMPRA  Y VENTA QUE OTORGA DE UNA PARTE, COMO VENDEDOR (CONTRAYENTE DE DEUDA) SEÑOR(A): MARQUINA JULCA ANA LUISA, IDENTIFICADO (A) CON DNI: 19996893,  DOMICILIADO EN ,  DISTRITO DE , PROVINCIA DE , DEPARTAMENTO DE , Y DE  OTRA PARTE EN CALIDAD DE COMPRADOR EL SEÑOR ALAIN RAVICHAGUA SOTELO, IDENTIFICADO CON DNI: 44892319 DOMICILIADO EN PROG. CUZCO N° 2309, DISTRITO DE HUANCAYO, PROVINCIA DE HUANCAYO, DEPARTAMENTO DE JUNÍN. BAJO LOS TÉRMINOS Y CONDICIONES SIGUIENTES:	\N	\N	12:30:00-05	1         	\N	3862	\N
100	JUANITA RUTH HERRERA SOTO	5	2	2020-07-10 01:25:06	PRINCIPAL	CAPITAL DE TRABAJO	43568148	0	NINGUNO		0	0	NINGUNO		0	69	1	9.1899999999999995	0	1	0	2020-07-11	918.899999999999977	26	0	2020-08-11		\N	0	\N	\N	\N	\N	\N	\N	35.3400000000000034	0.349999999999999978	0	35.7000000000000028	918.899999999999977	9.1899999999999995	0	0	26	928.200000000000045	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	72	\N	\N	HUANCAYO                                          	\N	\N	\N	CONSTE POR EL PRESENTE CONTRATO DE COMPRA  Y VENTA QUE OTORGA DE UNA PARTE, COMO VENDEDOR (CONTRAYENTE DE DEUDA) SEÑOR(A): YUPANQUI COTERA ERIKA, IDENTIFICADO (A) CON DNI: 43568148,  DOMICILIADO EN ,  DISTRITO DE , PROVINCIA DE , DEPARTAMENTO DE , Y DE  OTRA PARTE EN CALIDAD DE COMPRADOR EL SEÑOR ALAIN RAVICHAGUA SOTELO, IDENTIFICADO CON DNI: 44892319 DOMICILIADO EN PROG. CUZCO N° 2309, DISTRITO DE HUANCAYO, PROVINCIA DE HUANCAYO, DEPARTAMENTO DE JUNÍN. BAJO LOS TÉRMINOS Y CONDICIONES SIGUIENTES:	\N	\N	12:30:00-05	1         	\N	\N	\N
92	JUANITA RUTH HERRERA SOTO	5	2	2020-07-09 12:09:07	PRINCIPAL	CAPITAL DE TRABAJO	48117988	0	NINGUNO		0	0	NINGUNO		0	73	8	149.216000000000008	2	4	0	2020-07-24	466.300000000000011	8	0.479999999999999982	2020-11-09		\N	1	107202092072    	f	\N	2020-07-10 12:38:40.31	4	0	58.2899999999999991	18.6499999999999986	0.0599999999999999978	77	408.029999999999973	130.550000000000011	0.419999999999999984	0	7	539	1	77	0	0	0	0	0.5	0	2020-08-11 09:34:19.83	\N	\N	0	\N	0	76	\N	\N	HUANCAYO                                          	\N	\N	\N	CONSTE POR EL PRESENTE CONTRATO DE COMPRA  Y VENTA QUE OTORGA DE UNA PARTE, COMO VENDEDOR (CONTRAYENTE DE DEUDA) SEÑOR(A): SEDANO LIZANA SHEYLA, IDENTIFICADO (A) CON DNI: 48117988,  DOMICILIADO EN ,  DISTRITO DE , PROVINCIA DE , DEPARTAMENTO DE , Y DE  OTRA PARTE EN CALIDAD DE COMPRADOR EL SEÑOR ALAIN RAVICHAGUA SOTELO, IDENTIFICADO CON DNI: 44892319 DOMICILIADO EN PROG. CUZCO N° 2309, DISTRITO DE HUANCAYO, PROVINCIA DE HUANCAYO, DEPARTAMENTO DE JUNÍN. BAJO LOS TÉRMINOS Y CONDICIONES SIGUIENTES:	\N	\N	12:30:00-05	1         	\N	3862	\N
102	KEWIN GILBER VELA PEREZ	11	2	2020-07-10 10:05:36	PRINCIPAL	CAPITAL DE TRABAJO	00098119	0	NINGUNO	-	0	0	NINGUNO		0	28	12	84	0	1	0	2020-07-11	700	26	1.30000000000000004	2020-08-11		\N	1	1072020102150   	f	\N	2020-07-10 22:24:19.719	9	2	26.9200000000000017	3.22999999999999998	0.0500000000000000028	30.1999999999999993	0	0	0	0	0	0	0	0	0	0	0	0	0.5	0	2020-07-29 16:19:14.433	\N	\N	0	\N	0	26	\N	\N	PUCALLPA                                          	\N	\N	\N	CONSTE POR EL PRESENTE CONTRATO DE COMPRA  Y VENTA QUE OTORGA DE UNA PARTE, COMO VENDEDOR (CONTRAYENTE DE DEUDA) SEÑOR(A): URQUIA LOPEZ ROSA LUCIA, IDENTIFICADO (A) CON DNI: 00098119,  DOMICILIADO EN ,  DISTRITO DE , PROVINCIA DE , DEPARTAMENTO DE , Y DE  OTRA PARTE EN CALIDAD DE COMPRADOR EL SEÑOR ALAIN RAVICHAGUA SOTELO, IDENTIFICADO CON DNI: 44892319 DOMICILIADO EN PROG. CUZCO N° 2309, DISTRITO DE HUANCAYO, PROVINCIA DE HUANCAYO, DEPARTAMENTO DE JUNÍN. BAJO LOS TÉRMINOS Y CONDICIONES SIGUIENTES:	\N	\N	03:30:00-05	0         	\N	3863	\N
124	FRANK MARTINEZ YUPANQUI	2	2	2020-07-18 11:17:16	PRINCIPAL	CAPITAL DE TRABAJO	20682065	0	NINGUNO	0	0	0	NINGUNO		0	85	8	24	0	1	0	2020-07-20	300	26	1.04000000000000004	2020-08-19		\N	1	1872020124149   	f	\N	2020-07-18 11:19:25.354	4	0	11.5399999999999991	0.92000000000000004	0.0400000000000000008	12.5	242.340000000000003	19.3200000000000003	0.839999999999999969	0	21	262.5	0	0	0	0	0	0	0.5	0	2020-07-25 12:03:31.992	\N	\N	0	\N	0	90	\N	\N	HUANCAYO                                          	\N	\N	\N	CONSTE POR EL PRESENTE CONTRATO DE COMPRA  Y VENTA QUE OTORGA DE UNA PARTE, COMO VENDEDOR (CONTRAYENTE DE DEUDA) SEÑOR(A): URBANO PAUCARCAJA MARIA TEODORA , IDENTIFICADO (A) CON DNI: 20682065,  DOMICILIADO EN ,  DISTRITO DE , PROVINCIA DE , DEPARTAMENTO DE , Y DE  OTRA PARTE EN CALIDAD DE COMPRADOR EL SEÑOR ALAIN RAVICHAGUA SOTELO, IDENTIFICADO CON DNI: 44892319 DOMICILIADO EN PROG. CUZCO N° 2309, DISTRITO DE HUANCAYO, PROVINCIA DE HUANCAYO, DEPARTAMENTO DE JUNÍN. BAJO LOS TÉRMINOS Y CONDICIONES SIGUIENTES:	\N	\N	12:30:00-05	0         	\N	3876	\N
131	KEWIN GILBER VELA PEREZ	11	2	2020-08-06 06:16:44	PARALELO	CAPITAL DE TRABAJO	00124443	0	NINGUNO	0\r\n0\r\n\r\n\r\n\r\n0	0	0	NINGUNO		0	84	8	48	1	1	0	2020-08-13	600	4	0	2020-09-03		\N	1	782020131230    	f	\N	2020-08-07 09:25:46.575	9	0	150	12	0	162	450	36	0	0	3	486	0	0	0	0	0	0	0.5	0	2020-08-18 18:01:26.552	\N	\N	0	\N	0	89	\N	\N	PUCALLPA                                          	\N	\N	\N	CONSTE POR EL PRESENTE CONTRATO DE COMPRA  Y VENTA QUE OTORGA DE UNA PARTE, COMO VENDEDOR (CONTRAYENTE DE DEUDA) SEÑOR(A): AGUILAR YSLA JUAN MANUEL, IDENTIFICADO (A) CON DNI: 00124443,  DOMICILIADO EN ,  DISTRITO DE , PROVINCIA DE , DEPARTAMENTO DE , Y DE  OTRA PARTE EN CALIDAD DE COMPRADOR EL SEÑOR ALAIN RAVICHAGUA SOTELO, IDENTIFICADO CON DNI: 44892319 DOMICILIADO EN PROG. CUZCO N° 2309, DISTRITO DE HUANCAYO, PROVINCIA DE HUANCAYO, DEPARTAMENTO DE JUNÍN. BAJO LOS TÉRMINOS Y CONDICIONES SIGUIENTES:	\N	\N	03:00:00-05	0         	\N	3895	\N
63	MELISA DINA CHUQUILLANQUI CUNO 	3	2	2020-07-04 11:22:17	PRINCIPAL	CAPITAL DE TRABAJO	19946742	0	GARANTIAS FLOTANTE	AHORROS	180	0	NINGUNO		0	52	3	204.599999999999994	1	4	0	2020-07-11	1705	16	0	2020-10-24		\N	1	107202063657    	f	\N	2020-07-10 12:20:13.719	4	0	106.5625	12.7874999999999996	0	119.349999999999994	1598.43000000000006	191.819999999999993	0	0	15	1790.25	0	0	0	0	0	0	0.5	0	2020-07-10 14:46:52.849	\N	\N	0	\N	0	51	\N	\N	HUANCAYO                                          	\N	\N	\N	CONSTE POR EL PRESENTE CONTRATO DE COMPRA  Y VENTA QUE OTORGA DE UNA PARTE, COMO VENDEDOR (CONTRAYENTE DE DEUDA) SEÑOR(A): MARMOLEJO ORELLANA BETTY ELIZABETH, IDENTIFICADO (A) CON DNI: 19946742,  DOMICILIADO EN ,  DISTRITO DE , PROVINCIA DE , DEPARTAMENTO DE , Y DE  OTRA PARTE EN CALIDAD DE COMPRADOR EL SEÑOR ALAIN RAVICHAGUA SOTELO, IDENTIFICADO CON DNI: 44892319 DOMICILIADO EN PROG. CUZCO N° 2309, DISTRITO DE HUANCAYO, PROVINCIA DE HUANCAYO, DEPARTAMENTO DE JUNÍN. BAJO LOS TÉRMINOS Y CONDICIONES SIGUIENTES:	\N	\N	12:30:00-05	1         	\N	3862	\N
101	KEWIN GILBER VELA PEREZ	11	2	2020-07-10 09:16:34	RECURRENTE	CAPITAL DE TRABAJO	42148384	0	NINGUNO	0	0	0	NINGUNO	0	0	79	8	40	0	1	0	2020-07-11	500	26	0.780000000000000027	2020-08-11	ESPERO SU APROVACION	\N	1	1072020101573   	f	\N	2020-07-10 22:25:50.684	9	2	19.2300000000000004	1.54000000000000004	0.0299999999999999989	20.8000000000000007	0	0	0	0	0	0	0	0	0	0	0	0	0.5	0	2020-08-11 17:51:33.52	\N	\N	0	\N	0	84	\N	\N	PUCALLPA                                          	\N	\N	\N	CONSTE POR EL PRESENTE CONTRATO DE COMPRA  Y VENTA QUE OTORGA DE UNA PARTE, COMO VENDEDOR (CONTRAYENTE DE DEUDA) SEÑOR(A): SAJAMI VASQUEZ ERIKA LUCIA, IDENTIFICADO (A) CON DNI: 42148384,  DOMICILIADO EN ,  DISTRITO DE , PROVINCIA DE , DEPARTAMENTO DE , Y DE  OTRA PARTE EN CALIDAD DE COMPRADOR EL SEÑOR ALAIN RAVICHAGUA SOTELO, IDENTIFICADO CON DNI: 44892319 DOMICILIADO EN PROG. CUZCO N° 2309, DISTRITO DE HUANCAYO, PROVINCIA DE HUANCAYO, DEPARTAMENTO DE JUNÍN. BAJO LOS TÉRMINOS Y CONDICIONES SIGUIENTES:	\N	\N	03:00:00-05	0         	\N	3863	\N
118	JUANITA RUTH HERRERA SOTO	5	6	2020-07-15 10:17:03	PRINCIPAL	CAPITAL DE TRABAJO	19882294	0	GARANTIAS FLOTANTE	AHORRO PROGRAMADO 	55	0	NINGUNO		0	90	8	24	0	1	0	2020-07-16	300	26	1.04000000000000004	2020-08-15		\N	1	1572020118059   	f	\N	2020-07-15 11:34:26.534	4	0	11.5399999999999991	0.92000000000000004	0.0400000000000000008	12.5	230.800000000000011	18.3999999999999986	0.800000000000000044	0	20	250	0	0	1	0	0	0	0.5	0	2020-07-22 12:56:12.561	\N	\N	0	\N	0	95	\N	\N	HUANCAYO                                          	\N	\N	\N	CONSTE POR EL PRESENTE CONTRATO DE COMPRA  Y VENTA QUE OTORGA DE UNA PARTE, COMO VENDEDOR (CONTRAYENTE DE DEUDA) SEÑOR(A): HILARIO DE LA CRUZ RITA, IDENTIFICADO (A) CON DNI: 19882294,  DOMICILIADO EN ,  DISTRITO DE , PROVINCIA DE , DEPARTAMENTO DE , Y DE  OTRA PARTE EN CALIDAD DE COMPRADOR EL SEÑOR ALAIN RAVICHAGUA SOTELO, IDENTIFICADO CON DNI: 44892319 DOMICILIADO EN PROG. CUZCO N° 2309, DISTRITO DE HUANCAYO, PROVINCIA DE HUANCAYO, DEPARTAMENTO DE JUNÍN. BAJO LOS TÉRMINOS Y CONDICIONES SIGUIENTES:	\N	\N	12:30:00-05	1         	\N	3870	\N
105	JUANITA RUTH HERRERA SOTO	5	2	2020-07-11 12:17:02	PRINCIPAL	CAPITAL DE TRABAJO	48059190	0	NINGUNO		0	0	NINGUNO		0	82	1	55.3800000000000026	0	3	0	2020-07-13	2000	72	3.60000000000000009	2020-10-05		\N	1	1672020105167   	f	\N	2020-07-16 14:31:03.52	4	0	27.7800000000000011	0.770000000000000018	0.0500000000000000028	28.6000000000000014	2000	55.3800000000000026	3.60000000000000009	0	72	2059.19999999999982	0	0	0	0	0	0	0.5	0	2020-07-16 14:31:03.52	\N	\N	0	\N	0	86	\N	\N	HUANCAYO                                          	\N	\N	\N	CONSTE POR EL PRESENTE CONTRATO DE COMPRA  Y VENTA QUE OTORGA DE UNA PARTE, COMO VENDEDOR (CONTRAYENTE DE DEUDA) SEÑOR(A): TICSE CAMPOS LUCERO STEFANY, IDENTIFICADO (A) CON DNI: 48059190,  DOMICILIADO EN ,  DISTRITO DE , PROVINCIA DE , DEPARTAMENTO DE , Y DE  OTRA PARTE EN CALIDAD DE COMPRADOR EL SEÑOR ALAIN RAVICHAGUA SOTELO, IDENTIFICADO CON DNI: 44892319 DOMICILIADO EN PROG. CUZCO N° 2309, DISTRITO DE HUANCAYO, PROVINCIA DE HUANCAYO, DEPARTAMENTO DE JUNÍN. BAJO LOS TÉRMINOS Y CONDICIONES SIGUIENTES:	\N	\N	12:30:00-05	1         	\N	3872	\N
103	JUANITA RUTH HERRERA SOTO	5	2	2020-07-11 10:31:49	PRINCIPAL	CAPITAL DE TRABAJO	47858782	0	NINGUNO		0	0	NINGUNO		0	78	0	0	4	0	0	2020-07-12	168.300000000000011	1	0	2020-07-12		\N	1	1172020103814   	f	\N	2020-07-11 12:23:43.683	4	1	168	0	0	168	0	0	0	0	0	0	0	0	0	0	0	0	0.5	0.5	2020-07-11 12:28:13.249	\N	\N	0	\N	0	82	\N	\N	HUANCAYO                                          	\N	\N	\N	CONSTE POR EL PRESENTE CONTRATO DE COMPRA  Y VENTA QUE OTORGA DE UNA PARTE, COMO VENDEDOR (CONTRAYENTE DE DEUDA) SEÑOR(A): CAMAYO MOLINA DEIVES PERCY, IDENTIFICADO (A) CON DNI: 47858782,  DOMICILIADO EN ,  DISTRITO DE , PROVINCIA DE , DEPARTAMENTO DE , Y DE  OTRA PARTE EN CALIDAD DE COMPRADOR EL SEÑOR ALAIN RAVICHAGUA SOTELO, IDENTIFICADO CON DNI: 44892319 DOMICILIADO EN PROG. CUZCO N° 2309, DISTRITO DE HUANCAYO, PROVINCIA DE HUANCAYO, DEPARTAMENTO DE JUNÍN. BAJO LOS TÉRMINOS Y CONDICIONES SIGUIENTES:	\N	\N	12:30:00-05	1         	\N	3864	\N
104	JUANITA RUTH HERRERA SOTO	5	2	2020-07-11 11:43:10	PRINCIPAL	CAPITAL DE TRABAJO	76918572	0	NINGUNO		0	0	NINGUNO		0	81	1.10000002	10.0342000000000002	1	2	0	2020-07-18	456.100000000000023	8	0	2020-09-05		\N	1	1672020104995   	f	\N	2020-07-16 14:33:41.784	4	0	57.0125000000000028	1.25427500000000003	0	58.2999999999999972	456.100000000000023	10.0342000000000002	0	0	8	466.399999999999977	0	0	0	0	0	0	0.5	0	2020-07-16 14:33:41.784	\N	\N	0	\N	0	85	\N	\N	HUANCAYO                                          	\N	\N	\N	CONSTE POR EL PRESENTE CONTRATO DE COMPRA  Y VENTA QUE OTORGA DE UNA PARTE, COMO VENDEDOR (CONTRAYENTE DE DEUDA) SEÑOR(A): GOMEZ QUISPE NAYSHA EDILUZ, IDENTIFICADO (A) CON DNI: 76918572,  DOMICILIADO EN ,  DISTRITO DE , PROVINCIA DE , DEPARTAMENTO DE , Y DE  OTRA PARTE EN CALIDAD DE COMPRADOR EL SEÑOR ALAIN RAVICHAGUA SOTELO, IDENTIFICADO CON DNI: 44892319 DOMICILIADO EN PROG. CUZCO N° 2309, DISTRITO DE HUANCAYO, PROVINCIA DE HUANCAYO, DEPARTAMENTO DE JUNÍN. BAJO LOS TÉRMINOS Y CONDICIONES SIGUIENTES:	\N	\N	12:30:00-05	1         	\N	3872	\N
119	JUANITA RUTH HERRERA SOTO	5	2	2020-07-15 12:19:04	PRINCIPAL	CAPITAL DE TRABAJO	41738167	0	NINGUNO		0	0	NINGUNO		0	92	8	12	2	1	0	2020-07-30	150	2	0	2020-08-14		\N	0	\N	\N	\N	\N	\N	\N	75	6	0	81	150	12	0	0	2	162	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	97	\N	\N	HUANCAYO                                          	\N	\N	\N	CONSTE POR EL PRESENTE CONTRATO DE COMPRA  Y VENTA QUE OTORGA DE UNA PARTE, COMO VENDEDOR (CONTRAYENTE DE DEUDA) SEÑOR(A): MEDINA RIVEROS RUBEN EFRAIN, IDENTIFICADO (A) CON DNI: 41738167,  DOMICILIADO EN ,  DISTRITO DE , PROVINCIA DE , DEPARTAMENTO DE , Y DE  OTRA PARTE EN CALIDAD DE COMPRADOR EL SEÑOR ALAIN RAVICHAGUA SOTELO, IDENTIFICADO CON DNI: 44892319 DOMICILIADO EN PROG. CUZCO N° 2309, DISTRITO DE HUANCAYO, PROVINCIA DE HUANCAYO, DEPARTAMENTO DE JUNÍN. BAJO LOS TÉRMINOS Y CONDICIONES SIGUIENTES:	\N	\N	12:30:00-05	1         	\N	\N	\N
130	KEWIN GILBER VELA PEREZ	11	2	2020-08-01 07:08:08	PARALELO	CAPITAL DE TRABAJO	00035433	0	NINGUNO	0	0	0	NINGUNO		0	67	12	60	0	1	0	2020-08-03	500	26	1.56000000000000005	2020-09-01		\N	1	582020130456    	f	\N	2020-08-05 21:21:46.605	9	0	19.2300000000000004	2.31000000000000005	0.0599999999999999978	21.6000000000000014	192.300000000000011	23.1000000000000014	0.599999999999999978	0	10	216	2	43.2000000000000028	0	0	0	0	0.5	0	2020-08-23 10:39:13.873	\N	\N	0	\N	0	70	\N	\N	PUCALLPA                                          	\N	\N	\N	CONSTE POR EL PRESENTE CONTRATO DE COMPRA  Y VENTA QUE OTORGA DE UNA PARTE, COMO VENDEDOR (CONTRAYENTE DE DEUDA) SEÑOR(A): PEREZ PEREZ REYNA MARIA, IDENTIFICADO (A) CON DNI: 00035433,  DOMICILIADO EN ,  DISTRITO DE , PROVINCIA DE , DEPARTAMENTO DE , Y DE  OTRA PARTE EN CALIDAD DE COMPRADOR EL SEÑOR ALAIN RAVICHAGUA SOTELO, IDENTIFICADO CON DNI: 44892319 DOMICILIADO EN PROG. CUZCO N° 2309, DISTRITO DE HUANCAYO, PROVINCIA DE HUANCAYO, DEPARTAMENTO DE JUNÍN. BAJO LOS TÉRMINOS Y CONDICIONES SIGUIENTES:	\N	\N	03:00:00-05	0         	\N	3893	\N
125	KEWIN GILBER VELA PEREZ	11	2	2020-07-21 10:46:54	RECURRENTE	CAPITAL DE TRABAJO	48742387	0	NINGUNO	-	0	0	NINGUNO		0	94	12	96	1	1	0	2020-07-28	800	4	0	2020-08-18	ala esperara del credito	\N	1	2272020125323   	f	\N	2020-07-22 12:49:56.673	9	2	200	24	0	224	0	0	0	0	0	0	0	0	0	0	0	0	0.5	0	2020-08-18 17:59:55.491	\N	\N	0	\N	0	98	\N	\N	PUCALLPA                                          	\N	\N	\N	CONSTE POR EL PRESENTE CONTRATO DE COMPRA  Y VENTA QUE OTORGA DE UNA PARTE, COMO VENDEDOR (CONTRAYENTE DE DEUDA) SEÑOR(A): FUCHS VELA ISABEL, IDENTIFICADO (A) CON DNI: 48742387,  DOMICILIADO EN ,  DISTRITO DE , PROVINCIA DE , DEPARTAMENTO DE , Y DE  OTRA PARTE EN CALIDAD DE COMPRADOR EL SEÑOR ALAIN RAVICHAGUA SOTELO, IDENTIFICADO CON DNI: 44892319 DOMICILIADO EN PROG. CUZCO N° 2309, DISTRITO DE HUANCAYO, PROVINCIA DE HUANCAYO, DEPARTAMENTO DE JUNÍN. BAJO LOS TÉRMINOS Y CONDICIONES SIGUIENTES:	\N	\N	03:00:00-05	0         	\N	3880	\N
106	JUANITA RUTH HERRERA SOTO	5	2	2020-07-11 01:00:16	PRINCIPAL	CAPITAL DE TRABAJO	76663751	0	GARANTIAS FLOTANTE	AHORRO PROGRAMADO 	580	0	NINGUNO		0	83	1.5	0.761249999999999982	1	0	0	2020-07-18	203	1	0.0400000000000000008	2020-07-18		\N	1	1172020106056   	f	\N	2020-07-11 13:05:12.848	4	0	203	0.761249999999999982	0.0400000000000000008	203.800000000000011	203	0.761249999999999982	0.0400000000000000008	0	1	203.800000000000011	0	0	0	0	0	0	0.5	0	2020-07-11 13:05:12.848	\N	\N	0	\N	0	88	\N	\N	HUANCAYO                                          	\N	\N	\N	CONSTE POR EL PRESENTE CONTRATO DE COMPRA  Y VENTA QUE OTORGA DE UNA PARTE, COMO VENDEDOR (CONTRAYENTE DE DEUDA) SEÑOR(A): RAMOS NAVARRO ALEJANDRA MISHEL , IDENTIFICADO (A) CON DNI: 76663751,  DOMICILIADO EN ,  DISTRITO DE , PROVINCIA DE , DEPARTAMENTO DE , Y DE  OTRA PARTE EN CALIDAD DE COMPRADOR EL SEÑOR ALAIN RAVICHAGUA SOTELO, IDENTIFICADO CON DNI: 44892319 DOMICILIADO EN PROG. CUZCO N° 2309, DISTRITO DE HUANCAYO, PROVINCIA DE HUANCAYO, DEPARTAMENTO DE JUNÍN. BAJO LOS TÉRMINOS Y CONDICIONES SIGUIENTES:	\N	\N	12:30:00-05	1         	\N	3864	\N
108	JUANITA RUTH HERRERA SOTO	5	2	2020-07-13 10:21:07		CAPITAL DE TRABAJO	20682065	0	GARANTIAS FLOTANTE	AHORRO PROGRAMADO 	510	0	NINGUNO		0	85	8	24	0	1	0	2020-07-14	300	26	1.04000000000000004	2020-08-13		\N	1	1372020108837   	f	\N	2020-07-13 10:49:30.31	4	1	11.5399999999999991	0.92000000000000004	0.0400000000000000008	12.5	0	0	0	0	0	0	0	0	0	0	0	0	1	1	2020-07-22 09:27:18.632	\N	\N	0	\N	0	90	\N	\N	HUANCAYO                                          	\N	\N	\N	CONSTE POR EL PRESENTE CONTRATO DE COMPRA  Y VENTA QUE OTORGA DE UNA PARTE, COMO VENDEDOR (CONTRAYENTE DE DEUDA) SEÑOR(A): URBANO PAUCARCAJA MARIA TEODORA , IDENTIFICADO (A) CON DNI: 20682065,  DOMICILIADO EN ,  DISTRITO DE , PROVINCIA DE , DEPARTAMENTO DE , Y DE  OTRA PARTE EN CALIDAD DE COMPRADOR EL SEÑOR ALAIN RAVICHAGUA SOTELO, IDENTIFICADO CON DNI: 44892319 DOMICILIADO EN PROG. CUZCO N° 2309, DISTRITO DE HUANCAYO, PROVINCIA DE HUANCAYO, DEPARTAMENTO DE JUNÍN. BAJO LOS TÉRMINOS Y CONDICIONES SIGUIENTES:	\N	\N	12:30:00-05	1         	\N	3866	\N
126	JUANITA RUTH HERRERA SOTO	5	2	2020-07-22 11:04:19	PRINCIPAL	CAPITAL DE TRABAJO	19944260	0	NINGUNO		0	0	NINGUNO		0	62	2	34	0	2	0	2020-07-23	850	52	0	2020-09-22		\N	0	\N	\N	\N	\N	\N	\N	16.3500000000000014	0.650000000000000022	0	17	850	34	0	0	52	884	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	64	\N	\N	HUANCAYO                                          	\N	\N	\N	CONSTE POR EL PRESENTE CONTRATO DE COMPRA  Y VENTA QUE OTORGA DE UNA PARTE, COMO VENDEDOR (CONTRAYENTE DE DEUDA) SEÑOR(A): MOLINA QUISPE VERONICA TRINIDAD, IDENTIFICADO (A) CON DNI: 19944260,  DOMICILIADO EN ,  DISTRITO DE , PROVINCIA DE , DEPARTAMENTO DE , Y DE  OTRA PARTE EN CALIDAD DE COMPRADOR EL SEÑOR ALAIN RAVICHAGUA SOTELO, IDENTIFICADO CON DNI: 44892319 DOMICILIADO EN PROG. CUZCO N° 2309, DISTRITO DE HUANCAYO, PROVINCIA DE HUANCAYO, DEPARTAMENTO DE JUNÍN. BAJO LOS TÉRMINOS Y CONDICIONES SIGUIENTES:	\N	\N	12:30:00-05	1         	\N	\N	\N
15	MELISA DINA CHUQUILLANQUI CUNO 	3	6	2020-06-30 01:05:12	PRINCIPAL	CAPITAL DE TRABAJO	40129120	0	GARANTIAS FLOTANTE	ahorro en la coop.proinversion	300	0	NINGUNO		0	14	6	12	0	1	0	2020-07-01	200	26	1.30000000000000004	2020-07-31		\N	1	306202015048    	f	\N	2020-06-30 13:11:46.437	4	0	7.69000000000000039	0.46000000000000002	0.0500000000000000028	8.19999999999999929	38.4500000000000028	2.29999999999999982	0.25	0	5	41	5	41	1	0	0	0	0.5	0	2020-08-24 11:59:31.507	\N	\N	0	\N	0	12	\N	\N	HUANCAYO                                          	\N	\N	\N	CONSTE POR EL PRESENTE CONTRATO DE COMPRA  Y VENTA QUE OTORGA DE UNA PARTE, COMO VENDEDOR (CONTRAYENTE DE DEUDA) SEÑOR(A): CARRIZALES ROJAS JORGE, IDENTIFICADO (A) CON DNI: 40129120,  DOMICILIADO EN ,  DISTRITO DE , PROVINCIA DE , DEPARTAMENTO DE , Y DE  OTRA PARTE EN CALIDAD DE COMPRADOR EL SEÑOR ALAIN RAVICHAGUA SOTELO, IDENTIFICADO CON DNI: 44892319 DOMICILIADO EN PROG. CUZCO N° 2309, DISTRITO DE HUANCAYO, PROVINCIA DE HUANCAYO, DEPARTAMENTO DE JUNÍN. BAJO LOS TÉRMINOS Y CONDICIONES SIGUIENTES:	\N	\N	12:30:00-05	1         	\N	3848	\N
\.


--
-- Data for Name: sueldo_movimiento; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.sueldo_movimiento (id_sueldo_movimiento, id_personal, detalle_sueldo, monto, fecha_hora, estado, oficina, cajera, dni, obs, fecha_hora_ejecutado, agencia, cod_cierre, detalle_devolucion, fecha_hora_devuelto, user_devolucion, monto_devuelto, obs_cancelacion, id_agencia) FROM stdin;
18722	4	FALTANTE	6215.8999	2020-07-01 14:06:02.97257	f	HUANCAYO	BIM	\N	\N	\N	\N	\N	\N	\N	\N	0	\N	\N
18723	4	FALTANTE	1015.40002	2020-07-01 18:33:11.497307	f	HUANCAYO	BIM	\N	\N	\N	\N	\N	\N	\N	\N	0	\N	\N
18724	4	FALTANTE	18302.6992	2020-07-02 14:15:05.061587	f	HUANCAYO	BIM	\N	\N	\N	\N	\N	\N	\N	\N	0	\N	\N
18725	4	FALTANTE	343.399994	2020-07-03 14:55:18.207404	f	HUANCAYO	BIM	\N	\N	\N	\N	\N	\N	\N	\N	0	\N	\N
18726	4	FALTANTE	241.600006	2020-07-04 14:05:17.54667	f	HUANCAYO	BIM	\N	\N	\N	\N	\N	\N	\N	\N	0	\N	\N
18727	4	FALTANTE	3516.3999	2020-07-06 16:15:28.405665	f	HUANCAYO	BIM	\N	\N	\N	\N	\N	\N	\N	\N	0	\N	\N
18728	4	FALTANTE	570	2020-07-07 14:38:06.157801	f	HUANCAYO	BIM	\N	\N	\N	\N	\N	\N	\N	\N	0	\N	\N
18729	4	FALTANTE	75.4000015	2020-07-08 14:07:32.089585	f	HUANCAYO	BIM	\N	\N	\N	\N	\N	\N	\N	\N	0	\N	\N
18730	9	FALTANTE	51.7000008	2020-07-10 22:30:04.107443	f	PUCALLPA	LARIAS	\N	\N	\N	\N	\N	\N	\N	\N	0	\N	\N
18731	4	FALTANTE	1276.30005	2020-07-16 14:56:24.599671	f	HUANCAYO	BIM	\N	\N	\N	\N	\N	\N	\N	\N	0	\N	\N
18732	4	FALTANTE	562.75	2020-07-17 20:49:31.402574	f	HUANCAYO	BIM	\N	\N	\N	\N	\N	\N	\N	\N	0	\N	\N
18733	4	FALTANTE	1360.40002	2020-07-20 15:06:35.595329	f	HUANCAYO	BIM	\N	\N	\N	\N	\N	\N	\N	\N	0	\N	\N
18734	4	FALTANTE	993.599976	2020-07-22 14:24:52.605217	f	HUANCAYO	BIM	\N	\N	\N	\N	\N	\N	\N	\N	0	\N	\N
18735	4	FALTANTE	246.300003	2020-07-23 13:54:40.794642	f	HUANCAYO	BIM	\N	\N	\N	\N	\N	\N	\N	\N	0	\N	\N
18736	4	FALTANTE	15.6000004	2020-07-24 14:27:51.254152	f	HUANCAYO	BIM	\N	\N	\N	\N	\N	\N	\N	\N	0	\N	\N
18737	9	FALTANTE	100	2020-08-04 20:59:02.241374	f	PUCALLPA	LARIAS	\N	\N	\N	\N	\N	\N	\N	\N	0	\N	\N
18738	9	FALTANTE	802	2020-08-10 19:21:54.967861	f	PUCALLPA	LARIAS	\N	\N	\N	\N	\N	\N	\N	\N	0	\N	\N
18739	9	FALTANTE	34.2000008	2020-08-14 18:19:07.13452	f	PUCALLPA	LARIAS	\N	\N	\N	\N	\N	\N	\N	\N	0	\N	\N
18740	9	FALTANTE	800.799988	2020-08-23 10:53:51.399185	f	PUCALLPA	LARIAS	\N	\N	\N	\N	\N	\N	\N	\N	0	\N	\N
\.


--
-- Data for Name: tipo_ahorro; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.tipo_ahorro (id_tipo_ahorro, nombre_ahorro, obs) FROM stdin;
1	LIBRE	\N
4	CAMPAÑA	\N
2	PLAZO FIJO	\N
3	CON INTERES PROGRAMADO	\N
\.


--
-- Data for Name: tipo_notificacion; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.tipo_notificacion (id_tipo_notifi, nombre_notificacion, obs, valor) FROM stdin;
1	AVISO	NINGUNA	0
2	NOTI_NOR	NINGUNA	5
3	ULT_NOTI	NINGUNA	5
4	CAMPAÑA	NINGUNA	0
\.


--
-- Data for Name: usuario; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.usuario (id_usuario, apellidos, nombres, tipo_usuario, direccion, telefono, email, login, pass, obs, activado, salario, salario_actual, agencia, dni, ingreso_temprano, ingreso_tarde, ingreso_sabados, estado_sueldo, fecha_ingreso, movil, uniforme, aporte_afp, aporte_onp, id_agencia, creacion, cargo) FROM stdin;
6	GIRON QUISPE	YULY 	7	JR. JUNIN S/N BARRIO PORVENIR - HUANCAN	972666320	gironquispeyuly@gmail.com	YGIRON	lupita123	NINGUNA	t	1000	0	1	48121718	\N	\N	\N	f	2020-06-01	0	0	0	0	1	2020-06-29 10:30:36.748726	\N
9	ARIAS ROJAS 	LUZ	2	PJS.LOS ANGELES S/N	965859565	AROJAS@GMAIL.COM	LARIAS	1		t	1111	0	3	48495652	\N	\N	\N	f	2020-07-01	0	0	0	0	1	2020-06-30 22:55:45.182436	\N
13	RIVEROS PUMACAHUA	DEIBY	4	CALLE FRANCISCA DE LA CALLE S/N	916748248	deybirp1993@gmail.com	DRIVEROS	1	ninguna	t	0	0	1	48190126	\N	\N	\N	f	2020-07-01	0	0	0	0	1	2020-07-24 13:45:20.719806	\N
7	HERRERAV SOTO	JUANITA RUTH	4	JR CUSCO 1985 HUANCAYO	964920940	74422219@continental.com.pe	JHERRERA	1		t	850	0	1	74422219	\N	\N	\N	f	2020-07-01	0	0	0	0	1	2020-06-29 14:02:26.939779	\N
10	VELA PEREZ	KEWIN GIBER	4	PJS. GENERAL MONTERO N°128	985900883	kvela@proinversiones.com.pe	KVELAPEREZ	1		f	11111	0	3	76238544	\N	\N	\N	f	2020-07-01	0	0	0	0	1	2020-06-30 23:27:27.519072	\N
11	VELA PEREZ	KEWIN GILBER	4	PJS. GENERAL MONTERO N°128	985900883	kvela@proinversiones.com.pe	VELAPEREZ	1		t	1111	0	3	76238544	\N	\N	\N	f	2020-07-01	0	0	0	0	1	2020-06-30 23:31:55.291948	\N
3	CHUQUILLANQUI CUNO 	MELISA DINA	4	AV. L 1876	940776094	melisachuquillanquicuno@gmail.com	MCHUQUILLANQUI	1		t	930	0	1	74895655	\N	\N	\N	f	2020-07-01	0	0	0	0	1	2020-06-26 18:28:40.193861	\N
4	IPARRAGUIRRE MARMOLEJO	BETSY ERMEE	3	AV DANIEL A. CARRION HUAYUCACHI	929992141		BIM	1		t	850	0	1	48003256	\N	\N	\N	f	2020-07-01	0	0	0	0	1	2020-06-27 21:31:44.983395	\N
1	SOPORTE	SOPORTE	9	NO	NO	NO	SOPORTE	2508	\N	t	0	0	1	44892319	\N	\N	\N	f	2020-03-12	0	0	0	0	1	2020-06-26 18:25:18.217989	\N
5	HERRERA SOTO	JUANITA RUTH	4	JR CUZCO N°1985	964920940	74422219@CONTINENTAL.COM.PE	LHERRERA	AADDAF26	NINGUNA	t	850	0	1	74422219	\N	\N	\N	f	2020-07-01	0	0	0	0	1	2020-06-29 10:15:39.013349	\N
8	VELA PEREZ	KEWIN GILBERTO	4	PJS. GENERAL MONTERO N°128	985900883	kvela@proinversiones.com.pe	KVELA	1		t	11111	0	3	76238544	\N	\N	\N	f	2020-07-01	0	0	0	0	1	2020-06-30 22:51:50.893913	\N
2	MARTINEZ YUPANQUI	FRANK	9	PJS. PERU 160 LA  RIVERA	998588180	fmartinez@proinversiones.com	FMARTINEZ17	47286951		t	2500	0	1	47286951	\N	\N	\N	f	2019-12-15	0	0	0	0	1	2020-06-26 18:25:18.217989	\N
12	PINEDO CARHUAMACA	JOSE LUIS	4	CALLE PANAMERICANA S/N	945454519	jos0203@hotmail.com	JPINEDO	1	ninguna	t	0	0	1	45824125	\N	\N	\N	f	2020-07-09	0	0	0	0	1	2020-07-24 13:37:27.886101	\N
\.


--
-- Name: activar_reportes_id_activar_rpt_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.activar_reportes_id_activar_rpt_seq', 1, false);


--
-- Name: agencia_id_agencia_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.agencia_id_agencia_seq', 1, false);


--
-- Name: agencia_id_empresa_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.agencia_id_empresa_seq', 1, false);


--
-- Name: apertura_cierre_id_caja_apertura_cierre_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.apertura_cierre_id_caja_apertura_cierre_seq', 3909, true);


--
-- Name: aportaciones_id_aportaciones_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.aportaciones_id_aportaciones_seq', 1, false);


--
-- Name: banco_id_banco_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.banco_id_banco_seq', 2, true);


--
-- Name: billeteo_id_billeteo_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.billeteo_id_billeteo_seq', 1, false);


--
-- Name: boleta_id_boleta_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.boleta_id_boleta_seq', 1, false);


--
-- Name: boveda_id_boveda_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.boveda_id_boveda_seq', 15, true);


--
-- Name: campana_id_campana_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.campana_id_campana_seq', 1, false);


--
-- Name: central_riesgo_id_central_riesgo_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.central_riesgo_id_central_riesgo_seq', 1, false);


--
-- Name: cierre_caja_principal_id_cierre_principal_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.cierre_caja_principal_id_cierre_principal_seq', 3619, true);


--
-- Name: cierre_planilla_id_cierre_planilla_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.cierre_planilla_id_cierre_planilla_seq', 1, false);


--
-- Name: clave_cliente_id_clave_cliente_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.clave_cliente_id_clave_cliente_seq', 1, false);


--
-- Name: cliente_aval_id_cliente_aval_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.cliente_aval_id_cliente_aval_seq', 3576, true);


--
-- Name: cliente_beneficiario_id_cliente_beneficiario_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.cliente_beneficiario_id_cliente_beneficiario_seq', 431, true);


--
-- Name: compromiso_id_compromiso_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.compromiso_id_compromiso_seq', 1, false);


--
-- Name: conyuge_id_conyuge_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.conyuge_id_conyuge_seq', 3351, true);


--
-- Name: credito_refinanciado_id_refinanciado_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.credito_refinanciado_id_refinanciado_seq', 1, false);


--
-- Name: cronograma_ahorro_id_cronograma_ahorro_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.cronograma_ahorro_id_cronograma_ahorro_seq', 39, true);


--
-- Name: cronograma_pagos_id_cronograma_pago_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.cronograma_pagos_id_cronograma_pago_seq', 415331, true);


--
-- Name: departamento_id_departamento_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.departamento_id_departamento_seq', 1, false);


--
-- Name: detalle_negocio_id_detalle_negocio_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.detalle_negocio_id_detalle_negocio_seq', 1, false);


--
-- Name: detalle_negocio_id_negocio_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.detalle_negocio_id_negocio_seq', 1, false);


--
-- Name: dias_festivos_id_agencia_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.dias_festivos_id_agencia_seq', 1, false);


--
-- Name: dias_festivos_id_dia_festivo_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.dias_festivos_id_dia_festivo_seq', 1, false);


--
-- Name: distrito_id_distrito_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.distrito_id_distrito_seq', 1, false);


--
-- Name: distrito_id_provincia_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.distrito_id_provincia_seq', 1, false);


--
-- Name: empresa_id_empresa_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.empresa_id_empresa_seq', 1, false);


--
-- Name: equipos_id_agencia_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.equipos_id_agencia_seq', 1, false);


--
-- Name: equipos_id_aquipos_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.equipos_id_aquipos_seq', 1, false);


--
-- Name: ficha_domiciliaria_id_ficha_domiciliaria_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.ficha_domiciliaria_id_ficha_domiciliaria_seq', 1, false);


--
-- Name: ficha_economica_id_ficha_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.ficha_economica_id_ficha_seq', 1, false);


--
-- Name: gastos_operativos_id_gastos_operativos_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.gastos_operativos_id_gastos_operativos_seq', 1, false);


--
-- Name: gastos_operativos_id_negocio_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.gastos_operativos_id_negocio_seq', 1, false);


--
-- Name: historial_manipulacion_id_historial_manipulacion_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.historial_manipulacion_id_historial_manipulacion_seq', 82494, true);


--
-- Name: historial_manipulacion_id_usuario_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.historial_manipulacion_id_usuario_seq', 1, false);


--
-- Name: historial_visitas_id_historial_visitas_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.historial_visitas_id_historial_visitas_seq', 8099, true);


--
-- Name: ingreso_egreso_id_ingreso_egreso_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.ingreso_egreso_id_ingreso_egreso_seq', 63, true);


--
-- Name: insumo_negocio_id_detalle_negocio_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.insumo_negocio_id_detalle_negocio_seq', 1, false);


--
-- Name: insumo_negocio_id_insumo_negocio_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.insumo_negocio_id_insumo_negocio_seq', 1, false);


--
-- Name: inventario_bienes_id_inventario_bienes_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.inventario_bienes_id_inventario_bienes_seq', 1, false);


--
-- Name: libreta_ahorro_id_libreta_ahorro_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.libreta_ahorro_id_libreta_ahorro_seq', 383, true);


--
-- Name: metas_personal_id_metas_personal_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.metas_personal_id_metas_personal_seq', 1, false);


--
-- Name: movimiento_id_movimiento_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.movimiento_id_movimiento_seq', 8851, true);


--
-- Name: movimiento_usuario_id_movimiento_usuario_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.movimiento_usuario_id_movimiento_usuario_seq', 1, false);


--
-- Name: negocio_id_negocio_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.negocio_id_negocio_seq', 1, false);


--
-- Name: nivel_usuario_id_nivel_usuario_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.nivel_usuario_id_nivel_usuario_seq', 1, false);


--
-- Name: notificaciones_id_notificaciones_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.notificaciones_id_notificaciones_seq', 12584, true);


--
-- Name: operacion_banco_id_operacion_banco_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.operacion_banco_id_operacion_banco_seq', 3, true);


--
-- Name: operacion_id_operacion_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.operacion_id_operacion_seq', 206, true);


--
-- Name: operacion_pago_id_operacion_pago_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.operacion_pago_id_operacion_pago_seq', 192, true);


--
-- Name: pariente_id_pariente_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.pariente_id_pariente_seq', 1, false);


--
-- Name: planilla_id_planilla_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.planilla_id_planilla_seq', 1, false);


--
-- Name: provincia_id_departamento_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.provincia_id_departamento_seq', 1, false);


--
-- Name: provincia_id_provincia_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.provincia_id_provincia_seq', 1, false);


--
-- Name: sectores_id_sector_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.sectores_id_sector_seq', 1, false);


--
-- Name: solicitud_credito_id_solicitud_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.solicitud_credito_id_solicitud_seq', 1, false);


--
-- Name: sueldo_movimiento_id_sueldo_movimiento_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.sueldo_movimiento_id_sueldo_movimiento_seq', 18740, true);


--
-- Name: tipo_ahorro_id_tipo_ahorro_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.tipo_ahorro_id_tipo_ahorro_seq', 1, false);


--
-- Name: tipo_notificacion_id_tipo_notifi_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.tipo_notificacion_id_tipo_notifi_seq', 1, false);


--
-- Name: usuario_id_usuario_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.usuario_id_usuario_seq', 1, false);


--
-- Name: detalle_negocio id_detalle_negocio; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.detalle_negocio
    ADD CONSTRAINT id_detalle_negocio PRIMARY KEY (id_detalle_negocio);


--
-- Name: gastos_operativos id_gastos_operativos; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.gastos_operativos
    ADD CONSTRAINT id_gastos_operativos PRIMARY KEY (id_gastos_operativos);


--
-- Name: grantia_real id_grantia_real; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.grantia_real
    ADD CONSTRAINT id_grantia_real PRIMARY KEY (id_grantia_real);


--
-- Name: historial_visitas id_historial_visitas; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.historial_visitas
    ADD CONSTRAINT id_historial_visitas PRIMARY KEY (id_historial_visitas);


--
-- Name: insumo_negocio id_insumo_negocio; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.insumo_negocio
    ADD CONSTRAINT id_insumo_negocio PRIMARY KEY (id_insumo_negocio);


--
-- Name: metas_personal id_metas_personal; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.metas_personal
    ADD CONSTRAINT id_metas_personal PRIMARY KEY (id_metas_personal);


--
-- Name: negocio id_negocio; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.negocio
    ADD CONSTRAINT id_negocio PRIMARY KEY (id_negocio);


--
-- Name: rpt_cobranza_diaria id_rpt_cobranza_diaria; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.rpt_cobranza_diaria
    ADD CONSTRAINT id_rpt_cobranza_diaria PRIMARY KEY (id_rpt_cobranza_diaria);


--
-- Name: rpt_creditos_colocados_historial id_rpt_creditos_colocados_historial; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.rpt_creditos_colocados_historial
    ADD CONSTRAINT id_rpt_creditos_colocados_historial PRIMARY KEY (id_rpt_creditos_colocados_historial);


--
-- Name: rpt_movimientos_historial id_rpt_movimientos_historial; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.rpt_movimientos_historial
    ADD CONSTRAINT id_rpt_movimientos_historial PRIMARY KEY (id_rpt_movimientos_historial);


--
-- Name: fotos logos_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.fotos
    ADD CONSTRAINT logos_pkey PRIMARY KEY (codigo);


--
-- Name: activar_reportes pk_activar_rpt; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.activar_reportes
    ADD CONSTRAINT pk_activar_rpt PRIMARY KEY (id_activar_rpt);


--
-- Name: agencia pk_agencia; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.agencia
    ADD CONSTRAINT pk_agencia PRIMARY KEY (id_agencia);


--
-- Name: apertura_cierre pk_apertura_cierre; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.apertura_cierre
    ADD CONSTRAINT pk_apertura_cierre PRIMARY KEY (id_caja_apertura_cierre);


--
-- Name: ingreso_egreso pk_apertura_egreso; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ingreso_egreso
    ADD CONSTRAINT pk_apertura_egreso PRIMARY KEY (id_ingreso_egreso);


--
-- Name: aportaciones pk_aportaciones; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.aportaciones
    ADD CONSTRAINT pk_aportaciones PRIMARY KEY (id_aportaciones);


--
-- Name: banco pk_banco; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.banco
    ADD CONSTRAINT pk_banco PRIMARY KEY (id_banco);


--
-- Name: billeteo pk_billeteo; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.billeteo
    ADD CONSTRAINT pk_billeteo PRIMARY KEY (id_billeteo);


--
-- Name: boleta pk_boleta; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.boleta
    ADD CONSTRAINT pk_boleta PRIMARY KEY (id_boleta);


--
-- Name: boveda pk_boveda; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.boveda
    ADD CONSTRAINT pk_boveda PRIMARY KEY (id_boveda);


--
-- Name: campana pk_campana; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.campana
    ADD CONSTRAINT pk_campana PRIMARY KEY (id_campana);


--
-- Name: central_riesgo pk_central_riesgo; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.central_riesgo
    ADD CONSTRAINT pk_central_riesgo PRIMARY KEY (id_central_riesgo);


--
-- Name: cierre_planilla pk_cierre_planilla; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cierre_planilla
    ADD CONSTRAINT pk_cierre_planilla PRIMARY KEY (id_cierre_planilla);


--
-- Name: ciiu pk_ciiu; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ciiu
    ADD CONSTRAINT pk_ciiu PRIMARY KEY (id_ciiu);


--
-- Name: clave_cliente pk_clave_cliente; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.clave_cliente
    ADD CONSTRAINT pk_clave_cliente PRIMARY KEY (id_clave_cliente);


--
-- Name: cliente pk_cliente; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cliente
    ADD CONSTRAINT pk_cliente PRIMARY KEY (dni);


--
-- Name: cliente_aval pk_cliente_aval; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cliente_aval
    ADD CONSTRAINT pk_cliente_aval PRIMARY KEY (id_cliente_aval);


--
-- Name: cliente_beneficiario pk_cliente_beneficiario; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cliente_beneficiario
    ADD CONSTRAINT pk_cliente_beneficiario PRIMARY KEY (id_cliente_beneficiario);


--
-- Name: compromiso pk_compromiso; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.compromiso
    ADD CONSTRAINT pk_compromiso PRIMARY KEY (id_compromiso);


--
-- Name: conyuge pk_conyuge; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.conyuge
    ADD CONSTRAINT pk_conyuge PRIMARY KEY (id_conyuge);


--
-- Name: credito_refinanciado pk_credito_refinanciado; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.credito_refinanciado
    ADD CONSTRAINT pk_credito_refinanciado PRIMARY KEY (id_refinanciado);


--
-- Name: cronograma_ahorro pk_cronograma_ahorro; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cronograma_ahorro
    ADD CONSTRAINT pk_cronograma_ahorro PRIMARY KEY (id_cronograma_ahorro);


--
-- Name: cronograma_pagos pk_cronograma_pago; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cronograma_pagos
    ADD CONSTRAINT pk_cronograma_pago PRIMARY KEY (id_cronograma_pago);


--
-- Name: departamento pk_departamento; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.departamento
    ADD CONSTRAINT pk_departamento PRIMARY KEY (id_departamento);


--
-- Name: dias_festivos pk_dia_festivo; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.dias_festivos
    ADD CONSTRAINT pk_dia_festivo PRIMARY KEY (id_dia_festivo);


--
-- Name: distrito pk_distrito; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.distrito
    ADD CONSTRAINT pk_distrito PRIMARY KEY (id_distrito);


--
-- Name: empresa pk_empresa; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.empresa
    ADD CONSTRAINT pk_empresa PRIMARY KEY (id_empresa);


--
-- Name: equipos pk_equipos; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.equipos
    ADD CONSTRAINT pk_equipos PRIMARY KEY (id_aquipos);


--
-- Name: ficha_domiciliaria pk_ficha_domiciliaria; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ficha_domiciliaria
    ADD CONSTRAINT pk_ficha_domiciliaria PRIMARY KEY (id_ficha_domiciliaria);


--
-- Name: ficha_economica pk_ficha_economica; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ficha_economica
    ADD CONSTRAINT pk_ficha_economica PRIMARY KEY (id_ficha);


--
-- Name: cierre_caja_principal pk_id_caja_principal; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cierre_caja_principal
    ADD CONSTRAINT pk_id_caja_principal PRIMARY KEY (id_cierre_principal);


--
-- Name: historial_manipulacion pk_id_historial_manipulacion; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.historial_manipulacion
    ADD CONSTRAINT pk_id_historial_manipulacion PRIMARY KEY (id_historial_manipulacion);


--
-- Name: inventario_bienes pk_inventario_bienes; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.inventario_bienes
    ADD CONSTRAINT pk_inventario_bienes PRIMARY KEY (id_inventario_bienes);


--
-- Name: libreta_ahorro pk_libreta_ahorro; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.libreta_ahorro
    ADD CONSTRAINT pk_libreta_ahorro PRIMARY KEY (id_libreta_ahorro);


--
-- Name: movimiento pk_movimiento; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.movimiento
    ADD CONSTRAINT pk_movimiento PRIMARY KEY (id_movimiento);


--
-- Name: movimiento_usuario pk_movimiento_usuario; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.movimiento_usuario
    ADD CONSTRAINT pk_movimiento_usuario PRIMARY KEY (id_movimiento_usuario);


--
-- Name: nivel_usuario pk_nivel_usuario; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.nivel_usuario
    ADD CONSTRAINT pk_nivel_usuario PRIMARY KEY (id_nivel_usuario);


--
-- Name: notificaciones pk_notificaciones; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.notificaciones
    ADD CONSTRAINT pk_notificaciones PRIMARY KEY (id_notificaciones);


--
-- Name: operacion pk_operacion; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.operacion
    ADD CONSTRAINT pk_operacion PRIMARY KEY (id_operacion);


--
-- Name: operacion_banco pk_operacion_banco; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.operacion_banco
    ADD CONSTRAINT pk_operacion_banco PRIMARY KEY (id_operacion_banco);


--
-- Name: operacion_pago pk_operacion_pago; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.operacion_pago
    ADD CONSTRAINT pk_operacion_pago PRIMARY KEY (id_operacion_pago);


--
-- Name: pariente pk_pariente; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.pariente
    ADD CONSTRAINT pk_pariente PRIMARY KEY (id_pariente);


--
-- Name: planilla pk_planilla; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.planilla
    ADD CONSTRAINT pk_planilla PRIMARY KEY (id_planilla);


--
-- Name: provincia pk_provincia; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.provincia
    ADD CONSTRAINT pk_provincia PRIMARY KEY (id_provincia);


--
-- Name: sectores pk_sectores; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sectores
    ADD CONSTRAINT pk_sectores PRIMARY KEY (id_sector);


--
-- Name: solicitud_credito pk_solicitud_credito; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.solicitud_credito
    ADD CONSTRAINT pk_solicitud_credito PRIMARY KEY (id_solicitud);


--
-- Name: sueldo_movimiento pk_sueldo_movimiento; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sueldo_movimiento
    ADD CONSTRAINT pk_sueldo_movimiento PRIMARY KEY (id_sueldo_movimiento);


--
-- Name: tipo_ahorro pk_tipo_ahorro; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tipo_ahorro
    ADD CONSTRAINT pk_tipo_ahorro PRIMARY KEY (id_tipo_ahorro);


--
-- Name: tipo_notificacion pk_tipo_notificacion; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tipo_notificacion
    ADD CONSTRAINT pk_tipo_notificacion PRIMARY KEY (id_tipo_notifi);


--
-- Name: usuario pk_usuario; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.usuario
    ADD CONSTRAINT pk_usuario PRIMARY KEY (id_usuario);


--
-- Name: nivel_usuario unico_funcion; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.nivel_usuario
    ADD CONSTRAINT unico_funcion UNIQUE (funcion);


--
-- Name: usuario unico_login; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.usuario
    ADD CONSTRAINT unico_login UNIQUE (login);


--
-- Name: central_riesgo dni; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.central_riesgo
    ADD CONSTRAINT dni FOREIGN KEY (dni) REFERENCES public.cliente(dni) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: cliente_aval dni; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cliente_aval
    ADD CONSTRAINT dni FOREIGN KEY (dni) REFERENCES public.cliente(dni) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: cliente_beneficiario dni; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cliente_beneficiario
    ADD CONSTRAINT dni FOREIGN KEY (dni) REFERENCES public.cliente(dni) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: conyuge dni; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.conyuge
    ADD CONSTRAINT dni FOREIGN KEY (dni) REFERENCES public.cliente(dni) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: solicitud_credito dni; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.solicitud_credito
    ADD CONSTRAINT dni FOREIGN KEY (dni) REFERENCES public.cliente(dni) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: negocio dni; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.negocio
    ADD CONSTRAINT dni FOREIGN KEY (dni) REFERENCES public.cliente(dni) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: inventario_bienes dni; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.inventario_bienes
    ADD CONSTRAINT dni FOREIGN KEY (dni) REFERENCES public.cliente(dni) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: grantia_real dni; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.grantia_real
    ADD CONSTRAINT dni FOREIGN KEY (dni) REFERENCES public.cliente(dni) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: equipos id_agencia; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.equipos
    ADD CONSTRAINT id_agencia FOREIGN KEY (id_agencia) REFERENCES public.agencia(id_agencia) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: provincia id_departamento; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.provincia
    ADD CONSTRAINT id_departamento FOREIGN KEY (id_departamento) REFERENCES public.departamento(id_departamento) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: insumo_negocio id_detalle_negocio; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.insumo_negocio
    ADD CONSTRAINT id_detalle_negocio FOREIGN KEY (id_detalle_negocio) REFERENCES public.detalle_negocio(id_detalle_negocio) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: agencia id_empresa; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.agencia
    ADD CONSTRAINT id_empresa FOREIGN KEY (id_empresa) REFERENCES public.empresa(id_empresa) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: cronograma_ahorro id_libreta_ahorro; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cronograma_ahorro
    ADD CONSTRAINT id_libreta_ahorro FOREIGN KEY (id_libreta_ahorro) REFERENCES public.libreta_ahorro(id_libreta_ahorro) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: detalle_negocio id_negocio; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.detalle_negocio
    ADD CONSTRAINT id_negocio FOREIGN KEY (id_negocio) REFERENCES public.negocio(id_negocio) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: gastos_operativos id_negocio; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.gastos_operativos
    ADD CONSTRAINT id_negocio FOREIGN KEY (id_negocio) REFERENCES public.negocio(id_negocio) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: distrito id_provincia; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.distrito
    ADD CONSTRAINT id_provincia FOREIGN KEY (id_provincia) REFERENCES public.provincia(id_provincia) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: credito_refinanciado id_solicitud; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.credito_refinanciado
    ADD CONSTRAINT id_solicitud FOREIGN KEY (id_solicitud) REFERENCES public.solicitud_credito(id_solicitud) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: cronograma_pagos id_solicitud; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cronograma_pagos
    ADD CONSTRAINT id_solicitud FOREIGN KEY (id_solicitud) REFERENCES public.solicitud_credito(id_solicitud) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: notificaciones id_solicitud; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.notificaciones
    ADD CONSTRAINT id_solicitud FOREIGN KEY (id_solicitud) REFERENCES public.solicitud_credito(id_solicitud) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: movimiento_usuario id_usuario; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.movimiento_usuario
    ADD CONSTRAINT id_usuario FOREIGN KEY (id_usuario) REFERENCES public.usuario(id_usuario) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- PostgreSQL database dump complete
--


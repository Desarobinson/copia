--
-- PostgreSQL database dump
--

-- Dumped from database version 10.14 (Ubuntu 10.14-0ubuntu0.18.04.1)
-- Dumped by pg_dump version 10.14 (Ubuntu 10.14-0ubuntu0.18.04.1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: antecedente; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.antecedente (
    id_antecedente bigint NOT NULL,
    nombre_empresa character varying(250),
    dni character varying(8),
    fecha_hora_operacion timestamp without time zone NOT NULL,
    obs text
);


ALTER TABLE public.antecedente OWNER TO postgres;

--
-- Name: antecedente_id_antecedente_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.antecedente_id_antecedente_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.antecedente_id_antecedente_seq OWNER TO postgres;

--
-- Name: antecedente_id_antecedente_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.antecedente_id_antecedente_seq OWNED BY public.antecedente.id_antecedente;


--
-- Name: info; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.info (
    id_info bigint NOT NULL,
    nombre_empresa character varying(250),
    dni character varying(8),
    paterno character varying(250),
    fecha_hora_operacion timestamp without time zone NOT NULL,
    monto double precision
);


ALTER TABLE public.info OWNER TO postgres;

--
-- Name: info_id_info_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.info_id_info_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.info_id_info_seq OWNER TO postgres;

--
-- Name: info_id_info_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.info_id_info_seq OWNED BY public.info.id_info;


--
-- Name: antecedente id_antecedente; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.antecedente ALTER COLUMN id_antecedente SET DEFAULT nextval('public.antecedente_id_antecedente_seq'::regclass);


--
-- Name: info id_info; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.info ALTER COLUMN id_info SET DEFAULT nextval('public.info_id_info_seq'::regclass);


--
-- Data for Name: antecedente; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.antecedente (id_antecedente, nombre_empresa, dni, fecha_hora_operacion, obs) FROM stdin;
\.


--
-- Data for Name: info; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.info (id_info, nombre_empresa, dni, paterno, fecha_hora_operacion, monto) FROM stdin;
\.


--
-- Name: antecedente_id_antecedente_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.antecedente_id_antecedente_seq', 1, false);


--
-- Name: info_id_info_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.info_id_info_seq', 1, false);


--
-- Name: antecedente pk_antecedente; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.antecedente
    ADD CONSTRAINT pk_antecedente PRIMARY KEY (id_antecedente);


--
-- Name: info pk_info; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.info
    ADD CONSTRAINT pk_info PRIMARY KEY (id_info);


--
-- PostgreSQL database dump complete
--


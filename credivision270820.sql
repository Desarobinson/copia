--
-- PostgreSQL database dump
--

-- Dumped from database version 10.14 (Ubuntu 10.14-0ubuntu0.18.04.1)
-- Dumped by pg_dump version 10.14 (Ubuntu 10.14-0ubuntu0.18.04.1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: activar_reportes; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.activar_reportes (
    id_activar_rpt integer NOT NULL,
    rpt_cobranza boolean NOT NULL
);


ALTER TABLE public.activar_reportes OWNER TO postgres;

--
-- Name: activar_reportes_id_activar_rpt_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.activar_reportes_id_activar_rpt_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.activar_reportes_id_activar_rpt_seq OWNER TO postgres;

--
-- Name: activar_reportes_id_activar_rpt_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.activar_reportes_id_activar_rpt_seq OWNED BY public.activar_reportes.id_activar_rpt;


--
-- Name: agencia; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.agencia (
    id_agencia integer NOT NULL,
    id_empresa integer NOT NULL,
    nombre_agencia character varying(50) NOT NULL,
    direccion_agencia character varying(50) NOT NULL,
    telefono_agencia character varying(50),
    codigo character(3),
    mensaje character varying(250)
);


ALTER TABLE public.agencia OWNER TO postgres;

--
-- Name: agencia_id_agencia_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.agencia_id_agencia_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.agencia_id_agencia_seq OWNER TO postgres;

--
-- Name: agencia_id_agencia_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.agencia_id_agencia_seq OWNED BY public.agencia.id_agencia;


--
-- Name: agencia_id_empresa_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.agencia_id_empresa_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.agencia_id_empresa_seq OWNER TO postgres;

--
-- Name: agencia_id_empresa_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.agencia_id_empresa_seq OWNED BY public.agencia.id_empresa;


--
-- Name: apertura_cierre; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.apertura_cierre (
    id_caja_apertura_cierre bigint NOT NULL,
    equipo_registrado character varying(50) NOT NULL,
    id_usuario_apertura integer NOT NULL,
    fecha_hora_aperturado timestamp without time zone NOT NULL,
    monto_aperturado double precision NOT NULL,
    obs_aperturado text,
    id_usuario_cierre integer,
    fecha_hora_cierre timestamp without time zone,
    monto_cierre double precision,
    obs_cierre text,
    total_billetes double precision,
    total_monedas double precision,
    total_general double precision,
    total_otros_ingresos double precision,
    total_ingreso_ahorro double precision,
    total_cobrado double precision,
    total_ingresos double precision,
    total_sub_egresos double precision,
    total_retiros double precision,
    total_desembolsado double precision,
    total_egresos double precision,
    total_caja double precision,
    sobra_falta_caja double precision,
    cerrado_principal boolean DEFAULT false NOT NULL,
    id_agencia integer DEFAULT 1 NOT NULL
);


ALTER TABLE public.apertura_cierre OWNER TO postgres;

--
-- Name: apertura_cierre_id_caja_apertura_cierre_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.apertura_cierre_id_caja_apertura_cierre_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.apertura_cierre_id_caja_apertura_cierre_seq OWNER TO postgres;

--
-- Name: apertura_cierre_id_caja_apertura_cierre_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.apertura_cierre_id_caja_apertura_cierre_seq OWNED BY public.apertura_cierre.id_caja_apertura_cierre;


--
-- Name: aportaciones; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.aportaciones (
    id_aportaciones bigint NOT NULL,
    id_caja_apertura_cierre bigint NOT NULL,
    tipo_operacion character varying(15) NOT NULL,
    dni character(8) NOT NULL,
    fecha_hora timestamp without time zone NOT NULL,
    total_pagar real,
    agencia character varying(30),
    puntos bigint,
    id_agencia bigint
);


ALTER TABLE public.aportaciones OWNER TO postgres;

--
-- Name: aportaciones_id_aportaciones_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.aportaciones_id_aportaciones_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.aportaciones_id_aportaciones_seq OWNER TO postgres;

--
-- Name: aportaciones_id_aportaciones_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.aportaciones_id_aportaciones_seq OWNED BY public.aportaciones.id_aportaciones;


--
-- Name: banco; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.banco (
    id_banco bigint NOT NULL,
    entidad character varying(100),
    cuenta_nro character varying(50) NOT NULL,
    fecha_hora_operacion timestamp without time zone NOT NULL,
    usuario character varying(50) NOT NULL,
    obs text,
    monto_actual double precision
);


ALTER TABLE public.banco OWNER TO postgres;

--
-- Name: banco_id_banco_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.banco_id_banco_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.banco_id_banco_seq OWNER TO postgres;

--
-- Name: banco_id_banco_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.banco_id_banco_seq OWNED BY public.banco.id_banco;


--
-- Name: billeteo; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.billeteo (
    id_billeteo bigint NOT NULL,
    id_caja_apertura_cierre bigint NOT NULL,
    denominacion character varying(150) NOT NULL,
    cantidad integer NOT NULL,
    monto character(15) NOT NULL
);


ALTER TABLE public.billeteo OWNER TO postgres;

--
-- Name: billeteo_id_billeteo_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.billeteo_id_billeteo_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.billeteo_id_billeteo_seq OWNER TO postgres;

--
-- Name: billeteo_id_billeteo_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.billeteo_id_billeteo_seq OWNED BY public.billeteo.id_billeteo;


--
-- Name: boleta; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.boleta (
    id_boleta bigint NOT NULL,
    serie integer NOT NULL,
    inicio_boleta integer NOT NULL,
    id_agencia integer
);


ALTER TABLE public.boleta OWNER TO postgres;

--
-- Name: boleta_id_boleta_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.boleta_id_boleta_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.boleta_id_boleta_seq OWNER TO postgres;

--
-- Name: boleta_id_boleta_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.boleta_id_boleta_seq OWNED BY public.boleta.id_boleta;


--
-- Name: boveda; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.boveda (
    id_boveda bigint NOT NULL,
    monto_actual_boveda double precision,
    equipo_registrado character varying(50) NOT NULL,
    fecha_hora_operacion timestamp without time zone NOT NULL,
    tipo character varying(50) NOT NULL,
    monto_movimiento double precision,
    usuario character varying(50) NOT NULL,
    motivo character varying(50) NOT NULL,
    obs text,
    usuario_habilita character varying(100),
    cod_habiltacion bigint,
    confirmar boolean DEFAULT true NOT NULL,
    agencia character varying(50) NOT NULL,
    id_agencia bigint
);


ALTER TABLE public.boveda OWNER TO postgres;

--
-- Name: boveda_id_boveda_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.boveda_id_boveda_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.boveda_id_boveda_seq OWNER TO postgres;

--
-- Name: boveda_id_boveda_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.boveda_id_boveda_seq OWNED BY public.boveda.id_boveda;


--
-- Name: campana; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.campana (
    id_campana integer NOT NULL,
    name_campana character varying(50) NOT NULL,
    monto_campana real NOT NULL,
    inicio_capana timestamp without time zone NOT NULL,
    fin_capana timestamp without time zone NOT NULL,
    id_personal integer NOT NULL,
    obs text,
    id_agencia integer
);


ALTER TABLE public.campana OWNER TO postgres;

--
-- Name: campana_id_campana_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.campana_id_campana_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.campana_id_campana_seq OWNER TO postgres;

--
-- Name: campana_id_campana_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.campana_id_campana_seq OWNED BY public.campana.id_campana;


--
-- Name: central_riesgo; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.central_riesgo (
    id_central_riesgo bigint NOT NULL,
    dni character(8) NOT NULL,
    entidad character varying(80) NOT NULL,
    monto real,
    calificacion character varying(50),
    fecha date,
    id_usuario integer,
    id_agencia integer
);


ALTER TABLE public.central_riesgo OWNER TO postgres;

--
-- Name: central_riesgo_id_central_riesgo_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.central_riesgo_id_central_riesgo_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.central_riesgo_id_central_riesgo_seq OWNER TO postgres;

--
-- Name: central_riesgo_id_central_riesgo_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.central_riesgo_id_central_riesgo_seq OWNED BY public.central_riesgo.id_central_riesgo;


--
-- Name: cierre_caja_principal; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.cierre_caja_principal (
    id_cierre_principal bigint NOT NULL,
    id_usuario integer NOT NULL,
    fecha_cierre date,
    fecha_hora timestamp without time zone NOT NULL,
    obs text,
    monto_cierre real,
    id_agencia integer
);


ALTER TABLE public.cierre_caja_principal OWNER TO postgres;

--
-- Name: cierre_caja_principal_id_cierre_principal_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.cierre_caja_principal_id_cierre_principal_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cierre_caja_principal_id_cierre_principal_seq OWNER TO postgres;

--
-- Name: cierre_caja_principal_id_cierre_principal_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.cierre_caja_principal_id_cierre_principal_seq OWNED BY public.cierre_caja_principal.id_cierre_principal;


--
-- Name: cierre_planilla; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.cierre_planilla (
    id_cierre_planilla integer NOT NULL,
    monto_cierre real NOT NULL,
    fecha_hora_planilla timestamp without time zone,
    login character varying(250),
    agencia character varying(250),
    id_agencia integer
);


ALTER TABLE public.cierre_planilla OWNER TO postgres;

--
-- Name: cierre_planilla_id_cierre_planilla_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.cierre_planilla_id_cierre_planilla_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cierre_planilla_id_cierre_planilla_seq OWNER TO postgres;

--
-- Name: cierre_planilla_id_cierre_planilla_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.cierre_planilla_id_cierre_planilla_seq OWNED BY public.cierre_planilla.id_cierre_planilla;


--
-- Name: ciiu; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.ciiu (
    id_ciiu character varying NOT NULL,
    cod_ciiu character varying(4) NOT NULL,
    descripcion text
);


ALTER TABLE public.ciiu OWNER TO postgres;

--
-- Name: clave_cliente; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.clave_cliente (
    id_clave_cliente bigint NOT NULL,
    dni_cliente character(8) NOT NULL,
    clave character varying(50) NOT NULL
);


ALTER TABLE public.clave_cliente OWNER TO postgres;

--
-- Name: clave_cliente_id_clave_cliente_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.clave_cliente_id_clave_cliente_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.clave_cliente_id_clave_cliente_seq OWNER TO postgres;

--
-- Name: clave_cliente_id_clave_cliente_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.clave_cliente_id_clave_cliente_seq OWNED BY public.clave_cliente.id_clave_cliente;


--
-- Name: cliente; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.cliente (
    dni character(8) NOT NULL,
    paterno character varying(50) NOT NULL,
    materno character varying(50),
    nombres character varying(50) NOT NULL,
    sexo character(10) NOT NULL,
    estado_civil character varying(50) NOT NULL,
    fecha_nacimiento date,
    telefonos character varying(100),
    e_mail character varying(100),
    distrito character varying(50) NOT NULL,
    provincia character varying(50) NOT NULL,
    departamento character varying(50) NOT NULL,
    direccion character varying(120) NOT NULL,
    referencia character varying(120),
    obs text,
    fecha_hora_creacion timestamp without time zone NOT NULL,
    nro_oficina character(50),
    nro_expediente bigint,
    fecha_hora_expediente timestamp without time zone,
    id_analista integer,
    nombre_promotor character varying(100),
    id_plataforma integer,
    historial text,
    nro_ahorro bigint,
    calificacion character(20),
    pass_ahorro character(20),
    nro_expediente_dos bigint,
    estado_cliente character(1),
    usuario character(50),
    castigar_cliente character(5),
    motivo_castigo character(250),
    user_castigo character(50),
    fecha_hora_castigo timestamp without time zone,
    puntos smallint,
    codsocio bigint,
    aportado real,
    tiposocio character varying(50),
    id_registro_user integer,
    id_user_exp integer,
    id_agencia integer,
    fecha_hora_exp_ahorro time with time zone
);


ALTER TABLE public.cliente OWNER TO postgres;

--
-- Name: cliente_aval; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.cliente_aval (
    id_cliente_aval bigint NOT NULL,
    dni character(8) NOT NULL,
    aval character(8),
    parentesco character varying(20)
);


ALTER TABLE public.cliente_aval OWNER TO postgres;

--
-- Name: cliente_aval_id_cliente_aval_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.cliente_aval_id_cliente_aval_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cliente_aval_id_cliente_aval_seq OWNER TO postgres;

--
-- Name: cliente_aval_id_cliente_aval_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.cliente_aval_id_cliente_aval_seq OWNED BY public.cliente_aval.id_cliente_aval;


--
-- Name: cliente_beneficiario; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.cliente_beneficiario (
    id_cliente_beneficiario bigint NOT NULL,
    dni character(8) NOT NULL,
    beneficiario character(8),
    parantesco character varying(20),
    obs text,
    nombre_beni character varying(300)
);


ALTER TABLE public.cliente_beneficiario OWNER TO postgres;

--
-- Name: cliente_beneficiario_id_cliente_beneficiario_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.cliente_beneficiario_id_cliente_beneficiario_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cliente_beneficiario_id_cliente_beneficiario_seq OWNER TO postgres;

--
-- Name: cliente_beneficiario_id_cliente_beneficiario_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.cliente_beneficiario_id_cliente_beneficiario_seq OWNED BY public.cliente_beneficiario.id_cliente_beneficiario;


--
-- Name: compromiso; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.compromiso (
    id_compromiso bigint NOT NULL,
    id_solicitud bigint NOT NULL,
    tipo_compromiso character varying(15) NOT NULL,
    usuario character varying(20) NOT NULL,
    fecha_hora timestamp without time zone NOT NULL,
    fecha_compromiso timestamp without time zone NOT NULL,
    deuda_vencida real,
    id_notificaciones bigint,
    obs text
);


ALTER TABLE public.compromiso OWNER TO postgres;

--
-- Name: compromiso_id_compromiso_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.compromiso_id_compromiso_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.compromiso_id_compromiso_seq OWNER TO postgres;

--
-- Name: compromiso_id_compromiso_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.compromiso_id_compromiso_seq OWNED BY public.compromiso.id_compromiso;


--
-- Name: conyuge; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.conyuge (
    id_conyuge bigint NOT NULL,
    dni character(8) NOT NULL,
    conyuge character(8),
    estado integer DEFAULT 1 NOT NULL,
    obs text
);


ALTER TABLE public.conyuge OWNER TO postgres;

--
-- Name: conyuge_id_conyuge_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.conyuge_id_conyuge_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.conyuge_id_conyuge_seq OWNER TO postgres;

--
-- Name: conyuge_id_conyuge_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.conyuge_id_conyuge_seq OWNED BY public.conyuge.id_conyuge;


--
-- Name: credito_refinanciado; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.credito_refinanciado (
    id_refinanciado bigint NOT NULL,
    id_solicitud integer NOT NULL,
    nro_de_creditos character varying(100),
    detalle_dreditos text,
    fecha_hora_refinanciado timestamp without time zone,
    obs text
);


ALTER TABLE public.credito_refinanciado OWNER TO postgres;

--
-- Name: credito_refinanciado_id_refinanciado_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.credito_refinanciado_id_refinanciado_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.credito_refinanciado_id_refinanciado_seq OWNER TO postgres;

--
-- Name: credito_refinanciado_id_refinanciado_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.credito_refinanciado_id_refinanciado_seq OWNED BY public.credito_refinanciado.id_refinanciado;


--
-- Name: cronograma_ahorro; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.cronograma_ahorro (
    id_cronograma_ahorro bigint NOT NULL,
    id_libreta_ahorro bigint NOT NULL,
    pagado boolean DEFAULT false NOT NULL,
    monto_pactado double precision NOT NULL,
    monto_descontado boolean DEFAULT false NOT NULL,
    nro_cuota integer NOT NULL,
    fecha_pactada date NOT NULL,
    fecha_hora_pagado timestamp without time zone,
    monto_movimiento double precision,
    fecha_hora_amortizacion timestamp without time zone,
    amortizado double precision
);


ALTER TABLE public.cronograma_ahorro OWNER TO postgres;

--
-- Name: cronograma_ahorro_id_cronograma_ahorro_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.cronograma_ahorro_id_cronograma_ahorro_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cronograma_ahorro_id_cronograma_ahorro_seq OWNER TO postgres;

--
-- Name: cronograma_ahorro_id_cronograma_ahorro_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.cronograma_ahorro_id_cronograma_ahorro_seq OWNED BY public.cronograma_ahorro.id_cronograma_ahorro;


--
-- Name: cronograma_pagos; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.cronograma_pagos (
    id_cronograma_pago bigint NOT NULL,
    id_solicitud bigint NOT NULL,
    pagado boolean DEFAULT false NOT NULL,
    capital_x_cuota double precision NOT NULL,
    interes_x_cuota double precision NOT NULL,
    redondeo_x_cuota double precision NOT NULL,
    penalidad_x_cuota double precision NOT NULL,
    cuota_pagar double precision NOT NULL,
    ii double precision NOT NULL,
    ga double precision NOT NULL,
    cuota_descontada boolean DEFAULT false NOT NULL,
    nro_cuota integer NOT NULL,
    fecha_vencimiento date NOT NULL,
    fecha_hora_pagado timestamp without time zone,
    monto_capital double precision,
    monto_interes double precision,
    monto_redondeo double precision,
    mora double precision,
    otros_pagos double precision,
    notificacion double precision,
    fecha_hora_amortizacion timestamp without time zone,
    amortizado double precision,
    pago_con double precision,
    id_operacion bigint
);


ALTER TABLE public.cronograma_pagos OWNER TO postgres;

--
-- Name: cronograma_pagos_id_cronograma_pago_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.cronograma_pagos_id_cronograma_pago_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cronograma_pagos_id_cronograma_pago_seq OWNER TO postgres;

--
-- Name: cronograma_pagos_id_cronograma_pago_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.cronograma_pagos_id_cronograma_pago_seq OWNED BY public.cronograma_pagos.id_cronograma_pago;


--
-- Name: departamento; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.departamento (
    id_departamento integer NOT NULL,
    departamento character varying(250) NOT NULL
);


ALTER TABLE public.departamento OWNER TO postgres;

--
-- Name: departamento_id_departamento_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.departamento_id_departamento_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.departamento_id_departamento_seq OWNER TO postgres;

--
-- Name: departamento_id_departamento_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.departamento_id_departamento_seq OWNED BY public.departamento.id_departamento;


--
-- Name: detalle_negocio; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.detalle_negocio (
    id_detalle_negocio bigint NOT NULL,
    id_negocio bigint NOT NULL,
    tipo_negocio character varying(50),
    cantidad real,
    descripcion character varying(50),
    frecuencia_mes real,
    precio_compra real,
    precio_venta real,
    total_compra real,
    total_venta real,
    usuario character varying(50),
    agencia character varying(50),
    fecha_hora timestamp without time zone,
    estado character(1)
);


ALTER TABLE public.detalle_negocio OWNER TO postgres;

--
-- Name: detalle_negocio_id_detalle_negocio_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.detalle_negocio_id_detalle_negocio_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.detalle_negocio_id_detalle_negocio_seq OWNER TO postgres;

--
-- Name: detalle_negocio_id_detalle_negocio_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.detalle_negocio_id_detalle_negocio_seq OWNED BY public.detalle_negocio.id_detalle_negocio;


--
-- Name: detalle_negocio_id_negocio_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.detalle_negocio_id_negocio_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.detalle_negocio_id_negocio_seq OWNER TO postgres;

--
-- Name: detalle_negocio_id_negocio_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.detalle_negocio_id_negocio_seq OWNED BY public.detalle_negocio.id_negocio;


--
-- Name: dias_festivos; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.dias_festivos (
    id_dia_festivo integer NOT NULL,
    dia integer NOT NULL,
    mes integer NOT NULL,
    anio integer NOT NULL,
    titulo character varying(50),
    id_agencia integer NOT NULL
);


ALTER TABLE public.dias_festivos OWNER TO postgres;

--
-- Name: dias_festivos_id_agencia_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.dias_festivos_id_agencia_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.dias_festivos_id_agencia_seq OWNER TO postgres;

--
-- Name: dias_festivos_id_agencia_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.dias_festivos_id_agencia_seq OWNED BY public.dias_festivos.id_agencia;


--
-- Name: dias_festivos_id_dia_festivo_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.dias_festivos_id_dia_festivo_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.dias_festivos_id_dia_festivo_seq OWNER TO postgres;

--
-- Name: dias_festivos_id_dia_festivo_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.dias_festivos_id_dia_festivo_seq OWNED BY public.dias_festivos.id_dia_festivo;


--
-- Name: distrito; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.distrito (
    id_distrito integer NOT NULL,
    id_provincia integer NOT NULL,
    distrito character varying(50) NOT NULL
);


ALTER TABLE public.distrito OWNER TO postgres;

--
-- Name: distrito_id_distrito_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.distrito_id_distrito_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.distrito_id_distrito_seq OWNER TO postgres;

--
-- Name: distrito_id_distrito_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.distrito_id_distrito_seq OWNED BY public.distrito.id_distrito;


--
-- Name: distrito_id_provincia_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.distrito_id_provincia_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.distrito_id_provincia_seq OWNER TO postgres;

--
-- Name: distrito_id_provincia_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.distrito_id_provincia_seq OWNED BY public.distrito.id_provincia;


--
-- Name: empresa; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.empresa (
    id_empresa integer NOT NULL,
    nombre_empresa character varying(250) NOT NULL,
    codigo_empresa bigint,
    detalles_empresa character varying(250),
    nombre_corto character varying(250)
);


ALTER TABLE public.empresa OWNER TO postgres;

--
-- Name: empresa_id_empresa_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.empresa_id_empresa_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.empresa_id_empresa_seq OWNER TO postgres;

--
-- Name: empresa_id_empresa_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.empresa_id_empresa_seq OWNED BY public.empresa.id_empresa;


--
-- Name: equipos; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.equipos (
    id_aquipos integer NOT NULL,
    id_agencia integer NOT NULL,
    nombre_equipo character varying(50) NOT NULL,
    area character varying(50)
);


ALTER TABLE public.equipos OWNER TO postgres;

--
-- Name: equipos_id_agencia_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.equipos_id_agencia_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.equipos_id_agencia_seq OWNER TO postgres;

--
-- Name: equipos_id_agencia_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.equipos_id_agencia_seq OWNED BY public.equipos.id_agencia;


--
-- Name: equipos_id_aquipos_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.equipos_id_aquipos_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.equipos_id_aquipos_seq OWNER TO postgres;

--
-- Name: equipos_id_aquipos_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.equipos_id_aquipos_seq OWNED BY public.equipos.id_aquipos;


--
-- Name: ficha_domiciliaria; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.ficha_domiciliaria (
    id_ficha_domiciliaria bigint NOT NULL,
    id_analista bigint,
    id_solicitud bigint,
    agencia character varying(50),
    dni_cliente character(8) NOT NULL,
    zonificacion character varying(50),
    t_inmueble character varying(50),
    t_edificacion character varying(50),
    t_zona character varying(50),
    t_construccion character varying(50),
    est_inmueble character varying(50),
    zona character varying(50),
    serv_basicos character varying(50),
    grado_peligro character varying(50),
    num_depend character varying(50),
    name_conyuge character varying(150),
    nivel_instruccion character varying(50),
    acupacion character varying(50),
    direccion_correcta character varying(150),
    jardin character varying(50),
    cochera character varying(50),
    pisos_nro character varying(50),
    acabados character varying(50),
    conservacion character varying(50),
    seguridad character varying(50),
    ubicacion character varying(50),
    vigilancia character varying(50),
    pista character varying(50),
    vereda character varying(50),
    atendio character varying(50),
    residencia character varying(50),
    condicion character varying(50),
    t_residencia character varying(50),
    estado_civil character varying(50),
    habita character varying(50),
    medio_comunicacion character varying(50),
    name_informante character varying(150),
    documento_dni character varying(50),
    parentesco character varying(50),
    telefono_infor character varying(50),
    cerco character varying(150),
    fachada character varying(150),
    paredes character varying(150),
    techo character varying(150),
    piso character varying(150),
    puertas character varying(150),
    ventana character varying(150),
    suministro character varying(50),
    coment_primera_v character varying(250),
    coment_segunda_v character varying(250),
    nombre_vecino character varying(150),
    obs_vecino character varying(150),
    estado_formulario character varying(50),
    fecha_hora_ficha character varying(50),
    fecha_hora_modi character varying(50),
    id_agencia integer,
    obs text
);


ALTER TABLE public.ficha_domiciliaria OWNER TO postgres;

--
-- Name: ficha_domiciliaria_id_ficha_domiciliaria_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.ficha_domiciliaria_id_ficha_domiciliaria_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ficha_domiciliaria_id_ficha_domiciliaria_seq OWNER TO postgres;

--
-- Name: ficha_domiciliaria_id_ficha_domiciliaria_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.ficha_domiciliaria_id_ficha_domiciliaria_seq OWNED BY public.ficha_domiciliaria.id_ficha_domiciliaria;


--
-- Name: ficha_economica; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.ficha_economica (
    id_ficha bigint NOT NULL,
    dni_cliente character(8) NOT NULL,
    ingreso_uno double precision,
    ingreso_dos double precision,
    ingreso_tres double precision,
    carga_familar double precision,
    gastos_alimentacion double precision,
    gastos_estudio double precision,
    servicios_familiar double precision,
    otros_familiar double precision,
    capital_inicial double precision,
    ventas_negocio double precision,
    otro_venta_negocio double precision,
    personal_negocio double precision,
    alquiler_negocio double precision,
    servicios_negocio double precision,
    otros_negocio_salida double precision,
    resultado_economico double precision,
    fecha_hora_ficha timestamp without time zone,
    monto_aprobado double precision,
    fecha_hora_modificacion timestamp without time zone,
    obs text,
    estado_ficha boolean DEFAULT false NOT NULL,
    monto_inicial double precision,
    cambio_monto double precision,
    incremento double precision,
    fecha_limite date,
    sw character(1),
    agencia character varying(250),
    login character varying(15),
    obs_mod text
);


ALTER TABLE public.ficha_economica OWNER TO postgres;

--
-- Name: ficha_economica_id_ficha_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.ficha_economica_id_ficha_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ficha_economica_id_ficha_seq OWNER TO postgres;

--
-- Name: ficha_economica_id_ficha_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.ficha_economica_id_ficha_seq OWNED BY public.ficha_economica.id_ficha;


--
-- Name: fotos; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.fotos (
    codigo integer NOT NULL,
    dni character(8),
    tipo character varying(50),
    descripcion character varying(100),
    imagen bytea,
    fecha date,
    agencia character varying(250)
);


ALTER TABLE public.fotos OWNER TO postgres;

--
-- Name: gastos_operativos; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.gastos_operativos (
    id_gastos_operativos bigint NOT NULL,
    id_negocio bigint NOT NULL,
    descripcion character varying(50),
    monto real,
    usuario character varying(50),
    agencia character varying(50),
    fecha_hora timestamp without time zone,
    estado character(1)
);


ALTER TABLE public.gastos_operativos OWNER TO postgres;

--
-- Name: gastos_operativos_id_gastos_operativos_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.gastos_operativos_id_gastos_operativos_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.gastos_operativos_id_gastos_operativos_seq OWNER TO postgres;

--
-- Name: gastos_operativos_id_gastos_operativos_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.gastos_operativos_id_gastos_operativos_seq OWNED BY public.gastos_operativos.id_gastos_operativos;


--
-- Name: gastos_operativos_id_negocio_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.gastos_operativos_id_negocio_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.gastos_operativos_id_negocio_seq OWNER TO postgres;

--
-- Name: gastos_operativos_id_negocio_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.gastos_operativos_id_negocio_seq OWNED BY public.gastos_operativos.id_negocio;


--
-- Name: grantia_real; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.grantia_real (
    id_grantia_real bigint NOT NULL,
    dni character(8) NOT NULL,
    bien_real character varying(50) NOT NULL,
    caracteristicas character varying(820),
    comprobante character varying(50) NOT NULL,
    serie character varying(50) NOT NULL,
    estado character varying(50) NOT NULL,
    valor_compra real,
    valor_residual real,
    antiguedad real,
    expectativa real,
    id_usuario integer,
    fecha_hora timestamp without time zone NOT NULL,
    agencia character(30),
    valor_real real,
    credito real,
    valor_vendido real,
    almacen real,
    id_usuario_venta integer,
    fecha_venta character varying(50)
);


ALTER TABLE public.grantia_real OWNER TO postgres;

--
-- Name: historial_manipulacion; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.historial_manipulacion (
    id_historial_manipulacion bigint NOT NULL,
    id_usuario integer NOT NULL,
    operacion character varying(100) NOT NULL,
    time_ingreso timestamp without time zone,
    maquina character varying(100),
    agencia character varying(100)
);


ALTER TABLE public.historial_manipulacion OWNER TO postgres;

--
-- Name: historial_manipulacion_id_historial_manipulacion_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.historial_manipulacion_id_historial_manipulacion_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.historial_manipulacion_id_historial_manipulacion_seq OWNER TO postgres;

--
-- Name: historial_manipulacion_id_historial_manipulacion_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.historial_manipulacion_id_historial_manipulacion_seq OWNED BY public.historial_manipulacion.id_historial_manipulacion;


--
-- Name: historial_manipulacion_id_usuario_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.historial_manipulacion_id_usuario_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.historial_manipulacion_id_usuario_seq OWNER TO postgres;

--
-- Name: historial_manipulacion_id_usuario_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.historial_manipulacion_id_usuario_seq OWNED BY public.historial_manipulacion.id_usuario;


--
-- Name: historial_visitas; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.historial_visitas (
    id_historial_visitas bigint NOT NULL,
    dni character(8) NOT NULL,
    tipo_cliente character varying(250),
    nombre_completo character varying(250),
    fecha_hora_visita timestamp without time zone,
    operacion_realizar character varying(250) NOT NULL,
    id_usuario integer NOT NULL,
    credito character(20),
    ahorro character(20),
    detalle_operacion text,
    sacar_cita boolean DEFAULT false NOT NULL,
    cita_analista integer,
    resultado_cita text,
    fecha_cita_realizada timestamp with time zone,
    fecha_acitar timestamp with time zone,
    blokear boolean,
    agencia character varying(250),
    id_agencia integer
);


ALTER TABLE public.historial_visitas OWNER TO postgres;

--
-- Name: historial_visitas_id_historial_visitas_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.historial_visitas_id_historial_visitas_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.historial_visitas_id_historial_visitas_seq OWNER TO postgres;

--
-- Name: historial_visitas_id_historial_visitas_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.historial_visitas_id_historial_visitas_seq OWNED BY public.historial_visitas.id_historial_visitas;


--
-- Name: ingreso_egreso; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.ingreso_egreso (
    id_ingreso_egreso bigint NOT NULL,
    id_caja_apertura_cierre bigint,
    tipo smallint,
    monto double precision,
    solicitante character varying(200),
    concepto character varying(200),
    confirmar boolean DEFAULT true NOT NULL,
    sustento character varying(200),
    fecha_hora_operacion timestamp without time zone,
    agencia character varying(250),
    id_agencia integer,
    tipo_operacion integer DEFAULT 0 NOT NULL,
    numero_documento character varying(250)
);


ALTER TABLE public.ingreso_egreso OWNER TO postgres;

--
-- Name: ingreso_egreso_id_ingreso_egreso_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.ingreso_egreso_id_ingreso_egreso_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ingreso_egreso_id_ingreso_egreso_seq OWNER TO postgres;

--
-- Name: ingreso_egreso_id_ingreso_egreso_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.ingreso_egreso_id_ingreso_egreso_seq OWNED BY public.ingreso_egreso.id_ingreso_egreso;


--
-- Name: insumo_negocio; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.insumo_negocio (
    id_insumo_negocio bigint NOT NULL,
    id_detalle_negocio bigint NOT NULL,
    cantidad real,
    descripcion character varying(50),
    precio real,
    total real,
    usuario character varying(50),
    agencia character varying(50),
    fecha_hora timestamp without time zone,
    estado character(1)
);


ALTER TABLE public.insumo_negocio OWNER TO postgres;

--
-- Name: insumo_negocio_id_detalle_negocio_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.insumo_negocio_id_detalle_negocio_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.insumo_negocio_id_detalle_negocio_seq OWNER TO postgres;

--
-- Name: insumo_negocio_id_detalle_negocio_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.insumo_negocio_id_detalle_negocio_seq OWNED BY public.insumo_negocio.id_detalle_negocio;


--
-- Name: insumo_negocio_id_insumo_negocio_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.insumo_negocio_id_insumo_negocio_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.insumo_negocio_id_insumo_negocio_seq OWNER TO postgres;

--
-- Name: insumo_negocio_id_insumo_negocio_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.insumo_negocio_id_insumo_negocio_seq OWNED BY public.insumo_negocio.id_insumo_negocio;


--
-- Name: inventario_bienes; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.inventario_bienes (
    id_inventario_bienes bigint NOT NULL,
    dni character(8) NOT NULL,
    bien character varying(250) NOT NULL,
    caracteristica character varying(350) NOT NULL,
    valorizado double precision,
    usuario character varying(70) NOT NULL,
    fecha_hora_ingreso text,
    fecha_hora_manipulacion text
);


ALTER TABLE public.inventario_bienes OWNER TO postgres;

--
-- Name: inventario_bienes_id_inventario_bienes_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.inventario_bienes_id_inventario_bienes_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.inventario_bienes_id_inventario_bienes_seq OWNER TO postgres;

--
-- Name: inventario_bienes_id_inventario_bienes_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.inventario_bienes_id_inventario_bienes_seq OWNED BY public.inventario_bienes.id_inventario_bienes;


--
-- Name: libreta_ahorro; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.libreta_ahorro (
    id_libreta_ahorro bigint NOT NULL,
    dni character(8) NOT NULL,
    nombre_oficina character varying(50) NOT NULL,
    id_cajera integer,
    id_administrador real,
    id_benificiario bigint,
    id_tipo_ahorro bigint,
    fecha_apertura date NOT NULL,
    monto_apertura double precision NOT NULL,
    monto_actual double precision NOT NULL,
    interes_actual double precision,
    tiempo_plazo real,
    monto_meta double precision,
    bloquear boolean NOT NULL,
    obs text,
    fecha_a_retirar date,
    monto_financiado double precision,
    fecha_retiro date,
    id_cajera_retiro integer,
    tea double precision,
    interes_generado double precision,
    monto_retirado double precision,
    ultimo_movimiento date,
    monto_pactado double precision,
    dni_benificiario character(8),
    tipo_campana character(3),
    fin_campana date,
    producto text,
    usuario_ope character varying(50),
    id_analista integer,
    usuario_analista character varying(50),
    estado integer,
    id_agencia integer,
    periodo character varying(250),
    p_retiro integer
);


ALTER TABLE public.libreta_ahorro OWNER TO postgres;

--
-- Name: libreta_ahorro_id_libreta_ahorro_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.libreta_ahorro_id_libreta_ahorro_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.libreta_ahorro_id_libreta_ahorro_seq OWNER TO postgres;

--
-- Name: libreta_ahorro_id_libreta_ahorro_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.libreta_ahorro_id_libreta_ahorro_seq OWNED BY public.libreta_ahorro.id_libreta_ahorro;


--
-- Name: metas_personal; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.metas_personal (
    id_metas_personal bigint NOT NULL,
    fecha_hora timestamp without time zone,
    usuario character(50),
    nro_creditos double precision,
    nro_clientes double precision,
    monto_otrogado double precision,
    interes_otrogado double precision,
    redondeo_otrogado double precision,
    monto_saldo double precision,
    interes_saldo double precision,
    redondeo_saldo double precision,
    monto_cobrado double precision,
    interes_cobrado double precision,
    redondeo_cobrado double precision,
    monto_vencido double precision,
    porcentaje double precision,
    mora double precision,
    noti double precision,
    otros double precision,
    nro_creditos_fin double precision,
    nro_clientes_fin double precision,
    monto_otrogado_fin double precision,
    interes_otrogado_fin double precision,
    redondeo_otrogado_fin double precision,
    monto_saldo_fin double precision,
    interes_saldo_fin double precision,
    redondeo_saldo_fin double precision,
    monto_cobrado_fin double precision,
    interes_cobrado_fin double precision,
    redondeo_cobrado_fin double precision,
    monto_vencido_fin double precision,
    porcentaje_fin double precision,
    mora_cobrado double precision,
    noti_cobrado double precision,
    otros_cobrado double precision,
    colacar_meta double precision,
    saldo_meta double precision,
    monto_vencido_meta double precision,
    porcentaje_meta double precision,
    creditos_meta double precision,
    clientes_meta double precision,
    cumplimiento double precision,
    fecha_hora_cierre timestamp without time zone,
    id_usuario bigint,
    id_agencia integer
);


ALTER TABLE public.metas_personal OWNER TO postgres;

--
-- Name: metas_personal_id_metas_personal_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.metas_personal_id_metas_personal_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.metas_personal_id_metas_personal_seq OWNER TO postgres;

--
-- Name: metas_personal_id_metas_personal_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.metas_personal_id_metas_personal_seq OWNED BY public.metas_personal.id_metas_personal;


--
-- Name: movimiento; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.movimiento (
    id_movimiento bigint NOT NULL,
    id_libreta_ahorro bigint NOT NULL,
    id_cajera bigint NOT NULL,
    fecha_hora_movimiento timestamp without time zone,
    tipo_operacion character(10) NOT NULL,
    monto_movimiento double precision NOT NULL,
    interes_movimiento double precision,
    redondeo double precision,
    obs text,
    id_agencia bigint,
    agencia character(30),
    puntos bigint,
    id_caja_apertura_cierre bigint,
    cajera character(20),
    estado integer DEFAULT 1 NOT NULL,
    obs_cancelacion text,
    concepto character varying(100)
);


ALTER TABLE public.movimiento OWNER TO postgres;

--
-- Name: movimiento_id_movimiento_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.movimiento_id_movimiento_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.movimiento_id_movimiento_seq OWNER TO postgres;

--
-- Name: movimiento_id_movimiento_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.movimiento_id_movimiento_seq OWNED BY public.movimiento.id_movimiento;


--
-- Name: movimiento_usuario; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.movimiento_usuario (
    id_movimiento_usuario bigint NOT NULL,
    id_usuario bigint NOT NULL,
    id_cajera bigint NOT NULL,
    fecha_hora_movimiento timestamp without time zone,
    operacion character(20) NOT NULL,
    monto_operacion double precision NOT NULL,
    sustento text,
    agencia character varying(250)
);


ALTER TABLE public.movimiento_usuario OWNER TO postgres;

--
-- Name: movimiento_usuario_id_movimiento_usuario_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.movimiento_usuario_id_movimiento_usuario_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.movimiento_usuario_id_movimiento_usuario_seq OWNER TO postgres;

--
-- Name: movimiento_usuario_id_movimiento_usuario_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.movimiento_usuario_id_movimiento_usuario_seq OWNED BY public.movimiento_usuario.id_movimiento_usuario;


--
-- Name: negocio; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.negocio (
    id_negocio bigint NOT NULL,
    dni character(8) NOT NULL,
    nombre_actividad character varying(50) NOT NULL,
    ciiu character varying(120),
    distrito character varying(50) NOT NULL,
    provincia character varying(50) NOT NULL,
    departamento character varying(50) NOT NULL,
    direccion character varying(100) NOT NULL,
    referencia text NOT NULL,
    valorizacion real,
    obs text,
    estado_negocio character(1),
    id_agencia integer,
    obs_modificacion text
);


ALTER TABLE public.negocio OWNER TO postgres;

--
-- Name: negocio_id_negocio_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.negocio_id_negocio_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.negocio_id_negocio_seq OWNER TO postgres;

--
-- Name: negocio_id_negocio_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.negocio_id_negocio_seq OWNED BY public.negocio.id_negocio;


--
-- Name: nivel_usuario; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.nivel_usuario (
    id_nivel_usuario integer NOT NULL,
    funcion character varying(50),
    funciones text
);


ALTER TABLE public.nivel_usuario OWNER TO postgres;

--
-- Name: nivel_usuario_id_nivel_usuario_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.nivel_usuario_id_nivel_usuario_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.nivel_usuario_id_nivel_usuario_seq OWNER TO postgres;

--
-- Name: nivel_usuario_id_nivel_usuario_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.nivel_usuario_id_nivel_usuario_seq OWNED BY public.nivel_usuario.id_nivel_usuario;


--
-- Name: notificaciones; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.notificaciones (
    id_notificaciones bigint NOT NULL,
    id_solicitud bigint NOT NULL,
    monto_vencido double precision NOT NULL,
    dias_retraso bigint NOT NULL,
    id_analista bigint NOT NULL,
    id_tipo_notifi bigint NOT NULL,
    fecha_gestion date NOT NULL,
    costo double precision NOT NULL,
    accion character varying(120),
    fecha_accion timestamp without time zone,
    respuesta character varying(520),
    observacion character varying(520),
    usuario bigint,
    telefono character varying(520),
    tipo_direccion character varying(120),
    direcccion character varying(520),
    dni character(8),
    id_agencia integer
);


ALTER TABLE public.notificaciones OWNER TO postgres;

--
-- Name: notificaciones_id_notificaciones_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.notificaciones_id_notificaciones_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.notificaciones_id_notificaciones_seq OWNER TO postgres;

--
-- Name: notificaciones_id_notificaciones_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.notificaciones_id_notificaciones_seq OWNED BY public.notificaciones.id_notificaciones;


--
-- Name: operacion; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.operacion (
    id_operacion bigint NOT NULL,
    id_operacion_pago bigint NOT NULL,
    id_solicitud bigint NOT NULL,
    id_cajera integer,
    id_impresion bigint NOT NULL,
    fecha_hora timestamp without time zone NOT NULL,
    concepto character varying(80) NOT NULL,
    monto real NOT NULL,
    de_cuota integer,
    a_cuota integer,
    id_analista integer,
    id_agencia integer,
    capital real,
    interes real,
    redondeo real
);


ALTER TABLE public.operacion OWNER TO postgres;

--
-- Name: operacion_banco; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.operacion_banco (
    id_operacion_banco bigint NOT NULL,
    id_banco bigint NOT NULL,
    operacion character varying(80) NOT NULL,
    monto real NOT NULL,
    fecha_hora timestamp without time zone NOT NULL,
    usuario character varying(80) NOT NULL,
    agencia character varying(80) NOT NULL,
    cod_apertura integer,
    obs character varying(500) NOT NULL
);


ALTER TABLE public.operacion_banco OWNER TO postgres;

--
-- Name: operacion_banco_id_operacion_banco_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.operacion_banco_id_operacion_banco_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.operacion_banco_id_operacion_banco_seq OWNER TO postgres;

--
-- Name: operacion_banco_id_operacion_banco_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.operacion_banco_id_operacion_banco_seq OWNED BY public.operacion_banco.id_operacion_banco;


--
-- Name: operacion_id_operacion_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.operacion_id_operacion_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.operacion_id_operacion_seq OWNER TO postgres;

--
-- Name: operacion_id_operacion_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.operacion_id_operacion_seq OWNED BY public.operacion.id_operacion;


--
-- Name: operacion_pago; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.operacion_pago (
    id_operacion_pago bigint NOT NULL,
    id_caja_apertura_cierre bigint NOT NULL,
    fecha_hora timestamp without time zone NOT NULL,
    total_pagar real,
    pago_con real,
    vuelto real,
    agencia character(30),
    puntos bigint,
    id_agencia bigint,
    obs_cancelacion text
);


ALTER TABLE public.operacion_pago OWNER TO postgres;

--
-- Name: operacion_pago_id_operacion_pago_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.operacion_pago_id_operacion_pago_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.operacion_pago_id_operacion_pago_seq OWNER TO postgres;

--
-- Name: operacion_pago_id_operacion_pago_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.operacion_pago_id_operacion_pago_seq OWNED BY public.operacion_pago.id_operacion_pago;


--
-- Name: pariente; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.pariente (
    id_pariente integer NOT NULL,
    dniuno character(8),
    nombreuno character varying(50),
    parentescouno character varying(20),
    dnidos character(8),
    nombredos character varying(50),
    parentescodos character varying(20),
    estado character varying(20)
);


ALTER TABLE public.pariente OWNER TO postgres;

--
-- Name: pariente_id_pariente_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.pariente_id_pariente_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.pariente_id_pariente_seq OWNER TO postgres;

--
-- Name: pariente_id_pariente_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.pariente_id_pariente_seq OWNED BY public.pariente.id_pariente;


--
-- Name: planilla; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.planilla (
    id_planilla bigint NOT NULL,
    cod_generacion integer NOT NULL,
    cod_aprobacion integer,
    cod_planilla integer,
    fecha_hora timestamp without time zone NOT NULL,
    personal character varying(50) NOT NULL,
    cargo character varying(50) NOT NULL,
    dni character(8),
    sueldo real NOT NULL,
    incentivos double precision,
    bonificacion double precision,
    faltantes double precision,
    movil double precision,
    tardanza double precision,
    adelantos double precision,
    uniforme double precision,
    permisos double precision,
    sanciones double precision,
    otros double precision,
    aporte_afp double precision,
    aporte_onp double precision,
    desc_total double precision,
    neto_pagar double precision,
    estado boolean,
    oficina character varying(50) NOT NULL,
    fecha_hora_ejecutado time with time zone
);


ALTER TABLE public.planilla OWNER TO postgres;

--
-- Name: planilla_id_planilla_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.planilla_id_planilla_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.planilla_id_planilla_seq OWNER TO postgres;

--
-- Name: planilla_id_planilla_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.planilla_id_planilla_seq OWNED BY public.planilla.id_planilla;


--
-- Name: provincia; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.provincia (
    id_provincia integer NOT NULL,
    id_departamento integer NOT NULL,
    provincia character varying(50) NOT NULL
);


ALTER TABLE public.provincia OWNER TO postgres;

--
-- Name: provincia_id_departamento_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.provincia_id_departamento_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.provincia_id_departamento_seq OWNER TO postgres;

--
-- Name: provincia_id_departamento_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.provincia_id_departamento_seq OWNED BY public.provincia.id_departamento;


--
-- Name: provincia_id_provincia_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.provincia_id_provincia_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.provincia_id_provincia_seq OWNER TO postgres;

--
-- Name: provincia_id_provincia_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.provincia_id_provincia_seq OWNED BY public.provincia.id_provincia;


--
-- Name: rpt_cobranza_diaria; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.rpt_cobranza_diaria (
    id_rpt_cobranza_diaria bigint NOT NULL,
    fecha_hora timestamp without time zone,
    id_apertura integer NOT NULL,
    nombre_user character varying(120) NOT NULL,
    monto_inicio_total double precision,
    monto_inicio_hoy double precision,
    monto_fin_total double precision,
    interes_fin_hoy double precision,
    creditos_total double precision,
    creditos_hoy double precision,
    id_agencia integer
);


ALTER TABLE public.rpt_cobranza_diaria OWNER TO postgres;

--
-- Name: rpt_cobranza_diaria_id_rpt_cobranza_diaria_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.rpt_cobranza_diaria_id_rpt_cobranza_diaria_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.rpt_cobranza_diaria_id_rpt_cobranza_diaria_seq OWNER TO postgres;

--
-- Name: rpt_cobranza_diaria_id_rpt_cobranza_diaria_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.rpt_cobranza_diaria_id_rpt_cobranza_diaria_seq OWNED BY public.rpt_cobranza_diaria.id_rpt_cobranza_diaria;


--
-- Name: rpt_creditos_colocados_historial; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.rpt_creditos_colocados_historial (
    id_rpt_creditos_colocados_historial bigint NOT NULL,
    fecha_hora timestamp without time zone,
    id_analista integer NOT NULL,
    nombre_analista character varying(120) NOT NULL,
    nro_creditos double precision,
    nro_clientes double precision,
    monto_otrogado double precision,
    interes_otrogado double precision,
    redondeo_otrogado double precision,
    monto_saldo double precision,
    interes_saldo double precision,
    redondeo_saldo double precision,
    monto_cobrado double precision,
    interes_cobrado double precision,
    redondeo_cobrado double precision,
    diario_1_7 double precision,
    diario_8_30 double precision,
    diario_mas_30 double precision,
    total_diario double precision,
    semanal_1_7 double precision,
    semanal_8_30 double precision,
    semanal_mas_30 double precision,
    total_semanal double precision,
    total_general double precision,
    id_agencia integer
);


ALTER TABLE public.rpt_creditos_colocados_historial OWNER TO postgres;

--
-- Name: rpt_creditos_colocados_histor_id_rpt_creditos_colocados_his_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.rpt_creditos_colocados_histor_id_rpt_creditos_colocados_his_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.rpt_creditos_colocados_histor_id_rpt_creditos_colocados_his_seq OWNER TO postgres;

--
-- Name: rpt_creditos_colocados_histor_id_rpt_creditos_colocados_his_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.rpt_creditos_colocados_histor_id_rpt_creditos_colocados_his_seq OWNED BY public.rpt_creditos_colocados_historial.id_rpt_creditos_colocados_historial;


--
-- Name: rpt_movimientos_historial; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.rpt_movimientos_historial (
    id_rpt_movimientos_historial bigint NOT NULL,
    fecha_hora timestamp without time zone,
    nro_creditos double precision,
    nro_clientes double precision,
    monto_otrogado double precision,
    interes_otrogado double precision,
    redondeo_otrogado double precision,
    monto_saldo double precision,
    interes_saldo double precision,
    redondeo_saldo double precision,
    monto_cobrado double precision,
    interes_cobrado double precision,
    redondeo_cobrado double precision,
    monto_vencido double precision,
    porcentaje double precision,
    mora_generadas double precision,
    mora_pagada double precision,
    mora_saldo double precision,
    noti_generadas double precision,
    noti_pagada double precision,
    noti_saldo double precision,
    otros_generadas double precision,
    otros_pagada double precision,
    otros_saldo double precision,
    id_agencia integer
);


ALTER TABLE public.rpt_movimientos_historial OWNER TO postgres;

--
-- Name: sectores; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sectores (
    id_sector integer NOT NULL,
    sector character varying(50) NOT NULL,
    nombre character varying(50) NOT NULL,
    id_analista integer NOT NULL,
    obs text
);


ALTER TABLE public.sectores OWNER TO postgres;

--
-- Name: sectores_id_sector_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.sectores_id_sector_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sectores_id_sector_seq OWNER TO postgres;

--
-- Name: sectores_id_sector_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.sectores_id_sector_seq OWNED BY public.sectores.id_sector;


--
-- Name: solicitud_credito; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.solicitud_credito (
    id_solicitud bigint NOT NULL,
    nombre_analista character varying(50) NOT NULL,
    id_analista integer NOT NULL,
    id_administrador integer,
    fecha_hora_solicitud timestamp without time zone NOT NULL,
    tipo_credito character varying(70) NOT NULL,
    producto_credito character varying(70) NOT NULL,
    dni character(8) NOT NULL,
    id_conyuge bigint,
    tipo_garantia_cliente character varying(70) NOT NULL,
    descripcion_garantia_cliente character varying(120),
    valorizacion_garantia_cliente real,
    id_cliente_aval bigint,
    tipo_garantia_aval character varying(70) NOT NULL,
    descripcion_garantia_aval character varying(120),
    valorizacion_garantia_aval real,
    id_negocio bigint,
    interes real,
    monto_interes double precision,
    periodo smallint NOT NULL,
    meses smallint NOT NULL,
    penalidad double precision,
    fecha_inicio_pago date NOT NULL,
    monto_prestado double precision NOT NULL,
    nro_cuotas integer NOT NULL,
    redondeo_total double precision,
    fecha_finalizacion date NOT NULL,
    obs text,
    obs_internas text,
    estado smallint NOT NULL,
    nro_credito character(16),
    con_boleta boolean,
    nro_boleta character(15),
    fecha_hora_desembolso timestamp without time zone,
    id_cajera integer,
    estado_credito smallint,
    capital_x_cuota double precision,
    interes_x_cuota double precision,
    redondeo_x_cuota double precision,
    cuota_a_pagar double precision,
    capital_x_pagar double precision,
    interes_x_pagar double precision,
    redondeo_x_pagar double precision,
    penalidad_x_pagar double precision,
    cuotas_x_pagar integer,
    monto_x_pagar double precision,
    cuotas_vencidas integer,
    monto_vencido double precision,
    moras double precision,
    moras_pagadas double precision,
    notificaciones double precision,
    notificaciones_pagadas double precision,
    otros_pagos double precision,
    otros_pagos_pagadas double precision,
    ult_dia_pago timestamp without time zone,
    id_admin_condonacion bigint,
    fecha_hora_condonacion timestamp without time zone,
    monto_condonacion double precision,
    detalle_condonacion text,
    descuento_precancelacion double precision,
    id_ficha integer,
    dias smallint,
    noti_acomulada character varying(2),
    oficina character(50),
    calificacion smallint,
    area character(2),
    fecha_cierre date,
    contrato text,
    agencia character varying(30),
    penalidades_pagadas double precision,
    hora_pagar time with time zone,
    pago_en character(10),
    obs_negacion text,
    id_caja_apertura_cierre bigint,
    serie integer
);


ALTER TABLE public.solicitud_credito OWNER TO postgres;

--
-- Name: solicitud_credito_id_solicitud_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.solicitud_credito_id_solicitud_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.solicitud_credito_id_solicitud_seq OWNER TO postgres;

--
-- Name: solicitud_credito_id_solicitud_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.solicitud_credito_id_solicitud_seq OWNED BY public.solicitud_credito.id_solicitud;


--
-- Name: sueldo_movimiento; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sueldo_movimiento (
    id_sueldo_movimiento bigint NOT NULL,
    id_personal integer NOT NULL,
    detalle_sueldo character varying(50) NOT NULL,
    monto real NOT NULL,
    fecha_hora timestamp without time zone NOT NULL,
    estado boolean,
    oficina character varying(50) NOT NULL,
    cajera character varying(50) NOT NULL,
    dni character(8),
    obs text,
    fecha_hora_ejecutado time with time zone,
    agencia character(30),
    cod_cierre integer,
    detalle_devolucion character varying(250),
    fecha_hora_devuelto timestamp without time zone,
    user_devolucion character varying(50),
    monto_devuelto real DEFAULT 0 NOT NULL,
    obs_cancelacion text,
    id_agencia integer
);


ALTER TABLE public.sueldo_movimiento OWNER TO postgres;

--
-- Name: sueldo_movimiento_id_sueldo_movimiento_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.sueldo_movimiento_id_sueldo_movimiento_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sueldo_movimiento_id_sueldo_movimiento_seq OWNER TO postgres;

--
-- Name: sueldo_movimiento_id_sueldo_movimiento_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.sueldo_movimiento_id_sueldo_movimiento_seq OWNED BY public.sueldo_movimiento.id_sueldo_movimiento;


--
-- Name: tipo_ahorro; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tipo_ahorro (
    id_tipo_ahorro bigint NOT NULL,
    nombre_ahorro character varying(50) NOT NULL,
    obs text
);


ALTER TABLE public.tipo_ahorro OWNER TO postgres;

--
-- Name: tipo_ahorro_id_tipo_ahorro_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tipo_ahorro_id_tipo_ahorro_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tipo_ahorro_id_tipo_ahorro_seq OWNER TO postgres;

--
-- Name: tipo_ahorro_id_tipo_ahorro_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tipo_ahorro_id_tipo_ahorro_seq OWNED BY public.tipo_ahorro.id_tipo_ahorro;


--
-- Name: tipo_notificacion; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tipo_notificacion (
    id_tipo_notifi bigint NOT NULL,
    nombre_notificacion character varying(50) NOT NULL,
    obs text,
    valor real
);


ALTER TABLE public.tipo_notificacion OWNER TO postgres;

--
-- Name: tipo_notificacion_id_tipo_notifi_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tipo_notificacion_id_tipo_notifi_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tipo_notificacion_id_tipo_notifi_seq OWNER TO postgres;

--
-- Name: tipo_notificacion_id_tipo_notifi_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tipo_notificacion_id_tipo_notifi_seq OWNED BY public.tipo_notificacion.id_tipo_notifi;


--
-- Name: usuario; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.usuario (
    id_usuario integer NOT NULL,
    apellidos character varying(50) NOT NULL,
    nombres character varying(50) NOT NULL,
    tipo_usuario integer NOT NULL,
    direccion character varying(100),
    telefono character varying(100),
    email character varying(150),
    login character varying(50),
    pass character varying(50),
    obs text,
    activado boolean DEFAULT true NOT NULL,
    salario double precision,
    salario_actual double precision,
    agencia bigint,
    dni character(8),
    ingreso_temprano time with time zone,
    ingreso_tarde time with time zone,
    ingreso_sabados timestamp without time zone,
    estado_sueldo boolean DEFAULT false NOT NULL,
    fecha_ingreso date,
    movil real,
    uniforme real,
    aporte_afp real,
    aporte_onp real,
    id_agencia integer DEFAULT 1,
    creacion timestamp without time zone,
    cargo character varying(100)
);


ALTER TABLE public.usuario OWNER TO postgres;

--
-- Name: usuario_id_usuario_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.usuario_id_usuario_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.usuario_id_usuario_seq OWNER TO postgres;

--
-- Name: usuario_id_usuario_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.usuario_id_usuario_seq OWNED BY public.usuario.id_usuario;


--
-- Name: activar_reportes id_activar_rpt; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.activar_reportes ALTER COLUMN id_activar_rpt SET DEFAULT nextval('public.activar_reportes_id_activar_rpt_seq'::regclass);


--
-- Name: agencia id_agencia; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.agencia ALTER COLUMN id_agencia SET DEFAULT nextval('public.agencia_id_agencia_seq'::regclass);


--
-- Name: agencia id_empresa; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.agencia ALTER COLUMN id_empresa SET DEFAULT nextval('public.agencia_id_empresa_seq'::regclass);


--
-- Name: apertura_cierre id_caja_apertura_cierre; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.apertura_cierre ALTER COLUMN id_caja_apertura_cierre SET DEFAULT nextval('public.apertura_cierre_id_caja_apertura_cierre_seq'::regclass);


--
-- Name: aportaciones id_aportaciones; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.aportaciones ALTER COLUMN id_aportaciones SET DEFAULT nextval('public.aportaciones_id_aportaciones_seq'::regclass);


--
-- Name: banco id_banco; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.banco ALTER COLUMN id_banco SET DEFAULT nextval('public.banco_id_banco_seq'::regclass);


--
-- Name: billeteo id_billeteo; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.billeteo ALTER COLUMN id_billeteo SET DEFAULT nextval('public.billeteo_id_billeteo_seq'::regclass);


--
-- Name: boleta id_boleta; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.boleta ALTER COLUMN id_boleta SET DEFAULT nextval('public.boleta_id_boleta_seq'::regclass);


--
-- Name: boveda id_boveda; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.boveda ALTER COLUMN id_boveda SET DEFAULT nextval('public.boveda_id_boveda_seq'::regclass);


--
-- Name: campana id_campana; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.campana ALTER COLUMN id_campana SET DEFAULT nextval('public.campana_id_campana_seq'::regclass);


--
-- Name: central_riesgo id_central_riesgo; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.central_riesgo ALTER COLUMN id_central_riesgo SET DEFAULT nextval('public.central_riesgo_id_central_riesgo_seq'::regclass);


--
-- Name: cierre_caja_principal id_cierre_principal; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cierre_caja_principal ALTER COLUMN id_cierre_principal SET DEFAULT nextval('public.cierre_caja_principal_id_cierre_principal_seq'::regclass);


--
-- Name: cierre_planilla id_cierre_planilla; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cierre_planilla ALTER COLUMN id_cierre_planilla SET DEFAULT nextval('public.cierre_planilla_id_cierre_planilla_seq'::regclass);


--
-- Name: clave_cliente id_clave_cliente; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.clave_cliente ALTER COLUMN id_clave_cliente SET DEFAULT nextval('public.clave_cliente_id_clave_cliente_seq'::regclass);


--
-- Name: cliente_aval id_cliente_aval; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cliente_aval ALTER COLUMN id_cliente_aval SET DEFAULT nextval('public.cliente_aval_id_cliente_aval_seq'::regclass);


--
-- Name: cliente_beneficiario id_cliente_beneficiario; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cliente_beneficiario ALTER COLUMN id_cliente_beneficiario SET DEFAULT nextval('public.cliente_beneficiario_id_cliente_beneficiario_seq'::regclass);


--
-- Name: compromiso id_compromiso; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.compromiso ALTER COLUMN id_compromiso SET DEFAULT nextval('public.compromiso_id_compromiso_seq'::regclass);


--
-- Name: conyuge id_conyuge; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.conyuge ALTER COLUMN id_conyuge SET DEFAULT nextval('public.conyuge_id_conyuge_seq'::regclass);


--
-- Name: credito_refinanciado id_refinanciado; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.credito_refinanciado ALTER COLUMN id_refinanciado SET DEFAULT nextval('public.credito_refinanciado_id_refinanciado_seq'::regclass);


--
-- Name: cronograma_ahorro id_cronograma_ahorro; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cronograma_ahorro ALTER COLUMN id_cronograma_ahorro SET DEFAULT nextval('public.cronograma_ahorro_id_cronograma_ahorro_seq'::regclass);


--
-- Name: cronograma_pagos id_cronograma_pago; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cronograma_pagos ALTER COLUMN id_cronograma_pago SET DEFAULT nextval('public.cronograma_pagos_id_cronograma_pago_seq'::regclass);


--
-- Name: departamento id_departamento; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.departamento ALTER COLUMN id_departamento SET DEFAULT nextval('public.departamento_id_departamento_seq'::regclass);


--
-- Name: detalle_negocio id_detalle_negocio; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.detalle_negocio ALTER COLUMN id_detalle_negocio SET DEFAULT nextval('public.detalle_negocio_id_detalle_negocio_seq'::regclass);


--
-- Name: detalle_negocio id_negocio; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.detalle_negocio ALTER COLUMN id_negocio SET DEFAULT nextval('public.detalle_negocio_id_negocio_seq'::regclass);


--
-- Name: dias_festivos id_dia_festivo; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.dias_festivos ALTER COLUMN id_dia_festivo SET DEFAULT nextval('public.dias_festivos_id_dia_festivo_seq'::regclass);


--
-- Name: dias_festivos id_agencia; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.dias_festivos ALTER COLUMN id_agencia SET DEFAULT nextval('public.dias_festivos_id_agencia_seq'::regclass);


--
-- Name: distrito id_distrito; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.distrito ALTER COLUMN id_distrito SET DEFAULT nextval('public.distrito_id_distrito_seq'::regclass);


--
-- Name: distrito id_provincia; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.distrito ALTER COLUMN id_provincia SET DEFAULT nextval('public.distrito_id_provincia_seq'::regclass);


--
-- Name: empresa id_empresa; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.empresa ALTER COLUMN id_empresa SET DEFAULT nextval('public.empresa_id_empresa_seq'::regclass);


--
-- Name: equipos id_aquipos; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.equipos ALTER COLUMN id_aquipos SET DEFAULT nextval('public.equipos_id_aquipos_seq'::regclass);


--
-- Name: equipos id_agencia; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.equipos ALTER COLUMN id_agencia SET DEFAULT nextval('public.equipos_id_agencia_seq'::regclass);


--
-- Name: ficha_domiciliaria id_ficha_domiciliaria; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ficha_domiciliaria ALTER COLUMN id_ficha_domiciliaria SET DEFAULT nextval('public.ficha_domiciliaria_id_ficha_domiciliaria_seq'::regclass);


--
-- Name: ficha_economica id_ficha; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ficha_economica ALTER COLUMN id_ficha SET DEFAULT nextval('public.ficha_economica_id_ficha_seq'::regclass);


--
-- Name: gastos_operativos id_gastos_operativos; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.gastos_operativos ALTER COLUMN id_gastos_operativos SET DEFAULT nextval('public.gastos_operativos_id_gastos_operativos_seq'::regclass);


--
-- Name: gastos_operativos id_negocio; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.gastos_operativos ALTER COLUMN id_negocio SET DEFAULT nextval('public.gastos_operativos_id_negocio_seq'::regclass);


--
-- Name: historial_manipulacion id_historial_manipulacion; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.historial_manipulacion ALTER COLUMN id_historial_manipulacion SET DEFAULT nextval('public.historial_manipulacion_id_historial_manipulacion_seq'::regclass);


--
-- Name: historial_manipulacion id_usuario; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.historial_manipulacion ALTER COLUMN id_usuario SET DEFAULT nextval('public.historial_manipulacion_id_usuario_seq'::regclass);


--
-- Name: historial_visitas id_historial_visitas; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.historial_visitas ALTER COLUMN id_historial_visitas SET DEFAULT nextval('public.historial_visitas_id_historial_visitas_seq'::regclass);


--
-- Name: ingreso_egreso id_ingreso_egreso; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ingreso_egreso ALTER COLUMN id_ingreso_egreso SET DEFAULT nextval('public.ingreso_egreso_id_ingreso_egreso_seq'::regclass);


--
-- Name: insumo_negocio id_insumo_negocio; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.insumo_negocio ALTER COLUMN id_insumo_negocio SET DEFAULT nextval('public.insumo_negocio_id_insumo_negocio_seq'::regclass);


--
-- Name: insumo_negocio id_detalle_negocio; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.insumo_negocio ALTER COLUMN id_detalle_negocio SET DEFAULT nextval('public.insumo_negocio_id_detalle_negocio_seq'::regclass);


--
-- Name: inventario_bienes id_inventario_bienes; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.inventario_bienes ALTER COLUMN id_inventario_bienes SET DEFAULT nextval('public.inventario_bienes_id_inventario_bienes_seq'::regclass);


--
-- Name: libreta_ahorro id_libreta_ahorro; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.libreta_ahorro ALTER COLUMN id_libreta_ahorro SET DEFAULT nextval('public.libreta_ahorro_id_libreta_ahorro_seq'::regclass);


--
-- Name: metas_personal id_metas_personal; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.metas_personal ALTER COLUMN id_metas_personal SET DEFAULT nextval('public.metas_personal_id_metas_personal_seq'::regclass);


--
-- Name: movimiento id_movimiento; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.movimiento ALTER COLUMN id_movimiento SET DEFAULT nextval('public.movimiento_id_movimiento_seq'::regclass);


--
-- Name: movimiento_usuario id_movimiento_usuario; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.movimiento_usuario ALTER COLUMN id_movimiento_usuario SET DEFAULT nextval('public.movimiento_usuario_id_movimiento_usuario_seq'::regclass);


--
-- Name: negocio id_negocio; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.negocio ALTER COLUMN id_negocio SET DEFAULT nextval('public.negocio_id_negocio_seq'::regclass);


--
-- Name: nivel_usuario id_nivel_usuario; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.nivel_usuario ALTER COLUMN id_nivel_usuario SET DEFAULT nextval('public.nivel_usuario_id_nivel_usuario_seq'::regclass);


--
-- Name: notificaciones id_notificaciones; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.notificaciones ALTER COLUMN id_notificaciones SET DEFAULT nextval('public.notificaciones_id_notificaciones_seq'::regclass);


--
-- Name: operacion id_operacion; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.operacion ALTER COLUMN id_operacion SET DEFAULT nextval('public.operacion_id_operacion_seq'::regclass);


--
-- Name: operacion_banco id_operacion_banco; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.operacion_banco ALTER COLUMN id_operacion_banco SET DEFAULT nextval('public.operacion_banco_id_operacion_banco_seq'::regclass);


--
-- Name: operacion_pago id_operacion_pago; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.operacion_pago ALTER COLUMN id_operacion_pago SET DEFAULT nextval('public.operacion_pago_id_operacion_pago_seq'::regclass);


--
-- Name: pariente id_pariente; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.pariente ALTER COLUMN id_pariente SET DEFAULT nextval('public.pariente_id_pariente_seq'::regclass);


--
-- Name: planilla id_planilla; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.planilla ALTER COLUMN id_planilla SET DEFAULT nextval('public.planilla_id_planilla_seq'::regclass);


--
-- Name: provincia id_provincia; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.provincia ALTER COLUMN id_provincia SET DEFAULT nextval('public.provincia_id_provincia_seq'::regclass);


--
-- Name: provincia id_departamento; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.provincia ALTER COLUMN id_departamento SET DEFAULT nextval('public.provincia_id_departamento_seq'::regclass);


--
-- Name: rpt_cobranza_diaria id_rpt_cobranza_diaria; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.rpt_cobranza_diaria ALTER COLUMN id_rpt_cobranza_diaria SET DEFAULT nextval('public.rpt_cobranza_diaria_id_rpt_cobranza_diaria_seq'::regclass);


--
-- Name: rpt_creditos_colocados_historial id_rpt_creditos_colocados_historial; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.rpt_creditos_colocados_historial ALTER COLUMN id_rpt_creditos_colocados_historial SET DEFAULT nextval('public.rpt_creditos_colocados_histor_id_rpt_creditos_colocados_his_seq'::regclass);


--
-- Name: sectores id_sector; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sectores ALTER COLUMN id_sector SET DEFAULT nextval('public.sectores_id_sector_seq'::regclass);


--
-- Name: solicitud_credito id_solicitud; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.solicitud_credito ALTER COLUMN id_solicitud SET DEFAULT nextval('public.solicitud_credito_id_solicitud_seq'::regclass);


--
-- Name: sueldo_movimiento id_sueldo_movimiento; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sueldo_movimiento ALTER COLUMN id_sueldo_movimiento SET DEFAULT nextval('public.sueldo_movimiento_id_sueldo_movimiento_seq'::regclass);


--
-- Name: tipo_ahorro id_tipo_ahorro; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tipo_ahorro ALTER COLUMN id_tipo_ahorro SET DEFAULT nextval('public.tipo_ahorro_id_tipo_ahorro_seq'::regclass);


--
-- Name: tipo_notificacion id_tipo_notifi; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tipo_notificacion ALTER COLUMN id_tipo_notifi SET DEFAULT nextval('public.tipo_notificacion_id_tipo_notifi_seq'::regclass);


--
-- Name: usuario id_usuario; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.usuario ALTER COLUMN id_usuario SET DEFAULT nextval('public.usuario_id_usuario_seq'::regclass);


--
-- Data for Name: activar_reportes; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.activar_reportes (id_activar_rpt, rpt_cobranza) FROM stdin;
\.


--
-- Data for Name: agencia; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.agencia (id_agencia, id_empresa, nombre_agencia, direccion_agencia, telefono_agencia, codigo, mensaje) FROM stdin;
1	1	HUANCAYO	Jr. Mariscal Castilla N1855 - El Tambo	(064) 764919	1  	**GRACIAS POR SU PREFERENCIA**
2	1	CONCEPCION	Jr. Huancayo N 289	(064) 404425	2  	**GRACIAS POR SU PREFERENCIA**
3	1	PICHANAKI	Jr. Andres Avelino Caceres N 658	00000000	3  	**GRACIAS POR SU PREFERENCIA**
\.


--
-- Data for Name: apertura_cierre; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.apertura_cierre (id_caja_apertura_cierre, equipo_registrado, id_usuario_apertura, fecha_hora_aperturado, monto_aperturado, obs_aperturado, id_usuario_cierre, fecha_hora_cierre, monto_cierre, obs_cierre, total_billetes, total_monedas, total_general, total_otros_ingresos, total_ingreso_ahorro, total_cobrado, total_ingresos, total_sub_egresos, total_retiros, total_desembolsado, total_egresos, total_caja, sobra_falta_caja, cerrado_principal, id_agencia) FROM stdin;
3856	PRINCIPAL01	4	2020-07-07 02:07:33	500		\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	2
\.


--
-- Data for Name: aportaciones; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.aportaciones (id_aportaciones, id_caja_apertura_cierre, tipo_operacion, dni, fecha_hora, total_pagar, agencia, puntos, id_agencia) FROM stdin;
\.


--
-- Data for Name: banco; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.banco (id_banco, entidad, cuenta_nro, fecha_hora_operacion, usuario, obs, monto_actual) FROM stdin;
\.


--
-- Data for Name: billeteo; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.billeteo (id_billeteo, id_caja_apertura_cierre, denominacion, cantidad, monto) FROM stdin;
\.


--
-- Data for Name: boleta; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.boleta (id_boleta, serie, inicio_boleta, id_agencia) FROM stdin;
1	1	333	\N
\.


--
-- Data for Name: boveda; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.boveda (id_boveda, monto_actual_boveda, equipo_registrado, fecha_hora_operacion, tipo, monto_movimiento, usuario, motivo, obs, usuario_habilita, cod_habiltacion, confirmar, agencia, id_agencia) FROM stdin;
6	6000	DESKTOP-F8PPP5L	2020-08-06 14:03:13	DEPOSITO	6000	JHUAMAN	APORTE A BOVEDA	ingreso para desembolsos 	\N	\N	t	HUANCAYO	1
\.


--
-- Data for Name: campana; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.campana (id_campana, name_campana, monto_campana, inicio_capana, fin_capana, id_personal, obs, id_agencia) FROM stdin;
1	CANASTA 2019	370	2019-07-15 00:00:00	2019-12-25 00:00:00	1	N	\N
\.


--
-- Data for Name: central_riesgo; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.central_riesgo (id_central_riesgo, dni, entidad, monto, calificacion, fecha, id_usuario, id_agencia) FROM stdin;
\.


--
-- Data for Name: cierre_caja_principal; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.cierre_caja_principal (id_cierre_principal, id_usuario, fecha_cierre, fecha_hora, obs, monto_cierre, id_agencia) FROM stdin;
\.


--
-- Data for Name: cierre_planilla; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.cierre_planilla (id_cierre_planilla, monto_cierre, fecha_hora_planilla, login, agencia, id_agencia) FROM stdin;
\.


--
-- Data for Name: ciiu; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.ciiu (id_ciiu, cod_ciiu, descripcion) FROM stdin;
\.


--
-- Data for Name: clave_cliente; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.clave_cliente (id_clave_cliente, dni_cliente, clave) FROM stdin;
\.


--
-- Data for Name: cliente; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.cliente (dni, paterno, materno, nombres, sexo, estado_civil, fecha_nacimiento, telefonos, e_mail, distrito, provincia, departamento, direccion, referencia, obs, fecha_hora_creacion, nro_oficina, nro_expediente, fecha_hora_expediente, id_analista, nombre_promotor, id_plataforma, historial, nro_ahorro, calificacion, pass_ahorro, nro_expediente_dos, estado_cliente, usuario, castigar_cliente, motivo_castigo, user_castigo, fecha_hora_castigo, puntos, codsocio, aportado, tiposocio, id_registro_user, id_user_exp, id_agencia, fecha_hora_exp_ahorro) FROM stdin;
20413205	HINOSTROZA	HURTADO	MARINA IRAYDA	FEMENINO  	CONVIVIENTE	1979-06-29	954645235		Nueve De Julio	Concepción	Junin	Jr. Manuel Prado S/N	Frente Al Mercado Central		2020-07-07 13:38:20.672846	CONCEPCIÓN                                        	1	2020-07-07 13:39:36	3	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	3	3	2	\N
47766965	EFFIO 	MARQUEZ	STEPHANIE ANGGELINA	FEMENINO  	SOLTERO(A)	1992-05-06	963500166		Huancayo	Huancayo	Junin	Jr. Cajamarca  # 687	Por La Discoteka Insomio - O Sedam Huancayo	Cliente Con Ingresos Como Trabajadora En El Minsa Lima Con Contrato Y Cuenta Con Aval Que Es Su Madre Con Casa Propia	2020-07-15 09:41:26.239456	CONCEPCIÓN                                        	2	2020-07-15 09:43:58	5	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2	2	2	\N
43532534	ESPINOZA 	DE LA CRUZ	CARMEN	FEMENINO  	CONVIVIENTE	1986-03-26	913045376		Concepciòn	Concepción	Junin	Psj. San Antonio S/N 	Por La Capilla San Antonio 	Conyuge No Firma Por Motivos De Trabajo	2020-07-23 15:38:49.001596	CONCEPCIÓN                                        	3	2020-07-23 15:40:31	3	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2	2	2	\N
41332133	AQUINO	QUISPE	MIRKO TOLENTINO	MASCULINO 	SOLTERO(A)	1982-03-26	955744215		Pichanaqui	Chanchamayo	Junin	Pj. Hector Chumpitaz Ñ3-01Aahh Jose Carlos Mareategui	A Dos Cuadras De La Loza De Mareategui		2020-07-27 17:13:20.836846	CONCEPCIÓN                                        	4	2020-07-27 17:53:21	7	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	7	7	2	\N
47506173	CLEMENTE 	HILARIO	KATHERIN YURI 	FEMENINO  	SEPARADO(A)	1993-01-05	978006552	YURI_12@HOTMAIL.COM	San Agustín De Cajas	Huancayo	Junin	Jr. Mariscal De Sucre  S/N	A Altura Del Cementerio De San Agustin De Cajas	Cliente Cuenta Con Casa De Material Noble De Dos Pisos, Cuenta Con Mototaxi Pero A Nombre De Su Ex Pareja , Cuenta Con Pension De Su Ex Pareja 	2020-08-06 14:20:53.412443	HUANCAYO                                          	1	2020-08-06 14:21:58	5	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2	2	1	\N
41281404	SANTIVAÑEZ	DAVILA	LIZ ANGELA	FEMENINO  	CONVIVIENTE	1982-05-21	983643420	liz_angela@hotmail.com	San Jerónimo De Tunán	Huancayo	Junin	Jr. Catalina Huanca 198	A Una Cuadra Del Parque O Frente Al Colegio Sanabria	Cliente Cuenta Con Buena Ubicacion Del Negocio , Conyuge Se Dedica Al Transporte	2020-08-14 10:31:41.223653	CONCEPCIÓN                                        	5	2020-08-14 10:32:38	3	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	3	3	2	\N
43075285	CAINICELA	DE LA O 	NANCY ROSALVINA	FEMENINO  	SEPARADO(A)	1985-06-02	913027779	nancy_rosalvina@hotmail.com	Concepciòn	Concepción	Junin	Jr. Agricultura S/N	Por La Plazuela De Colegio Nueve De Julio	Cliente Cuenta Con Garantia De Artefacto Cocina De Cuatro Ornillas Indurama	2020-08-15 10:02:37.945605	CONCEPCIÓN                                        	6	2020-08-15 10:05:08	3	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	3	3	2	\N
20400482	CHAMORRO 	MUÑOZ	ANA MARIA	FEMENINO  	SOLTERO(A)	1961-07-25	922609611	ana_chamorro@hotmail.com	Concepciòn	Concepción	Junin	Carretera Central 1241	Por El Agente De Caja Piura 	Cliente Con Casa Propia Con Negocio De Verduras	2020-08-17 16:06:20.787577	CONCEPCIÓN                                        	7	2020-08-17 16:06:50	3	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	3	3	2	\N
48017310	CRESPIN	SANCHEZ	PATRICIA GISELA	MASCULINO 	SOLTERO(A)	2020-08-18	972385134	patriciagiselacrespinsanchez@gmail.com	Concepciòn	Concepción	Junin	Jr.Mariscal Castilla S/N	A 10 Cuadras Del Hotel Huaychulo	Cliente Con Casa Familiar	2020-08-18 11:38:23.635662	CONCEPCIÓN                                        	8	2020-08-18 11:38:50	3	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	3	3	2	\N
\.


--
-- Data for Name: cliente_aval; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.cliente_aval (id_cliente_aval, dni, aval, parentesco) FROM stdin;
\.


--
-- Data for Name: cliente_beneficiario; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.cliente_beneficiario (id_cliente_beneficiario, dni, beneficiario, parantesco, obs, nombre_beni) FROM stdin;
\.


--
-- Data for Name: compromiso; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.compromiso (id_compromiso, id_solicitud, tipo_compromiso, usuario, fecha_hora, fecha_compromiso, deuda_vencida, id_notificaciones, obs) FROM stdin;
\.


--
-- Data for Name: conyuge; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.conyuge (id_conyuge, dni, conyuge, estado, obs) FROM stdin;
\.


--
-- Data for Name: credito_refinanciado; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.credito_refinanciado (id_refinanciado, id_solicitud, nro_de_creditos, detalle_dreditos, fecha_hora_refinanciado, obs) FROM stdin;
\.


--
-- Data for Name: cronograma_ahorro; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.cronograma_ahorro (id_cronograma_ahorro, id_libreta_ahorro, pagado, monto_pactado, monto_descontado, nro_cuota, fecha_pactada, fecha_hora_pagado, monto_movimiento, fecha_hora_amortizacion, amortizado) FROM stdin;
\.


--
-- Data for Name: cronograma_pagos; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.cronograma_pagos (id_cronograma_pago, id_solicitud, pagado, capital_x_cuota, interes_x_cuota, redondeo_x_cuota, penalidad_x_cuota, cuota_pagar, ii, ga, cuota_descontada, nro_cuota, fecha_vencimiento, fecha_hora_pagado, monto_capital, monto_interes, monto_redondeo, mora, otros_pagos, notificacion, fecha_hora_amortizacion, amortizado, pago_con, id_operacion) FROM stdin;
1	1	f	10	0.810000000000000053	0	0	10.8000000000000007	3	3	f	1	2020-07-08	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
2	1	f	10	0.810000000000000053	0	0	10.8000000000000007	3	3	f	2	2020-07-09	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
3	1	f	10	0.810000000000000053	0	0	10.8000000000000007	3	3	f	3	2020-07-10	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
4	1	f	10	0.810000000000000053	0	0	10.8000000000000007	3	3	f	4	2020-07-11	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
5	1	f	10	0.810000000000000053	0	0	10.8000000000000007	3	3	f	5	2020-07-13	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
6	1	f	10	0.810000000000000053	0	0	10.8000000000000007	3	3	f	6	2020-07-14	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
7	1	f	10	0.810000000000000053	0	0	10.8000000000000007	3	3	f	7	2020-07-15	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
8	1	f	10	0.810000000000000053	0	0	10.8000000000000007	3	3	f	8	2020-07-16	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
9	1	f	10	0.810000000000000053	0	0	10.8000000000000007	3	3	f	9	2020-07-17	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
10	1	f	10	0.810000000000000053	0	0	10.8000000000000007	3	3	f	10	2020-07-18	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
11	1	f	10	0.810000000000000053	0	0	10.8000000000000007	3	3	f	11	2020-07-20	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
12	1	f	10	0.810000000000000053	0	0	10.8000000000000007	3	3	f	12	2020-07-21	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
13	1	f	10	0.810000000000000053	0	0	10.8000000000000007	3	3	f	13	2020-07-22	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
14	1	f	10	0.810000000000000053	0	0	10.8000000000000007	3	3	f	14	2020-07-23	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
15	1	f	10	0.810000000000000053	0	0	10.8000000000000007	3	3	f	15	2020-07-24	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
16	1	f	10	0.810000000000000053	0	0	10.8000000000000007	3	3	f	16	2020-07-25	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
17	1	f	10	0.810000000000000053	0	0	10.8000000000000007	3	3	f	17	2020-07-27	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
18	1	f	10	0.810000000000000053	0	0	10.8000000000000007	3	3	f	18	2020-07-28	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
19	1	f	10	0.810000000000000053	0	0	10.8000000000000007	3	3	f	19	2020-07-30	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
20	1	f	10	0.810000000000000053	0	0	10.8000000000000007	3	3	f	20	2020-07-31	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
21	1	f	10	0.810000000000000053	0	0	10.8000000000000007	3	3	f	21	2020-08-01	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
22	1	f	10	0.810000000000000053	0	0	10.8000000000000007	3	3	f	22	2020-08-03	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
23	1	f	10	0.810000000000000053	0	0	10.8000000000000007	3	3	f	23	2020-08-04	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
24	1	f	10	0.810000000000000053	0	0	10.8000000000000007	3	3	f	24	2020-08-05	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
25	1	f	10	0.810000000000000053	0	0	10.8000000000000007	3	3	f	25	2020-08-06	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
26	1	f	10	0.810000000000000053	0	0	10.8000000000000007	3	3	f	26	2020-08-07	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
27	1	f	10	0.810000000000000053	0	0	10.8000000000000007	3	3	f	27	2020-08-08	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
28	1	f	10	0.810000000000000053	0	0	10.8000000000000007	3	3	f	28	2020-08-10	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
29	1	f	10	0.810000000000000053	0	0	10.8000000000000007	3	3	f	29	2020-08-11	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
30	1	f	10	0.810000000000000053	0	0	10.8000000000000007	3	3	f	30	2020-08-12	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
\.


--
-- Data for Name: departamento; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.departamento (id_departamento, departamento) FROM stdin;
1	JUNIN
2	LIMA
3	HUANUCO
4	UCAYALI
\.


--
-- Data for Name: detalle_negocio; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.detalle_negocio (id_detalle_negocio, id_negocio, tipo_negocio, cantidad, descripcion, frecuencia_mes, precio_compra, precio_venta, total_compra, total_venta, usuario, agencia, fecha_hora, estado) FROM stdin;
\.


--
-- Data for Name: dias_festivos; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.dias_festivos (id_dia_festivo, dia, mes, anio, titulo, id_agencia) FROM stdin;
1	1	1	0	n	1
2	29	7	0	n	1
4	25	12	0	n	1
5	29	6	0	n	1
6	8	10	0	n	1
7	9	4	0	n	1
3	1	5	0	n	1
\.


--
-- Data for Name: distrito; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.distrito (id_distrito, id_provincia, distrito) FROM stdin;
1	1	CHANCHAMAYO
2	1	PERENE
3	1	PICHANAQUI
4	1	SAN LUIS DE CHUARO
5	1	SAN RAMÒN
6	1	VITOC
7	2	AHUAC
8	2	CHONGOS BAJO
9	2	CHUPACA
10	2	HUACHAC
11	2	HUAMANCACA CHICO
12	2	SAN JUAN DE JARPA
13	2	SAN JUAN DE ISCOS
14	2	TRES DE DICIEMBRE
15	2	YANACANCHA
16	3	ANDAMARCA
17	3	ACO
18	3	CHAMBARA
19	3	COCHAS
20	3	COMAS
21	3	CONCEPCIÒN
22	3	HEROINAS TOLEDO
23	3	MANZANARES
24	3	MATAHUASI
25	3	MITO
26	3	NUEVE DE JULIO
27	3	ORCOTUNA
28	3	SAN JOSE DE QUERO
29	3	SANTA ROSA DE OCOPA
30	4	HUANCAYO
31	4	CARHUACALLAGA
32	4	CHACAPAMPA
33	4	CHICCHE
34	4	CHILCA
35	4	CHONGOS ALTO
36	4	CHUPURO
37	4	COLCA
38	4	CULLHUAS
39	4	EL TAMBO
40	4	HUACRAPUQUIO
41	4	HUALHUAS
42	4	HUANCÁN
43	4	HUASICANCHA
44	4	HUAYUCACHI
45	4	INGENIO
46	4	PARIAHUANCA
47	4	PILCOMAYO
48	4	PUCARÁ
49	4	QUICHUAY
50	4	QUILCAS
51	4	SAN AGUSTÍN DE CAJAS
52	4	SAN JERÓNIMO DE TUNÁN
53	4	SAN PEDRO DE SAÑOS
54	4	SANTO DOMINGO DE ACOB.
56	4	SICAYA
57	4	VIQUES
58	5	JAUJA
59	6	SATIPO
60	7	TARMA
55	4	SAPALLANGA
61	8	YAULI
62	9	CASTILLO GRANDE
63	9	JOSE CRESPO Y CASTILLO
64	9	PUCAYACU
65	9	PUEBLO NUEVO
66	9	DANIEL ALOMIA ROBLES
67	9	LUYANDO
68	9	RUPA-RUPA
69	9	HERMILIO VALDIZAN
70	9	MARIANO DAMAZO BERAUN
71	9	SANTO DOMINGO DE ANDA
72	10	RAYMONDI
73	10	SEPAHUA
74	10	TAHUANIA
75	10	YURUA
76	10	CALLERIA
77	10	CAMPOVERDE
78	10	IPARIA
79	10	MANANTAY
80	10	MASISEA
81	10	NUEVA REQUENA
82	10	YARINACOHCA
83	10	CURIMANÁ
84	10	IRZOLA
85	10	PADRE ABAD
86	10	PURÚS
\.


--
-- Data for Name: empresa; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.empresa (id_empresa, nombre_empresa, codigo_empresa, detalles_empresa, nombre_corto) FROM stdin;
1	CREDIT VISION SAC RUC:20604591156	1	detalles	CREDIVISION RUC:20604591156
\.


--
-- Data for Name: equipos; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.equipos (id_aquipos, id_agencia, nombre_equipo, area) FROM stdin;
1	1	DESKTOP-CQ135NL	INFORMATICA
2	1	DJ-XAVEX	GERENCIA
3	1	DESKTOP-2COHNSG	ADMINISTRACION
4	1	DESKTOP-RD921RC	ASESOR01
5	1	DESKTOP-PDVNOOT	ASESOR02
6	1	DESKTOP-6HII7R6	CAJA
7	3	ADMINIPUCALLPA	CAJA-ASESOR
8	1	DESKTOP-06NBHBA	ANALISTA
9	1	DESKTOP-A1GIC9E	CREDI CAJA
10	1	DESKTOP-F8PPP5L	VISION GERENCIA
11	2	PRINCIPAL01	GERENCIA
12	2	PC6	ANALSITA
13	2	PC100	CAJA
14	3	DESKTOP-2NANGC4	ANALISTA PICHA
15	3	PC	ASESOR PICHANAKY
\.


--
-- Data for Name: ficha_domiciliaria; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.ficha_domiciliaria (id_ficha_domiciliaria, id_analista, id_solicitud, agencia, dni_cliente, zonificacion, t_inmueble, t_edificacion, t_zona, t_construccion, est_inmueble, zona, serv_basicos, grado_peligro, num_depend, name_conyuge, nivel_instruccion, acupacion, direccion_correcta, jardin, cochera, pisos_nro, acabados, conservacion, seguridad, ubicacion, vigilancia, pista, vereda, atendio, residencia, condicion, t_residencia, estado_civil, habita, medio_comunicacion, name_informante, documento_dni, parentesco, telefono_infor, cerco, fachada, paredes, techo, piso, puertas, ventana, suministro, coment_primera_v, coment_segunda_v, nombre_vecino, obs_vecino, estado_formulario, fecha_hora_ficha, fecha_hora_modi, id_agencia, obs) FROM stdin;
0	3	\N	CONCEPCIÓN	20413205	Centrico	Casa		Rural	Noble	Bueno	Buena	Luz, Agua y Desague		0		SECUNDARIA	Independiente	JR. MANUEL PRADO S/N	933861157 		2								SI	Permanente	Propia	10 AÑOS	Casado(a)	SI	Personalmente	JESENIA 		HIJA		\N	\N	\N	\N	\N	\N	\N	69481890	CLIENTE CON BUENAS REFERENCIAS 	FRENTE A LA PLAZA PRINCIPAL 	ANA MARIA CHAMORRO	CARRETERA CENTRAL 	\N	2020-7-7 13:47:35.80	\N	\N	\N
1	2	\N	CONCEPCIÓN	43532534	Aledaña	Casa		Urbana	Noble	Muy bueno	Buena	Luz, Agua y Desague		1	PERALES PAITAN WUILY	SUPERIOR	Independiente	AV. FERROCARRIL S/N	913045376 		3								SI	Permanente	Familiar	URBANA	Casado(a)	SI	Personalmente	CARMEN ESPINOZA DE LA CRUZ		CONYUGE	954408997	\N	\N	\N	\N	\N	\N	\N	69481890	CLIENTE CON MUY BUENA UBICACION DEL NEGOCIO , NEGOCIO MUY CUYDADO	POR LA CAPILLA SAN ANTONIO 	MAYUMI SORIANO 	PSJ. SAN ANTONIO S/N	\N	2020-7-23 15:53:24.977	\N	\N	\N
2	2	\N	CONCEPCIÓN	43532534	Aledaña	Casa		Urbana	Noble	Muy bueno	Buena	Luz, Agua y Desague		1	PERALES PAITAN WUILY	SUPERIOR	Independiente	AV. FERROCARRIL S/N	913045376 		3								SI	Permanente	Familiar	URBANA	Casado(a)	SI	Personalmente	CARMEN ESPINOZA DE LA CRUZ		CONYUGE	954408997	\N	\N	\N	\N	\N	\N	\N	69481890	CLIENTE CON MUY BUENA UBICACION DEL NEGOCIO , NEGOCIO MUY CUYDADO	POR LA CAPILLA SAN ANTONIO 	MAYUMI SORIANO 	PSJ. SAN ANTONIO S/N	\N	2020-7-23 15:53:56.178	\N	\N	\N
3	7	\N	PICHANAKI	41332133	Alejada	Casa		Rural	Rústico	Aceptable	Regular	Luz, Agua y Desague		1		SUPERIOR	Independiente		 		1								SI	Permanente	Familiar	10 años	Soltero(a)		Personalmente	Irma Quispe		Madre		\N	\N	\N	\N	\N	\N	\N	72475204	el cliente es buen vecino y sin problemas 	a dos cuadras de la loza Mareategui	Karla Huachurunto Taipe	Pje Hector Chumpitaz Ñ4-01- AAHH JOse Carlos Mareategui	\N	2020-7-27 18:18:11.469	\N	\N	\N
4	2	\N	HUANCAYO	47506173	Alejada	Casa		Urbana	Noble	Muy bueno	Buena	Luz, Agua y Desague		3	SALAZAR CORDOVA CESAR	SECUNDARIA	Independiente	JR. MARISCAL DE SUCRE S/N 	978006552 		3								SI	Permanente	Propia	5 AÑOS	Soltero(a)	NO	Personalmente	CLEMENTE YURI 		EX CONYUGE 		\N	\N	\N	\N	\N	\N	\N	69463284	CLIENTE CON BUENAS REFERENCIAS  CON DOMICILIO PROPIO Y CON PROYECCIONES DE INVERSIONES DE NEGOCIO 	POR EL CEMENTERIO DE CAJAS 	JAIME VILLALBA		\N	2020-8-6 14:30:35.3	\N	\N	\N
5	3	\N	CONCEPCIÓN	41281404	Centrico	Casa		Urbana	Noble	Muy bueno	Buena	Luz, Agua y Desague		3	cesar	SUPERIOR	Independiente	jr. catalina huanca 198	983643420 		2								SI	Permanente	Propia	casa 	Casado(a)	SI	Personalmente	liz santivañez				\N	\N	\N	\N	\N	\N	\N	69386828	cliente cuenta con negocio de venta de abarrotes, verduras, frutas entre otros con muy buena ubicacion cabe m,encionar que cliente cuenta con otros ingresos de docente .	a tres cuadras del colegio sanabria			\N	2020-8-14 10:41:8.823	\N	\N	\N
6	3	\N	CONCEPCIÓN	41281404	Centrico	Casa		Urbana	Noble	Muy bueno	Buena	Luz, Agua y Desague		3	cesar	SUPERIOR	Independiente	jr. catalina huanca 198	983643420 		2								SI	Permanente	Propia	casa 	Casado(a)	SI	Personalmente	liz santivañez				\N	\N	\N	\N	\N	\N	\N	69386828	cliente cuenta con negocio de venta de abarrotes, verduras, frutas entre otros con muy buena ubicacion cabe m,encionar que cliente cuenta con otros ingresos de docente .	a tres cuadras del colegio sanabria			\N	2020-8-14 10:41:47.249	\N	\N	\N
7	3	\N	CONCEPCIÓN	43075285	Centrico	Casa		Urbana	Noble	Bueno	Buena	Luz, Agua y Desague		2	rafael	SECUNDARIA	Independiente	jr. agricultura s/n	913027779 		3								SI	Permanente	Alquilado	10 años	Soltero(a)	NO	Personalmente	cainicela de la o nancy		familiar		\N	\N	\N	\N	\N	\N	\N	69510455	cliente vive en casa alquilada por mas de 8 años frente a la plazuela	frente a la plazuela del 9 de julio			\N	2020-8-15 10:14:21.395	\N	\N	\N
8	3	\N	CONCEPCIÓN	20400482	Centrico	Casa		Urbana	Noble	Bueno	Buena	Luz, Agua y Desague		1	no tiene	SECUNDARIA	Independiente	jr. nueve de julio s/n	964427363 		1								SI	Permanente	Propia	10 años	Soltero(a)	SI	Personalmente	ana maria chamorro		mismo	922609611	\N	\N	\N	\N	\N	\N	\N	69386828	cliente cuenta con casa propia con muy buena ubicacion 	frente al puesto de carnes	ivan chamorro	carretera central 1248	\N	2020-8-17 16:16:26.120	\N	\N	\N
9	3	\N	CONCEPCIÓN	48017310	Aledaña	Casa		Rural	Rústico	Bueno	Buena	Luz, Agua y Desague		2		SECUNDARIA	Independiente	Jr.mariscal castilla	972385134 		2								SI	Permanente	Familiar	10 años	Soltero(a)	NO	Personalmente	patricias		titular		\N	\N	\N	\N	\N	\N	\N	75358186	cleinte con casa propia y negocio de moto taxis	a 10 cuadras del hotel huaychulo			\N	2020-8-18 11:47:54.217	\N	\N	\N
\.


--
-- Data for Name: ficha_economica; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.ficha_economica (id_ficha, dni_cliente, ingreso_uno, ingreso_dos, ingreso_tres, carga_familar, gastos_alimentacion, gastos_estudio, servicios_familiar, otros_familiar, capital_inicial, ventas_negocio, otro_venta_negocio, personal_negocio, alquiler_negocio, servicios_negocio, otros_negocio_salida, resultado_economico, fecha_hora_ficha, monto_aprobado, fecha_hora_modificacion, obs, estado_ficha, monto_inicial, cambio_monto, incremento, fecha_limite, sw, agencia, login, obs_mod) FROM stdin;
0	20413205	100	0	600	350	3000	0	0	0	1560	800	30	100	200	350	\N	\N	2020-07-07 00:00:00	384	2020-07-07 00:00:00		f	1	\N	\N	\N	\N	CONCEPCIÓN	LCARBAJAL	VENTA DE VERDURAS 
1	43532534	200	100	7000	3500	0	50	100	0	1906	850	100	100	500	500	\N	\N	2020-07-23 00:00:00	684.799999999999955	2020-07-23 00:00:00	cliente con muy buen historial y negocio lleno 	f	2	\N	\N	\N	\N	CONCEPCIÓN	JHUAMAN	BODEGA - MINI MARKET
2	41332133	100	0	0	5000	20000	0	0	0	2000	0	165	0	150	1030	\N	\N	2020-07-27 00:00:00	764	2020-07-27 00:00:00	cliente con buen patrimonio	f	3	\N	\N	\N	\N	PICHANAKI	ADURAND	DOCENTE
3	47506173	200	150	250	1000	60000	2000	15000	5000	4500	2500	100	450	500	550	\N	\N	2020-08-06 00:00:00	1120	2020-08-06 00:00:00	CLIENTE CUENTA CON CASA PROPIA Y LOCAL DONDE PREPARA SU PRODUCCION DE CEBICHE	f	4	\N	\N	\N	\N	HUANCAYO	JHUAMAN	VENTA DE CEBICHE 
4	41281404	300	200	12000	7000	18000	200	4000	1200	9100	6200	100	300	1500	600	\N	\N	2020-08-14 00:00:00	2720	2020-08-14 00:00:00	cliente cuenta con casa propia de material noble donde esta ubicado su negocio , cuneta tambien con otros ingresos de docencia 	f	5	\N	\N	\N	\N	CONCEPCIÓN	LCARBAJAL	BODEGA - VENTA DE ABARROTES Y OTROS
5	43075285	300	300	500	3500	0	100	1000	0	2686	1040	40	220	200	500	\N	\N	2020-08-15 00:00:00	868.799999999999955	2020-08-15 00:00:00	cliente cuenta con creditos en otros bancos donde son pagados por sus hijos y hermanos 	f	6	\N	\N	\N	\N	CONCEPCIÓN	LCARBAJAL	VENTA DE DESAYUNOS 
6	20400482	300	0	2000	2000	25000	100	200	0	15400	9200	20	200	100	850	\N	\N	2020-08-17 00:00:00	4184	2020-08-17 00:00:00	negocio con muy buena ubicacion de el mercado central	f	7	\N	\N	\N	\N	CONCEPCIÓN	LCARBAJAL	VENTA DE VERDURAS
\.


--
-- Data for Name: fotos; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.fotos (codigo, dni, tipo, descripcion, imagen, fecha, agencia) FROM stdin;
\.


--
-- Data for Name: gastos_operativos; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.gastos_operativos (id_gastos_operativos, id_negocio, descripcion, monto, usuario, agencia, fecha_hora, estado) FROM stdin;
\.


--
-- Data for Name: grantia_real; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.grantia_real (id_grantia_real, dni, bien_real, caracteristicas, comprobante, serie, estado, valor_compra, valor_residual, antiguedad, expectativa, id_usuario, fecha_hora, agencia, valor_real, credito, valor_vendido, almacen, id_usuario_venta, fecha_venta) FROM stdin;
\.


--
-- Data for Name: historial_manipulacion; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.historial_manipulacion (id_historial_manipulacion, id_usuario, operacion, time_ingreso, maquina, agencia) FROM stdin;
82184	1	INGRESO AL SISTEMA	2020-06-28 09:37:02	DESKTOP-CQ135NL	HUANCAYO
82185	1	INGRESO AL SISTEMA	2020-06-28 09:54:51	DESKTOP-CQ135NL	HUANCAYO
82186	1	INGRESO AL SISTEMA	2020-06-28 10:01:22	DESKTOP-CQ135NL	HUANCAYO
82187	1	INGRESO AL SISTEMA	2020-06-28 10:10:29	DESKTOP-CQ135NL	HUANCAYO
82188	1	INGRESO AL SISTEMA	2020-06-28 10:38:13	DJ-XAVEX	HUANCAYO
82189	1	INGRESO AL SISTEMA	2020-06-28 10:45:19	DESKTOP-CQ135NL	TINGOMARIA
82190	1	INGRESO AL SISTEMA	2020-06-28 16:47:30	DESKTOP-CQ135NL	TINGOMARIA
82191	1	INGRESO AL SISTEMA	2020-06-28 17:08:55	DESKTOP-CQ135NL	TINGOMARIA
82192	2	INGRESO AL SISTEMA	2020-06-28 23:40:37	DJ-XAVEX	HUANCAYO
82193	2	INGRESO AL SISTEMA	2020-06-29 09:59:35	DJ-XAVEX	HUANCAYO
82194	1	INGRESO AL SISTEMA	2020-06-29 11:26:43	DESKTOP-CQ135NL	HUNACAYO
82195	1	INGRESO AL SISTEMA	2020-06-29 13:40:48	DESKTOP-2COHNSG	HUANCAYO
82196	1	INGRESO AL SISTEMA	2020-06-29 13:46:16	DESKTOP-RD921RC	HUANCAYO
82197	1	INGRESO AL SISTEMA	2020-06-29 13:50:20	DESKTOP-PDVNOOT	HUANCAYO
82198	2	INGRESO AL SISTEMA	2020-06-29 13:54:50	DESKTOP-PDVNOOT	HUANCAYO
82199	7	INGRESO AL SISTEMA	2020-06-29 14:03:28	DESKTOP-RD921RC	HUANCAYO
82200	7	INGRESO AL SISTEMA	2020-06-29 14:27:37	DJ-XAVEX	HUANCAYO
82201	3	INGRESO AL SISTEMA	2020-06-29 14:38:31	DESKTOP-PDVNOOT	HUANCAYO
82202	3	INGRESO AL SISTEMA	2020-06-29 14:44:24	DESKTOP-PDVNOOT	HUANCAYO
82203	4	INGRESO AL SISTEMA	2020-06-29 14:48:44	DESKTOP-6HII7R6	HUANCAYO
82204	6	INGRESO AL SISTEMA	2020-06-29 14:51:39	DESKTOP-2COHNSG	HUANCAYO
82205	1	INGRESO AL SISTEMA	2020-06-29 15:31:32	DESKTOP-6HII7R6	HUANCAYO
82206	7	INGRESO AL SISTEMA	2020-06-29 15:31:56	DESKTOP-RD921RC	HUANCAYO
82207	7	INGRESO AL SISTEMA	2020-06-29 15:33:24	DESKTOP-6HII7R6	HUANCAYO
82208	6	INGRESO AL SISTEMA	2020-06-29 15:45:11	DESKTOP-2COHNSG	HUANCAYO
82209	7	INGRESO AL SISTEMA	2020-06-29 15:49:33	DESKTOP-RD921RC	HUANCAYO
82210	4	INGRESO AL SISTEMA	2020-06-29 15:54:29	DESKTOP-6HII7R6	HUANCAYO
82211	5	INGRESO AL SISTEMA	2020-06-29 16:07:48	DESKTOP-RD921RC	HUANCAYO
82212	5	INGRESO AL SISTEMA	2020-06-29 16:19:08	DESKTOP-RD921RC	HUANCAYO
82213	7	INGRESO AL SISTEMA	2020-06-29 16:20:44	DESKTOP-RD921RC	HUANCAYO
82214	4	INGRESO AL SISTEMA	2020-06-29 16:26:00	DESKTOP-6HII7R6	HUANCAYO
82215	1	INGRESO AL SISTEMA	2020-06-29 16:33:24	DESKTOP-CQ135NL	HUANCAYO
82216	4	INGRESO AL SISTEMA	2020-06-29 16:34:17	DESKTOP-CQ135NL	HUANCAYO
82217	1	INGRESO AL SISTEMA	2020-06-29 20:42:58	DESKTOP-CQ135NL	HUANCAYO
82218	1	INGRESO AL SISTEMA	2020-06-29 20:50:54	DESKTOP-CQ135NL	HUANCAYO
82219	1	INGRESO AL SISTEMA	2020-06-29 20:54:03	DESKTOP-CQ135NL	HUANCAYO
82220	4	INGRESO AL SISTEMA	2020-06-30 10:06:05	DESKTOP-6HII7R6	HUANCAYO
82221	6	INGRESO AL SISTEMA	2020-06-30 10:10:01	DESKTOP-2COHNSG	HUANCAYO
82222	5	INGRESO AL SISTEMA	2020-06-30 10:12:53	DESKTOP-RD921RC	HUANCAYO
82223	2	INGRESO AL SISTEMA	2020-06-30 10:18:14	DJ-XAVEX	HUANCAYO
82224	6	INGRESO AL SISTEMA	2020-06-30 10:23:18	DESKTOP-2COHNSG	HUANCAYO
82225	5	INGRESO AL SISTEMA	2020-06-30 10:24:48	DESKTOP-RD921RC	HUANCAYO
82226	4	INGRESO AL SISTEMA	2020-06-30 10:27:39	DESKTOP-6HII7R6	HUANCAYO
82227	6	INGRESO AL SISTEMA	2020-06-30 10:50:20	DESKTOP-2COHNSG	HUANCAYO
82228	3	INGRESO AL SISTEMA	2020-06-30 10:52:38	DESKTOP-2COHNSG	HUANCAYO
82229	6	INGRESO AL SISTEMA	2020-06-30 10:58:27	DESKTOP-2COHNSG	HUANCAYO
82230	5	INGRESO AL SISTEMA	2020-06-30 11:00:00	DESKTOP-RD921RC	HUANCAYO
82231	6	INGRESO AL SISTEMA	2020-06-30 11:08:33	DESKTOP-2COHNSG	HUANCAYO
82232	4	INGRESO AL SISTEMA	2020-06-30 11:25:36	DESKTOP-6HII7R6	HUANCAYO
82233	5	INGRESO AL SISTEMA	2020-06-30 11:55:34	DESKTOP-RD921RC	HUANCAYO
82234	5	INGRESO AL SISTEMA	2020-06-30 12:15:46	DESKTOP-RD921RC	HUANCAYO
82235	3	INGRESO AL SISTEMA	2020-06-30 12:22:41	DESKTOP-PDVNOOT	HUANCAYO
82236	4	INGRESO AL SISTEMA	2020-06-30 12:56:00	DESKTOP-6HII7R6	HUANCAYO
82237	4	INGRESO AL SISTEMA	2020-06-30 12:58:20	DESKTOP-CQ135NL	HUANCAYO
82238	4	INGRESO AL SISTEMA	2020-06-30 13:00:31	DESKTOP-CQ135NL	HUANCAYO
82239	4	INGRESO AL SISTEMA	2020-06-30 13:02:41	DESKTOP-CQ135NL	HUANCAYO
82240	4	INGRESO AL SISTEMA	2020-06-30 13:06:16	DESKTOP-6HII7R6	HUANCAYO
82241	2	INGRESO AL SISTEMA	2020-06-30 13:08:32	DESKTOP-6HII7R6	HUANCAYO
82242	5	INGRESO AL SISTEMA	2020-06-30 13:09:48	DESKTOP-RD921RC	HUANCAYO
82243	6	INGRESO AL SISTEMA	2020-06-30 13:58:14	DESKTOP-2COHNSG	HUANCAYO
82244	5	INGRESO AL SISTEMA	2020-06-30 14:50:42	DESKTOP-RD921RC	HUANCAYO
82245	5	INGRESO AL SISTEMA	2020-06-30 16:09:50	DESKTOP-RD921RC	HUANCAYO
82246	1	INGRESO AL SISTEMA	2020-06-30 22:21:55	ADMINIPUCALLPA	HUANCAYO
82247	1	INGRESO AL SISTEMA	2020-06-30 22:25:37	DESKTOP-CQ135NL	PUCALLPA
82248	1	INGRESO AL SISTEMA	2020-06-30 22:31:08	ADMINIPUCALLPA	PUCALLPA
82249	1	INGRESO AL SISTEMA	2020-06-30 22:44:09	ADMINIPUCALLPA	PUCALLPA
82250	2	INGRESO AL SISTEMA	2020-06-30 22:49:30	DJ-XAVEX	HUANCAYO
82251	8	INGRESO AL SISTEMA	2020-06-30 23:02:33	ADMINIPUCALLPA	PUCALLPA
82252	2	INGRESO AL SISTEMA	2020-06-30 23:25:25	DJ-XAVEX	HUANCAYO
82253	11	INGRESO AL SISTEMA	2020-06-30 23:32:32	ADMINIPUCALLPA	PUCALLPA
82254	2	INGRESO AL SISTEMA	2020-06-30 23:41:25	DJ-XAVEX	HUANCAYO
82255	2	INGRESO AL SISTEMA	2020-06-30 23:42:01	DJ-XAVEX	HUANCAYO
82256	2	INGRESO AL SISTEMA	2020-06-30 23:42:40	DJ-XAVEX	HUANCAYO
82257	11	INGRESO AL SISTEMA	2020-07-01 09:12:04	ADMINIPUCALLPA	PUCALLPA
82258	9	INGRESO AL SISTEMA	2020-07-01 09:55:25	ADMINIPUCALLPA	PUCALLPA
82259	3	INGRESO AL SISTEMA	2020-07-01 10:06:30	DESKTOP-PDVNOOT	HUANCAYO
82260	2	INGRESO AL SISTEMA	2020-07-01 10:07:39	DJ-XAVEX	HUANCAYO
82261	2	INGRESO AL SISTEMA	2020-07-01 10:17:14	DJ-XAVEX	HUANCAYO
82262	5	INGRESO AL SISTEMA	2020-07-01 10:17:24	DESKTOP-RD921RC	HUANCAYO
82263	11	INGRESO AL SISTEMA	2020-07-01 10:17:56	ADMINIPUCALLPA	PUCALLPA
82264	11	INGRESO AL SISTEMA	2020-07-01 10:59:10	ADMINIPUCALLPA	PUCALLPA
82265	2	INGRESO AL SISTEMA	2020-07-01 11:04:16	DJ-XAVEX	HUANCAYO
82266	11	INGRESO AL SISTEMA	2020-07-01 11:10:41	ADMINIPUCALLPA	PUCALLPA
82267	11	INGRESO AL SISTEMA	2020-07-01 11:32:34	ADMINIPUCALLPA	PUCALLPA
82268	4	INGRESO AL SISTEMA	2020-07-01 11:42:36	DESKTOP-6HII7R6	HUANCAYO
82269	4	INGRESO AL SISTEMA	2020-07-01 11:47:02	DESKTOP-6HII7R6	HUANCAYO
82270	5	INGRESO AL SISTEMA	2020-07-01 13:51:50	DESKTOP-RD921RC	HUANCAYO
82271	5	INGRESO AL SISTEMA	2020-07-01 14:21:05	DESKTOP-RD921RC	HUANCAYO
82272	4	INGRESO AL SISTEMA	2020-07-01 14:43:06	DJ-XAVEX	HUANCAYO
82273	2	INGRESO AL SISTEMA	2020-07-01 17:28:46	DJ-XAVEX	HUANCAYO
82274	4	INGRESO AL SISTEMA	2020-07-01 17:30:40	DJ-XAVEX	HUANCAYO
82275	4	INGRESO AL SISTEMA	2020-07-01 18:15:24	DESKTOP-CQ135NL	HUANCAYO
82276	4	INGRESO AL SISTEMA	2020-07-01 18:31:26	DJ-XAVEX	HUANCAYO
82277	11	INGRESO AL SISTEMA	2020-07-01 18:41:18	ADMINIPUCALLPA	PUCALLPA
82278	2	INGRESO AL SISTEMA	2020-07-01 19:14:33	DJ-XAVEX	HUANCAYO
82279	9	INGRESO AL SISTEMA	2020-07-01 19:37:26	ADMINIPUCALLPA	PUCALLPA
82280	2	INGRESO AL SISTEMA	2020-07-01 21:24:04	DJ-XAVEX	HUANCAYO
82281	1	INGRESO AL SISTEMA	2020-07-01 23:02:20	DESKTOP-CQ135NL	HUANCAYO
82282	11	INGRESO AL SISTEMA	2020-07-01 23:28:45	ADMINIPUCALLPA	PUCALLPA
82283	11	INGRESO AL SISTEMA	2020-07-02 08:47:36	ADMINIPUCALLPA	PUCALLPA
82284	4	INGRESO AL SISTEMA	2020-07-02 08:47:37	DESKTOP-6HII7R6	HUANCAYO
82285	3	INGRESO AL SISTEMA	2020-07-02 08:57:40	DESKTOP-PDVNOOT	HUANCAYO
82286	2	INGRESO AL SISTEMA	2020-07-02 08:59:05	DESKTOP-2COHNSG	HUANCAYO
82287	2	INGRESO AL SISTEMA	2020-07-02 11:31:26	DESKTOP-2COHNSG	HUANCAYO
82288	2	INGRESO AL SISTEMA	2020-07-02 12:01:49	DESKTOP-2COHNSG	HUANCAYO
82289	2	INGRESO AL SISTEMA	2020-07-02 13:33:46	DESKTOP-2COHNSG	HUANCAYO
82290	11	INGRESO AL SISTEMA	2020-07-02 13:41:41	ADMINIPUCALLPA	PUCALLPA
82291	4	INGRESO AL SISTEMA	2020-07-02 13:43:27	DESKTOP-6HII7R6	HUANCAYO
82292	2	INGRESO AL SISTEMA	2020-07-02 20:21:15	DJ-XAVEX	HUANCAYO
82293	11	INGRESO AL SISTEMA	2020-07-02 22:39:09	ADMINIPUCALLPA	PUCALLPA
82294	4	INGRESO AL SISTEMA	2020-07-03 08:57:13	DESKTOP-6HII7R6	HUANCAYO
82295	11	INGRESO AL SISTEMA	2020-07-03 09:01:09	ADMINIPUCALLPA	PUCALLPA
82296	5	INGRESO AL SISTEMA	2020-07-03 09:03:30	DESKTOP-RD921RC	HUANCAYO
82297	2	INGRESO AL SISTEMA	2020-07-03 09:21:10	DESKTOP-6HII7R6	HUANCAYO
82298	3	INGRESO AL SISTEMA	2020-07-03 09:56:18	DESKTOP-2COHNSG	HUANCAYO
82299	4	INGRESO AL SISTEMA	2020-07-03 10:22:07	DESKTOP-6HII7R6	HUANCAYO
82300	5	INGRESO AL SISTEMA	2020-07-03 10:22:18	DESKTOP-RD921RC	HUANCAYO
82301	3	INGRESO AL SISTEMA	2020-07-03 10:26:10	DESKTOP-2COHNSG	HUANCAYO
82302	11	INGRESO AL SISTEMA	2020-07-03 12:01:05	ADMINIPUCALLPA	PUCALLPA
82303	5	INGRESO AL SISTEMA	2020-07-03 12:15:52	DESKTOP-RD921RC	HUANCAYO
82304	5	INGRESO AL SISTEMA	2020-07-03 12:20:14	DESKTOP-RD921RC	HUANCAYO
82305	5	INGRESO AL SISTEMA	2020-07-03 12:35:37	DESKTOP-RD921RC	HUANCAYO
82306	5	INGRESO AL SISTEMA	2020-07-03 12:48:04	DESKTOP-RD921RC	HUANCAYO
82307	2	INGRESO AL SISTEMA	2020-07-03 13:02:14	DESKTOP-6HII7R6	HUANCAYO
82308	3	INGRESO AL SISTEMA	2020-07-03 14:18:00	DESKTOP-2COHNSG	HUANCAYO
82309	11	INGRESO AL SISTEMA	2020-07-03 15:00:24	ADMINIPUCALLPA	PUCALLPA
82310	3	INGRESO AL SISTEMA	2020-07-04 09:19:32	DESKTOP-PDVNOOT	HUANCAYO
82311	2	INGRESO AL SISTEMA	2020-07-04 09:23:42	DESKTOP-2COHNSG	HUANCAYO
82312	3	INGRESO AL SISTEMA	2020-07-04 09:29:35	DESKTOP-PDVNOOT	HUANCAYO
82313	4	INGRESO AL SISTEMA	2020-07-04 09:34:36	DESKTOP-6HII7R6	HUANCAYO
82314	4	INGRESO AL SISTEMA	2020-07-04 09:46:53	DESKTOP-2COHNSG	HUANCAYO
82315	5	INGRESO AL SISTEMA	2020-07-04 11:16:41	DESKTOP-RD921RC	HUANCAYO
82316	5	INGRESO AL SISTEMA	2020-07-04 11:55:00	DESKTOP-RD921RC	HUANCAYO
82317	4	INGRESO AL SISTEMA	2020-07-06 09:17:56	DESKTOP-6HII7R6	HUANCAYO
82318	5	INGRESO AL SISTEMA	2020-07-06 09:28:35	DESKTOP-RD921RC	HUANCAYO
82319	2	INGRESO AL SISTEMA	2020-07-06 09:57:36	DESKTOP-2COHNSG	HUANCAYO
82320	5	INGRESO AL SISTEMA	2020-07-06 10:40:04	DESKTOP-RD921RC	HUANCAYO
82321	5	INGRESO AL SISTEMA	2020-07-06 12:29:13	DESKTOP-RD921RC	HUANCAYO
82322	5	INGRESO AL SISTEMA	2020-07-06 12:30:31	DESKTOP-RD921RC	HUANCAYO
82323	5	INGRESO AL SISTEMA	2020-07-06 12:33:11	DESKTOP-RD921RC	HUANCAYO
82324	5	INGRESO AL SISTEMA	2020-07-06 12:35:28	DESKTOP-RD921RC	HUANCAYO
82325	5	INGRESO AL SISTEMA	2020-07-06 12:42:46	DESKTOP-RD921RC	HUANCAYO
82326	5	INGRESO AL SISTEMA	2020-07-06 13:30:30	DESKTOP-RD921RC	HUANCAYO
82327	1	INGRESO AL SISTEMA	2020-07-06 14:06:32	DESKTOP-06NBHBA	HUANCAYO
82328	1	INGRESO AL SISTEMA	2020-07-06 14:12:51	DESKTOP-A1GIC9E	HUANCAYO
82329	1	INGRESO AL SISTEMA	2020-07-06 14:14:37	DESKTOP-F8PPP5L	HUANCAYO
82330	1	INGRESO AL SISTEMA	2020-07-06 14:16:05	DESKTOP-06NBHBA	HUANCAYO
82331	1	INGRESO AL SISTEMA	2020-07-06 14:16:29	DESKTOP-06NBHBA	HUANCAYO
82332	1	INGRESO AL SISTEMA	2020-07-06 14:16:48	DESKTOP-06NBHBA	HUANCAYO
82333	1	INGRESO AL SISTEMA	2020-07-07 12:06:57	DESKTOP-CQ135NL	CONCEPCIÓN
82334	1	INGRESO AL SISTEMA	2020-07-07 12:48:00	PRINCIPAL01	CONCEPCIÓN
82335	1	INGRESO AL SISTEMA	2020-07-07 12:51:18	DESKTOP-CQ135NL	CONCEPCIÓN
82336	1	INGRESO AL SISTEMA	2020-07-07 13:01:57	PC6	CONCEPCIÓN
82337	2	INGRESO AL SISTEMA	2020-07-07 13:22:34	PRINCIPAL01	CONCEPCIÓN
82338	2	INGRESO AL SISTEMA	2020-07-07 13:34:35	PRINCIPAL01	CONCEPCIÓN
82339	3	INGRESO AL SISTEMA	2020-07-07 13:35:15	PRINCIPAL01	CONCEPCIÓN
82340	4	INGRESO AL SISTEMA	2020-07-07 14:06:58	PRINCIPAL01	CONCEPCIÓN
82341	2	INGRESO AL SISTEMA	2020-07-07 14:10:19	PRINCIPAL01	CONCEPCIÓN
82342	1	INGRESO AL SISTEMA	2020-07-07 14:32:51	PC100	CONCEPCIÓN
82343	2	INGRESO AL SISTEMA	2020-07-07 14:33:44	PRINCIPAL01	CONCEPCIÓN
82344	2	INGRESO AL SISTEMA	2020-07-07 16:44:54	PRINCIPAL01	CONCEPCIÓN
82345	2	INGRESO AL SISTEMA	2020-07-10 17:33:14	PRINCIPAL01	CONCEPCIÓN
82346	2	INGRESO AL SISTEMA	2020-07-13 09:36:56	PRINCIPAL01	CONCEPCIÓN
82347	1	INGRESO AL SISTEMA	2020-07-14 17:09:24	DESKTOP-CQ135NL	PICHANAKI
82348	1	INGRESO AL SISTEMA	2020-07-14 17:23:08	DESKTOP-2NANGC4	PICHANAKI
82349	1	INGRESO AL SISTEMA	2020-07-14 17:37:40	PC	PICHANAKI
82350	2	INGRESO AL SISTEMA	2020-07-15 09:09:43	PRINCIPAL01	CONCEPCIÓN
82351	2	INGRESO AL SISTEMA	2020-07-15 09:30:52	PRINCIPAL01	CONCEPCIÓN
82352	6	INGRESO AL SISTEMA	2020-07-16 10:41:16	DESKTOP-2NANGC4	PICHANAKI
82353	7	INGRESO AL SISTEMA	2020-07-16 11:03:23	PC	PICHANAKI
82354	7	INGRESO AL SISTEMA	2020-07-16 11:55:58	PC	PICHANAKI
82355	7	INGRESO AL SISTEMA	2020-07-21 11:01:00	PC	PICHANAKI
82356	2	INGRESO AL SISTEMA	2020-07-23 15:31:05	PRINCIPAL01	CONCEPCIÓN
82357	3	INGRESO AL SISTEMA	2020-07-23 16:36:50	PRINCIPAL01	CONCEPCIÓN
82358	2	INGRESO AL SISTEMA	2020-07-23 16:43:35	PRINCIPAL01	CONCEPCIÓN
82359	2	INGRESO AL SISTEMA	2020-07-24 09:35:16	PRINCIPAL01	CONCEPCIÓN
82360	4	INGRESO AL SISTEMA	2020-07-24 09:38:58	PRINCIPAL01	CONCEPCIÓN
82361	2	INGRESO AL SISTEMA	2020-07-24 09:44:58	PRINCIPAL01	CONCEPCIÓN
82362	7	INGRESO AL SISTEMA	2020-07-27 17:05:38	DESKTOP-2NANGC4	PICHANAKI
82363	7	INGRESO AL SISTEMA	2020-07-27 17:32:33	DESKTOP-2NANGC4	PICHANAKI
82364	2	INGRESO AL SISTEMA	2020-07-27 17:43:37	PRINCIPAL01	CONCEPCIÓN
82365	7	INGRESO AL SISTEMA	2020-07-27 17:52:22	DESKTOP-2NANGC4	PICHANAKI
82366	7	INGRESO AL SISTEMA	2020-07-27 17:52:53	PRINCIPAL01	CONCEPCIÓN
82367	2	INGRESO AL SISTEMA	2020-08-03 16:53:54	DESKTOP-F8PPP5L	HUANCAYO
82368	2	INGRESO AL SISTEMA	2020-08-06 13:52:09	DESKTOP-F8PPP5L	HUANCAYO
82369	2	INGRESO AL SISTEMA	2020-08-06 14:15:59	DESKTOP-F8PPP5L	HUANCAYO
82370	3	INGRESO AL SISTEMA	2020-08-14 10:27:19	PRINCIPAL01	CONCEPCIÓN
82371	2	INGRESO AL SISTEMA	2020-08-14 11:19:26	PRINCIPAL01	CONCEPCIÓN
82372	2	INGRESO AL SISTEMA	2020-08-15 09:58:37	PRINCIPAL01	CONCEPCIÓN
82373	3	INGRESO AL SISTEMA	2020-08-15 09:59:07	PRINCIPAL01	CONCEPCIÓN
82374	2	INGRESO AL SISTEMA	2020-08-15 10:33:25	PRINCIPAL01	CONCEPCIÓN
82375	3	INGRESO AL SISTEMA	2020-08-17 15:57:16	PRINCIPAL01	CONCEPCIÓN
82376	3	INGRESO AL SISTEMA	2020-08-18 11:33:29	PRINCIPAL01	CONCEPCIÓN
82377	3	INGRESO AL SISTEMA	2020-08-18 15:50:02	PRINCIPAL01	CONCEPCIÓN
82378	3	INGRESO AL SISTEMA	2020-08-18 19:30:37	PRINCIPAL01	CONCEPCIÓN
\.


--
-- Data for Name: historial_visitas; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.historial_visitas (id_historial_visitas, dni, tipo_cliente, nombre_completo, fecha_hora_visita, operacion_realizar, id_usuario, credito, ahorro, detalle_operacion, sacar_cita, cita_analista, resultado_cita, fecha_cita_realizada, fecha_acitar, blokear, agencia, id_agencia) FROM stdin;
8073	20413205	\N		2020-07-07 13:38:21.861	REGISTRADO	3	\N	\N		f	\N	\N	\N	\N	\N	\N	\N
8074	47766965	\N		2020-07-15 09:41:27.327	REGISTRADO	2	\N	\N	cliente con ingresos como trabajadora en el minsa lima con contrato y cuenta con aval que es su madre con casa propia	f	\N	\N	\N	\N	\N	\N	\N
8075	43532534	\N		2020-07-23 15:38:51.675	REGISTRADO	2	\N	\N	CONYUGE NO FIRMA POR MOTIVOS DE TRABAJO	f	\N	\N	\N	\N	\N	\N	\N
8076	41332133	\N		2020-07-27 17:13:57.406	REGISTRADO	7	\N	\N		f	\N	\N	\N	\N	\N	\N	\N
8077	47506173	\N		2020-08-06 14:20:52.671	REGISTRADO	2	\N	\N	CLIENTE CUENTA CON CASA DE MATERIAL NOBLE DE DOS PISOS, CUENTA CON MOTOTAXI PERO A NOMBRE DE SU EX PAREJA , CUENTA CON PENSION DE SU EX PAREJA 	f	\N	\N	\N	\N	\N	\N	\N
8078	41281404	\N		2020-08-14 10:31:44.196	REGISTRADO	3	\N	\N	cliente cuenta con buena ubicacion del negocio , conyuge se dedica al transporte	f	\N	\N	\N	\N	\N	\N	\N
8079	43075285	\N		2020-08-15 10:02:42.389	REGISTRADO	3	\N	\N	cliente cuenta con garantia de artefacto cocina de cuatro ornillas indurama	f	\N	\N	\N	\N	\N	\N	\N
8080	20400482	\N		2020-08-17 16:06:22.268	REGISTRADO	3	\N	\N	cliente con casa propia con negocio de verduras	f	\N	\N	\N	\N	\N	\N	\N
8081	48017310	\N		2020-08-18 11:38:26.452	REGISTRADO	3	\N	\N	cliente con casa familiar	f	\N	\N	\N	\N	\N	\N	\N
\.


--
-- Data for Name: ingreso_egreso; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.ingreso_egreso (id_ingreso_egreso, id_caja_apertura_cierre, tipo, monto, solicitante, concepto, confirmar, sustento, fecha_hora_operacion, agencia, id_agencia, tipo_operacion, numero_documento) FROM stdin;
\.


--
-- Data for Name: insumo_negocio; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.insumo_negocio (id_insumo_negocio, id_detalle_negocio, cantidad, descripcion, precio, total, usuario, agencia, fecha_hora, estado) FROM stdin;
\.


--
-- Data for Name: inventario_bienes; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.inventario_bienes (id_inventario_bienes, dni, bien, caracteristica, valorizado, usuario, fecha_hora_ingreso, fecha_hora_manipulacion) FROM stdin;
\.


--
-- Data for Name: libreta_ahorro; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.libreta_ahorro (id_libreta_ahorro, dni, nombre_oficina, id_cajera, id_administrador, id_benificiario, id_tipo_ahorro, fecha_apertura, monto_apertura, monto_actual, interes_actual, tiempo_plazo, monto_meta, bloquear, obs, fecha_a_retirar, monto_financiado, fecha_retiro, id_cajera_retiro, tea, interes_generado, monto_retirado, ultimo_movimiento, monto_pactado, dni_benificiario, tipo_campana, fin_campana, producto, usuario_ope, id_analista, usuario_analista, estado, id_agencia, periodo, p_retiro) FROM stdin;
12	40129120	HUANCAYO	\N	6	\N	\N	2020-06-30	0	320	0.0700000000000000067	3	0	f		2020-09-28	0	\N	\N	4	0	0	2020-07-02	0	\N	0  	\N	LIBRE	\N	4	BIM	1	1	MESES	0
3	23701995	HUANCAYO	\N	6	\N	\N	2020-06-30	0	400	0	6	0	f		2020-12-27	0	\N	\N	4	0	0	2020-06-30	0	\N	0  	\N	CON INTERES PROGRAMADO	\N	4	BIM	1	1	MESES	180
4	19940774	HUANCAYO	\N	6	\N	\N	2020-06-30	0	15	0	3	0	f		2020-09-28	0	\N	\N	4	0	0	2020-06-30	0	\N	0  	\N	CON INTERES PROGRAMADO	\N	4	BIM	1	1	MESES	90
5	44974563	HUANCAYO	\N	6	\N	\N	2020-06-30	0	0	0	3	0	f		2020-09-28	0	\N	\N	4	0	0	2020-06-30	0	\N	0  	\N	CON INTERES PROGRAMADO	\N	4	BIM	1	1	MESES	90
6	46041015	HUANCAYO	\N	6	\N	\N	2020-06-30	0	0	0	3	0	f		2020-09-28	0	\N	\N	4	0	0	2020-06-30	0	\N	0  	\N	CON INTERES PROGRAMADO	\N	4	BIM	1	1	MESES	90
7	20113864	HUANCAYO	\N	6	\N	\N	2020-06-30	0	0	0	3	0	f		2020-09-28	0	\N	\N	4	0	0	2020-06-30	0	\N	0  	\N	CON INTERES PROGRAMADO	\N	4	BIM	1	1	MESES	90
8	76621787	HUANCAYO	\N	6	\N	\N	2020-06-30	0	0	0	3	0	f		2020-09-28	0	\N	\N	4	0	0	2020-06-30	0	\N	0  	\N	CON INTERES PROGRAMADO	\N	4	BIM	1	1	MESES	90
9	42297478	HUANCAYO	\N	6	\N	\N	2020-06-30	0	0	0	3	0	f		2020-09-28	0	\N	\N	4	0	0	2020-06-30	0	\N	0  	\N	CON INTERES PROGRAMADO	\N	4	BIM	1	1	MESES	90
10	41123341	HUANCAYO	\N	6	\N	\N	2020-06-30	0	0	0	3	0	f		2020-09-28	0	\N	\N	4	0	0	2020-06-30	0	\N	0  	\N	CON INTERES PROGRAMADO	\N	4	BIM	1	1	MESES	90
11	47475777	HUANCAYO	\N	6	\N	\N	2020-06-30	0	0	0	3	0	f		2020-09-28	0	\N	\N	4	0	0	2020-06-30	0	\N	0  	\N	CON INTERES PROGRAMADO	\N	4	BIM	1	1	MESES	90
2	40129120	HUANCAYO	\N	6	\N	\N	2020-06-30	0	300	0	3	0	f		2020-09-28	0	\N	\N	4	0	0	2020-06-30	0	\N	0  	\N	CON INTERES PROGRAMADO	\N	4	BIM	1	1	MESES	90
24	00061656	PUCALLPA	\N	\N	\N	\N	2020-07-01	0	0	0	0	0	f		2020-07-01	0	\N	\N	0	0	0	2020-07-01	0	\N	0  	\N	PLAZO FIJO	\N	9	LARIAS	0	3		0
18	42297478	HUANCAYO	\N	6	\N	\N	2020-06-30	0	355	0	3	0	f		2020-09-28	0	\N	\N	4	0	0	2020-06-30	0	\N	0  	\N	LIBRE	\N	4	BIM	1	1	MESES	0
14	44974563	HUANCAYO	\N	6	\N	\N	2020-06-30	0	0	0	3	0	f		2020-09-28	0	\N	\N	4	0	0	2020-06-30	0	\N	0  	\N	LIBRE	\N	4	BIM	1	1	MESES	0
19	41123341	HUANCAYO	\N	6	\N	\N	2020-06-30	0	603	0.23000000000000001	3	0	f		2020-09-28	0	\N	\N	4	0	0	2020-07-03	0	\N	0  	\N	LIBRE	\N	4	BIM	1	1	MESES	0
20	47475777	HUANCAYO	\N	6	\N	\N	2020-06-30	0	5	0	3	0	f		2020-09-28	0	\N	\N	4	0	0	2020-06-30	0	\N	0  	\N	LIBRE	\N	4	BIM	1	1	MESES	0
33	41123341	HUANCAYO	\N	2	\N	\N	2020-07-03	0	0	0	1	0	f		2020-08-02	0	\N	\N	4	0	0	2020-07-03	0	\N	0  	\N	LIBRE	\N	4	BIM	1	1	MESES	0
13	19940774	HUANCAYO	\N	6	\N	\N	2020-06-30	0	20	0	3	0	f		2020-09-28	0	\N	\N	4	0	0	2020-06-30	0	\N	0  	\N	LIBRE	\N	4	BIM	1	1	MESES	0
15	46041015	HUANCAYO	\N	6	\N	\N	2020-06-30	0	826.100000000000023	0	3	0	f		2020-09-28	0	\N	\N	4	0	0	2020-06-30	0	\N	0  	\N	LIBRE	\N	4	BIM	1	1	MESES	0
16	20113864	HUANCAYO	\N	6	\N	\N	2020-06-30	0	420	0	3	0	f		2020-09-28	0	\N	\N	4	0	0	2020-06-30	0	\N	0  	\N	LIBRE	\N	4	BIM	1	1	MESES	0
17	76621787	HUANCAYO	\N	6	\N	\N	2020-06-30	0	90	0	3	0	f		2020-09-28	0	\N	\N	4	0	0	2020-06-30	0	\N	0  	\N	LIBRE	\N	4	BIM	1	1	MESES	0
25	00061656	PUCALLPA	\N	2	\N	\N	2020-07-01	0	20500	3075	8	0	f		2021-02-26	0	\N	\N	15	0	0	2020-07-01	0	\N	0  	\N	PLAZO FIJO	\N	9	LARIAS	1	3	MESES	0
26	00098119	PUCALLPA	\N	\N	\N	\N	2020-07-01	0	0	0	3	0	f		2023-07-01	0	\N	\N	4	0	0	2020-07-01	0	\N	0  	\N	LIBRE	\N	9	LARIAS	0	3	AÑOS	0
23	45745478	HUANCAYO	\N	6	\N	\N	2020-06-30	0	570.42999999999995	0	1	0	f		2021-06-30	0	\N	\N	4	0	0	2020-06-30	0	\N	0  	\N	LIBRE	\N	4	BIM	1	1	AÑOS	0
22	19942242	HUANCAYO	\N	6	\N	\N	2020-06-30	0	1128.79999999999995	0	1	0	f		2021-06-30	0	\N	\N	4	0	0	2020-06-30	0	\N	0  	\N	LIBRE	\N	4	BIM	1	1	AÑOS	0
21	46561831	HUANCAYO	\N	6	\N	\N	2020-06-30	0	802	0	1	0	f		2021-06-30	0	\N	\N	4	0	0	2020-06-30	0	\N	0  	\N	LIBRE	\N	4	BIM	1	1	AÑOS	0
1	40129120	HUANCAYO	\N	6	\N	\N	2020-06-29	0	0	0	3	0	f		2020-09-27	0	\N	\N	4	0	0	2020-06-29	0	\N	0  	\N	CON INTERES PROGRAMADO	\N	4	BIM	1	1	MESES	0
27	00098119	PUCALLPA	\N	2	\N	\N	2020-07-01	0	0	0	3	0	f		2023-07-01	0	\N	\N	48	0	0	2020-07-01	0	\N	0  	\N	LIBRE	\N	9	LARIAS	1	3	AÑOS	0
31	19856332	HUANCAYO	\N	2	\N	\N	2020-07-02	0	10	0	6	0	f		2020-12-29	0	\N	\N	4	0	0	2020-07-02	0	\N	0  	\N	LIBRE	\N	4	BIM	1	1	MESES	0
30	20107243	HUANCAYO	\N	2	\N	\N	2020-07-02	0	480	0	3	0	f		2020-09-30	0	\N	\N	4	0	0	2020-07-02	0	\N	0  	\N	LIBRE	\N	4	BIM	1	1	MESES	0
28	20107243	HUANCAYO	\N	2	\N	\N	2020-07-02	0	20000	3000	10	0	f		2021-04-28	0	\N	\N	15	0	0	2020-07-02	0	\N	0  	\N	PLAZO FIJO	\N	4	BIM	1	1	MESES	0
29	20107243	HUANCAYO	\N	2	\N	\N	2020-07-02	0	2000	0	1	0	f		2020-08-01	0	\N	\N	0	0	0	2020-07-02	0	\N	0  	\N	LIBRE	\N	4	BIM	1	1	MESES	0
32	20648525	HUANCAYO	\N	2	\N	\N	2020-07-02	0	35	0	6	0	f		2020-12-29	0	\N	\N	4	0	0	2020-07-02	0	\N	0  	\N	LIBRE	\N	4	BIM	1	1	MESES	0
35	19944260	HUANCAYO	\N	\N	\N	\N	2020-07-04	0	0	0	1	0	f		2020-07-05	0	\N	\N	4	0	0	2020-07-04	0	\N	0  	\N	LIBRE	\N	4	BIM	0	1	DIAS	0
34	20064201	HUANCAYO	\N	2	\N	\N	2020-07-03	0	4220	126.599999999999994	3	0	f		2020-10-01	0	\N	\N	3	0	0	2020-07-03	0	\N	0  	\N	PLAZO FIJO	\N	4	BIM	1	1	MESES	0
36	00000000	HUANCAYO	\N	2	\N	\N	2020-07-04	0	925	0	9	0	f		2021-03-31	0	\N	\N	4	0	0	2020-07-04	0	\N	0  	\N	LIBRE	\N	4	BIM	1	1	MESES	0
37	46357683	HUANCAYO	\N	\N	\N	\N	2020-07-06	0	0	0	6	0	f		2021-01-02	0	\N	\N	4	0	0	2020-07-06	0	\N	0  	\N	LIBRE	\N	4	BIM	0	1	MESES	0
\.


--
-- Data for Name: metas_personal; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.metas_personal (id_metas_personal, fecha_hora, usuario, nro_creditos, nro_clientes, monto_otrogado, interes_otrogado, redondeo_otrogado, monto_saldo, interes_saldo, redondeo_saldo, monto_cobrado, interes_cobrado, redondeo_cobrado, monto_vencido, porcentaje, mora, noti, otros, nro_creditos_fin, nro_clientes_fin, monto_otrogado_fin, interes_otrogado_fin, redondeo_otrogado_fin, monto_saldo_fin, interes_saldo_fin, redondeo_saldo_fin, monto_cobrado_fin, interes_cobrado_fin, redondeo_cobrado_fin, monto_vencido_fin, porcentaje_fin, mora_cobrado, noti_cobrado, otros_cobrado, colacar_meta, saldo_meta, monto_vencido_meta, porcentaje_meta, creditos_meta, clientes_meta, cumplimiento, fecha_hora_cierre, id_usuario, id_agencia) FROM stdin;
\.


--
-- Data for Name: movimiento; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.movimiento (id_movimiento, id_libreta_ahorro, id_cajera, fecha_hora_movimiento, tipo_operacion, monto_movimiento, interes_movimiento, redondeo, obs, id_agencia, agencia, puntos, id_caja_apertura_cierre, cajera, estado, obs_cancelacion, concepto) FROM stdin;
8786	2	4	2020-06-30 10:12:01	DEPOSITO  	300	\N	\N	\N	0	HUANCAYO                      	1	3848	BIM                 	1	\N	CAPITAL
8787	3	4	2020-06-30 10:59:37	DEPOSITO  	400	\N	\N	\N	1	HUANCAYO                      	1	3848	BIM                 	1	\N	CAPITAL
8788	4	4	2020-06-30 12:18:20	DEPOSITO  	15	\N	\N	\N	1	HUANCAYO                      	1	3848	BIM                 	1	\N	CAPITAL
8789	12	4	2020-06-30 01:31:31	DEPOSITO  	300	\N	\N	\N	1	HUANCAYO                      	1	3848	BIM                 	1	\N	CAPITAL
8790	12	4	2020-06-30 01:31:52	DEPOSITO  	10	\N	\N	\N	1	HUANCAYO                      	1	3848	BIM                 	1	\N	CAPITAL
8791	13	4	2020-06-30 02:04:21	DEPOSITO  	15	\N	\N	\N	1	HUANCAYO                      	1	3848	BIM                 	1	\N	CAPITAL
8792	13	4	2020-06-30 02:04:55	DEPOSITO  	20	\N	\N	\N	1	HUANCAYO                      	1	3848	BIM                 	1	\N	CAPITAL
8793	15	4	2020-06-30 02:06:17	DEPOSITO  	826.100000000000023	\N	\N	\N	1	HUANCAYO                      	1	3848	BIM                 	1	\N	CAPITAL
8794	16	4	2020-06-30 02:07:44	DEPOSITO  	90	\N	\N	\N	1	HUANCAYO                      	1	3848	BIM                 	1	\N	CAPITAL
8795	16	4	2020-06-30 02:10:35	DEPOSITO  	330	\N	\N	\N	1	HUANCAYO                      	1	3848	BIM                 	1	\N	CAPITAL
8796	17	4	2020-06-30 02:11:56	DEPOSITO  	90	\N	\N	\N	1	HUANCAYO                      	1	3848	BIM                 	1	\N	CAPITAL
8797	18	4	2020-06-30 02:12:34	DEPOSITO  	355	\N	\N	\N	1	HUANCAYO                      	1	3848	BIM                 	1	\N	CAPITAL
8798	19	4	2020-06-30 02:13:22	DEPOSITO  	685	\N	\N	\N	1	HUANCAYO                      	1	3848	BIM                 	1	\N	CAPITAL
8799	20	4	2020-06-30 02:13:57	DEPOSITO  	5	\N	\N	\N	1	HUANCAYO                      	1	3848	BIM                 	1	\N	CAPITAL
8800	12	4	2020-06-30 02:15:52	DEPOSITO  	300	\N	\N	\N	1	HUANCAYO                      	1	3848	BIM                 	1	\N	CAPITAL
8801	23	4	2020-06-30 02:55:52	DEPOSITO  	570.42999999999995	\N	\N	\N	1	HUANCAYO                      	1	3848	BIM                 	1	\N	CAPITAL
8802	22	4	2020-06-30 02:58:51	DEPOSITO  	1128.79999999999995	\N	\N	\N	1	HUANCAYO                      	1	3848	BIM                 	1	\N	CAPITAL
8803	21	4	2020-06-30 03:00:07	DEPOSITO  	802	\N	\N	\N	1	HUANCAYO                      	1	3848	BIM                 	1	\N	CAPITAL
8804	25	9	2020-07-01 07:45:51	DEPOSITO  	20500	\N	\N	\N	3	PUCALLPA                      	1	3851	LARIAS              	1	\N	CAPITAL
8805	30	4	2020-07-02 09:42:38	DEPOSITO  	480	\N	\N	\N	1	HUANCAYO                      	1	3852	BIM                 	1	\N	CAPITAL
8806	29	4	2020-07-02 09:42:57	DEPOSITO  	4000	\N	\N	\N	1	HUANCAYO                      	1	3852	BIM                 	1	\N	CAPITAL
8807	28	4	2020-07-02 09:43:14	DEPOSITO  	20000	\N	\N	\N	1	HUANCAYO                      	1	3852	BIM                 	1	\N	CAPITAL
8808	29	4	2020-07-02 09:45:36	RETIRO    	2000	\N	\N	\N	1	HUANCAYO                      	1	3852	BIM                 	1	\N	
8809	31	4	2020-07-02 12:01:41	DEPOSITO  	10	\N	\N	\N	1	HUANCAYO                      	1	3852	BIM                 	1	\N	CAPITAL
8810	32	4	2020-07-02 12:09:01	DEPOSITO  	35	\N	\N	\N	1	HUANCAYO                      	1	3852	BIM                 	1	\N	CAPITAL
8811	12	4	2020-07-02 01:11:47	DEPOSITO  	10	\N	\N	\N	1	HUANCAYO                      	1	3852	BIM                 	1	\N	CAPITAL
8812	19	4	2020-07-03 09:44:40	RETIRO    	80.5999999999999943	\N	\N	\N	1	HUANCAYO                      	1	3853	BIM                 	1	\N	
8813	19	4	2020-07-03 10:37:58	RETIRO    	1.39999999999999991	\N	\N	\N	1	HUANCAYO                      	1	3853	BIM                 	1	\N	
8814	34	4	2020-07-03 02:42:09	DEPOSITO  	4220	\N	\N	\N	1	HUANCAYO                      	1	3853	BIM                 	1	\N	CAPITAL
8815	36	4	2020-07-04 01:35:08	DEPOSITO  	925	\N	\N	\N	1	HUANCAYO                      	1	3854	BIM                 	1	\N	CAPITAL
\.


--
-- Data for Name: movimiento_usuario; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.movimiento_usuario (id_movimiento_usuario, id_usuario, id_cajera, fecha_hora_movimiento, operacion, monto_operacion, sustento, agencia) FROM stdin;
\.


--
-- Data for Name: negocio; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.negocio (id_negocio, dni, nombre_actividad, ciiu, distrito, provincia, departamento, direccion, referencia, valorizacion, obs, estado_negocio, id_agencia, obs_modificacion) FROM stdin;
1	20413205	VENTA DE VERDURAS 	COMERCIO	CONCEPCIÒN	CONCEPCIÓN	JUNIN	JR. NUEVE DE JULIO 	FRENTE AL MERCADO DE CONCEPCION 	2500	VENTA DE VERDURAS	\N	\N	\N
2	43532534	BODEGA - MINI MARKET	COMERCIO	CONCEPCIÒN	CONCEPCIÓN	JUNIN	PSJ. SAN ANTONIO S/N	POR LA CAPILLA SAN ANTONIO	7000	VENTA DE BEBIDAS Y VERDURAS	\N	\N	\N
3	41332133	DOCENTE	SERVICIO	PICHANAQUI	CHANCHAMAYO	JUNIN	LOS ANGELES	A DOS CUANDRAS DEL PARQUE LOS ANGELES	2000	DICTADO DE CLASES	\N	\N	\N
4	47506173	VENTA DE CEBICHE 	SERVICIO	SAN AGUSTÍN DE CAJAS	HUANCAYO	JUNIN	JR. MARISCAL DE SUCRE S/N	POR EL CEMENTERIO DE SAN AGUSTIN 	5000	VENTA DE COMIDAS 	\N	\N	\N
5	41281404	BODEGA - VENTA DE ABARROTES Y OTROS	COMERCIO	SAN JERÓNIMO DE TUNÁN	HUANCAYO	JUNIN	JR. CATALINA HUANCA 198	A TRES CUADRAS DEL COLEGIO SANABRIA	15000	VENAT DE VERDURAS , FRUTAS, ABARROTES	\N	\N	\N
6	43075285	VENTA DE DESAYUNOS 	SERVICIO	CONCEPCIÒN	CONCEPCIÓN	JUNIN	JR. NUEVE DE JULIO S/N 	FRENTE AL MERCADO DE CONCEPCION 	3000	REALIZA LA VENTA DE MACA, SOYA ENTRE OTROS	\N	\N	\N
7	20400482	VENTA DE VERDURAS	COMERCIO	CONCEPCIÒN	CONCEPCIÓN	JUNIN	CARRETERA CENTRAL 1241	ENTRE RICARDO PALMA Y CARRETERA CENTRAL	7000	VENTA DE VERDURAS	\N	\N	\N
8	48017310	MOTOTAXI	SERVICIO	CONCEPCIÒN	CONCEPCIÓN	JUNIN	JR. RAMON CASTILLA S/N	A 10 CUADRAS DEL HOTEL HUAYCHULO	10000	VENTA DE SERVICIO DE MOTOTAXI	\N	\N	\N
\.


--
-- Data for Name: nivel_usuario; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.nivel_usuario (id_nivel_usuario, funcion, funciones) FROM stdin;
1	PLATAFORMA	PLATAFORMA
2	CAJA-PLATAFORMA	CAJA-PLATAFORMA
3	CAJA	CAJA
4	ANALISTA	ANALISTA
5	RECUPERADOR	RECUPERADOR
6	ASESOR LEGAL	ASESOR LEGAL
7	ADMINISTRADOR	ADMINISTRADOR
8	CONTABILIDAD	CONTABILIDAD
9	GERENCIA	GERENCIA
10	INFORMATICA	INFORMATICA
\.


--
-- Data for Name: notificaciones; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.notificaciones (id_notificaciones, id_solicitud, monto_vencido, dias_retraso, id_analista, id_tipo_notifi, fecha_gestion, costo, accion, fecha_accion, respuesta, observacion, usuario, telefono, tipo_direccion, direcccion, dni, id_agencia) FROM stdin;
\.


--
-- Data for Name: operacion; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.operacion (id_operacion, id_operacion_pago, id_solicitud, id_cajera, id_impresion, fecha_hora, concepto, monto, de_cuota, a_cuota, id_analista, id_agencia, capital, interes, redondeo) FROM stdin;
\.


--
-- Data for Name: operacion_banco; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.operacion_banco (id_operacion_banco, id_banco, operacion, monto, fecha_hora, usuario, agencia, cod_apertura, obs) FROM stdin;
\.


--
-- Data for Name: operacion_pago; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.operacion_pago (id_operacion_pago, id_caja_apertura_cierre, fecha_hora, total_pagar, pago_con, vuelto, agencia, puntos, id_agencia, obs_cancelacion) FROM stdin;
\.


--
-- Data for Name: pariente; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.pariente (id_pariente, dniuno, nombreuno, parentescouno, dnidos, nombredos, parentescodos, estado) FROM stdin;
1	47766965	EFFIO  MARQUEZ STEPHANIE ANGGELINA	PADRE	47766965	EFFIO  MARQUEZ STEPHANIE ANGGELINA	HIJO(A)	ACTIVO
\.


--
-- Data for Name: planilla; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.planilla (id_planilla, cod_generacion, cod_aprobacion, cod_planilla, fecha_hora, personal, cargo, dni, sueldo, incentivos, bonificacion, faltantes, movil, tardanza, adelantos, uniforme, permisos, sanciones, otros, aporte_afp, aporte_onp, desc_total, neto_pagar, estado, oficina, fecha_hora_ejecutado) FROM stdin;
\.


--
-- Data for Name: provincia; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.provincia (id_provincia, id_departamento, provincia) FROM stdin;
1	1	CHANCHAMAYO
2	1	CHUPACA
3	1	CONCEPCIÓN
4	1	HUANCAYO
5	1	JAUJA
6	1	SATIPO
7	1	TARMA
8	1	YAULI
9	3	LEONCIO PRADO
10	4	PUCALLPA
\.


--
-- Data for Name: rpt_cobranza_diaria; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.rpt_cobranza_diaria (id_rpt_cobranza_diaria, fecha_hora, id_apertura, nombre_user, monto_inicio_total, monto_inicio_hoy, monto_fin_total, interes_fin_hoy, creditos_total, creditos_hoy, id_agencia) FROM stdin;
1	2020-07-07 02:07:34	4	4	0	0	\N	\N	\N	\N	2
\.


--
-- Data for Name: rpt_creditos_colocados_historial; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.rpt_creditos_colocados_historial (id_rpt_creditos_colocados_historial, fecha_hora, id_analista, nombre_analista, nro_creditos, nro_clientes, monto_otrogado, interes_otrogado, redondeo_otrogado, monto_saldo, interes_saldo, redondeo_saldo, monto_cobrado, interes_cobrado, redondeo_cobrado, diario_1_7, diario_8_30, diario_mas_30, total_diario, semanal_1_7, semanal_8_30, semanal_mas_30, total_semanal, total_general, id_agencia) FROM stdin;
\.


--
-- Data for Name: rpt_movimientos_historial; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.rpt_movimientos_historial (id_rpt_movimientos_historial, fecha_hora, nro_creditos, nro_clientes, monto_otrogado, interes_otrogado, redondeo_otrogado, monto_saldo, interes_saldo, redondeo_saldo, monto_cobrado, interes_cobrado, redondeo_cobrado, monto_vencido, porcentaje, mora_generadas, mora_pagada, mora_saldo, noti_generadas, noti_pagada, noti_saldo, otros_generadas, otros_pagada, otros_saldo, id_agencia) FROM stdin;
\.


--
-- Data for Name: sectores; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.sectores (id_sector, sector, nombre, id_analista, obs) FROM stdin;
\.


--
-- Data for Name: solicitud_credito; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.solicitud_credito (id_solicitud, nombre_analista, id_analista, id_administrador, fecha_hora_solicitud, tipo_credito, producto_credito, dni, id_conyuge, tipo_garantia_cliente, descripcion_garantia_cliente, valorizacion_garantia_cliente, id_cliente_aval, tipo_garantia_aval, descripcion_garantia_aval, valorizacion_garantia_aval, id_negocio, interes, monto_interes, periodo, meses, penalidad, fecha_inicio_pago, monto_prestado, nro_cuotas, redondeo_total, fecha_finalizacion, obs, obs_internas, estado, nro_credito, con_boleta, nro_boleta, fecha_hora_desembolso, id_cajera, estado_credito, capital_x_cuota, interes_x_cuota, redondeo_x_cuota, cuota_a_pagar, capital_x_pagar, interes_x_pagar, redondeo_x_pagar, penalidad_x_pagar, cuotas_x_pagar, monto_x_pagar, cuotas_vencidas, monto_vencido, moras, moras_pagadas, notificaciones, notificaciones_pagadas, otros_pagos, otros_pagos_pagadas, ult_dia_pago, id_admin_condonacion, fecha_hora_condonacion, monto_condonacion, detalle_condonacion, descuento_precancelacion, id_ficha, dias, noti_acomulada, oficina, calificacion, area, fecha_cierre, contrato, agencia, penalidades_pagadas, hora_pagar, pago_en, obs_negacion, id_caja_apertura_cierre, serie) FROM stdin;
1	FIORELA CARBAJAL CHAO SING	3	2	2020-07-07 02:03:05	PRINCIPAL	CAPITAL DE TRABAJO	20413205	0	DOCUMENTO DE VIVIENDA	COMPRA Y VENTA DE VIVIENDA 	15000	0	NINGUNO		0	1	7	24.2300000000000004	0	1	0	2020-07-08	300	30	0	2020-08-12		\N	1	7720201920      	f	\N	2020-07-07 14:11:51.861	4	0	10	0.810000000000000053	0	10.8000000000000007	300	24.2300000000000004	0	0	30	324	0	0	0	0	0	0	0.5	0	2020-07-07 14:11:51.861	\N	\N	0	\N	0	0	\N	\N	CONCEPCIÓN                                        	\N	\N	\N	CONSTE POR EL PRESENTE CONTRATO DE COMPRA  Y VENTA QUE OTORGA DE UNA PARTE, COMO VENDEDOR (CONTRAYENTE DE DEUDA) SEÑOR(A): HINOSTROZA HURTADO MARINA IRAYDA, IDENTIFICADO (A) CON DNI: 20413205,  DOMICILIADO EN ,  DISTRITO DE , PROVINCIA DE , DEPARTAMENTO DE , Y DE  OTRA PARTE EN CALIDAD DE COMPRADOR EL SEÑOR ALAIN RAVICHAGUA SOTELO, IDENTIFICADO CON DNI: 44892319 DOMICILIADO EN PROG. CUZCO N° 2309, DISTRITO DE HUANCAYO, PROVINCIA DE HUANCAYO, DEPARTAMENTO DE JUNÍN. BAJO LOS TÉRMINOS Y CONDICIONES SIGUIENTES:	\N	\N	12:15:00-05	0         	\N	3856	\N
2	JAVIER ORLANDO HUAMAN DURAND	2	2	2020-07-23 06:37:46	PRINCIPAL	CAPITAL DE TRABAJO	43532534	0	NINGUNO	sin garantia	3000	0	NINGUNO		0	2	7.5	43.2700000000000031	0	1	0	2020-07-24	500	30	0	2020-08-28	cliente con muy buena ubicacion del negocio	\N	0	\N	\N	\N	\N	\N	\N	16.6700000000000017	1.43999999999999995	0	18.1000000000000014	500	43.2700000000000031	0	0	30	543	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2	\N	\N	CONCEPCIÓN                                        	\N	\N	\N	CONSTE POR EL PRESENTE CONTRATO DE COMPRA  Y VENTA QUE OTORGA DE UNA PARTE, COMO VENDEDOR (CONTRAYENTE DE DEUDA) SEÑOR(A): ESPINOZA  DE LA CRUZ CARMEN, IDENTIFICADO (A) CON DNI: 43532534,  DOMICILIADO EN ,  DISTRITO DE , PROVINCIA DE , DEPARTAMENTO DE , Y DE  OTRA PARTE EN CALIDAD DE COMPRADOR EL SEÑOR ALAIN RAVICHAGUA SOTELO, IDENTIFICADO CON DNI: 44892319 DOMICILIADO EN PROG. CUZCO N° 2309, DISTRITO DE HUANCAYO, PROVINCIA DE HUANCAYO, DEPARTAMENTO DE JUNÍN. BAJO LOS TÉRMINOS Y CONDICIONES SIGUIENTES:	\N	\N	15:00:00-05	0         	\N	\N	\N
3	BEATRIZ ANGELA HUAMAN DURAND	7	0	2020-07-27 07:01:15	PRINCIPAL	ESTUDIO	41332133	0	DOCUMENTO DE VIVIENDA	material rustico, 200mm2 	20000	0	NINGUNO		0	3	13	65.0000000000004974	3	1	0	2020-08-26	500	1	0	2020-08-26	credito destinado para estudios	\N	0	\N	\N	\N	\N	\N	\N	500	65.5	0	565	500	65.0000000000004974	0	0	1	565	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	3	\N	\N	PICHANAKI                                         	\N	\N	\N	CONSTE POR EL PRESENTE CONTRATO DE COMPRA  Y VENTA QUE OTORGA DE UNA PARTE, COMO VENDEDOR (CONTRAYENTE DE DEUDA) SEÑOR(A): AQUINO QUISPE MIRKO TOLENTINO, IDENTIFICADO (A) CON DNI: 41332133,  DOMICILIADO EN ,  DISTRITO DE , PROVINCIA DE , DEPARTAMENTO DE , Y DE  OTRA PARTE EN CALIDAD DE COMPRADOR EL SEÑOR ALAIN RAVICHAGUA SOTELO, IDENTIFICADO CON DNI: 44892319 DOMICILIADO EN PROG. CUZCO N° 2309, DISTRITO DE HUANCAYO, PROVINCIA DE HUANCAYO, DEPARTAMENTO DE JUNÍN. BAJO LOS TÉRMINOS Y CONDICIONES SIGUIENTES:	\N	\N	06:00:00-05	1         	\N	\N	\N
4	FIORELA CARBAJAL CHAO SING	3	2	2020-08-14 11:15:09	PRINCIPAL	CAPITAL DE TRABAJO	41281404	0	DOCUMENTO DE VIVIENDA	compra y venta 	60000	0	NINGUNO		0	5	10.8000002	735.480000000000018	1	3	0	2020-08-21	2270	12	0	2020-11-06	cliente cuenta con buena capacidad de pago y diversos ingresos 	\N	0	\N	\N	\N	\N	\N	\N	189.166666666666998	61.2899999999999991	0	250.5	2270	735.480000000000018	0	0	12	3006	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	6	\N	\N	CONCEPCIÓN                                        	\N	\N	\N	CONSTE POR EL PRESENTE CONTRATO DE COMPRA  Y VENTA QUE OTORGA DE UNA PARTE, COMO VENDEDOR (CONTRAYENTE DE DEUDA) SEÑOR(A): SANTIVAÑEZ DAVILA LIZ ANGELA, IDENTIFICADO (A) CON DNI: 41281404,  DOMICILIADO EN ,  DISTRITO DE , PROVINCIA DE , DEPARTAMENTO DE , Y DE  OTRA PARTE EN CALIDAD DE COMPRADOR EL SEÑOR ALAIN RAVICHAGUA SOTELO, IDENTIFICADO CON DNI: 44892319 DOMICILIADO EN PROG. CUZCO N° 2309, DISTRITO DE HUANCAYO, PROVINCIA DE HUANCAYO, DEPARTAMENTO DE JUNÍN. BAJO LOS TÉRMINOS Y CONDICIONES SIGUIENTES:	\N	\N	15:00:00-05	0         	\N	\N	\N
5	FIORELA CARBAJAL CHAO SING	3	2	2020-08-15 10:28:29	PRINCIPAL	CAPITAL DE TRABAJO	43075285	0	PRENDIARIOS	deja en garantia cocina indura	1200	0	NINGUNO		0	6	14.1999998	511.199999999999989	2	4	0	2020-08-31	900	8	0	2020-12-15	cliente pagara en oficina y tambien podremos realizar cobranza en campo con previa autorizacion de gerencia	\N	0	\N	\N	\N	\N	\N	\N	112.5	63.8999999999999986	0	176.400000000000006	900	511.199999999999989	0	0	8	1411.20000000000005	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	7	\N	\N	CONCEPCIÓN                                        	\N	\N	\N	CONSTE POR EL PRESENTE CONTRATO DE COMPRA  Y VENTA QUE OTORGA DE UNA PARTE, COMO VENDEDOR (CONTRAYENTE DE DEUDA) SEÑOR(A): CAINICELA DE LA O  NANCY ROSALVINA, IDENTIFICADO (A) CON DNI: 43075285,  DOMICILIADO EN ,  DISTRITO DE , PROVINCIA DE , DEPARTAMENTO DE , Y DE  OTRA PARTE EN CALIDAD DE COMPRADOR EL SEÑOR ALAIN RAVICHAGUA SOTELO, IDENTIFICADO CON DNI: 44892319 DOMICILIADO EN PROG. CUZCO N° 2309, DISTRITO DE HUANCAYO, PROVINCIA DE HUANCAYO, DEPARTAMENTO DE JUNÍN. BAJO LOS TÉRMINOS Y CONDICIONES SIGUIENTES:	\N	\N	15:00:00-05	1         	\N	\N	\N
\.


--
-- Data for Name: sueldo_movimiento; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.sueldo_movimiento (id_sueldo_movimiento, id_personal, detalle_sueldo, monto, fecha_hora, estado, oficina, cajera, dni, obs, fecha_hora_ejecutado, agencia, cod_cierre, detalle_devolucion, fecha_hora_devuelto, user_devolucion, monto_devuelto, obs_cancelacion, id_agencia) FROM stdin;
18722	4	FALTANTE	6215.8999	2020-07-01 14:06:02.97257	f	HUANCAYO	BIM	\N	\N	\N	\N	\N	\N	\N	\N	0	\N	\N
18723	4	FALTANTE	1015.40002	2020-07-01 18:33:11.497307	f	HUANCAYO	BIM	\N	\N	\N	\N	\N	\N	\N	\N	0	\N	\N
18724	4	FALTANTE	18302.6992	2020-07-02 14:15:05.061587	f	HUANCAYO	BIM	\N	\N	\N	\N	\N	\N	\N	\N	0	\N	\N
18725	4	FALTANTE	343.399994	2020-07-03 14:55:18.207404	f	HUANCAYO	BIM	\N	\N	\N	\N	\N	\N	\N	\N	0	\N	\N
18726	4	FALTANTE	241.600006	2020-07-04 14:05:17.54667	f	HUANCAYO	BIM	\N	\N	\N	\N	\N	\N	\N	\N	0	\N	\N
\.


--
-- Data for Name: tipo_ahorro; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.tipo_ahorro (id_tipo_ahorro, nombre_ahorro, obs) FROM stdin;
1	LIBRE	\N
4	CAMPAÑA	\N
2	PLAZO FIJO	\N
3	CON INTERES PROGRAMADO	\N
\.


--
-- Data for Name: tipo_notificacion; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.tipo_notificacion (id_tipo_notifi, nombre_notificacion, obs, valor) FROM stdin;
1	AVISO	NINGUNA	0
2	NOTI_NOR	NINGUNA	5
3	ULT_NOTI	NINGUNA	5
4	CAMPAÑA	NINGUNA	0
\.


--
-- Data for Name: usuario; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.usuario (id_usuario, apellidos, nombres, tipo_usuario, direccion, telefono, email, login, pass, obs, activado, salario, salario_actual, agencia, dni, ingreso_temprano, ingreso_tarde, ingreso_sabados, estado_sueldo, fecha_ingreso, movil, uniforme, aporte_afp, aporte_onp, id_agencia, creacion, cargo) FROM stdin;
1	SOPORTE	SOPORTE	9	NO	NO	NO	SOPORTE	2508	\N	t	0	0	1	44892319	\N	\N	\N	f	2020-03-12	0	0	0	0	1	2020-06-26 18:25:18.217989	\N
2	HUAMAN DURAND	JAVIER ORLANDO	9	JR. HUANCAYO 289	994444296	javi_ange_12@hotmail.com	JHUAMAN	orlando2320		t	2500	0	2	40668463	\N	\N	\N	f	2020-07-07	0	0	0	0	2	2020-07-07 13:22:04.725736	\N
3	CARBAJAL CHAO SING	FIORELA	4	JR. HUANCAYO 271	921630537		LCARBAJAL	1		t	950	0	2	45258564	\N	\N	\N	f	2020-07-07	0	0	0	0	2	2020-07-07 13:26:36.540247	\N
4	HUAMAN DURAND	SOLEDAD	2	JR. HUANCAYO 265	958427832		SHUAMAN	1		t	600	0	2	41514549	\N	\N	\N	f	2020-07-07	0	0	0	0	2	2020-07-07 13:29:37.852217	\N
5	VILCA TERREROS	JESUS ARTURO	4	AV.LEANDRA TORRES # 278	981663986	thesun_183@hotmail.com	JVILCA	1		t	940	0	2	44505690	\N	\N	\N	f	2020-07-13	0	0	0	0	1	2020-07-13 09:51:15.49666	\N
6	HUAMAN DURAND	MISHEL FABIOLA	2	JR. 7 DE JUNIO  # 370	935787187		FHUAMAN	1		t	350	0	2	47494415	\N	\N	\N	f	2020-07-15	0	0	0	0	3	2020-07-15 09:18:24.002094	\N
7	HUAMAN DURAND	BEATRIZ ANGELA	4	JR.PRIMERO DE MAYO # 846	928957963	BEA_ANGEL@HOTMAIL.COM	ADURAND	1		t	500	0	2	45640541	\N	\N	\N	f	2020-07-15	0	0	0	0	3	2020-07-15 09:23:00.774227	\N
\.


--
-- Name: activar_reportes_id_activar_rpt_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.activar_reportes_id_activar_rpt_seq', 1, false);


--
-- Name: agencia_id_agencia_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.agencia_id_agencia_seq', 1, false);


--
-- Name: agencia_id_empresa_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.agencia_id_empresa_seq', 1, false);


--
-- Name: apertura_cierre_id_caja_apertura_cierre_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.apertura_cierre_id_caja_apertura_cierre_seq', 3856, true);


--
-- Name: aportaciones_id_aportaciones_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.aportaciones_id_aportaciones_seq', 1, false);


--
-- Name: banco_id_banco_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.banco_id_banco_seq', 2, true);


--
-- Name: billeteo_id_billeteo_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.billeteo_id_billeteo_seq', 1, false);


--
-- Name: boleta_id_boleta_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.boleta_id_boleta_seq', 1, false);


--
-- Name: boveda_id_boveda_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.boveda_id_boveda_seq', 6, true);


--
-- Name: campana_id_campana_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.campana_id_campana_seq', 1, false);


--
-- Name: central_riesgo_id_central_riesgo_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.central_riesgo_id_central_riesgo_seq', 1, false);


--
-- Name: cierre_caja_principal_id_cierre_principal_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.cierre_caja_principal_id_cierre_principal_seq', 3619, true);


--
-- Name: cierre_planilla_id_cierre_planilla_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.cierre_planilla_id_cierre_planilla_seq', 1, false);


--
-- Name: clave_cliente_id_clave_cliente_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.clave_cliente_id_clave_cliente_seq', 1, false);


--
-- Name: cliente_aval_id_cliente_aval_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.cliente_aval_id_cliente_aval_seq', 3576, true);


--
-- Name: cliente_beneficiario_id_cliente_beneficiario_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.cliente_beneficiario_id_cliente_beneficiario_seq', 405, true);


--
-- Name: compromiso_id_compromiso_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.compromiso_id_compromiso_seq', 1, false);


--
-- Name: conyuge_id_conyuge_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.conyuge_id_conyuge_seq', 3351, true);


--
-- Name: credito_refinanciado_id_refinanciado_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.credito_refinanciado_id_refinanciado_seq', 1, false);


--
-- Name: cronograma_ahorro_id_cronograma_ahorro_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.cronograma_ahorro_id_cronograma_ahorro_seq', 39, true);


--
-- Name: cronograma_pagos_id_cronograma_pago_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.cronograma_pagos_id_cronograma_pago_seq', 30, true);


--
-- Name: departamento_id_departamento_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.departamento_id_departamento_seq', 1, false);


--
-- Name: detalle_negocio_id_detalle_negocio_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.detalle_negocio_id_detalle_negocio_seq', 1, false);


--
-- Name: detalle_negocio_id_negocio_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.detalle_negocio_id_negocio_seq', 1, false);


--
-- Name: dias_festivos_id_agencia_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.dias_festivos_id_agencia_seq', 1, false);


--
-- Name: dias_festivos_id_dia_festivo_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.dias_festivos_id_dia_festivo_seq', 1, false);


--
-- Name: distrito_id_distrito_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.distrito_id_distrito_seq', 1, false);


--
-- Name: distrito_id_provincia_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.distrito_id_provincia_seq', 1, false);


--
-- Name: empresa_id_empresa_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.empresa_id_empresa_seq', 1, false);


--
-- Name: equipos_id_agencia_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.equipos_id_agencia_seq', 1, false);


--
-- Name: equipos_id_aquipos_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.equipos_id_aquipos_seq', 1, false);


--
-- Name: ficha_domiciliaria_id_ficha_domiciliaria_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.ficha_domiciliaria_id_ficha_domiciliaria_seq', 1, false);


--
-- Name: ficha_economica_id_ficha_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.ficha_economica_id_ficha_seq', 1, false);


--
-- Name: gastos_operativos_id_gastos_operativos_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.gastos_operativos_id_gastos_operativos_seq', 1, false);


--
-- Name: gastos_operativos_id_negocio_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.gastos_operativos_id_negocio_seq', 1, false);


--
-- Name: historial_manipulacion_id_historial_manipulacion_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.historial_manipulacion_id_historial_manipulacion_seq', 82378, true);


--
-- Name: historial_manipulacion_id_usuario_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.historial_manipulacion_id_usuario_seq', 1, false);


--
-- Name: historial_visitas_id_historial_visitas_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.historial_visitas_id_historial_visitas_seq', 8081, true);


--
-- Name: ingreso_egreso_id_ingreso_egreso_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.ingreso_egreso_id_ingreso_egreso_seq', 9, true);


--
-- Name: insumo_negocio_id_detalle_negocio_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.insumo_negocio_id_detalle_negocio_seq', 1, false);


--
-- Name: insumo_negocio_id_insumo_negocio_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.insumo_negocio_id_insumo_negocio_seq', 1, false);


--
-- Name: inventario_bienes_id_inventario_bienes_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.inventario_bienes_id_inventario_bienes_seq', 1, false);


--
-- Name: libreta_ahorro_id_libreta_ahorro_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.libreta_ahorro_id_libreta_ahorro_seq', 383, true);


--
-- Name: metas_personal_id_metas_personal_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.metas_personal_id_metas_personal_seq', 1, false);


--
-- Name: movimiento_id_movimiento_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.movimiento_id_movimiento_seq', 8815, true);


--
-- Name: movimiento_usuario_id_movimiento_usuario_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.movimiento_usuario_id_movimiento_usuario_seq', 1, false);


--
-- Name: negocio_id_negocio_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.negocio_id_negocio_seq', 1, false);


--
-- Name: nivel_usuario_id_nivel_usuario_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.nivel_usuario_id_nivel_usuario_seq', 1, false);


--
-- Name: notificaciones_id_notificaciones_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.notificaciones_id_notificaciones_seq', 12584, true);


--
-- Name: operacion_banco_id_operacion_banco_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.operacion_banco_id_operacion_banco_seq', 3, true);


--
-- Name: operacion_id_operacion_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.operacion_id_operacion_seq', 8, true);


--
-- Name: operacion_pago_id_operacion_pago_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.operacion_pago_id_operacion_pago_seq', 6, true);


--
-- Name: pariente_id_pariente_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.pariente_id_pariente_seq', 1, false);


--
-- Name: planilla_id_planilla_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.planilla_id_planilla_seq', 1, false);


--
-- Name: provincia_id_departamento_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.provincia_id_departamento_seq', 1, false);


--
-- Name: provincia_id_provincia_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.provincia_id_provincia_seq', 1, false);


--
-- Name: rpt_cobranza_diaria_id_rpt_cobranza_diaria_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.rpt_cobranza_diaria_id_rpt_cobranza_diaria_seq', 1, true);


--
-- Name: rpt_creditos_colocados_histor_id_rpt_creditos_colocados_his_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.rpt_creditos_colocados_histor_id_rpt_creditos_colocados_his_seq', 1, false);


--
-- Name: sectores_id_sector_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.sectores_id_sector_seq', 1, false);


--
-- Name: solicitud_credito_id_solicitud_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.solicitud_credito_id_solicitud_seq', 1, false);


--
-- Name: sueldo_movimiento_id_sueldo_movimiento_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.sueldo_movimiento_id_sueldo_movimiento_seq', 18726, true);


--
-- Name: tipo_ahorro_id_tipo_ahorro_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.tipo_ahorro_id_tipo_ahorro_seq', 1, false);


--
-- Name: tipo_notificacion_id_tipo_notifi_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.tipo_notificacion_id_tipo_notifi_seq', 1, false);


--
-- Name: usuario_id_usuario_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.usuario_id_usuario_seq', 1, false);


--
-- Name: detalle_negocio id_detalle_negocio; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.detalle_negocio
    ADD CONSTRAINT id_detalle_negocio PRIMARY KEY (id_detalle_negocio);


--
-- Name: gastos_operativos id_gastos_operativos; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.gastos_operativos
    ADD CONSTRAINT id_gastos_operativos PRIMARY KEY (id_gastos_operativos);


--
-- Name: grantia_real id_grantia_real; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.grantia_real
    ADD CONSTRAINT id_grantia_real PRIMARY KEY (id_grantia_real);


--
-- Name: historial_visitas id_historial_visitas; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.historial_visitas
    ADD CONSTRAINT id_historial_visitas PRIMARY KEY (id_historial_visitas);


--
-- Name: insumo_negocio id_insumo_negocio; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.insumo_negocio
    ADD CONSTRAINT id_insumo_negocio PRIMARY KEY (id_insumo_negocio);


--
-- Name: metas_personal id_metas_personal; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.metas_personal
    ADD CONSTRAINT id_metas_personal PRIMARY KEY (id_metas_personal);


--
-- Name: negocio id_negocio; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.negocio
    ADD CONSTRAINT id_negocio PRIMARY KEY (id_negocio);


--
-- Name: rpt_cobranza_diaria id_rpt_cobranza_diaria; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.rpt_cobranza_diaria
    ADD CONSTRAINT id_rpt_cobranza_diaria PRIMARY KEY (id_rpt_cobranza_diaria);


--
-- Name: rpt_creditos_colocados_historial id_rpt_creditos_colocados_historial; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.rpt_creditos_colocados_historial
    ADD CONSTRAINT id_rpt_creditos_colocados_historial PRIMARY KEY (id_rpt_creditos_colocados_historial);


--
-- Name: rpt_movimientos_historial id_rpt_movimientos_historial; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.rpt_movimientos_historial
    ADD CONSTRAINT id_rpt_movimientos_historial PRIMARY KEY (id_rpt_movimientos_historial);


--
-- Name: fotos logos_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.fotos
    ADD CONSTRAINT logos_pkey PRIMARY KEY (codigo);


--
-- Name: activar_reportes pk_activar_rpt; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.activar_reportes
    ADD CONSTRAINT pk_activar_rpt PRIMARY KEY (id_activar_rpt);


--
-- Name: agencia pk_agencia; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.agencia
    ADD CONSTRAINT pk_agencia PRIMARY KEY (id_agencia);


--
-- Name: apertura_cierre pk_apertura_cierre; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.apertura_cierre
    ADD CONSTRAINT pk_apertura_cierre PRIMARY KEY (id_caja_apertura_cierre);


--
-- Name: ingreso_egreso pk_apertura_egreso; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ingreso_egreso
    ADD CONSTRAINT pk_apertura_egreso PRIMARY KEY (id_ingreso_egreso);


--
-- Name: aportaciones pk_aportaciones; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.aportaciones
    ADD CONSTRAINT pk_aportaciones PRIMARY KEY (id_aportaciones);


--
-- Name: banco pk_banco; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.banco
    ADD CONSTRAINT pk_banco PRIMARY KEY (id_banco);


--
-- Name: billeteo pk_billeteo; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.billeteo
    ADD CONSTRAINT pk_billeteo PRIMARY KEY (id_billeteo);


--
-- Name: boleta pk_boleta; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.boleta
    ADD CONSTRAINT pk_boleta PRIMARY KEY (id_boleta);


--
-- Name: boveda pk_boveda; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.boveda
    ADD CONSTRAINT pk_boveda PRIMARY KEY (id_boveda);


--
-- Name: campana pk_campana; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.campana
    ADD CONSTRAINT pk_campana PRIMARY KEY (id_campana);


--
-- Name: central_riesgo pk_central_riesgo; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.central_riesgo
    ADD CONSTRAINT pk_central_riesgo PRIMARY KEY (id_central_riesgo);


--
-- Name: cierre_planilla pk_cierre_planilla; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cierre_planilla
    ADD CONSTRAINT pk_cierre_planilla PRIMARY KEY (id_cierre_planilla);


--
-- Name: ciiu pk_ciiu; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ciiu
    ADD CONSTRAINT pk_ciiu PRIMARY KEY (id_ciiu);


--
-- Name: clave_cliente pk_clave_cliente; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.clave_cliente
    ADD CONSTRAINT pk_clave_cliente PRIMARY KEY (id_clave_cliente);


--
-- Name: cliente pk_cliente; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cliente
    ADD CONSTRAINT pk_cliente PRIMARY KEY (dni);


--
-- Name: cliente_aval pk_cliente_aval; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cliente_aval
    ADD CONSTRAINT pk_cliente_aval PRIMARY KEY (id_cliente_aval);


--
-- Name: cliente_beneficiario pk_cliente_beneficiario; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cliente_beneficiario
    ADD CONSTRAINT pk_cliente_beneficiario PRIMARY KEY (id_cliente_beneficiario);


--
-- Name: compromiso pk_compromiso; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.compromiso
    ADD CONSTRAINT pk_compromiso PRIMARY KEY (id_compromiso);


--
-- Name: conyuge pk_conyuge; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.conyuge
    ADD CONSTRAINT pk_conyuge PRIMARY KEY (id_conyuge);


--
-- Name: credito_refinanciado pk_credito_refinanciado; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.credito_refinanciado
    ADD CONSTRAINT pk_credito_refinanciado PRIMARY KEY (id_refinanciado);


--
-- Name: cronograma_ahorro pk_cronograma_ahorro; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cronograma_ahorro
    ADD CONSTRAINT pk_cronograma_ahorro PRIMARY KEY (id_cronograma_ahorro);


--
-- Name: cronograma_pagos pk_cronograma_pago; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cronograma_pagos
    ADD CONSTRAINT pk_cronograma_pago PRIMARY KEY (id_cronograma_pago);


--
-- Name: departamento pk_departamento; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.departamento
    ADD CONSTRAINT pk_departamento PRIMARY KEY (id_departamento);


--
-- Name: dias_festivos pk_dia_festivo; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.dias_festivos
    ADD CONSTRAINT pk_dia_festivo PRIMARY KEY (id_dia_festivo);


--
-- Name: distrito pk_distrito; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.distrito
    ADD CONSTRAINT pk_distrito PRIMARY KEY (id_distrito);


--
-- Name: empresa pk_empresa; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.empresa
    ADD CONSTRAINT pk_empresa PRIMARY KEY (id_empresa);


--
-- Name: equipos pk_equipos; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.equipos
    ADD CONSTRAINT pk_equipos PRIMARY KEY (id_aquipos);


--
-- Name: ficha_domiciliaria pk_ficha_domiciliaria; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ficha_domiciliaria
    ADD CONSTRAINT pk_ficha_domiciliaria PRIMARY KEY (id_ficha_domiciliaria);


--
-- Name: ficha_economica pk_ficha_economica; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ficha_economica
    ADD CONSTRAINT pk_ficha_economica PRIMARY KEY (id_ficha);


--
-- Name: cierre_caja_principal pk_id_caja_principal; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cierre_caja_principal
    ADD CONSTRAINT pk_id_caja_principal PRIMARY KEY (id_cierre_principal);


--
-- Name: historial_manipulacion pk_id_historial_manipulacion; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.historial_manipulacion
    ADD CONSTRAINT pk_id_historial_manipulacion PRIMARY KEY (id_historial_manipulacion);


--
-- Name: inventario_bienes pk_inventario_bienes; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.inventario_bienes
    ADD CONSTRAINT pk_inventario_bienes PRIMARY KEY (id_inventario_bienes);


--
-- Name: libreta_ahorro pk_libreta_ahorro; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.libreta_ahorro
    ADD CONSTRAINT pk_libreta_ahorro PRIMARY KEY (id_libreta_ahorro);


--
-- Name: movimiento pk_movimiento; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.movimiento
    ADD CONSTRAINT pk_movimiento PRIMARY KEY (id_movimiento);


--
-- Name: movimiento_usuario pk_movimiento_usuario; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.movimiento_usuario
    ADD CONSTRAINT pk_movimiento_usuario PRIMARY KEY (id_movimiento_usuario);


--
-- Name: nivel_usuario pk_nivel_usuario; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.nivel_usuario
    ADD CONSTRAINT pk_nivel_usuario PRIMARY KEY (id_nivel_usuario);


--
-- Name: notificaciones pk_notificaciones; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.notificaciones
    ADD CONSTRAINT pk_notificaciones PRIMARY KEY (id_notificaciones);


--
-- Name: operacion pk_operacion; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.operacion
    ADD CONSTRAINT pk_operacion PRIMARY KEY (id_operacion);


--
-- Name: operacion_banco pk_operacion_banco; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.operacion_banco
    ADD CONSTRAINT pk_operacion_banco PRIMARY KEY (id_operacion_banco);


--
-- Name: operacion_pago pk_operacion_pago; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.operacion_pago
    ADD CONSTRAINT pk_operacion_pago PRIMARY KEY (id_operacion_pago);


--
-- Name: pariente pk_pariente; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.pariente
    ADD CONSTRAINT pk_pariente PRIMARY KEY (id_pariente);


--
-- Name: planilla pk_planilla; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.planilla
    ADD CONSTRAINT pk_planilla PRIMARY KEY (id_planilla);


--
-- Name: provincia pk_provincia; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.provincia
    ADD CONSTRAINT pk_provincia PRIMARY KEY (id_provincia);


--
-- Name: sectores pk_sectores; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sectores
    ADD CONSTRAINT pk_sectores PRIMARY KEY (id_sector);


--
-- Name: solicitud_credito pk_solicitud_credito; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.solicitud_credito
    ADD CONSTRAINT pk_solicitud_credito PRIMARY KEY (id_solicitud);


--
-- Name: sueldo_movimiento pk_sueldo_movimiento; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sueldo_movimiento
    ADD CONSTRAINT pk_sueldo_movimiento PRIMARY KEY (id_sueldo_movimiento);


--
-- Name: tipo_ahorro pk_tipo_ahorro; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tipo_ahorro
    ADD CONSTRAINT pk_tipo_ahorro PRIMARY KEY (id_tipo_ahorro);


--
-- Name: tipo_notificacion pk_tipo_notificacion; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tipo_notificacion
    ADD CONSTRAINT pk_tipo_notificacion PRIMARY KEY (id_tipo_notifi);


--
-- Name: usuario pk_usuario; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.usuario
    ADD CONSTRAINT pk_usuario PRIMARY KEY (id_usuario);


--
-- Name: nivel_usuario unico_funcion; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.nivel_usuario
    ADD CONSTRAINT unico_funcion UNIQUE (funcion);


--
-- Name: usuario unico_login; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.usuario
    ADD CONSTRAINT unico_login UNIQUE (login);


--
-- Name: central_riesgo dni; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.central_riesgo
    ADD CONSTRAINT dni FOREIGN KEY (dni) REFERENCES public.cliente(dni) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: cliente_aval dni; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cliente_aval
    ADD CONSTRAINT dni FOREIGN KEY (dni) REFERENCES public.cliente(dni) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: cliente_beneficiario dni; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cliente_beneficiario
    ADD CONSTRAINT dni FOREIGN KEY (dni) REFERENCES public.cliente(dni) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: conyuge dni; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.conyuge
    ADD CONSTRAINT dni FOREIGN KEY (dni) REFERENCES public.cliente(dni) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: solicitud_credito dni; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.solicitud_credito
    ADD CONSTRAINT dni FOREIGN KEY (dni) REFERENCES public.cliente(dni) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: negocio dni; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.negocio
    ADD CONSTRAINT dni FOREIGN KEY (dni) REFERENCES public.cliente(dni) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: inventario_bienes dni; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.inventario_bienes
    ADD CONSTRAINT dni FOREIGN KEY (dni) REFERENCES public.cliente(dni) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: grantia_real dni; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.grantia_real
    ADD CONSTRAINT dni FOREIGN KEY (dni) REFERENCES public.cliente(dni) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: equipos id_agencia; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.equipos
    ADD CONSTRAINT id_agencia FOREIGN KEY (id_agencia) REFERENCES public.agencia(id_agencia) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: provincia id_departamento; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.provincia
    ADD CONSTRAINT id_departamento FOREIGN KEY (id_departamento) REFERENCES public.departamento(id_departamento) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: insumo_negocio id_detalle_negocio; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.insumo_negocio
    ADD CONSTRAINT id_detalle_negocio FOREIGN KEY (id_detalle_negocio) REFERENCES public.detalle_negocio(id_detalle_negocio) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: agencia id_empresa; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.agencia
    ADD CONSTRAINT id_empresa FOREIGN KEY (id_empresa) REFERENCES public.empresa(id_empresa) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: cronograma_ahorro id_libreta_ahorro; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cronograma_ahorro
    ADD CONSTRAINT id_libreta_ahorro FOREIGN KEY (id_libreta_ahorro) REFERENCES public.libreta_ahorro(id_libreta_ahorro) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: detalle_negocio id_negocio; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.detalle_negocio
    ADD CONSTRAINT id_negocio FOREIGN KEY (id_negocio) REFERENCES public.negocio(id_negocio) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: gastos_operativos id_negocio; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.gastos_operativos
    ADD CONSTRAINT id_negocio FOREIGN KEY (id_negocio) REFERENCES public.negocio(id_negocio) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: distrito id_provincia; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.distrito
    ADD CONSTRAINT id_provincia FOREIGN KEY (id_provincia) REFERENCES public.provincia(id_provincia) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: credito_refinanciado id_solicitud; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.credito_refinanciado
    ADD CONSTRAINT id_solicitud FOREIGN KEY (id_solicitud) REFERENCES public.solicitud_credito(id_solicitud) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: notificaciones id_solicitud; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.notificaciones
    ADD CONSTRAINT id_solicitud FOREIGN KEY (id_solicitud) REFERENCES public.solicitud_credito(id_solicitud) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: cronograma_pagos id_solicitud; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cronograma_pagos
    ADD CONSTRAINT id_solicitud FOREIGN KEY (id_solicitud) REFERENCES public.solicitud_credito(id_solicitud) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: movimiento_usuario id_usuario; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.movimiento_usuario
    ADD CONSTRAINT id_usuario FOREIGN KEY (id_usuario) REFERENCES public.usuario(id_usuario) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- PostgreSQL database dump complete
--

